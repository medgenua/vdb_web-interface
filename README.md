## VARIANTDB : Flexible Variant filtering and annotation tool

VariantDB is an online, centralized Variant filtering and annotation tool for Whole Exome Sequencing (WES) data. 

It consists of: 

* A php/cgi enabled webserver (we use apache)
* a mysql database using MyISAM tables (we use version is MySQL v8)
* A large annotation stack

VariantDB can be setup on a single machine, but keep the following requirements in mind : 

* Our 20K WES samples database is 550G in size. 
* The annotation stack has a 520G data footprint (mainly CADD/ANNOVAR)
* parallellization of maintainance and querying requires sufficient CPU/RAM. 
    
   
We have a dedicated webserver with 8CPU / 16G of ram and a semi-dedicated DB server with 8CPU and 64G RAM. 



For information on installation, please contact me at : geert -dot- vandeweyer -at- uza -dot- be

Geert
