<?php
// variables
$action = $_POST['action'];
$sid = $_POST['sid'];
$vid = $_POST['vid'];
$uid = $_POST['uid'];
$setid = $_POST['setid'];
$page_id = $_POST['page'];
// check for illegal values 
if (!is_numeric($sid) || !is_numeric($vid) || !is_numeric($uid) || !is_numeric($setid) || !is_numeric($page_id)) {
    echo "ILLEGAL VARIABLES PROVIDED. <br/>";
    echo "$sid : $vid : $uid : $setid : $page_id <br/>";
    //echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
    exit();
}
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// 0. check permission. 
$rows = runQuery("SELECT ps.pid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid Where ps.sid = '$sid' AND pu.uid = '$uid'", "Projects_x_Samples:Projects_x_Users");
if (count($rows) == 0) {
    echo "<h3>Permission denied</h3>";
    echo "<p>You do not have access to this sample.</p>";
    echo "<p><input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    exit();
}

// 1. GET THE FORM
if ($action == 'getform') {
    // get class etc from saved values.
    list($class, $inheritance, $inh_mode, $validation, $validation_details) = explode("|", $_POST['cv']);

    // get variant details
    $v = runQuery("SELECT chr, start, RefAllele, AltAllele FROM `Variants` WHERE id = '$vid'", "Variants")[0];
    // output	
    echo "<h3>Edit Significance and Validity for a Saved Result Set</h3>";
    echo "<p><table align=center><tr><td align=right class=emph>Variant:</td><td align=left>chr" . $v['chr'] . ":" . $v['start'] . " : " . $v['RefAllele'] . "/" . $v['AltAllele'] . "</td></tr>";
    // class
    echo "<tr><td align=right class=emph>Diagnostic Class:</td><td align=left>";
    $classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');

    if ($class == '') {
        $class = 0;
    }
    echo "<select id='VariantClass' name='class'>";
    for ($i = 0; $i <= 6; $i++) {
        if ($class == $i) {
            echo "<option value=$i SELECTED>" . $classes[$i] . "</option>";
        } else {
            echo "<option value=$i>" . $classes[$i] . "</option>";
        }
    }
    echo "</select></td></tr>";
    // Inheritance Mode
    echo "<tr><td align=right class=emph title='For functional variants, specify the (expected) inheritance model'>Inheritance Mode:</td><td align=left>";
    $inhm = array('0' => '-', '1' => 'Dominant', '2' => 'Recessive', '3', 'Unknown');
    if ($inh_mode == '') {
        $inh_mode = 0;
    }
    echo "<select id='VariantInhMode' name='inhm'>";
    for ($i = 0; $i <= 2; $i++) {
        if ($inh_mode == $i) {
            echo "<option value=$i SELECTED>" . $inhm[$i] . "</option>";
        } else {
            echo "<option value=$i>" . $inhm[$i] . "</option>";
        }
    }
    echo "</select></td></tr>";

    // inheritance
    echo "<tr><td align=right class=emph>Inheritance</td><td align=left>";
    echo "<select id='VariantInh' name='inh'>";
    $inheritances = array('0' => '-', '1' => 'Paternal', '2' => 'Maternal', '3' => 'De Novo', '4' => 'Both Parents');
    for ($i = 0; $i <= 4; $i++) {
        if ($inheritance == $i) {
            echo "<option value=$i SELECTED>" . $inheritances[$i] . "</option>";
        } else {
            echo "<option value=$i>" . $inheritances[$i] . "</option>";
        }
    }
    echo "</select></td></tr>";
    // validation
    echo "<tr title='Send requests for additional methods to the system admin'><td align=right class=emph>Validation Method</td><td align=left>";
    echo "<select id='VariantValidation' name='inh'>";
    $valmethods = array("-", "Sanger", "HRM", "qPCR", "MLPA", "NGS", "MicroArray");
    foreach ($valmethods as $key => $method) {
        if ($validation == "$method") {
            echo "<option value='$method' SELECTED>$method</option>";
        } else {
            echo "<option value='$method'>$method</option>";
        }
    }
    echo "</select></td></tr>";
    echo "<tr title='Add Validation Details'><td align=right class=emph>Validation Details</td><td align=left>";
    echo "<textarea id='Validationdetails' cols=30 row=5>" . stripslashes($validation_details) . "</textarea>";

    echo "</td></tr>";
    echo "</table>";
    echo "</p>";
    // some hidden fields
    echo "<input type=hidden id='sid' value='$sid'><input type=hidden id='vid' value='$vid'><input type=hidden id='uid' value='$uid'><input type=hidden id='setid' value='$setid'><input type=hidden id='page' value='$page_id'>";
    // submission buttons
    echo "<p><input type=submit value='Submit' class=button onClick=\"UpdateStoredValidity()\"/> <input type=submit value='Cancel' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";

    exit();
}

// update values
if ($action == 'Update') {
    // some variables.
    $colors = array(0 => '#aeaeae', 1 => 'red', 2 => 'red', 3 => 'orange', 4 => 'green', 5 => 'blue');
    $inh = array(0 => 'ND', 1 => 'P', 2 =>  'M', 3 => 'DN', 4 => 'Both', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined', 'Both' => 'Both Parents');
    $inhm = array(0 => '-', 1 => 'Dominant', 2 => 'Recessive', '3' => 'Unknown');
    $classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');

    // get extra posted values
    $vclass = $_POST['vclass'];
    $vinh = $_POST['vinh'];
    $vinhm = $_POST['vinhm'];
    $vval = $_POST['vval'];
    $vvaldet = $_POST['vvaldet'];
    $new_values = "$vclass|$vinh|$vinhm|$vval|$vvaldet";
    $current_values = $_POST['cv'];
    list($cclass, $cinheritance, $cinh_mode, $cvalidation, $cvalidation_details) = explode("|", $_POST['cv']);
    // get the old entry.
    $row = runQuery("SELECT contents FROM `Samples_x_Saved_Results_Pages` WHERE set_id = '$setid' AND page = '$page_id'", "Samples_x_Saved_Results_Pages")[0];
    $contents = $row['contents'];
    // correct some syntax problems...
    $contents = str_replace("up-to-date!</p></p>", "up-to-date!</p>", $contents);
    // replace html entities
    //$contents = htmlentities($contents);
    // DomDocument is very sensitive to syntax. silence problems...
    $internalErrors = libxml_use_internal_errors(true);
    $doc = new DomDocument;
    @$doc->loadHTML($contents);

    // 1. border color for the div.
    $color = $colors[$vclass];
    $doc->getElementById("box_$vid")->setAttribute("style", "width:15em;border:1px solid $color;padding:0.3em;font-size:0.75em");
    // 2. stored values
    $doc->getElementById("sv_$vid")->nodeValue = $new_values;
    // 3. update class.
    $doc->getElementById("dc_$vid")->nodeValue = $classes[$vclass];
    // 4+5. update inh // problem with legacy results : missing nodes
    if (!$doc->getElementById("ih_$vid")) {
        // the inh_$vid node is missing : legacy set. add missing  
        // create the new elements.
        $ih_e = $doc->createElement("span", $inh[$inh[$vinh]]);
        $ih_e->setAttribute("id", "ih_$vid");

        $im_e = $doc->createElement("span", $inhm[$vinhm]);
        $im_e->setAttribute("id", "im_$vid");

        //$label_ih = $doc->getElementById("dc_$vid")->previousSibling->cloneNode()->nodeValue("Set Inheritance:");
        $label_ihm = $doc->createElement("span", "Inh.Mode:");
        $label_ihm->setAttribute("class", "italic");
        $newline = $doc->createElement("br");

        // insert inh_mode after Auto class'd.
        if (!$doc->getElementById("ac_$vid")) {
            trigger_error("ac_ tag not found for $vid in setStoredValidity with post : " . json_encode($_POST), E_USER_ERROR);
        } else {
            $ac_e = $doc->getElementById("ac_$vid");
            $ac_e->parentNode->insertBefore($im_e, $ac_e->nextSibling);
            $ac_e->parentNode->insertBefore($label_ihm, $ac_e->nextSibling);
            $ac_e->parentNode->insertBefore($newline, $ac_e->nextSibling);
        }
        // insert inh in location of mislabeled one.
        $del_e = $doc->getElementById("$vid");
        $del_e->parentNode->insertBefore($ih_e, $del_e);
        $del_e->parentNode->removeChild($del_e);
        //$doc->appendChild($e);
    } else {
        $doc->getElementById("ih_$vid")->nodeValue = $inh[$inh[$vinh]];
        $doc->getElementById("im_$vid")->nodeValue = $inhm[$vinhm];
    }

    // 6. update validation
    $doc->getElementById("vm_$vid")->nodeValue = $vval;
    // 7. update validation details
    $doc->getElementById("vd_$vid")->nodeValue = $vvaldet;

    // convert back to string.
    $new_contents = $doc->saveHTML();
    $mysqli = GetConnection();
    doQuery("UPDATE `Samples_x_Saved_Results_Pages` SET `contents` = '" . $mysqli->real_escape_string($new_contents) . "' WHERE  set_id = '$setid' AND page = '$page_id'", "Samples_x_Saved_Results_Pages");
    // update log
    $entry = 'Updated Saved Signifance and Validity';
    $arguments = '';
    if ($cinheritance != $vinh) {
        $inheritances = array('0' => '-', '1' => 'Paternal', '2' => 'Maternal', '3' => 'De Novo', '4' => 'Both Parents');
        $arguments .= "Inheritance (" . $inheritances[$cinheritance] . " to " . $inheritances[$vinh] . "),";
    }
    if ($cinh_mode != $vinhm) {
        $inhm = array('0' => 'Unknown', '1' => 'Dominant', '2' => 'Recessive');
        $arguments .= "Inheritance Mode (" . $inhm[$cinh_mode] . " to " . $inhm[$vinhm] . "),";
    }

    if ($cclass == '') {
        $cclass = 0;
    }
    if ($cclass != $vclass) {
        $classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');

        $arguments .= "Diagnostic Class (" . $classes[$cclass] . " to " . $classes[$vclass] . "),";
    }
    if ($cvalidation != $vval) {
        $valmethods = array("-", "Sanger", "HRM", "qPCR", "MLPA", "NGS", "MicroArray");
        $arguments .= "Validation Method (" . $cvalidation . " to " . $vval . "),";
    }
    if (addslashes($cvalidation_details) != addslashes($vvaldet)) {
        if ($cvalidation_details == '') {
            $cvalidation_details = '-';
        }
        $arguments .= "Validation Details (old value:" . addslashes($cvalidation_details) . "),";
    }
    if ($arguments == '') {
        $arguments = 'No changes made';
    } else {
        $arguments = substr($arguments, 0, -1);
    }
    insertQuery("INSERT INTO `Log` (sid, vid, uid, entry, arguments) VALUES ('$sid','$vid','$uid','$entry','$arguments')", "Log");
    // all done.
    echo "OK";
    exit();
}
