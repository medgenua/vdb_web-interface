<?php
// translate all GET to POST
foreach ($_GET as $k => $v) {
    $_POST[$k] = $v;
}
$action = $_POST['a'];
include('../.LoadCredentials.php');
## GET USERID FOR USE IN COOKIES
if ($action == 'u') {
    echo $_SESSION['userID'];
    exit;
}
#######################
# CONNECT TO DATABASE #
#######################
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

## GET MAIN POSTED VARIABLES
$uid = $_POST['uid'];
//$name = $_POST['name'];
## STORE COOKIE
if ($action == 's') {
    $name = $_POST['name'];
    $value = addslashes($_POST['value']);
    insertQuery("INSERT INTO `Cookies` (ckey,cvalue) VALUES ('$uid" . "_$name','$value') ON DUPLICATE KEY UPDATE cvalue = '$value'", "Cookies");
}
## GET COOKIE
elseif ($action == 'g') {
    $name = $_POST['name'];
    $r = array_shift(...[runQuery("SELECT cvalue FROM `Cookies` WHERE ckey = '$uid" . "_$name'", "Cookies")]);
    if (isset($r['cvalue'])) {
        echo $r['cvalue'];
    } else {
        echo "";
    }
}
## BATCH GET COOKIE
elseif ($action == 'bg') {
    $in_str = "";
    $result = array();

    foreach ($_POST as $k => $v) {
        if ($k == 'uid' || $k == 'a' || $k == '') {
            continue;
        }
        $in_str .= "'$uid" . "_$k',";
    }
    if ($in_str != '') {
        $in_str = substr($in_str, 0, -1);
    } else {
        echo json_encode($result);
        exit;
    }
    $r = runQuery("SELECT ckey,cvalue FROM `Cookies` WHERE ckey IN ($in_str)", "Cookies");
    foreach ($r as $k => $row) {
        $row['ckey'] = str_replace($uid . "_", "", $row['ckey']);
        $result[$row['ckey']] = $row['cvalue'];
    }
    echo json_encode($result);
}

## DELETE COOKIE
elseif ($action == 'd') {
    $name = $_POST['name'];
    doQuery("DELETE FROM `Cookies` WHERE ckey = '$uid" . "_$name'", "Cookies");
}
## BATCH Set
elseif ($action == 'bs') {
    doQuery("DELETE FROM `Cookies` WHERE `ckey` LIKE '$uid" . "\_%'", "Cookies");
    foreach ($_POST as $k => $v) {
        if ($k == 'uid' || $k == 'a') {
            continue;
        }
        //if ($k != 'FCs' && $k != 'FS') {
        $v = addslashes($v);
        //}
        insertQuery("INSERT INTO `Cookies` (ckey,cvalue) VALUES ('$uid" . "_$k','$v') ", "Cookies");
    }
}
## BATCH DELETE COOKIE
elseif ($action == 'bd') {
    $in_str = "";
    foreach ($_POST as $k => $v) {
        if ($k == 'uid' || $k == 'a' || $k == '') {
            continue;
        }
        $in_str .= "'$uid" . "_$k',";
    }
    if ($in_str != '') {
        $in_str = substr($in_str, 0, -1);
    }
    doQuery("DELETE FROM `Cookies` WHERE ckey IN ($in_str)", "Cookies");
}
