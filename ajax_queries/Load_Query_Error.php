<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');
$userid = $_GET['uid'];
$qid = $_GET['qid'];
if (!is_numeric($userid) || !is_numeric($qid)) {
    echo -1;
    exit;
}
//require("../includes/inc_query_functions.inc");

## check access of user to sample.
if (file_exists("$scriptdir/Query_Results/$userid/$qid/stderr")) {
    echo "<div class='w100'><h3>Query failed.</h3>";
    echo "<p>Investigate the error below, and correct your settings.</p><p><input type=button onClick=\"RunQuery('0');\" value='Run Query'></p>";
    echo "<p><span class=emph>Error message:</span><pre>";
    include("$scriptdir/Query_Results/$userid/$qid/stderr");
    echo "</pre>";

    echo "</div>";
} else {
    trigger_error("Unable to locate error log for request:" . http_build_query($_GET, '', ','), E_USER_ERROR);
    echo "<div class='w100'><h3>Query Failed</h3>";
    echo "<p>Unable to locate the error log. This is probably a system errors. Please report.</p>";
    echo "<p><input type=button onClick=\"RunQuery('0');\" value='Retry Query'></p>";
    echo "</div>";
}
exit;
