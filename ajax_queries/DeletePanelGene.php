<?php
session_start();
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');

$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

## GET MAIN POSTED VARIABLES
//$uid = $_GET['uid'];
$uid = $_SESSION['userID'];
$gpid = $_GET['gpid'];
//allowed ?
$rows = array_shift(...[runQuery("SELECT rw FROM `GenePanels_x_Users` WHERE gpid = '$gpid' AND uid = '$uid'", "GenePanels_x_Users")]);
if ($rows['rw'] != 1) {
    echo "denied";
    exit;
}
$symbol = $_GET['symbol'];
doQuery("DELETE FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = '$gpid' AND Symbol = '$symbol'", "GenePanels_x_Genes_ncbigene");
doQuery("DELETE FROM `Variants_x_GenePanels_ncbigene` WHERE gpid = '$gpid' AND gene = '$symbol'", "Variants_x_GenePanels_ncbigene");
doQuery("UPDATE `GenePanels` SET LastEdit = CURRENT_TIMESTAMP WHERE id = '$gpid'");
doQuery("INSERT INTO `GenePanels_Log` (gpid, uid, message) VALUES ('$gpid','$uid','Deleted Gene : $symbol')");
//clearMemcache("GenePanels_x_Genes:Variants_x_GenePanels");
// remove bed files.
$command = "rm -f $scriptdir/BED_Files/$gpid.full.* $scriptdir/BED_Files/$gpid.cds.*";
system($command);



echo "1";
