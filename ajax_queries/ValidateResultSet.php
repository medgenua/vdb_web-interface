<?php

// GOAL :
//   - Finalize a "saved filter set"
//      - set readonly
//      - update variant classes etc from the saved to live results
//      - update classifiers 
//   - manage saved sets : delete / un-finalize / ... 



// variables
$action = $_POST['action'];
$sid = $_POST['sid'];
$setid = $_POST['setid'];
$uid = $_POST['uid'];
// check for illegal values 
if (!is_numeric($sid) || !is_numeric($setid) || !is_numeric($uid)) {
    echo "ILLEGAL VARIABLES PROVIDED. <br/>";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
    exit();
}
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// sanitize input
if (isset($_POST['comments'])) {
    $comments = SqlEscapeValues($_POST['comments']);
} else {
    $comments = '';
}

// mapping array
$classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');
$inhm = array('0' => 'Unknown', '1' => 'Dominant', '2' => 'Recessive');

// 0. check permission. 
$rows = runQuery("SELECT ps.pid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid Where ps.sid = '$sid' AND pu.uid = '$uid' AND pu.editvariant = 1", "Projects_x_Samples:Projects_x_Users");
if (count($rows) == 0) {
    echo "<h3>Permission denied</h3>";
    echo "<p>You do not have sufficient access to validate variant sets.</p>";
    echo "<p><input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    exit();
}


// 1. update db 
if ($action == 'validate') {
    // get values to update in DB.
    $changed_values = explode('@@@', $_POST['allchanges']);


    // update data in database for changed variants. 
    foreach ($changed_values as $k => $item) {
        if ($item == '') {
            continue;
        }
        list($vid, $vclass, $vinh, $vinhm, $vvalidation, $vvalidation_details) = explode('|', $item);
        $vvalidation_details = SqlEscapeValues($vvalidation_details);
        if (is_numeric($vid)) {
            // insert into logs if changes.
            // get old settings.
            $vs = runQuery("SELECT inheritance, InheritanceMode, class, validation, validationdetails,autoclassified FROM `Variants_x_Samples` WHERE vid = '$vid' AND sid = '$sid'", "Variants_x_Samples")[0];
            $v = runQuery("SELECT chr, start, RefAllele, AltAllele FROM `Variants` WHERE id = '$vid'", "Variants")[0];
            // update values
            $updateSummary = 0;
            $entry = 'Updated Signifance and Validity';
            $arguments = '';
            if ($vs['inheritance'] != $vinh) {
                $updateSummary = 1;
                $inheritances = array('0' => '-', '1' => 'Paternal', '2' => 'Maternal', '3' => 'De Novo', '4' => 'Both Parents');
                $arguments .= "Inheritance (" . $inheritances[$vs['inheritance']] . " to " . $inheritances[$vinh] . "),";
            }
            if ($vs['InheritanceMode'] != $vinhm) {
                $updateSummary = 1;
                $arguments .= "Inheritance Mode (" . $inhm[$vs['InheritanceMode']] . " to " . $inhm[$vinhm] . "),";
            }

            if ($vs['class'] == '') {
                $vs['class'] = 0;
            }
            if ($vs['class'] != $vclass) {
                $updateSummary = 1;
                $arguments .= "Diagnostic Class (" . $classes[$vs['class']] . " to " . $classes[$vclass] . "),";
            }
            if ($vs['validation'] != $vvalidation) {
                $arguments .= "Validation Method (" . $vs['validation'] . " to " . $vvalidation . "),";
            }
            if ($vs['validationdetails'] != $vvalidation_details) {
                if ($vs['validationdetails'] == '') {
                    $vs['validationdetails'] = '-';
                }
                $arguments .= "Validation Details (old value:" . SqlEscapeValues($vs['validationdetails']) . "),";
            }
            if ($arguments == '') {
                $arguments = 'No changes made';
            } else {
                $arguments = substr($arguments, 0, -1);
            }
            insertQuery("INSERT INTO `Log` (sid, vid, uid, entry, arguments) VALUES ('$sid','$vid','$uid','$entry','$arguments')", "Log");
            $ct = "Variants_x_Samples";
            if ($updateSummary == 1) {
                $ct .= ":Variants_x_Users_Summary:Variants_x_Projects_Summary:Summary";
            }
            doQuery("UPDATE `Variants_x_Samples` SET class = '$vclass', inheritance = '$vinh',  InheritanceMode = '$vinhm', validation = '$vvalidation', validationdetails = '$vvalidation_details' WHERE vid = '$vid' AND sid = '$sid'", $ct);
            if ($updateSummary == 1) {
                $lock = fopen("../Query_Results/.VariantSummary.lock", 'a');
                if (flock($lock, LOCK_EX)) {
                    if (!file_exists("../Query_Results/.VariantSummary.queue")) {
                        touch("../Query_Results/.VariantSummary.queue");
                        system("chmod 777 '../Query_Results/.VariantSummary.queue'");
                    }
                    $file = fopen("../Query_Results/.VariantSummary.queue", 'a');
                    fwrite($file, "$vid,$sid\n");
                    fclose($file);
                    // release lock
                    flock($lock, LOCK_UN);
                } else {
                    echo "Error locking file!";
                }
                fclose($lock);
            }
        }
    }
    ///////////////////////////////////////////////////
    // Check if variants can be added to classifiers. //
    ///////////////////////////////////////////////////
    // load the current classifier variants
    $c_rows = runQuery("SELECT c.id, c.Name, cu.can_validate FROM `Classifiers` c JOIN `Users_x_Classifiers` cu ON c.id = cu.cid WHERE cu.uid = '$uid' AND `can_add` = 1", "Classifiers:Users_x_Classifiers");
    $classifiers = array();
    foreach ($c_rows as $c_row) {
        $classifiers[$c_row["Name"]] = array(
            "validate" => $c_row["can_validate"],
            "variants" => array(),
            "id" => $c_row["id"]
        );
        // add the variants.
        $c_variants = runQuery("SELECT vid FROM `Classifiers_x_Variants` WHERE cid = '" . $c_row['id'] . "'");
        foreach ($c_variants as $c_variant) {
            array_push($classifiers[$c_row["Name"]]["variants"], $c_variant['vid']);
        }
    }
    // load the checkbox values from the saved results (for classifiers) : array[variant_id] = checkbox_DOM
    $saved_pages = runQuery("SELECT `contents` FROM `Samples_x_Saved_Results_Pages` WHERE `set_id` = '$setid'");
    $cb_contents = array();
    foreach ($saved_pages as $page) {
        // extract cb_tables & cleanup.
        // for each id & label/for : add the 'ic_<cid>_' prefix
        $internalErrors = libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        // load, escaping non default characters (eg > in cPOINT)
        //$doc->loadHTML(htmlentities($page['contents']));
        $doc->loadHTML($page['contents']);
        $tables = $doc->getElementsByTagName("table");
        foreach ($tables as $table) {
            $node_id = $table->getAttribute('id');
            if (substr($node_id, 0, 9) == "cb_table_") {
                $node_vid = substr($node_id, 9);
                $str = $doc->saveHTML($table);
                $str = preg_replace("/<table id=[\"']cb_table_\d+[\"'] cellspacing=[\"']0[\"'] style=[\"']width:100%[\"']>/", "<tbody>", $str);
                $str = preg_replace("/<\/table>/", "</tbody>", $str);
                $str = preg_replace("/(id=[\"'])/", "$1" . "ic_%cid%" . "_", $str);
                $str = preg_replace("/(for=[\"'])/", "$1" . "ic_%cid%" . "_", $str);
                $cb_contents[$node_vid] = $str;
            }
        }
    }

    // then loop all detected variants and process.
    foreach ($cb_contents as $vid => $cbs) {
        // get class & inheritance
        $v_row = runQuery("SELECT `class`, `inheritance`, `InheritanceMode`, `validationdetails` FROM `Variants_x_Samples` WHERE `vid` = '$vid' AND `sid` = '$sid'")[0];
        $vclass = $v_row['class'];
        $vinh = $v_row['inheritance'];
        $vinhm = $v_row['InheritanceMode'];
        $vcomments = $v_row['validationdetails'];
        // skip if not set or FP 
        if ($vclass == 0 || $vclass == 6) {
            continue;
        }
        // do I have access to a classifier for this class ? 
        if (!array_key_exists($classes[$vclass], $classifiers)) {
            continue;
        }
        // skip if variant is already in the classifier.
        if (in_array($vid, $classifiers[$classes[$vclass]]["variants"])) {
            //trigger_error("Variant $vid is already in classifier " . $classes[$vclass]);
            continue;
        }

        // construct info : cid, vid, added_by, validate_reject, 
        if ($classifiers[$classes[$vclass]]["validate"] == 1) {
            $validate = 1;
            $validated_on = "CURRENT_TIMESTAMP";
            $validate_by = $uid;
        } else {
            $validate = 0;
            $validated_on = "'0000-00-00 00:00:00'";
            $validate_by = 0;
        }
        // if inheritance is recessive, set genotype to Hom.Alt. ; else : any alternate
        // set to any.alt on request of lab
        //if ($vinhm == 2) {
        //    $gt = "2";
        //} else {
        $gt = "1,2";
        //}
        // the classifier id
        $cid = $classifiers[$classes[$vclass]]["id"];
        // checkboxes 
        $cbs = str_replace('%cid%', $cid, $cbs);
        $cbs = SqlEscapeValues($cbs);

        // reason to add == variant validation details (from 'subbox')
        $comments = SqlEscapeValues($vcomments);

        // insert the variant !
        doQuery("INSERT INTO `Classifiers_x_Variants` VALUES ('$cid','$vid','$uid', CURRENT_TIMESTAMP, '$validate','$validate_by', $validated_on, '$gt', '$vclass', '$vinhm', '$comments', '$cbs', '$sid') ");
    }

    // validate the result set.
    doQuery("UPDATE `Samples_x_Saved_Results` SET `Validated` = 1, `Validation_Comments` = '$comments',`validation_uid` = $uid WHERE set_id = '$setid'", "Samples_x_Saved_Results");
    echo "OK";
    exit();
}
if ($action == 'delete') {
    doQuery("DELETE FROM `Samples_x_Saved_Results` WHERE set_id = '$setid'", "Samples_x_Saved_Results");
    doQuery("DELETE FROM `Samples_x_Saved_Results_Pages` WHERE set_id = '$setid'", "Samples_x_Saved_Results_Pages");
    echo "OK";
    exit;
}
// update values
if ($action == 'Update') {
    // get extra posted values
    $vclass = $_POST['vclass'];
    $vinh = $_POST['vinh'];
    $vval = $_POST['vval'];
    $vvaldet = $_POST['vvaldet'];
    // get old settings.
    $vs = runQuery("SELECT inheritance, class, validation, validationdetails FROM `Variants_x_Samples` WHERE vid = '$vid' AND sid = '$sid'", "Variants_x_Samples")[0];
    $v = runQuery("SELECT chr, start, RefAllele, AltAllele FROM `Variants` WHERE id = '$vid'", "Variants")[0];

    // update log
    $entry = 'Updated Signifance and Validity';
    $arguments = '';
    $updateSummary = 0;
    if ($vs['inheritance'] != $vinh) {
        $updateSummary = 1;
        $inheritances = array('0' => '-', '1' => 'Paternal', '2' => 'Maternal', '3' => 'De Novo', '4' => 'Both Parents');
        $arguments .= "Inheritance (" . $inheritances[$vs['inheritance']] . " to " . $inheritances[$vinh] . "),";
    }
    if ($vs['class'] == '') {
        $vs['class'] = 0;
    }
    if ($vs['class'] != $vclass) {
        $updateSummary = 1;
        $classes = array('0' => '-', '1' => 'Known Disease Variant', '2' => 'Pathogenic', '3' => 'Unclear', '4' => 'Benign Variant', '5' => 'False Positive');
        $arguments .= "Diagnostic Class (" . $classes[$vs['class']] . " to " . $classes[$vclass] . "),";
    }
    if ($vs['validation'] != $vval) {
        $valmethods = array("-", "Sanger", "HRM", "qPCR", "MLPA", "NGS", "MicroArray");
        $arguments .= "Validation Method (" . $valmethods . " to " . $vval . "),";
    }
    if ($vs['validationdetails'] != SqlEscapeValues($vvaldet)) {
        if ($vs['validationdetails'] == '') {
            $vs['validationdetails'] = '-';
        }
        $arguments .= "Validation Details (old value:" . $vs['validationdetails'] . "),";
    }
    if ($arguments == '') {
        $arguments = 'No changes made';
    } else {
        $arguments = substr($arguments, 0, -1);
    }
    insertQuery("INSERT INTO `Log` (sid, vid, uid, entry, arguments) VALUES ('$sid','$vid','$uid','$entry','$arguments')", "Log");
    // update values
    $ct = "Variants_x_Samples";
    if ($updateSummary == 1) {
        $ct .= ":Variants_x_Users_Summary:Variants_x_Projects_Summary:Summary";
    }
    doQuery("UPDATE `Variants_x_Samples` SET class = '$vclass' , inheritance = '$vinh', validation = '$vval', validationdetails = '" . SqlEscapeValues($vvaldet) . "' WHERE vid = '$vid' AND sid = '$sid'", $ct);
    if ($updateSummary == 1) {
        $lock = fopen("../Query_Results/.VariantSummary.lock", 'a');
        if (flock($lock, LOCK_EX)) {
            if (!file_exists("../Query_Results/.VariantSummary.queue")) {
                touch("../Query_Results/.VariantSummary.queue");
                system("chmod 777 '../Query_Results/.VariantSummary.queue'");
            }
            $file = fopen("../Query_Results/.VariantSummary.queue", 'a');
            fwrite($file, "$vid,$sid\n");
            fclose($file);
            // release lock
            flock($lock, LOCK_UN);
        } else {
            echo "Error locking file!";
        }
        fclose($lock);
    }
    // all done.
    echo "OK";
    exit();
}
