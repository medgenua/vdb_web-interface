<?php

$sid = $_GET['sid'];
if (!is_numeric($sid)) {
    echo "Invalid Sample ID";
    exit;
}
#######################
# CONNECT TO DATABASE # ASC
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// Get sample name
$row = runQuery("SELECT Name FROM `Samples` WHERE id = '$sid'", "Samples");
if (count($row) == 0) {
    echo "<h3>Sample '$sid' not found.</h3>";
    exit();
}
$sname = $row[0]['Name'];
// disabled the old format, it was a mess. typical calls come with 'summary' type.
if ($_GET['type'] != 'summary') {
    echo "<h3>Summary Log for Sample '$sname'</h3>";
    $rows = runQuery("SELECT arguments, COUNT(id) AS nrLines FROM  `Log` WHERE sid =  '$sid' GROUP BY arguments ORDER BY arguments ASC", "Log");
    if (count($rows) == 0) {
        print "<p>No events logged.</p>";
    }
    foreach ($rows as $k => $row) {
        // header
        echo "<span class=emph>Type: " . $row['arguments'] . ":</span> " . $row['nrLines'] . " entries.<br/>";
        if (substr($row['arguments'], 0, 10) == 'Annotation') {
            echo "&nbsp;&nbsp;<a href='index.php?page=LogDetails&t=" . $row['arguments'] . "&s=$sid'>Examine Annotation Updates </a><br/>";
        } else {
            echo "<table cellspacing=0 class=w100 style='padding-left:1em'>";
            echo "<tr><th class=top>Variant</th><th class=top>Date</th><th class=top>Information</th><th class=top>Performed by User</th></tr>";
            $srs = runQuery("SELECT l.time, v.chr, v.start, v.RefAllele, v.AltAllele, l.entry, u.LastName, u.FirstName FROM `Log` l JOIN `Variants` v JOIN `Users`u ON l.uid = u.id AND v.id = l.vid WHERE l.sid = '$sid' AND l.arguments = '" . $row['arguments'] . "' ORDER BY time DESC LIMIT 5", "Log:Variants");
            echo "<br/>";
            foreach ($srs as $k => $sr) {
                echo "<tr><td>chr" . $sr['chr'] . ":" . $sr['start'] . " " . $sr['RefAllele'] . "/" . $sr['AltAllele'] . "</td>";
                echo "<td>" . $sr['time'] . "</td>";
                echo "<td>" . $sr['entry'] . "</td>";
                echo "<td>" . $sr['LastName'] . ", " . substr($sr['FirstName'], 0, 1) . ".</td></tr>";
            }
            echo "<tr><td colspan=4 class=last>&nbsp;</td></tr></table><br/>";
        }
    }
}
// switched to simple chronological listing of all entries.
else {

    $rows = runQuery("SELECT l.time, l.entry, l.arguments, u.LastName, u.FirstName, l.vid FROM `Log` l JOIN `Users` u ON l.uid = u.id WHERE l.sid = '$sid' ORDER BY l.id DESC", "Log:Users");

    if (count($rows) == 0) {
        echo "<h3>Log for Sample '$sname'</h3>";
        echo "<p>No entries present.</p>";
        exit();
    }
    echo "<h3>Log for Sample '$sname'</h3>";
    echo "<p><ul id=nodisc>";
    foreach ($rows as $k => $row) {
        $srow = runQuery("SELECT chr, start FROM `Variants` WHERE id = " . $row['vid'], "Variants")[0];
        echo "<li><span class='italic'>" . substr($row['time'], 0, 10) . ": " . $row['LastName'] . " " . substr($row['FirstName'], 0, 1) . ".:</span> chr" . $srow['chr'] . ":" . number_format($srow['start'], 0, '', ',') . ' : ' . $row['entry'] . ":" . $row['arguments'] . "</li>";
    }
    echo "</ul></p>";
}
