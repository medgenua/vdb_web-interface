<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

## GET MAIN POSTED VARIABLES
$uid = $_POST['uid'];
$StoreWhat = $_POST['StoreWhat'];
$SaveAs = addslashes($_POST['SaveAs']);
$Comments = addslashes($_POST['SaveComments']);

## unset them in the POST array
unset($_POST['uid']);
unset($_POST['StoreWhat']);
unset($_POST['SaveAs']);
unset($_POST['SaveComments']);
if ($StoreWhat == 'Filter') {
    ## Store the rest of the variables in a string, seperated by '@@@';
    $settings = '';
    $filtertree = '';
    foreach ($_POST as $key => $value) {
        if ($key == 'FilterLogic') {
            $filtertree = addslashes($value);
        } else {
            $settings .= "$key|||$value@@@";
        }
    }
    if ($settings != '') {
        $settings = substr($settings, 0, -3);
    }
    $settings = addslashes($settings);
    $filtertree = addslashes($filtertree);
    ## store
    insertQuery("INSERT INTO `Users_x_FilterSettings` (uid, FilterName, FilterSettings, Comments,FilterTree) VALUES ('$uid','$SaveAs','$settings','$Comments','$filtertree')", "Users_x_FilterSettings");
} elseif ($StoreWhat == 'Annotation') {
    $selected = addslashes($_POST['selected']);
    # store
    insertQuery("INSERT INTO `Users_x_Annotations` (uid, AnnotationName, AnnotationSettings, Comments) VALUES ('$uid','$SaveAs','$selected','$Comments')", "Users_x_Annotations");
}
echo "1";
