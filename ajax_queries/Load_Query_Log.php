<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');

$userid = $_GET['uid'];
$qid = $_GET['qid'];
$killfile = $_GET['kf'];
if (!is_numeric($userid) || !is_numeric($qid)) {
    echo -1;
    exit;
}

## check access of user to sample.
if (file_exists("$scriptdir/Query_Results/$userid/$qid/stdout")) {
    echo "<div class='w100'><h3>Query is running</h3>";
    echo "<p><input type=button class=button onClick=\"KillQuery('$killfile')\" value='Stop Query'></p>";
    echo "<h4>Query Logs</h4>";
    echo "<pre>";
    include("$scriptdir/Query_Results/$userid/$qid/stdout");
    echo "</pre>";
    echo "</div>";
} else {
    echo "<div class='w100'><h3>Query Log not found</h3>";
    echo "<p>Locating : $scriptdir/Query_Results/$userid/$qid/stdout</p>";
    echo "</div>";
}
exit;
