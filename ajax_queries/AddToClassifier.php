<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');


// variables
$action = $_POST['action'];
$sid = (isset($_POST['sid']) ? $_POST['sid'] : '0');
$vid = (isset($_POST['vid']) ? $_POST['vid'] : '0');
$saved = (isset($_POST['saved']) ? $_POST['saved'] : '0');
$uid = (isset($_SESSION['userID']) ? $_SESSION['userID'] : $_POST['uid']);

// check for illegal values 
if (!is_numeric($sid) || !is_numeric($vid) || !is_numeric($uid)) {
    echo "ILLEGAL VARIABLES PROVIDED. <br/>";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
    exit();
}
// 0. check permission on sample, if specified. 
$rows = runQuery("SELECT ps.pid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid Where ps.sid = '$sid' AND pu.uid = '$uid'", "Projects_x_Samples:Projects_x_Users");
if ($sid != 0 && count($rows) == 0) {
    echo "<h3>Permission denied</h3>";
    echo "<p>You do not have access to this sample.</p>";
    echo "<p><input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    exit();
}
$cr = runQuery("SELECT c.Name, c.Description, c.id FROM `Classifiers` c JOIN `Users_x_Classifiers` uc ON c.id = uc.cid WHERE uc.uid = '$uid' AND can_add = '1'", "Classifiers:Users_x_Classifiers");
if (count($cr) == 0) {
    // no classifiers present : redirect.
    echo "<h3>No classifiers available</h3>";
    echo "<p>You don't have 'add' permission on any classifiers.  To create one now, click here</p>";
    echo "<p><input type=submit value='Cancel' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    exit();
}

// 1. GET THE FORM
if ($action == 'getform') {
    // load the default checkbox table?
    $lcb = isset($_POST['lcb']) ? $_POST['lcb'] : 1;
    $cbs = '';
    if ($lcb == 1) {
        $cbq = runQuery("SELECT c.`Title`,c.`Options`,c.`cid` FROM `Report_Section_CheckBox` c JOIN `Users_x_Report_Sections_CheckBox` uc ON uc.cid = c.cid WHERE uc.uid = $uid AND c.default = 1", "Report_Section_CheckBox:Users_x_Report_Sections_CheckBox");
        foreach ($cbq as $row) {
            $name = $row['Title'];
            $items = explode("||", $row['Options']);
            $cid = $row['cid'];
            $idx = 0;
            if (count($items) > 0) {
                $cbs .= "<tr><th colspan=6><span class=emph>$name</span></th></tr><tr>";
            }
            foreach ($items as $k) {
                $idx++;
                if ($idx > 6) {
                    $cbs .= "</tr><tr>";
                    $idx = 1;
                }
                $cbs .= "<td ><input type=checkbox class='NoteBox' title='N/A' id='a2c_$vid" . '_0_' . $cid . "_$k'><label for='$vid" . '_0_' . $cid . "_$k'>$k</label></td>";
            }
            while ($idx > 0 && $idx < 6) {
                $idx++;
                $cbs .= "<td>&nbsp;</td>";
            }
            if ($idx > 0) {
                $cbs .= "</tr>";
            }
        }
        if ($cbs != '') {
            $cbs = "<table id='cb_a2c_$vid' cellspacing=0 style='width:100%' class='cb_a2c'>$cbs</table>";
        }
    }

    // get class, inheritance, validation, and details.
    $vs = runQuery("SELECT inheritance, InheritanceMode, class, validation, validationdetails, AltCount FROM `Variants_x_Samples` WHERE vid = '$vid' AND sid = '$sid'", "Variants_x_Samples")[0];
    // get variant details
    $v = runQuery("SELECT chr, start, RefAllele, AltAllele FROM `Variants` WHERE id = '$vid'", "Variants")[0];
    $lr = runQuery("SELECT cv.cid,c.Name,c.Description, cv.validate_reject,cv.genotype,cv.class,cv.inheritance_mode,cv.comments,cv.cbs FROM `Classifiers_x_Variants` cv JOIN `Classifiers` c ON c.id = cv.cid WHERE vid = '$vid'", "Classifiers:Classifiers_x_Variants");
    $listed = array();
    foreach ($lr as $row) {
        $listed[$row['cid']] = 1;
    }

    // output	
    echo "<div style='height:90vh;overflow-y:auto;'>";
    echo "<h3>Add Variant to Classifier: </h3>";
    echo "<p><table align=center>";

    echo "<tr><td align=right class=emph>Variant:</td><td align=left>chr" . $v['chr'] . ":" . $v['start'] . " : " . $v['RefAllele'] . "/" . $v['AltAllele'] . "</td></tr>";
    // select classifiers
    echo "<tr><td align=right class=emph>Classifier:</td><td align=left><select id='add_to' name='add_to'  onChange=\"CheckSkipValidation('$uid')\">";
    $cr = runQuery("SELECT c.id,c.Name,c.Description FROM `Classifiers` c JOIN `Users_x_Classifiers` uc ON c.id = uc.cid WHERE uc.`can_add` = 1 AND uc.uid = '$uid' ORDER BY c.Name", "Classifiers:Users_x_Classifiers");
    echo "<option value='-' DISABLED selected> - </option>";
    foreach ($cr as $crow) {
        if (array_key_exists($crow['id'], $listed)) {
            echo "<option value='" . $crow['id'] . "' disabled >" . $crow['Name'] . " (member)</option>";
        } else {
            echo "<option value='" . $crow['id'] . "' >" . $crow['Name'] . "</option>";
        }
    }
    echo "</select></td></tr>";
    // genotype
    echo "<tr><td align=right class=emph>Apply to genotype:</td><td align=left>";
    $gts = array("0" => "Homozygous Reference", "1" => "Heterozygous", "2" => "Homozygous Alternate", "1,2" => "Any Alternate", "0,1,2" => "All Genotypes");
    echo "<select id='GenoType' name='genotype'>";
    foreach ($gts as $k => $v) {
        //if ($vs['AltCount'] == "$k") {
        if ($k == "1,2") {
            echo "<option value='$k' SELECTED>" . $v . "</option>";
        } else {
            echo "<option value='$k'>" . $v . "</option>";
        }
    }
    echo "</select></td></tr>";

    // class
    echo "<tr><td align=right class=emph>Diagnostic Class:</td><td align=left>";
    $classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');

    if ($vs['class'] == '') {
        $vs['class'] = 0;
    }
    echo "<select id='VariantClass' name='class'>";
    for ($i = 0; $i <= 6; $i++) {
        if ($vs['class'] == $i) {
            echo "<option value=$i SELECTED>" . $classes[$i] . "</option>";
        } else {
            echo "<option value=$i>" . $classes[$i] . "</option>";
        }
    }
    echo "</select></td></tr>";
    // Inheritance Mode
    echo "<tr><td align=right class=emph title='For functional variants, specify the (expected) inheritance model'>Inheritance Mode:</td><td align=left>";
    $inhm = array('0' => '-', '1' => 'Dominant', '2' => 'Recessive', '3' => 'Unknown');
    if ($vs['InheritanceMode'] == '') {
        $vs['InheritanceMode'] = 0;
    }
    echo "<select id='VariantInhMode' name='inhm'>";
    for ($i = 0; $i <= 2; $i++) {
        if ($vs['InheritanceMode'] == $i) {
            echo "<option value=$i SELECTED>" . $inhm[$i] . "</option>";
        } else {
            echo "<option value=$i>" . $inhm[$i] . "</option>";
        }
    }
    echo "</select></td></tr>";

    // comments
    echo "<tr><td align=right class=emph title='Why should this variant be added to the classifier'>Reason to add:</td><td align=left>";
    echo "<textarea id='comments' cols=30 row=5></textarea>";
    echo "</td></tr>";
    // can user skip validation?
    $c_selected = $cr[0]['id'];
    $vr = array_shift(...[runQuery("SELECT `can_validate` FROM `Users_x_Classifiers` WHERE uid = '$uid' AND cid = '$c_selected'", "Users_x_Classifiers")]);
    $checked = ($vr['can_validate'] == 1 ? '' : 'checked');
    $hidden = ($vr['can_validate'] == 1 ? '' : "style='display:none'");
    echo "<tr id='SkipValRow' $hidden><td align=right class=emph title='Check to request a second user to validate the variant addition'>Validation Required</td><td align=left><input type=checkbox id='need_validation' value='1' $checked/></td></tr>";
    echo "</table>";
    echo "</p>";
    // some hidden fields
    echo "<input type=hidden id='sid' value='$sid'><input type=hidden id='vid' value='$vid'><input type=hidden id='uid' value='$uid'>";
    // div to hold the checkboxes if present.
    if ($cbs == '') {
        echo "<div style='display:none' id='cbContainer'></div>";
    } else {
        echo "<div id='cbContainer'>$cbs</div>";
    }
    // submission buttons
    if ($saved == 1) {
        echo "<p><input type=submit value='Submit' class=button onClick=\"AddSavedVariantToClassifier('0')\"/> <input type=submit value='Cancel' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    } else {
        echo "<p><input type=submit value='Submit' class=button onClick=\"AddVariantToClassifier('0')\"/> <input type=submit value='Cancel' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    }
    // what classifiers can I validate ? 
    $cr = runQuery("SELECT c.id,uc.can_validate, uc.can_remove FROM `Classifiers` c JOIN `Users_x_Classifiers` uc ON c.id = uc.cid WHERE (uc.`can_validate` = 1 OR uc.`can_remove` = 1 ) AND uc.uid = '$uid' ORDER BY c.Name", "Classifiers:Users_x_Classifiers");
    $can_validate = array();
    $can_remove = array();
    foreach ($cr as $crow) {
        if ($crow['can_validate'] == 1) {
            array_push($can_validate, $crow['id']);
        }
        if ($crow['can_remove'] == 1) {
            array_push($can_remove, $crow['id']);
        }
    }
    // was it listed before?
    $lr = runQuery("SELECT cv.cid,c.Name,c.Description, cv.validate_reject,cv.genotype,cv.class,cv.inheritance_mode,cv.comments,cv.cbs FROM `Classifiers_x_Variants` cv JOIN `Classifiers` c ON c.id = cv.cid WHERE vid = '$vid'", "Classifiers:Classifiers_x_Variants");
    if (count($lr) > 0) {
        $valrej = array('-1' => 'Rejected', '0' => 'Pending', '1' => 'Accepted');
        echo "<h3>NOTE: Variant is already included in classifiers:</h3>";
        echo "<p><table align=center cellspacing=0 style='width:75em;text-align:left'>";
        echo "<tr><th style='font-weight:bold;border-top:1px solid white;border-bottom:1px solid white;'>Action</th><th style='font-weight:bold;border-top:1px solid white;border-bottom:1px solid white;'>Classifier</th><th style='font-weight:bold;border-top:1px solid white;border-bottom:1px solid white;'>Status</th><th style='font-weight:bold;border-top:1px solid white;border-bottom:1px solid white;'>As Genotype</th><th style='font-weight:bold;border-top:1px solid white;border-bottom:1px solid white;'>Class</th><th style='font-weight:bold;border-top:1px solid white;border-bottom:1px solid white;'>Inh.Mode</th><th style='font-weight:bold;border-top:1px solid white;border-bottom:1px solid white;'>Comment</th></tr>";
        foreach ($lr as $k => $row) {
            //trigger_error(json_encode($row));
            $line = "<tr>";
            // actions : details/edit
            $line .= "<td><a title='Details' href='index.php?page=inbox_actions&a=1&vid=$vid&c=" . $row['cid'] . "' target='_blank'><img src='Images/layout/edit.gif' style='height:1.1em;'></a>";
            // actions : remove
            if (in_array($row['cid'], $can_remove)) {
                $line .= " <img title='Remove' src='Images/layout/icon_trash.gif' style='height:1.1em;' onClick=\"ApproveClassifierVariant('-1','$vid','" . $row['cid'] . "','$uid','$sid','$saved')\"/>";
            }
            // actions : approve & delete
            if (in_array($row['cid'], $can_validate) && $row['validate_reject'] == 0) {
                $line .= " <img title='Approve' src='Images/layout/icon_approve.gif' style='height:1.1em;' onClick=\"ApproveClassifierVariant('1','$vid','" . $row['cid'] . "','$uid','$sid','$saved')\"/>";
                $line .= " <img title='Reject'  src='Images/layout/icon_disapprove.gif' style='height:1.1em;' onClick=\"ApproveClassifierVariant('0','$vid','" . $row['cid'] . "','$uid','$sid','$saved')\"/>";
            }
            $line .= "</td><td>" . $row['Name'] . "</td><td>" . $valrej[$row['validate_reject']];
            $line .= "</td><td>" . $gts[$row['genotype']] . "</td><td>" . $classes[$row['class']];
            $line .= "</td><td>" . $row['inheritance_mode'] . "/" . $inhm[$row['inheritance_mode']];
            $line .= "</td><td>" . $row['comments'] . "</td></tr>";

            $line .= "</tr>";
            // checkboxes ? 
            if ($row['cbs'] != '') {
                $table = stripslashes($row['cbs']);
                // only those allowed to validate/reject can reset checkboxes here, if variant is not validated/rejected yet.
                if (!in_array($row['cid'], $can_validate) || $row['validate_reject'] != 0) {
                    $table = preg_replace("/type=['\"]*checkbox['\"]*/", "type='checkbox' DISABLED ", $table);
                }
                $line .= "<tr><td>&nbsp;</td><td id='cbContainer' colspan=6><table style='margin-bottom:1em;width:100%;font-size:0.9em' id='cb_vc_" . $row['cid'] . "_$vid' class='cb_a2c' cellspacing=0>$table</table></td></tr>";
            }
            echo $line;
        }
        echo "<tr> <td colspan=7 style='border-top:1px solid white'> &nbsp;</td></tr></table></p>";
    }
    echo "</div>";
    exit();
}
// approve/disapprove/delete classifier variant
if ($action == 'approve') {
    if (!isset($_POST['type'])) {
        // invalid !
        exit;
    }

    $type = $_POST['type'];
    if (!isset($_POST['cid']) || !is_numeric($_POST['cid'])) {
        // invalid !
        exit;
    }
    $cid = $_POST['cid'];
    if ($type == -2) { // confirmed delete.
        // allowed?
        $cr = array_shift(...[runQuery("SELECT uc.can_remove FROM `Users_x_Classifiers` uc WHERE uc.cid = '$cid' AND uc.uid = '$uid'", "Users_x_Classifiers")]);
        if (count($cr) == 0 || $cr['can_remove'] == 0) {
            // not allowed
            trigger_error("Not allowed to remove vid $vid from cid $cid for uid $uid", E_USER_WARNING);
            exit;
        }
        // remove.
        doQuery("DELETE FROM `Classifiers_x_Variants` WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
        echo "ok";
        exit;
    } elseif ($type == -1) {
        // print confirmation message.
        echo "<h3>Confirmation needed</h3>";
        echo "<p>Deleting a variant from a classifier can not be undone. Are you sure you want to proceed?</p>";
        echo "<p><input type=submit class=button value='Yes' onClick=\"ApproveClassifierVariant('-2','$vid','$cid','$uid','$sid','$saved')\" /> &nbsp <input type=submit class=button value=Cancel onClick=\"AddToClassifier('$vid','$sid','$uid','$saved')\" /></p>";
        exit;
    }
    // here for type >= 0
    // allowed?
    $cr = array_shift(...[runQuery("SELECT uc.can_validate FROM `Users_x_Classifiers` uc WHERE uc.cid = '$cid' AND uc.uid = '$uid'", "Users_x_Classifiers")]);
    if (count($cr) == 0 || $cr['can_validate'] == 0) {
        // not allowed
        trigger_error("Not allowed to validate vid $vid in cid $cid for uid $uid", E_USER_WARNING);
        exit;
    }
    if ($type == 0) {
        // reject.
        $cb = isset($_POST['cb']) ? addslashes($_POST['cb']) : '';
        doQuery("UPDATE `Classifiers_x_Variants` SET `validate_reject` = -1,`validated_by` = '$uid', `cbs` = '$cb',`validated_on` = CURRENT_TIMESTAMP WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
    } elseif ($type == 1) {
        // approve.
        $cb = isset($_POST['cb']) ? addslashes($_POST['cb']) : '';
        doQuery("UPDATE `Classifiers_x_Variants` SET `validate_reject` = 1 ,`validated_by` = '$uid', `cbs` = '$cb',`validated_on` = CURRENT_TIMESTAMP WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
    } elseif ($type == 2) {
        // undo status
        doQuery("UPDATE `Classifiers_x_Variants` SET `validate_reject` = 0, `validated_by` = '0',`validated_on` = CURRENT_TIMESTAMP  WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
        $cb = isset($_POST['cb']) ? addslashes($_POST['cb']) : '';
        if ($cb != '') {
            doQuery("UPDATE `Classifiers_x_Variants` SET `cbs` = '$cb' WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
        }
    }
    echo "ok";
    exit;
}


// add variant to selected classifier
if ($action == 'Add') {
    // get extra posted values
    $vclass = $_POST['vclass'];
    $gt = $_POST['gt'];
    $vinhm = $_POST['vinhm'];
    $cid = $_POST['cid'];
    $comments = addslashes($_POST['comments']);
    $need_validation = $_POST['nv'];

    //error_log($_POST['cb']);
    $cbTable = (isset($_POST['cb']) ? $_POST['cb'] : '');
    // check if variant not in classifier.
    $rows = runQuery("SELECT vid FROM  `Classifiers_x_Variants` WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
    if (count($rows) > 0) {
        echo "PROBLEM: Variant is already listed as included in this classifier. ";
        exit;
    }
    // replace checkbox IDs (add classifier id)
    $cbTable = preg_replace("/(id=[\"'])a2c_/", "$1" . "ic_$cid" . "_", $cbTable);
    $cbTable = preg_replace("/(for=[\"'])a2c_/", "$1" . "ic_$cid" . "_", $cbTable);
    $cbTable = addslashes($cbTable);
    // insert.
    $lid = insertQuery("INSERT INTO `Classifiers_x_Variants` (`cid`, `vid`, `added_by`, `genotype`,`class`,`inheritance_mode`,`comments`,`sid`,`cbs`) VALUES ('$cid','$vid','$uid','$gt','$vclass','$vinhm','$comments','$sid','$cbTable')", "Classifier_x_Variants");

    // need to validate? 
    if ($need_validation == 1) {
        // Notify users that need to validate.
        $vrows = runQuery("SELECT uid FROM `Users_x_Classifiers` WHERE cid = '$cid'", "Users_x_Classifiers");
        foreach ($vrows as $k => $row) {
            $luid = $row['uid'];
            doQuery("REPLACE INTO `Inbox_actions` (`uid`,`action_type`) VALUES ('$luid','Validate Classifier')", "Inbox_actions");
        }
    } else {
        // update variant status.
        doQuery("UPDATE `Classifiers_x_Variants` SET `validate_reject` = '1' , `validated_by` = '$uid', `validated_on` = CURRENT_TIMESTAMP WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
    }
    echo "OK";
    exit();
}
// 3. Check permission to skip validation.
if ($action == 'CheckSkipVal') {
    $cid = (isset($_POST['cid']) ? $_POST['cid'] : 0);
    $row = array_shift(...[runQuery("SELECT `can_validate` FROM `Users_x_Classifiers` WHERE `uid` = $uid AND `cid` = $cid", "Users_x_Classifiers")]);
    if (count($row) == 0 || $row['can_validate'] == 0) {
        echo 0;
    } else {
        echo 1;
    }
    exit;
}
