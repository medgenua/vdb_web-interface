<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

$uid = $_GET['uid'];
$nrs = $_GET['nrs'];
if (!is_numeric($uid) || !is_numeric($nrs)) {
    die('Only numeric values accepted !!');
}

// get username
$row = runQuery("SELECT email, passwd_sha1 FROM `Users` WHERE id = '$uid'", "Users")[0];

$username = $row['email'];
if ($row['passwd_sha1'] == '') {
    die("FTP not activated for $username");
}
// check for files in ftp-dir
$dir = $config['FTP_DATA'] . "/$username";
$Files = array();
$VCFs = array();
$BAMs = array();
$VCFdirs = array();
$BAMdirs = array();
function TraverseDir($dir)
{
    $vcfs = $bams = '';
    $It =  opendir("$dir");
    if ($It) {
        while ($Filename = readdir($It)) {
            if ($Filename == '.' || $Filename == '..') {
                continue;
            }
            if (substr($Filename, -3) == "vcf" || substr($Filename, -3) == "bcf") {
                //return 1;	
                $vcfs .= "<option value='$dir/$Filename'>$Filename</option>";
            } elseif (strtolower(substr($Filename, -3)) == "bam") {
                $bams .= "<option value='$dir/$Filename'>$Filename</option>";
            } elseif (is_dir("$dir/$Filename")) {
                list($dv, $db) = TraverseDir("$dir/$Filename");
                if ($dv != '') {
                    $vcfs .= $dv;
                }
                if ($db != "") {
                    $bams .= $db;
                }
            }
        }
    }
    return array($vcfs, $bams);
}

$It =  opendir("$dir");
if ($It) {
    while ($Filename = readdir($It)) {
        if ($Filename == '.' || $Filename == '..')
            continue;
        if (strtolower(substr($Filename, -3)) == "vcf" || strtolower(substr($Filename, -3)) == "bcf") {
            $VCFs[] = $Filename;
        } elseif (strtolower(substr($Filename, -3)) == "bam") {
            $BAMs[] = $Filename;
        } elseif (is_dir("$dir/$Filename")) {
            list($dv, $db) = TraverseDir("$dir/$Filename");
            if ($dv != '') {
                $VCFdirs["$Filename"] = $dv;
            }
            if ($db != "") {
                $BAMdirs["$Filename"] = $db;
            }
        }
    }
}


sort($VCFs);
sort($BAMs);
ksort($VCFdirs);
ksort($BAMdirs);

// build output
$output = "<span class=emph>Sample $nrs:</span><br/>";
$output .= "<table>";
// samplename
$output .= "<tr><td>Sample Name:</td><td><input type=text name=name$nrs size=30 /></td></tr>";
// vcf file
$output .= "<tr><td>Select VCF File (mandatory):</td><td><select name=vcf$nrs>";
if (count($VCFs) > 0) {
    $output .= "<optgroup label='Root Folder'>";
}
//for ($i = 0; $i <= $num; $i += 1) {
foreach ($VCFs as $key => $value) {
    $output .= "<option value='$value'>" . $value . "</option>";
}
if (count($VCFs) > 0) {
    $output .= "</optgroup>";
}
foreach ($VCFdirs as $sdir => $options) {
    $output .= "<optgroup label='$sdir'>";
    $options = str_replace("$dir/", "", $options);
    $output .= $options;
    $output .= "</optgroup>";
}


$output .= "</select></td></tr>";
// vcf type 
$output .= "<tr><td>Select VCF Type:</td><td><select name=Format$nrs>";
$output .= "<optgroup label='Default Formats'>";
$output .= "<option value='UG' SELECTED>GATK Unified Genotyper</option>";
$output .= "<option value='HC'>GATK Haplotype Caller</option>";
$output .= "<option value='MT'>GATK MuTect</option>";
$output .= "<option value='VS'>Samtools VarScan Somatic</option>";
$output .= "<option value='VSC'>Samtools VarScan Cohort</option>";
$output .= "<option value='23'>23andMe (created by arrogantrobot)</option>";
$output .= "<option value='IT'>Torrent Variant Caller</option>";

$output .= "</optgroup>";
$output .= "</select></td></tr>";
/*
// OPTIONAL GVCF file.
$output .= "<tr><td>Select GVCF File (optional):</td><td><select name=gvcf$nrs>";
if (count($VCFs) > 0) {
	$output .= "<optgroup label='Root Folder'>";
}
foreach($VCFs as $key => $value) {
	$output .= "<option value='$value'>".$value."</option>";
}
if (count($VCFs) > 0) {
	$output .= "</optgroup>";
}
foreach($VCFdirs as $sdir => $options) {
	$output .= "<optgroup label='$sdir'>";
	$options = str_replace("$dir/","",$options);
	$output .= $options;
	$output .= "</optgroup>";
}
$output .= "</select></td></tr>";
$output .= "<tr><td>&nbsp;</td><td><span class=italic>GVCF files contain genome-wide information on the reliability of the reference allele. To reduce file size, this information is stored in blocks (genomic regions) of similar coverage. They are generated using GATK HaploTypeCaller '-ERC GVCF' parameter.</span></td></tr>";
*/

// bam file
$output .= "<tr><td>Select BAM File (optional):</td><td><select name=bam$nrs>";
$output .= "<option value='' selected>None</option>";
if (count($BAMs) > 0) {
    $output .= "<optgroup label='Root Folder'>";
}
foreach ($BAMs as $key => $value) {
    $output .= "<option value='$value'>$value</option>";
}
if (count($BAMs) > 0) {
    $output .= "</optgroup>";
}
foreach ($BAMdirs as $sdir => $options) {
    $output .= "<optgroup label='$sdir'>";
    $options = str_replace("$dir/", "", $options);
    $output .= $options;
    $output .= "</optgroup>";
}

$output .= "</select></td></tr>";
$output .= "<tr><td>Sample Gender:</td><td><select name=gender$nrs><option value='undef'>Not Specified</option><option value='Male'>Male</option><option value='Female'>Female</option></select></td></tr>";
$output .= "<tr><td>Store Datafiles:</td><td><input type=checkbox CHECKED name='storedata$nrs'></td></tr>";
$output .= "</table>";

echo $output;
