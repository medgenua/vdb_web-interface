<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

$userid = $_GET['uid'];
$qid = $_GET['qid'];
$slicestart = $_GET['slicestart'];

// saving results
if (strlen($slicestart) > 5 && substr($slicestart, 0, 5) == 'save|') {
    echo "<div class='w100'><h3>Query Results</h3>";
    // navigation
    echo "<h4>Actions</h4>";
    echo "<p style='padding-left:1em'>";
    echo "<input type=\"button\" onclick=\"RunQuery('0');\" value=\"Re-Run Query\" /> &nbsp; | &nbsp; ";
    echo "<input type=\"button\" onclick=\"SaveResults('$userid');\" value=\"Re-Run & Save Results\" /></p>";
    echo "<h4>Statistics and remarks</h4>";
    list($k, $set_id) = explode("|", $slicestart);
    if (file_exists("$scriptdir/Query_Results/$userid/$qid/stats")) {
        foreach (file("$scriptdir/Query_Results/$userid/$qid/stats") as $line) {
            # split with fallback.
            list($k, $v) = explode("=", $line, 2) + array(NULL, NULL);
            if ($v !== NULL) {
                $stats[$k] = $v;
            }
        }
        echo "<p>" . $stats["Passed"] . " out of " . $stats["Total"] . " variants passed the used filter settings.</p>";
    } else {
        echo "<p>Could not read stats file. This might indicate a problem. Please report.</p>";
        trigger_error("STATS file not found at $scriptdir/Query_Results/$userid/$qid/stats", E_USER_ERROR);
    }
    echo "<h4>Results</h4><p>Results were saved.</h4>";
    exit;
}
// export
if ($slicestart == 'all') {
    echo "<div class='w100'><h3>Query Results</h3>";
    // navigation
    echo "<h4>Actions</h4>";
    echo "<p style='padding-left:1em'>";
    echo "<input type=\"button\" onclick=\"RunQuery('all');\" value=\"Re-Run Query\" />  ";
    echo "<h4>Statistics and remarks</h4>";
    //slist($k, $set_id) = explode("|", $slicestart);
    if (file_exists("$scriptdir/Query_Results/$userid/$qid/stats")) {
        foreach (file("$scriptdir/Query_Results/$userid/$qid/stats") as $line) {
            # split with fallback.
            list($k, $v) = explode("=", $line, 2) + array(NULL, NULL);
            if ($v !== NULL) {
                $stats[$k] = $v;
            }
        }
        echo "<p>" . $stats["Passed"] . " out of " . $stats["Total"] . " variants passed the used filter settings.</p>";
    } else {
        echo "<p>Could not read stats file. This might indicate a problem. Please report.</p>";
        trigger_error("STATS file not found at $scriptdir/Query_Results/$userid/$qid/stats", E_USER_ERROR);
    }

    // logs
    echo "<h4>Query Logs</h4>";
    echo "<div id='QueryLog'>";
    echo "<span class='indent' onmouseover='' style='cursor: pointer;' onclick=\"LoadQueryLog('$scriptdir/Query_Results/$userid/$qid/stdout')\"> Load runtime details.</span>";
    echo "</div>";
    // results: 
    if (file_exists("$scriptdir/Query_Results/$userid/$qid/results.txt")) {
        $results_file = "$scriptdir/Query_Results/$userid/$qid/results.txt";
    } elseif (file_exists("$scriptdir/Query_Results/$userid/$qid/results.csv")) {
        $results_file = "$scriptdir/Query_Results/$userid/$qid/results.csv";
    } else {
        trigger_error("Results file not found: '$scriptdir/Query_Results/$userid/$qid/results.[csv|txt]", E_USER_ERROR);
        exit();
    }
    echo "<h4>Results</h4>";
    if (isset($_GET['format']) && $_GET['format'] == 'inline') {
        echo "<textarea style='width:100%;' rows=100>";
        $handle = fopen($results_file, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                // process the line read.
                echo $line;
            }

            fclose($handle);
        } else {
            trigger_error("Failed to open results file : $scriptdir/Query_Results/$userid/$qid/results.0", E_USER_ERROR);
        }
    } else {
        // as file for download.
        $results_url = str_replace("$scriptdir/", "", $results_file);
        echo "Save Results : <a href='$results_url' download>Click here</a></p>";
    }
    exit;
}

if (!is_numeric($userid) || !is_numeric($qid) || !is_numeric($slicestart)) {
    trigger_error("Invalid Parameters in Load_Query_Results : " . http_build_query($_GET, '', ','), E_USER_ERROR);
    echo "<div class='w100'><h3>Invalid request.</h3></div>";
    exit;
}

## check access of user to sample.
if (file_exists("$scriptdir/Query_Results/$userid/$qid/results.$slicestart")) {
    echo "<div class='w100'><h3>Query Results</h3>";
    // navigation
    echo "<h4>Actions</h4>";
    echo "<p style='padding-left:1em'>";
    echo "<input type=\"button\" onclick=\"RunQuery('0');\" value=\"Re-Run Query\" /> &nbsp; | &nbsp; ";
    echo "<input type=\"button\" onclick=\"SaveResults('$userid');\" value=\"Re-Run & Save Results\" /> &nbsp; | &nbsp; ";
    echo "Load Page : <select id=setPage onChange=\"ShowResults('$userid','$qid')\">";
    $i = 0;
    while (file_exists("$scriptdir/Query_Results/$userid/$qid/results.$i")) {
        $print_start = $i + 1;
        $print_end = $i + 100;
        if ($i == $slicestart) {
            $s = "SELECTED";
        } else {
            $s = "";
        }
        echo "<option value=$i $s>Result $print_start to $print_end</option>";
        $i = $i + 100;
    }
    echo "</select>";
    // stats 
    echo "<h4>Statistics and remarks</h4>";
    $stats = array();
    if (file_exists("$scriptdir/Query_Results/$userid/$qid/stats")) {
        foreach (file("$scriptdir/Query_Results/$userid/$qid/stats") as $line) {
            # split with fallback.
            list($k, $v) = explode("=", $line, 2) + array(NULL, NULL);
            if ($v !== NULL) {
                $stats[$k] = $v;
            }
        }
        echo "<p>" . $stats["Passed"] . " out of " . $stats["Total"] . " variants passed the used filter settings.</p>";
    } else {
        echo "<p> Could not read stats file. This might indicate a problem. Please report.</p>";
        trigger_error("STATS file not found at $scriptdir/Query_Results/$userid/$qid/stats", E_USER_ERROR);
    }
    // logs
    echo "<h4>Query Logs</h4>";
    echo "<div id='QueryLog'>";
    echo "<span class='indent' onmouseover='' style='cursor: pointer;' onclick=\"LoadQueryLog('$scriptdir/Query_Results/$userid/$qid/stdout')\"> Load runtime details.</span>";
    echo "</div>";

    // results
    include("$scriptdir/Query_Results/$userid/$qid/results.$slicestart");
    exit;
}
// results.slice not found : maybe no results present.
if (file_exists("$scriptdir/Query_Results/$userid/$qid/stats")) {
    foreach (file("$scriptdir/Query_Results/$userid/$qid/stats") as $line) {
        # split with fallback.
        list($k, $v) = explode("=", $line, 2) + array(NULL, NULL);
        if ($v !== NULL) {
            $stats[$k] = $v;
        }
    }
    if ($stats["Passed"] == 0) {
        echo "<div class='w100'><h3>Query Results</h3>";
        // navigation
        echo "<h4>Actions</h4>";
        echo "<p style='padding-left:1em'>";
        echo "<input type=\"button\" onclick=\"RunQuery('0');\" value=\"Re-Run Query\" /> &nbsp; | &nbsp; ";
        // access : allowed to save ? 

        echo "<input id='RunSave' type=\"button\" onclick=\"SaveResults('$userid');\" value=\"Re-Run & Save Results\" /> </p> ";
        // stats 
        echo "<h4>Statistics and remarks</h4>";
        echo "<p>" . $stats["Passed"] . " out of " . $stats["Total"] . " variants passed the used filter settings.</p>";
        // logs
        echo "<h4>Query Logs</h4>";
        echo "<div id='QueryLog'>";
        echo "<span class='indent' onmouseover='' style='cursor: pointer;' onclick=\"LoadQueryLog('$scriptdir/Query_Results/$userid/$qid/stdout')\"> Load runtime details.</span>";
        echo "</div>";
        echo "<h4>Results</h4>";
        echo "<p> No results.</p>";
        exit;
    }
}
# if here : not normal.   
trigger_error("Results not found Load_Query_Results : $scriptdir/Query_Results/$userid/$qid/result.$slicestart", E_USER_ERROR);
echo "<div class='w100'><h3>Query Results not found</h3>";
echo "</div>";

exit;
