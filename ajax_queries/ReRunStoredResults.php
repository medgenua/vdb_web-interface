<?php

#######################
# CONNECT TO DATABASE # ASC
#######################
include('../.LoadCredentials.php');

$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
require("../includes/inc_filter_functions.inc");
include('../includes/inc_logging.inc');

// load xml library
require_once('../xmlLib2.php');


$uid = $_SESSION['userID'];
$setid = $_GET['setid'];
$result = array('status' => 'ok');
if (!is_numeric($setid)) {
    $result['status'] = 'Error';
    $result['msg'] =  "Invalid Set ID";
    echo json_encode($result);
    exit;
}

if (!is_numeric($uid)) {
    echo "invalid userid : '$uid'";
    exit;
}

if (!isset($_GET['get'])) {
    echo "action not specified.";
    exit;
}

// read the filter config xml
$configuration = my_xml2array("../Filter/Filter_Options.xml");

// get filter rules
$rows = runQuery("SELECT `set_filters` AS 'SetRules', `set_annotations` AS 'Anno', `set_filtertree` AS 'tree',sid, uid FROM `Samples_x_Saved_Results` WHERE set_id = '$setid'", "Samples_x_Saved_Results");
if (count($rows) == 0) {
    $result['status'] = 'Error';
    $result['msg'] =  "Set ID not found.";
    echo json_encode($result);
    exit;
}
$row = $rows[0];
$jstree = $row['tree'];
$result['Anno'] = $row['Anno'];
// get the filter fields. 
$sid = $row['sid'];
$uid = $row['uid'];
$clear_cookies = '';
$clear_dyn = '';
$listed = 0;
$settings = array();
$cats = array();
foreach (explode("@@@", $row['SetRules']) as $set) {
    list($k, $v) = explode('|||', $set);
    $settings[$k] = $v;
    if (substr($k, 0, 8) == 'category') {
        array_push($cats, $k);
    }
}
// go through categories, and build the table rows.
foreach ($cats as $cat_idx) {
    $old_nr = substr($cat_idx, 8);
    $listed++;
    $new_nr = $listed;
    // clear old cookies.
    $clear_cookies .= "'$uid" . "_N$listed','$uid" . "_C$listed','$uid" . "_P$listed','$uid" . "_A$listed','$uid" . "_V$listed','$uid" . "_s$listed','$uid" . "\_nr\_do\_$listed',";
    $clear_dyn .= "`ckey` LIKE '$uid" . "\_do\_$listed" . "\_%' OR `ckey` LIKE '$uid" . "\_dyn$listed" . "\_%' OR ";
    // update jstree
    $jstree = str_replace("rule_" . $old_nr . '\"', "rule_" . $new_nr . 'tmpstr\"', $jstree);
    $jstree = str_replace("rule_" . $old_nr . '_', "rule_" . $new_nr . 'tmpstr_', $jstree);

    // get filter details.
    $category = $settings[$cat_idx];
    $params = $settings["param$old_nr"];
    $negate = $settings["negate$old_nr"];
    if (isset($settings["ssqv$old_nr"])) {
        $ssqv = $settings["ssqv$old_nr"];
    } else {
        $ssqv = "";
    }
    if ($category == 'Dynamic_Filters') {
        // get options, if any.
        $nr_do = $settings["nr_do_$old_nr"];
        $options = array("nr_do_$old_nr" => $nr_do);
        for ($i = 1; $i <= $nr_do; $i++) {
            //$options["do$old_nr"."_$i"."_name"] = $settings["do$old_nr"."_$i"."_name"];
            //$options["do$old_nr"."_$i"."_value"] = $settings["do$old_nr"."_$i"."_value"];
            // by name
            $options[$settings["do$old_nr" . "_$i" . "_name"]] = $settings["do$old_nr" . "_$i" . "_value"];
            trigger_error("added do " . $settings["do$old_nr" . "_$i" . "_name"] . " : " . $settings["do$old_nr" . "_$i" . "_value"]);
        }
        // get info.
        $dyn = GetDynamic($category, $params, $options, $new_nr);
        // add to results.
        $result['rules'][$new_nr] = array('c' => $category, 'v' => array($negate, $dyn['first'], $dyn['second'], ''));
        continue;
    }
    $arg = $settings["argument$old_nr"];
    $values = (isset($settings["value$old_nr"])) ? $settings["value$old_nr"] : '';
    // first level
    $first_level = GetSets($category, $params, $new_nr);
    // first level subselect ? 
    $first_level['result'] .= "<span id='ss$new_nr'>" . GetSubSelect($category, $params, $new_nr, $ssqv)['result'] . "</span>";
    // second level
    $second_level = GetArguments($category, $params, $arg, $values, $new_nr);
    // third level.
    $third_level = GetValues($category, $params, $values, $new_nr);
    $result['rules'][$new_nr] = array('c' => $category, 'v' => array($negate, $first_level, $second_level, $third_level));
}
$result['listed'] = $listed;
$jstree = str_replace("tmpstr", "", $jstree);

// depending on the source, we need to strip extra slashes
if (isset($_GET['strip']) && $_GET['strip'] == 1) {
    $result['jstree'] = stripslashes($jstree);
} else {
    $result['jstree'] = $jstree; // stripslashes($jstree);
}

echo (json_encode($result));
//echo stripslashes($row[$get]);
exit;
