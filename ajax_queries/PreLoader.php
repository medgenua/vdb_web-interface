<?php
//#######################
//# READ CREDS #
//#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');

// batch uses post.
if (isset($_POST['sid']) && isset($_POST['uid']) && isset($_POST['data'])) {
	if ($_POST['data'] == '') {
		// empty submission.
		exit;
	}
	if (!isset($_POST['sid']) || !is_numeric($_POST['sid'])) {
		exit;
	}
	$sid = $_POST['sid'];
	if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
		exit;
	}
	$uid = $_POST['uid'];
	// entries
	$entries = explode('@@@',substr($_POST['data'],0,-3));
	$out = '';
	foreach($entries as $entry) {
		list($cat,$param) = explode('@@',$entry);
		if ($cat != '' && $param != '') {
			$out .= "$sid;$uid;$cat;$param\n";
		}
	}	
	if ($out != '') {
		$lock = fopen("../Query_Results/.preloader.lock",'a');
		if (flock($lock,LOCK_EX))  {
			if (!file_exists("../Query_Results/.preloader.queue")) {
				touch("../Query_Results/.preloader.queue");
				system("chmod 777 '../Query_Results/.preloader.queue'");
			}
			$file = fopen("../Query_Results/.preloader.queue",'a');
		  	fwrite($file,$out);
			fclose($file);
		  	// release lock
		  	flock($lock,LOCK_UN);
		}else{
		  	echo "Error locking file!";
		}
		fclose($lock);
	}


}
else {
	// continue with single submission.
	if (!isset($_GET['sid']) || !is_numeric($_GET['sid'])) {
		exit;
	}
	$sid = $_GET['sid'];
	
	if (!isset($_GET['c']) || $_GET['c'] == '') {
		error_log("category not set\n");
	
		exit;
	}
	$category = $_GET['c'];


	if (!isset($_GET['p']) || $_GET['p'] == '') {
		error_log("'p' not set for $category");
		exit;
	}
	$parameter = $_GET['p'];
	
	if (!isset($_GET['uid']) || !is_numeric($_GET['uid'])) {
		exit;
	}
	$uid = $_GET['uid'];
	
	$lock = fopen("../Query_Results/.preloader.lock",'a');
	if (flock($lock,LOCK_EX))  {
		if (!file_exists("../Query_Results/.preloader.queue")) {
			touch("../Query_Results/.preloader.queue");
			system("chmod 777 '../Query_Results/.preloader.queue'");
		}
		$file = fopen("../Query_Results/.preloader.queue",'a');
	  	fwrite($file,"$sid;$uid;$category;$parameter\n");
		fclose($file);
	  	// release lock
	  	flock($lock,LOCK_UN);
	}else{
	  	echo "Error locking file!";
	}

	fclose($lock);
}
// launch preloader if not running.
$running = 0;
$pid_file = "$scriptdir/Query_Logs/VariantDB_PreLoader.pid";
if (file_exists($pid_file)) {
	$pids = explode("\n",file_get_contents($pid_file));
	foreach($pids as $pid) {
		$pid = rtrim($pid);
		if (file_exists("/proc/$pid")) {
			$running = 1;
		}
	}
}
if ($running == 0) {
	$log_file = "$scriptdir/Query_Logs/VariantDB_PreLoader.log";
	$command = "(echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd $scriptdir/cgi-bin && ./Service_Launcher.sh '$scriptdir/cgi-bin' 'VariantDB_PreLoader.pl' '$log_file'\" ) 2>/dev/null";
        //$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/cgi-bin/ && ./Launch_VDBPL.sh\") 2>/dev/null";
        $out = shell_exec($command);
}
