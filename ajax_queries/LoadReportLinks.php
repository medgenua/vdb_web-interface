<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');

$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_GET['uid'];
if (!is_numeric($userid)) {
    $return = array("ERROR" => 'invalid userid provided');
    // print as json econded string. 
    echo json_encode($return);
    exit;
}
$qids = $_GET['qids']; // explode(",", $_GET['qids']);
// check qids for validity
foreach ($qids as $idx => $qid) {
    if (!is_numeric($qid)) {
        $return = array("ERROR" => 'invalid query_id provided');
        // print as json econded string. 
        echo json_encode($return);

        exit;
    }
    // access ?
    $quid = file_get_contents("$scriptdir/api/query_results/$qid/uid");
    $quid = rtrim($quid);
    if ($quid != $userid) {
        $return = array("ERROR" => "provided userID does not match the stored userid for report $qid");
        // print as json econded string. 
        echo json_encode($return);
        exit;
    }
}

require("../includes/inc_query_functions.inc");

## check access of user to sample.
$alldone = 1;
$links = '';
foreach ($qids as $idx => $qid) {
    $status = file_get_contents("$scriptdir/api/query_results/$qid/status");
    $status = rtrim($status);
    if (file_exists("$scriptdir/api/query_results/$qid/Report.pdf")) {
        // get the sampleID
        $fh = fopen("$scriptdir/api/query_results/$qid/form", 'r');
        $sid = -1;
        while (!feof($fh)) {
            $line = fgets($fh);
            $line = rtrim($line);
            $p = explode("=", $line);
            if ($p[0] == 'sid') {
                $sid = $p[1];
                break;
            }
        }
        fclose($fh);
        $row = runQuery("SELECT Name FROM `Samples` WHERE id = '$sid'", "Samples")[0];
        $sname = $row['Name'];
        $links .= "<li><a href='api/query_results/$qid/Report.pdf' target='_blank'> $sname</a></li>";
    } elseif ($status == 'Error') {
        // get the sampleID
        $fh = fopen("$scriptdir/api/query_results/$qid/form", 'r');
        $sid = -1;
        while (!feof($fh)) {
            $line = fgets($fh);
            $line = rtrim($line);
            $p = explode("=", $line);
            if ($p[0] == 'sid') {
                $sid = $p[1];
                break;
            }
        }
        fclose($fh);
        $row = runQuery("SELECT Name FROM `Samples` WHERE id = '$sid'", "Sample")[0];
        $sname = $row['Name'];
        $message = file_get_contents("$scriptdir/api/query_results/$qid/stderr");
        $message = addslashes($message);
        $links .= "<li title=\"$message\">$sname : Reporting Failed</li>";
    } else {
        $alldone = 0;
    }
}
$return = array();
$return['links'] = '';
if ($links == '') {
    $return['links'] .= "<li style='list-style-type:none'>No finished reports yet.";
} else {
    $return['links'] .= "$links";
}
if ($alldone == 1) {
    $return['status'] = 'Finished';
} else {
    $return['status'] = 'Running';
}
echo json_encode($return);
