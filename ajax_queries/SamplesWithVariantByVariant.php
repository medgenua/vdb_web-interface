<?php


//#######################
//# CONNECT TO DATABASE #
//#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// parameters
$pos = (isset($_POST['pos']) ? $_POST['pos'] : 0);
$ref = (isset($_POST['ref']) ? $_POST['ref'] : 0);
$alt = (isset($_POST['alt']) ? $_POST['alt'] : 0);
$uid = $_SESSION['userID'];

if (!is_numeric($uid) || !$uid) {
    // invalid userid
    echo "ERROR : invalid userid. Log in first !";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
    exit;
}

// code mappings
$inh = array(0 => 'ND', 1 => 'P', 2 =>  'M', 3 => 'DN', 4 => 'Both', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => '-', 'Both' => 'Both Parents');
//%inhm = (0,'-',1,'Dominant',2,'Recessive','3','Unknown');
$classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');
$chr_hash = range(0, 22);
array_push($chr_hash, "X", "Y", "MT");

trigger_error("      ");
trigger_error($pos);
$pos_parts = explode(":", $pos);
$chr = $pos_parts[0];
$position = $pos_parts[1];

trigger_error($chr);
$chr = str_replace("chr", "", $chr);
if ($chr == "X") {
    $chr = 23;
} elseif ($chr == "Y") {
    $chr = 24;
} elseif (substr($chr, 0, 1) == "M") {
    $chr = 25;
}


$vid_info = array("chr" => $chr, "start" => $position, "RefAllele" => $ref, "AltAllele" => $alt);

# vid
$vid_info = array_shift(...[runQuery("SELECT id FROM `Variants` WHERE chr = '$chr' AND start = '$position' AND RefAllele = '$ref' AND AltAllele = '$alt'")]);
$vid = $vid_info["id"];
trigger_error("variant ID : $vid");
// get variant 
$vid_info = array_shift(...[runQuery("SELECT v.chr, v.start, v.RefAllele, v.AltAllele FROM `Variants` v WHERE id = '$vid'", "Variants")]);
if (count($vid_info) == 0) {
    echo "ERROR : variant not found.";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";

    exit;
}
$variant = "chr" . $chr_hash[$vid_info['chr']] . ":" . number_format($vid_info['start']) . " : " . $vid_info['RefAllele'] . "/" . $vid_info['AltAllele'];


///////////////////
// SUMMARY STATS //
///////////////////
// stats up to date ? 
$sStatus = runQuery("SELECT `SummaryStatus` FROM `Users` WHERE id = '$uid'", "Users")[0];
// get stats
$row = runQuery("SELECT * FROM `Variants_x_Users_Summary` WHERE vid = '$vid' AND uid = '$uid'")[0];
$summary_table =  "<p ><table id='summary_table' align=center cellspacing=0 class=w95 style='margin-top:2em;'><thead><tr><th class=top>Sample Category</th><th class=top>Homozygous Reference</th><th class=top>Heterozygous</th><th class=top>Homozygous Alternate</th><th class=top style='border-left:3px double #fff'>Inheritance</th><th class=top >#Samples</th><th class=top style='border-left:3px double #fff'>Classification</th><th class=top >#Samples</th><th class=top>Classification</th><th class=top >#Samples</th></tr></thead>";
// Overall / denovo / pathogenic & uvKL4.
$summary_table .= "<tr><td>All Samples</td><td>" . $row['AllHomRef'] . "</td><td>" . $row['AllHet'] . "</td><td>" . $row['AllHomAlt'] . "</td><td style='border-left:3px double #fff'>De Novo</td><td>" . $row['nrDeNovo'] . "</td><td style='border-left:3px double #fff'>Pathogenic</td><td>" . $row['nrPatho'] . "</td><td >UVKL4</td><td>" . $row['nrUVKL4'] . "</td></tr>";
// Controls.
$summary_table .= "<tr><td>Control Samples</td><td>" . $row['ConHomRef'] . "</td><td>" . $row['ConHet'] . "</td><td>" . $row['ConHomAlt'] . "</td><td style='border-left:3px double #fff'>Maternal</td><td>" . $row['nrMaternal'] . "</td><td style='border-left:3px double #fff'>UVKL3</td><td>" . $row['nrUVKL3'] . "</td><td >UVKL2</td><td>" . $row['nrUVKL2'] . "</td></tr>";
// Females.
$summary_table .= "<tr><td>Female Samples</td><td>" . $row['FemaleHomRef'] . "</td><td>" . $row['FemaleHet'] . "</td><td>" . $row['FemaleHomAlt'] . "</td><td style='border-left:3px double #fff'>Paternal</td><td>" . $row['nrPaternal'] . "</td><td style='border-left:3px double #fff'>Benign</td><td>" . $row['nrBenign'] . "</td><td >False Positive</td><td>" . $row['nrFalsePositive'] . "</td></tr>";
// Males.
$summary_table .= "<tr><td>Male Samples</td><td>" . $row['MaleHomRef'] . "</td><td>" . $row['MaleHet'] . "</td><td>" . $row['MaleHomAlt'] . "</td><td style='border-left:3px double #fff'>BiParental</td><td>" . $row['nrBiParental'] . "</td><td style='border-left:3px double #fff'>Dominant Model</td><td>" . $row['nrDominant'] . "</td><td >Recessive Model</td><td>" . $row['nrRecessive'] . "</td></tr>";

$summary_table .= "<tr><td colspan=10 class=last>&nbsp;</td></tr></table></p>";


//////////////////////////
// DETAILS PER GENOTYPE //
//////////////////////////
// get samples
$sids = runQuery("SELECT p.name AS pname, p.id AS pid, s.id AS sid, s.gender , s.Name AS sname FROM `Samples` s JOIN  `Projects_x_Samples` ps JOIN `Projects_x_Users` pu JOIN `Projects` p ON pu.pid = ps.pid AND ps.sid = s.id AND p.id = ps.pid WHERE pu.uid = $uid ", "Samples:Projects_x_Samples:Projects_x_Users:Projects");
if (count($sids) == 0) {
    $output = "No samples available<br/>";
    echo "$output";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";

    exit;
}
$samples = array();
foreach ($sids as $k => $row) {
    $samples[$row['sid']] = array(
        'sn' => $row['sname'],
        'pn' => $row['pname'],
        'pid' => $row['pid'],
        'gender' => $row['gender']
    );
}
$insid = implode(",", array_keys($samples));
// in batches of 100 samples.
$from = 0;
$size = 100;
$table = array(
    0 => array(),
    1 => array(),
    2 => array()
);

while ($from <= count(array_keys($samples))) {
    $insid = implode(",", array_keys(array_slice($samples, $from, $size, TRUE)));
    $from += $size;
    // get genotypes.,
    $gts = runQuery("SELECT sid, inheritance,InheritanceMode,class,Filter,AltCount,RefDepth,AltDepth,DeltaPL,StretchLengthA,StretchLengthB FROM `Variants_x_Samples` WHERE vid = '$vid' AND sid IN ($insid)", "Variants_x_Samples");
    // process
    foreach ($gts as $k => $sr) {
        // sample
        $out = "<tr><td>" . $samples[$sr['sid']]['sn'] . "</td>";
        // project
        $out .= "<td>" . $samples[$sr['sid']]['pn'] . "</td>";
        // gender
        $out .= "<td>" . $samples[$sr['sid']]['gender'] . "</td>";
        // inheritance
        $out .= "<td>" . $inh[$inh[$sr['inheritance']]] . "</td>";
        // class
        $out .= "<td>" . $classes[$sr['class']] . "</td>";
        // depth
        $out .= "<td>" . $sr['RefDepth'] . "/" . $sr['AltDepth'] . "</td>";
        // filter
        $out .= "<td>" . $sr['Filter'] . "</td>";
        //DeltaPL
        $out .= "<td>" . $sr['DeltaPL'] . "</td>";
        //stretch
        if ($sr['StretchLengthA'] + $sr['StretchLengthB'] > 0) {
            $out .= "<td>" . $sr['StretchLengthA'] . "/" . $sr['StretchLengthB'] . "</td>";
        } else {
            $out .= "<td>-</td>";
        }
        $out .= "</tr>";
        array_push($table[$sr['AltCount']], $out);
    }
}
/////////////////////////
// TOP SELECTION BOXES //
/////////////////////////
echo "<div style='height:90vh;overflow-y:auto;'>";
// Three tabs, one per genotype. 
echo "<h3>Occurence overview of $variant</h3>";
// warning if summary stats are out of date
if ($sStatus['SummaryStatus'] != 2) {
    echo "<p><span class='emph red'>WARNING: Summary Statistics are not up-to-date</span>";
    //if running : do not show update action
    if ($sStatus['SummaryStatus'] != 1) {
        # not running. Queued ? 
        $squeue = explode("\n", `cat ../Query_Results/.UserSummary.queue`);
        if (!in_array($uid, $squeue)) {
            echo " <span id='updateUserSummary' class=strong onmouseover='' style='cursor: pointer;' OnClick=\"UpdateSummary('User','$uid')\">Update now.</span>";
        } else {
            # queued : inform user
            echo "<span id='italic'> (Queued for update)</span>";
        }
    } else {
        echo "<span id='italic'> (Update Running)";
    }
    echo "</p>";
}

echo "<p><table align=center class=w95><tr><th onmouseover=\"this.style.cursor='pointer'\" class='overlay_big headsel' id=head_summary onClick=\"ToggleSamplesWithVariant('Summary')\">Summary</th><th onmouseover=\"this.style.cursor='pointer'\" class=overlay_big id=head_HomRef onClick=\"ToggleSamplesWithVariant('HomRef')\">Homozygous Reference (" . count($table[0]) . ")</th>";
echo "<th onmouseover=\"this.style.cursor='pointer'\" id=head_Het class='overlay_big' onClick=\"ToggleSamplesWithVariant('Het')\">Heterozygous (" . count($table[1]) . ")</th><th onmouseover=\"this.style.cursor='pointer'\" class=overlay_big id=head_HomAlt onClick=\"ToggleSamplesWithVariant('HomAlt')\">Homozygous Alternate (" . count($table[2]) . ")</th></tr></table>";
echo "</p>";

// print summary table.
echo $summary_table;

// print datails tables.
echo  "<p ><table id='details_table' style='display:none;' align=center cellspacing=0 class=w95><thead><tr><th class=top>Sample</th><th class=top>Project</th><th class=top>Gender</th><th class=top>Inheritance</th><th class=top>D.Class</th><th class=top>Ref/Alt.Depth</th><th class=top>Filter</th><th class=top>DeltaPL</th><th class=top>Stretch</th></tr></thead>";
// homref
if (count($table[0]) == 0) {
    $table[0][] = "<tr><td colspan=9 align=left ><span class=italic>&nbsp; &nbsp;No samples found</span></td></tr>";
}
echo "<tbody id='tbody_HomRef' style='display:none'>" . join("", $table[0]) . "</tbody>";
// het 
if (count($table[1]) == 0) {
    $table[1][] = "<tr><td colspan=9 align=left ><span class=italic>&nbsp; &nbsp;No samples found</span></td></tr>";
}
echo "<tbody id='tbody_Het'  style='display:none'>" . join("", $table[1]) . "</tbody>";
// hom alt
if (count($table[2]) == 0) {
    $table[2][] = "<tr><td colspan=9 align=left ><span class=italic>&nbsp; &nbsp;No samples found</span></td></tr>";
}
echo "<tbody id='tbody_HomAlt' style='display:none'>" . join("", $table[2]) . "</tbody>";

echo "<tr><td class=last colspan=9>&nbsp;</td></tr></table></p>";
echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
echo "</div>";
