<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

## GET MAIN POSTED VARIABLES
$uid = $_POST['uid'];
$SaveAs = addslashes($_POST['SaveAs']);
$Comments = addslashes($_POST['SaveComments']);
$sid = $_POST['sid'];
$cbs = $_POST['cb'];
## unset them in the POST array
unset($_POST['uid']);

unset($_POST['SaveAs']);
unset($_POST['SaveComments']);
unset($_POST['sid']);
unset($_POST['cb']);
# unset some non-needed items:
unset($_POST['build']);
unset($_POST['session_path']);
unset($_POST['kill']);
unset($_POST['region']);
unset($_POST['project']);
unset($_POST['nranno']);




## Store the rest of the variables in a string, seperated by '@@@';
$settings = '';
$filtertree = '';
$annotations = array();
foreach ($_POST as $key => $value) {
    if ($key == 'logic') {
        $filtertree = addslashes($value);
    } elseif (substr($key, 0, 4) == 'anno') {
        array_push($annotations, $value);
    } else {
        $settings .= "$key|||$value@@@";
    }
}
if ($settings != '') {
    $settings = substr($settings, 0, -3);
}
$annotations = implode("@@@", $annotations);
$settings = addslashes($settings);
$filtertree = addslashes($filtertree);
## store
$set_id = insertQuery("INSERT INTO `Samples_x_Saved_Results` (sid, uid, set_name, set_comments, set_filters, set_filtertree,set_annotations,set_vids,cbs) VALUES ('$sid','$uid','$SaveAs','$Comments','$settings','$filtertree','$annotations','Queued','$cbs')", "Samples_x_Saved_Results");
echo "$set_id";
