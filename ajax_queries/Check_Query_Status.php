<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');

//$db = "NGS-Variants".$_SESSION['dbname'];
$userid = $_GET['uid'];
$qid = $_GET['qid'];
if (!is_numeric($userid) || !is_numeric($qid)) {
    exit;
}

# if error file is present & not empty => stop analysis.
if (file_exists("$scriptdir/Query_Results/$userid/$qid/stderr") && filesize("$scriptdir/Query_Results/$userid/$qid/stderr") > 0) {
    echo json_encode(array('status' => 'ERROR'));
    system('ps aux | grep UI_' . $qid . ' | grep -v grep | awk \'{print $2}\' |xargs kill >/dev/null 2>&1');
    exit;
}
if (file_exists("$scriptdir/Query_Results/$userid/$qid/status")) {
    $status = file_get_contents("$scriptdir/Query_Results/$userid/$qid/status");
    echo json_encode(array('status' => $status));
} else {
    echo json_encode(array('status' => 'ERROR'));
}
exit;
