<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// get samples


//vars
$uid = $_POST['u'];
$gid = $_POST['q'];
$show_actions = (isset($_POST['a'])) ? $_POST['a'] : 1;
// get usergroup information
$row = runQuery("SELECT description, affiliation, administrator, editvariant, editclinic, opengroup, name FROM `Usergroups` WHERE id = '$gid'", "Usergroups")[0];
$name = $row['name'];
$descr = $row['description'];
$allowedaffi = $row['affiliation'];
$admin = $row['administrator'];
$editvariant = $row['editvariant'];
$editclinic = $row['editclinic'];
$opengroup = $row['opengroup'];
if ($editvariant == 1 && $editclinic == 1) {
    $permission = 'Edit Variants and Clinical information';
} elseif ($editcnv == 1) {
    $permission = 'Edit Variant information';
} elseif ($editclinic == 1) {
    $permission = 'Edit Clinical information';
} else {
    $permission = 'Read Only Access';
}

// get affiliations information
if ($opengroup != 1) {

    $rows = runQuery("SELECT id,name FROM `Affiliation` WHERE id IN ($allowedaffi) ORDER BY name", "Affiliation");
    $affis = array();
    foreach ($rows as $row) {
        $affis[$row['id']] = $row['name'];
    }
}

// if show_actions == 0, include a header
if ($show_actions == 0) {
    echo "<span class=emph>Group Information: $name</span><br/>";
}

// get admin information
$row = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '$admin'", "Users")[0];
$adminname = $row['LastName'] . " " . $row['FirstName'];

echo "<ul class=clear style='margin-left:10px'>\n";
echo "<li><span class=italic>- Description:</span> $descr</li>\n";
echo "<li><span class=italic>- Administrator:</span> $adminname</li>";
echo "<li><span class=italic>- Permissions:</span> $permission</li>";
echo "<li><span class=italic>- Allowed Institutions:</span> ";
if ($opengroup == 1) {
    echo " All</li>";
} else {
    echo "<ul class=clear style='margin-left:15px'>";
    foreach ($affis as $id => $name) {
        echo "<li>- $name</li>\n";
    }
    echo "</ul>\n";
    echo "</li>\n";
}
echo "<li><span class=italic>- Projects:</span><ul class=clear style='margin-left:15px'>";
// get projects shared with this group:
$rows = runQuery("SELECT p.id, p.Name,COUNT(ps.sid) AS nrSamples FROM `Projects` p JOIN `Projects_x_Usergroups` pu JOIN `Projects_x_Samples` ps ON ps.pid = p.id AND p.id = pu.pid WHERE pu.gid = '$gid'", "Projects:Projects_x_Usergroups");
if (count($rows) == 0) {
    echo "<li>None</li>\n";
} else {
    foreach ($rows as $row) {
        $pname = $row['Name'];
        $pid = $row['id'];
        $nrS = $row['nrSamples'];
        echo "<li>- $pname : $nrS samples </li>\n";
    }
}
echo "</ul>\n";
echo "</li>\n";
if ($show_actions == 1) {
    echo "<li> => <a href='index.php?page=group&type=mine&action=leave&gid=$gid' class=italic>Leave This Group</li>\n";
}
echo "</ul>\n";
