<?php
//#######################
//# CONNECT TO DATABASE #
//#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_SESSION['userID'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');



if ($userid == '') {
    echo json_encode(['status' => 'error', 'error' => 'no user provided']);
    exit;
}

// check access to sample: must have write access.
$sid = $_POST['sid'];
$r = runQuery("SELECT pu.editsample FROM Projects_x_Users pu JOIN Projects_x_Samples ps ON ps.pid = pu.pid WHERE ps.sid = '$sid' AND pu.uid = '$userid'", "Projects_x_Users;Projects_x_Samples");
if ($r[0]['editsample'] == 0) {
    echo json_encode(['status' => 'error', 'error' => 'Insufficient access to sample']);
    exit;
}


// private
$rows = runQuery("SELECT rsc.Title,rsc.cid,rsc.Options,rsc.Public,rsc.`Default` FROM `Report_Section_CheckBox` rsc WHERE rsc.uid = '$userid'", "Report_Section_CheckBox");
$done = array();
$out = array();
foreach ($rows as $row) {
    // skip seen.
    if (isset($done[$row['cid']])) {
        continue;
    }
    $selected = ($row['Default'] == 1) ? 'checked' : '';
    $done[$row['cid']] = 1;
    $out[$row['Title']] = "<input type=checkbox name='cb' value='" . $row['cid'] . "' $selected /> " . $row['Title'] . "&nbsp; "; //<br/>";
}

$rows = runQuery("SELECT ursc.edit, ursc.full,rsc.Title,rsc.cid,rsc.Options,rsc.Public,rsc.`Default` FROM `Report_Section_CheckBox` rsc JOIN `Users_x_Report_Sections_CheckBox` ursc ON rsc.cid = ursc.cid WHERE ursc.uid = '$userid'", "Report_Section_CheckBox:Users_x_Report_Sections_Checkbox");
foreach ($rows as $row) {
    // skip seen.
    if (isset($done[$row['cid']])) {
        continue;
    }
    $selected = ($row['Default'] == 1) ? 'checked' : '';
    $done[$row['cid']] = 1;
    $out[$row['Title']] = "<input type=checkbox name='cb' value='" . $row['cid'] . "' $selected /> " . $row['Title'] . "&nbsp; "; //"<br/>";
}
// public
$rows = runQuery("SELECT rsc.Title,rsc.cid,rsc.Options,rsc.Public,rsc.`Default` FROM `Report_Section_CheckBox` rsc WHERE rsc.Public = '1'", "Report_Section_CheckBox");
foreach ($rows as $row) {
    // skip seen.
    if (isset($done[$row['cid']])) {
        continue;
    }
    $selected = ($row['Default'] == 1) ? 'checked' : '';
    $done[$row['cid']] = 1;
    $out[$row['Title']] = "<input type=checkbox name='cb' value='" . $row['cid'] . "' $selected /> " . $row['Title'] . "&nbsp; "; //"<br/>";
}

// sort.
ksort($out);
if (count($out) == 0) {
    array_push($out, "No checkbox lists available. Create them <a href='index.php?page=report&t=checkboxes&cid=new'>here</a>");
}
# check frequency status of the user.
$row = runQuery("SELECT SummaryStatus FROM Users WHERE id = '$userid'")[0];
$stats = [0 => 'Pending', 1 => "Running", 2 => 'up-to-date'];
$freq_status = $stats[$row['SummaryStatus']];


echo json_encode(['status' => 'ok', 'checkboxes' => join("", $out), 'frequencies' => $freq_status]);
//foreach ($out as $k => $v) {
//    echo $v;
//}
