<?php
#######################
# READ CREDS #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// load xml library
require_once('../xmlLib2.php');

// DEBUG :
$return['status'] = 'OK';
$return['finished'] = 0; //$nr_finished;
echo json_encode($return);
exit();
// preloader is not used anymore.
if (!isset($_POST['sid']) || !is_numeric($_POST['sid'])) {
    exit;
}
$sid = $_POST['sid'];

if (!isset($_POST['uid']) || !is_numeric($_POST['uid'])) {
    exit;
}
$uid = $_POST['uid'];


$idx = 1;
$nr_finished = 0;
$return = array();
while (array_key_exists("item$idx", $_POST)) {
    list($cat, $param) = explode("|", $_POST["item$idx"]);
    $tables = GetTable($cat, $param, $uid, $sid);
    if ($tables == '') {
        #error_log("Skipping $cat:$param");
        $nr_finished++;
        $idx++;
        continue;
    }
    $tables = explode("|", $tables);
    $finished = 0;
    foreach ($tables as $name) {
        $row = array_shift(...[runQuery("SELECT `state` FROM `VIEWS` WHERE `name` = '$name'", "VIEWS")]);
        if (count($row) > 0) {

            $finished += $row['state'];
        }
    }
    if ($finished == count($tables)) {
        $nr_finished++;
    }
    $idx++;
}
$return['status'] = 'OK';
$return['finished'] = $nr_finished;
echo json_encode($return);

function GetTable($cat, $param, $uid, $sid)
{
    // dynamic is a static map. Update if new dynamic filters are created !
    $table = '';
    if ($cat == 'Dynamic_Filters') {
        $map = array(
            'De_Novo' => 'dyn_dn_Variants_x_Samples',
            'Dominant' => 'dyn_fam_Variants_x_Samples', // whole family, based on sid
            'Recessive___general__' => 'dyn_fam_Variants_x_Samples', // whole family, based on sid
            'Recessive___biparental__' => 'dyn_fam_Variants_x_Samples', // whole family, based on sid.
            'Recessive___Compound_Heterozygous__' => 'dyn_comhet_Variants_x_Samples|Variants_x_ANNOVAR_ncbigene', // trio only.
            'Pathogenic_Prediction' => 'Variants_x_ANNOVAR_ljb26_all|Variants_x_ANNOVAR_dbnsfp30a|Variants_x_ANNOVAR_CADD|Variants_x_SIFT'
        );
        $table = "VIEW_$uid" . "_$sid" . "_" . $map[$param];
        $table = str_replace("|", "|VIEW_$uid" . "_$sid" . "_", $table);
        return ($table);
    }
    if ($cat == 'Variants') {
        $table = "VIEW_$uid" . "_$sid" . "_" . 'Variants';
        return ($table);
    }
    $configuration = my_xml2array("../Filter/Filter_Options.xml");
    $from = get_value_by_path($configuration, "FilterConfiguration/$cat/$param/settings/from");
    // new summary tables
    #error_log($from['value']);
    if (preg_match("/Users_Summary/", $from['value']) || preg_match("/Projects_Summary/", $from['value'])) {
        $table = "VIEW_$uid" . "_" . $from['value'];
        #error_log($table);
        return ($table);
    }
    // other occurence queries
    if ($cat == 'Occurence' && preg_match("/Control_Samples/", $param)) {
        $table = "VIEW_$uid" . "_" . "controls_Variants_x_Samples";
        return ($table);
    }
    if ($cat == 'Occurence' && (preg_match("/By_Project/", $param) || preg_match("/All_Samples/", $param))) {
        $table = "VIEW_$uid" . "_" . 'all_samples_Variants_x_Samples';
        return ($table);
    }
    // skip occurence filters in samples.
    if (preg_match("/All_Samples/", $param) || preg_match("/By_Project/", $param) || preg_match("/In.*Samples.*/", $param) ||  preg_match("/.*Occ.*Samples.*/", $param)) {
        return ('');
    }

    // skip match_afterwards.
    $ma = get_value_by_path($configuration, "FilterConfiguration/$cat/$param/settings/Match_Afterwards");
    if ($ma['value'] != '') {
        return ('');
    }

    $from = explode(",", $from['value']);
    // skip multitable queries for now.
    if (count($from) > 1) {
        return ('');
    }
    $table = explode(" ", $from[0]);
    if ($cat == 'Family') {
        $table[0] = "$param" . "_" . $table[0];
    }

    return ("VIEW_$uid" . "_" . $sid . "_" . $table[0]);
}
