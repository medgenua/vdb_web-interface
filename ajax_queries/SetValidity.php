<?php
// variables
$action = $_POST['action'];
$sid = $_POST['sid'];
$vid = $_POST['vid'];
$uid = $_POST['uid'];

// check for illegal values 
if (!is_numeric($sid) || !is_numeric($vid) || !is_numeric($uid)) {
    echo "ILLEGAL VARIABLES PROVIDED. <br/>";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
    exit();
}
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// 0. check permission. 
$rows = runQuery("SELECT ps.pid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid Where ps.sid = '$sid' AND pu.uid = '$uid'", "Projects_x_Samples:Projects_x_Users");
if (count($rows) == 0) {
    echo "<h3>Permission denied</h3>";
    echo "<p>You do not have access to this sample.</p>";
    echo "<p><input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    exit();
}

// 1. GET THE FORM
if ($action == 'getform') {
    // get class, inheritance, validation, and details.
    $vs = runQuery("SELECT inheritance, InheritanceMode, class, validation, validationdetails FROM `Variants_x_Samples` WHERE vid = '$vid' AND sid = '$sid'", "Variants_x_Samples")[0];
    // get variant details
    $v = runQuery("SELECT chr, start, RefAllele, AltAllele FROM `Variants` WHERE id = '$vid'", "Variants")[0];
    // output	
    echo "<h3>Edit Significance and Validity</h3>";
    echo "<p><table align=center><tr><td align=right class=emph>Variant:</td><td align=left>chr" . $v['chr'] . ":" . $v['start'] . " : " . $v['RefAllele'] . "/" . $v['AltAllele'] . "</td></tr>";
    // class
    echo "<tr><td align=right class=emph>Diagnostic Class:</td><td align=left>";
    $classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');

    if ($vs['class'] == '') {
        $vs['class'] = 0;
    }
    echo "<select id='VariantClass' name='class'>";
    for ($i = 0; $i <= 6; $i++) {
        if ($vs['class'] == $i) {
            echo "<option value=$i SELECTED>" . $classes[$i] . "</option>";
        } else {
            echo "<option value=$i>" . $classes[$i] . "</option>";
        }
    }
    echo "</select></td></tr>";
    // Inheritance Mode
    echo "<tr><td align=right class=emph title='For functional variants, specify the (expected) inheritance model'>Inheritance Mode:</td><td align=left>";
    $inhm = array('0' => '-', '1' => 'Dominant', '2' => 'Recessive', '3', 'Unknown');
    if ($vs['InheritanceMode'] == '') {
        $vs['InheritanceMode'] = 0;
    }
    echo "<select id='VariantInhMode' name='inhm'>";
    for ($i = 0; $i <= 2; $i++) {
        if ($vs['InheritanceMode'] == $i) {
            echo "<option value=$i SELECTED>" . $inhm[$i] . "</option>";
        } else {
            echo "<option value=$i>" . $inhm[$i] . "</option>";
        }
    }
    echo "</select></td></tr>";

    // inheritance
    echo "<tr><td align=right class=emph>Inheritance</td><td align=left>";
    echo "<select id='VariantInh' name='inh'>";
    $inheritances = array('0' => '-', '1' => 'Paternal', '2' => 'Maternal', '3' => 'De Novo', '4' => 'Both Parents');
    for ($i = 0; $i <= 4; $i++) {
        if ($vs['inheritance'] == $i) {
            echo "<option value=$i SELECTED>" . $inheritances[$i] . "</option>";
        } else {
            echo "<option value=$i>" . $inheritances[$i] . "</option>";
        }
    }
    echo "</select></td></tr>";
    // validation
    echo "<tr title='Send requests for additional methods to the system admin'><td align=right class=emph>Validation Method</td><td align=left>";
    echo "<select id='VariantValidation' name='inh'>";
    $valmethods = array("-", "Sanger", "HRM", "qPCR", "MLPA", "NGS", "MicroArray");
    if ($vs['validation'] == "-") {
        $vs['validation'] = "-";
    }
    foreach ($valmethods as $key => $method) {
        if ($vs['validation'] == "$method") {
            echo "<option value='$method' SELECTED>$method</option>";
        } else {
            echo "<option value='$method'>$method</option>";
        }
    }
    echo "</select></td></tr>";
    echo "<tr title='Add Validation Details'><td align=right class=emph>Validation Details</td><td align=left>";
    echo "<textarea id='Validationdetails' cols=30 row=5>" . stripslashes($vs['validationdetails']) . "</textarea>";

    echo "</td></tr>";
    echo "</table>";
    echo "</p>";
    // some hidden fields
    echo "<input type=hidden id='sid' value='$sid'><input type=hidden id='vid' value='$vid'><input type=hidden id='uid' value='$uid'>";
    // submission buttons
    echo "<p><input type=submit value='Submit and Update Query Results' class=button onClick=\"UpdateValidity('1')\"/> <input type=submit value='Submit' class=button onClick=\"UpdateValidity('0')\"/> <input type=submit value='Cancel' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";

    exit();
}

// update values
if ($action == 'Update') {
    // get extra posted values
    $vclass = $_POST['vclass'];
    $vinh = $_POST['vinh'];
    $vinhm = $_POST['vinhm'];
    $vval = $_POST['vval'];
    $vvaldet = $_POST['vvaldet'];
    // get old settings.
    $vs = runQuery("SELECT inheritance, InheritanceMode, class, validation, validationdetails,autoclassified FROM `Variants_x_Samples` WHERE vid = '$vid' AND sid = '$sid'", "Variants_x_Samples")[0];
    $v = runQuery("SELECT chr, start, RefAllele, AltAllele FROM `Variants` WHERE id = '$vid'", "Variants")[0];
    $vac = $vs['autoclassified'];

    $updateSummary = 0;
    // update log
    $entry = 'Updated Signifance and Validity';
    $arguments = '';
    if ($vs['inheritance'] != $vinh) {
        $inheritances = array('0' => '-', '1' => 'Paternal', '2' => 'Maternal', '3' => 'De Novo', '4' => 'Both Parents');
        $arguments .= "Inheritance (" . $inheritances[$vs['inheritance']] . " to " . $inheritances[$vinh] . "),";
        $updateSummary = 1;
    }
    if ($vs['InheritanceMode'] != $vinhm) {
        $inhm = array('0' => 'Unknown', '1' => 'Dominant', '2' => 'Recessive');
        $arguments .= "Inheritance Mode (" . $inhm[$vs['InheritanceMode']] . " to " . $inhm[$vinhm] . "),";
        $updateSummary = 1;
    }

    if ($vs['class'] == '') {
        $vs['class'] = 0;
    }
    if ($vs['class'] != $vclass) {
        // changed, so reset autoclassified.
        $vac = 0;
        if (preg_match("/Auto-Classified: /", $vs['validationdetails']) && !preg_match("/Over-ruled Auto-Classified: /", $vs['validationdetails'])) {
            preg_replace("/Auto-Classified: /", "Over-ruled Auto-Classified: ", $vs['validationdetails']);
        }
        // code class for log.
        $classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');
        $arguments .= "Diagnostic Class (" . $classes[$vs['class']] . " to " . $classes[$vclass] . "),";
        $updateSummary = 1;
    }
    if ($vs['validation'] != $vval) {
        $valmethods = array("-", "Sanger", "HRM", "qPCR", "MLPA", "NGS", "MicroArray");
        $arguments .= "Validation Method (" . $vs['validation'] . " to " . $vval . "),";
    }
    if (addslashes($vs['validationdetails']) != addslashes($vvaldet)) {
        if ($vs['validationdetails'] == '') {
            $vs['validationdetails'] = '-';
        }
        $arguments .= "Validation Details (old value:" . addslashes($vs['validationdetails']) . "),";
    }
    if ($arguments == '') {
        $arguments = 'No changes made';
    } else {
        $arguments = substr($arguments, 0, -1);
    }
    // update values
    $ct = "Variants_x_Samples";
    if ($updateSummary == 1) {
        $ct .= ":Variants_x_Users_Summary:Variants_x_Projects_Summary:Summary";
    }
    doQuery("UPDATE `Variants_x_Samples` SET class = '$vclass' , InheritanceMode = '$vinhm', inheritance = '$vinh', validation = '$vval', validationdetails = '" . addslashes($vvaldet) . "', autoclassified = '$vac' WHERE vid = '$vid' AND sid = '$sid'", $ct);

    // log.	
    doQuery("INSERT INTO `Log` (sid, vid, uid, entry, arguments) VALUES ('$sid','$vid','$uid','$entry','$arguments')", "Log");

    // update the summary tables?
    if ($updateSummary == 1) {
        // set status to 0 for projects.
        //  => not implemented. zero would cause full user/project update. 
        //  => we take a small risk of leaving the table out-of-date. There is only a one second delay in starting the update. 

        // queue for update.
        $lock = fopen("../Query_Results/.VariantSummary.lock", 'a');
        if (flock($lock, LOCK_EX)) {
            if (!file_exists("../Query_Results/.VariantSummary.queue")) {
                touch("../Query_Results/.VariantSummary.queue");
                system("chmod 777 '../Query_Results/.VariantSummary.queue'");
            }
            $file = fopen("../Query_Results/.VariantSummary.queue", 'a');
            fwrite($file, "$vid,$sid\n");
            fclose($file);
            // release lock
            flock($lock, LOCK_UN);
        } else {
            echo "Error locking file!";
            exit();
        }
        fclose($lock);
    }
    // all done.
    echo "OK";
    exit();
}
