<?php
// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}
$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_SESSION['userID'];
$uid = $userid;
// get sampleID
$sid = $_GET['sid'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// get details for sample
$row = runQuery("SELECT s.Name, s.Gender, s.IsControl, s.affected,s.auto_gender,ps.pid AS pid, p.Name As pname, s.clinical AS FreeText FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects` p ON s.id = ps.sid AND p.id = ps.pid WHERE s.id = '$sid'", "Samples:Projects_x_Samples:Projects")[0];
$sname = stripslashes($row['Name']);
$sgender = $row['Gender'];
$pid = $row['pid'];
$projectname = $row['pname'];
$FreeClin = $row['FreeText'];
$ic = $row['IsControl'];
$ia = $row['affected'];
$ag = $row['auto_gender'];

// get permissions on this sample for current user.
$row = runQuery("SELECT editvariant, editclinic, editsample FROM `Projects_x_Users` WHERE pid = $pid AND uid = $userid", "Projects_x_Users")[0];
$editvariant = $row['editvariant'];
$editsample = $row['editsample'];
$editclinic = $row['editclinic'];

// get all available projects where you have editsample access.
$rows = runQuery("SELECT p.id, p.Name FROM `Projects` p JOIN `Projects_x_Users` pu ON p.id = pu.pid WHERE pu.uid = '$userid' AND pu.editsample = 1", "Projects:Projects_x_Users");
foreach ($rows as $k => $row) {
    $projects[$row['Name']] = $row['id'];
}
ksort($projects);


// print main sample details
// table head
echo "<h3>Set Sample Details</h3>";
echo "<form>";
echo "<p><table cellspacing=0 style='width:95%' id='DetailsTable'>";
echo "<thead >";
echo "<tr>";
echo "<td colspan=3 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>General Details</td>";
echo "</tr>";
echo "<tr>";
echo "<th class=top>Sample Name</th><th class=top>Gender</th><th class=top>Project</th></tr></thead>";
// table body with general details => == row 1.
echo "<tbody id=general>";
echo "<tr>";
if ($editsample == 1) {
    echo "<td><input type=text id='sname$sid' name='sname$sid' value='$sname' size=40></td>";
    switch ($sgender) {
        case "Male":
            echo "<td><select id='gender$sid' name='gender$sid' ><option value='Undef'>Unknown</option><option value='Male' SELECTED>Male</option><option value='Female' >Female</option></select></td>";
            break;
        case "Female":
            echo "<td><select id='gender$sid' name='gender$sid' ><option value='Undef'>Unknown</option><option value='Male'>Male</option><option value='Female' SELECTED>Female</option></select></td>";
            break;
        default:
            echo "<td><select id='gender$sid' name='gender$sid' ><option value='Undef' SELECTED>Unknown</option><option value='Male'>Male</option><option value='Female' >Female</option></select> ";
            if ($ag != '.') {
                echo "<span class=italic>(Estimated gender: $ag)</span></td>";
            }
            break;
    }
    echo "<td><select id='pid$sid' name='pid$sid'>";
    foreach ($projects as $pname => $ppid) {
        $selected = '';
        if ($ppid == $pid) {
            $selected = 'SELECTED';
        }
        echo "<option value='$ppid' $selected>$pname</option>";
    }
    echo "</select></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<th class=top>Sample is a Control Sample</th><th colspan=2 class=top>Sample is Affected</th></tr>";
    echo "<tr><td >";
    if ($ic == 1) {
        echo "<input type=radio id='IC$sid' name='IC$sid' value='1' checked /> Yes / <input type=radio id='IC$sid' name='IC$sid' value='0' /> No";
    } else {
        echo "<input type='radio' id='IC$sid' name='IC$sid' value='1' /> Yes / <input type=radio id='IC$sid' name='IC$sid' value='0' checked /> No";
    }
    echo "</td><td colspan=2>";
    if ($ia == 1) {
        echo "<input type=radio id='IA$sid' name='IA$sid' value='1' checked /> Yes / <input type=radio id='IA$sid' name='IA$sid' value='0' /> No";
    } else {
        echo "<input type='radio' id='IA$sid' name='IA$sid' value='1' /> Yes / <input type=radio id='IA$sid' name='IA$sid' value='0' checked /> No";
    }

    echo "</td></tr>";
} else {
    // no changes allowed, just print.
    echo "<td>$sname</td>";
    echo "<td>$sgender</td>";
    echo "<td>$projectname</td>";
    echo "<tr>";
    echo "<th class=top>Sample is a Control Sample</th><th colspan=2 class=top>Sample is Affected</th></tr>";
    echo "<tr><td>";
    if ($ic == 1) {
        echo "Yes";
    } else {
        echo "No";
    }
    echo "</td><td colspan=2>";
    if ($ia == 1) {
        echo "Yes";
    } else {
        echo "No";
    }

    echo "</td></tr>";
}
echo "</tr>";

echo "</tbody>";
// Relations available?
$famcode = array(1 => 'Replicates', 2 => 'Children', 3 => 'Siblings');
$nrfam = 0;
for ($i = 1; $i <= 3; $i++) {
    echo "<tbody id='Relation$i'>";
    echo "<tr class='spacer' ><td colspan=3></td></tr>";
    echo " <tr>\n";
    echo "    <td colspan=3 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Sample Relations: " . $famcode[$i] . " &nbsp; ";
    if ($editsample == 1) {
        echo "<span title='Add " . $famcode[$i] . "' onClick=\"AddRelation('$sid','$i')\"><img src='Images/layout/plus-icon.png'  style='height:1em;'></span>";
    }
    echo "</td>\n";
    echo "  </tr>\n";
    $rows = runQuery("SELECT ss.sid2, s.Name FROM `Samples_x_Samples` ss JOIN `Samples` s ON ss.sid2 = s.id WHERE sid1 = '$sid' AND ss.Relation = '$i'", "Samples_x_Samples:Samples");
    foreach ($rows as $k => $row) {
        if ($i == 2) {
            $nrfam++;
        }
        $sid2 = $row['sid2'];
        echo "<tr>";
        if ($editsample == 1) {
            echo "<td style='text-align:right;'><select id='DEL-$i-$sid-$sid2'><option value='0' SELECTED>Keep</option><option value='1'>Delete</option></select></td><td colspan=2>" . $row['Name'] . " <a title='Switch to sample' href='index.php?page=samples&pt=single&sid=" . $row['sid2'] . "'><img style='margin-bottom:-0.2em;' src='Images/layout/edit.gif' /></a></td></tr>";
        } else {
            echo "<td>&nbsp;</td><td colspan=2>" . $row['Name'] . " <a title='Switch to sample' href='index.php?page=samples&pt=single&sid=" . $row['sid2'] . "'><img style='height:0.8em;margin-bottom:-0.1em;' src='Images/layout/eye.png' /></a></td></tr>";
        }
    }
    echo "</tbody>";
}
# Get parents
$rows = runQuery("SELECT ss.sid1, s.Name FROM `Samples_x_Samples` ss JOIN `Samples` s ON ss.sid1 = s.id WHERE ss.sid2 = '$sid' AND Relation = 2", "Samples_x_Samples:Samples");

echo "<tbody id='Relation4'>";
echo "<tr class='spacer' ><td colspan=3></td></tr>";
echo "<tr>";
echo "<td colspan=3 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Sample Relations: Parents &nbsp; ";
if ($editsample == 1) {
    echo "<span title='Add Parent' onClick=\"AddRelation('$sid','4')\"><img src='Images/layout/plus-icon.png' style='height:1em;'></span>";
}
echo "</td>";
echo "</tr>\n";
foreach ($rows as $k => $row) {
    $nrfam++;
    $sid2 = $row['sid1'];
    echo "<tr>";
    if ($editsample == 1) {
        echo "<td style='text-align:right;'><select id='DEL-4-$sid-$sid2'><option value='0' SELECTED>Keep</option><option value='1'>Delete</option></select></td><td colspan=2>" . $row['Name'] . " <a title='Switch to sample' href='index.php?page=samples&pt=single&sid=" . $row['sid1'] . "'><img style='margin-bottom:-0.2em;' src='Images/layout/edit.gif' /></a></td></tr>";
    } else {
        echo "<td>&nbsp;</td><td colspan=2>" . $row['Name'] . " <a title='Switch to sample' href='index.php?page=samples&pt=single&sid=" . $row['sid1'] . "'><img style='height:0.8em;margin-bottom:-0.1em;' src='Images/layout/eye.png' /></a></td></tr>";
    }
}
echo "</tbody>";
##custom relation groups. 
$cg = array(5 => 'A', 6 => 'B');
for ($i = 5; $i <= 6; $i++) {
    $rows = runQuery("SELECT ss.sid1, s.Name FROM `Samples_x_Samples` ss JOIN `Samples` s ON ss.sid1 = s.id WHERE ss.sid2 = '$sid' AND Relation = $i", "Samples_x_Samples:Samples");
    echo "<tbody id='Relation$i'>";
    echo "<tr class='spacer' ><td colspan=3></td></tr>";
    echo "<tr>";
    echo "<td colspan=3 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Custom Relations: Group " . $cg[$i] . " &nbsp; ";
    if ($editsample == 1) {
        echo "<span title='Add Sample' onClick=\"AddRelation('$sid','$i')\"><img src='Images/layout/plus-icon.png' style='height:1em;'></span>";
    }
    echo "</td></tr>\n";
    foreach ($rows as $k => $row) {
        #$nrfam++;
        $sid2 = $row['sid1'];
        echo "<tr>";
        if ($editsample == 1) {
            echo "<td style='text-align:right;'><select id='DEL-$i-$sid-$sid2'><option value='0' SELECTED>Keep</option><option value='1'>Delete</option></select></td><td colspan=2>" . $row['Name'] . " <a title='Switch to sample' href='index.php?page=samples&pt=single&sid=" . $row['sid1'] . "'><img style='margin-bottom:-0.2em;' src='Images/layout/edit.gif' /></a></td></tr>";
        } else {
            echo "<td>&nbsp;</td><td colspan=2>" . $row['Name'] . " <a title='Switch to sample' href='index.php?page=samples&pt=single&sid=" . $row['sid1'] . "'><img style='height:0.8em;margin-bottom:-0.1em;' src='Images/layout/eye.png' /></a></td></tr>";
        }
    }
    echo "</tbody>";
}

// FULL PEDIGREE 
if ($nrfam > 0) {
    echo "<tbody id='pedigree'>";
    echo "<tr class='spacer'><td colspan=3></td></tr>";
    echo "<tr><td colspan=3 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Pedigree</td></tr>";
    $counter = 0;
    $notes = GetPedigree($sid, $counter, $uid);
    echo "<tr><td colspan=3 align=center><img style='display:block;margin-left:auto;margin-right:auto;' src='Images/pedigrees/$sid.$uid.ped.png?r=" . rand() . "' alt='Pedigree plot' /><br/>$notes</td></tr>";
}

// CLINICAL INFO
echo "<tbody id='FreeClinicalInfo'>";
echo "<tr class='spacer' ><td colspan=3></td></tr>";
echo "<tr>";
echo "<td colspan=3 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Clinical Details</td>";
echo "</tr>";
echo "<tr>";
echo "<td colspan=3 ><span class=emph>Free Text Information</span><br/>";
if ($editclinic == 1) {
    echo "<textarea id=FreeClinArea style='width:100%' rows=10>" . stripslashes($FreeClin) . "</textarea></td>";
} else {
    echo "<textarea id=FreeClinArea style='width:100%' rows=10 readonly='readonly'>" . stripslashes($FreeClin) . "</textarea></td>";
}
echo "</tr>";
echo "<tr>";
echo "<td colspan=3 ><span class=emph>Human Phenotype Ontology</span> ";
if ($editclinic == 1) {
    echo "<span title='Add Phenotype' onClick=\"AddPhenotype('$sid')\"><img src='Images/layout/plus-icon.png' style='height:1em;'></span>";
}
echo "</td></tr></tbody>";
$rows = runQuery("SELECT t.id, t.Name, t.Definition, tt.tid2, GROUP_CONCAT(tb.Name SEPARATOR ', ') AS MemberOf FROM `Samples_x_HPO_Terms` st JOIN `HPO_Terms` t JOIN `HPO_Term_x_Term` tt JOIN `HPO_Terms`tb ON st.tid = t.id AND t.id = tt.tid1 AND tt.tid2 = tb.id WHERE st.sid = '$sid' GROUP BY t.id ORDER BY t.id", "Samples_x_HPO_Terms:HPO_Terms:HPO_Term_x_Term");
$out = "<tr id='hpo_header' ><th class='strong underline' style='padding-left:1em;' >Phenotype</th><th class='strong underline'>Definition</th><th class='strong underline'>Member of</th></tr><tbody id='HPO'>";
$valid = 0;
foreach ($rows as $k => $row) {
    $tid = $row['id'];
    if ($row['id'] == null) {
        continue;
    }
    $valid++;
    $out .= "<tr id='HPO_$tid'><td  style='padding-left:1em;padding-bottom:0.5em;vertical-align:top'>";
    if ($editclinic == 1) {
        $out .= "<span title='Remove Phenotype' onClick=\"DelPhenotype('$sid','$tid')\"><img src='Images/layout/icon_trash.gif' style='height:0.8em;'></span> ";
    }
    $out .= $row['Name'] . "</td><td style='vertical-align:top;padding-bottom:0.5em;'>" . $row['Definition'] . "</td><td style='vertical-align:top;padding-bottom:0.5em;'>" . $row['MemberOf'] . "</td></tr>";
}
if ($valid == 0) {
    echo "$out<tr id='nopheno'><td colspan=3 style='padding-left:1em;' class=italic>No Structured phenotypes available</td></tr><tbody id='HPO'></tbody>";
} else {
    echo $out . "</tbody>";
}
echo "<tr><td colspan=3 class=last>&nbsp;</td></tr>";
echo "</table>";

echo "</form>";
echo "</p>";
if ($editsample == 1) {
    echo "<p><input type=submit value='Save' onClick=\"SaveSampleDetails('$sid')\"></p>";
}

// add header to hpo table if phenotypes are present.



// GetPedigree function
function GetPedigree($sid, $counter, $uid)
{
    include('../.LoadCredentials.php');
    $tree = array();
    $tree = GetRelatives($tree, $sid, $counter);
    // check if everything is available (genders etc)
    // check validity where possible
    $final = $tree;
    $missing_idx = 0;
    $guessed_genders = array();
    $couples = array();
    foreach ($tree as $s => $array) {
        // missing parent?
        if (count($tree[$s]['parents']) == 1) {
            // gender known for the one present => add second as 'missing'
            if ($tree[$tree[$s]['parents'][0]]['gender'] == 'Male') {
                // already specified from siblings?
                if (isset($couples[$tree[$s]['parents'][0]])) {
                    array_push($final[$s]['parents'], 'Missing_parent_' . $couples[$tree[$s]['parents'][0]]);
                } else {
                    $missing_idx++;
                    $final['Missing_parent_' . $missing_idx] = array();
                    $final['Missing_parent_' . $missing_idx]['gender'] = 'Female';
                    $final['Missing_parent_' . $missing_idx]['affected'] = 0;
                    $final['Missing_parent_' . $missing_idx]['sname'] = 'Missing_parent_' . $missing_idx;
                    $final['Missing_parent_' . $missing_idx]['parents'] = array();
                    $couples[$tree[$s]['parents'][0]] = $missing_idx;
                    array_push($final[$s]['parents'], 'Missing_parent_' . $missing_idx);
                }
            } elseif ($tree[$tree[$s]['parents'][0]]['gender'] == 'Female') {
                // already specified from siblings?
                if (isset($couples[$tree[$s]['parents'][0]])) {
                    array_push($final[$s]['parents'], 'Missing_parent_' . $couples[$tree[$s]['parents'][0]]);
                } else {
                    $missing_idx++;
                    $final['Missing_parent_' . $missing_idx] = array();
                    $final['Missing_parent_' . $missing_idx]['gender'] = 'Male';
                    $final['Missing_parent_' . $missing_idx]['affected'] = 0;
                    $final['Missing_parent_' . $missing_idx]['sname'] = 'Missing_parent_' . $missing_idx;
                    $final['Missing_parent_' . $missing_idx]['parents'] = array();
                    array_push($final[$s]['parents'], 'Missing_parent_' . $missing_idx);
                    $couples[$tree[$s]['parents'][0]] = $missing_idx;
                }
            }
            // arbitrarily set gender of specified parent (and mention it)
            else {
                // already specified from siblings?
                if (isset($couples[$tree[$s]['parents'][0]])) {
                    array_push($final[$s]['parents'], 'Missing_parent_' . $couples[$tree[$s]['parents'][0]]);
                } else {

                    $missing_idx++;
                    $final['Missing_parent_' . $missing_idx] = array();
                    $final['Missing_parent_' . $missing_idx]['gender'] = 'Male';
                    $final['Missing_parent_' . $missing_idx]['parents'] = array();
                    $final['Missing_parent_' . $missing_idx]['affected'] = 0;
                    $final['Missing_parent_' . $missing_idx]['sname'] = 'Missing_parent_' . $missing_idx;
                    array_push($final[$s]['parents'], 'Missing_parent_' . $missing_idx);
                    $final[$tree[$s]['parents'][0]]['gender'] = 'Female';
                    array_push($guessed_genders, $tree[$s]['parents'][0]);
                    $couples[$tree[$s]['parents'][0]] = $missing_idx;
                }
            }
        }
        // missing gender
        elseif (count($tree[$s]['parents']) == 2) {
            // one missing
            if ($tree[$tree[$s]['parents'][0]]['gender'] != 'Unknown' && $tree[$tree[$s]['parents'][1]]['gender'] == 'Unknown') {
                if ($tree[$tree[$s]['parents'][0]]['gender'] == 'Male') {
                    $final[$tree[$s]['parents'][1]]['gender'] = 'Female';
                } else {
                    $final[$tree[$s]['parents'][1]]['gender'] = 'Male';
                }
            } elseif ($tree[$tree[$s]['parents'][1]]['gender'] != 'Unknown' && $tree[$tree[$s]['parents'][0]]['gender'] == 'Unknown') {
                if ($tree[$tree[$s]['parents'][1]]['gender'] == 'Male') {
                    $final[$tree[$s]['parents'][0]]['gender'] = 'Female';
                } else {
                    $final[$tree[$s]['parents'][0]]['gender'] = 'Male';
                }
            }
            // both missing
            elseif ($tree[$tree[$s]['parents'][1]]['gender'] == 'Unknown' && $tree[$tree[$s]['parents'][0]]['gender'] == 'Unknown') {

                $final[$tree[$s]['parents'][0]]['gender'] = 'Male';
                $final[$tree[$s]['parents'][1]]['gender'] = 'Female';
                array_push($guessed_genders, $tree[$s]['parents'][0]);
                array_push($guessed_genders, $tree[$s]['parents'][1]);
            }
        }
    }
    // update tree
    $tree = $final;
    // add note to random genders.
    foreach ($guessed_genders as $k => $s) {
        $ln = $tree[$s]['sname'] . "_-RG-";
        if (substr($tree[$s]['sname'], -4) != '-RG-') {
            $tree[$s]['sname'] = $ln;
        }
        //unset($tree[$s]);
        /*foreach($tree as $sv => $array) {
			for($i = 0; $i<count($tree[$sv]['parents']); $i++) {
				if ($tree[$sv]['parents'][$i] == $s) {
					$tree[$sv]['parents'][$i] = $ln;
				}
			}
		}
		*/
    }
    // write out
    #$tree['NA']['sname'] = 'NA';
    $fh = fopen("/tmp/$sid.$uid.ped", 'w');
    fwrite($fh, "ped,id,father,mother,sex,affected,avail\n");
    foreach ($tree as $s => $array) {
        if ($s == 'NA' || $tree[$s]['sname'] == 'NA') {
            trigger_error("NA for $s");
            continue;
        }
        $out = "1," . $tree[$s]['sname'] . ",";
        $m = $f = 'NA';
        $unclass = array();
        foreach ($tree[$s]['parents'] as $k => $ps) {
            if ($tree[$ps]['gender'] == 'Male') {
                $f = $ps;
            } elseif ($tree[$ps]['gender'] == 'Female') {
                $m = $ps;
            }
        }
        // Validate tree entries (gender, missing, ...)
        // TODO
        // one missing parent => skip, gives errors
        if (($m == 'NA' || $f == 'NA') && count($tree[$s]['parents']) > 0) {
            trigger_error("Missing parent for $s : $m/$f");
            // check parents.
            continue;
        }

        // generate output.
        $out .= $tree[$f]['sname'] . "," . $tree[$m]['sname'] . "," . $tree[$s]['gender'] . "," . $tree[$s]['affected'] . ",";
        if ($s == $sid) {
            $out .= "1\n";
        } else {
            $out .= "0\n";
        }
        fwrite($fh, $out);
    }
    $r = '';
    if ($missing_idx > 0) {
        #fwrite($fh,'2,Note: "Missing Parent": Automatically added indivual to complete pedigree,0,0,0,0,0'."\n");
        $r .= '<span class=emph>Note:</span> "Missing Parent": Automatically added indivual to complete pedigree' . "<br/>";
    }
    if (count($guessed_genders) > 0) {
        #fwrite($fh,'2,Note: "-RG-": Gender was randomly assigned to generate a valid pedigree,0,0,0,0,0'."\n");
        $r .= '<span class=emph>Note:</span> "-RG-": Gender was randomly assigned to generate a valid pedigree' . "<br/>";
    }

    fclose($fh);
    system("cp /tmp/$sid.$uid.ped /home/vdbdev/dump/");
    $cmd =  "(echo $scriptpass | sudo -u $scriptuser -S bash -c '";
    $cmd = "(export PATH=\$PATH:$path; ";
    $cmd .= "Rscript \"$scriptdir/includes/ped.R\" \"/tmp/$sid.$uid.ped\" \"$scriptdir/Images/pedigrees/$sid.$uid.ped.png\" && ";
    $cmd .= "chmod a+x \"$scriptdir/Images/pedigrees/$sid.$uid.ped.png\" &&  ";
    $cmd .= "rm -f \"/tmp/$sid.$uid.ped\" ";
    $cmd .= ") 2>>" . $config['LOG_PATH'] . "/Rscript.log >>" . $config['LOG_PATH'] . "/Rscript.log";
    try {
        system($cmd, $rc);
    } catch (Error $e) {
        trigger_error($e->getMessage(), E_USER_ERROR);
    }
    return ($r);
}

function GetRelatives($tree, $sid, $counter)
{
    // get full pedigree information.

    // recursive function to build full pedigree tree
    // first add node for current sample
    if (!array_key_exists($sid, $tree)) {
        $tree[$sid] = array('parents' => array(), 'children' => array());
    }
    // sample already seen.
    if (isset($tree[$sid]['done'])) {
        return ($tree);
    }
    // get sample gender, name, etc
    $srow = runQuery("SELECT Name, gender, affected,auto_gender FROM `Samples` WHERE id = '$sid'", "Samples")[0];
    // store sample name && affected status
    $tree[$sid]['sname'] = $srow['Name'];
    $tree[$sid]['affected'] = $srow['affected'];
    // gender set
    $tree[$sid]['gender'] = 'Unknown';
    if ($srow['gender'] != 'Undef') {
        $tree[$sid]['gender'] = $srow['gender'];
    }
    // gender estimated
    elseif ($srow['auto_gender'] != '.') {
        $tree[$sid]['gender'] = $srow['auto_gender'];
    }
    // estimate now.
    else {
        $vrow = runQuery("SELECT COUNT(vs.vid) AS nr FROM  `Variants_x_Samples` vs JOIN `Variants` v ON  vs.vid = v.id WHERE  vs.AltCount = 2 AND v.chr = 23 AND v.start BETWEEN 4000000 AND 154000000 AND vs.sid = '$sid'", "Variants_x_Samples:Variants")[0];
        $homs = $vrow['nr'];
        $vrow = runQuery("SELECT COUNT(vs.vid) AS nr FROM  `Variants_x_Samples` vs JOIN `Variants` v ON  vs.vid = v.id WHERE  v.chr = 23 AND v.start BETWEEN 4000000 AND 154000000 AND vs.sid = '$sid'", "Variants_x_Samples:Variants")[0];
        $total = $vrow['nr'];
        if ($total > 50) {
            if ($homs / $total > 0.6) {
                // male.
                $tree[$sid]['gender'] = 'Male';
                doQuery("UPDATE `Samples` SET `auto_gender` = 'Male' WHERE id = '$sid'", "Samples");
            } elseif ($homs / $total < 0.55) {
                // female
                $tree[$sid]['gender'] = 'Female';
                doQuery("UPDATE `Samples` SET `auto_gender` = 'Female' WHERE id = '$sid'", "Samples");
            }
        }
    }
    // family to fetch details for.
    $todo = array();
    // get siblings. (skipped, gives errors if sibling has no parents specified.)
    /*$frows = runQuery("SELECT sid2 FROM `Samples_x_Samples` WHERE sid1 = '$sid' AND Relation = 3 ","Samples_x_Samples");
	if ( count($frows) > 0 && !is_array($frows[0])) $frows = OneToMulti($frows);
	foreach($frows as $k => $row){
		array_push($todo,$row['sid2']);
		if (!array_key_exists('siblings',$tree[$sid])) {
			$tree[$sid]['siblings'] = array();
		}
		array_push($tree[$sid]['siblings'], $row['sid2']);
	}	
	*/
    // get children
    $frows = runQuery("SELECT sid2 FROM `Samples_x_Samples` WHERE sid1 = '$sid' AND Relation = 2", "Samples_x_Samples");
    foreach ($frows as $k => $row) {
        array_push($todo, $row['sid2']);
        if (!array_key_exists('children', $tree[$sid])) {
            $tree[$sid]['children'] = array();
        }
        array_push($tree[$sid]['children'], $row['sid2']);
    }

    // get parents
    $frows = runQuery("SELECT sid1 FROM `Samples_x_Samples` WHERE sid2 = '$sid' AND Relation = 2", "Samples_x_Samples");
    foreach ($frows as $k => $row) {
        array_push($todo, $row['sid1']);
        if (!array_key_exists('parents', $tree[$sid])) {
            $tree[$sid]['parents'] = array();
        }
        array_push($tree[$sid]['parents'], $row['sid1']);
    }
    $tree[$sid]['done'] = 1;
    // recurse samples
    foreach ($todo as $k => $v) {
        $tree = GetRelatives($tree, $v, $counter);
    }
    return ($tree);
}
