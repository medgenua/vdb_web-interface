<?php
$sid = $_POST['sid'];
if (!is_numeric($sid)) {
    echo "Invalid Sample ID";
    exit;
}
$setid = $_POST['setid'];
if (!is_numeric($setid)) {
    echo "Invalid Set ID";
    exit;
}
$page_idx = $_POST['page_idx'];
if (!is_numeric($page_idx)) {
    echo "Invalid page ID";
    exit;
}

#######################
# CONNECT TO DATABASE # ASC
#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');

$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$uid = $_SESSION['userID'];

if (!is_numeric($uid)) {
    echo "invalid userid : '$uid'";
    exit;
}
// Get sample name
$row = runQuery("SELECT Name FROM `Samples` WHERE id = '$sid'", "Samples");
if (count($row) == 0) {
    echo "<h3>Sample '$sid' not found.</h3>";
    exit();
}
$sname = $row[0]['Name'];
// Get Set name
$row = array_shift(...[runQuery("SELECT set_name,Validated FROM `Samples_x_Saved_Results` WHERE set_id = '$setid'", "Samples_x_Saved_Results")]);
if (!is_array($row) || count($row) == 0) {
    echo "<h3>Set_ID '$setid' not found.</h3>";
    exit();
}
$set_name = $row['set_name'];
$validated = $row['Validated'];
// Get Nr.Pages.
$row = runQuery("SELECT COUNT(set_id) AS nrp FROM `Samples_x_Saved_Results_Pages` WHERE set_id = '$setid'", "Samples_x_Saved_Results_Pages")[0];

$nr_pages = $row['nrp'];

if ($row['nrp'] == 0) {
    echo "<h3>Saved Results Set '$set_name' : no results.</h3>";
    echo "<p>No stored pages found for the current set.</p>";
    exit;
}


echo "<h3>Saved Results Set '$set_name' : page $page_idx/$nr_pages</h3>";
// hidden fields.
echo "<input type=hidden id='setid' value='$setid'/>";
echo "<input type=hidden id='pagenr' value='$page_idx'/>";
echo "<p style='padding-left:1em'>Switch page : <select id='stored_page_select'>";
for ($i = 1; $i <= $nr_pages; $i++) {
    if ($i == $page_idx) {
        $select = 'selected="selected"';
    } else {
        $select = '';
    }
    echo "<option $select value='$i'>Page $i</option>";
}
echo "</select> &nbsp; <input type=button value=' Show ' onClick=\"ShowStoredResults('$uid','$sid','$setid')\" /></p>";

$row = runQuery("SELECT contents FROM `Samples_x_Saved_Results_Pages` WHERE set_id = '$setid' AND page = '$page_idx'", "Samples_x_Saved_Results_Pages")[0];

//////////////////////////////////////
// VARIANT EDITING (class, inh,...) //
//////////////////////////////////////
// disable editing when validated.
if ($validated == 1) {
    // hide edit icons
    $pattern = "/[\"']Images\/layout\/edit.gif[\"'] style=['\"]height:1.25em['\"]/";
    $replace = "'Images/layout/edit.gif' style=\"display:none;\"";
    $out = preg_replace($pattern, $replace, $row['contents']);
    // disable checkboxes
    $out = preg_replace("/ class=[\"']NoteBox['\"] /", " disabled class='NoteBox' ", $out);
}
// else: update the edit scripts to work on saved data.
else {
    // check if data is compatible.
    if (preg_match("/<span id=[\"']sv_\d+[\"'] style/", $row['contents'])) {
        // original saved data : onClick
        $pattern = "/ on[Cc]lick=\"setValidity\(('\d+','\d+','\d+')\)/";
        $replace = " onClick=\"setStoredValidity($1,'$setid','$page_idx')";
        $out = preg_replace($pattern, $replace, $row['contents']);
    } else {
        // hide edit icons.
        $pattern = "/[\"']Images\/layout\/edit.gif[\"'] style=['\"]height:1.25em['\"]/";
        $replace = "'Images\/layout\/edit.gif' style=\"display:none;\"";
        $out = preg_replace($pattern, $replace, $row['contents']);
        // add message.
        $pattern = "/<h4>Results Table<\/h4>/";
        $replace = "<h4>Results Table</h4><p><span class='emph' style='color:red'>EDITING DISABLED:</span> These saved results are in a legacy format or too few annotations were added. Please re-run and re-save them to enable editing of stored results.</p>";
        $out = preg_replace($pattern, $replace, $out);
    }
}
////////////////////////////
// CLASSIFIER INFORMATION //
////////////////////////////
// get classifier variants.
$rows = runQuery("SELECT cid, can_validate FROM `Users_x_Classifiers` WHERE uid = '$uid' AND (`can_use` + `can_add` + `can_validate` + `can_remove` + `can_share` ) > 0", "Users_x_Classifiers");
$classifiers = array();
foreach ($rows as $row) {
    $classifiers[$row[0]] = $row[1];
}
$variants = array();
$rows = array();
if (count($classifiers) > 0) {
    $rows = runQuery("SELECT cid,vid, validate_reject FROM `Classifiers_x_Variants` WHERE cid IN (" . implode(",", array_keys($classifiers)) . ")", "Classifiers_x_Variants");
}
foreach ($rows as $row) {
    # skip rejected variants
    if ($row['validate_reject'] == -1) {
        continue;
    };
    # store the others with validation status.
    $variants[$row['vid']][$row['cid']]['s'] = $row['validate_reject'];
    # and add info if user is allowed to validate.
    $variants[$row['vid']][$row['cid']]['a'] = $classifiers[$row['cid']];
}

// load the html, but ignore parsing errors
$internalErrors = libxml_use_internal_errors(true);
$doc = new DOMDocument();
//@$doc->loadHTML(htmlentities($out));
@$doc->loadHTML($out);
$selector = new DOMXPath($doc);

// find the boxes.
// SET CLASSIFIER STATUS + INFO-button if missing.
foreach ($selector->query('//*[starts-with(@id,"box_")]') as $node) {
    $vid = substr($node->getAttribute("id"), 4);
    $box = $doc->getElementByID("box_$vid");
    // button missing? 
    $idiv = $doc->getElementByID("InfoDiv_$vid");
    if (!$idiv) {
        // create it.
        error_log("IDIV is missing");
        $html = "<span style='float:right' title='Show samples with this variant' onClick=\"SamplesWithVariant('$vid','$uid','$sid')\" id='InfoDiv_$vid'><img src='Images/layout/icon_info.gif' style='height:1.25em' /></span>";
        $fragment = $doc->createDocumentFragment();
        $fragment->appendXML($html);
        $idx = 0;
        foreach ($box->childNodes as $child) {
            $idx++;
            if ($idx == 2) {
                $child->parentNode->insertBefore($fragment, $child);
                break;
            }
        }
    }
    // continue with classifiers.
    if (!array_key_exists($vid, $variants)) {
        continue;
    }
    // what is replacement?
    $img = "Images/layout/add_to_classifier.png";
    foreach (array_keys($variants[$vid]) as $cid) {
        if ($variants[$vid][$cid]['s'] == 1) {
            $img = "Images/layout/added_to_classifier.png";
        } else {
            $img = "Images/layout/validate_classifier.png";
            break;
        }
    }
    // get it in the main DOM
    foreach ($box->childNodes as $child) {
        $html = $doc->saveHTML($child);
        // if onClick is AddToClassifier:
        if (strstr($html, "AddToClassifier")) {
            $html = preg_replace('/img src=.*.png"/', 'img src="' . $img . '"', $html);
            // correct img tag.
            $html = preg_replace('/em"><\/span>/', 'em"/></span>', $html);

            $fragment = $doc->createDocumentFragment();
            $fragment->appendXML($html);
            $doc->getElementByID("box_$vid")->replaceChild($fragment, $child);
        }
    }
    $out = $doc->saveHTML();
}
// overall replacement of Add to SavedAdd
$out = preg_replace("/AddToClassifier/", "AddSavedToClassifier", $out);
// hide LoadQueryLog (use LoadStoredQueryLog from results table)
$out = preg_replace("/QueryLog'/", "QueryLog' style='display:none'", $out);
echo $out;
