<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_GET['uid'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

$type = $_GET['type'];
$in = '';
if ($type == 's') {
    $in = $_GET['id'];
} elseif ($type == 'p') {
    $pid = $_GET['id'];
    // get samples
    $rows = runQuery("SELECT sid FROM `Projects_x_Samples` ps WHERE pid = $pid", "Projects_x_Samples");
    if (count($rows) > 0) {
        foreach ($rows as $k => $row) {
            $in .= $row['sid'] . ",";
        }
        $in = substr($in, 0, -1);
    }
}
// no results.
if ($in == '') {
    echo "No samples found or selected.";
    exit;
}

// search available options.
$rows = runQuery("SELECT ca.aid,ca.field_name FROM `Custom_Annotations` ca JOIN `Custom_Annotations_x_Samples` cas ON ca.aid = cas.aid WHERE cas.sid IN ($in) GROUP BY field_name ORDER BY field_name", "Custom_Annotations:Custom_Annotations_x_Samples");
if (count($rows) == 0) {
    echo "No custom annotations available.";
    exit;
}
// make checkboxes
$colcount = 0;
$output = "";
foreach ($rows as $k => $row) {
    $colcount++;
    if ($colcount > 4) {
        $output .= "</tr>";
        $colcount = 1;
    }
    if ($colcount == 1) {
        $output .= "<tr>";
    }
    $iname = $row['field_name'];
    $iid = $row['aid'];
    $output .= "<td title='Custom VCF Annotation Field.'><input type=checkbox name='annotations' value='Custom_VCF_Fields;$iid' onclick='AnnotationsToCookies()'> $iname</td>";
}
echo $output;
