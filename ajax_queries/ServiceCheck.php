<?php
// GOAL :
// - CHECK PIDS OF VARIANTDB MONITORS
// - LAUNCH IF PID NOT FOUND

// read credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}
$db = "NGS-Variants" . $_SESSION['dbname'];
// if coming from api curl : this variable is not set yet.
if (!isset($_SESSION['db'])) {
    $_SESSION['db'] = $db;
}

include('../includes/inc_logging.inc');
include('../includes/inc_query_functions.inc');
// Update the .htaccess file cgi-bin that sets path.
$PATH = '';
if (array_key_exists('CONDA_ENV', $config)) {
    $PATH .= $config['CONDA_ENV'] . "/bin:";
}
if (array_key_exists('PATH', $config)) {
    $PATH .= $config['PATH'] . ":";
}
// micromamba
$PATH .= $config["SCRIPTDIR"] . "/dependencies/micromamba:";
$PATH .= getenv('PATH');
file_put_contents($config['SCRIPTDIR'] . "/cgi-bin/.htaccess", "SetEnv PATH $PATH");

// LIST OF MONITORS (monitor => launch_by : 
$monitors = array(
    "Annotations/monitor.py",        // monitors the 'update.txt', launches annotations on new variants
    "cgi-bin/VariantDB_Query_Runner.py",    // processes api filter calls (also launched from API_wrapper if no HPC is used (upon submission))
    //"cgi-bin/VariantDB_Report_Runner.py",    // Launched from Report_Wrapper if not running yet.
    "Annotations/GATK_Annotations/LoadVariants.py",    // maintains the Variants_xxx_Summary tables.
    //"cgi-bin/VariantDB_Update_Validator.py", // monitor if the validation process needs to be run.
);


foreach ($monitors as $monitor) {
    # reverse string & split once : dir vs scriptname.
    list($name, $path) = explode("/", strrev($monitor), 2);
    $name = strrev($name);
    $path = strrev($path);
    // pid exists?
    $running = 1;
    $pid_file = "$scriptdir/Query_Logs/" . str_replace(".py", ".pid", $name);
    if (file_exists($pid_file)) {
        $pids = explode("\n", file_get_contents($pid_file));
        foreach ($pids as $pid) {
            if ($pid == "") {
                continue;
            }
            // if one of the processes has failed, kill all others.
            if (!file_exists("/proc/$pid")) {
                $running = 0;
            }
        }
        // if one is not running, kill all.
        if ($running == 0) {
            foreach ($pids as $pid) {
                if ($pid == "") {
                    continue;
                }
                // kill processes that are still running for a failed sibling.
                if (file_exists("/proc/$pid")) {
                    $command = "(echo $scriptpass | sudo -u $scriptuser -S bash -c \"kill -9 $pid  \" ) 2>/dev/null";
                    system($command);
                }
            }
        }
    }
    // no pid file => not running.
    else {
        $running = 0;
    }
    // launch if not running.
    if ($running == 0) {
        trigger_error("Launching Monitor : $monitor");
        $log_file = "$scriptdir/Query_Logs/" . str_replace(".py", ".log", $name);
        $command = "(echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd $scriptdir/cgi-bin && ./Service_Launcher.sh '$scriptdir/$path' '$name' '$log_file' >$log_file 2>&1\" ) 2>/dev/null";
        system("$command");
    }
}

// clean up old tmp tables.
$tables = runQuery("SHOW TABLES");
foreach ($tables as $table) {
    $table = $table[0];
    //trigger_error("this is tble : $table");
    if (strpos($table, 'QueryResults') !== 0) {
        continue;
    }
    //trigger_error("Evaluating $table");
    // get creation date:
    $cd = runQuery("SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'NGS-Variants-hg19' AND table_name = '$table'");
    $cdate = strtotime(explode(" ", $cd[0]['create_time'])[0]);
    $cd = explode(" ", $cd[0]['create_time'])[0];
    // more than two days old?
    if (time() - $cdate > 24 * 60 * 60) {
        trigger_error("dropping : $table : $cd");
        doQuery("DROP TABLE IF EXISTS `$table`");
    }
}
