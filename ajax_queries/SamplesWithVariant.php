<?php


//#######################
//# CONNECT TO DATABASE #
//#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// parameters
$uid = (isset($_POST['uid']) ? $_POST['uid'] : 0);
$vid = (isset($_POST['vid']) ? $_POST['vid'] : 0);
$sid = (isset($_POST['sid']) ? $_POST['sid'] : 0);
if (!is_numeric($uid) || !$uid) {
    // invalid userid
    echo "ERROR : invalid userid provided";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
    exit;
}
if (!is_numeric($vid) || !$vid) {
    // invalid userid
    echo "ERROR : invalid userid provided";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
    exit;
}

// code mappings
$inh = array(0 => 'ND', 1 => 'P', 2 =>  'M', 3 => 'DN', 4 => 'Both', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => '-', 'Both' => 'Both Parents');
//%inhm = (0,'-',1,'Dominant',2,'Recessive','3','Unknown');
$classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');
$chr_hash = range(0, 22);
array_push($chr_hash, "X", "Y", "MT");


// get variant 
$vid_info = array_shift(...[runQuery("SELECT v.chr, v.start, v.RefAllele, v.AltAllele FROM `Variants` v WHERE id = '$vid'", "Variants")]);
if (count($vid_info) == 0) {
    echo "ERROR : variant not found.";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";

    exit;
}
$variant = "chr" . $chr_hash[$vid_info['chr']] . ":" . number_format($vid_info['start']) . " : " . $vid_info['RefAllele'] . "/" . $vid_info['AltAllele'];


///////////////////
// SUMMARY STATS //
///////////////////
// stats up to date ? 
$sStatus = runQuery("SELECT `SummaryStatus` FROM `Users` WHERE id = '$uid'", "Users")[0];
// get stats
$summary_row = runQuery("SELECT * FROM `Variants_x_Users_Summary` WHERE vid = '$vid' AND uid = '$uid'")[0];
$summary_table =  "<p ><table id='summary_table' align=center cellspacing=0 class=w95 style='margin-top:2em;'><thead><tr><th class=top>Sample Category</th><th class=top>Homozygous Reference</th><th class=top>Heterozygous</th><th class=top>Homozygous Alternate</th><th class=top style='border-left:3px double #fff'>Inheritance</th><th class=top >#Samples</th><th class=top style='border-left:3px double #fff'>Classification</th><th class=top >#Samples</th><th class=top>Classification</th><th class=top >#Samples</th></tr></thead>";
// Overall / denovo / pathogenic & uvKL4.
$summary_table .= "<tr><td>All Samples</td><td>" . $summary_row['AllHomRef'] . "</td><td>" . $summary_row['AllHet'] . "</td><td>" . $summary_row['AllHomAlt'] . "</td><td style='border-left:3px double #fff'>De Novo</td><td>" . $summary_row['nrDeNovo'] . "</td><td style='border-left:3px double #fff'>Pathogenic</td><td>" . $summary_row['nrPatho'] . "</td><td >UVKL4</td><td>" . $summary_row['nrUVKL4'] . "</td></tr>";
// Controls.
$summary_table .= "<tr><td>Control Samples</td><td>" . $summary_row['ConHomRef'] . "</td><td>" . $summary_row['ConHet'] . "</td><td>" . $summary_row['ConHomAlt'] . "</td><td style='border-left:3px double #fff'>Maternal</td><td>" . $summary_row['nrMaternal'] . "</td><td style='border-left:3px double #fff'>UVKL3</td><td>" . $summary_row['nrUVKL3'] . "</td><td >UVKL2</td><td>" . $summary_row['nrUVKL2'] . "</td></tr>";
// Females.
$summary_table .= "<tr><td>Female Samples</td><td>" . $summary_row['FemaleHomRef'] . "</td><td>" . $summary_row['FemaleHet'] . "</td><td>" . $summary_row['FemaleHomAlt'] . "</td><td style='border-left:3px double #fff'>Paternal</td><td>" . $summary_row['nrPaternal'] . "</td><td style='border-left:3px double #fff'>Benign</td><td>" . $summary_row['nrBenign'] . "</td><td >False Positive</td><td>" . $summary_row['nrFalsePositive'] . "</td></tr>";
// Males.
$summary_table .= "<tr><td>Male Samples</td><td>" . $summary_row['MaleHomRef'] . "</td><td>" . $summary_row['MaleHet'] . "</td><td>" . $summary_row['MaleHomAlt'] . "</td><td style='border-left:3px double #fff'>BiParental</td><td>" . $summary_row['nrBiParental'] . "</td><td style='border-left:3px double #fff'>Dominant Model</td><td>" . $summary_row['nrDominant'] . "</td><td >Recessive Model</td><td>" . $summary_row['nrRecessive'] . "</td></tr>";

$summary_table .= "<tr><td colspan=10 class=last>&nbsp;</td></tr></table></p>";


//////////////////////////
// DETAILS PER GENOTYPE //
//////////////////////////
// get samples
$sids = runQuery("SELECT p.name AS pname, p.id AS pid, s.id AS sid, s.gender , s.Name AS sname FROM `Samples` s JOIN  `Projects_x_Samples` ps JOIN `Projects_x_Users` pu JOIN `Projects` p ON pu.pid = ps.pid AND ps.sid = s.id AND p.id = ps.pid WHERE pu.uid = $uid ", "Samples:Projects_x_Samples:Projects_x_Users:Projects");
if (count($sids) == 0) {
    $output = "No samples available<br/>";
    echo "$output";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";

    exit;
}

/////////////////////////
// TOP SELECTION BOXES //
/////////////////////////
echo "<div style='height:90vh;overflow-y:auto;'>";
// Three tabs, one per genotype. 
echo "<h3>Occurence overview of $variant</h3>";
// warning if summary stats are out of date
if ($sStatus['SummaryStatus'] != 2) {
    echo "<p><span class='emph red'>WARNING: Summary Statistics are not up-to-date</span>";
    //if running : do not show update action
    if ($sStatus['SummaryStatus'] != 1) {
        # not running. Queued ? 
        $squeue = explode("\n", `cat ../Query_Results/.UserSummary.queue`);
        if (!in_array($uid, $squeue)) {
            echo " <span id='updateUserSummary' class=strong onmouseover='' style='cursor: pointer;' OnClick=\"UpdateSummary('User','$uid')\">Update now.</span>";
        } else {
            # queued : inform user
            echo "<span id='italic'> (Queued for update)</span>";
        }
    } else {
        echo "<span id='italic'> (Update Running)";
    }
    echo "</p>";
}

echo "<p><table align=center class=w95><tr>";
echo "  <th onmouseover=\"this.style.cursor='pointer'\" class='overlay_big headsel' id=head_summary onClick=\"ToggleSamplesWithVariant('Summary')\">Summary</th>";
echo "  <th onmouseover=\"this.style.cursor='pointer'\" class=overlay_big id=head_HomRef onClick=\"ToggleSamplesWithVariant('HomRef','$vid','$uid')\">Homozygous Reference (" . $summary_row['AllHomRef'] . ")</th>";
echo "  <th onmouseover=\"this.style.cursor='pointer'\" id=head_Het class='overlay_big' onClick=\"ToggleSamplesWithVariant('Het','$vid','$uid')\">Heterozygous (" . $summary_row['AllHet'] . ")</th>";
echo "  <th onmouseover=\"this.style.cursor='pointer'\" class=overlay_big id=head_HomAlt onClick=\"ToggleSamplesWithVariant('HomAlt','$vid','$uid')\">Homozygous Alternate (" . $summary_row['AllHomAlt'] . ")</th>";
echo "</tr></table></p>";

// print summary table.
echo $summary_table;

// print details tables.
echo "<p >";
echo " <input id='loaded_HomRef' type=hidden value='0'/>";
echo " <input id='loaded_Het' type=hidden value='0'/>";
echo " <input id='loaded_HomAlt' type=hidden value='0'/>";
echo " <table id='details_table' style='display:none;' align=center cellspacing=0 class=w95><thead><tr><th class=top>Sample</th><th class=top>Project</th><th class=top>Gender</th><th class=top>Inheritance</th><th class=top>D.Class</th><th class=top>Ref/Alt.Depth</th><th class=top>Filter</th><th class=top>DeltaPL</th><th class=top>Stretch</th></tr></thead>";
// homref
echo "<tbody id='tbody_HomRef' style='display:none'><tr><td colspan=9 align=left ><span class=italic>&nbsp; &nbsp; -- loading --</span></td></tr></tbody>";
// het 
echo "<tbody id='tbody_Het'  style='display:none'><tr><td colspan=9 align=left ><span class=italic>&nbsp; &nbsp; -- loading --</span></td></tr></tbody>";
// hom alt
echo "<tbody id='tbody_HomAlt' style='display:none'><tr><td colspan=9 align=left ><span class=italic>&nbsp; &nbsp; -- loading --</span></td></tr></tbody>";

echo "<tr><td class=last colspan=9>&nbsp;</td></tr></table></p>";
echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
echo "</div>";
