<?php
include('../includes/inc_logging.inc');

$cbid = isset($_POST['id']) ? $_POST['id'] : '';
if ($cbid == '') {
    echo "invalid  checkbox_id provided: $cbid";
    exit;
}
$cbv = isset($_POST['v']) ? $_POST['v'] : '';
if (!is_numeric($cbv) || $cbv > 1) {
    echo "invalid checkbox value : $cbv";
    exit;
}
$pagenr = isset($_POST['p']) ? $_POST['p'] : '';
if (!is_numeric($pagenr) || $pagenr > 1) {
    echo "invalid result page : $pagenr";
    exit;
}

#######################
# CONNECT TO DATABASE # 
#######################
include('../.LoadCredentials.php');

$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$uid = isset($_SESSION['userID']) ? $_SESSION['userID'] : 'na';
if (!is_numeric($uid)) {
    echo "invalid user id provided: $uid";
    exit;
}

# todo : permissions to access & edit.
$row = runQuery("SELECT FirstName, LastName FROM `Users` WHERE id = '$uid'", "Users")[0];
$username = $row['LastName'] . " " . substr($row['FirstName'], 0, 1) . ".";
// get the page
list($vid, $set, $cblid, $item) = explode("_", $cbid, 4);

$row = array_shift(...[runQuery("SELECT `contents` FROM `Samples_x_Saved_Results_Pages` WHERE `set_id` = '$set' AND `page` = '$pagenr'", "Samples_x_Saved_Results_Pages")]);
if (count($row) == 0) {
    echo "<h3>Sample '$sid' not found. Please Report.</h3>";
    trigger_error("Sample not found... : SELECT `contents` FROM `Samples_x_Saved_Results_Pages` WHERE `set_id` = '$set' AND `page` = '$pagenr'", E_USER_ERROR);
    exit();
}
$contents = $row['contents'];

$internalErrors = libxml_use_internal_errors(true);
$doc = new DomDocument;
$doc->loadHTML($contents);
if ($cbv == 0) {
    // remove 'checked'
    if ($doc->getElementById("$cbid")) {
        $doc->getElementById("$cbid")->removeAttribute('checked');
        $doc->getElementById("$cbid")->setAttribute('title', "UnSet by $username");
    }
    //$contents = str_replace(" id='$cbid' checked>"," id='$cbid'>",$contents);
    // (re)set title
    //$contents = preg_replace("/ title='.?' id='$cbid'/"," title='UnSet by $username' id='$cbid'",$contents,1);
}
// add checked
elseif ($cbv == 1) {
    //$contents = str_replace(" id='$cbid'>"," id='$cbid' checked>",$contents);
    // (re)set title
    if ($doc->getElementById("$cbid")) {
        $doc->getElementById("$cbid")->setAttribute('title', "Set by $username");
        $doc->getElementById("$cbid")->setAttribute('checked', "checked");
    }
}
$new_contents = $doc->saveHTML();
doQuery("UPDATE `Samples_x_Saved_Results_Pages` SET `contents` = '" . SqlEscapeValues($new_contents) . "' WHERE  set_id = '$set' AND page = '$pagenr'", "Samples_x_Saved_Results_Pages");
echo "OK";
