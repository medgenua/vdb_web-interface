<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

## GET MAIN POSTED VARIABLES
if (isset($_GET['uid'])) {
    $uid = $_GET['uid'];
} else {
    exit;
}
$uids = array();
if (isset($_GET['gid'])) {
    // get users.
    $rows = runQuery("SELECT uid FROM `Usergroups_x_User` WHERE gid = '" . $_GET['gid'] . "'", "Usergroups_x_User");
    foreach ($rows as $row) {
        array_push($uids, $row['uid']);
    }
}
$ShareWhat = $_GET['ShareWhat'];
$id = $_GET['id'];

////////////
// ACTION //
////////////
if (isset($_GET['ShareWith'])) {
    if (is_numeric($_GET['ShareWith'])) {
        array_push($uids, $_GET['ShareWith']);
    }
    if ($ShareWhat == 'Filter') {
        // permission? 
        $perms = runQuery("SELECT fid FROM `Users_x_FilterSettings` WHERE fid = $id AND uid = $uid", "User_x_FilterSettings");
        if (count($perms) == 0) {
            echo "You don't have enough permissions to share this filter.";
            exit;
        }
        foreach ($uids as $uuid) {
            insertQuery("INSERT IGNORE INTO `Users_x_Shared_Filters` (uid, fid) VALUES ('$uuid','$id')", "Users_x_Shared_Filters");
        }
        if (isset($_GET['gid'])) {
            insertQuery("INSERT IGNORE INTO `Usergroups_x_Shared_Filters` (gid, fid) VALUES ('" . $_GET['gid'] . "','$id')", "Usergroups_x_Shared_Filters");
        }
        echo "1";
        exit;
    } elseif ($ShareWhat == 'Annotation') {
        // permission? 
        $query = runQuery("SELECT aid FROM `Users_x_Annotations` WHERE aid = $id AND uid = $uid", "Users_x_Annotations");
        if (count($query) == 0) {
            echo "You don't have enough permissions to share this annotation set.";
            exit;
        }
        foreach ($uids as $uuid) {
            insertQuery("INSERT IGNORE INTO `Users_x_Shared_Annotations` (uid, aid) VALUES ('$uuid','$id')", "Users_x_Shared_Annotations");
        }
        if (isset($_GET['gid'])) {
            insertQuery("INSERT IGNORE INTO `Usergroups_x_Shared_Annotations` (gid, aid) VALUES ('" . $_GET['gid'] . "','$id')", "Usergroups_x_Shared_Filters");
        }
        echo "1";
        exit;
    }
}


echo "<div class='section' style='width:68%;border:0.2em dashed #aaa;margin-left:15%;margin-right:15%;padding:1em;color:#fff;text-align:left;'>";

/////////////
// FILTERS //
/////////////
if ($ShareWhat == 'Filter') {
    // permission ? 
    $row = runQuery("SELECT fid,FilterName FROM `Users_x_FilterSettings` WHERE fid = '$id' AND uid = '$uid'", "Users_x_FilterSettings");
    if (count($row) == 0) {
        echo "<p>You don't have enough permissions to share this filter.</p>";
        echo "<p><input type=submit class=button value='Close' onClick=\"document.getElementById('overlay').style.display='none';\" /></p>";
        exit;
    }
    echo "<h3>Share Saved $ShareWhat : '" . $row['FilterName'] . "'</h3>";

    // output LEFT: current usergroups with access.
    echo "<div class='toleft' style='width:40%'>";
    echo "<h4 style='color:#efefef;'>Currently shared with:</h4>";
    // get usergroups filter is currently shared with.
    $rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug JOIN `Usergroups_x_Shared_Filters` us ON ug.id = us.gid WHERE us.fid = $id", "Usergroups:Usergroups_x_Shared_Filters");
    $shared_groups = array();
    $usergroupstring = '';
    foreach ($rows as $k => $row) {
        $shared_groups[$row['id']] = $row['name'];
        $usergroupstring .= " - " . $row['name'] . " <img src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em' onClick=\"UnShareSettings('Filter','$id','$uid','" . $row['id'] . "','group')\" /><br/>";
    }
    echo "<div style='height:15em;overflow-y:auto'><span class=emph>Usergroups:</span><br/>";
    if ($usergroupstring != '') {
        echo "$usergroupstring";
    } else {
        echo " - Not shared";
    }
    echo "</div>";
    // get users filter is currently shared with.
    $rows = runQuery("SELECT u.LastName, u.FirstName, u.id FROM `Users` u JOIN `Users_x_Shared_Filters` us ON u.id = us.uid WHERE us.fid = $id", "Users_x_Shared_Filters");
    $shared = array();
    $userstring = '';
    foreach ($rows as $k => $row) {
        $shared[$row['id']] = $row['LastName'] . ' ' . $row['FirstName'];
        $userstring .= " - " . $row['LastName'] . " " . $row['FirstName'] . " <img src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em' onClick=\"UnShareSettings('Filter','$id','$uid','" . $row['id'] . "','user')\" /><br/>";
    }
    echo "<div style='height:15em;overflow-y:auto'><span class=emph>Users:</span><br/>";
    if ($userstring != '') {
        echo "$userstring";
    } else {
        echo " - Not shared";
    }
    echo "</div>";
    echo "</div>";
    // get my affiliation.
    $row = runQuery("SELECT a.id, a.name FROM `Affiliation` a JOIN `Users` u ON u.Affiliation = a.id WHERE u.id = $uid", "Affiliation:Users")[0];
    $myaffi = $row['id'];
    $myaffiname = $row['name'];
    // output RIGHT: usergroups without access.	
    echo "<div class='toright' style='width:40%'>";
    echo "<h4 style='color:#efefef;'>Share with additional user/groups:</h4>";
    // groups
    $rows = runQuery("SELECT ug.name, ug.id, ug.affiliation,ug.opengroup FROM `Usergroups` ug ORDER BY name ASC", "Usergroups");
    $usergroupstring = '';
    foreach ($rows as $k => $row) {
        if (isset($shared_groups[$row['id']])) {
            continue;
        }
        $affis = explode(",", $row['affiliation']);
        if ($row['opengroup'] == 0 && !in_array($myaffi, $affis)) {
            continue;
        }
        $usergroupstring .= " - " . $row['name'] . " <img src='Images/layout/plus-icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareWithGroup('Filter','$id','$uid','" . $row['id'] . "')\" /><br/>";
    }
    echo "<div style='height:15em;overflow-y:auto'><span class=emph>Usergroups:</span><br/>";
    if ($usergroupstring != '') {
        echo "$usergroupstring";
    } else {
        echo " - No usergroups available";
    }
    echo "</div>";

    // users 	
    $rows = runQuery("SELECT u.LastName, u.FirstName, u.id FROM `Users` u WHERE u.id <> '$uid' AND u.Affiliation = '$myaffi' ORDER BY LastName ASC", "Users");
    $userstring = '';
    foreach ($rows as $k => $row) {
        if (!isset($shared[$row['id']])) {
            $userstring .= " - " . $row['LastName'] . " " . $row['FirstName'] . "<img src='Images/layout/plus-icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareWithUser('Filter','$id','$uid','" . $row['id'] . "')\" /><br/>";
        }
    }
    // other affiliations
    $rows = runQuery("SELECT u.LastName, u.FirstName, u.id, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id WHERE u.Affiliation <> '$myaffi' ORDER BY a.name ASC, u.LastName ASC", "Users:Affiliation");
    $lastaffi = $myaffiname;
    foreach ($rows as $k => $row) {
        if (!isset($shared[$row['id']])) {
            if ($row['name'] != $lastaffi) {
                $lastaffi = $row['name'];
                $userstring .= "<span class='italic underline'>$lastaffi</span><br/>";
            }
            $userstring .= " - " . $row['LastName'] . " " . $row['FirstName'] . "<img src='Images/layout/plus-icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareWithUser('Filter','$id','$uid','" . $row['id'] . "')\" /><br/>";
        }
    }
    echo "<div style='height:15em;overflow-y:auto'><span class=emph>Users:</span><br/>";
    if ($userstring != '') {
        echo "<span class='italic underline'>$myaffiname</span><br/>$userstring";
    } else {
        echo " - No users available";
    }
    echo "</div>";

    echo "</div>";
    echo "<br style='clear:both;'/>";
    echo "<p><input type=submit class=button value='Done' onClick=\"document.getElementById('overlay').style.display='none';\" /></p>";
}
/////////////////
// ANNOTATIONS //
/////////////////
elseif ($ShareWhat == 'Annotation') {
    // permission ? 
    $row = runQuery("SELECT aid,AnnotationName FROM `Users_x_Annotations` WHERE aid = '$id' AND uid = '$uid'", "Users_x_Annotations");
    if (count($row) == 0) {
        echo "<p>You don't have enough permissions to share this annotation set.</p>";
        echo "<p><input type=submit class=button value='Close' onClick=\"document.getElementById('overlay').style.display='none';\" /></p>";
        echo "<p>SELECT aid,AnnotationName FROM `Users_x_Annotations` WHERE aid = '$id' AND uid = '$uid'</p>";
        exit;
    }
    echo "<h3>Share Saved $ShareWhat : '" . $row['AnnotationName'] . "'</h3>";

    // output LEFT: current usergroups with access.
    echo "<div class='toleft' style='width:40%'>";
    echo "<h4 style='color:#efefef;'>Currently shared with:</h4>";
    // get users filter is currently shared with.
    $rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug JOIN `Usergroups_x_Shared_Annotations` us ON ug.id = us.gid WHERE us.aid = $id", "Usergroups:Usergroups_x_Shared_Annotations");
    $shared_groups = array();
    $usergroupstring = '';
    foreach ($rows as $k => $row) {
        $shared_groups[$row['id']] = $row['name'];
        $usergroupstring .= " - " . $row['name'] . " <img src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em' onClick=\"UnShareSettings('Annotation','$id','$uid','" . $row['id'] . "','group')\" /><br/>";
    }
    echo "<div style='height:15em;overflow-y:auto'><span class=emph>Usergroups:</span><br/>";
    if ($usergroupstring != '') {
        echo "$usergroupstring";
    } else {
        echo " - Not shared";
    }
    echo "</div>";
    // output LEFT: current users with access.
    // get users filter is currently shared with.
    $rows = runQuery("SELECT u.LastName, u.FirstName, u.id FROM `Users` u JOIN `Users_x_Shared_Annotations` us ON u.id = us.uid WHERE us.aid = $id", "Users:Users_x_Shared_Annotations");
    $shared = array();
    $userstring = '';
    foreach ($rows as $k => $row) {
        $shared[$row['id']] = $row['LastName'] . ' ' . $row['FirstName'];
        $userstring .= " - " . $row['LastName'] . " " . $row['FirstName'] . "<img src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em' onClick=\"UnShareSettings('Annotation','$id','$uid','" . $row['id'] . "','user')\" /><br/>";
    }
    echo "<div style='height:15em;overflow-y:auto'><span class=emph>Users:</span><br/>";
    if ($userstring != '') {
        echo "$userstring";
    } else {
        echo " - Not shared";
    }
    echo "</div>";

    echo "</div>";
    // get my affiliation.
    $row = runQuery("SELECT a.id, a.name FROM `Affiliation` a JOIN `Users` u ON u.Affiliation = a.id WHERE u.id = $uid", "Affiliation:Users")[0];
    $myaffi = $row['id'];
    $myaffiname = $row['name'];
    // output RIGHT: current users/groups without access.	
    echo "<div class='toright' style='width:40%'>";
    echo "<h4 style='color:#efefef;'>Share with additional users/groups:</h4>";
    // groups
    $rows = runQuery("SELECT ug.name, ug.id, ug.affiliation,ug.opengroup FROM `Usergroups` ug ORDER BY name ASC", "Usergroups");
    $usergroupstring = '';
    foreach ($rows as $k => $row) {
        if (isset($shared_groups[$row['id']])) {
            continue;
        }
        $affis = explode(",", $row['affiliation']);
        if ($row['opengroup'] == 0 && !in_array($myaffi, $affis)) {
            continue;
        }
        $usergroupstring .= " - " . $row['name'] . " <img src='Images/layout/plus-icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareWithGroup('Annotation','$id','$uid','" . $row['id'] . "')\" /><br/>";
    }
    echo "<div style='height:15em;overflow-y:auto'><span class=emph>Usergroups:</span><br/>";
    if ($usergroupstring != '') {
        echo "$usergroupstring";
    } else {
        echo " - No usergroups available";
    }
    echo "</div>";
    // users
    $rows = runQuery("SELECT u.LastName, u.FirstName, u.id FROM `Users` u WHERE u.id <> '$uid' AND u.Affiliation = '$myaffi' ORDER BY LastName", "Users");
    $userstring = '';
    foreach ($rows as $k => $row) {
        if (!isset($shared[$row['id']])) {
            $userstring .= " - " . $row['LastName'] . " " . $row['FirstName'] . "<img src='Images/layout/plus-icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareWithUser('Annotation','$id','$uid','" . $row['id'] . "')\" /><br/>";
        }
    }
    // other affiliations
    $rows = runQuery("SELECT u.LastName, u.FirstName, u.id, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id WHERE u.Affiliation <> '$myaffi' ORDER BY a.name ASC, u.LastName ASC", "Users:Affiliation");
    $lastaffi = $myaffiname;
    foreach ($rows as $k => $row) {
        if (!isset($shared[$row['id']])) {
            if ($row['name'] != $lastaffi) {
                $lastaffi = $row['name'];
                $userstring .= "<span class='italic underline'>$lastaffi</span><br/>";
            }
            $userstring .= " - " . $row['LastName'] . " " . $row['FirstName'] . "<img src='Images/layout/plus-icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareWithUser('Annotation','$id','$uid','" . $row['id'] . "')\" /><br/>";
        }
    }
    echo "<div style='height:15em;overflow-y:auto'><span class=emph>Users:</span><br/>";

    if ($userstring != '') {
        echo "<span class='italic underline'>$myaffiname</span><br/>$userstring";
    } else {
        echo " - No users available";
    }
    echo "</div>";

    echo "</div>";
    echo "<br style='clear:both;'/>";
    echo "<p><input type=submit class=button value='Done' onClick=\"document.getElementById('overlay').style.display='none';\" /></p>";
}

//echo "1";
