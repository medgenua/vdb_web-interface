<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

## GET MAIN POSTED VARIABLES
$uid = $_GET['uid'];
$DeleteWhat = $_GET['DeleteWhat'];
$id = $_GET['id'];

if ($DeleteWhat == 'Filter') {
    // in a section?
    $rows = runQuery("SELECT Name FROM `Report_Sections` WHERE `FilterSet` = '$id'", "Report_Sections");
    $used_in = '';
    foreach ($rows as $k => $row) {
        $used_in .= " - " . $row['Name'] . "\n";
    }
    if ($used_in != '') {
        echo "ERROR: Cannot Remove Filter, it is included in report sections:\n$used_in";
        exit;
    }
    // private filter.
    $rows = runQuery("SELECT fid FROM `Users_x_FilterSettings` WHERE fid = '$id' AND uid = '$uid'", "Users_x_FilterSettings");
    if (count($rows)  > 0) {
        doQuery("DELETE FROM `Users_x_FilterSettings` WHERE fid = '$id' AND uid = '$uid'", "Users_x_FilterSettings");
        // shared with other users?
        doQuery("DELETE FROM `Users_x_Shared_Filters` WHERE fid = '$id'", "Users_x_Shared_Filters");
        // shared with usergroup?
        doQuery("DELETE FROM `Usergroups_x_Shared_Filters` WHERE fid = '$id'", "Usergroups_x_Shared_Filters");
    }
    // shared filter
    else {
        $rows = runQuery("SELECT fid FROM `Users_x_Shared_Filters` WHERE fid = '$id' AND uid = '$uid'", "Users_x_Shared_Filters");
        if (count($rows) > 0) {
            doQuery("DELETE FROM `Users_x_Shared_Filters` WHERE fid = '$id' AND uid = '$uid'", "Users_x_Shared_Filters");
        }
    }
} elseif ($DeleteWhat == 'Annotation') {
    $all_in_sections = array();
    $rows = runQuery("SELECT `rsid`,`Name`, `Annotations` FROM `Report_Sections`", "Annotations");
    foreach ($rows as $k => $row) {
        $sids = explode(",", $row['Annotations']);
        foreach ($sids as $sid) {
            $all_in_sections[$sid][$row['rsid']] = $row['Name'];
        }
    }
    // in a section?
    if (array_key_exists($id, $all_in_sections)) {
        $used_in = implode("\n", array_values($all_in_sections[$id]));
        echo "ERROR: Cannot Remove Annotation Set, it is included in report sections:\n$used_in";
        exit;
    }
    // private.
    $rows = runQuery("SELECT aid FROM `Users_x_Annotations` WHERE aid = '$id' AND uid = '$uid'", "Users_x_Annotations");
    if (count($rows) > 0) {
        doQuery("DELETE FROM `Users_x_Annotations` WHERE aid = '$id' AND uid = '$uid'", "Users_x_Annotations");
        // shared with other users?
        doQuery("DELETE FROM `Users_x_Shared_Annotations` WHERE aid = '$id'", "Users_x_Shared_Annotations");
        // shared with usergroup?
        doQuery("DELETE FROM `Usergroups_x_Shared_Annotations` WHERE aid = '$id'", "Usergroups_x_Shared_Annotations");
    }
    // shared
    else {
        $rows = runQuery("SELECT aid FROM `Users_x_Shared_Annotations` WHERE aid = '$id' AND uid = '$uid'", "Users_x_Shared_Annotations");
        if (count($rows) > 0) {
            doQuery("DELETE FROM `Users_x_Shared_Annotations` WHERE aid = '$id' AND uid = '$uid'", "Users_x_Shared_Annotations");
        }
    }
}

echo "1";
