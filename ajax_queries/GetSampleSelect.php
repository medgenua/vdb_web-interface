<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');

$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_GET['uid'];
$setlink = 0;
if (isset($_GET['setlink'])) {
    $setlink = $_GET['setlink'];
}
if ($setlink == 1) {
    // in filtering page, load some info. 
    $action = "OnChange=\"CheckDataFiles($userid,this.options[this.selectedIndex].value);LoadCustomAnnotations('sid_list',$userid)\"";
}

require("../includes/inc_query_functions.inc");

// get samples ; from provided project .
$pid_query = (isset($_GET['pid'])) ? "AND ps.pid = '" . $_GET['pid'] . "'" : "";
$rows = runQuery("SELECT p.Name AS pname, s.id, s.Name FROM `Samples` s JOIN  `Projects_x_Samples` ps JOIN `Projects_x_Users` pu JOIN `Projects` p ON pu.pid = ps.pid AND ps.sid = s.id AND p.id = ps.pid WHERE pu.uid = $userid $pid_query ORDER BY p.name ASC, s.Name ASC", "Samples:Projects_x_Samples:Projects_x_Users:Projects");
$output = '';
// no samples found, except when 'PID' is empty
if (count($rows) == 0 && (isset($_GET['pid']) && $_GET['pid'] != '')) {
    $output = "No samples available";
} else {
    $currgroup = '';
    // project is not always provided. 
    if (isset($_GET['pid'])) {
        // two rows : first is project selection, second is samples in that project.: 
        $projects = runQuery("SELECT p.Name, p.id FROM `Projects` p JOIN `Projects_x_Users` pu ON pu.pid = p.id WHERE pu.uid = '$userid' ORDER BY p.Name", 'Projects_x_Users:Projects');
        $output .= "Projects : <select name='projectid' id='projectid' style='width:92%' onChange='UpdateProjectSamples($userid)'>";
        // request for project-based listing, but none provided yet.
        if ($_GET['pid'] == '') {
            $output .= "<option value='' SELECTED disabled='disabled'></option>";
        }
        foreach ($projects as $project) {
            $selected = ($project['id'] == $_GET['pid']) ? 'SELECTED' : '';
            $output .= "<option value='" . $project['id'] . "' $selected>" . $project['Name'] . "</option>";
        }
        $output .= "</select><br/>";
        $output .= "Samples: <select name=sampleid id=sampleid $action style='width:92%'><option value='' SELECTED disabled='disabled'></option>";
    } else {
        $output = "<select name=sampleid id=sampleid $action style='width:98%'><option value='' SELECTED disabled='disabled'></option>";
    }
    foreach ($rows as $k => $row) {
        if ($currgroup != $row['pname']) {
            if ($currgroup != '') {
                $output .= "</optgroup>";
            }
            $output .= "<optgroup label='Project: " . $row['pname'] . "'>";
            $currgroup = $row['pname'];
        }
        $output .= "<option value='" . $row['id'] . "'>" . $row['Name'] . "</option>";
    }
    echo "</optgroup>";
    $output .= "</select>";
}
echo "$output";
