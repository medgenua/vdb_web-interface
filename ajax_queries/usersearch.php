<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];

$input = strtolower($_GET['input']);
$len = strlen($input);
$limit = isset($_GET['limit']) ? (int) $_GET['limit'] : 10;

require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

$aResults = array();
$count = 0;

$uid = $_SESSION['userID'];

if ($len) {
    // fetch results
    $rows = runQuery("SELECT u.id, u.LastName, u.FirstName FROM `Users` u WHERE u.LastName LIKE '%$input%' OR u.FirstName LIKE '%$input%' LIMIT $limit", "Users");
    foreach ($rows as $k => $row) {
        $id = $row['id'];
        $LastName = $row['LastName'];
        $FirstName = $row['FirstName'];
        $aResults[] = array("id" => $id, "value" => "$LastName $FirstName");
    }
}





header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0



if (isset($_REQUEST['json'])) {
    header("Content-Type: application/json");

    echo "{\"results\": [";
    $arr = array();
    for ($i = 0; $i < count($aResults); $i++) {
        $arr[] = "{\"id\": \"" . $aResults[$i]['id'] . "\", \"value\": \"" . $aResults[$i]['value'] . "\", \"info\": \"\"}";
    }
    echo implode(", ", $arr);
    echo "]}";
} else {
    header("Content-Type: text/xml");

    echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?><results>";
    for ($i = 0; $i < count($aResults); $i++) {
        echo "<rs id=\"" . $aResults[$i]['id'] . "\" info=\"" . $aResults[$i]['info'] . "\">" . $aResults[$i]['value'] . "</rs>";
    }
    echo "</results>";
}
