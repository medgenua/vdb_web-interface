<?php
include('../includes/inc_logging.inc');

$sid = $_GET['sid'];
if (!is_numeric($sid)) {
    echo "Invalid Sample ID";
    exit;
}
#######################
# CONNECT TO DATABASE # ASC
#######################
include('../.LoadCredentials.php');

$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$uid = $_SESSION['userID'];

if (!is_numeric($uid)) {
    echo "invalid userid : '$uid'";
    exit;
}
// Get sample name
$row = runQuery("SELECT Name FROM `Samples` WHERE id = '$sid'", "Samples");
if (count($row) == 0) {
    echo "<h3>Sample '$sid' not found.</h3>";
    exit();
}
$sname = $row[0]['Name'];

// get access : altering filters requires editsample rights
$row = runQuery("SELECT pu.editsample FROM Projects_x_Users pu JOIN Projects_x_Samples ps ON ps.pid = pu.pid WHERE ps.sid = '$sid' AND pu.uid = '$uid'", "Projects_x_Samples:Projects_x_Users");
$edit_rights = $row['0']['editsample'];

echo "<h3>Stored filtering results for sample '$sname'</h3>";

$rows = runQuery("SELECT l.date, l.set_name, l.set_comments, u.LastName, u.FirstName, l.set_vids, l.set_id,l.Validated, l.Validation_Comments, vu.LastName AS vuLN, vu.FirstName AS vuFN, l.deleted, l.delete_comments, l.delete_uid,l.uid,l.validation_uid FROM  (`Users` u  JOIN `Samples_x_Saved_Results` l  ON  l.uid = u.id ) LEFT  JOIN `Users` vu ON l.validation_uid = vu.id WHERE l.sid = '$sid' ORDER BY l.set_id DESC", "Samples_x_Saved_Results:Users");

if (count($rows) == 0) {
    echo "<p>No entries present.</p>";
    exit();
}

// track variant sets, put into seperate tables.
$nr_live = 0;
$live_table = '';
$del_table = '';
$nr_del = 0;
// process
foreach ($rows as $k => $row) {
    // set vids can be queued, running, or a list of vids.
    $nrv = ($row['set_vids'] == 'Queued' || $row['set_vids'] == 'Running' || $row['set_vids'] == '0') ? $row['set_vids'] : count(explode(",", $row['set_vids']));
    $setid = $row['set_id'];
    $set_comments = ($row['set_comments'] == '') ? 'S: -' : 'S: ' . stripslashes($row['set_comments']);

    // no variants : reset nrVids
    if ($row['set_vids'] == '') {
        $nrv = 0;
    }
    $line = "<tr>";
    if (is_numeric($nrv)) {
        $line .= "<td><img title='Load' src='Images/layout/eye.png' style='height:0.7em' onClick='ShowStoredResults($uid,$sid,$setid,1)'> ";
        $nrv .= " variants";
    } else {
        $line .= "<td><img title='Load' src='Images/layout/eye_gray.png' style='height:0.7em'> ";
    }
    $line .= "<img title='Show Log' src='Images/layout/log.png' style='height:0.7em;' onClick=\"LoadStoredQueryLog('$uid','$setid')\"/> <img title='Re-Run' src='Images/layout/redo.png' style='height:0.9em' onClick='ReRunStoredResult($uid,$sid,$setid)'> ";
    // deletion of validated sets requires extra confirmation and permissions.
    if ($row['Validated'] == 0) {
        if ($edit_rights == 1) {
            $line .= "<img title='Delete' src='Images/layout/icon_trash.gif' style='height:0.85em' onClick='DeleteStoredResults($uid,$setid,$sid)'> ";
            $line .= "<img title='Validate' src='Images/layout/validate.png' style='height:0.9em' onClick='ValidateSavedResults($uid,$setid,$sid)'> ";
        }
        if ($row['Validation_Comments'] == '') {
            $val = '';
            $val_comments = '';
        } else {
            $val = "<td  class='red italic'>Validation undone</td>";
            $val_comments = '<br/>V: ' . stripslashes($row['Validation_Comments']);
        }
    } else {
        // deleting is only allowed for the validator user.	
        if ($row['deleted'] == 0 && $uid == $row['validation_uid']) {
            $line .= "<img title='Delete' src='Images/layout/icon_trash.gif' style='height:0.85em' onClick='DeleteValidatedResults($uid,$setid,$sid)'> ";
        }

        $val = '<td >' . $row['vuLN'] . ', ' . substr($row['vuFN'], 0, 1) . '</td>';
        $val_comments = ($row['Validation_Comments'] == '') ? "<br/>V: -" : "<br/>V: " . stripslashes($row['Validation_Comments']);
    }

    // remainder : distiction between deleted items and non-deleted items.
    if ($row['deleted'] == 0) {
        $line .= "</td>";
        $line .= "<td> " . stripslashes($row['set_name']) . "</td>";
        $line .= "<td>$set_comments $val_comments</td>";
        $line .= "<td>" . $row['date'] . "</td>";
        $line .= "<td>" . $row['LastName'] . ", " . substr($row['FirstName'], 0, 1) . ".</td>";
        $line .= "<td>$nrv</td>";
        $line .= $val;
        $line .= "</tr>";
        $live_table .= $line;
    } else {
        // who deleted it, and why?
        $urow = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '" . $row['delete_uid'] . "'", "Users")[0];
        $duser = $urow['LastName'] . ", " . substr($urow['FirstName'], 0, 1) . ".";
        $line .= "<img title='Restore Saved Result' src='Images/layout/undelete.png' style='height:1.1em;' onClick='UndeleteSavedResults($uid,$setid,$sid)'> ";
        $line .= "</td><td> " . stripslashes($row['set_name']) . "</td>";
        $line .= "<td>$set_comments $val_comments</td>";
        $line .= "<td>$nrv</td>";
        $line .= $val;
        $line .= "<td>$duser</td>";
        $line .= "<td>" . stripslashes($row['delete_comments']) . "</td>";
        $line .= "</tr>";
        $del_table .= $line;
        $nr_del += 1;
    }
}
if ($live_table != '') {
    echo "<table cellspacing=0 class=w100 style='padding-left:1em'>";
    echo "<tr><th class=top>Actions</th><th class=top>Set Name</th><th class=top title='(S)aving and (V)alidation Comments'>Comments</th><th class=top>Date</th><th class=top>Saved By</th><th class=top>Nr.Variants</th><th class=top>Validated By</th></tr>";
    echo $live_table;
    echo "</table></p>";
}
if ($del_table != '') {
    echo "<p id='ShowDelSets' onmouseover=\"this.style.cursor='pointer'\" onclick='ShowDeletedSets()'> Show $nr_del validated but deleted result sets</p>";
    echo "<table style='display:none;padding-left:1em;' id=DeletedResults cellspacing=0 class=w100 >";
    echo "<tr><th class=top>Actions</th><th class=top>Set Name</th><th class=top title='(S)aving and (V)alidation Comments'>Comments</th><th class=top>Nr.Variants</th><th class=top>Validated By</th><th class=top>Deleted By</th><th class=top>Delete Comments</th></tr>$del_table</table>";
}
if ($live_table == '' && $del_table == '') {
    echo "<p>No entries present.</p>";
}
