<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_GET['uid'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// get samples
$rows = runQuery("SELECT p.Name AS pname, p.id FROM `Projects_x_Users` pu JOIN `Projects` p ON pu.pid = p.id WHERE pu.uid = $userid ORDER BY p.name ASC", "Projects_x_Users:Projects");
$pid = '';
if (array_key_exists("pid", $_GET)) {
    $pid = $_GET['pid'];
}
if (count($rows) == 0) {
    $output = "No projects available";
} else {
    $output = "<input type=hidden name=sampleid id=sampleid value='project'><select onChange=\"SetProjectCookie();LoadCustomAnnotations('pid',$userid)\" name=projectsearch id=searchfield style='width:98%'><option value='' SELECTED disabled='disabled'></option>";
    foreach ($rows as $k => $row) {
        $s = '';
        if ($row['id'] == $pid) {
            $s = "selected";
        }
        $output .= "<option value='" . $row['id'] . "' $s>" . $row['pname'] . "</option>";
    }
    $output .= "</select>";
}
echo "$output";
