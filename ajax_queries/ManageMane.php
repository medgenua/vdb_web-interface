<?php
// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}

$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_SESSION['userID'];
// get set_id
$set = $_POST['set'];
if (!is_numeric($set)) {
    echo "invalid set-id given.";
    exit();
}
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// access ?
$access = runQuery("SELECT `set_id` FROM `TranscriptSets_x_Users` WHERE `set_id` = '$set' AND `uid` = '$userid' AND `edit` = 1", "TranscriptSets_x_Users");
if (count($access) == 0) {
    echo "Access Denied.";
    exit();
}

if ($_POST['action'] == 'd') {
    $nmid = $_POST['nm'];
    if (!preg_match('/^[A-Z]{2}_\d+(\.\d+)*$/', $nmid)) {
        echo "$nmid : Invalid format: Transcript must follow NM_012345.1 syntax (two prefix chars, version is optional).";
        exit();
    }
    $res = doQuery("DELETE FROM `TranscriptSets_Data` WHERE set_id = '$set' AND nm_id = '$nmid'", "TranscriptSets_Data");
    if ($res) {
        echo "OK";
    } else {
        echo "Failed to delete transcript from database. Please report";
    }
    exit();
}
// change type
elseif ($_POST['action'] == 't') {
    $nm_type = $_POST['type'];
    if (!is_numeric($nm_type)) {
        echo "Invalid type provided. Must be numeric.";
        exit();
    }
    $nmid = $_POST['nm'];
    if (!preg_match('/^[A-Z]{2}_\d+(\.\d+)*$/', $nmid)) {
        echo "$nmid : Invalid format: Transcript must follow NM_012345.1 syntax (two prefix chars, version is optional).";
        exit();
    }
    $res = doQuery("UPDATE `TranscriptSets_Data` SET `favourite` = '$nm_type' WHERE `set_id` = '$set' AND `nm_id` = '$nmid'", "TranscriptSets_Data");
    if (!$res) {
        echo "Could not update database entry. Please report";
        exit();
    } else {
        echo "OK";
    }
}
// update comment
elseif ($_POST['action'] == 'c') {
    // new comment
    $nm_comment = $mysqli->real_escape_string($_POST['comment']);
    // transcript
    $nmid = $_POST['nm'];
    if (!preg_match('/^[A-Z]{2}_\d+(\.\d+)*$/', $nmid)) {
        echo "$nmid : Invalid format: Transcript must follow NM_012345.1 syntax (two prefix chars, version is optional).";
        exit();
    }
    $res = doQuery("UPDATE `TranscriptSets_Data` SET `Comments` = '$nm_comment' WHERE `set_id` = '$set' AND `nm_id` = '$nmid'", "TranscriptSets_Data");
    if (!$res) {
        echo "Could not update database entry. Please report";
        exit();
    } else {
        echo "OK";
    }
}
