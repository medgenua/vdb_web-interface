<?php
// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}

$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_SESSION['userID'];
// get sampleID
$sid = $_GET['sid'];
if (!is_numeric($sid)) {
    echo "invalid sid given.";
    exit();
}
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

if ($_GET['action'] == 'delete') {
    $tid = $_GET['tid'];
    doQuery("DELETE FROM `Samples_x_HPO_Terms` WHERE sid = '$sid' AND tid = '$tid'", "Samples_x_HPO_Tems");
} elseif ($_GET['action'] == 'LoadTerms') {
    $limit = 15;
    $term = strtolower($_GET['term']);
    $rows = runQuery("SELECT t.id, t.Name, GROUP_CONCAT(s.Synonym SEPARATOR ', ') AS Synonyms FROM `HPO_Terms` t JOIN `HPO_Synonyms` s ON t.id = s.tid WHERE Synonym LIKE '%$term%' GROUP BY t.id ORDER BY t.Name LIMIT $limit", "HPO_Terms:HPO_Synonyms");
    $valid = 0;
    $output = '';
    foreach ($rows as $k => $row) {
        if ($row['id'] == null) {
            continue;
        }
        $valid++;
        $tid = $row['id'];
        $Name = strtolower($row['Name']);
        $Synonyms = strtolower($row['Synonyms']);
        $Synonyms = preg_replace("/$Name,*/", "", $Synonyms);
        $Name = preg_replace("/($term)/", "<span class=red>$1</span>", $Name);
        $Synonyms = preg_replace("/($term)/", "<span class=red>$1</span>", $Synonyms);
        $output .= "<tr style='vertical-align:top'><td style='padding-bottom:0.5em;'><span  onClick=\"SetHPO('$sid','$tid')\"><img src='Images/layout/plus-icon.png' style='height:1em;'></span><td>$Name</td><td style='padding-left:1.5em;padding-bottom:0.5em;'>$Synonyms</td></tr>";
    }
    if ($output != '') {
        echo "<p><table cellspacing=0 style='text-align:left;margin:auto;'><tr style='vertical-align:top'><th class=top style='color:#fff;width:2.5em;'>Add</th><th class=top style='color:#fff'>HPO Term</th><th class=top style='color:#fff;padding-left:1.5em;'>Synonyms</th></tr>";
        echo $output;
        echo "<tr><td colspan=3 class=last>&nbsp;</td></tr></table></p>";
    } else {
        echo "<p>No matching terms found.</p>";
    }
} elseif ($_GET['action'] == 'add') {
    $tid = $_GET['tid'];
    $row = runQuery("SELECT sid FROM `Samples_x_HPO_Terms` WHERE sid = '$sid' AND tid = '$tid'", "Samples_x_HPO_Terms");
    if (count($row) > 0) {
        echo "exists";
    } else {
        $row = runQuery("SELECT t.Name, t.Definition, tt.tid2, GROUP_CONCAT(tb.Name SEPARATOR ', ') AS MemberOf FROM `HPO_Terms` t JOIN `HPO_Term_x_Term` tt JOIN `HPO_Terms` tb ON t.id = tt.tid1 AND tt.tid2 = tb.id WHERE t.id = '$tid' GROUP BY t.id ", "HPO_Terms:HPO_Term_x_Term:HPO_Terms")[0];
        $Name = $row['Name'];
        $Definition = $row['Definition'];
        $MemberOf = $row['MemberOf'];
        $output = "<span title='Remove Phenotype' onClick=\"DelPhenotype('$sid','$tid')\"><img src='Images/layout/icon_trash.gif' style='height:0.8em;'></span> ";
        $output .= "$Name:#:";
        $output .= "$Definition:#:$MemberOf";

        insertQuery("INSERT INTO `Samples_x_HPO_Terms` (`sid`,`tid`) VALUES ('$sid','$tid')", "Samples_x_HPO_Terms");
        ## semi-array seperated by :#: to be split in javascript.
        echo $output;
    }
}
