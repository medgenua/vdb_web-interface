<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');

$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_GET['uid'];
$sid = $_GET['sid'];
$type = $_GET['type'] or die('Type not specified');

require("../includes/inc_query_functions.inc");

## check access of user to sample.

## check if files are available, present link if so
if ($type == 'check') {
    echo "<iframe style='display:none;width:5;height:5' id='downloadframe' src='page_404.php' ></iframe>";
    ## path is in ajaxy_queries => add ../
    ## datadir is set in .LoadCredentials.php
    $bam = "$datadir/$sid.bam";
    $cram = "$datadir/$sid.cram";
    $vcf = "$datadir/$sid.vcf.gz";
    $dp = 0;
    $downloads = '';
    if (file_exists($bam) || file_exists($cram) || file_exists($vcf)) {
        echo "IGV : <a href='javascript:void(0)' onClick=\"GetDataFiles('$userid','$sid')\">Load data</a>";
        $downloads = "<br/>Download :";
    }
    if (file_exists($bam)) {
        $downloads .= " <a href='download.php?file=$sid.bam' target=downloadframe>bam</a>,<a href='download.php?file=$sid.bam.bai' target=downloadframe>bai</a>,";
    }
    if (file_exists($cram)) {
        $downloads .= " <a href='download.php?file=$sid.cram' target=downloadframe>cram</a>,<a href='download.php?file=$sid.cram.crai' target=downloadframe>crai</a>,";
    }
    if (file_exists($vcf)) {
        $downloads .= " <a href='download.php?file=$sid.vcf.gz' target=downloadframe>vcf.gz</a>, <a href='download.php?file=$sid.vcf.gz.tbi' target=downloadframe>vcf.gz.tbi</a>,";
    }
    if ($downloads != '') {
        echo substr($downloads, 0, -1);
        echo "<div id='IGVlink' style='display:none;'></div>";
    } else {
        echo "BAM/CRAM/VCF not available";
    }
} elseif ($type == 'download') { // is actually : send to IGV.
    ## $datadir is set in .LoadCredentials.php
    $bam = "$datadir/$sid.bam";
    $cram = "$datadir/$sid.cram";
    $vcf = "$datadir/$sid.vcf.gz";
    ## get samplename from DB
    $row = runQuery("SELECT Name FROM `Samples` WHERE id = '$sid'", "Samples")[0];
    $samplename = $row['Name'];
    ## this is the link to the Storage directory.
    if (isset($config['IGVdata'])) {
        $baseurl = $config['IGVdata'] . "/";
    } else {
        $baseurl = "http://biominas002.biomina.be/~gvandeweyer/";
    }
    $url = "http://localhost:60151/load?merge=true";
    $files = "";
    $names = "";
    if (file_exists($bam)) {
        $files .= "$baseurl$sid.bam,";
        $names .= "$samplename.Reads,";
    }
    if (file_exists($cram)) {
        $files .= "$baseurl$sid.cram,";
        $names .= "$samplename.Reads,";
    }
    if (file_exists($vcf)) {
        $files .= "$baseurl$sid.vcf.gz,";
        $names .= "$samplename.Variants,";
    }
    $url .= "&file=" . substr($files, 0, -1) . "&name=" . substr($names, 0, -1);
    echo $url;
}
