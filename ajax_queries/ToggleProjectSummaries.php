<?php

#######################
# CONNECT TO DATABASE # ASC
#######################
include('../.LoadCredentials.php');

$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
require("../includes/inc_filter_functions.inc");
include('../includes/inc_logging.inc');


$uid = $_SESSION['userID'];
$pid = $_GET['pid'];
$result = array('status' => 'ok');
if (!is_numeric($pid)) {
    $result['status'] = 'Error';
    $result['msg'] =  "Invalid Project ID : $pid";
    echo json_encode($result);
    exit;
}

// get current status
$row = runQuery("SELECT `summarize` FROM `Projects` WHERE id = '$pid'", "Projects")[0];
if ($row['summarize'] == 0) {
    doQuery("UPDATE `Projects` SET `summarize` = 1 WHERE id = '$pid'");
    $result['new_status'] = 1;
} else {
    doQuery("UPDATE `Projects` SET `summarize` = 0 WHERE id = '$pid'");
    $result['new_status'] = 0;
}

// set summary trigger for all users with access.
doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id IN (SELECT `uid` FROM `Projects_x_Users` WHERE pid = '$pid')");
echo (json_encode($result));
//echo stripslashes($row[$get]);
exit;
