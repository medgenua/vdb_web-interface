<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];

$input = strtolower($_GET['input']);
$len = strlen($input);
$limit = isset($_GET['limit']) ? (int) $_GET['limit'] : 10;

require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

$aResults = array();
$count = 0;

if (!isset($_SESSION['userID'])) {
    trigger_error("User session expired / not logged in", E_USER_NOTICE);
    $aResults[] = array("id" => 0, "value" => "Please log in first !");
} elseif ($len) {
    $uid = $_SESSION['userID'];
    // fetch results
    $rows = runQuery("SELECT s.id, s.Name,p.Name AS pname FROM `Samples` s JOIN  `Projects_x_Samples` ps JOIN `Projects_x_Users` pu JOIN `Projects` p ON p.id = pu.pid AND pu.pid = ps.pid AND ps.sid = s.id WHERE pu.uid = '$uid' AND s.Name LIKE '%$input%' ORDER BY s.Name LIMIT $limit", "Samples:Projects_x_Samples:Projects_x_Users:Projects");
    foreach ($rows as $k => $row) {
        $sid = $row['id'];
        $samplename = $row['Name'];
        $pname = $row['pname'];
        $aResults[] = array("id" => $sid, "value" => "$samplename  --  project: $pname");
    }
}





header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0



if (isset($_REQUEST['json'])) {
    header("Content-Type: application/json");

    echo "{\"results\": [";
    $arr = array();
    for ($i = 0; $i < count($aResults); $i++) {
        $arr[] = "{\"id\": \"" . $aResults[$i]['id'] . "\", \"value\": \"" . $aResults[$i]['value'] . "\", \"info\": \"\"}";
    }
    echo implode(", ", $arr);
    echo "]}";
} else {
    header("Content-Type: text/xml");

    echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?><results>";
    for ($i = 0; $i < count($aResults); $i++) {
        echo "<rs id=\"" . $aResults[$i]['id'] . "\" info=\"" . $aResults[$i]['info'] . "\">" . $aResults[$i]['value'] . "</rs>";
    }
    echo "</results>";
}
