<?php
include('../includes/inc_logging.inc');

$uid = (isset($_GET['uid']) ? $_GET['uid'] : 'none');
$set = (isset($_GET['setid']) ? $_GET['setid'] : 'none');

if (!is_numeric($uid)) {
    echo "<h3>ERROR : invalid userid</h3>";
    echo "<p>Your session might have expired. Please re-login and try again. Otherwise, please report.</p>";
    exit;
}

if (!is_numeric($set)) {
    echo "<h3>ERROR : invalid setid</h3>";
    echo "<p>Please Report.</p>";
    exit;
}

// load DB connection. 
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

// access ? 
$sr = array_shift(...[runQuery("SELECT ps.sid, ss.`query_log`,ss.`set_name` FROM `Samples_x_Saved_Results` ss JOIN `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ss.sid = ps.sid AND ps.pid = pu.pid WHERE ss.`set_id` = '$set' AND pu.uid = '$uid'", "Samples_x_Saved_Results:Projects_x_Samples:Projects_x_Users")]);
if (count($sr) == 0) {
    echo "<h3>ERROR : Access denied</h3>";
    echo "<p>You don't have access to this sample.</p>";
    print_r($sr);
    exit;
}

// ok, get log.
echo "<h3>Query Log for " . $sr['set_name'] . "</h3>";

if ($sr['query_log'] === '') {

    echo "<p> Query Log is empty. Please report if this is unexpected.</p>";
} else {
    echo "<pre id='queryLog'>" . $sr['query_log'] . "</pre>";
}
exit;
