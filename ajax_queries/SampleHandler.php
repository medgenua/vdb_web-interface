<?php

#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_GET['uid'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

$action = $_GET['a'];
$yesno = array("1" => 'Yes', '0' => 'No');

// DELETE
if ($action == 'd') {
    $sids = explode(",", $_GET['s']);
    $sidstr = '';
    $error = '';
    $ok = '';
    // double check permissions.
    foreach ($sids as $sid) {
        if (!is_numeric($sid)) {
            continue;
        }
        $row = array_shift(...[runQuery("SELECT s.Name, pu.editsample FROM `Samples` s JOIN `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON s.id = ps.sid AND ps.pid = pu.pid WHERE ps.sid = '$sid' AND pu.uid = '$userid'", "Samples:Projects_x_Users:Projects_x_Samples")]);
        if (count($row) == 0) {
            $error .= "<li>Invalid SampleID: $sid</li>";
            continue;
        }
        if ($row['editsample'] == 1) {
            $sidstr .= "$sid,";
            $ok .= "<li>" . $row['Name'] . "</li>";
        } else {
            $error .= "<li>" . $row['Name'] . "</li>";
        }
    }
    if ($sidstr == '') {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
        echo "<p>Invalid SampleIDs provided, or no access to the selected samples.</p>";
        echo "<input type=button name='Close' value='Close' onClick=\"document.getElementById('overlay').style.display='none'\">";
        echo "</div>";
        exit;
    }
    $sidstr = substr($sidstr, 0, -1);

    echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
    echo "<h3 style='text-align:left;'>Confirmation Needed</h3>";
    echo "<p  style='text-align:left;'>Please confirm that the following samples should be deleted?</p>";
    echo "<p  style='text-align:left;'><ol class=twocol>";
    echo "$ok";
    echo "</ol></p>";
    if ($error != '') {
        echo "<p  style='text-align:left;'>The following samples cannot be deleted due to insufficient permissions:<ol class=twocol>";
        echo "$error";
        echo "</ol></p>";
    }
    echo "<form action='' method=GET>";
    echo "<input type=hidden id='todelete' value='$sidstr'>";
    echo "<p  style='text-align:left;'><input type=button name='DeleteYes' value='Yes, Delete Samples' OnClick='ConfirmedDeleteSamples($userid)'> &nbsp; <input type=button name='DeleteNo' value='No, Keep Them' onClick=\"document.getElementById('overlay').style.display='none'\"></form></p>";
    echo "</div>";
    exit;
}
// confirmed deletion => actual deletion !
if ($action == 'cd') {
    $sids = explode(",", $_GET['s']);
    $sidstr = '';
    $error = '';
    $ok = '';
    // double check permissions.
    foreach ($sids as $sid) {
        if (!is_numeric($sid)) {
            continue;
        }
        $row = array_shift(...[runQuery("SELECT s.Name, pu.editsample FROM `Samples` s JOIN `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON s.id = ps.sid AND ps.pid = pu.pid WHERE ps.sid = '$sid' AND pu.uid = '$userid'", "Samples:Projects_x_Samples:Projects_x_Users")]);
        if (count($row) == 0) {
            $error .= "<li>Invalid SampleID: $sid</li>";
            continue;
        }

        if ($row['editsample'] == 1) {
            $sidstr .= "$sid,";
            $ok .= "<li>" . $row['Name'] . "</li>";
        } else {
            $error .= "<li>" . $row['Name'] . "</li>";
        }
    }
    if ($sidstr == '') {
        print "0";
        exit;
    }
    $sidstr = substr($sidstr, 0, -1);
    // get involved projects
    $rows = runQuery("SELECT DISTINCT(pid) FROM `Projects_x_Samples` WHERE sid IN ($sidstr)", "Projects_x_Samples");
    $pids = array();
    foreach ($rows as $k => $row) {
        $pids[$row['pid']] = 1;
        // update summarystatus.
        doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id = '" . $row['pid'] . "'", "Projects:Summary");
        // get users with access.
        $urows = runQuery("SELECT `uid` FROM `Projects_x_Users` WHERE `pid` = '" . $row['pid'] . "'", "Projects_x_Users");
        foreach ($urows as $urow) {
            $thisuid = $urow['uid'];
            doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$thisuid'", "Users:Summary");
        }
    }


    // DELETE THE QUICK STUFF HERE 
    doQuery("DELETE FROM `Projects_x_Samples` WHERE sid IN ($sidstr)", "Projects_x_Samples:Summary");
    // delete empty projects
    foreach ($pids as $pid => $dummy) {
        if ($pid == '') {
            continue;
        }
        // get number of samples (remaining => do not use the cache)
        $rows = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$pid'", "Projects_x_Samples");
        if (count($rows) == 0) {
            // delete from projects_x_usergroups
            doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$pid'", "Projects_x_Usergroups");
            // delete from projects_x_users
            doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$pid'", "Projects_x_Users");
            // delete from projects
            doQuery("DELETE FROM `Projects` WHERE id = '$pid'", "Projects");
        }
    }
    // delete from samples
    doQuery("DELETE FROM `Samples` WHERE id IN ($sidstr)", "Samples");
    // delete from samples_x_HPO
    doQuery("DELETE FROM `Samples_x_HPO_Terms` WHERE sid IN ($sidstr)", "Samples_x_HPO_Terms");
    // delete from Samples_x_Samples
    doQuery("DELETE FROM `Samples_x_Samples` WHERE sid1 IN ($sidstr)", "Samples_x_Samples");
    doQuery("DELETE FROM `Samples_x_Samples` WHERE sid2 IN ($sidstr)", "Samples_x_Samples");
    // delete from samples_x_Saved_Results*
    $srows = runQuery("SELECT set_id FROM `Samples_x_Saved_Results` WHERE sid in ($sidstr)", "Samples_x_Saved_Results");
    foreach ($srows as $k => $srow) {
        doQuery("DELETE FROM `Samples_x_Saved_Results_Pages` WHERE `set_id` = '" . $srow['set_id'] . "'", "Samples_x_Saved_Results_Pages");
    }
    doQuery("DELETE FROM `Samples_x_Saved_Results` WHERE sid IN ($sidstr)", "Samples_x_Saved_Results");

    // write out the sidstring to the slow-query queue.
    $fh = fopen("$scriptdir/Query_Results/.delete_queue", "a");
    fwrite($fh, "$sidstr\n");
    fclose($fh);
    // Delete datafiles.
    $storage = $datadir;
    if (file_exists($storage) && $storage != '' && $storage != '/') {
        $sids = explode(',', $sidstr);
        foreach ($sids as $key => $sid) {
            system("rm -f $storage$sid.*");
        }
    }

    // give feedback.
    echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
    echo "<h3 style='text-align:left;'>Samples deleted</h3>";
    echo "<p>The samples were deleted. You can now close this screen</p>";
    echo "<p  style='text-align:left;'><input type=button name='close' value='Close' onClick=\"document.getElementById('overlay').style.display='none';location.reload()\"></form></p>";
    echo "</div>";
    exit;
}

# move samples.
if ($action = 'm') {
    $sids = explode(",", $_GET['s']);
    $sidstr = '';
    $error = '';
    $ok = '';
    // double check permissions.
    foreach ($sids as $sid) {
        if (!is_numeric($sid)) {
            continue;
        }
        $row = array_shift(...[runQuery("SELECT s.Name, pu.editsample FROM `Samples` s JOIN `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON s.id = ps.sid AND ps.pid = pu.pid WHERE ps.sid = '$sid' AND pu.uid = '$userid'", "Samples:Projects_x_Users:Projects_x_Samples")]);
        if (count($row) == 0) {
            $error .= "<li>Invalid SampleID: $sid</li>";
            continue;
        }
        if ($row['editsample'] == 1) {
            $sidstr .= "$sid,";
            $ok .= "<li>" . $row['Name'] . "</li>";
        } else {
            $error .= "<li>" . $row['Name'] . "</li>";
        }
    }
    if ($sidstr == '') {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>No Samples Selected</span>";
        echo "<p>Either no samples were selected, or you do not have sufficient permissions to move them.";
        echo "<p><input type=button value='Close' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";
        exit;
    }
    $sidstr = substr($sidstr, 0, -1);

    //target pid:
    $targetpid = $_GET['p'];
    $NewName = '';
    if ($targetpid == 'new') {
        $NewName = $_GET['tn'];
        if ($NewName == '') {
            echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>No Project Name provided</span>";
            echo "<p>The name for the new project was empty. Please go back and try again.</p>";
            echo "<p><input type=button value='Close' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";
            exit();
        }
        $NewNamedb = addslashes($NewName);
        $targetpid = insertQuery("INSERT INTO `Projects` (Name, userID) VALUES ('$NewNamedb','$userid')", "Projects");
        // set permissions for current user
        insertQuery("INSERT INTO `Projects_x_Users` (pid, uid, editvariant, editclinic, editsample) VALUES ('$targetpid','$userid','1','1','1')", "Projects_x_Users");
    } else {
        $row = array_shift(...[runQuery("SELECT Name FROM `Projects` WHERE id = '$targetpid'", "Projects")]);
        if (!is_array($row) || count($row) == 0) {
            echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Invalid target project provided</span>";
            echo "<p>The name for the new project was not found in the database. Please go back and try again.</p>";
            echo "<p><input type=button value='Close' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";
            exit;
        }
        $NewName = $row[0];
    }
    doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id = '$targetpid'", "Projects:Variants_x_Projects_Summary:Summary");
    // get originating projects
    $rows = runQuery("SELECT DISTINCT(pid) AS pid FROM `Projects_x_Samples` WHERE sid IN ($sidstr)", "Projects_x_Samples");
    $fromstring = '';
    $fromarray = array();
    foreach ($rows as $k => $row) {
        $fromstring .= $row['pid'] . ',';
        $fromarray[$row['pid']] = 1;
        // update summary status.
        doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id = '" . $row['pid'] . "'", "Projects"); // caches were cleared above
    }
    $fromstring = substr($fromstring, 0, -1);
    // update permissions for other user => present options if needed.
    $gconflicts = '';
    // list groups with old permission and option to change it
    $groups = runQuery("SELECT gid, p.Name AS pname FROM `Projects_x_Usergroups` pu JOIN `Projects` p ON p.id = pu.pid WHERE pid In ($fromstring) OR pid = $targetpid", "Projects_x_Usergroups:Projects");
    if (count($groups) > 0) {
        $gconflicts .= "<tr><th colspan=4 $firstcell class=top style='color:#FFF;'>User Groups</th></tr>";
        $gconflicts .= "<tr><th class=top style='color:#FFF;'>Group Name</th><th class=top style='color:#FFF;'>Access Type</th><th class=top style='color:#FFF;'>Source Project (name)</th><th class=top style='color:#FFF;'>Target Project</th></tr>";
        foreach ($groups as $k => $gr) {
            $groupid = $gr['gid'];
            $pname = $gr['pname'];
            //## get group permissions
            $gpr = runQuery("SELECT editclinic, editvariant, editsample, name FROM `Usergroups` WHERE id = '$groupid'", "Usergroups")[0];
            $editcnv = $gpr['editvariant'];
            $editclinic = $gpr['editclinic'];
            $editsample = $gpr['editsample'];
            $gname = $gpr['name'];
            if ($editcnv == 1 && $editclinic == 1) {
                $perm = 'Full';
            } elseif ($editcnv == 1) {
                $perm = 'Variant';
            } elseif ($editclinic == 1) {
                $perm = 'Clinic';
            } else {
                $perm = 'Read-only';
            }
            if ($editsample == 1) {
                $perm .= " / Project Control";
            }
            $check = runQuery("SELECT gid FROM `Projects_x_Usergroups` WHERE pid IN ($fromstring) AND gid = '$groupid'", "Projects_x_Usergroups");
            if (count($check) > 0) {
                $oldacc = 1;
            } else {
                $oldacc = 0;
            }
            $check = runQuery("SELECT gid FROM `Projects_x_Usergroups`WHERE pid = '$targetpid' AND gid = '$groupid'", "Projects_x_Usergroups");
            if (count($check) > 0) {
                $newacc = 1;
            } else {
                $newacc = 0;
            }
            $gconflicts .= "<tr><th class=left NOWRAP style='color:#FFF;'>$gname</th><td style='color:#FFF;'>$perm</td><td>" . $yesno[$oldacc] . " ($pname)</td><td style='color:#FFF;'><span id=add$groupid>" . $yesno[$newacc] . "</span></td></tr>";
        }
    }
    // seperate users
    $users = runQuery("SELECT u.FirstName, u.LastName, pu.uid, pu.editvariant, pu.editclinic, pu.editsample, pu.pid FROM `Projects_x_Users` pu JOIN `Users` u ON pu.uid = u.id WHERE pid IN ($fromstring) OR pid = '$targetpid'", "Projects_x_Users:Users");
    $uconflicts = '';
    if (count($users) > 0) {
        $doneusers = array();
        $uconflicts .= "<tr><th colspan=4 class=top style='color:#FFF;'>Seperate Users</th></tr>";
        $uconflicts .= "<tr><th class=top style='color:#FFF;'>User Name</th><th class=top style='color:#FFF;'>Source Project Permission</th><th class=top style='color:#FFF;'>Target Project Permission</th></tr>";
        foreach ($users as $k => $ur) {
            $thisuserid = $ur['uid'];

            if ($doneusers[$thisuserid] == 1) {
                // user done before
                continue;
            }
            // reset summary.
            doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE `id` = '$thisuserid'", "Users:Variants_x_Users_Summary:Summary");
            $doneusers[$thisuserid] = 1;
            $thisusername = $ur['FirstName'] . " " . $ur['LastName'];
            // get group permissions
            $editcnv = $ur['editvariant'];
            $editclinic = $ur['editclinic'];
            $editsample = $ur['editsample'];
            $project = $ur['pid'];
            if ($editcnv == 1 && $editclinic == 1) {
                $perm = 'Full';
            } elseif ($editcnv == 1) {
                $perm = 'Variant';
            } elseif ($editclinic == 1) {
                $perm = 'Clinic';
            } else {
                $perm = 'Read-only';
            }
            if ($editsample == 1) {
                $perm .= " / Project Control";
            }
            // check other project permission for current user.
            // current pid is source project . check the new permission.
            if (array_key_exists($project, $fromarray)) {
                $oldperm = $perm;
                $cr = array_shift(...[runQuery("SELECT editvariant, editclinic, editsample FROM `Projects_x_Users` WHERE pid = '$targetpid' AND uid = $thisuserid", "Projects_x_Users")]);
                if (is_array($cr) && count($cr) > 0) {
                    $editcnv = $cr['editvariant'];
                    $editclinic = $cr['editclinic'];
                    $editsample = $cr['editsample'];
                    if ($editcnv == 1 && $editclinic == 1) {
                        $perm = 'Full';
                    } elseif ($editcnv == 1) {
                        $perm = 'Variant';
                    } elseif ($editclinic == 1) {
                        $perm = 'Clinic';
                    } else {
                        $perm = 'Read-only';
                    }
                    if ($editsample == 1) {
                        $perm .= " / Project Control";
                    }
                    $newperm = $perm;
                } else {
                    $newperm = 'none';
                }
            }
            // current pid is the target project, check source projects.
            else {
                $newperm = $perm;
                $cr = array_shift(...[runQuery("SELECT editvariant, editclinic, editsample FROM `Projects_x_Users` WHERE pid IN ($fromstring) AND uid = $thisuserid", "Projects_x_Users")]);
                if (is_array($cr) && count($cr) > 0) {
                    $editcnv = $cr['editvariant'];
                    $editclinic = $cr['editclinic'];
                    $editsample = $cr['editsample'];
                    if ($editcnv == 1 && $editclinic == 1) {
                        $perm = 'Full';
                    } elseif ($editcnv == 1) {
                        $perm = 'Variant';
                    } elseif ($editclinic == 1) {
                        $perm = 'Clinic';
                    } else {
                        $perm = 'Read-only';
                    }
                    if ($editsample == 1) {
                        $perm .= " / Project Control";
                    }
                    $oldperm = $perm;
                } else {
                    $oldperm = 'none';
                }
            }
            // print user
            $uconflicts .= "<tr><th class=left NOWRAP style='color:#FFF;'>$thisusername</th><td style='color:#FFF;'>$oldperm</td><td style='color:#FFF;'>$newperm </td></tr>";
        }
    }
    echo "<div class=section style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'>";
    // are there conflicts? => print title
    if ($uconflicts != '' || $gconflicts != '') {
        echo "<span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Review Permission Settings</span>";
        echo "<form action='index.php?page=projects' method=POST><input type=hidden name=pid value='$targetpid'>";
        echo "<p style='text-align:left;'>An overview of usergroup and user permissions with regard to the affected projects is given below. Click on the 'Change Permissions' button below to manage permissions for the new project. Permissions are not automatically transferred !<br/><br/>";
        // TODO : make this ajax-based ?

    }
    // group conflicts ?
    if ($gconflicts != '') {
        echo "<table cellspacing=0 class=w75 >";
        echo $gconflicts;
        echo "</table><br/>";
    }
    if ($uconflicts != '') {
        echo "<table cellspacing=0 class=w75 >";
        echo $uconflicts;
        echo "</table><br/>";
    }
    if ($uconflicts != '' | $gconflicts != '') {
        echo "<p style='text-align:left;'><input type='submit' name='ManagePermissions' value='Change Permissions'></p></form>";
    }
    // NOW MOVE THE SAMPLES
    doQuery("UPDATE `Projects_x_Samples` SET pid = '$targetpid' WHERE sid IN ($sidstr)", "Projects_x_Samples:Variants_x_Projects_Summary:Summary"); // relevant caches on user summary were cleared above

    // Remove empty projects. 
    foreach ($fromarray as $pid => $dummy) {
        $srows = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$pid'", "Projects_x_Samples");
        if (count($srows) == 0) {
            // remove project
            doQuery("DELETE FROM `Projects` WHERE id = '$pid'", "Projects");
            // remove from permissiosn
            doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$pid'", "Projects_x_Users");
            doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$pid'", "Projects_x_Usergroups");
        }
    }

    // present user with button to go back to 'recent'
    echo "<span style='padding-top:2em;float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'> Sample Migration Done</span>";
    echo "<p style='text-align:left'>Samples were moved successfully to '$NewName'</p>";
    echo "<input type=button name='Close' value='Close' onClick=\"window.location.href='index.php?page=samples'\">";
    echo "</div>";
    exit();
}
