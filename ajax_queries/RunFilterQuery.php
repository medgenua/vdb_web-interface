<?php

// load xml library
require_once('../xmlLib2.php');

// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}
$db = "NGS-Variants-hg19";
include('../includes/inc_logging.inc');

// read the filter config xml
$configuration = my_xml2array("../Filter/Filter_Options.xml");


// get main posted values
$sid = $_POST['sid'];
$listed = $_POST['listed']; // this is the number of filter items.


////////////////////////
// Set The main query //
////////////////////////
$select = "SELECT v.chr, v.start, v.stop, v.RefAllele, v.AltAllele, vs.inheritance, vs.class, vs.PhredPolymorphism, vs.Filter, vs.AltCount, vs.RefDepth, vs.AltDepth, vs.PhredGenotype, s.Name, s.gender";
$from = "FROM `Variants` v JOIN `Variants_x_Samples` vs JOIN `Samples` s ";
$on = "ON s.id = vs.sid AND vs.id = v.id ";
$where = "WHERE s.id = '$sid' ";
/////////////////////////
// ADD filter settings //
/////////////////////////
for ($idx = 1; $idx <= $listed; $idx++) {
    // skip unset categories
    if (array_key_exists("category$idx", $_POST)) {
        $category = stripslashes($_POST["category$idx"]);
    } else {
        continue;
    }
    $negate = stripslashes($_POST["negate$idx"]);
    // skip unset parameters.
    if (array_key_exists("param$idx", $_POST)) {
        $param = stripslashes($_POST["param$idx"]);
    } else {
        echo "param$idx not found<br/>";
        continue;
    }
    if (array_key_exists("argument$idx", $_POST)) {
        $arguments = stripslashes($_POST["argument$idx"]);
    } else {
        $arguments = '';
    }
    $arguments = explode('@@@', $arguments);
    $arguments = array_filter($arguments); // this filters empty [ AND ANY OTHER FALSE VALUE!]
    if (array_key_exists("value$idx", $_POST)) {
        $value = stripslashes($_POST["value$idx"]);
    } else {
        $value = '';
    }
    // compose where
    $xmlnode = get_value_by_path($configuration, "FilterConfiguration/$category/$param");
    if (!array_key_exists("attributes", $xmlnode)) {
        continue;
    }
    $xmlattributes = $xmlnode['attributes'];
    $MultiJoin = $xmlattributes['MultiJoin'];

    $WhereQuery = $xmlattributes['WhereQuery'];
    echo "temp where: $WhereQuery<br/>";

    if ($MultiJoin == 'IN') {
        // compose the IN string for replacement.
        $in = '';
        foreach ($arguments as $key => $argument) {
            $xmlnode = get_value_by_path($configuration, "FilterConfiguration/$category/$param/$argument");
            if ($xmlnode['value'] == '') {
                continue;
            }
            $in .= $xmlnode['value'] . ",";
        }
        if ($in != "") {
            $in = substr($in, 0, -1);
        }
        $WhereQuery = str_replace('%replace', $in, $WhereQuery);
    } elseif (count($arguments) == 1) {
        // replace with selected argument value
        $argument = $arguments[0];
        $xmlnode = get_value_by_path($configuration, "FilterConfiguration/$category/$param/$argument");
        $xmlvalue = $xmlnode['value'];
        $WhereQuery = str_replace('%replace', $xmlvalue, $WhereQuery);
    } else {
        echo "count: " . count($arguments) . "\n";
    }
    // if there is a %value to replace, do so
    if (strpos($WhereQuery, '%value')) {
        $WhereQuery = str_replace('%value', $value, $WhereQuery);
    }
    $where .= " AND $WhereQuery";
}
echo "$select $from $on $where <br/>";
