<?php

include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');

// check if running
$annorunning = system("qstat -ru $scriptuser | grep 'Ann.' | wc -l");
if ($annorunning > 0) {
    echo "<span style='font-weight:bold;font-color:red;'>Annotation updates are running, response might be slow</span>";
    exit;
}
// annotation pending?
$pending = system("cat ../Annotations/update.txt");
if ($pending > 0) {
    echo "<span style='font-weight:bold;font-color:red;'>Annotation updates pending</span>";
    exit;
}
