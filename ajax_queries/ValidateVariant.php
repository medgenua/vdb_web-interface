<?php
// GOAL : 
//   - for variants added to classifier : approve / reject / reset this varaint. 


// variables
$action = $_GET['action'];
$vid = $_GET['vid'];
$cid = $_GET['cid'];
$uid = $_GET['uid'];
$comments = '';
if (isset($_GET['comments'])) {
    $comments = addslashes($_GET['comments']);
}

// check for illegal values 
if (!is_numeric($cid) || !is_numeric($vid) || !is_numeric($uid)) {
    echo "ILLEGAL VARIABLES PROVIDED. <br/>";
    echo "vid : '$vid' <br/> cid: '$cid'<br/> uid: '$uid' <br/> action: '$action'<br/>";
    exit();
}
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// DELETE A VARIANT
if ($action == 'delete') {
    // 0. check permission. 
    $rows = runQuery("SELECT uid FROM `Users_x_Classifiers` WHERE cid = '$cid' AND uid = '$uid' AND can_remove = 1", "Users_x_Classifiers");
    if (count($rows) == 0) {
        echo "You do not have the permissions to validate variants for this classifier.";
        exit();
    }
    doQuery("DELETE FROM `Classifiers_x_Variants` WHERE  cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
    echo "OK";
    exit();
}
// UPDATE STATUS OF VARIANTS


if (!is_numeric($action)) {
    echo "ILLEGAL ACTION PROVIDED. should be numeric<br/>";
    echo "action: '$action'<br/>";
    exit();
}

// 0. check permission. 
$rows = runQuery("SELECT uid FROM `Users_x_Classifiers` WHERE cid = '$cid' AND uid = '$uid' AND can_validate = 1", "Users_x_Classifiers");
if (count($rows) == 0) {
    echo "You do not have the permissions to validate variants for this classifier.";
    exit();
}
// 1a. reset validation.
if ($action == 0 && !isset($_GET['comments'])) {
    doQuery("UPDATE `Classifiers_x_Variants` SET `validate_reject` = '$action' , `validated_by` = '$uid', `validated_on` = CURRENT_TIMESTAMP WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");
    // reset actions.
    // get distinct uid 
    $rows = runQuery("SELECT uid FROM `Users_x_Classifiers` WHERE `cid` = '$cid' AND `can_validate` = 1", "Users_x_Classifiers");
    foreach ($rows as $key => $row) {
        $a_uid = $row['uid'];
        doQuery("REPLACE INTO `Inbox_actions` (`uid`,`action_type`) VALUES ('$a_uid','Validate Classifier')", "Inbox_actions");
    }
    echo "OK";
    exit();
}

// 1b. update db 
doQuery("UPDATE `Classifiers_x_Variants` SET `validate_reject` = '$action' , `validated_by` = '$uid', `validated_on` = CURRENT_TIMESTAMP, `comments` = '$comments' WHERE cid = '$cid' AND vid = '$vid'", "Classifiers_x_Variants");

// if action was not 'undecided' => re-evaluate the inbox_actions.
if ($action != '0') {
    // get distinct uid 
    $rows = runQuery("SELECT uid FROM `Inbox_actions` WHERE `action_type` = 'Validate Classifier' ", "Inbox_actions");
    foreach ($rows as $key => $row) {
        $a_uid = $row['uid'];
        // check accessible classifiers.
        $cl_rows = runQuery("SELECT cid FROM `Users_x_Classifiers` WHERE uid = '$uid' AND can_validate = 1", "Users_x_Classifiers");
        $cids = '';
        foreach ($cl_rows as $clkey => $cl_row) {
            $cids .= $cl_row['cid'] . ",";
        }
        $cids = substr($cids, 0, -1);
        // check if any variants left.
        $vrows = runQuery("SELECT vid FROM `Classifiers_x_Variants` WHERE `cid` IN ($cids) AND `validate_reject` = 0", "Classifiers_x_Variants");
        if (count($vrows) == 0) {
            // none left. delete from the inbox for this user.
            doQuery("DELETE FROM `Inbox_actions` WHERE uid = '$a_uid' AND `action_type` = 'Validate Classifier'", "Inbox_actions");
        }
    }
}
echo "OK";
exit();
