<?php
#######################
# CONNECT to database #
#######################
include('../.LoadCredentials.php');
include('../includes/inc_logging.inc');
require("../includes/inc_query_functions.inc");
if (isset($_GET['gb'])) {
    $newbuild = $_GET['gb'];
    $_SESSION['dbname'] = $newbuild;
    # is current build?

    $row = array_shift(...[runQuery("SELECT name, StringName, Description FROM `NGS-Variants-Admin`.`CurrentBuild` WHERE name = '$newbuild'")]);
    if (!empty($row)) {
        # current build, use default colors.
        $_SESSION['dbname'] = $row['name'];
        $_SESSION['dbstring'] = $row['StringName'];
        $_SESSION['dbdescription'] = stripslashes($row['Description']);
        $_SESSION['Archived'] = 0;
    } else {
        # must be previous build, adapt color
        $row = runQuery("SELECT name, StringName, Description FROM `NGS-Variants-Admin`.`PreviousBuilds` WHERE name = '$newbuild'")[0];
        //$_SESSION['bgcolor'] = "style='background-color:#E0E0E0;'";
        $_SESSION['dbname'] = $row['name'];
        $_SESSION['dbstring'] = $row['StringName'];
        $_SESSION['dbdescription'] = stripslashes($row['Description']);
        $_SESSION['Archived'] = 1;
    }
}
echo "Build Changed To" . $row['name'];
