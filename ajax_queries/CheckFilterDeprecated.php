<?php
include('../includes/inc_logging.inc');

// no category.
if (!isset($_GET['c']) || $_GET['c'] == '') {
	exit;
}
// no selection.
if (!isset($_GET['s']) || $_GET['s'] == '') {
	exit;
}

// load xml library
require_once('../xmlLib2.php');

// read the filter config xml
$configuration = my_xml2array("../Filter/Filter_Options.xml");

$title = '';
$content = '';
// check if deprecated.
if (get_value_by_path($configuration,"FilterConfiguration/".$_GET['c']."/".$_GET['s']."/deprecated")) {
	$title = 'WARNING: Underlying annotations are not available for novel samples.';
	$content = ' Deprecated Filter. Use with caution.';
}

echo json_encode(array("title" => $title, 'content' => $content));
exit;
