<?php

// GOAL: LOAD list of saved & shared annotation sets.


#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];

if (isset($_GET['uid'])) {
    $uid = $_GET['uid'];
}
if (!is_numeric($uid) || $uid == '') {
    $uid = $_SESSION['userID'];
}
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// output 
echo "<h3>Preset Annotation Selections</h3><p>";
echo "<p><input type=submit onClick='ApplyAnnotations($uid)' value='Add Annotations'></p>";

echo "<form name='StoredAnnotations'>";
echo "<span class=emph>My Annotations</span><br/>";
// get results
$rows = runQuery("SELECT aid, AnnotationName,Highlight FROM `Users_x_Annotations`  WHERE uid = $uid ORDER BY Highlight DESC, AnnotationName ASC", "Users_x_Annotations");
if (count($rows) == 0) {
    echo " &nbsp; No saved annotations available.";
} else {
    foreach ($rows as $k => $row) {
        $icons = " <img src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em' onClick=\"DeleteSetting('Annotation','" . $row['aid'] . "','$uid',getAnnotations)\" />  <img src='Images/layout/share_icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareSetting('Annotation','" . $row['aid'] . "','$uid')\" />";
        echo " &nbsp; <input type=checkbox name='annotations' value='" . $row['aid'] . "'> ";
        if ($row['Highlight'] == 1) {
            echo "<img src='Images/layout/fav_full.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ManageFavorites('OwnAnno','" . $row['aid'] . "','0',getAnnotations)\" /> ";
            echo $row['AnnotationName'];
        } else {
            echo $row['AnnotationName'];
            echo " <img src='Images/layout/fav_empty.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ManageFavorites('OwnAnno','" . $row['aid'] . "','1',getAnnotations)\" /> ";
        }
        echo $icons;
        echo "<br/>";
    }
}
echo "<br/><span class=emph>Shared Annotations</span><br/>";

// get results
$rows = runQuery("SELECT ua.aid, ua.AnnotationName,us.Highlight FROM `Users_x_Annotations` ua JOIN `Users_x_Shared_Annotations` us ON us.aid = ua.aid WHERE us.uid = $uid ORDER BY us.Highlight DESC, ua.AnnotationName ASC", "Users_x_Annotations:Users_x_Shared_Annotations");
if (count($rows) == 0) {
    echo " &nbsp; No shared annotations available.";
} else {
    foreach ($rows as $k => $row) {
        $icons = " <img src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em' onClick=\"DeleteSetting('Annotation','" . $row['aid'] . "','$uid',getAnnotations)\" />";
        echo " &nbsp; <input type=checkbox name='annotations' value='" . $row['aid'] . "'> ";

        if ($row['Highlight'] == 1) {
            echo "<img src='Images/layout/fav_full.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ManageFavorites('SharedAnno','" . $row['aid'] . "','0',getAnnotations)\" /> ";
            echo $row['AnnotationName'];
        } else {
            echo $row['AnnotationName'];
            echo " <img src='Images/layout/fav_empty.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ManageFavorites('SharedAnno','" . $row['aid'] . "','1',getAnnotations)\" /> ";
        }
        echo $icons;
        echo "<br/>";
    }
}

echo "</form>";

echo "</p>";
