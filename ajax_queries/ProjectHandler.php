<?php

#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');

$userid = $_GET['uid'];

$action = $_GET['a'];

require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// Variables
$altcount = array(0 => 'Hom.Reference', 1 => 'Heterozygous', 2 => 'Hom.Alterative');
$inh = array(0 => 'ND', 1 => 'P', 2 => 'M', 3 => 'DN', 4 => 'Both', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined', 'Both' => 'Both Parents');
$inhm = array(0 => '-', 1 => 'Dominant', 2 => 'Recessive', '3' => 'Unknown');
$classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');
$AA_Codes = array('G' => 'Gly', 'A' => 'Ala', 'L' => 'Leu', 'M' => 'Met', 'F' => 'Phe', 'W' => 'Trp', 'K' => 'Lys', 'Q' => 'Gln', 'E' => 'Glu', 'S' => 'Ser', 'P' => 'Pro', 'V' => 'Val', 'I' => 'Ile', 'C' => 'Cys', 'Y' => 'Tyr', 'H' => 'His', 'R' => 'Arg', 'N' => 'Asn', 'D' => 'Asp', 'T' => 'Thr', 'X' => 'X');
$aa_str = implode("", array_keys($AA_Codes));
$chr_hash = array();
for ($i = 1; $i <= 22; $i++) {
    $chr_hash[$i] = $i;
}
$chr_hash[23] = 'X';
$chr_hash[24] = 'Y';
$chr_hash[25] = 'MT';

// EXPORT TARBALL
if ($action == 'e') {
    $pid = $_GET['p'];

    if (!is_numeric($pid) || $pid == '') {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
        echo "<p>Invalid ProjectID provided, or no access to the selected project.</p>";
        echo "<input type=button name='Close' value='Close' onClick=\"document.getElementById('overlay').style.display='none'\">";
        echo "</div>";
        exit;
    }
    // double check permissions.
    $row = runQuery("SELECT pu.pid, p.Name, p.created FROM  `Projects_x_Users` pu  JOIN `Projects` p ON p.id = pu.pid WHERE pu.pid = '$pid' AND pu.uid = '$userid' AND pu.editsample = 1", "Projects_x_Users:Projects");
    if (count($row) == 0) {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
        echo "<p>Invalid ProjectID provided, or no access to the selected project.</p>";
        echo "<input type=button name='Close' value='Close' onClick=\"document.getElementById('overlay').style.display='none'\">";
        echo "</div>";
        exit;
    }
    // mk tmp dir.
    $tempdir = "/tmp/vdb_export_" . time();
    while (file_exists($tempdir)) {
        sleep(1);
        $tempdir = "/tmp/vdb_export_" . time();
    }
    system("mkdir '$tempdir'");
    //  write out project name & details.
    $fh = fopen("$tempdir/project.txt", 'w');
    fwrite($fh, "name=" . $row['Name'] . "\n");
    fwrite($fh, "date=" . $row['created'] . "\n");
    fclose($fh);
    // samples
    $samples = array();
    $rows = runQuery("SELECT s.id, s.Name, s.gender, s.clinical, s.IsControl, s.affected FROM `Samples` s JOIN `Projects_x_Samples` ps ON ps.sid = s.id WHERE ps.pid = '$pid'", "Samples:Projects_x_Samples");
    $fh = fopen("$tempdir/samples.txt", 'w');
    foreach ($rows as $k => $row) {
        fwrite($fh, $row['Name'] . "\t" . $row['gender'] . "\t" . $row['IsControl'] . "\t" . $row['affected'] . "\n");
        if ($row['clinical'] != '') {
            $cl = fopen("$tempdir/clin_" . $row['Name'] . ".txt", 'w');
            fwrite($cl, $row['clinical']);
            fclose($cl);
        }
        $samples[$row['id']] = $row['Name'];
    }
    fclose($fh);
    // relations.
    $sidstr = implode(",", array_keys($samples));
    $rows = runQuery("SELECT sid1, sid2, Relation FROM `Samples_x_Samples` WHERE sid1 IN ($sidstr) AND sid2 IN ($sidstr)", "Samples_x_Samples");
    $fh = fopen("$tempdir/relations.txt", 'w');
    foreach ($rows as $k => $row) {
        fwrite($fh, $samples[$row['sid1']] . "\t" . $samples[$row['sid2']] . "\t" . $row['Relation'] . "\n");
    }
    fclose($fh);
    // HPO clinic.
    $rows = runQuery("SELECT sid, tid FROM `Samples_x_HPO_Terms` WHERE sid IN ($sidstr)", "Samples_x_HPO_Terms");
    $fh = fopen("$tempdir/hpo.txt", 'w');
    foreach ($rows as $k => $row) {
        fwrite($fh, $samples[$row['sid']] . "\t" . $row['tid'] . "\n");
    }
    fclose($fh);
    // variants.
    $vars = array();
    $rows = runQuery("SELECT v.* ,vs.* FROM `Variants` v JOIN `Variants_x_Samples` vs ON v.id = vs.vid WHERE vs.sid IN ($sidstr)", "Variants:Variants_x_Samples");
    $fh = fopen("$tempdir/variants.txt", 'w');
    foreach ($rows as $k => $row) {
        // store variant by id. chr:pos:ref:alt
        $vars[$row[0]] = $row[3] . ':' . $row[1] . ':' . $row[4] . ':' . $row[5];
        // remove unneeded (all associatives and a few positional).
        foreach ($row as $key => $value) {
            if (!is_int($key)) {
                unset($row[$key]);
            }
        }
        unset($row[9]);
        unset($row[8]);
        unset($row[0]);
        //$row = array_diff_key($row, [0 => 'vid', '8' => 'dummy', '9' => 'vid']);
        // replace sample_id by sample_name.
        $row[10] = $samples[$row[10]];
        fwrite($fh, implode("\t", $row) . "\n");
    }
    fclose($fh);
    // custom annotations.
    $rows = runQuery("SELECT DISTINCT(aid) FROM `Custom_Annotations_x_Samples` WHERE sid IN ($sidstr)", "Custom_Annotations_x_Samples");
    //$fh = fopen("$tempdir/custom_annotations.txt",'w');
    $custom_anno = array();
    foreach ($rows as $k => $row) {
        $aid = $row['aid'];
        $sr = runQuery("SELECT value_type, field_name FROM `Custom_Annotations` WHERE aid = '$aid'", "Custom_Annotations")[0];
        $custom_anno[$aid]['type'] = $sr['value_type'];
        $custom_anno[$aid]['name'] = $sr['value_name'];
        if ($sr['value_type'] == 'list') {
            // get value codes.
            $vcqs = runQuery("SELECT field_value, field_code FROM `Custom_Annotations_x_Value_Codes` WHERE aid = '$aid'");
            foreach ($vcqs as $k => $v) {
                $custom_anno[$aid]['values'][$vcr['field_code']] = $vcr['field_value'];
            }
        }
    }
    // get non-coded custom per type.
    $ctypes = ["decimal", "integer", "varchar"];
    foreach ($ctypes as $type) {
        $trows = runQuery("SELECT * FROM `Variants_x_Custom_Annotations_$type` WHERE sid IN ($sidstr)", "Variants_x_Custom_Annotations_$type");
        $fh = fopen("$tempdir/custom_annotations_$type.txt", 'w');
        foreach ($trows as $k => $row) {
            // remove unneeded (all associatives).
            foreach ($row as $key => $value) {
                if (!is_int($key)) {
                    unset($row[$key]);
                }
            }
            // replace.
            $row[0] = $vars[$row[0]];
            $row[1] = $custom_anno[$row[1]]['name'];
            $row[2] = $samples[$row[2]];
            fwrite($fh, implode("\t", $row) . "\n");
        }
        fclose($fh);
    }
    // get coded custom
    $rows = runQuery("SELECT * FROM `Variants_x_Custom_Annotations_list` WHERE sid IN ($sidstr)", "Variants_x_Custom_Annotations_list");
    $fh = fopen("$tempdir/custom_annotations_list.txt", 'w');
    foreach ($rows as $k => $row) {
        // remove unneeded (all associatives).
        foreach ($row as $key => $value) {
            if (!is_int($key)) {
                unset($row[$key]);
            }
        }
        // replace.
        $row[0] = $vars[$row[0]];
        $row[1] = $custom_anno[$row[1]]['name'];
        $row[2] = $samples[$row[2]];
        $row[3] = $custom_anno[$row[1]]['values'][$row[3]];
        fwrite($fh, implode("\t", $row) . "\n");
    }
    fclose($fh);
    // make token.
    $token = substr(md5(rand()), 0, 12);
    system("mkdir $tempdir/.gnupg");
    $outfile = "Export_Files/$userid." . time() . ".tar.gz.gpg";

    system("cd '$tempdir' ; tar cz *txt | gpg --no-permission-warning --homedir $tempdir/.gnupg -c --passphrase '$token' -o $scriptdir/$outfile");
    // output
    echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
}
// EXPORT CSV
if ($action == 'c') {
    $pid = $_GET['p'];
    // valid pid ?
    if (!is_numeric($pid) || $pid == '') {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
        echo "<p>Invalid ProjectID provided, or no access to the selected project.</p>";
        echo "<input type=button name='Close' value='Close' onClick=\"document.getElementById('overlay').style.display='none'\">";
        echo "</div>";
        exit;
    }
    // double check permissions.
    $row = array_shift(...[runQuery("SELECT pu.pid, p.Name, p.created FROM  `Projects_x_Users` pu  JOIN `Projects` p ON p.id = pu.pid WHERE pu.pid = '$pid' AND pu.uid = '$userid' AND pu.editsample = 1", "Projects_x_Users:Projects")]);
    if (count($rows) == 0) {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
        echo "<p>Invalid ProjectID provided, or no access to the selected project.</p>";
        echo "<input type=button name='Close' value='Close' onClick=\"document.getElementById('overlay').style.display='none'\">";
        echo "</div>";
        exit;
    }
    $project = $row['Name'];

    // samples in project
    $sidstr = '';
    $rows = runQuery("SELECT s.id FROM `Samples` s JOIN `Projects_x_Samples` ps ON ps.sid = s.id WHERE ps.pid = '$pid'", "Projects_x_Samples:Samples");
    //while($row = mysql_fetch_array($query)) {
    foreach ($rows as $k => $row) {
        $sidstr .= $row['id'] . ',';
    }
    if ($sidstr == '') {
        echo "no samples in report.";
        exit;
    }
    $sidstr = substr($sidstr, 0, -1);

    // variants.
    $vars = array();
    $results = array();
    $rows = runQuery("SELECT v.chr, v.start, v.RefAllele, v.AltAllele, vs.vid ,vs.sid, vs.inheritance, vs.InheritanceMode,vs.class,vs.AltCount,vs.RefDepth,vs.AltDepth FROM `Variants` v JOIN `Variants_x_Samples` vs ON v.id = vs.vid WHERE vs.sid IN ($sidstr) AND vs.class IN (1,2)", "Variants:Variants_x_Samples");

    //while ($row = mysql_fetch_row($query)) {
    foreach ($rows as $k => $row) {

        // store results : sample : vid : "inh,inhMode,class,zygosity,RefDepth/AltDepth
        $results[$row[5]][$row[4]] = $altcount[$row[9]] . ',' . $row[10] . '/' . $row[11] . ',' . $inh[$inh[$row[6]]] . ',' . $classes[$row[8]] . ',' . $inhm[$row[7]];
        // store variants
        if (!array_key_exists($row[4], $vars)) {
            $vars[$row[4]]['l'] = "chr" . $chr_hash[$row[0]] . ":" . $row[1];
            $vars[$row[4]]['ra'] = $row[2];
            $vars[$row[4]]['aa'] = $row[3];
            $vars[$row[4]]['g'] = array();
        }
    }
    if (count($vars) == 0) {
        echo "No variants to report.";
        exit;
    }
    // load value codes.
    $value_codes = array();
    $rows = runQuery("SELECT `id`, `Item_Value`, `Table_x_Column` FROM `Value_Codes` WHERE `Table_x_Column` LIKE 'Variants_x_ANNOVAR_refgene_%'", "Value_Codes");
    foreach ($rows as $k => $row) {
        $value_codes[$row[2] . ":" . $row[0]] = $row[1];
    }
    // get meta-data : genes
    if (count($vars) > 0) {
        $srows = runQuery("SELECT vid, GeneSymbol,Transcript, Exon, GeneLocation,VariantType,CPointNT, CPointAA FROM `Variants_x_ANNOVAR_refgene` WHERE vid IN (" . implode(",", array_keys($vars)) . ")", "Variants_x_ANNOVAR_refgene");
        foreach ($srows as $k => $row) {
            # gene : symbol / transcript : info
            $vt = $value_codes["Variants_x_ANNOVAR_refgene_VariantType:" . $row[5]];
            $gl = $value_codes["Variants_x_ANNOVAR_refgene_GeneLocation:" . $row[4]];
            if (preg_match("/c\.([ACGT]+)(\d+)([ACGT]+)/", $row[6], $matches)) {
                $cnt = "c." . $matches[2] . $matches[1] . ">" . $matches[3];
            } else {
                $cnt = $row[6];
            }
            $pnot = '';
            for ($i = 0; $i < strlen($row[7]); $i++) {
                if (preg_match("/[$aa_str]/", substr($row[7], $i, 1))) {
                    $pnot .= $AA_Codes[substr($row[7], $i, 1)];
                } else {
                    $pnot .= substr($row[7], $i, 1);
                }
            }
            $caa = $pnot;
            /*
			if (preg_match("/p.([$aa_str]+)(\d+)([$aa_str]+)/",$row[7],$matches)) {
				$caa = "p.";
				for ($i = 0 ; $i< strlen($matches[1][0]); $i++) {
					$caa .= $AA_Codes[substr($matches[1][0],$i,1)];
				}
				$caa .= $matches[2][0];
				for ($i = 0 ; $i< strlen($matches[3][0]); $i++) {
					$caa .= $AA_Codes[substr($matches[3][0],$i,1)];
				}
			}
			else {
				$caa = $row[7];
			}
*/
            if (strstr($vt, 'splicing') || strstr($gl, 'splicing')) {
                $vars[$row[0]]['g'][$row[1]][$row[2]] = "splicing,$gl,$vt,$cnt,$caa";
            } else {
                $vars[$row[0]]['g'][$row[1]][$row[2]] = $row[3] . ",$gl,$vt,$cnt,$caa";
            }
        }
    }
    // meta-data : samples with variants.
    $samples = array();
    $rows = runQuery("SELECT id, Name, gender FROM `Samples` WHERE id IN (" . implode(",", array_keys($results)) . ")", "Samples");
    foreach ($rows as $k => $row) {
        $samples[$row[0]]['n'] = $row[1];
        $samples[$row[0]]['g'] = $row[2];
    }
    // meta-data : parents

    $rows = runQuery("SELECT s.Name,s.gender,ss.sid2, s.id FROM `Samples` s JOIN `Samples_x_Samples` ss ON ss.sid1 = s.id WHERE ss.Relation = '2' AND sid2 IN (" . implode(",", array_keys($results)) . ")", "Samples:Samples_x_Samples");
    foreach ($rows as $k => $row) {
        $samples[$row[2]]["parent:" . $row[1]] = $row[0];
    }
    // construct output.
    foreach ($results as $sid => $vid_array) {
        // sample info
        $sample = $samples[$sid]['n'] . ',' . $samples[$sid]['g'] . ",$project,";
        if (array_key_exists("parent:Male", $samples[$sid])) {
            $sample .= $samples[$sid]["parent:Male"] . ',';
        } else {
            $sample .= "na,";
        }
        if (array_key_exists("parent:Female", $samples[$sid])) {
            $sample .= $samples[$sid]["parent:Female"] . ',';
        } else {
            $sample .= "na,";
        }
        // variant info
        foreach ($vid_array as $vid => $info) {
            // add variant location, inh, class and depth.
            $sam_and_var = $sample . $vars[$vid]['l'] . ',' . $vars[$vid]['ra'] . '/' . $vars[$vid]['aa'] . ',' . $results[$sid][$vid];
            // then loop genes.
            foreach ($vars[$vid]['g'] as $gene => $tr_details) {
                // loop transcripts.
                foreach ($tr_details as $tr => $info) {
                    $out = $sam_and_var . ",$gene,$tr,$info\n";
                    echo $out;
                }
            }
        }
    }
    exit;
}

// DELETE
if ($action == 'd') {
    $pid = $_GET['p'];

    if (!is_numeric($pid) || $pid == '') {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
        echo "<p>Invalid ProjectID provided, or no access to the selected project.</p>";
        echo "<input type=button name='Close' value='Close' onClick=\"document.getElementById('overlay').style.display='none'\">";
        echo "</div>";
        exit;
    }
    // double check permissions.
    $row = array_shift(...[runQuery("SELECT pu.pid, p.Name, p.created FROM  `Projects_x_Users` pu  JOIN `Projects` p ON p.id = pu.pid WHERE pu.pid = '$pid' AND pu.uid = '$userid' AND pu.editsample = 1", "Projects_x_Users:Projects")]);
    if (count($row) == 0) {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
        echo "<p>Invalid ProjectID provided, or no access to the selected project.</p>";
        echo "<input type=button name='Close' value='Close' onClick=\"document.getElementById('overlay').style.display='none'\">";
        echo "</div>";
        exit;
    }
    $pname = $row['Name'];
    $date = substr($row['created'], 0, 10);;
    echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
    echo "<h3 style='text-align:left;'>Confirmation Needed : '$pname'</h3>";
    echo "<p  style='text-align:left;'>Please confirm that this project should be deleted. </p>";
    // get samples.
    $rows = runQuery("SELECT s.Name FROM `Samples` s JOIN `Projects_x_Samples` ps ON ps.sid = s.id WHERE ps.pid = '$pid'", "Samples:Projects_x_Samples");
    echo "<p style='text-align:left;'>The following samples in the project will be deleted:</p>";
    $list = '';
    echo "<p  style='text-align:left;padding-left:1em;'>";
    foreach ($rows as $k => $row) {
        $list .= $row['Name'] . ', ';
    }
    if ($list != '') {
        echo $list;
    } else {
        echo "Project does not have any samples";
    }
    echo "</p>";

    echo "<p  style='text-align:left;'><input type=button name='DeleteYes' value='Yes, Delete Project' OnClick='ConfirmedDeleteProject($userid,$pid)'> &nbsp; <input type=button name='DeleteNo' value='No, Keep It' onClick=\"document.getElementById('overlay').style.display='none'\"></p>";
    echo "</div>";
    exit;
}
// confirmed deletion => actual deletion !
if ($action == 'cd') {
    $pid = $_GET['p'];
    // double check permissions
    $row = runQuery("SELECT pu.pid FROM  `Projects_x_Users` pu  WHERE pu.pid = '$pid' AND pu.uid = '$userid' AND pu.editsample = 1", "Projects_x_Users");
    if (count($row) == 0) {
        echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
        echo "<h3 style='text-align:left;'>Access Denied</h3>";
        echo "<p>The project was not deleted. You can now close this screen</p>";
        echo "<p  style='text-align:left;'><input type=button name='close' value='Close' onClick=\"document.getElementById('overlay').style.display='none';location.reload()\"></form></p>";
        echo "</div>";

        exit;
    }
    // get samples.
    $rows = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$pid'", "Projects_x_Samples");
    $sidstr = '';
    foreach ($rows as $k => $row) {
        $sidstr .= $row['sid'] . ',';
    }
    if ($sidstr != '') {
        $sidstr = substr($sidstr, 0, -1);
    }
    // get all users to re-summarize.
    $users = runQuery("SELECT uid FROM `Projects_x_Users` WHERE pid = '$pid'", "Projects_x_Users");
    foreach ($users as $k => $user) {
        $this_uid = $user['uid'];
        doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$this_uid'", "Users");
    }
    // DELETE THE QUICK STUFF HERE 
    doQuery("DELETE FROM `Projects_x_Samples` WHERE pid = '$pid'", "Projects_x_Samples");
    // delete from projects_x_usergroups
    doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$pid'", "Projects_x_Usergroups");
    // delete from projects_x_users
    doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$pid'", "Projects_x_Users");
    // delete from projects
    doQuery("DELETE FROM `Projects` WHERE id = '$pid'", "Projects");
    // only handle if samples in project.
    if ($sidstr != '') {
        // delete from samples
        doQuery("DELETE FROM `Samples` WHERE id IN ($sidstr)", "Samples");
        // delete from samples_x_HPO
        doQuery("DELETE FROM `Samples_x_HPO_Terms` WHERE sid IN ($sidstr)", "Samples_x_HPO_Terms");
        // delete from Samples_x_Samples
        doQuery("DELETE FROM `Samples_x_Samples` WHERE sid1 IN ($sidstr)", "Samples_x_Samples");
        doQuery("DELETE FROM `Samples_x_Samples` WHERE sid2 IN ($sidstr)", "Samples_x_Samples");
        // delete from samples_x_Saved_Results*
        $srows = runQuery("SELECT set_id FROM `Samples_x_Saved_Results` WHERE sid in ($sidstr)", "Samples_x_Saved_Results");
        foreach ($srows as $k => $srow) {
            doQuery("DELETE FROM Samples_x_Saved_Results_Pages` WHERE `set_id` = '" . $srow['set_id'] . "'", "Samples_x_Saved_Results_Pages");
        }
        doQuery("DELETE FROM `Samples_x_Saved_Results` WHERE sid IN ($sidstr)", "Samples_x_Saved_Results");

        // write out the sidstring to the slow-query queue.
        $fh = fopen("$scriptdir/Query_Results/.delete_queue", "a");
        fwrite($fh, "$sidstr\n");
        fclose($fh);
    }
    // Delete datafiles.
    $storage = $datadir;
    if (file_exists($storage) && $storage != '' && $storage != '/') {
        $sids = explode(',', $sidstr);
        foreach ($sids as $key => $sid) {
            system("rm -f $storage$sid.*");
        }
    }

    // give feedback.
    echo "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
    echo "<h3 style='text-align:left;'>Project deleted</h3>";
    echo "<p>The project was deleted. You can now close this screen</p>";
    echo "<p  style='text-align:left;'><input type=button name='close' value='Close' onClick=\"document.getElementById('overlay').style.display='none';location.reload()\"></form></p>";
    echo "</div>";
    exit;
}
