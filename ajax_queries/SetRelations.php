<?php
// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}
$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_SESSION['userID'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// get Main details from post
$sid = $_POST['sid'];
$newgender = $_POST['gender'];
$newpid = $_POST['pid'];
$newsname = addslashes(urldecode($_POST['sname']));
$ic = $_POST['sic']; // isControl
$ia = $_POST['sia']; // isAffected

// free clinical text
$FreeClin = addslashes($_POST['FreeClin']); // escaped by javascript escape()

// get current details
$current_details = runQuery("SELECT Gender, affected, isControl FROM `Samples` WHERE id = '$sid'", "Samples");
$update = 0;
if ($newgender != $current_details['Gender'] || $ia != $current_details['affected'] || $ic != $current_details['isControl']) {
    $update = 1;
}

// UPDATE main details in db
doQuery("UPDATE `Samples` SET Name = '$newsname', Gender = '$newgender',IsControl = '$ic',affected = '$ia' WHERE id = '$sid'", "Samples");
// UPDATE Projects_x_Samples and remove empty projects.
$row = runQuery("SELECT pid FROM `Projects_x_Samples` WHERE sid = '$sid'", "Projects_x_Samples")[0];
$oldpid = $row['pid'];
if ($oldpid != $newpid) {
    $update = 1;
}

doQuery("UPDATE `Projects_x_Samples` SET pid = '$newpid' WHERE sid = '$sid'", "Projects_x_Samples");
$rows = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$oldpid'", "Projects_x_Samples");
if (count($rows) == 0) {
    // project empty => delete.
    doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$oldpid'", "Projects_x_Users");
    doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$oldpid'", "Projects_x_Usergroups");
    doQuery("DELETE FROM `Projects` WHERE id = '$oldpid'", "Projects");
}

// should we check for changes ? 
// update summary status
if ($update == 1) {
    doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE `id` IN ('$oldpid','$newpid')", "Projects:Summary");
    // get users.
    $rows = runQuery("SELECT `uid` FROM `Projects_x_Users` WHERE `pid` IN ('$oldpid','$newpid')", "Projects_x_Users");
    foreach ($rows as $row) {
        $uid = $row['uid'];
        doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE `id` = '$uid'", "Users:Summary");
    }
}

// UPDATE Relations
$posted = $_POST['posted'];
for ($i = 1; $i <= $posted; $i++) {
    $relation = $_POST["Relation$i"];
    $items = explode('-', $relation);
    if ($items[0] == 'DEL') {
        // relation to remove.
        switch ($items[1]) {
            case '4':
                // items[2] = child of items[3] in relationtype 2;	
                doQuery("DELETE FROM `Samples_x_Samples` WHERE Relation = '2' AND sid2 = '" . $items[2] . "' AND sid1 = '" . $items[3] . "'", "Samples_x_Samples:dyn_%_Variants_x_Samples");
                break;
            case '1':
            case '3':
            case '5':
            case '6':
                // reciprocal delete needed (Replicates, siblings and custom present in both directions)
                doQuery("DELETE FROM `Samples_x_Samples` WHERE Relation = '" . $items[1] . "' AND sid2 = '" . $items[2] . "' AND sid1 = '" . $items[3] . "'", "Samples_x_Samples:dyn_%_Variants_x_Samples");
                doQuery("DELETE FROM `Samples_x_Samples` WHERE Relation = '" . $items[1] . "' AND sid1 = '" . $items[2] . "' AND sid2 = '" . $items[3] . "'", "Samples_x_Samples:dyn_%_Variants_x_Samples");
                break;
            case '2';
                // delete regularly (items[2] is parent of items[3])
                doQuery("DELETE FROM `Samples_x_Samples` WHERE Relation = '" . $items[1] . "' AND sid1 = '" . $items[2] . "' AND sid2 = '" . $items[3] . "'", "Samples_x_Samples:dyn_%_Variants_x_Samples");
                break;
        }
    } elseif ($items[0] == 'SET') {
        // relation to add.
        switch ($items[1]) {
            case '4':
                // items[2] = child of items[3] => Convert to item[3] is parent of item[2] in relationtype 2
                insertQuery("INSERT INTO `Samples_x_Samples` (Relation, sid2, sid1) VALUES ('2','" . $items[2] . "','" . $items[3] . "') ON DUPLICATE KEY UPDATE sid1=sid1", "Samples_x_Samples:dyn_%_Variants_x_Samples");
                break;
            case '1':
            case '3':
            case '5':
            case '6':
                // reciprocal delete needed (Replicates and siblings present in both directions)
                insertQuery("INSERT INTO `Samples_x_Samples` (Relation, sid2,sid1) VALUES ('" . $items[1] . "','" . $items[2] . "','" . $items[3] . "') ON DUPLICATE KEY UPDATE sid1=sid1", "Samples_x_Samples:dyn_%_Variants_x_Samples");
                insertQuery("INSERT INTO `Samples_x_Samples` (Relation, sid1,sid2) VALUES ('" . $items[1] . "','" . $items[2] . "','" . $items[3] . "') ON DUPLICATE KEY UPDATE sid1=sid1", "Samples_x_Samples:dyn_%_Variants_x_Samples");

                break;
            case '2';
                // delete regularly (items[2] is parent of items[3])
                insertQuery("INSERT INTO `Samples_x_Samples` (Relation,sid1,sid2) VALUES ('" . $items[1] . "','" . $items[2] . "','" . $items[3] . "') ON DUPLICATE KEY UPDATE sid1=sid1 ", "Samples_x_Samples:dyn_%_Variants_x_Samples");
                break;
        }
    }
}
// update clinical info
doQuery("UPDATE `Samples` SET clinical = '$FreeClin' WHERE id = '$sid'", "Samples");
// exit with successcode
exit(1);
