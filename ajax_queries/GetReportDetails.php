<?php
if (!isset($_GET['r']) || !is_numeric($_GET['r'])) {
    echo "invalid report id provided.";
    exit;
}
$rid = $_GET['r'];
//#######################
//# CONNECT TO DATABASE #
//#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
$uid = $_SESSION['userID'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// permission ok?
$perms = runQuery("SELECT rid FROM `Report_Definitions` WHERE rid = '$rid' AND (Owner = '$uid' OR Public = 1)", "Report_Definitions");
if (count($perms) == 0) {
    $shared = runQuery("SELECT rid FROM `Users_x_Report_Definitions` WHERE rid = '$rid' AND uid = '$uid'", "Users_x_Report_Definitions");
    if (count($shared) == 0) {
        echo "No access to provided filter.";
        exit;
    }
}

// get details.
$details = runQuery("SELECT Name, Description, Sections FROM `Report_Definitions` WHERE rid = '$rid'", "Report_Definitions")[0];
$sections = runQuery("SELECT rs.rsid, rs.Name, rs.Description, rs.Annotations, uf.FilterName, uf.Comments FROM `Report_Sections` rs JOIN `Users_x_FilterSettings` uf ON rs.FilterSet = uf.fid WHERE rs.rsid IN (" . $details['Sections'] . ")", "Report_Sections:Users_x_FilterSettings");
foreach ($sections as $k => $row) {
    $details['sections_details'][$row['rsid']] = $row;
}
echo "<p ><span class=emph>Template Details:</span> " . $details['Name'] . "<ul>";
echo "<li><span class='italic underline'>Description:</span><div class=indent>" . $details['Description'] . "</div></li>";
echo "<li><span class='italic underline'>Included Sections:</span><ol>";
foreach ($details['sections_details'] as $sd) {
    echo "<li>" . $sd['Name'] . "<ul>";
    echo "  <li>Filter: " . $sd['FilterName'] . "</li>";
    echo "  <li>Comments: " . $sd['Comments'] . "</li>";
    echo "</ul></li>";
}
echo "</ol></li></p>";
