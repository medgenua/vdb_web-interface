<?php
include('../includes/inc_logging.inc');

$setid = $_GET['setid'];
if (!is_numeric($setid)) {
    echo "Invalid Set ID";
    exit;
}
#######################
# CONNECT TO DATABASE # ASC
#######################
include('../.LoadCredentials.php');

$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$uid = $_SESSION['userID'];

if (!is_numeric($uid)) {
    echo "invalid userid : '$uid'";
    exit;
}

if (!isset($_GET['get'])) {
    echo "action not specified.";
    exit;
}
$get_array = array('rules' => 'set_filters', 'anno' => 'set_annotations', 'tree' => 'set_filtertree');
$get = $get_array[$_GET['get']];
// get filter rules
$row = runQuery("SELECT `$get` FROM `Samples_x_Saved_Results` WHERE set_id = '$setid'", "Samples_x_Saved_Results")[0];
echo stripslashes($row[$get]);
exit;
