<?php

$uid = $_POST['uid'];
$sid = $_POST['sid'];
$set_id = $_POST['setid'];

#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');


// allowed ? only the one who validated or deleted the set can undelete. 
$row = runQuery("SELECT uid, `validation_uid`, `delete_uid` FROM `Samples_x_Saved_Results` WHERE `set_id` = '$set_id'", "Samples_x_Saved_Results")[0];
if ($uid != $row['validation_uid'] && $uid != $row['delete_uid']) {
    echo "ERROR: You are not allowed to undelete this set.";
    exit;
}

// set to deleted.
doQuery("UPDATE `Samples_x_Saved_Results` SET deleted = 0, delete_uid = '', delete_comments = '' WHERE set_id = '$set_id'", "Samples_x_Saved_Results");
// add to log 
//TODO

echo "OK";
exit();
