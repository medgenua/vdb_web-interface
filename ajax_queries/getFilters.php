<?php

// GOAL : LOAD list of saved & shared filter sets


#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
$uid = $_SESSION['userID'];
//$uid = $_GET['uid'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// output 
echo "<h3>Preset Filters</h3><p >";
echo "<p><input type=submit onClick='WrapApplyFilter($uid)' value='Apply Filter'></p>";

echo "<form name='Storedfilters'>";
// get results
$rows = runQuery("SELECT fid, FilterName,Comments,Highlight FROM `Users_x_FilterSettings` WHERE uid = $uid ORDER BY Highlight DESC, FilterName ASC", "Users_x_FilterSettings");
echo "<span class=emph>My Filters</span><br/>";
if (count($rows) == 0) {
    echo " &nbsp; No saved filtersettings available.";
} else {
    echo "<form name='Storedfilters'>";
    foreach ($rows as $k => $row) {
        $row['Comments'] = str_replace("'", "&#39;", $row['Comments']);
        $icons = " <img src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em' onClick=\"DeleteSetting('Filter','" . $row['fid'] . "','$uid',getFilters)\" />  <img src='Images/layout/share_icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareSetting('Filter','" . $row['fid'] . "','$uid')\" />";
        echo " &nbsp; <input type=checkbox name='filters' value='" . $row['fid'] . "'>";
        if ($row['Highlight'] == 1) {
            echo "<img src='Images/layout/fav_full.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ManageFavorites('OwnFilter','" . $row['fid'] . "','0',getFilters)\" />";
            echo " <span id='f_name_" . $row['fid'] . "' title='" . $row['Comments'] . "'> " . $row['FilterName'] . "</span> ";
        } else {
            echo "<span id='f_name_" . $row['fid'] . "' title='" . $row['Comments'] . "'> " . $row['FilterName'] . "</span>";
            echo " <img src='Images/layout/fav_empty.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ManageFavorites('OwnFilter','" . $row['fid'] . "','1',getFilters)\" /> ";
        }
        echo $icons;
        echo "<br/>";
    }
}

// shared filters.
echo "<br/><span class=emph>Shared Filters</span><br/>";
$rows = runQuery("SELECT uf.fid, uf.FilterName,uf.Comments,us.Highlight FROM `Users_x_FilterSettings` uf JOIN `Users_x_Shared_Filters` us ON us.fid = uf.fid WHERE us.uid = $uid ORDER BY us.Highlight DESC, uf.FilterName ASC", "Users_x_FilterSettings:Users_x_Shared_Filters");
if (count($rows) == 0) {
    echo " &nbsp; No shared filters available.";
} else {
    foreach ($rows as $k => $row) {
        $row['Comments'] = str_replace("'", "&#39;", $row['Comments']);
        $icons = " <img src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em' onClick=\"DeleteSetting('Filter','" . $row['fid'] . "','$uid',getFilters)\" /> ";
        echo " &nbsp; <input type=checkbox name='filters' value='" . $row['fid'] . "'>";
        if ($row['Highlight'] == 1) {
            echo "<img src='Images/layout/fav_full.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ManageFavorites('SharedFilter','" . $row['fid'] . "','0',getFilters)\" /> ";
            echo "<span id='f_name_" . $row['fid'] . "' title='" . $row['Comments'] . "'> " . $row['FilterName'] . "</span>";
        } else {
            echo "<span id='f_name_" . $row['fid'] . "' title='" . $row['Comments'] . "'> " . $row['FilterName'] . "</span>";
            echo " <img src='Images/layout/fav_empty.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ManageFavorites('SharedFilter','" . $row['fid'] . "','1',getFilters)\" /> ";
        }
        echo $icons;
        echo "<br/>";
    }
}
echo "</form>";
echo "</p>";
