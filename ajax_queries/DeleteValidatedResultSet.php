<?php

$uid = $_POST['uid'];
$sid = $_POST['sid'];
$set_id = $_POST['setid'];
$action = $_POST['a'];
$comments = addslashes($_POST['comments']);

#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');


// allowed ? only the one who validated the set can delete. 
$row = runQuery("SELECT uid, `validation_uid` FROM `Samples_x_Saved_Results` WHERE `set_id` = '$set_id'", "Samples_x_Saved_Results")[0];
if ($uid != $row['validation_uid']) {
    echo "ERROR: You are not allowed to edit this validated set.";
    exit;
}
if ($action == 'd') {
    // set to deleted.
    doQuery("UPDATE `Samples_x_Saved_Results` SET deleted = 1, delete_uid = '$uid', delete_comments = '$comments' WHERE set_id = '$set_id'", "Samples_x_Saved_Results");
} elseif ($action == 'u') {
    // set to "unvalidate"
    doQuery("UPDATE `Samples_x_Saved_Results` SET `Validated` = 0, `validation_uid` = 0, `Validation_Comments` = '$comments' WHERE set_id = '$set_id'", "Samples_x_Saved_Results");
}
// add to log 
//TODO : what log, log is variant based.

echo "OK";
exit();
