<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
require("../includes/inc_filter_functions.inc");
include('../includes/inc_logging.inc');

// load xml library
require_once('../xmlLib2.php');
// read the filter config xml
$configuration = my_xml2array("../Filter/Filter_Options.xml");

// FROM SAVED FILTER
if (isset($_GET['fids'])) {
    ## GET MAIN POSTED VARIABLES
    $fids = $_GET['fids'];
    $uid = $_GET['uid'];
    $listed = $_GET['listed'];
    $sid = $_GET['sid'];
    if ($sid == 'project') {
        $pid = $_GET['pid'];
    }
    if ($sid == 'region') {
        $region = $_GET['region'];
    }
    // this will hold the results.
    $result = array('listed' => $listed, 'comment' => '', 'rules' => array(), 'jstrees' => array());

    $clear_cookies = '';
    $clear_dyn = '';
    // process different filters.
    foreach (explode(",", $fids) as $fid) {
        $query = "SELECT `FilterName`, `FilterSettings`, `FilterTree` FROM `Users_x_FilterSettings` WHERE `uid` = '$uid' AND `fid` = '$fid'";
        $frow = array_shift(...[runQuery($query, "Users_x_FilterSettings")]);
        // no results : shared ? 
        if (!$frow || count($frow) == 0) {
            $query = "SELECT f.`FilterName`, f.`FilterSettings`, f.`FilterTree` FROM `Users_x_FilterSettings` f JOIN `Users_x_Shared_Filters` us ON us.fid = f.fid WHERE us.`uid` = '$uid' AND us.`fid` = '$fid'";
            $frow = array_shift(...[runQuery($query, "Users_x_FilterSettings")]);
            if (count($frow) == 0) {
                $comments .= "Filter $fid not loaded : No access.<br/>";
                continue;
            }
        }
        // get tree
        $jstree = $frow['FilterTree'];
        $result['comment'] .= $frow['FilterName'] . ",";
        // settings
        $settings = array();
        $cats = array();
        foreach (explode("@@@", $frow['FilterSettings']) as $set) {
            if ($set == '') {
                continue;
            }
            list($k, $v) = explode('|||', $set);
            $settings[$k] = $v;
            if (substr($k, 0, 8) == 'category') {
                array_push($cats, $k);
            }
        }
        // go through categories, and build the table rows.
        foreach ($cats as $cat_idx) {
            $old_nr = substr($cat_idx, 8);
            $listed++;
            $new_nr = $listed;
            // clear old cookies.
            $clear_cookies .= "'$uid" . "_N$listed','$uid" . "_C$listed','$uid" . "_P$listed','$uid" . "_A$listed','$uid" . "_V$listed','$uid" . "_s$listed','$uid" . "\_nr\_do\_$listed',";
            $clear_dyn .= "`ckey` LIKE '$uid" . "\_do\_$listed" . "\_%' OR `ckey` LIKE '$uid" . "\_dyn$listed" . "\_%' OR ";

            // update jstree
            $jstree = str_replace("rule_" . $old_nr . '\"', "rule_" . $new_nr . 'tmpstr\"', $jstree);
            $jstree = str_replace("rule_" . $old_nr . '_', "rule_" . $new_nr . 'tmpstr_', $jstree);

            // get filter details.
            $category = $settings[$cat_idx];
            $params = $settings["param$old_nr"];
            $negate = $settings["negate$old_nr"];
            if (isset($settings["ssqv$old_nr"])) {
                $ssqv = $settings["ssqv$old_nr"];
                //trigger_error("ssqv : $ssqv");
            } else {
                $ssqv = "";
            }
            if ($category == 'Dynamic_Filters') {
                // get options. 
                $nr_do = $settings["nr_do_$old_nr"];
                $options = array("nr_do_$old_nr" => $nr_do);
                for ($i = 1; $i <= $nr_do; $i++) {
                    //$options["do$old_nr"."_$i"."_name"] = $settings["do$old_nr"."_$i"."_name"];
                    //$options["do$old_nr"."_$i"."_value"] = $settings["do$old_nr"."_$i"."_value"];
                    // by name
                    $options[$settings["do$old_nr" . "_$i" . "_name"]] = $settings["do$old_nr" . "_$i" . "_value"];
                }
                // get info.
                $dyn = GetDynamic($category, $params, $options, $new_nr);
                // add to results.
                $result['rules'][$new_nr] = array('c' => $category, 'v' => array($negate, $dyn['first'], $dyn['second'], ''));
                continue;
            }
            $arg = (isset($settings["argument$old_nr"])) ? $settings["argument$old_nr"] : '';
            $values = (isset($settings["value$old_nr"])) ? $settings["value$old_nr"] : '';
            // first level
            $first_level = GetSets($category, $params, $new_nr);
            // subselect in first level
            $subselect = GetSubSelect($category, $params, $new_nr, $ssqv);
            $first_level['result'] .= '<span id=ss' . $new_nr . '>' . $subselect['result'] . '</span>';
            // second level
            $second_level = GetArguments($category, $params, $arg, $values, $new_nr);
            // third level.
            if (!isset($second_level['skip_values']) || $second_level['skip_values'] == 0) {
                $third_level = GetValues($category, $params, $values, $new_nr);
            } else {
                $third_level = array('status' => 'ok', 'result' => '');
            }
            $result['rules'][$new_nr] = array('c' => $category, 'v' => array($negate, $first_level, $second_level, $third_level));
        }
        $result['listed'] = $listed;
        $jstree = str_replace("tmpstr", "", $jstree);

        $result['jstrees'][$fid] = stripslashes($jstree);
    }
    // clear cookies.
    if ($clear_cookies != '') {
        doQuery("DELETE FROM `Cookies` WHERE ckey IN (" . substr($clear_cookies, 0, -1) . ")", "Cookies");
        doQuery("DELETE FROM `Cookies` WHERE " . substr($clear_dyn, 0, -4), "Cookies");
    }
    echo json_encode($result);
    exit;
}
// From Cookies
elseif (isset($_GET['fc'])) {
    ## GET MAIN POSTED VARIABLES
    $uid = $_GET['uid'];
    $listed = 0;
    $sid = $_GET['sid'];
    if ($sid == 'project') {
        $pid = $_GET['pid'];
    }
    if ($sid == 'region') {
        $region = $_GET['region'];
    }
    // this will hold the results.
    $result = array('listed' => $listed, 'comment' => '', 'rules' => array(), 'jstree' => '');

    $clear_cookies = '';
    $clear_dyn = '';
    // process different filters.
    $jsrow = array_shift(...[runQuery("SELECT `cvalue` FROM `Cookies` WHERE `ckey` = '$uid" . "_FCs'", "Cookies")]);
    // no data
    if (empty($jsrow)) {
        echo json_encode($result);
        exit;
    }
    $jstree = $jsrow['cvalue'];
    // settings
    $settings = array();
    $cats = array();
    $rows = runQuery("SELECT `ckey`, `cvalue` FROM `Cookies` WHERE `ckey` LIKE '$uid" . "\_%'", "Cookies");
    foreach ($rows as $row) {
        list($tmp, $k) = explode("_", $row['ckey'], 2);
        if ($k == 'FCs' || $k == 'FS') {
            continue;
        }
        $settings[$k] = $row['cvalue'];
        if (substr($k, 0, 1) == 'C') {
            array_push($cats, $k);
        }
    }
    // go through categories, and build the table rows.
    foreach ($cats as $cat_idx) {
        $old_nr = substr($cat_idx, 1);
        $listed++;
        $new_nr = $listed;
        // clear old cookies.
        $clear_cookies .= "'$uid" . "_N$listed','$uid" . "_C$listed','$uid" . "_P$listed','$uid" . "_A$listed','$uid" . "_V$listed','$uid" . "_s$listed','$uid" . "\_nr\_do\_$listed',";
        $clear_dyn .= "`ckey` LIKE '$uid" . "\_do\_$listed" . "\_%' OR `ckey` LIKE '$uid" . "\_dyn$listed" . "\_%' OR ";

        // update jstree
        $jstree = str_replace("rule_" . $old_nr . '"', "rule_" . $new_nr . 'tmpstr"', $jstree);
        $jstree = str_replace("rule_" . $old_nr . '_', "rule_" . $new_nr . 'tmpstr_', $jstree);

        // get filter details.
        $category = $settings[$cat_idx];
        $params = $settings["P$old_nr"];
        $negate = $settings["N$old_nr"];
        if (isset($settings["s$old_nr"])) {
            $ssqv = $settings["s$old_nr"];
        } else {
            $ssqv = "";
        }
        if ($category == 'Dynamic_Filters') {
            // get options. 
            $nr_do = $settings["nr_do_$old_nr"];
            $options = array("nr_do_$old_nr" => $nr_do);
            for ($i = 1; $i <= $nr_do; $i++) {
                // numbered (saved style)
                //$options["do_$old_nr"."_$i"."_name"] = $settings["do_$old_nr"."_$i"."_name"];
                //$options["do_$old_nr"."_$i"."_value"] = $settings["do_$old_nr"."_$i"."_value"];
                // by name
                $options[$settings["do_$old_nr" . "_$i" . "_name"]] = $settings["do_$old_nr" . "_$i" . "_value"];
            }

            // get info.
            $dyn = GetDynamic($category, $params, $options, $new_nr);
            // add to results.
            $result['rules'][$new_nr] = array('c' => $category, 'v' => array($negate, $dyn['first'], $dyn['second'], ''));
            //$result['rules'][$new_nr] = array('c' => $category,'v' => array($negate,$dyn));
            continue;
        }

        $arg = (isset($settings["A$old_nr"])) ? $settings["A$old_nr"] : '';
        $values = (isset($settings["V$old_nr"])) ? $settings["V$old_nr"] : '';
        // first level
        $first_level = GetSets($category, $params, $new_nr);
        // subselect in first level
        $subselect = GetSubSelect($category, $params, $new_nr, $ssqv);
        $first_level['result'] .= '<span id=ss' . $new_nr . '>' . $subselect['result'] . '</span>';
        // second level
        $second_level = GetArguments($category, $params, $arg, $values, $new_nr);
        // third level.
        $third_level = GetValues($category, $params, $values, $new_nr);
        $result['rules'][$new_nr] = array('c' => $category, 'v' => array($negate, $first_level, $second_level, $third_level));
    }
    $result['listed'] = $listed;
    $jstree = str_replace("tmpstr", "", $jstree);
    $result['jstree'] = $jstree;


    // clear cookies.
    if ($clear_cookies != '') {
        doQuery("DELETE FROM `Cookies` WHERE ckey IN (" . substr($clear_cookies, 0, -1) . ")", "Cookies");
        doQuery("DELETE FROM `Cookies` WHERE " . substr($clear_dyn, 0, -4), "Cookies");
    }
    echo json_encode($result);
    exit;
}
