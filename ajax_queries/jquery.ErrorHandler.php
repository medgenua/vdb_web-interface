<?php
// This script is used to route errors to the system admin. 
// we do this by redirecting it to the php logging routine. 
// messages are sent from javascript/global.js

// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

$error_msg = $_POST['source'] . " " . $_POST['status'] . " " . $_POST['code']  . " : " . $_POST['msg'] . " : " . $_POST['url'];

// there is some issue with the autosuggest routine that can't be fixed. so skip errors for that one..
if (strpos($error_msg, 'bsn.AutoSuggest') === false) {
    trigger_error($error_msg, E_USER_ERROR);
} else {
    trigger_error($error_msg, E_USER_NOTICE);
}

echo "Msg Sent.";
