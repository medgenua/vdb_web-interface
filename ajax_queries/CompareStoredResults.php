<?php
include('../includes/inc_logging.inc');

// variables
$sid = $_POST['sid'];
$uid = $_POST['uid'];
$setid = $_POST['setid'];
// check for illegal values 
if (!is_numeric($sid) || !is_numeric($uid) || !is_numeric($setid)) {
    echo "ILLEGAL VARIABLES PROVIDED. <br/>";
    //echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";
    exit();
}

$inh = array(0 => 'ND', 1 => 'P', 2 =>  'M', 3 => 'DN', 4 => 'Both', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined', 'Both' => 'Both Parents');
$inhm = array(0 => '-', 1 => 'Dominant', 2 => 'Recessive', '3' => 'Unknown');
$classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');


#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

// 0. check permission. 
$rows = runQuery("SELECT ps.pid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid Where ps.sid = '$sid' AND pu.uid = '$uid'", "Projects_x_Samples:Projects_x_Users");
if (count($rows) == 0) {
    echo "<h3>Permission denied</h3>";
    echo "<p>You do not have access to this sample.</p>";
    echo "<p><input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    exit();
}
// get vidlist.
$r = runQuery("SELECT `set_vids` FROM `Samples_x_Saved_Results` WHERE `set_id` = '$setid' AND `sid` = '$sid'", "Samples_x_Saved_Results")[0];
$vidlist = $r['set_vids'];

if ($vidlist == 'Queued') {
    echo "<h3>Results not ready</h3>";
    echo "<p>The query is still queued for execution. Please try again later.</p>";
    echo "<p><input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
    exit();
}
if ($vidlist == '') {
    $vidlist = '0';
}
// GET vid details from DB ('live' data)
$db_vids = runQuery("SELECT vid, inheritance, InheritanceMode,class,validation,validationdetails FROM `Variants_x_Samples` WHERE sid = '$sid' AND vid IN ($vidlist)", "Variants_x_Samples");

// get pages.
$rows = runQuery("SELECT contents FROM `Samples_x_Saved_Results_Pages` WHERE set_id = '$setid' ORDER BY page ASC", "Samples_x_Saved_Results_Pages");
$all_contents = '';
foreach ($rows as $page) {
    $all_contents .= $page['contents'];
}
// make searchable DOM
$internalErrors = libxml_use_internal_errors(true);
$doc = new DomDocument;
if ($all_contents != '') {
    $doc->loadHTML($all_contents);
}
// compare results.
$differences = '';
foreach ($db_vids as $k => $db_row) {
    $changed = '';
    // get variantID
    $vid = $db_row['vid'];
    // get stored values.
    list($sdc, $sinh, $sihm, $sval, $svd) = explode("|", $doc->getElementById("sv_$vid")->nodeValue);
    if ($sdc != $db_row['class']) {
        $changed .= "<li>DC: " . $classes[$db_row['class']] . " => " . $classes[$sdc] . "</li>";
    }
    if ($sinh != $db_row['inheritance']) {
        $changed .= "<li>Inheritance: " . $inh[$inh[$db_row['inheritance']]] . " => " . $inh[$inh[$sinh]] . "</li>";
    }
    if ($sihm != $db_row['InheritanceMode']) {
        $changed .= "<li>Inheritance Mode: " . $inhm[$db_row['InheritanceMode']] . " => " . $inhm[$sihm] . "</li>";
    }
    if ($sval != $db_row['validation']) {
        $changed .= "<li>Validation: " . $db_row['validation'] . " => " . $sval . "</li>";
    }
    if ($svd != $db_row['validationdetails']) {
        $changed .= "<li>Validation Details: " . $db_row['validationdetails'] . " => " . $svd . "</li>";
    }
    // print if anything changed.
    if ($changed != '') {
        // get variant details.
        $v = runQuery("SELECT chr, start, RefAllele, AltAllele FROM `Variants` WHERE id = '$vid'", "Variants")[0];
        echo "Variant: chr" . $v['chr'] . ":" . $v['start'] . " : " . $v['RefAllele'] . "/" . $v['AltAllele'] . ":<ul class=compact>$changed</ul>";
        $differences .= "$vid|" . $doc->getElementById("sv_$vid")->nodeValue . "@@@";
    }
}
if ($differences != '') {
    echo "<span id='all_diffs' style='display:none'>$differences</span>";
}
