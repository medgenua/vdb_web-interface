<?php

// variables
$vid = (isset($_GET['vid']) ? $_GET['vid'] : $vid);

// check for illegal values 
if (!is_numeric($vid)) {
    echo "ILLEGAL VARIABLES PROVIDED. <br/>";
    exit();
}

$AA_Codes = array('G' => 'Gly', 'A' => 'Ala', 'L' => 'Leu', 'M' => 'Met', 'F' => 'Phe', 'W' => 'Trp', 'K' => 'Lys', 'Q' => 'Gln', 'E' => 'Glu', 'S' => 'Ser', 'P' => 'Pro', 'V' => 'Val', 'I' => 'Ile', 'C' => 'Cys', 'Y' => 'Tyr', 'H' => 'His', 'R' => 'Arg', 'N' => 'Asn', 'D' => 'Asp', 'T' => 'Thr', 'X' => 'X');

#######################
# CONNECT TO DATABASE #
#######################
// called/included from two locations...
// options one : through jquery
if (file_exists('../.LoadCredentials.php')) {
    include('../.LoadCredentials.php');
    require("../includes/inc_query_functions.inc");
    include('../includes/inc_logging.inc');
}
// option two : included : logging & query already active.
elseif (file_exists('.LoadCredentials.php')) {
    include('.LoadCredentials.php');
} else {
    error_log("File not found : .LoadCredentials.php");
    echo "Error in loading variant annotations. Please report";
    exit;
}
$db = "NGS-Variants" . $_SESSION['dbname'];

// userid
$uid = (isset($_GET['uid']) ? $_GET['uid'] : $_SESSION['userID']);


// depends on ajax vs include call.
if (file_exists("../includes/inc_query_functions.inc")) {
    require_once("../includes/inc_query_functions.inc");
} elseif (file_exists("includes/inc_query_functions.inc")) {
    require_once("includes/inc_query_functions.inc");
} else {
    error_log("File not found : includes/inc_query_functions.inc");
    echo "Error in loading variant annotations. Please report";
    exit;
}

// output 
$output = "<p style='margin-left:-0.5em;margin-top:0em;margin-bottom:-0.8em;'><span class='italic underline'  >Annotation Summary:</span></p>";

// value codes
$codes = array();
$rows = runQuery("SELECT id, `Item_Value`, `Table_x_Column` FROM `Value_Codes`", "Value_Codes");
foreach ($rows as $key => $row) {
    $codes[$row['Table_x_Column'] . '|' . $row['id']] = $row['Item_Value'];
}
// refseq
$rows = runQuery("SELECT GeneSymbol,Transcript,Exon,GeneLocation,VariantType,CPointNT,CPointAA FROM `Variants_x_ANNOVAR_ncbigene` WHERE vid = '$vid'", "Variants_x_ANNOVAR_ncbigene");
$rs = array();
$rs_nr = 0;
foreach ($rows as $key => $row) {
    $rs_nr++;
    // one transcript per variant type.
    $vtype = ($codes['Variants_x_ANNOVAR_ncbigene_VariantType' . "|" . $row['VariantType']] == '') ? '.' : $codes['Variants_x_ANNOVAR_ncbigene_VariantType' . "|" . $row['VariantType']];
    $row['Transcript'] = ($row['Transcript'] == '') ? '.' : $row['Transcript'];
    $row['Exon'] = ($row['Exon'] == '') ? '.' : $row['Exon'];
    $row['CPointNT'] = ($row['CPointNT'] == '') ? '.' : $row['CPointNT'];
    $row['CPointAA'] = ($row['CPointAA'] == '') ? '.' : $row['CPointAA'];
    $gl = ($codes['Variants_x_ANNOVAR_ncbigene_GeneLocation' . "|" . $row['GeneLocation']] == '') ? '.' : $codes['Variants_x_ANNOVAR_ncbigene_GeneLocation' . "|" . $row['GeneLocation']];

    $rs[$row['GeneSymbol']][$vtype]['tr'] = $row['Transcript'];
    $rs[$row['GeneSymbol']][$vtype]['ex'] = $row['Exon'];
    $rs[$row['GeneSymbol']][$vtype]['gl'] = $gl;
    $rs[$row['GeneSymbol']][$vtype]['CNT'] = $row['CPointNT'];
    $rs[$row['GeneSymbol']][$vtype]['CAA'] = $row['CPointAA'];
}
if (count($rs) > 0) {
    $output .= "<p style='font-size:0.9em;'><span class=emph>RefSeq:</span> <span class=italic>$rs_nr hits</span><br/>";
    $output .= "<table cellspacing=0 class=w95 style='margin-top:-1em;margin-bottom:-2em;font-size:0.8em;margin-left:2em;' >";
    $output .= "<tr><th class=top>Gene</th><th class=top>Transcript</th><th class=top>Effect</th><th class=top>Location</th><th class=top>Exon</th><th class=top>cPOINT</th></tr>";
    foreach ($rs as $gene => $effects) {
        foreach ($effects as $effect => $details) {
            // rewrite cpointAA
            $value = $details['CAA'];
            $svn = '';
            for ($svi = 0; $svi < strlen($value); $svi++) {
                if (preg_match("/[A-Z]/", substr($value, $svi, 1))) {
                    $svn .= $AA_Codes[substr($value, $svi, 1)];
                } else {
                    $svn .= substr($value, $svi, 1);
                }
            }
            $value = "$svn";
            $output .= "<tr><td>$gene</td><td>" . $details['tr'] . "</td><td>$effect</td><td>" . $details['gl'] . "</td><td>" . $details['ex'] . "</td><td>" . $details['CNT'] . " ; " . $value . "</td></tr>";
        }
    }
    $output .= "<tr><td class=last colspan=6>&nbsp;</td></tr></table></p>";
} else {
    $output .= "<p style='font-size:0.9em;'><span class=emph>RefSeq:</span> <span class=italic>no hits</span></p>";
}
// snpEff
$rows = runQuery("SELECT GeneSymbol,Transcript,Exon,Effect,EffectImpact,AAchange FROM `Variants_x_snpEff` WHERE vid = '$vid'", "Variants_x_snpEff");
$se = array();
$se_nr = 0;
foreach ($rows as $key => $row) {
    $se_nr++;
    // one transcript per variant type.
    $vtype = ($codes['Variants_x_snpEff_Effect' . "|" . $row['Effect']] == '') ? '.' : $codes['Variants_x_snpEff_Effect' . "|" . $row['Effect']];
    $vimpact = ($codes['Variants_x_snpEff_EffectImpact' . "|" . $row['EffectImpact']] == '') ? '.' : $codes['Variants_x_snpEff_EffectImpact' . "|" . $row['EffectImpact']];

    $row['Transcript'] = ($row['Transcript'] == '') ? '.' : $row['Transcript'];
    $row['Exon'] = ($row['Exon'] == '') ? '.' : $row['Exon'];
    $row['AAchange'] = ($row['AAchange'] == '') ? '.' : $row['AAchange'];

    $se[$row['GeneSymbol']][$vtype]['tr'] = $row['Transcript'];
    $se[$row['GeneSymbol']][$vtype]['ex'] = $row['Exon'];
    $se[$row['GeneSymbol']][$vtype]['CAA'] = $row['AAchange'];
    $se[$row['GeneSymbol']][$vtype]['impact'] = $vimpact;
}
if (count($se) > 0) {
    $output .= "<p style='font-size:0.9em;'><span class=emph>SnpEffect:</span> <span class=italic>$se_nr hits</span><br/>";
    $output .= "<table cellspacing=0 class=w95 style='margin-top:-1em;margin-bottom:-2em;font-size:0.8em;margin-left:2em;' >";
    $output .= "<tr><th class=top>Gene</th><th class=top>Transcript</th><th class=top>Effect</th><th class=top>Impact</th><th class=top>Exon</th><th class=top>cPointAA</th></tr>";
    foreach ($se as $gene => $effects) {
        foreach ($effects as $effect => $details) {

            $output .= "<tr><td>$gene</td><td>" . $details['tr'] . "</td><td>$effect</td><td>" . $details['impact'] . "</td><td>" . $details['ex'] . "</td><td>" . $details['CAA'] . "</td></tr>";
        }
    }
    $output .= "<tr><td class=last colspan=6>&nbsp;</td></tr></table></p>";
} else {
    $output .= "<p style='font-size:0.9em;'><span class=emph>SnpEffect:</span> <span class=italic>no hits</span></p>";
}


// patho-predictions from dbNSFP
$rows = runQuery("SELECT * FROM `Variants_x_ANNOVAR_dbnsfp30a` WHERE vid = '$vid'", "Variants_x_ANNOVAR_dbnsfp30a");
$patho = "";
$p_nr = 0;
if (count($rows) > 0) {
    $test = array("SIFT_pred", "Polyphen2_HDIV_pred", "MutationTaster_pred", "MutationAssessor_pred", "PROVEAN_pred", "MetaSVM_pred");
    foreach ($test as $k => $algo) {
        if ($rows[0][$algo] != '.') {
            $score = str_replace("_pred", "_score", $algo);
            $name = str_replace("_pred", "", $algo);
            $name = str_replace("_score", "", $name);
            if (array_key_exists($score, $rows[0])) {
                $patho .= "<span style='padding-left:2em;'>$name: " . $rows[0][$algo] . " (" . $rows[0][$score] . ")</span>";
            } else {
                $patho .= "<span style='padding-left:2em;'>$name: " . $rows[0][$algo] . "</span>";
            }
            $p_nr++;
        }
    }
    //if ($rows[0]['CADD_phred'] > 0) {
    //	$patho .= "<span style='padding-left:2em;'>CADD_phred: ".$rows[0]['CADD_phred']."</span>";
    //	$p_nr++;
    //}
}
// CADD 1.4
$rows = runQuery("SELECT `Phred_Score` FROM `Variants_x_CADD_v1.4` WHERE vid = '$vid'", "Variants_x_CADD_v1.4");
if (count($rows) > 0) {
    $p_nr++;
    $row = $rows[0];
    $patho .= "<span style='padding-left:2em;'>CADDv1.4_Phred: " . $row['Phred_Score'] . "</span>";
}

if ($patho == '') {
    // try web-results for indels?
    // mutation taster
    $rows = runQuery("SELECT Score, Effect FROM `Variants_x_MutationTaster` WHERE vid = '$vid'", "Variants_x_MutationTaster");
    $web = array();
    foreach ($rows as $key => $row) {
        if ($row['Effect'] != '.') {
            $p_nr++;
            # only replace neutral events.
            if (array_key_exists('mt', $web) && ($web['mt']['p'] == 'N' || $web['mt']['P']) && ($web['mt']['s'] < $row['Score'])) {
                $web['mt']['t'] = "<span style='padding-left:2em;'>MutationTaster: " . $row['Effect'] . " (" . $row['Score'] . ")</span>";
                $web['mt']['p'] = $row['Effect'];
                $web['mt']['s'] = $row['Score'];
            } else {
                $web['mt']['t'] = "<span style='padding-left:2em;'>MutationTaster: " . $row['Effect'] . " (" . $row['Score'] . ")</span>";
                $web['mt']['p'] = $row['Effect'];
                $web['mt']['s'] = $row['Score'];
            }
        }
    }

    // sift
    $rows = runQuery("SELECT SIFTScore, SIFTEffect,PROVEANScore,PROVEANEffect FROM `Variants_x_SIFT` WHERE vid = '$vid'", "Variants_x_SIFT");
    foreach ($rows as $key => $row) {
        if ($row['SIFTEffect'] != '.') {
            $p_nr++;
            # only replace neutral events.
            if (array_key_exists('sift', $web) && ($web['sift']['p'] == 'T' || ($web['sift']['p'] != 'T' && $web['sift']['s'] < $row['SIFTScore']))) {
                $web['sift']['t'] = "<span style='padding-left:2em;'>SIFT: " . $row['SIFTEffect'] . " (" . $row['SIFTScore'] . ")</span>";
                $web['sift']['p'] = $row['SIFTEffect'];
                $web['sift']['s'] = $row['SIFTScore'];
            } else {
                $web['sift']['t'] = "<span style='padding-left:2em;'>SIFT: " . $row['SIFTEffect'] . " (" . $row['SIFTScore'] . ")</span>";
                $web['sift']['p'] = $row['SIFTEffect'];
                $web['sift']['s'] = $row['SIFTScore'];
            }
        }
        if ($row['SIFTEffect'] != '.') {
            $p_nr++;
            # only replace neutral events.
            if (array_key_exists('provean', $web) && ($web['provean']['p'] == 'N' || ($web['provean']['p'] != 'N' && $web['provean']['s'] < $row['PROVEANScore']))) {
                $web['provean']['t'] = "<span style='padding-left:2em;'>SIFT: " . $row['PROVEANEffect'] . " (" . $row['PROVEANScore'] . ")</span>";
                $web['provean']['p'] = $row['PROVEANEffect'];
                $web['provean']['s'] = $row['PROVEANScore'];
            } else {
                $web['provean']['t'] = "<span style='padding-left:2em;'>SIFT: " . $row['PROVEANEffect'] . " (" . $row['PROVEANScore'] . ")</span>";
                $web['provean']['p'] = $row['PROVEANEffect'];
                $web['provean']['s'] = $row['PROVEANScore'];
            }
        }
    }
    foreach ($web as $tool => $row) {
        $patho .= $web[$tool]['t'];
    }
    if ($patho != '') {
        $fromweb = " taken from webtools";
    }
}
if ($patho == '') {
    $output .= "<p style='font-size:0.9em;'><span class=emph>Patho.Predictions:</span> <span class=italic>no hits</span></p>";
} else {
    $output .= "<p style='font-size:0.9em;'><span class=emph>Patho.Predictions:</span> <span class=italic>$p_nr hits</span><br/>$patho</p>";
}
// clinvar
$rows = runQuery("SELECT vc.AA_MatchType, vc.Effect, vc.NP_ID, cvdb.Class, cvdb.ClassComment FROM `Variants_x_ClinVar` vc JOIN `ClinVar_db` cvdb ON vc.cvid = cvdb.id WHERE vid = '$vid'", "Variants_x_ClinVar");
$cv = array();
$cv_nr = 0;
foreach ($rows as $key => $row) {
    $cv_nr++;
    $mt = ($codes['Variants_x_ClinVar_AA_MatchType|' . $row['AA_MatchType']] == '') ? '.' : $codes['Variants_x_ClinVar_AA_MatchType|' . $row['AA_MatchType']];
    //$mt = 'Variants_x_ClinVar_AA_MatchType|'.$row['AA_MatchType'];
    $eff = $codes['Variants_x_ClinVar_Effect|' . $row['Effect']];
    $cpaa = ($row['NP_ID'] == '.') ? '.' : substr($row['NP_ID'], 12);
    $class = ($codes['Variants_x_ClinVar_Class|' . $row['Class']] == '') ? '.' : $codes['Variants_x_ClinVar_Class|' . $row['Class']];
    $classcomment = ($codes['Variants_x_ClinVar_ClassComment|' . $row['ClassComment']] == '') ? '.' : $codes['Variants_x_ClinVar_ClassComment|' . $row['ClassComment']];
    $cv[$cpaa . "|" . $class . "|" . $mt] = "<tr><td>$mt</td><td>$cpaa</td><td>$eff</td><td title='$classcomment'>$class</td></tr>";
}
if (count($cv) > 0) {
    $output .= "<p style='font-size:0.9em;'><span class=emph>ClinVar:</span> <span class=italic>$cv_nr hits</span><br/>";
    $output .= "<table cellspacing=0 class=w95 style='margin-top:-1em;margin-bottom:-2em;font-size:0.8em;margin-left:2em;' >";
    $output .= "<tr><th class=top>MatchType</th><th class=top>cPointAA</th><th class=top>Effect</th><th class=top>Class</th></tr>";
    foreach ($cv as $key => $entry) {
        $output .= $entry;
    }
    $output .= "<tr><td class=last colspan=4>&nbsp;</td></tr></table></p>";
} else {
    $output .= "<p style='font-size:0.9em;'><span class=emph>ClinVar:</span> <span class=italic>no hits</span></p>";
}

// population frequencies : own samples, snp148, exac, kaviar, ...
/*
$rows = runQuery("SELECT sid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu on ps.pid = pu.pid WHERE pu.uid = '$uid'","Projects_x_Samples:Projects_x_Users");
if (!is_array($rows[0])) $rows = OneToMulti($rows);
$sids = '';
$nr_sids = count($rows);
foreach($rows as $key => $row){
	$sids .= $row['sid'].",";
}
if ($sids != '') {
	$sids = substr($sids,0,-1);
	$rows = runQuery("SELECT sid FROM `Variants_x_Samples` WHERE vid = '$vid' AND sid IN ($sids) AND AltCount > 0","Variants_x_Samples");
	if (!is_array($rows[0])) $rows = OneToMulti($rows);
	$of = number_format(count($rows)/$nr_sids,3,'.','');
	$freq .= "<span style='padding-left:2em;' title='#seen in n samples'>Own Data: $of (n:$nr_sids)</span>";
}
// 1kg
$rows = runQuery("SELECT AlleleFreq FROM `Variants_x_ANNOVAR_1000g2014oct_all` WHERE vid = '$vid'","Variants_x_ANNOVAR_1000g2014oct_all");
if (!is_array($rows[0])) $rows = OneToMulti($rows);
$rows[0]['AlleleFreq'] = ($rows[0]['AlleleFreq'] == '') ? 'n/a' : $rows[0]['AlleleFreq'];
$rows[0]['AlleleFreq'] = ($rows[0]['AlleleFreq'] < 0) ? '.' : $rows[0]['AlleleFreq'];
$freq .= "<span style='padding-left:2em;' title='1000 genomes, october 2014 release, all populations'>1kgOct14: ".$rows[0]['AlleleFreq']."</span>";
// esp 6500
$rows = runQuery("SELECT AlleleFreq FROM `Variants_x_ANNOVAR_esp6500si_all` WHERE vid = '$vid'","Variants_x_ANNOVAR_esp6500si_all");
if (!is_array($rows[0])) $rows = OneToMulti($rows);
$rows[0]['AlleleFreq'] = ($rows[0]['AlleleFreq'] == '') ? 'n/a' : $rows[0]['AlleleFreq'];
$rows[0]['AlleleFreq'] = ($rows[0]['AlleleFreq'] < 0) ? '.' : $rows[0]['AlleleFreq'];
$freq .= "<span style='padding-left:2em;' title='Exome Sequencing Project, 6500 samples, all populations'>esp6500: ".$rows[0]['AlleleFreq']."</span>";
// exac v3
$rows = runQuery("SELECT ALL_AlleleFreq, ALL_PopSize FROM `Variants_x_ANNOVAR_exac03` WHERE vid = '$vid'","Variants_x_ANNOVAR_exac03");
if (!is_array($rows[0])) $rows = OneToMulti($rows);
$rows[0]['ALL_AlleleFreq'] = ($rows[0]['ALL_AlleleFreq'] == '') ? 'n/a' : $rows[0]['ALL_AlleleFreq'];
$rows[0]['ALL_AlleleFreq'] = ($rows[0]['ALL_AlleleFreq'] < 0) ? '.' : $rows[0]['ALL_AlleleFreq'];
$rows[0]['ALL_PopSize'] = ($rows[0]['ALL_PopSize'] == '') ? 'n/a' : $rows[0]['ALL_PopSize'];
$freq .= "<span style='padding-left:2em;' title='Exac v3, all populations'>exac03: ".$rows[0]['ALL_AlleleFreq']." (n:".$rows[0]['ALL_PopSize'].")</span>";
// exac v3 non_psych
$rows = runQuery("SELECT ALL_AlleleFreq, ALL_PopSize FROM `Variants_x_ANNOVAR_exac03_nonpsych` WHERE vid = '$vid'","Variants_x_ANNOVAR_exac03_nonpsych");
if (!is_array($rows[0])) $rows = OneToMulti($rows);
$rows[0]['ALL_AlleleFreq'] = ($rows[0]['ALL_AlleleFreq'] == '') ? 'n/a' : $rows[0]['ALL_AlleleFreq'];
$rows[0]['ALL_AlleleFreq'] = ($rows[0]['ALL_AlleleFreq'] < 0) ? '.' : $rows[0]['ALL_AlleleFreq'];
$rows[0]['ALL_PopSize'] = ($rows[0]['ALL_PopSize'] == '') ? 'n/a' : $rows[0]['ALL_PopSize'];

//$freq .= "<span style='padding-left:2em;' title='Exac v3, all populations without psychiatric cases'>exac03np: ".$rows[0]['ALL_AlleleFreq']." (n:".$rows[0]['ALL_PopSize'].")</span>";

// snp142
$rows = runQuery("SELECT MAF, PopSize FROM `Variants_x_ANNOVAR_snp142` WHERE vid = '$vid'","Variants_x_ANNOVAR_snp142");
if (!is_array($rows[0])) $rows = OneToMulti($rows);
$rows[0]['MAF'] = ($rows[0]['MAF'] == '') ? 'n/a' : $rows[0]['MAF'];
$rows[0]['ALL_AlleleFreq'] = ($rows[0]['ALL_AlleleFreq'] < 0) ? '.' : $rows[0]['ALL_AlleleFreq'];
$rows[0]['PopSize'] = ($rows[0]['PopSize'] == '') ? 'n/a' : $rows[0]['PopSize'];
$freq .= "<span style='padding-left:2em;' title='dbSNPv142'>snp142: ".$rows[0]['MAF']." (n:".$rows[0]['PopSize'].")</span>";
*/
// own data : 
$freq = '';
$rows = runQuery("SELECT * FROM `Variants_x_Users_Summary` WHERE vid = '$vid' AND uid = '$uid'", "Variants_x_Users_Summary");
$urows = runQuery("SELECT * FROM `Users` WHERE id = '$uid'", "Users");
$freq .= "<span style='padding-left:2em;' title=''>all.hetAlt: " . $rows[0]['AllHet'] . " (n:" . $urows[0]['nrSamples'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>all.homAlt: " . $rows[0]['AllHomAlt'] . " (n:" . $urows[0]['nrSamples'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>all.hetAlt: " . $rows[0]['ConHet'] . " (n:" . $urows[0]['nrControls'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>all.homAlt: " . $rows[0]['ConHomAlt'] . " (n:" . $urows[0]['nrControls'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>Male.hetAlt: " . $rows[0]['MaleHet'] . " (n:" . $urows[0]['nrMales'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>Male.homAlt: " . $rows[0]['MaleHomAlt'] . " (n:" . $urows[0]['nrMales'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>Female.hetAlt: " . $rows[0]['FemaleHet'] . " (n:" . $urows[0]['nrFemales'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>Femle.homAlt: " . $rows[0]['FemaleHomAlt'] . " (n:" . $urows[0]['nrFemales'] . ")</span>";
$output .= "<p style='font-size:0.9em;'><span class=emph>In-house Frequencies:</span> <br/>$freq</p>";

// gnomad v2.1
$grows = runQuery("SELECT * FROM `Variants_x_ANNOVAR_gnomad_g_21` WHERE vid = '$vid'", "Variants_x_ANNOVAR_gnomad_g_21");
$erows = runQuery("SELECT * FROM `Variants_x_ANNOVAR_gnomad_e_21` WHERE vid = '$vid'", "Variants_x_ANNOVAR_gnomad_e_21");
$freq = '';
$freq .= "<span style='padding-left:2em;' title=''>AF.ALL: " . $grows[0]['AF_ALL'] . " (n:" . $grows[0]['AN_ALL'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>nHomAlt.ALL: " . $grows[0]['Hom_ALL'] . " (n:" . $grows[0]['AN_ALL'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>AF.Female: " . $grows[0]['AF_Female'] . " (n:" . $grows[0]['AN_Female'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>AF.Male: " . $grows[0]['AF_Male'] . " (n:" . $grows[0]['AN_Male'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>AF.NFE: " . $grows[0]['AF_nfe'] . " (n:" . $grows[0]['AN_nfe'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>nHomAlt.NFE: " . $grows[0]['Hom_nfe'] . " (n:" . $grows[0]['AN_nfe'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>AF.controls: " . $grows[0]['controls_AF'] . " (n:" . $grows[0]['controls_AN'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>nHomAlt.controls: " . $grows[0]['controls_nhomalt'] . " (n:" . $grows[0]['controls_AN'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>AF.female.controls: " . $grows[0]['controls_AF_female'] . " (n:" . $grows[0]['controls_AN_female'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>nHomAlt.female.controls: " . $grows[0]['controls_nhomalt_female'] . " (n:" . $grows[0]['controls_AN_female'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>AF.male.controls: " . $grows[0]['controls_AF_male'] . " (n:" . $grows[0]['controls_AN_male'] . ")</span>";
$freq .= "<span style='padding-left:2em;' title=''>nHomAlt.male.controls: " . $grows[0]['controls_nhomalt_male'] . " (n:" . $grows[0]['controls_AN_female'] . ")</span>";
$output .= "<p style='font-size:0.9em;'><span class=emph>gnomADv2.1 (genomes):</span> <br/>$freq</p>";

echo "$output";
