<?php
// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}
$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_SESSION['userID'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// get sampleID
$sid = $_GET['sid'];
$relcount = $_GET['RelCount'];
$type = (isset($_GET['type'])) ?  $_GET['type'] : '';


if ($type == 'projects') {
    // get samples != current sid.
    $projects = runQuery("SELECT p.Name, p.id FROM `Projects` p JOIN `Projects_x_Users` pu ON pu.pid = p.id WHERE pu.uid = '$userid'", "Projects_x_Users:Projects");
    echo "<select style='width:40%' id='RelationSelect${relcount}_pid' name='RelationSelect${relcount}_pid' onChange='LoadRelationSamples($relcount,$sid)'>";
    echo "<option value='' SELECTED disabled='disabled'></option>";
    foreach ($projects as $k => $row) {
        echo "<option value='" . $row['id'] . "'>" . stripslashes($row['Name']) . "</option>";
    }
    echo "</select> | <span id='ProjectSamples_$relcount'><select style='width:50%'><option value='' SELECTED disabled='disabled'></option></select></span>";
}
// samples for provided project.
elseif ($type == 'samples' && isset($_GET['pid'])) {
    $pid = $_GET['pid'];
    $rows = runQuery("SELECT s.id AS sid, s.Name AS sname FROM `Samples` s JOIN `Projects_x_Samples` ps ON s.id = ps.sid WHERE ps.pid = '$pid' ORDER BY s.Name ASC", "Samples:Projects_x_Samples");
    echo "<select style='width:50%' id='RelationSelect$relcount' name='RelationSelect$relcount'>";
    echo "<option value='' SELECTED disabled='disabled'></option>";
    foreach ($rows as $k => $row) {
        if ($row['sid'] == $sid) continue;
        echo "<option value='" . $row['sid'] . "'>" . stripslashes($row['sname']) . "</option>";
    }
    // close last project group
    echo "</select>";
} else {
    trigger_error("Invalid/No type an/or pid provided in AddRelationRow.php : TYPE:$type ; PID:" . $_GET['pid'], E_USER_ERROR);
    echo "Invalid arguments provided. Please Report.";
}
