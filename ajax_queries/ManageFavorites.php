<?php
// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

$item = $_GET['item'];
$id = $_GET['id'];
$toggle = $_GET['toggle'];

$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_SESSION['userID'];

if (!is_numeric($id) || !is_numeric($toggle)) {
    exit;
}

if ($item == 'OwnFilter') {
    doQuery("UPDATE `Users_x_FilterSettings` SET `Highlight` = '$toggle' WHERE `fid` = '$id' AND `uid` = '$userid'", "Users_x_FilterSettings");
} elseif ($item == 'SharedFilter') {
    doQuery("UPDATE `Users_x_Shared_Filters` SET `Highlight` = '$toggle' WHERE `fid` = '$id' AND `uid` = '$userid'", "Users_x_Shared_Filters");
} elseif ($item == 'OwnAnno') {
    doQuery("UPDATE `Users_x_Annotations` SET `Highlight` = '$toggle' WHERE `aid` = '$id' AND `uid` = '$userid'", "Users_x_Annotations");
} elseif ($item == 'SharedAnno') {
    doQuery("UPDATE `Users_x_Shared_Annotations` SET `Highlight` = '$toggle' WHERE `aid` = '$id' AND `uid` = '$userid'", "Users_x_Shared_Annotations");
}
exit;
