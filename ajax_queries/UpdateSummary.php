<?php

//#######################
//# CONNECT TO DATABASE #
//#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// parameters
$type = (isset($_GET['type']) ? $_GET['type'] : 'unset');
$uid = (isset($_GET['uid']) ? $_GET['uid'] : 'unset');

$result = array();
// validation
if (!is_numeric($uid)) {
    // return error.
    $result['status'] = 'Error';
    $resutl['msg'] = 'User ID should be a numeric value.';
    echo (json_encode($result));
    exit;
}
if ($type == 'p') {
    // open
    $filepath = "$scriptdir/Query_Results/.ProjectSummary.queue";
} elseif ($type == 'u') {
    $filepath = "$scriptdir/Query_Results/.UserSummary.queue";
} else {
    $result['status'] = 'Error';
    $result['msg'] = 'Summary type not recognized.';
    echo (json_encode($result));
    exit;
}

$result = LockWrite($filepath, $uid, $result);
echo (json_encode($result));
exit;


function LockWrite($filepath, $content, $return)
{
    // use lck path
    $lckfile = str_replace(".queue", ".lck", $filepath);
    $lck = fopen("$lckfile", "a");
    if (!$lck) {
        $return['status'] = 'Error';
        $return['msg'] = 'Cannot open file:  ' . $filepath;
        return ($return);
    }
    // lock
    if (flock($lck, LOCK_EX)) {
        if ($lckfile !== $filepath) {
            $file = fopen($filepath, "a");
            if (!$file) {
                $return['status'] = 'Error';
                $return['msg'] = 'Cannot open file:  ' . $filepath;
                return ($return);
            }
            fwrite($file, "$content\n");
            fclose($file);
        } else {
            fwrite($lck, "$content\n");
        }
        // release lock
        flock($lck, LOCK_UN);
    } else {

        $return['status'] = 'Error';
        $return['msg']   = "Error locking file : $filepath";
        return ($return);
    }
    // close
    fclose($lck);
    return (array('status' => 'ok'));
}
