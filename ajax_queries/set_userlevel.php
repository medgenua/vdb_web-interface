<?php
// connect to db
// load credentials
$sessionid = session_id();
if (empty($sessionid)) {
    include('../.LoadCredentials.php');
}
$db = "NGS-Variants" . $_SESSION['dbname'];
$userid = $_SESSION['userID'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// fetch user details
$userid = $_POST['userid'];
$row = runQuery("SELECT FirstName, LastName, level FROM `Users` WHERE id = '$userid'", "Users")[0];
$fname = $row['FirstName'];
$lname = $row['LastName'];
$level = $row['level'];

# check permission
if ($level < 3) {
    echo "<p>$fname $lname ($userid), you cheater ! You're not an admin, how did you get here?</p>";
} else {
    #permission ok	
    # get uids array
    $uids = $_POST['uids'];
    foreach ($uids as $key => $uid) {
        # get new permission level
        $perm = $_POST["perm_$uid"];
        doQuery("UPDATE `Users` SET level = '$perm' WHERE id = '$uid'", "Users");
    }
}
echo "<meta http-equiv='refresh' content='0;URL=index.php?page=admin_pages&amp;type=users'>\n";
