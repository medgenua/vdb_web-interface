<?php


//#######################
//# CONNECT TO DATABASE #
//#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

// parameters
$uid = (isset($_POST['uid']) ? $_POST['uid'] : 0);
$vid = (isset($_POST['vid']) ? $_POST['vid'] : 0);
$altcount = (isset($_POST['altcount']) ? $_POST['altcount'] : '.');
$gts = ['HomRef', 'Het', 'HomAlt'];

if (!is_numeric($uid) || !$uid) {
    // invalid userid
    echo "<tr><td colspan=9>ERROR : invalid userid provided</td></tr>";
    exit;
}
if (!is_numeric($vid) || !$vid) {
    // invalid userid
    echo "<tr><td colspan=9>ERROR : invalid userid provided</td></tr>";
    exit;
}

if (!in_array($altcount, $gts)) {
    // invalid userid
    echo "<tr><td colspan=9>ERROR : invalid GT provided : $altcount</td></tr>";
    exit;
}
$altcount = array_search($altcount, $gts);
// code mappings
$inh = array(0 => 'ND', 1 => 'P', 2 =>  'M', 3 => 'DN', 4 => 'Both', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => '-', 'Both' => 'Both Parents');
//%inhm = (0,'-',1,'Dominant',2,'Recessive','3','Unknown');
$classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');
$chr_hash = range(0, 22);
array_push($chr_hash, "X", "Y", "MT");



//////////////////////////
// DETAILS PER GENOTYPE //
//////////////////////////
// get samples
$sids = runQuery("SELECT p.name AS pname, p.id AS pid, s.id AS sid, s.gender , s.Name AS sname FROM `Samples` s JOIN  `Projects_x_Samples` ps JOIN `Projects_x_Users` pu JOIN `Projects` p ON pu.pid = ps.pid AND ps.sid = s.id AND p.id = ps.pid WHERE pu.uid = $uid ", "Samples:Projects_x_Samples:Projects_x_Users:Projects");
if (count($sids) == 0) {
    $output = "No samples available<br/>";
    echo "$output";
    echo "<input type=submit value='Close' class=button onClick=\"document.getElementById('overlay').style.display='none'\"/>";

    exit;
}

$samples = array();
foreach ($sids as $k => $row) {
    $samples[$row['sid']] = array(
        'sn' => $row['sname'],
        'pn' => $row['pname'],
        'pid' => $row['pid'],
        'gender' => $row['gender']
    );
}
$insid = implode(",", array_keys($samples));
// in batches of 100 samples.
$from = 0;
$size = 100;
$table = array(
    0 => array(),
    1 => array(),
    2 => array()
);


while ($from <= count(array_keys($samples))) {
    $insid = implode(",", array_keys(array_slice($samples, $from, $size, TRUE)));
    //error_log($insid);
    $from += $size;
    // get genotypes.,
    $gts = runQuery("SELECT sid, inheritance,InheritanceMode,class,Filter,AltCount,RefDepth,AltDepth,DeltaPL,StretchLengthA,StretchLengthB FROM `Variants_x_Samples` WHERE vid = '$vid' AND AltCount = '$altcount' AND sid IN ($insid)", "Variants_x_Samples");
    // process
    foreach ($gts as $k => $sr) {
        // sample
        $out = "<tr><td>" . $samples[$sr['sid']]['sn'] . "</td>";
        // project
        $out .= "<td>" . $samples[$sr['sid']]['pn'] . "</td>";
        // gender
        $out .= "<td>" . $samples[$sr['sid']]['gender'] . "</td>";
        // inheritance
        $out .= "<td>" . $inh[$inh[$sr['inheritance']]] . "</td>";
        // class
        $out .= "<td>" . $classes[$sr['class']] . "</td>";
        // depth
        $out .= "<td>" . $sr['RefDepth'] . "/" . $sr['AltDepth'] . "</td>";
        // filter
        $out .= "<td>" . $sr['Filter'] . "</td>";
        //DeltaPL
        $out .= "<td>" . $sr['DeltaPL'] . "</td>";
        //stretch
        if ($sr['StretchLengthA'] + $sr['StretchLengthB'] > 0) {
            $out .= "<td>" . $sr['StretchLengthA'] . "/" . $sr['StretchLengthB'] . "</td>";
        } else {
            $out .= "<td>-</td>";
        }
        $out .= "</tr>";
        array_push($table[$sr['AltCount']], $out);
    }
}

// map altcount to name
$names = ["HomRef", "Het", "HomAlt"];
// output
if (count($table[$altcount]) == 0) {
    $table[$altcount][] = "<tr><td colspan=9 align=left ><span class=italic>&nbsp; &nbsp;No samples found</span></td></tr>";
}
echo join("", $table[$altcount]);
