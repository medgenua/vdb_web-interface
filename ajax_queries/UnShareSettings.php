<?php
#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
include('../includes/inc_logging.inc');

## GET MAIN POSTED VARIABLES
$uid = $_GET['uid'];
$DeleteWhat = $_GET['UnShareWhat'];
$SharedWith = $_GET['SharedWith'];
$id = $_GET['id'];
$type = $_GET['type'];

if ($DeleteWhat == 'Filter') {
    // permission?
    $query = runQuery("SELECT uid FROM `Users_x_FilterSettings` WHERE uid = '$uid' AND fid = '$id'", "Users_x_FilterSettings");
    if (count($query) == 0) {
        echo "Not allowed.";
        exit;
    }
    if ($type == 'group') {
        doQuery("DELETE FROM `Usergroups_x_Shared_Filters` WHERE fid = '$id' AND gid = '$SharedWith'", "Usergroups_x_Shared_Filters");
    } elseif ($type == 'user') {
        doQuery("DELETE FROM `Users_x_Shared_Filters` WHERE fid = '$id' AND uid = '$SharedWith'", "Users_x_Shared_Filters");
    }
} elseif ($DeleteWhat == 'Annotation') {
    // permission?
    $query = runQuery("SELECT uid FROM `Users_x_Annotations` WHERE uid = '$uid' AND aid = '$id'", "Users_x_Annotations");
    if (count($query) == 0) {
        echo "Not allowed.";
        exit;
    }
    if ($type == 'group') {
        doQuery("DELETE FROM `Usergroups_x_Shared_Annotations` WHERE aid = '$id' AND gid = '$SharedWith'", "Usergroups_x_Shared_Annotations");
    } elseif ($type == 'user') {
        doQuery("DELETE FROM `Users_x_Shared_Annotations` WHERE aid = '$id' AND uid = '$SharedWith'", "Users_x_Shared_Annotations");
    }
}

echo "1";
