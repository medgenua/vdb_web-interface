<?php
require("includes/inc_query_functions.inc");
include('includes/inc_logging.inc');

//vars

$uid = $_GET['u'];
$type = (isset($_GET['type'])) ? $_GET['type'] : '';
$from = $_GET['from'];
//$custom = $_GET['cstm'];
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "NGS-Variants" . $_SESSION['dbname'];


if (isset($_GET['pid'])) {
    $pid = $_GET['pid'];

    if ($type == 'user') {
        doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$pid' AND uid = '$uid'", "Projects_Users");
        doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$uid'", "Users:Variants_x_Users_Summary:Summary");
    } elseif ($type == 'group') {
        doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$pid' AND gid = '$uid'", "Projects_x_Usergroups");
    }
    echo "<meta http-equiv='refresh' content='0;URL=index.php?page=$from&pid=$pid'>\n";
} elseif (isset($_GET['gpid'])) {
    $gpid = $_GET['gpid'];
    if ($type == 'user') {
        doQuery("DELETE FROM `GenePanels_x_Users` WHERE gpid = '$gpid' AND uid = '$uid'", "GenePanels_x_Users");
    } elseif ($type == 'group') {
        doQuery("DELETE FROM `GenePanels_x_Usergroups` WHERE gpid = '$gpid' AND gid = '$uid'", "GenePanels_x_Usergroups");
    }
    // HACK the from
    $from = "genepanels&t=manage&a=s";
    echo "<meta http-equiv='refresh' content='0;URL=index.php?page=$from&p=$gpid'>\n";
} elseif (isset($_GET['rsid'])) {
    $sel_id = $_GET['rsid'];
    $col = $table = $title = $prefix = '';
    if (substr($sel_id, 0, 2) == 'rs') {
        $col = 'rsid';
        $table = 'Report_Sections';
        $title = 'Section';
        $prefix = "rs_";
    } elseif (substr($sel_id, 0, 2) == 'rd') {
        $col = 'rid';
        $table = 'Report_Definitions';
        $title = 'Definition';
        $prefix = "rd_";
    }
    $id = substr($sel_id, 3);
    if ($type == 'user') {
        doQuery("DELETE FROM `Users_x_$table` WHERE $col = '$id' AND uid = '$uid'", "Users_x_$table");
    } else {
        doQuery("DELETE FROM `Usergroups_x_$table` WHERE $col = '$id' AND gid = '$uid'", "Usergroups_x_$table");
    }
    $from = "report&t=share&i=$prefix$id";
    echo "<meta http-equiv='refresh' content='0;URL=index.php?page=$from'>\n";
} elseif (isset($_GET['setid'])) {
    $setid = $_GET['setid'];
    if ($type == 'user') {
        doQuery("DELETE FROM `TranscriptSets_x_Users` WHERE set_id = '$setid' AND uid = '$uid'", "TranscriptSets_x_Users");
    } elseif ($type == 'group') {
        doQuery("DELETE FROM `TranscriptSets_x_UserGroups` WHERE set_id = '$setid' AND gid = '$uid'", "TranscriptSets_x_UserGroups");
    }
    $from = "mane&t=manage&a=s&setid=$setid";
    echo "<meta http-equiv='refresh' content='0;URL=index.php?page=$from'>\n";
}
