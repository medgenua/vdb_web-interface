<?php

if (!isset($loggedin) || $loggedin != 1) {
    include('page_login.php');
    exit;
}
// disable cgi time-outs.
set_time_limit(0);
?>
<!-- hidden field to track number of preloading items. -->
<input type=hidden id='timer_id' value=0 />
<!-- load the javascript -->
<script type='text/javascript' src='javascripts/ajax_variants.js?<?php echo $tip; ?>'></script>
<script type='text/javascript'>
    LoadVariantsPage()
    <?php
    // set of routines to inject the sample id into javascript run on page loading
    if (isset($_GET['s']) && is_numeric($_GET['s'])) {
        $sid = $_GET['s'];
        // access ?
        $rows = runQuery("SELECT ps.pid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu on pu.pid = ps.pid WHERE pu.uid = '$userid' AND ps.sid = '$sid'", "Projects_x_Samples:Projects_x_Users");
        if (count($rows) > 0) {
            $pid = $rows[0]['pid'];
            $snr = runQuery("SELECT name FROM `Samples` WHERE id = '$sid'", "Samples")[0];
            $pnr = runQuery("SELECT name FROM `Projects` WHERE id = '$pid'")[0];
            $full_name = $snr['name'] . " -- " . $pnr['name'];
            // set cookies
            echo "   setCookie('SID', '$sid', 1)\n";
            echo "   setCookie('SN', '$full_name', 1)\n";
        }
    }
    ?>
</script>
<div class='section'>

    <!-- Top panel: sample selector -->
    <div class='w100' style='margin-top:1.5em;'>
        <table cellspacing=0 style='width:95%'>
            <thead id='SourceSample'>
                <tr>
                    <td colspan=2 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Use This Sample/Region</td>
                    <td style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Data Visualisation in IGV</td>
                </tr>
                <tr>
                    <td style='width:15em;'><select id='selecttype' onChange="SetSampleSelect('<?php echo $userid; ?>')" name='selecttype'>
                            <option value='Type' selected>Type the sample name</option>
                            <option value='List'>Select Sample From List</option>
                            <option value='Region'>Provide a Genomic Region or Gene Symbol</option>
                            <!--<option value='Project'>Select a project</option>-->
                        </select></td>
                    <td id=sampleselect><input type=hidden name=sampleid id=sampleid value=''><input type=text style='width:98%;' value='' name=samplesource id=searchfield></td>
                    <td style='width:20em;' id=LoadDataLink>--</td>
                </tr>
            </thead>
        </table>
    </div>

    <!-- Tabs -->
    <div id="Variantheader">
        <ul>
            <li class="selected" id='FilterTabList'><a href="javascript:void(0)" onClick="ToggleTab('FilterTab')">Filter Settings</a></li>
            <li id='FilterComboTabList'><a href="javascript:void(0)" onClick="ToggleTab('FilterComboTab')">Filter Logic&nbsp;</a></li>
            <li id='AnnotationTabList'><a href="javascript:void(0)" onClick="ToggleTab('AnnotationTab')">Annotations</a></li>
            <li id='ExportTabList'><a href="javascript:void(0)" onClick="ToggleTab('ExportTab')">Export</a></li>
            <li id='ChartTabList'><a href="javascript:void(0)" onClick="ToggleTab('ChartTab')">Statistical Charts</a></li>
            <li id='LogTabList'><a href="javascript:void(0)" onClick="ToggleTab('LogTab')">Sample Log</a></li>
            <li id='SavedTabList'><a href="javascript:void(0)" onClick="ToggleTab('SavedTab')">Saved Results</a></li>

        </ul>
        <span class=toright title='Use this link to completely erase your current session. This should be used upon errors, or unexpected behaviour. You will be logged out and all set filters/annotations will be cleared.'><a href='index.php?page=reinitialise' style='font-style:italic;color:red;text-decoration:none'>Re-Initialize Session</a></span>
    </div>

    <!-- DIV HOLDING THE TAB CONTENTS -->
    <div id=FilterTabs class=h40em style='overflow:auto'>

        <!-- first tab : filter settings -->
        <div class='w100' id='FilterTab'>
            <div class='w75 toleft' id='filterbuilder'>
                <h3>Build your query</h3>
                <div class=toright style='color:blue;margin-right:5em;margin-top:-2em;font-size:0.9em;font-style:italic' onClick="ClearFilters()" onmouseover="this.style.cursor='pointer'">Clear Current Filters</div>
                <table id=querytable cellspacing=0 style='width:95%'>
                    <?php
                    // load the main categories in separate table sections
                    require_once('xmlLib2.php');
                    $configuration = my_xml2array("Filter/Filter_Options.xml");
                    $categories = get_value_by_path($configuration, "FilterConfiguration");
                    ?>
                    <thead>
                        <tr>
                            <!--<th class=top>Inter-Group Logic</th>-->
                            <th class=top style='padding-top:1em;'>Negate</th>
                            <th class=top style='padding-top:1em;'>Filter On</th>
                            <th class=top style='padding-top:1em;'>Argument</th>
                            <th class=top style='padding-top:1em;'>Values</th>
                            <!--   <th class=top style='padding-top:1em;'>Intra-Group Logic</th>-->
                        </tr>
                    </thead>
                    <?php
                    $output = '';
                    foreach ($categories as $subkey => $category) {
                        if (!is_numeric($subkey)) {
                            continue;
                        }
                        $name = $category['name'];
                        if ($name == 'null') {
                            continue;
                        }
                        $output .= "<tbody id='FilterBy$name'>\n";
                        $output .= "<tr class='spacer' ><td></td></tr>";
                        $output .= "  <tr >\n";
                        $output .= "    <td colspan=4 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic;'>Filter On $name Information &nbsp;\n";
                        $output .= "    <span title='Add Filter Option' onClick=\"AddCategory('$name')\"><img src='Images/layout/plus-icon.png' style='height:1em;'></span></td>\n";
                        $output .= "  </tr>\n";
                        $output .= "</tbody>\n";
                    }
                    //$output = str_replace('rowspan=1',"rowspan=$catcount",$output);
                    echo $output;
                    ?>
                    <tr>
                        <td colspan=5 class=last>&nbsp;</td>
                    </tr>
                </table>
            </div>
            <!-- Saved Filter Settings -->
            <div class='toright w25'>
                <div id='StoredFilters'>
                    <!-- holds ajax results -->
                </div>
                <h3>Save Current Filter Settings</h3>
                <p><input type=submit onClick="SaveFilter('<?php echo $userid; ?>')" value='Save Current Filter'></p>
            </div>
        </div>
        <!-- first (bis) tab: Define the filter combination logic -->
        <div class='w100' id='FilterComboTab' style='display:none;'>
            <div class='w100 toleft'>
                <form name='FilterComboForm'>
                    <h3>Defined Filtering Logic</h3>
                    <p style='font-size:0.95em;'>Empty rules will be discarded. Drag-and-drop rules to refine filter settings.<br /><button type='button' onClick='switchType("AND")'>set rule type as AND</button> &nbsp; <button type='button' onClick='switchType("OR")'>set rule type as OR</button> &nbsp; | &nbsp; <button type='button' onClick="AddRule('AND')">Add 'AND' rule</button> &nbsp; <button type='button' onClick="AddRule('OR')">Add 'OR' rule</button> &nbsp; | &nbsp; <button type='button' onClick="RemoveNode()">Remove selected branch</button></p>
                    <!-- todo : add the jstree module here -->
                    <div style='border:1px black solid;height:27em;width:90%;background:white;margin:2em;overflow:auto;padding:0.5em;'>
                        <span class=emph>RESULT</span>&nbsp;<span id='LogicComments' style='margin-left:3em;font-weight:bold;color:red;'>&nbsp;</span>
                        <div id='filter_tree'>
                            <ul>
                                <li data-jstree='{ "opened" : true, "type" : "logic_AND" }'>AND</li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- second tab: select annotations.-->
        <div class='w100' id=AnnotationTab style='display:none;'>
            <div class='w75 toleft'>
                <form name='annotationform'>
                    <h3>Select Annotations To show</h3>
                    <p>Hover the mouse over available annotations to get more information.</p>
                    <p>
                    <table id=annotable cellspacing=0 style='width:99%'>
                        <?php
                        // load the main categories in separate table sections
                        $configuration = my_xml2array("Annotations/Config.xml");
                        $anno_conf = get_value_by_path($configuration, "Annotation_Configuration");
                        $output = "\n";
                        $categories = array();
                        foreach ($anno_conf as $subkey => $source) {
                            if (!is_numeric($subkey)) {
                                continue;
                            }
                            if ($source['name'] == 'null') {
                                continue;
                            }
                            if ($source['name'] == 'Custom_VCF_Fields') {
                                $categories[$source['name']] = array();
                                continue;
                            }
                            $name = $source['name'];
                            $items = get_value_by_path($configuration, "Annotation_Configuration/" . $source['name']);
                            foreach ($items as $ssubkey => $item) {
                                if (!is_numeric($ssubkey)) {
                                    continue;
                                }
                                $iname = $item['name'];
                                if ($iname == 'null') {
                                    continue;
                                }
                                $license = get_value_by_path($configuration, "Annotation_Configuration/$name/$iname/license");
                                if ($license && $license['value'] != '') {
                                    // check if allowed
                                    $l_row = runQuery("SELECT l.lid FROM `License` l JOIN `Users_x_License` ul ON ul.lid = l.lid WHERE ul.uid = '$userid' AND  l.LicenseName = '" . $license['value'] . "'", "License:Users_x_License");
                                    if (empty($l_row)) {
                                        //license failed. skip filter.
                                        continue;
                                    }
                                }
                                $help = get_value_by_path($configuration, "Annotation_Configuration/$name/$iname/help");
                                $category = get_value_by_path($configuration, "Annotation_Configuration/$name/$iname/category");

                                if (get_value_by_path($configuration, "Annotation_Configuration/$name/$iname/deprecated")) {
                                    if (substr($iname, 0, 1) == '_') {
                                        $iname = substr($iname, 1);
                                    }
                                    $output .= "<input type=checkbox name='annotations' style='display:none' value='$name;$iname' >";
                                    continue;
                                }

                                if (substr($iname, 0, 1) == '_') {
                                    $iname = substr($iname, 1);
                                }
                                $category = $category['value'];
                                if (!array_key_exists($category, $categories)) {
                                    $categories[$category] = array();
                                }
                                $categories[$category][$iname] = array('source' => $name);
                                if ($help && $help['value'] != '') {
                                    $categories[$category][$iname]['help'] = $help['value'];
                                } else {
                                    $categories[$category][$iname]['help'] = '';
                                }
                            }
                        }
                        foreach ($categories as $category => $items) {
                            $output .= "<tbody  id='AnnotateBy$category'>\n";
                            $output .= "  <tr >\n";
                            $output .= "    <th colspan=4 class=top style='padding-left:0em;text-align:left;background:#cecece;font-style:italic;'>$category Information &nbsp;";
                            $output .= "<input type=checkbox title='check all' id='CheckAll_$category' onChange=\"CheckAll('$category');\">";
                            $output .= "</td></tr>\n";
                            // custom fields annotations are taken from DB, sample specific.
                            if ($category == 'Custom_VCF_Fields') {
                                $output .= "<tr id='initial_custom_row'><td colspan=4>Select a sample/project to show available custom annotations</td></tr>";
                            }
                            // normal annotations.
                            else {

                                $colcount = 0;
                                ksort($items);
                                foreach ($items as $iname => $info) {
                                    $colcount++;
                                    if ($colcount > 4) {
                                        $output .= "</tr>";
                                        $colcount = 1;
                                    }
                                    if ($colcount == 1) {
                                        $output .= "<tr>";
                                    }
                                    $title = $info['help'];
                                    $source = $info['source'];
                                    $title = str_replace("'", "\'", $title);
                                    $output .= "<td title='$title'><input type=checkbox name='annotations' value='$source;$iname' onclick='AnnotationsToCookies()'> $iname</td>";
                                }
                                while ($colcount < 4) {
                                    $colcount++;
                                    $output .= "<td>&nbsp;</td>";
                                }
                                $output .=  "</tr>";
                            }
                            $output .= "</tbody>\n";
                        }
                        $output .= "</table>";
                        $output .= "</form>";
                        echo $output;
                        $output = '';
                        ?>
            </div>
            <!-- Saved Annotations Settings -->
            <div class='toright w25'>
                <div id='StoredAnnotations'>
                    <!-- holds ajax results -->
                </div>
                <h3>Save Current Annotation Settings</h3>
                <p><input type=submit onClick="SaveAnnotations('<?php echo $userid; ?>')" value='Save Current Annotations'></p>
            </div>
        </div>

        <!-- third tab: export tab.-->
        <div class='w100' id=ExportTab style='display:none;'>
            <div class='toright w100'>
                <h3>Select Export Options</h3>
                <p>
                <table cellspacing=0>

                    <tr>
                        <td>Field Separator:</td>
                        <td><select name=seperator id=seperator>
                                <option value='comma' selected>Comma</option>
                                <option value='tab' selected>Tab</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Add Quotes Around Fields:</td>
                        <td><select name=quote id=quote>
                                <option value='y'>Yes</option>
                                <option value='n' selected>No</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Collapse gene symbols for multiple variants:</td>
                        <td><select name=collapse id=collapse>
                                <option value='y'>Yes</option>
                                <option value='n' selected>No</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td>How To Return Results: </td>
                        <td><select name='returnAs' id='returnAs'>
                                <option value='file' selected>As a file</option>
                                <option value=inline>Copy-Paste</option>
                            </select></td>
                    </tr>
                </table>
                </p>
            </div>
        </div>
        <!-- Fourth tab: Graphs and tables with variant stats -->
        <div class='w100' id=ChartTab style='display:none;'>
            <div class='toright w100'>
                <h3>Variant Statistics</h3>
                <p>Select the plots and tables to show. Plots will be generated based on the selected filter settings.</p>
                <p>
                <table cellspacing=0 width='90%'>
                    <!-- three per row. -->
                    <?php
                    $col = 1;
                    echo "<tr>";
                    $configuration = my_xml2array("charts/charts.xml");
                    $charts = get_value_by_path($configuration, "charts");
                    $output = "\n";
                    $scripts = '';
                    foreach ($charts as $subkey => $category) {
                        if (!is_numeric($subkey)) {
                            continue;
                        }
                        $name = $category['name'];
                        if ($name == 'null') {
                            continue;
                        }
                        if ($col > 4) {
                            echo "</tr>\n<tr>";
                            $col = 1;
                        }
                        $fullname = get_value_by_path($configuration, "charts/$name/name");
                        $fullname = $fullname['value'];
                        $help = get_value_by_path($configuration, "charts/$name/help");
                        $help = $help['value'];
                        if (file_exists("charts/$name.js")) {
                            $scripts .= "<script type=text/javascript src='charts/$name.js?$tip'></script>\n";
                        }
                        echo "<td title='$help'><input type=checkbox name='chart[]' id='chart[]' value='$name' CHECKED /> $fullname</td>";
                        $col++;
                    }
                    while ($col <= 4) {
                        echo "<td>&nbsp;</td>";
                        $col++;
                    }
                    echo "</tr>";
                    ?>
                </table>
                </p>
            </div>
        </div>

        <!-- fifth tab : LOG -->
        <div class='w100' id='LogTab' style='display:none;'>
            <h3>Sample Log.</h3>
            <p>Log is loaded upon sample selection</p>
        </div>
        <!-- sixth tab : stored results -->
        <div class='w100' id='SavedTab' style='display:none;'>
            <h3>Saved Filtering Results.</h3>
            <p>Available sets are loaded upon sample selection</p>
        </div>

    </div> <!-- End of filtertabs -->
    <p id='Loaded_Filter' style='color:green;margin-bottom:-1em;'></p>


</div> <!-- end of filterSettings -->

</div> <!-- end of section -->
<!-- DIV for holding query results -->
<div class=section class='w100' id=QueryResultsHolder>

    <div id=QueryResults>
        <h3>Query Results</h3>
        <p style='padding-left:1em'><input type=checkbox id='widetable' name='widetable'> Use wide output table format, putting all annotations in a single line per variant</p>

        <p style='padding-left:1em'><input type=button onClick="RunQuery('0');" value="Execute"> &nbsp; &nbsp; <input type=button onClick="SaveResults('<?php echo $userid; ?>');" value="Save Results"></p>
        <p>Query was not yet executed</p>
    </div>
</div>

<?php
// print chart loading scripts
echo $scripts;
?>

<!-- OFFLOAD LOADING OF SAVED FILTERS & ANNO -->
<script type='text/javascript'>
    // load the stored filters list
    getFilters('<?php echo $userid; ?>');
    getAnnotations('<?php echo $userid; ?>');
</script>