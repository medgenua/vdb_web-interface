<?php
// check login
if ($loggedin != 1) {
    echo "<div class=section><h3>Manage your projects</h3>\n";
    include('page_login.php');
    echo "</div>\n";
    exit();
}

// get pid
if (isset($_POST['pid'])) {
    $pid = $_POST['pid'];
} elseif (isset($_GET['pid'])) {
    $pid = $_GET['pid'];
} else {
    $pid = '';
}

// no project selected, show overview.
if ($pid == '') {

    echo "<div class=section>";
    echo "<h3>Select a Project</h3>";
    echo "<p><table cellspacing=0 style='margin-left:1em;min-width:50%'>";
    echo "<tr><th class=top >Project Name</th><th class=top >Project Owner</th><th class=top>Created</th><th class=top>#Samples</th><th class=top title='use project in frequency summaries'>Summarize</th><th class=top>Actions</th></tr>";
    $rows = runQuery("SELECT p.id, p.Name, p.Created, u.LastName, u.FirstName,pu.editsample, p.summarize FROM `Projects` p JOIN `Projects_x_Users` pu JOIN `Users` u ON p.id = pu.pid AND p.userID = u.id WHERE pu.uid = '$userid' ORDER BY p.Name ASC", "Projects:Projects_x_Users:Users");
    $summarize_icons = array('Images/layout/no.png', 'Images/layout/yes.png');
    foreach ($rows as $k => $row) {
        $pid = $row['id'];
        $pname = $row['Name'];
        $owner = $row['LastName'] . " " . $row['FirstName'];
        $created = $row['Created'];
        $out = "<tr class=vrow>";
        $out .= "<td>$pname</td>";
        $out .= "<td>$owner</td>";
        $out .= "<td>$created</td>";
        $sr = runQuery("SELECT COUNT(sid) AS nrsid FROM `Projects_x_Samples` WHERE pid = '$pid'", "Projects_x_Samples")[0];
        $out .= "<td>" . $sr['nrsid'] . "</td>";
        if ($row['editsample'] == 0) {
            $filter = "style='filter:grayscale(100%);' title='Not allowed to change'";
        } else {
            $filter = "title='Click to inlcude/exclude projet from frequency statistics' onClick='ToggleProjectFrequencies($pid)'";
        }
        $out .= "<td><img height='15' src='" . $summarize_icons[$row['summarize']] . "' $filter /></td>";
        if (array_key_exists('Archived', $_SESSION) && $_SESSION['Archived'] == 1) {
            $out .= "<td>Archived</td>";
        } elseif ($row['editsample'] == 1) {
            //share
            $out .= "<td><img onclick=\"javascript:location.href='index.php?page=projects&pid=$pid'\" title='Share Project' src='Images/layout/share_icon.png' style='height:1em'>";
            // delete
            $out .= " <img src='Images/layout/icon_trash.gif' style='height:1.1em;' title='Delete Project' onclick=\"DeleteProject('$userid','$pid')\">";
            // export tar
            $out .= " <img src='Images/layout/export.png' style='height:1.1em;' title='Export Project Tarball' onclick=\"ExportProject('$userid','$pid')\">";
            // export csv
            $out .= " <img src='Images/layout/csv.png' style='height:1.2em;' title='Export to CSV' onclick=\"ExportCSV('$userid','$pid')\">";
            $out .= "</td>";
        } else {
            $out .= "<td>Read-Only</td>";
        }
        $out .= "</tr>";

        echo $out;
    }
    echo "<tr><td colspan=6 class=last>&nbsp;</td></tr></table>";
}
// process items
elseif ($pid != '' && !isset($_POST['share']) && !isset($_POST['finalise'])) {
    // get project details
    $row = runQuery("SELECT Name, userID FROM `Projects` WHERE id = '$pid'", "Projects")[0];
    $pname = $row['Name'];
    $pown = $row['userID'];
    // get my own affiliation
    $row = runQuery("SELECT Affiliation FROM `Users` WHERE id = '$userid'", "Users")[0];
    $myaffi = $row['Affiliation'];
    ## check editsample 
    $row = runQuery("SELECT editvariant, editclinic, editsample FROM `Projects_x_Users` WHERE pid = $pid AND uid = $userid", "Projects_x_Users")[0];
    $editsample = $row['editsample'];
    if (array_key_exists('Archived', $_SESSION) && $_SESSION['Archived'] == 1) {
        echo "<div class=section>\n";
        echo "<h3>Archived Project</h3>\n";
        echo "<p>Archived projects can not be shared. Please switch to the current genome build to share this project</p>\n";
    } elseif ($pown == $userid || $editsample == 1) {

        echo "<div class=section>\n";
        echo "<h3>Share Project: '$pname'</h3>\n";
        echo "<p>Please select the usergroups and/or users you want to share this project with from the lists below and press 'Share'.</p>\n";
        echo "<p>\n";
        echo "<form action='index.php?page=projects' method=POST>\n";
        echo " <input type=hidden name=pid value='$pid'></p>\n";

        // get groups with access
        $groups = runQuery("SELECT ug.id, ug.name, ug.editvariant, ug.editclinic,ug.editsample FROM `Usergroups` ug JOIN `Projects_x_Usergroups` ppg ON ug.id = ppg.gid WHERE ppg.pid = '$pid'", "Usergroups:Projects_x_Usergroups");
        echo "<ol>\n";
        $gwa = array(); // groups with access
        foreach ($groups as $k => $row) {
            $ugname = $row['name'];
            $ugid = $row['id'];
            $ugcnv = $row['editvariant'];
            $ugclin = $row['editclinic'];
            $ugsample = $row['editsample'];
            $gwa[$ugid] = "$ugname@@@$ugcnv@@@$ugclin@@@$ugsample";
        }
        // list groups without access 
        echo "<p><table cellspacing=20>\n";
        echo "<tr>\n";
        echo "<td class=clear valign=top>\n";
        echo "<span class=emph>Share With Usergroups</span><br>\n";
        echo "<select name='groups[]' size=10 MULTIPLE>\n";
        // private
        echo "<OPTGROUP label='Private'>";
        $rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug WHERE $myaffi IN (ug.affiliation) AND ug.opengroup = 0 GROUP BY ug.id", "Usergroups");
        $printed = 0;
        foreach ($rows as $k => $row) {
            $ugname = $row['name'];
            $ugid = $row['id'];
            if (!array_key_exists($ugid, $gwa)) {
                $printed++;
                echo "<option value='$ugid'>$ugname</option>\n";
            }
        }
        if ($printed == 0) {
            echo "<option value='' DISABLED>No groups available</option>";
        }

        echo " </OPTGROUP>";
        // open
        echo "<OPTGROUP label='Public'>";
        $rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug WHERE ug.opengroup = 1 GROUP BY ug.id", "Usergroups");
        $printed = 0;
        foreach ($rows as $k => $row) {
            $ugname = $row['name'];
            $ugid = $row['id'];
            if (!array_key_exists($ugid, $gwa)) {
                $printed++;
                echo "<option value='$ugid'>$ugname</option>\n";
            }
        }
        if ($printed == 0) {
            echo "<option value='' DISABLED>No groups available</option>";
        }

        echo " </OPTGROUP>";
        echo "</select>\n";
        echo "</td>\n";
        // get seperate users with  access
        $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name, pp.editvariant, pp.editclinic, pp.editsample FROM `Users`u JOIN `Projects_x_Users` pp JOIN `Affiliation` a ON u.Affiliation = a.id AND u.id = pp.uid WHERE pp.pid = '$pid' ORDER BY a.id, u.LastName", "Users:Projects_x_Users:Affiliation");
        $uwa = array();
        foreach ($rows as $k => $row) {
            $lname = $row['LastName'];
            $currid = $row['id'];
            $fname = $row['FirstName'];
            $affi = $row['name'];
            $editcnv = $row['editvariant'];
            $editclinic = $row['editclinic'];
            $editsample = $row['editsample'];
            $uwa[$currid] = "$lname $fname@@@$affi@@@$editcnv@@@$editclinic@@@$editsample";
        }
        // list users with no or restricted access
        echo "<td class=clear valign=top>\n";
        echo "<span class=emph>Share With Single Users</span><br>\n";
        echo "<select name='users[]' size=10 MULTIPLE>\n";
        $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id ORDER BY a.name, u.LastName", "Users:Affiliation");
        $currinst = '';
        $printed = 0;
        foreach ($rows as $k => $row) {
            $uid = $row['id'];
            // skip users with full access
            if (array_key_exists($uid, $uwa)) {
                $pieces = explode('@@@', $uwa[$uid]);
                if ($pieces[2] == 1 && $pieces[3] == 1 && $pieces[4] == 1) {
                    continue;
                }
            }
            $lastname = $row['LastName'];
            $firstname = $row['FirstName'];
            $institute = $row['name'];
            if ($currinst != $institute) {
                if ($currinst != '') {
                    echo "</optgroup>";
                }
                echo "<optgroup label='$institute'>\n";
                $currinst = $institute;
            }
            echo "<option value='$uid'>$lastname $firstname</option>\n";
            $printed++;
        }
        if ($printed == 0) {
            echo "<option value='' DISABLED>No users available</option>";
        }
        echo "</select>\n";
        echo "</td>\n";
        echo "</tr>\n";
        echo "</table>\n";
        echo "<input type=submit class=button name=share value='Share'></p>\n";
        echo "</form>\n";
        echo "</div>\n";
        // show who has access now.
        echo "<div class=section>\n";
        echo "<h3>Users and usergroups with access</h3>\n";
        echo "<p>The project is currently accessible by the following users and usergroups. 'Full' means read-write access to variant and clinical information. 'Read-only' means no information can be changed. 'Variant' and 'Clinic' mean that only variant an clinical information can be changed respectively. 'Project Control' means this user can share, join or delete the project or samples within it.</p>\n";
        echo "You can remove their access by clicking on the garbage bin. Removing access from usergroup only prevents new users from gaining access to projects when joining the usergroup. Actual access to projects has to be removed on a per-user basis.</p>\n";
        echo "<p><table cellspacing=20 >\n";
        echo "<tr>\n";
        echo "<td valign=top>\n";
        echo "<span class=emph>Usergroups with access:</span><br>\n";
        echo "<ol>\n";
        foreach ($gwa as $gid => $value) {
            $values = explode('@@@', $value);
            $gname = $values[0];
            $editcnv = $values[1];
            $editclinic = $values[2];
            $editsample = $values[3];
            if ($editcnv == 1 && $editclinic == 1) {
                $perm = 'Full';
            } elseif ($editcnv == 1) {
                $perm = 'Variant';
            } elseif ($editclinic == 1) {
                $perm = 'Clinic';
            } else {
                $perm = 'Read-only';
            }
            if ($editsample == 1) {
                $perm .= " / Project Control";
            }
            echo "<li>$gname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?pid=$pid&u=$gid&type=group&from=projects\"><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></a></li>\n";
        }
        echo "</ol>\n";
        echo "</td>\n";
        // seperate users
        echo "<td valign=top >\n";
        echo "<span class=emph>Users with access:</span><br>\n";
        $currinst = '';
        echo "<ul id=nodisc>\n";
        foreach ($uwa as $uid => $value) {
            $pieces = explode('@@@', $value);
            $uname = $pieces[0];
            $institute = $pieces[1];
            $editcnv = $pieces[2];
            $editclinic = $pieces[3];
            $editsample = $pieces[4];
            // check institution
            if ($currinst != $institute) {
                if ($currinst != '') {
                    echo "</ol>";
                }
                echo "<li><span class=italic>&nbsp;&nbsp;$institute</span><ol>\n";
                $currinst = $institute;
            }
            // check permissions
            if ($editcnv == 1 && $editclinic == 1) {
                $perm = 'Full';
            } elseif ($editcnv == 1) {
                $perm = 'Variant';
            } elseif ($editclinic == 1) {
                $perm = 'Clinic';
            } else {
                $perm = 'Read-only';
            }
            if ($editsample == 1 || $pown == $uid) {
                $perm .= " / Project Control";
            }

            echo "<li>$uname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?pid=$pid&u=$uid&type=user&from=projects\"><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></a></li>\n";
        }
        echo "</ol></ul>\n";
        //echo "</select>\n";
        echo "</td>\n";
        echo "</tr>\n";
        echo "</table>\n";
    } else {
        echo "<div class=section>\n";
        echo "<h3>Share Project Error</h3>\n";
        echo "<p>You are not allowed to share this project.</p>\n";
    }
} elseif ($pid != '' && isset($_POST['share'])) {
    // get project details
    $row = runQuery("SELECT name FROM `Projects` WHERE id = '$pid'", "Projects")[0];
    $pname = $row['name'];

    // get posted vars
    $addgroups = $_POST['groups'];
    $addusers = $_POST['users'];
    echo "<div class=section>\n";
    echo " <h3>Project '$pname' Shared</h3>\n";
    // process groups
    if (count($addgroups) > 0) {

        echo "<p>Project shared with following usergroups:</p>\n";
        echo "<table cellspacing=0>\n";
        echo "<th class=top>Group</th>\n";
        echo "<th class=top>Edit Variants</th>\n";
        echo "<th class=top>Edit Clinic</th>\n";
        echo "<th class=top>Project Control</th>\n";
        echo "</tr>\n";
        $yesno = array('Not Allowed', 'Allowed');
        foreach ($addgroups as $gid) {
            // get permission and other info on usergroup
            $row = runQuery("SELECT name, editvariant, editclinic, editsample FROM `Usergroups` WHERE id = '$gid'", "Usergroups")[0];
            $gname = $row['name'];
            $editcnv = $row['editvariant'];
            $editclinic = $row['editclinic'];
            $editsample = $row['editsample'];
            // insert into permissions for group
            insertQuery("INSERT INTO `Projects_x_Usergroups` (pid, gid, sharedby) VALUES ('$pid', '$gid','$userid') ON DUPLICATE KEY UPDATE gid = '$gid'", "Projects_x_Usergroups");
            // update permissions for users in group
            $rows = runQuery("SELECT uid FROM `Usergroups_x_User` WHERE gid = '$gid'", "Usergroups_x_User");
            foreach ($rows as $k => $row) {
                $uid = $row['uid'];
                // has permission?
                $arow = array_shift(...[runQuery("SELECT editvariant,editclinic,editsample FROM `Projects_x_Users` WHERE `pid` = '$pid' AND `uid` = '$uid'", "Projects_x_Users")]);
                if (is_array($arow) && count($arow) > 0) {

                    $edit = max($arow['editvariant'], $editcnv);
                    $clin = max($arow['editclinic'], $editclinic);
                    $shar = max($arow['editsample'], $editsample);
                    doQuery("UPDATE `Projects_x_Users` SET `editvariant` = '$edit', `editclinic` = '$clin', `editsample` = '$shar' WHERE `uid` = '$uid' AND `pid` = '$pid'", "Projects_x_Users"); // no impact on summary tables
                } else {
                    // project for user ; reset SummaryStatus & insert permissions.
                    doQuery("INSERT INTO `Projects_x_Users` (pid,uid,editvariant,editclinic,editsample) VALUES ('$pid','$uid', '$editcnv', '$editclinic','$editsample')", "Projects_x_Users");
                    doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$uid'", "Variants_x_Users_Summary:Summary");
                    // notify the user (to Inbox)
                    $subject = "New project made available ($pname)";
                    insertQuery("INSERT INTO `Inbox` (Inbox.from, Inbox.to, Inbox.subject, Inbox.body, Inbox.type, Inbox.values, Inbox.date) VALUES ('$userid','$uid','$subject','','shared_project','$pid',NOW())", "Inbox");
                }
            }
            echo "<tr>\n";
            echo "<td >$gname</td>\n";
            echo "<td>" . $yesno[$editcnv] . "</td>\n";
            echo "<td>" . $yesno[$editclinic] . "</td>\n";
            echo "<td>" . $yesno[$editsample] . "</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n";
        echo "</p>\n";
    }
    // process single users
    if (is_array($addusers) && count($addusers) > 0) {

        echo "<p>Please specify the permissions for the users you want to share this project with, and press 'Finish'.</p>\n";
        echo "<form action='index.php?page=projects' method=POST>\n";
        echo "<input type=hidden name=pid value='$pid'>\n";
        echo "<input type=hidden name=finalise value=1>\n";
        echo "<table cellspacing=0>\n";
        echo "<th class=top>User</th>\n";
        echo "<th class=top>Edit Variants</th>\n";
        echo "<th class=top>Edit Clinic</th>\n";
        echo "<th class=top>Project Control</th>\n";
        echo "</tr>\n";
        foreach ($addusers as $uid) {
            // get userinfo 
            $row = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '$uid'", "Users")[0];
            $fname = $row['FirstName'];
            $lname = $row['LastName'];
            // print table row
            echo "<tr>\n";
            echo "<td ><input type=hidden name='users[]' value='$uid'>$lname $fname</td>\n";
            echo "<td><input type=radio name='cnv_$uid' value=1 checked> Yes <input type=radio name='cnv_$uid' value=0> No</td>\n";
            echo "<td><input type=radio name='clinic_$uid' value=1 checked> Yes <input type=radio name='clinic_$uid' value=0> No</td>\n";
            echo "<td><input type=radio name='sample_$uid' value=1 > Yes <input type=radio name='sample_$uid' value=0 checked> No</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n";
        echo "</p>\n";
        echo "<input type=submit class=button name=next value='Finish'>\n";
        echo "</form>\n";
    }
    echo "<p><a href='index.php?page=projects&pid=$pid'>Go Back</a></p>\n";
    echo "</div>\n";
} elseif ($pid != '' && isset($_POST['finalise'])) {
    // get project details
    $row = runQuery("SELECT name FROM `Projects` WHERE id = '$pid'", "Projects")[0];
    $pname = $row['name'];
    echo "<div class=section>\n";
    echo " <h3>Project '$pname' Shared</h3>\n";

    // get vars: 
    $addusers = $_POST['users'];
    $yesno = array('Not Allowed', 'Allowed');
    // print table
    echo "<p>The following users gained access to the project.</p>\n";
    echo "<p><table cellspacing=0>\n";
    echo "<th class=top>User</th>\n";
    echo "<th class=top>Edit Variants</th>\n";
    echo "<th class=top>Edit Clinic</th>\n";
    echo "<th class=top>Project Control</th>\n";
    echo "</tr>\n";

    foreach ($addusers as $uid) {
        $editcnv = $_POST["cnv_$uid"];
        $editclinic = $_POST["clinic_$uid"];
        $editsample = $_POST["sample_$uid"];
        // user info
        $row = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '$uid'", "Users")[0];
        $fname = $row['FirstName'];
        $lname = $row['LastName'];
        // print table row
        echo "<tr>\n";
        echo "<td>$lname $fname</td>\n";
        echo "<td>" . $yesno[$editcnv] . "</td>\n";
        echo "<td>" . $yesno[$editclinic] . "</td>\n";
        echo "<td>" . $yesno[$editsample] . "</td>\n";
        echo "</tr>\n";
        // INSERT or UPDATE permissions
        $arow = array_shift(...[runQuery("SELECT editvariant,editclinic,editsample FROM `Projects_x_Users` WHERE `pid` = '$pid' AND `uid` = '$uid'", "Projects_x_Users")]);
        if (is_array($arow) && count($arow) > 0) {
            $edit = max($arow['editvariant'], $editcnv);
            $clin = max($arow['editclinic'], $editclinic);
            $shar = max($arow['editsample'], $editsample);
            doQuery("UPDATE `Projects_x_Users` SET `editvariant` = '$edit', `editclinic` = '$clin', `editsample` = '$shar' WHERE `uid` = '$uid' AND `pid` = '$pid'"); // no impact on summary tables.
        } else {
            // project for user ; reset SummaryStatus & insert permissions.
            doQuery("INSERT INTO `Projects_x_Users` (pid,uid,editvariant,editclinic,editsample) VALUES ('$pid','$uid', '$editcnv', '$editclinic','$editsample')", "Projects_x_Users");
            doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$uid'", "Variants_x_Users_Summary:Summary");
            // notify the user (to Inbox)
            $subject = "New project made available ($pname)";
            insertQuery("INSERT INTO `Inbox` (Inbox.from, Inbox.to, Inbox.subject, Inbox.body, Inbox.type, Inbox.values, Inbox.date) VALUES ('$userid','$uid','$subject','','shared_project','$pid',NOW())", "Inbox");
        }
    }
    echo "<p><a href='index.php?page=projects&pid=$pid'>Go Back</a></p>\n";
    echo "</div>\n";
}
