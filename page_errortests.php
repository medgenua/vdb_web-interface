<?php
// the tests.
if (isset($_GET['inc_fun_1'])) {
    trigger_error("Testing incorrect function call, missing arguments");
    $list = explode("My Dummy String");
}
if (isset($_GET['inc_fun_2'])) {
    trigger_error("Testing incorrect function call, non-existing function");
    $list = expode(" ", "My Dummy String");
}
if (isset($_GET['inc_val'])) {
    trigger_error("Testing incorrect value trigger");
    $n = number_format("hello world");
}
if (isset($_GET['manual_trigger'])) {
    trigger_error("Manually triggered Fatal Error", E_USER_ERROR);
}

?>


<div class=section>
    <h3>Introduction</h3>
    <p>This page contains a few checks for error handling. No data is altered in the system, and all tests should be safe to use.</p>
    <p>Click the 'TEST' buttons to perform each test individually.</p>

    <h4>Non-Existing function</h4>
    <p><input type=submit class=buttion value='TEST' onClick="window.location.href='index.php?page=errortests&inc_fun_2'"> : Run a Non-Existing function : In this case : the expode() function (instead of explode()</p>

    <h4>Incorrect function</h4>
    <p><input onClick="window.location.href='index.php?page=errortests&inc_fun_1'" type=submit class=button value='TEST'> : Run a misconfigured function : in this case, the explode() function, run with a single argument</p>

    <h4>Value Error</h4>
    <p><input type=submit class=buttion value='TEST' onClick="window.location.href='index.php?page=errortests&inc_val'"> : Invalid input value : In this case : number_format('MyString')</p>

    <h4>Manually thrown Error</h4>
    <p><input type=submit class=buttion value='TEST' onClick="window.location.href='index.php?page=errortests&manual_trigger'"> : Manually thrown error. This can be used in validation of input variables.</p>

    <h4>Missing jquery target</h4>
    <p><input type=submit class=buttion value='TEST' name='inc_syntax' onClick='javascript:CallAjax_Missing()'> : Invalid syntax : In this case : no ; in the calld ajax script.</p>

    <h4>PHP Syntax Error</h4>
    <p><input type=submit class=buttion value='TEST' name='inc_syntax' onClick='javascript:CallAjax_Syntax_Problem()'> : Invalid syntax : In this case : no ; in the called ajax script.</p>

    <h4>Javascript syntax Error</h4>
    <p><input type=submit class=buttion value='TEST' name='inc_syntax' onClick='javascript:SyntaxIssue()'> : Javascript Syntax Error : In this case : Missing closing bracket for function.</p>


</div>

<script type=text/javascript>
    function CallAjax_Missing() {
        console.log("Calling ajax_queries/Test.NotFound.php")
        $.get("ajax_queries/Test.NotFound.php", function(data) {

            // no need to do anything.
        });

    }

    function SyntaxIssue() {
        $.getScript('javascripts/BrokenScript.js')
    }

    function CallAjax_Syntax_Problem() {
        $.get("ajax_queries/Test.Broken.php", function(data) {
            // no need to do anything.
        });
    }
</script>