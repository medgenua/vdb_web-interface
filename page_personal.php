<?php

if ($loggedin != 1) {
    include('page_login.php');
    exit();
}

// SOME VARIABLES
$checked = array("", "checked");

//BUILD PAGE
echo "<div class=section>\n";
if (isset($_POST['submit'])) {
    $FirstName = $_POST['firstname'];
    $LastName = $_POST['lastname'];
    $email = $_POST['email'];
    $mailing = $_POST['mailing'];
    $Affiliation = $_POST['affiliation'];
    $error = '';
    if ($Affiliation == 'CreateAffi') {
        $Affiliation = $_POST['NewAffiliationName'];
        if ($Affiliation == '') {
            $error .= "<li>Newly created affiliation name can not be empty</li>";
        }
        $newaffi = 1;
    } else {
        $newaffi = 0;
    }
    $password = $_POST['pass'];
    $query = "SELECT id FROM `Users` WHERE id = '$userid' AND password=MD5('$password')";
    $auth  = runQuery($query, "Users");
    if ($password == "") {
        $error = $error . "<li>Password needed to update settings</li>\n";
    } elseif (count($auth) == 0) {
        $error = $error . "<li>Specified password incorrect for logged in user </li>\n";
    }
    if ($FirstName == "") {
        $error = $error . "<li>First name can not be empty</li>\n";
    }

    if ($LastName == "") {
        $error = $error . "<li>Last name can not be empty</li>\n";
    }
    if ($Affiliation == "") {
        $error = $error . "<li>Affiliation can not be empty</li>\n";
    }
    if (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email) || $email == "") {
        $error = $error . "<li>You specified an invalid email address<li>\n";
    }
    // API processing.
    $apiKey = '';
    $apiExpires = 1;
    $apiExpDate = 2000 - 01 - 01;

    if (isset($_POST['CreateAPIkey'])) {
        $apiKey = generateRandomString(32);
        $row = runQuery("SELECT id FROM `Users` WHERE apiKey = '$apiKey'");
        while (!empty($row)) {
            $apiKey = generateRandomString(32);
            $row = runQuery("SELECT id FROM `Users` WHERE apiKey = '$apiKey'");
        }
        $apiExpDate = date('Y-m-d', (time() + 7 * 24 * 60 * 60));
    } elseif (isset($_POST['ReNewAPI'])) {
        $apiKey = $_POST['currentAPI'];
        switch ($_POST['ReNewAPI']) {
            case 'disable':
                $apiKey = '';
                $apiExpDate = '2000-01-01';
                $apiExpires = 1;
                break;
            case 'week':
                $apiExpDate = date('Y-m-d', (time() + 7 * 24 * 60 * 60));
                $apiExpires = 1;
                break;
            case 'forever':
                $apiExpDate = '2000-01-01';
                $apiExpires = 0;
                break;
        }
    }
    if ($error == "") {
        // inserting new affiliation if needed
        if ($newaffi == 1) {
            $affid = insertQuery("INSERT INTO Affiliation (name) VALUES ('$Affiliation')", "Affiliation");
            $Affiliation = $affid;
        }
        //Updating entry
        doQuery("UPDATE `Users` SET FirstName='$FirstName', LastName = '$LastName', Affiliation = '$Affiliation', email = '$email', mailinglist = '$mailing',apiKey = '$apiKey', apiExpires = '$apiExpires',apiExpirationDate = '$apiExpDate' WHERE id = '$userid'", "Users");
        echo "<h3>Success !</h3>\n";
        echo "<p>Your user details were successfully updated.</p>\n";
        echo "<meta http-equiv='refresh' content='0;URL=index.php?page=personal' />";
        echo "</div>\n";
    } else {
        echo "<h3>Incorrect information provided !</h3>\n";
        echo "<p>There were some problems, so nothing has been updated. Please fix them and try again:\n<ol>\n";
        echo "$error";
        echo "</ol>\n";
        echo "</p></div>\n";
    }
} else {
    $row = array_shift(...[runQuery("SELECT u.LastName, u.FirstName, u.Affiliation, u.email,u.mailinglist,u.apiKey,u.apiExpires,u.apiExpirationDate FROM `Users` u WHERE u.id = '$userid'", "Users")]);
    $LastName = $row['LastName'];
    $FirstName = $row['FirstName'];
    $Affiliation = $row['Affiliation'];
    $mailing = $row['mailinglist'];
    $email = $row['email'];
    $apiKey = $row['apiKey'];
    $apiExpires = $row['apiExpires'];
    $apiExpDate = $row['apiExpirationDate'];
    echo "<h3>User Details</h3>\n";
    echo "<p>You can change all your personal details here. Change them below and press submit. All fields are mandatory.</p>\n";
    echo "</div>\n";
}
// MAIN FORM
echo "<div class=section>\n";
echo "<h3>Personal details</h3>\n";
echo "<p>\n";
echo "<form action=index.php?page=personal method=POST>\n";
echo "<table cellspacing=0 style='margin-left:1em;'>\n";
echo "<input type=hidden name=userid value=$userid>\n";
echo " <tr>\n  <th class=top>Option</th>\n";
echo "  <th class=top>Value</td>\n </tr>\n";
echo " <tr>\n";
echo "  <th class=left>First Name (*)</th>\n";
echo "  <td ><input type=text name=firstname value=\"$FirstName\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th class=left>Last Name (*)</th>\n";
echo "  <td ><input type=text name=lastname value=\"$LastName\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th class=left>Email/login (*)</th>\n";
echo "  <td ><input type=text name=email value=\"$email\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th class=left>Affiliation (*)</th>\n";
$rows = runQuery("SELECT id, name FROM `Affiliation` ORDER BY name", "Affiliation");
echo "  <td ><select id='affiliation' name='affiliation' onchange='CheckNew()'><option value='CreateAffi'>Create New Affiliation</option>";
foreach ($rows as $k => $row) {
    $aid = $row['id'];
    $aname = $row['name'];
    if ($aid == $Affiliation) {
        $selected = 'SELECTED';
    } else {
        $selected = '';
    }
    echo "  <option value=\"$aid\" $selected>$aname</option>\n";
}
echo "</select> <span id='NewAffiName' style='display:none;'> &nbsp; New Affliliation Name: <input type=text size=40 value='' name='NewAffiliationName'></span></td>\n";
echo " </tr>\n";
echo "<tr><th class=left>Recieve E-Mails (*)</th>";
echo "<td><select id='mailing' name='mailing'>";
if ($mailing == 0) {
    echo "<option value='0' SELECTED>No emails</option><option value='1'>Critical (e.g. bugs affecting #results)</option><option value='2'>Critical &amp; New Features (max 1/month)</option>";
} elseif ($mailing == 1) {
    echo "<option value='0' >No</option><option value='1' SELECTED>Critical (e.g. bugs affecting #results)</option><option value='2'>Critical &amp; New Features (max 1/month)</option>";
} elseif ($mailing == 2) {
    echo "<option value='0' >No</option><option value='1' >Critical (e.g. bugs affecting #results)</option><option value='2' SELECTED>Critical &amp; New Features (max 1/month)</option>";
}
echo "</select></td></tr>";


echo "<tr><td colspan=2 class=last>&nbsp;</td></tr>";
echo "</table></p>\n";

// API information //
echo "<h3>API Key</h3>";
echo "<p>The VariantDB API allows you to access the system and perform remote and automated filtering. For more information, please read the documentation <a href='index.php?page=tutorial&topic=api'>here</a>";
// case 1: api is active : show info && option to renew.
echo "<table cellspacing=0 style='margin-left:1em;'>\n";
if ($apiKey != '' && ($apiExpires == 0 || strtotime($apiExpDate) > strtotime(date("Y-m-d")))) {
    echo "<tr><th class=left>Current API-Key : </th><td>$apiKey<input type=hidden name='currentAPI' value='$apiKey'/></td></tr>";
    if ($apiExpires == 1) {
        echo "<tr><th class=left>Valid until: </th><td>$apiExpDate</td><tr/>";
        echo "<tr><th class=left>Renew Key:</th><td><select name='ReNewAPI'><option value='disable'>No, disable key now</option><option selected value='week'>Valid for 1 week</option><option value='forever'>Valid forever</option></select></td><tr/>";
    } else {
        echo "<th class=left>Valid until: </th><td>Does not expire</td><tr/>";
        echo "<tr><th class=left>Expire API Key:</th><td><select name='ReNewAPI'><option value='disable'>Disable key now</option><option value='week'>Expire in 1 week</option><option value='forever' selected>Never, keep key valid forever</option></select></td></tr>";
    }
} else {
    echo "<tr><td class=left>Create API Key:</th><td><input type=checkbox name=CreateAPIkey></td></tr>";
}
echo "</table>";


echo "</div>\n";


echo "<div class=section>\n";
echo "<h3>Double check and submit</h3>\n";
echo "<p>\n";
echo "<table style='margin-left:1em';>\n";
echo "<tr>\n";
echo " <th class=left>Password (*)</th>\n";
echo " <td ><input type=password name=pass size=40 maxlength=20 /></td>\n";
echo "</tr>\n";
echo "</table>\n";
echo "</p><p>\n";
echo "<input type=submit class=button name=submit value=Submit>\n";
echo "</form>\n";
echo "</div>\n";



function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>

<script type='text/javascript'>
    function CheckNew() {
        if (document.getElementById('affiliation').options[document.getElementById('affiliation').selectedIndex].value === 'CreateAffi') {
            document.getElementById('NewAffiName').style.display = '';
        } else {
            document.getElementById('NewAffiName').style.display = 'none';
        }
    }
</script>