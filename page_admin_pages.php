<?php
if ($level < 3) {
    echo "<div class=section><h3>Access Denied</h3><p>Only System administrator has access to these pages</p></div>";
    exit();
}

$type = $_GET['type'];

if ($type == 'license') {
    // 	
    echo "<div class=section>\n";
    echo "<h3>License Management (UNDER DEVELOPMENT)</h3>\n";
    echo "<p><table cellspacing=0>";
    echo " <tr>\n";
    echo "  <th class=top >Name <img src='Images/layout/plus-icon.png' style='margin-bottom:-0.1em;height:0.9em;' onClick='NewLicense(\"$level\")' title='Define New License' /></td>\n";
    echo "  <th class=top style='padding-left:3em'>URL</td>\n";
    echo "  <th class=top style='padding-left:3em'>Actions</td>\n";
    echo " <tr>\n";
    $rows = runQuery("SELECT lid, LicenseName, URL, Description FROM `License` ORDER BY LicenseName", "License");
    foreach ($rows as $k => $row) {
        $lid = $row['lid'];
        $LicenseName = $row['LicenseName'];
        $URL = $row['URL'];
        $desc = $row['Description'];
        echo " <tr >";
        echo "  <td title='$desc'>$LicenseName</td>";
        echo "  <td style='padding-left:3em'><a href='$URL'>Package Homepage</a></td>";
        echo "  <td style='padding-left:3em'><img title='Edit details' src='Images/layout/edit.gif' style='margin-bottom:-0.1em;height:0.9em;' onClick='EditLicense(\"$lid\",\"$level\")' /> / <img title='manage users with access' src='Images/layout/share_icon.png' style='margin-bottom:-0.1em;height:0.9em;' onClick='ShareLicense(\"$lid\",\"$level\")' /> / <img title='Delete the license' src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em;' onClick='DeleteLicense(\"$lid\",\"$level\")' /></td>";
        echo " </tr>";
    }
    echo "</table>";
} elseif ($type == 'users') {
    // userlevels
    $levels[] = "<option value=0 selected>Disabled</option>\n<option value=1>Guest access</option>\n<option value=2>Full access</option>\n<option value=3>Administrator</option>\n";
    $levels[] = "<option value=0 >Disabled</option>\n<option value=1 selected>Guest access</option>\n<option value=2>Full access</option>\n<option value=3>Administrator</option>\n";
    $levels[] = "<option value=0 >Disabled</option>\n<option value=1 >Guest access</option>\n<option value=2 selected>Full access</option>\n<option value=3>Administrator</option>\n";
    $levels[] = "<option value=0 >Disabled</option>\n<option value=1 >Guest access</option>\n<option value=2 >Full access</option>\n<option value=3 selected>Administrator</option>\n";

    echo "<div class=section>\n";
    echo "<h3>User Management</h3>\n";
    echo "<p>Overview of users, and option to set user levels. These levels define what can be used on the site.</p>\n";
    echo "<form action='ajax_queries/set_userlevel.php' method=POST>\n";
    echo "<input type=hidden name=userid value=$userid>";
    echo "<p>\n<table cellspacing=0>\n";
    echo " <tr>\n";
    echo "  <th class=top>Name</td>\n";
    echo "  <th class=top>username</td>\n";
    echo "  <th class=top>Affiliation</td>\n";
    echo "  <th class=top>E-mail</td>\n";
    echo "  <th class=top>level</td>\n";
    echo " <tr>\n";
    $switch = 0;
    $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name, u.Affiliation, u.email, u.level FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id", "Users:Affiliation");
    foreach ($rows as $k => $row) {
        $uid = $row['id'];
        $uLastName = $row['LastName'];
        $uFirstName = $row['FirstName'];
        $uAffiliation = $row['Affiliation'];
        $uAffiName = $row['name'];
        $uemail = $row['email'];
        $ulevel = $row['level'];
        echo " <tr>\n";
        echo "  <td ><input type=hidden name='uids[]' value='$uid'>$uFirstName $uLastName</td>\n";
        echo "  <td >$uname</td>\n";
        echo "  <td >$uAffiName</td>\n";
        echo "  <td>$uemail</td>\n";
        echo "  <td><select name=perm_$uid>" . $levels[$ulevel] . "</select></td>\n";
        echo " </tr>\n";
    }
    echo "<tr><td colspan=5 class=last>&nbsp;</td></tr>";
    echo "</table>\n";
    echo "</p>";
    echo "<input type=submit class=button value=Update></form>\n";
    echo "</div>\n";
} elseif ($type == 'impersonate') {
    echo "<div class=section>";
    echo "<h3>Impersonate User</h3>";
    echo "<p>Impersonating a different user will allow administrators to access all data of that user, and to perform tasks in their name. Select a user from the list below and click 'Impersonate'. You will be redirected to their start page. To access your own results again, log out and log in under your own account. </p>";
    $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id WHERE u.id != '$userid' ORDER BY a.name ASC, u.LastName ASC", "Users:Affiliation");
    $curraffi = '';
    echo "<p><form action='Impersonate_user.php' method='POST'>";
    echo "<select name='ImpersonateID'>";
    foreach ($rows as $k => $row) {
        $id = $row['id'];
        $name = $row['LastName'] . ' ' . $row['FirstName'];
        $affi = $row['name'];
        if ($affi != $curraffi) {
            ## print affiliation group
            if ($curraffi != '') {
                echo "</optgroup>";
            }
            echo "<optgroup label='$affi'>";
            $curraffi = $affi;
        }
        echo "<option value='$id'>$name</option>";
    }
    if ($curraffi != '') {
        echo "</optgroup>";
    }
    echo "</select>";
    echo "<input type='submit' name='Impersonate' value='Impersonate'></form> </p>";
    echo "</div>";
} elseif ($type == 'update') {
    include('page_update.php');
} elseif ($type == 'lift') {
    $step = $_GET['step'];
    if (!isset($_GET['step'])) {
        include('LiftOver/inc_Intro.inc');
    } else {
        $row = array_shift(...[runQuery("SELECT file FROM `NGS-Variants-Admin`.`LiftOverSteps` WHERE idx = '$step'", "LiftOverSteps")]);
        include($row['file']);
    }
} elseif ($type == 'SiteStatus') {
    if (isset($_POST['changestatus'])) {
        $newstatus = $_POST['SS'];
        $SiteStatus = $newstatus;
        doQuery("UPDATE `NGS-Variants-Admin`.`SiteStatus` SET status = '$newstatus' WHERE 1=1", "SiteStatus");
    }
    echo "<div class=section>\n";
    echo "<h3>Platform Status : $SiteStatus</h3>\n";
    echo "<p>Access to the platform can be blocked for all non-admin users, so that upgrades can be performed safely. To do so, set the status to 'Under Construction'. When you are planning a LiftOver operation to a new genomic build, use 'LiftOver'. This will block all future analyses, while allowing regular browsing (i.e. The platform becomes read-only). </p>\n";
    echo "<form action='index.php?page=admin_pages&amp;type=SiteStatus' method=POST>\n";
    echo "Set platform status: <select name='SS'>";
    if ($SiteStatus == 'Operative') {
        echo "<option value=Operative selected>Fully Operational</option><option value=Construction>Under Construction</option><option value=LiftOver>Performing LiftOver</option></select>";
    } elseif ($SiteStatus == 'Construction') {
        echo "<option value=Operative >Fully Operational</option><option value=Construction selected>Under Construction</option><option value=LiftOver>Performing LiftOver</option></select>";
    } elseif ($SiteStatus == 'LiftOver') {
        echo "<option value=Operative >Fully Operational</option><option value=Construction>Under Construction</option><option value=LiftOver selected>Performing LiftOver</option></select>";
    } else {
        echo "<option value=Operative >Fully Operational</option><option value=Construction>Under Construction</option><option value=LiftOver >Performing LiftOver</option></select>";
    }
    echo "<input type=submit class=button value='Set Mode' name=changestatus></p>";
    echo "</form>";
    echo "</div>\n";
} elseif ($type == 'mcstats') {
    echo "<div class=section>\n";
    echo "<h3>Memcache statistics</h3>\n";
    if ($usemc != 1) {
        echo "<p>Memcache is not activated.</p>";
        echo "</div>";
    } else {
        // first, delete caches?
        if (isset($_POST['ClearMC'])) {
            doQuery("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE 1", "memcache_idx");
        }
        // first, delete caches (disabled)?
        if (isset($_POST['ClearSelectedMC'])) {
            $selected = $_POST['clear'];
            foreach ($selected as $idx => $tname) {
                doQuery("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` = '$tname'", "memcache_idx");
            }
        }

        echo "<form action='index.php?page=admin_pages&type=mcstats' method='POST'>";
        echo "<p><input type=submit class=button name='ClearMC' value='Clear All Buffered Data' /> or <input type=submit class=button name='ClearSelectedMC' value='Clear Selected Data' /> </p><p>";
        echo "<table cellspacing=0 class=w75>";
        echo "<tr><th class=top>Table</th><th class=top>Clear</th></tr>";
        $rows = runQuery("SELECT `Table_Name` FROM `NGS-Variants-Admin`.`memcache_idx` ORDER BY `Table_Name`", "memcache_idx");
        foreach ($rows as $k => $row) {
            echo "<tr><td>" . $row['Table_Name'] . "</td><td><input type='checkbox' name=\"clear[]\" value='" . $row['Table_Name'] . "' /></td></tr>";
        }

        echo "</table></form>";
        echo "</div>";
    }
} elseif ($type == 'newsbrief') {
    if (isset($_POST['sendmail'])) {
        echo "<div class=section>";
        echo "<h3>Sending Emails</h3>";
        echo "<p><ul>";
        $type = $_POST['etype'];
        $message = $_POST['message'];
        $subject = $_POST['subject'];
        if ($message == '' || $subject == '') {
            echo "<div class=section><h3>Error, subject or message missing.</h3></div>";
            exit;
        }
        $headers = "From: $adminemail\r\n";
        // get recipients.
        $query = "";
        if ($type == 'updates') {
            $query = "SELECT email FROM `Users` WHERE mailinglist = 2 AND level > 0";
            $subj = "VariantDB News: $subject";
        } elseif ($type == 'critical') {
            $query = "SELECT email FROM `Users` WHERE mailinglist IN (1,2) AND level > 0";
            $subj = "VariantDB Critical Update: $subject";
        } elseif ($type == 'all') {
            $query = "SELECT email FROM `Users` WHERE level > 0";
            $subj = "VariantDB Message: $subject";
        }
        $q = runQuery($query, "Users");
        $ok = 0;
        foreach ($q as $k => $row) {
            $to = $row['email'];
            $lh = $headers . "To: $to\r\n";
            if (!mail($to, $subj, $message, $lh)) {
                echo "<li> Unable to send email to '$to'</li>";
            } else {
                $ok++;
            }
        }
        echo "<li> $ok emails were sent successfully.</li>";
        echo "</ul></p></div>";
    } else {
        echo "<div class=section>\n";
        echo "<h3>Send Email to Users</h3>\n";
        echo "<form action='index.php?page=admin_pages&type=newsbrief' method='POST'>";
        echo "<p>Provide the email content below. The subject description is added to the default subject (VariantDB News: / VariantDB Critical Update: ) and must be max 25 characters. Respect user preferences and don't use the send-to-all functionality!";
        echo "<table cellspacing=0 style='margin-left:1em;'>\n";
        //echo "<input type=hidden name=userid value=$userid>\n";
        echo "  <th class=left>Email Type:</th>\n";
        echo "  <td ><select id='etype' name='etype'><option value='updates'>Monthly News</option><option value='critical'>Critical Update</option><option value='all'>Message To All Users</option></select></td>\n";
        echo " </tr>\n";
        echo " <tr>\n";
        echo "  <th class=left>Subject:</th>";
        echo "  <td><input type=text name='subject' id='subject' maxlength='25' size='25' /></td>";
        echo "  </tr>";
        echo " <tr>\n";
        echo "  <th class=left>Message:</th>";
        echo "   <td><textarea id='message' name='message' rows=10 cols=40 ></textarea></td>";
        echo " </tr>";
        echo " <tr><td colspan=2><input type=submit class=button name='sendmail' value='Send' /></td></tr>";
        echo "</form>";
        echo "</div>\n";
    }
} else {
    echo "<div class=section>\n";
    echo "<h3>Administration Pages</h3>\n";
    echo "<p>These pages are only accessible for the platform administrators. They are assigned by a userlevel of 3 (see 'users'). The different tasks that can be performed from here are accessible from the top menu.</p><p> One advice: <span class=bold> Be Careful !</span></p>\n";
    echo "</div>\n";
}
