<?php
// check login
if ($loggedin != 1) {
    echo "<div class=section><h3>Manage your projects</h3>\n";
    include('page_login.php');
    echo "</div>\n";
    exit();
}
// get import-id
if (isset($_POST['iid'])) {
    $iid = $_POST['iid'];
} elseif (isset($_GET['iid'])) {
    $iid = $_GET['iid'];
} else {
    $iid = '';
}


// no import submitted, show input fields.
if ($iid == '') {

    echo "<div class=section>";
    echo "<h3>Import VariantDB projects</h3>";
    echo "<p>This form can only be used to import projects that were exported from (a different instance of) VariantDB. The required files are gpg-encrypted, gzipped tarballs :'.tar.gz.gpg'. </p>";
    echo "<p>Small projects can be uploaded directly. For large projects (>2Gb), the archive must be uploaded to the FTP server.</p>";
    echo "<p>Select a source: <select id='source' onchange='toggleSelect()'><option selected>Direct Upload</option><option>FTP server</option></select></p>";
    // upload form.
    echo "<div id='direct_upload'>";
    echo "<p><span class=emph>Direct Upload:</span></p>";
    echo "<form action='index.php?page=import' method=POST enctype='multipart/form-data'>";
    echo "<input type=hidden name='source' value='upload'>";
    echo "<input type=hidden name='iid' value='" . time() . "'>";
    echo "<p>Select a file : <input type='file' id='upload_file' name='upload_file' onchange='ValidateUpload()' /><span id='upload_file_comment' style='color:red;'>&nbsp;</span></p>";
    echo "<p>Passphrase : <input type=text id='upload_pass' name='upload_pass' size=14 oninput='ValidateUpload()' /></p>";
    echo "<p><input type=submit name='SubmitUPLOAD' id='SubmitUPLOAD' value='Import' disabled/></p>";
    echo "</form></div>";
    // ftp form.
    echo "<div id='ftp_server' style='display:none;'>";
    echo "<p><span class=emph>FTP server:</span></p>";
    echo "<form action='index.php?page=import' method=POST>";
    echo "<input type=hidden name='source' value='ftp'>";
    echo "<input type=hidden name='iid' value='" . time() . "'>";
    //echo "<p>Select a file : <input type='file' id='ftp_file' name='ftp_file' onchange='ValidateUpload()' /><span id='gpg_file_comment'>&nbsp;</span></p>";
    echo "<p>Passphrase : <input type=text id='ftp_pass' name='ftp_pass' size=14 onchange='ValidateFTP()' /></p>";
    echo "<p><input type=submit name='SubmitFTP' value='Import' disabled/></p>";
    echo "</form></div>";



    echo "</div>";
}
// import id specified, direct upload.
elseif (isset($_POST['source']) && $_POST['source'] == 'upload') {
    // upload succeeded ? 
    if ($_FILES['upload_file']['error']) {
        $phpFileUploadErrors = array(
            0 => 'There is no error, the file uploaded with success',
            1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
            2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
            3 => 'The uploaded file was only partially uploaded',
            4 => 'No file was uploaded',
            6 => 'Missing a temporary folder',
            7 => 'Failed to write file to disk.',
            8 => 'A PHP extension stopped the file upload.',
        );
        echo "<div class=section><h3>Error: Failed to upload file</h3></div>";
        echo "<p>Sorry, there was an error uploading your file: " . $phpFileUploadErrors[$_FILES['upload_file']['error']] . "</p>";
        echo "<p>The system admin was notified.</p>";
        echo "</div>";

        trigger_error("Failed to upload file: " . $phpFileUploadErrors[$_FILES['upload_file']['error']], E_USER_ERROR);
    }

    $pass = $_POST['upload_pass'];

    // make a tmp file (path is constructed similar to export routine).
    $tmpdir = "/tmp/vdb_$userid.$iid";
    if (file_exists($tmpdir)) {
        $result = system("rm -Rf '$tmpdir'", $result);
        if ($result != 0) {
            trigger_error("Error removing dir : $tmpdir", E_USER_ERROR);
        }
    }
    if (!mkdir($tmpdir, 0700, true)) {
        trigger_error("Error creating dir : $tmpdir", E_USER_ERROR);
    }
    // move uploaded file to tmp dir.
    $target_file = $tmpdir . "/" . basename($_FILES["upload_file"]["name"]);
    trigger_error("uploading file: " . $_FILES['upload_file']['tmp_name']);
    if (!move_uploaded_file($_FILES["upload_file"]["tmp_name"], $target_file)) {
        echo "<div class=section><h3>Error: Failed to upload file</h3></div>";
        echo "<p>Sorry, there was an error uploading your file. Did you check if it was smaller than 2Gb? Otherwise, please report this.</p>";
        echo "</div>";
        trigger_error("Failed to move uploaded file:" . $target_file, E_USER_ERROR);
        exit;
    }
    // decrypt && extract.
    system("mkdir '$tmpdir/.gnupg'");
    // might not be optimal in security, as we bypass some integrity checks. 
    $command = "(cd $tmpdir && gpg --ignore-mdc-error --pinentry-mode loopback --no-permission-warning --homedir $tmpdir/.gnupg/ -d --passphrase '$pass' $target_file | tar xz ) 2>$tmpdir/decrypt_stderr";
    system($command, $return);

    if ($return != 0) {
        echo "<div class=section><h3>Error: Failed to decode file</h3></div>";
        echo "<p>Sorry, there was an error extracting your file. Did you provide the correct password? Otherwise, please report this.</p>";
        echo "</div>";
        $stderr = file_get_contents("$tmpdir/decrypt_stderr");
        trigger_error("Failed to decrypt file :$target_file. Error msg : $stderr", E_USER_ERROR);
        exit;
    }
    // put userid in place
    $fh = fopen("$tmpdir/uid", 'w');
    fwrite($fh, $userid);
    fclose($fh);
    // launch import.
    #$command = "(echo 0 > $tmpdir/status ; cd $scriptdir/cgi-bin ; perl ImportProject.pl -d '$tmpdir' > $tmpdir/progress.txt 2>&1 && echo 1 > $tmpdir/status) > /dev/null 2>&1 &";
    $command = "(echo 0 > $tmpdir/status && cd $scriptdir/cgi-bin && " . $config['CONDA_ENV'] . "/bin/python ImportProject.py -d '$tmpdir' >> $tmpdir/progress.txt 2>&1 && echo 1 > $tmpdir/status) >> $tmpdir/progress.txt 2>&1 &";
    trigger_error($command);

    system($command, $result);
    if ($result != 0) {
        echo "<div class=section><h3>Error: Failed to decode file</h3></div>";
        echo "<p>Sorry, there was an error launching the import routine. Please report this.</p>";
        echo "</div>";
        $stderr = file_get_contents("$tmpdir/progress.txt");
        trigger_error("Failed to launch import. Error msg : $stderr", E_USER_ERROR);
        exit;
    }
    // redirect to monitor page.
    echo "<div class=section><h3>Import started</h3>";
    echo "<p>You will be redirected to a monitor page in 3 seconds</p>";
    echo "<meta http-equiv='refresh' content='3;URL=index.php?page=import&iid=$iid&m=1'>\n";
    echo "</div>";
}

// monitor
elseif (isset($_GET['m'])) {
    $tmpdir = "/tmp/vdb_$userid.$iid";
    // print progress.
    echo "<div class=section>";
    echo "<h3>Import Progress</h3>";
    $out = 'starting up...';
    if (file_exists("$tmpdir/progress.txt")) {
        $out = file_get_contents("$tmpdir/progress.txt");
    }

    // clean up if finished
    $stat = file_get_contents("$tmpdir/status");
    $stat = rtrim($stat);
    if ($stat == 1) {
        system("rm -Rf $tmpdir");
        echo "<p>Import finished.</p>";
    } else {
        // else: refresh in 30s.
        echo "<p>Import running. Page will refresh in 30s.</p>";
        echo "<meta http-equiv='refresh' content='30;URL=index.php?page=import&iid=$iid&m=1'>\n";
    }

    echo "<p><pre>";
    echo $out;
    echo "</pre></p>";
    echo "</div>";
}
