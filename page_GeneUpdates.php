<?php

echo "<div class=section>";
echo "<h3>Overview of out-of-date Gene Symbols</h3>";

// get my gene panels.
$query = "SELECT gpg.gid,gpg.Symbol,gpg.Comment,gp.Name FROM `GenePanels` gp JOIN `GenePanels_x_Genes_ncbigenes` gpg JOIN `GenePanels_x_Users` u ON gpg.gpid = gp.id AND u.gpid = gp.id WHERE u.uid = '$userid'";
$rows = runQuery($query, "GenePanels:GenePanels_x_Genes_ncbigenes:GenePanels_x_Users");
$gp_genes = array();
foreach ($rows as $row) {
    $gp_genes[$row['Symbol']]['gid'] = $row['gid'];
    $gp_genes[$row['Symbol']]['comment'] = $row['Comment'];
    $gp_genes[$row['Symbol']]['gp'][$row['Name']] = 1; // = $row['Name'];
}

// fetch genes.
$query = "SELECT id, refGene, refLink, GROUP_CONCAT(transcript ORDER BY `transcript` SEPARATOR ', ') as transcript FROM `GeneName_Discrepancies` GROUP BY id,refGene,refLink  ORDER BY `refGene` ";
$rows = runQuery($query, "GeneName_Discrepancies");
// placeholders
$outdated_inPanel = "";
$outdated = "";
$removed_inPanel = "";
$removed = "";
$novel = "";
$novel_inPanel = "";
foreach ($rows as $row) {
    # removed
    if ($row['refLink'] == -1) {
        if (array_key_exists($row['refGene'], $gp_genes)) {
            // try to find the current standard name : 
            $sqr = array_shift(...[runQuery("SELECT DISTINCT `refLink` AS 'refLink' FROM `GeneName_Discrepancies` WHERE `refGene` = '" . $row['refGene'] . "' AND `refLink` != '-1'", "GeneName_Discrepancies")]);
            if (count($sqr) > 0) {
                $reflink = $sqr['refLink'];
            } else {
                $reflink = "unknown";
            }
            $removed_inPanel .= "<tr class=vrow><td NOWRAP style='vertical-align:top'>" . $row['refGene'] . "</td><td class=italic NOWRAP style='vertical-align:top'>$reflink</td><td style='vertical-align:top'>" . $row['transcript'] . "</td><td class=italic style='vertical-align:top'>unknown</td><td style='vertical-align:top'>" . implode(", ", array_keys($gp_genes[$row['refGene']]['gp'])) . "</td></tr>";
        } else {
            $removed .= "<tr class=vrow NOWRAP><td style='vertical-align:top'>" . $row['refGene'] . "</td><td class=italic NOWRAP style='vertical-align:top'>unknown</td><td style='vertical-align:top'>" . $row['transcript'] . "</td><td class=italic style='vertical-align:top'>unknown</td><td style='vertical-align:top'> - </td></tr>";
        }
    }
    #novel transcript
    elseif ($row['refGene'] == $row['refLink']) {
        if (array_key_exists($row['refLink'], $gp_genes)) {
            $novel_inPanel .= "<tr class=vrow><td style='vertical-align:top'>" . $row['refGene'] . "</td><td style='vertical-align:top'>" . $row['refLink'] . "</td><td style='vertical-align:top'>" . $row['transcript'] . "</td><td style='vertical-align:top'>" . $row['id'] . "</td><td style='vertical-align:top'>" . implode(", ", array_keys($gp_genes[$row['refLink']]['gp'])) . "</td></tr>";
        } else {
            $novel .= "<tr class=vrow><td NOWRAP style='vertical-align:top'>" . $row['refGene'] . "</td><td style='vertical-align:top'>" . $row['refLink'] . "</td><td NOWRAP style='vertical-align:top'>" . $row['transcript'] . "</td><td style='vertical-align:top'>" . $row['id'] . "</td><td style='vertical-align:top'> - </td></tr>";
        }
    }
    # replaced
    else {
        if (array_key_exists($row['refGene'], $gp_genes) || array_key_exists($row['refLink'], $gp_genes)) {
            $outdated_inPanel .= "<tr class=vrow><td NOWRAP style='vertical-align:top'>" . $row['refGene'] . "</td><td NOWRAP style='vertical-align:top'>" . $row['refLink'] . "</td><td style='vertical-align:top'>" . $row['transcript'] . "</td><td style='vertical-align:top'>" . $row['id'] . "</td><td style='vertical-align:top'>OLD symbol: " . implode(", ", array_keys($gp_genes[$row['refGene']]['gp'])) . "<br/>NEW symbol: " . implode(", ", array_keys($gp_genes[$row['refLink']]['gp'])) . "</td></tr>";
        } else {
            $outdated .= "<tr class=vrow><td NOWRAP style='vertical-align:top'>" . $row['refGene'] . "</td><td NOWRAP style='vertical-align:top'>" . $row['refLink'] . "</td><td style='vertical-align:top'>" . $row['transcript'] . "</td><td style='vertical-align:top'>" . $row['id'] . "</td><td style='vertical-align:top'> - </td></tr>";
        }
    }
}
echo "<h4 style='color:red'>Genes Present in GenePanels</h4>";
echo "<span class=emph>Replaced Gene Symbols</span>";
if ($outdated_inPanel != '') {
    echo "<p>The following symbols listed in GenePanels are outdated:";
    echo "<table style='margin-left:2em;'  cellspacing=0 width='100%'>";
    echo "<tr ><th class=top>Symbol</th><th class=top>Current Standard</th><th class=top>Transcript</th><th class=top>GeneID</th><th class=top>GenePanel</th></tr>";
    echo $outdated_inPanel;
    echo "<tr><td class=last colspan=5>&nbsp;</td></tr>";
    echo "</table></p>";
} else {
    echo "<p>No entries found</p>";
}

echo "<span class=emph>Removed Gene Symbols</span>";
if ($removed_inPanel != '') {
    echo "<p>The following symbols or transcripts listed in GenePanels are outdated and no replacement was found:";
    echo "<table style='margin-left:2em;'  cellspacing=0 width='100%'>";
    echo "<tr ><th class=top>Symbol</th><th class=top>Current Standard</th><th class=top>Transcript</th><th class=top>GeneID</th><th class=top>GenePanel</th></tr>";
    echo $removed_inPanel;
    echo "<tr><td class=last colspan=5>&nbsp;</td></tr>";
    echo "</table></p>";
} else {
    echo "<p>No entries found</p>";
}
echo "<span class=emph>Novel Transcripts</span>";
if ($novel_inPanel != '') {
    echo "<p>The following transcripts listed in GenePanels are novel:";
    echo "<table style='margin-left:2em;'  cellspacing=0 width='100%'>";
    echo "<tr ><th class=top>Symbol</th><th class=top>Current Standard</th><th class=top>Novel Transcripts</th><th class=top>GeneID</th><th class=top>GenePanel</th></tr>";
    echo $novel_inPanel;
    echo "<tr><td class=last colspan=5>&nbsp;</td></tr>";
    echo "</table></p>";
} else {
    echo "<p>No entries found</p>";
}



echo "<h4 style='color:red'>Genes not Present in GenePanels</h4>";
echo "<span class=emph>Replaced Gene Symbols</span>";
if ($outdated != '') {
    echo "<p>The following symbols are outdated but not used in GenePanels:";
    echo "<table style='margin-left:2em;'  cellspacing=0 width='100%'>";
    echo "<tr ><th class=top>Symbol</th><th class=top>Current Standard</th><th class=top>Transcript</th><th class=top>GeneID</th><th class=top>GenePanel</th></tr>";
    echo $outdated;
    echo "<tr><td class=last colspan=5>&nbsp;</td></tr>";
    echo "</table></p>";
} else {
    echo "<p>No entries found</p>";
}
echo "<span class=emph>Removed Gene Symbols</span>";
if ($removed != '') {
    echo "<p>The following symbols or transcripts are outdated without replacement, but not used in GenePanels:";
    echo "<table style='margin-left:2em;'  cellspacing=0 width='100%'>";
    echo "<tr ><th class=top>Symbol</th><th class=top>Current Standard</th><th class=top>Transcript</th><th class=top>GeneID</th><th class=top>GenePanel</th></tr>";
    echo $removed;
    echo "<tr><td class=last colspan=5>&nbsp;</td></tr>";
    echo "</table></p>";
} else {
    echo "<p>No entries found</p>";
}
echo "<span class=emph>Novel Gene Symbols</span>";
if ($novel != '') {
    echo "<p>The following symbols or transcript are novel, but not used in GenePanels:";
    echo "<table style='margin-left:2em;' cellspacing=0 width='100%'>";
    echo "<tr ><th class=top>Symbol</th><th class=top>Current Standard</th><th class=top>Transcript</th><th class=top>GeneID</th><th class=top>GenePanel</th></tr>";
    echo $novel;
    echo "<tr><td class=last colspan=5>&nbsp;</td></tr>";
    echo "</table></p>";
} else {
    echo "<p>No entries found</p>";
}

echo "</div>";
