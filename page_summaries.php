<link rel='stylesheet' href='javascripts/jstree/themes/default/style.min.css'/>
<script type='text/javascript' src='javascripts/jstree/jstree.min.js'></script>
<script type='text/javascript' src='javascripts/bsn.AutoSuggest_2.1.3.js'></script>
<link rel='stylesheet' type='text/css' href='StyleSheets/autosuggest.css' />

<div class=section><h3>Variant frequency</h3>
<p><span class=strong>NOTE:</span> User "Edwin" wordt gebruikt, vermits alle import onder die account gebeurd.</p>

<div>
   <p><input type='text' id='pos' size=30 /> Copy variant location here : (chr7:1245879) </p>
   <p><input type='text' id='ref' size=30 /> Variant Reference Allele </p>
   <p><input type='text' id='alt' size=30 /> Variant Alternate Allele </p>
   <p><input class=button type=submit value="Get Frequencies" onClick="LoadFrequencies()"</p>
</div>

<script type='text/javascript' src='javascripts/page.variants.js'></script>
<script type='text/javascript'>
function LoadFrequencies() {
    // loading ...
    SetLoading('Loading...');
    // fetch.
    var url = "ajax_queries/SamplesWithVariantByVariant.php";
    var pars = {
        pos: $("#pos").val(),
        ref: $("#ref").val(),
        alt: $("#alt").val()
    };  
    console.log(pars);
    $.post(url, pars, function (data) {
        $('#overlay').html(data);
    }); 
}   
</script>

