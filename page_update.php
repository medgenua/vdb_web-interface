<?php
if ($loggedin != 1) {
	echo "<div class=section><h3>Access Denied</h3>\n";
	include('page_login.php');
	echo "</div>";
	exit();
}
if ($level < 3) {
	echo "<div class=section><h3>Access Denied</h3>\n";
	echo "<p>Administrator account is required to view this page.</p>";
	echo "</div>";
	exit();
}
//require_once 'xmlLib.php';

//////////////////////////////////////////////////////
// 0. Run the annotation updater in the background. //
//////////////////////////////////////////////////////
if (isset($_GET['annotate']) && $_GET['annotate'] == 1) {
	// first : check if database is up to date.
	$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/../Database_Revisions && hg incoming > /tmp/$rand.hg.out && chmod 777 /tmp/$rand.hg.out \") > /dev/null 2>&1"; 
	system($command);
	$content = file_get_contents("/tmp/$rand.hg.out");
	system("rm -f /tmp/$rand.hg.out");
	if (!stristr($content,'no changes found')) {
		echo "<div class=section><h3>Database not up to date</h3>";
		echo "<p>You can not update the annotation files with an out-dated database. Please go back an update your database first.</p>";
		echo "</div>";
		exit();
	}
	$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/Annotations && chmod 777 Config.md5 && perl UpdateDBs.pl -u '$username' & \") >/$scriptdir/Query_Logs/VariantDB_UpdateDBs.log 2>&1";
	system($command);
	echo "<div class=section><h3>Annotation databases update started</h3>";
	echo "<p>The annotation files are being updated in the background. This can take a while to finish if new files have to be fetched. You will recieve an email with the results once the update is finished.</p>";
	$newmd5 = md5_file("$scriptdir/Annotations/Config.xml");
	$fh = fopen("$scriptdir/Annotations/Config.md5",'w');
	fwrite($fh,$newmd5);
	fclose($fh);
	echo "<p><a href='index.php?page=admin_pages&type=update'>Go back to the System updates page</a>.</p>";
	echo "</div>";
	//exit;	
	
}

//////////////////////////
// 1. UPDATE THE SYSTEM //
//////////////////////////

elseif (isset($_POST['submit'])) {
	ob_end_flush();
	if (count($_POST['update']) > 0) {
		echo "<div class=section>";
		echo "<h3>Updating VariantDB</h3>";
		echo "<p>The output of the updating process is shown below. </p>";
		echo " <p><ol>";
		$rand = rand(1,1500);
		$dodb = 0; // switch for database update.
		foreach ($_POST['update'] as $key => $value) {
			$updatedir = '';
			$title = '';
			switch ($value) {
				case "web": 
					$updatedir = $scriptdir; #$sitedir;
					$title = 'Web-Interface';
					break;
				case "credentials":
					$updatedir = "$scriptdir/../.Credentials";
					$title = 'Credentials Scripts';
					break;
				case "maintenance":
					$updatedir = $maintenancedir;
					$title = 'Maintenance Scripts';
					break;
				case "database":
					// seperate processing, can take a while !
					$dodb = 1;
					continue;
					break;
			}
			if ($updatedir == '') {
				continue;
			}
			$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $updatedir && hg pull -u > /tmp/$rand.hg.out && chmod 777 /tmp/$rand.hg.out \") > /dev/null 2>&1";
			system($command);
			$content = file_get_contents("/tmp/$rand.hg.out");
			system("rm -f /tmp/$rand.hg.out");
			echo "<li>Update Results for $title <br/>";
			echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>$content</textarea></li>";
			flush();
			// trigger validation
			$lck = fopen("$scriptdir/Query_Results/.validator.lck","r+");
			if(flock($lck,LOCK_EX)) {
				$q = fopen("$scriptdir/Query_Results/.validator.queue","a");
				fwrite($q, "ANNO\n");
				fclose($q);
				flock($lck,LOCK_UN);
			}
			fclose($lck);
		}
		$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir && chmod 777 Annotations/panel.update.running.txt && chmod 777 Annotations/update.txt && chmod 777 Annotations/update.panel.txt && chmod 777 Annotations/global.update.running.txt \") > /dev/null 2>&1";
		system($command);
				// now database
		if ($dodb == 1) {
			// disable access to webinterface.
			doQuery("UPDATE `NGS-Variants-Admin`.`SiteStatus` SET status = 'Construction' WHERE 1=1","SiteStatus");
			$updatedir = "$scriptdir/../Database_Revisions";
			$command = " (echo 1 > /tmp/$rand.db.status && COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $updatedir && hg pull -u > /tmp/$rand.hg.out && perl ApplyUpdates.pl >> /tmp/$rand.hg.out && chmod 777 /tmp/$rand.hg.out \" && echo 0 > /tmp/$rand.db.status && chmod 777 /tmp/$rand.db.status) > /dev/null 2>&1 &";
			echo "<script type='text/javascript'>interval1 = setInterval(\"reloadupdatespan('$rand')\",10000)</script>\n";
			echo "<li>Updating Database: <br/><pre id='db$rand' class=scrollbarbox>Starting Update Process...</pre></li>\n";
			system($command);
			flush();
			echo "<script type='text/javascript'>interval2 = setInterval(\"checkupdatestatus(2,$rand)\",10000)</script>\n"; 
		}
		// check if annotation config changed
		$newmd5 = md5_file("$scriptdir/Annotations/Config.xml");
		if (file_exists("$scriptdir/Annotations/Config.md5")){
			$oldmd5 = file_get_contents("$scriptdir/Annotations/Config.md5");
		}
		else {
			$oldmd5 = '';
		}
		if ("$oldmd5" != "$newmd5") {
			$note = "<div style='border:1px solid red;'><p><span class=strong style='color:red'>NOTE:</span> The configuration file of the available was updated. It is <span class=bold>highly</span> recommended to run the updater for the annotation databases. This will download any additional databases that are missing on the installation. ";
			if ($dodb == 0) {
				$note  .= "<span class=bold>You should first update the database the the latest version before starting the annotation update! </span>";
			}
			$note .= "</p><p><a href='index.php?page=admin_pages&type=update&annotate=1'> ==> START UPDATE <== </p></div>";
		}
		else {
			$note = '';
		}
		// hide link untill finished.
		echo "</ol></p>";
		if ($dodb == 1) {
			echo $note;	
			echo "<p><span id='toupdates' style='display:none;'><a href='index.php?page=admin_pages&type=update'>Go back to the System updates page</a>.</span></p>";
		}
		else {
			echo $note;
			echo "<p><a href='index.php?page=admin_pages&type=update'>Go back to the System updates page</a>.</p>";
		}			
		echo "</div>";
		// reset the cookie
		setcookie('updates','',1);
		// rerun check for updates
		//echo "<script type='text/javascript' src='javascripts/updates.js'></script>\n";
		//exit();
	}
}

//////////////////////////
// 2. CHECK FOR UPDATES //
//////////////////////////
else {
	// output 
	echo "<div class=section>";
	echo "<h3>Checking for VariantDB updates</h3>";
	echo "<p> Check the items you want to update below. After submission, all the updates will be fetched from the main repository. It is highly recommended to perform updates to the database at a low usage time, as the system will be put in a read-only mode, and a backup will be taken, which may cause severe performance drops for a couple of minutes.</p><p id=loading>Checking for updates....</p>";
	echo "<form action='index.php?page=admin_pages&type=update' method=POST>";
	$updates = 0;
	echo "<p><ol>";

	// Check for updates for Web-Interface ($scriptdir in credentials is full path to web-dir)
	$rand = rand(1,1500);
	$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir && hg incoming > /tmp/$rand.hg.out && chmod 777 /tmp/$rand.hg.out \") >> /tmp/output 2>&1"; 
	system($command);
	$content = file_get_contents("/tmp/$rand.hg.out");
	system("rm -f /tmp/$rand.hg.out");
	if (stristr($content,'no changes found')) {
		echo "<li>Web-Interface : no changes found</li>";
	}
	else {
		echo "<li>Web-Interface : <input type='checkbox' name='update[]' value='web' checked /> Update<br/>";
		echo "<textarea style='padding-left:2em;margin-left:2em;overflow:autp;' cols=75 rows=9 readonly='yes'>$content</textarea></li>"; 
		$updates++;
	}
	flush();
	// Check for updates for Credentials scripts
	$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/../.Credentials && hg incoming > /tmp/$rand.hg.out && chmod 777 /tmp/$rand.hg.out \") > /dev/null 2>&1"; 
	system($command);
	$content = file_get_contents("/tmp/$rand.hg.out");
	system("rm -f /tmp/$rand.hg.out");
	if (stristr($content,'no changes found')) {
		echo "<li>Credentials Scripts : no changes found</li>";
	}
	else {
		echo "<li>Credentials Scripts : <input type='checkbox' name='update[]' value='credentials' checked /> Update <br/>";
		echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>$content</textarea></li>"; 
		$updates++;
	}
	flush();
	// Check for updates for Database 
	$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/../Database_Revisions && hg incoming > /tmp/$rand.hg.out && chmod 777 /tmp/$rand.hg.out \") > /dev/null 2>&1"; 
	system($command);
	$content = file_get_contents("/tmp/$rand.hg.out");
	system("rm -f /tmp/$rand.hg.out");
	if (stristr($content,'no changes found')) {
		echo "<li>Database Revisions : no changes found</li>";
	}
	else {
		echo "<li>Database Revisions : <input type='checkbox' name='update[]' value='database' /> Update <br/>";
		echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>$content</textarea></li>"; 
		$updates++;
	}
	flush();


	echo "</ol></p>";
	if ($updates > 0) {
		echo "<p><input type=submit name=submit value='Submit' class=button></p>";
	}
	echo "</form>";
	// check if annotation config changed
	$newmd5 = md5_file("$scriptdir/Annotations/Config.xml");
	if (file_exists("$scriptdir/Annotations/Config.md5")){
		$oldmd5 = file_get_contents("$scriptdir/Annotations/Config.md5");
	}
	else {
		$oldmd5 = '';
	}
	if ("$oldmd5" != "$newmd5") {
		$note = "<div style='border:1px solid red;'><p><span class=strong style='color:red'>NOTE:</span> The configuration file of the available annotations was updated. It is <span class=strong>highly</span> recommended to run the updater for the annotation databases. This will download any additional databases that are missing on the installation. ";
		if ($dodb == 0) {
			$note  .= "<span class=strong>You should first update the database to the latest version before starting the annotation update! </span>";
		}
		$note .= "</p><p style='padding-left:2em'><a href='index.php?page=admin_pages&type=update&annotate=1'> ==> START UPDATE <== </p></div>";
	}
	else {
		$note = '';
	}
	echo "$note";
	echo "</div>";
}
?>
<script type="text/javascript">document.getElementById('loading').style.display='none'</script>
