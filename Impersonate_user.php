<?php
///////////////////
// START SESSION //
///////////////////
ini_set("session.cookie_lifetime", 3600 * 24); // session life set to 1 day
session_start(); //Allows you to use sessions

//#######################
//# CONNECT to database #
//#######################
$ok = include('.LoadCredentials.php');
if ($ok != 1) {
    include('inc_installation_problem.inc');
    exit();
}
include('includes/inc_logging.inc');
// SET DATABASE HERE. NOW LIFTOVER PROOF !!
$_SESSION['db'] = 'NGS-Variants' . $_SESSION['dbname'];
require("includes/inc_query_functions.inc");
// check if there is a user logged in of level >= 3
if ($_SESSION['level'] < 3) {
    echo "You are not allowed to Impersonate other users!";
    exit();
}

// user to impersonate
$newuid = $_POST['ImpersonateID'];
if ($newuid == '') {
    echo "No userid specified.";
    exit();
}

// get user details
$query = "SELECT level, FirstName, LastName, email FROM `Users` WHERE id = '$newuid'";
$row = array_shift(...[runQuery($query)]);
if (empty($row)) {
    echo "User ID does not match an existing user.<br/>";
    exit();
} else {
    $_SESSION['logged_in'] = "true";
    $_SESSION['email'] = stripslashes($row['email']); //Saves your username (email in this case).
    $_SESSION['level'] = $row['level'];
    $_SESSION['FirstName'] = $row['FirstName'];
    $_SESSION['LastName'] = $row['LastName'];
    $_SESSION['userID'] = $newuid;
    // redirect to page_recent.
    echo "<meta http-equiv='refresh' content='0;URL=index.php?page=recent'>\n";
}
