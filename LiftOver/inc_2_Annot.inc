<?php
#################################################
#  LIFTOVER STEP 2 : Fill Annotation Tables	#
#	- PROVIDE NEW TABLE SOURCES (Anno_datavalues)		#
#	- TEST VALIDITY for downloading		#
#	- WHEN PROVIDED : download tables	#
#						#
#	=> ACCESS TO CURRENT BUILD READONLY	#
#################################################
echo "<div class=section>\n";
echo "<h3 id=maintitle>LiftOver Step 2: Define New Annotation Tables/Sources</h3>\n";
ob_end_flush();

# First provide new table sources 
if (!isset($_POST['newsources']) && isset($_POST['staticsfilled'])) {
    $shortname = $_POST['shortname'];
    echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
    # pass on static tables
    $tables = $_POST['statictables'];
    foreach ($tables as $key => $tablename) {
        echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
    }
    # pass on  build name
    echo "<input type=hidden name=shortname value='$shortname'>\n";
    # get urls to update
    $currentdb = "NGS-Variants" . $_SESSION['dbname'];
    $newdb = "NGS-Variants" . $shortname;
    $mysqli = GetConnection();
    $mysql->select_db($currentdb);
    $rows = runQuery("SELECT Annotation, Value, Information FROM `Annotation_DataValues` ORDER BY Annotation", "Annotation_DataValues");
    $currentscript = '';
    echo "<p>Annotations on the platform are regularly updated automatically. To achieve this, the data are fetched from public servers. Please provide the keys as used by the tools to download these tables for the new genomic build.</p>";
    echo "<p><span class=nadruk>Note:</span> It is the responsibility of the system administrator to lift the platform only if all available datasources are available in the new genome build. It is therefore HIGHLY recommended to test the download of new genome build tables on a seperate system before applying the update here.</p>\n";
    echo "<p><table cellspacing=0 class=w100>\n";
    echo "<tr><th class=top>Annotation Tool</th><th class=top>Current Key</th><th class=top>New Key</th><th class=top>Information</th></tr>";
    foreach ($rows as $row) {
        $anno = $row['Annotation'];
        $value = $row['Value'];
        $info = $row['Information'];
        echo "<tr>\n";
        echo "<td >$anno</td>\n";
        echo "<td>$value</td>\n";
        echo "<td><input type=text name='newkey_$anno' size=30></td>\n";
        echo "<td>$info</td>";
        echo "</tr>\n";
    }
    echo "</table></p>\n";
    echo "<p><input type=submit class=button name=newsources value=Proceed>\n";
    echo "</form>\n";
    echo "</p>\n";
} elseif (isset($_POST['newsources']) && !isset($_POST['filltables'])) {
    # get posted variables
    $shortname = $_POST['shortname'];
    $tables = $_POST['statictables'];
    # set database names	
    $currentdb = "NGS-Variants" . $_SESSION['dbname'];
    $newdb = "NGS-Variants" . $shortname;
    $mysqli = GetConnection();
    $mysqli->select_db($currentdb);
    # process entries
    echo "<p><span class=strong>Checking keys:</span><ul id=ul-simple style='padding-left:15px'>\n";
    $rows = runQuery("SELECT Annotation, Value, Information FROM `Annotation_DataValues` ORDER BY Annotation", "Annotation_DataValues");
    $formstring = '';
    $numrows = count($rows);
    $currrow = 0;
    $failed = 0;
    foreach ($rows as $row) {
        $currow++;
        $anno = $row['Annotation'];
        $annorep = str_replace('.', '_', $anno);
        $key = $_POST["newkey_$annorep"];
        echo "<li>$currow/$numrows: $anno: ";
        flush();
        $ll = `cd $sitedir/LiftOver && perl Scripts/TestDownload.pl -D $key -a $anno`;
        if (substr($ll, 0, 2) != 'OK') {
            echo " $ll</li>\n";
            $failed++;
        } else {
            echo " $ll</li>\n";
        }
        flush();
    }

    if ($failed > 0) {
        # print form to revert the current liftover.
        echo "<p>Some keys were found to be invalid or missing. You <span class=nadruk>CAN NOT</span> proceed the lift while annotations are not available. To restore full access on the current genome build, press 'Restore' below. If you want to correct the keys, press the back button of your browser.</p>\n";
        echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
        echo "<input type=hidden name='shortname' value='$shortname' />";
        echo "<input type=submit class=button name='RevertLift' Value='Revert Liftover Process' /></form>";
        echo "</form>\n";
        echo "</p>";
    } else {
        // print form to continue to next stage: filling annotation tables
        echo "<p>All keys were successfully validated. You can now proceed to downloading the annotation tables.</p>\n";
        echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
        // pass on static tables
        foreach ($tables as $key => $tablename) {
            echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
        }
        // insert values in database.
        $rows = runQuery("SELECT Annotation, Information FROM `Annotation_DataValues` ORDER BY Annotation", "Annotation_DataValues");
        foreach ($rows as $row) {
            $anno = $row['Annotation'];
            $info = $row['Information'];
            $annorep = str_replace('.', '_', $anno);
            $key = $_POST["newkey_$annorep"];
            doQuery("INSERT INTO `NGS-Variants$shortname`.`Annotation_DataValues` (Annotation, Value, Information) VALUES ('$anno','$key','$info')");
        }

        // UPDATE STAGE IN LiftsInProgress TABLE
        doQuery("UPDATE `NGS-Variants-Admin`.`LiftsInProgress` set step='2b' WHERE frombuild = '" . $_SESSION['dbname'] . "' AND tobuild = '$shortname'");
        // pass on password & build name
        echo "<input type=hidden name=shortname value='$shortname'></p>\n";
        echo "<p><input type=submit class=button name=filltables value='Continue'></p>";
        echo "</form>\n";
        echo "</p>";
    }
}
// REVERT liftover.
if (isset($_POST['RevertLift'])) {
    // restore permissions
    $currdb = substr($_SESSION['dbname'], 1);
    $newdb = substr($_POST['shortname'], 1);
    // restore permissions
    doQuery("TRUNCATE TABLE `NGS-Variants-$currdb`.`Projects_x_Users`");
    doQuery("INSERT INTO  `NGS-Variants-$currdb`.`Projects_x_Users` SELECT * FROM `NGS-Variants-Admin`.`$currdb" . "To$newdb" . "UserPermissions`");
    doQuery("DROP TABLE `NGS-Variants-Admin`.`$currdb" . "To$newdb" . "UserPermissions`");
    // drop target database
    doQuery("DROP DATABASE `NGS-Variants-$newdb`");
    // remove lift in progress record
    doQuery("DELETE FROM `NGS-Variants-Admin`.`LiftsInProgress` WHERE frombuild = '-$currdb' AND tobuild = '-$newdb'");
    // remove new build info
    doQuery("DELETE FROM `NGS-Variants-Admin`.`NewBuild` WHERE name = '-$newdb'");
    // restore access.
    doQuery("UPDATE `NGS-Variants-Admin`.`SiteStatus` SET status = 'Operative' WHERE 1 = 1");
    echo "<script type='text/javascript'>document.getElementById('maintitle').style.display='none';</script>";
    echo "<h3>LiftOver Cancelled</h3>";
    echo "<p>The LiftOver process was reverted. Access to VariantDB is restored for the current ($currdb) build.</p>";
}
// DOWNLOAD THE ANNOTATION TABLES.
elseif (isset($_POST['filltables'])) {
    # get posted variables
    $shortname = $_POST['shortname'];
    $tables = $_POST['statictables'];
    # set database names	
    $targetdb = $shortname;
    $sourcedb = $_SESSION['dbname'];
    $currentdb = "NGS-Variants" . $_SESSION['dbname'];
    $newdb = "NGS-Variants" . $shortname;
    $mysqli = GetConnection();
    $mysqli->select_db($currentdb);

    echo "<p><span id='runningspan'><span class=strong>Running downloads:</span> ";
    echo "All the new annotation tables are now being downloaded (several Gigabytes, so be patient). Once finished, there should appear a button here to continue. </span></p>";
    echo "<p>Please inspect the output of the process below before continuing for errors, and retry downloads manually if needed.</p>";
    echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=3' method=POST>\n";
    # pass on static tables
    foreach ($tables as $key => $tablename) {
        echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
    }
    # pass on password & build name
    echo "<input type=hidden name=shortname value='$shortname'></p>\n";
    echo "<p><span id=continuebutton style='display:none;'><input type=submit class=button name=GoCurateTables value=Proceed></span></p>\n";
    echo "</form>";
    echo "<script type='text/javascript'>interval1 = setInterval(\"reloadspan('UpdateDB')\",10000)</script>\n";
    echo "<p><span class=strong>Output:</span><pre id='UpdateDB' class=scrollbarbox style='height:400px'>Starting downloads...</pre><hr>\n";
    flush();
    //$command = " ( COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/Annotations && perl UpdateDBs.pl -u '$adminemail' -b '$shortname' > $scriptdir/LiftOver/Files/UpdateDB.out\"  ) ";
    $output  = system("(echo 1 > LiftOver/Files/UpdateDB.status && sleep 5 && COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/Annotations && perl UpdateDBs.pl -u '$adminemail' -b '$shortname' > $scriptdir/LiftOver/Files/UpdateDB.out 2>&1\" && echo 0 > LiftOver/Files/UpdateDB.status &) > /dev/null 2>&1");
    flush();
    echo "<script type='text/javascript'>interval2 = setInterval(\"checkdownloadstatus(2)\",10000)</script>\n";
    # print complete hidden form
    # proceed button is shown above the scroll boxes when status becomes 0.
}
echo "</div>";
