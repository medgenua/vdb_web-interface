#!/usr/bin/perl
use Getopt::Std;
use DBI;
# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);

$|++;
# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";
$chromhash{ "X" } = "23";
$chromhash{ "XY" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "MT" } = "25";
##########################
# COMMAND LINE ARGUMENTS #
##########################
# d = Source Database, mandatory.
# D = Target Database, mandatory.
# G = Max Gap (default 10000) in basepairs to merge fragments
# F = Min Fraction (default 0.9) to retain fragment if partially deleted 
getopts('D:d:G:F:', \%opts);  # option are in %opts

########################### 
# CHECK MANDATORY OPTIONS #
###########################
if (!$opts{'D'} || $opts{'D'} eq '') {
	die('Target Database is mandatory');
}
if (!$opts{'d'} || $opts{'d'} eq '') {
	die('Source Database is mandatory');
}
$sourcedb = "NGS-Variants" . $opts{'d'};
$targetdb = "NGS-Variants" . $opts{'D'};

############################## 
# CONNECT TO SOURCE DATABASE #
##############################
print "1/ Connecting to the Source database ($sourcedb)\n";
$connectionInfocnv="dbi:mysql:$sourcedb:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) or die('Could not connect to database') ;
print "\t=> Done\n";

########################
# CHECK FOR CHAIN FILE #
########################
my $tempsource = $opts{'d'};
$tempsource =~ s/^-//;
my $temptarget = $opts{'D'};
$temptarget =~ s/^-//;
$temptarget =~ s/^hg/Hg/;
our $chainfile = "$scriptdir/LiftOver/ChainFiles/$tempsource"."To$temptarget".".over.chain";
if (!-e $chainfile || !-s $chainfile) {
	my $url ="http://hgdownload.cse.ucsc.edu/goldenPath/$tempsource/liftOver/$tempsource"."To$temptarget".".over.chain.gz";
	system("wget -q -O '$chainfile.gz' '$url' && gunzip '$chainfile.gz' --force");
	if (-z "$chainfile" || -e "$chainfile.gz") {
		die('Chain File Could not be fetched from the internet');
	}
	print "OK\n";
	
}	

# set values
if ($opts{'G'}) {
	$Gap = $opts{'G'};
}
else {
	$Gap = 10000;
}
if ($opts{'F'}) {
	$retain = $opts{'F'};
}
else {
	$retain = 0.9;
}

$table = 'Variants';
#############################################
# GET COLUMN HEADERS, EXTRACT POSITION COLS #
#############################################
print "2/ Determining the position and identifier columns for table '$table'\n";
my $sth = $dbh->prepare("SELECT * FROM `$table` WHERE 1=0");
$sth->execute;
my @fields = @{$sth->{NAME}};
$sth->finish;
my $chrcol = '';
my $startcol = '';
my $stopcol = '';
my $idcol = '';
my $liftable = 0;
foreach (@fields) {
	$colname = $_;
	if ($colname =~ m/^chr/i) {
		$chrcol = $colname;
	}
	if ($colname =~ m/^(start|position)$/i) {
		$startcol = $colname;
	}
	if ($colname =~ m/^(stop|end|position)$/i) {
		$stopcol = $colname;
	}
	if($colname =~ m/^id$/i) {
		$idcol = $colname;
	}
	if ($colname eq 'LiftedFrom') {
		$liftable = 1;
	}
}
if ($liftable == 0) {
	die("$table is considered Genome Build Independent (no LiftedFrom Column");
}
if ($chrcol eq '' || $startcol eq '' || $stopcol eq '' || $idcol eq '') {
	die('Could not determine all the necessary columns');
}
else {
	print "\t=> Done, using: $chrcol, $startcol, $stopcol and $idcol\n";
}

###########################
# DUMP DATA TO NEW TABLE  #
###########################
print "3/ Copy Data to new build database\n";
$dbh->do("TRUNCATE TABLE `$targetdb`.`$table`");
$query = "INSERT INTO `$targetdb`.`$table` SELECT * FROM `$sourcedb`.`$table`";
$dbh->do($query);
print "\t=> Done\n";
##################
# LIFT THE DATA! #
##################
my $dumpfile = "$scriptdir/LiftOver/Files/LiftOver_$table.region.chrIN";
my $chrdump = "$scriptdir/LiftOver/Files/LiftOver_$table.region.chrIN";
my $chrdumpOK = "$scriptdir/LiftOver/Files/LiftOver_$table.region.chrOK";
my $chrdumpBAD = "$scriptdir/LiftOver/Files/LiftOver_$table.region.chrBAD";

my $query = "SELECT $chrcol, $startcol, $stopcol, $idcol, LiftedFrom FROM `$targetdb`.`$table` WHERE $chrcol = ?";
my $dumpquery = "SELECT CONCAT('chr',$chrcol), $startcol, $stopcol, $idcol FROM `$targetdb`.`$table`  WHERE $chrcol = ? AND LiftedFrom = '' ";
#$dbh->do("ALTER TABLE `$targetdb`.`$table` DISABLE KEYS");
$absth = $dbh->prepare($query);
$dumpsth = $dbh->prepare($dumpquery);
print "4/ Lifting the coordinate information\n";
my %faileditems;
my %masslift;
my $total;
for ($i=1;$i<=25;$i++) {
	print "\nChr ".$chromhash{$i}." : ";
	# first perform mass lift for speed
	if (-e "$dumpfile") {
		unlink($dumpfile);
	}
	# suboptimal since select into local outfile is not supported. 
   	$dumpsth->execute($i);
	my $cache = $dumpsth->fetchall_arrayref();
	open OUT, ">$dumpfile";
	foreach(@$cache) {
		## bed is zero based half open.
		$_->[1]--;
		print OUT join("\t",@$_)."\n";
	}
	close OUT;
	if ($i >= 23 ) {
		system("sed -e 's/chr23/chrX/g' -e 's/chr24/chrY/g' -e 's/chr25/chrM/g' '$chrdump' > '$chrdump.tmp.txt' && mv '$chrdump.tmp.txt' '$chrdump'");
	}
	my $rows = 0;
	$rows = $absth->execute($i);
	$total = $total + $rows;
   	system("$scriptdir/LiftOver/bin/liftOver -minMatch=0.9 '$chrdump' '$chainfile' '$chrdumpOK' '$chrdumpBAD' > /dev/null 2>&1");
	%masslift = ();
	open IN, "$chrdumpOK";
	while (<IN>) {
		chomp($_);
		my @pieces = split(/\t/,$_);
		$pieces[0] =~ s/chr//;
		# key in masslift is identifier 'ID' from the 'idcol' in the table 
		$masslift{$pieces[3]} = $pieces[0]."\t".($pieces[1] + 1)."\t".$pieces[2];
	}
	close IN;
	print keys(%masslift) ." exact lifts out of $rows items, updating tables & processing remaining items: ";
	# then fetch data row by row and update
	my $rowcache;
   	if ($rows < 10000) {
		$max = $rows;
   	}
   	else {
   	     $max = 10000;
   	}
   	my $item = 0;
   	my $perc = 10;
   	my $tresh = int(($rows / 100)*$perc);
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $absth->fetchall_arrayref(undef,$max)|| []})) {
		$item++;
		if ($item > $tresh) {
			print " $perc%";
			$perc = $perc + 10;
			$tresh = int(($rows / 100) * $perc);
		}
	
		my $chr = $result->[0];
		my $chrtxt = $chromhash{$chr};
		my $start = $result->[1];
		my $stop = $result->[2];
		my $size = $stop - $start + 1;
		my $id = $result->[3];
		my $lifted = $result->[4];
		my $name = '';
		if ($lifted eq '') {
			# original info, try lifting, this subroutine takes masslift results into account !
			my ($status,$text,$newchrtxt,$newstart,$newstop) = &Lift($chrtxt,$start,$stop,$id);
			if ($status == 1) {
				# lift success
				# check size!
				my $newchr = $chromhash{$newchrtxt};
				my $newsize = $newstop - $newstart + 1;
				if ($newsize < $retain*$size) {
					#print "\tFailure: chr$chrtxt:$start-$stop : New mapping (chr$newchr:$newstart-$newstop) is too small ($newsize < $retain*$size)!\n";
					$faileditems{$id} = "LiftOver Failed: Mapped region is smaller than minimal fraction ($retain) of original size.";  
				}
				else {
					$dbh->do("UPDATE LOW_PRIORITY `$targetdb`.`$table` SET $chrcol = '$newchr', $startcol = '$newstart', $stopcol = '$newstop', LiftedFrom ='". $opts{'d'} ."' WHERE $idcol = '$id'");
				}
			}
			else {
				#print "\tFailure: chr$chrtxt:$start-$stop : $status : $text\n";
				$faileditems{$id} = $text;
			}
		}
		else {
			# need to fetch original data and perform lift on those !
			##############################
			## THIS NEEDS TO BE CREATED ##
			##############################
		}	
   	}
	print " Done.";
}
my $sr = ($total - keys(%faileditems)) / $total;
$sr = sprintf("%.2f", ($sr*100));
print "\n\t=> Finished with a successrate of $sr%\n";
print "\t=> Storing " . keys(%faileditems) . " failed items for curation\n";
############################
# PROCESS THE FAILED ITEMS #
############################
# delete from target table, store in failedLift table for manual curation
for my $key (keys(%faileditems)) {
	#$dbh->do("DELETE FROM `$targetdb`.`$table` WHERE id = '$key'");
	my $arguments = $faileditems{$key};
	$dbh->do("INSERT INTO `NGS-Variants-Admin`.`FailedLifts` (frombuild, tobuild, tablename, itemID, Reason) VALUES ('".$opts{'d'}."','".$opts{'D'}."','$table','$key','$arguments')");
}
my $vids = join(",",keys(%faileditems));
print "\t=>Deleting variants (Variants and Variants_x_Samples) in target build. Variants can be recovered after curation from the old build.\n";
$dbh->do("DELETE FROM `$targetdb`.`$table` WHERE id IN ($vids)");
$dbh->do("DELETE FROM `$targetdb`.`Variants_x_Samples` WHERE vid IN ($vids)");
print "\n\n => DONE!\n";

## update status of lift
$dbh->do("UPDATE `NGS-Variants-Admin`.`LiftsInProgress` SET step = '3b' WHERE frombuild = '".$opts{'d'}."' AND tobuild = '".$opts{'D'}."'");


###############
# SUBROUTINES #
###############

sub Lift {
	# returns array of STATUS(0=failed,1=OK),REASON(if failed) or Chr,Start,Stop(if OK)
   	my($chr, $start, $stop,$id) = @_;
	#print "  Chr$chr:$start-$stop ($id) : ";
	if ($masslift{$id}) {
		my @parts = split(/\t/,$masslift{$id});
		#print "Success.\n";
		return (1,'ok',$parts[0],$parts[1],$parts[2]);
	}
	else {
		#print "Not in masslift : ";
		my $infile = "$scriptdir/LiftOver/Files/LiftOver_$table.region.in";
		my $outOK = "$scriptdir/LiftOver/Files/LiftOver_$table.region.outOK";
		my $outBAD = "$scriptdir/LiftOver/Files/LiftOver_$table.region.outBAD";
		my $outMerged = "$scriptdir/LiftOver/Files/LiftOver_$table.region.outMerged";
		open OUT, ">$infile";
		print OUT "chr$chr\t".($start-1)."\t$stop\t$id";
		close OUT;
		system("$scriptdir/LiftOver/bin/liftOver -minMatch=0.9 -multiple '$infile' '$chainfile' '$outOK' '$outBAD' > /dev/null 2>&1");
		if (-z $outOK) {
			# lift failed
			my $reason = `head -n 1 $outBAD`;
			chomp($reason);
			$reason =~ s/#//;
			$reason = "LiftOver Failed: $reason";
			#print "chr$chr:$start-$stop : $reason\n";
			#print "Failure 1.\n";
			return (0,$reason,'','','');
		}
		else {
			# lift ok
			system("$scriptdir/LiftOver/bin/liftOverMerge -mergeGap=$Gap '$outOK' '$outMerged' && mv '$outMerged' '$outOK' > /dev/null 2>&1");
			open IN, "$outOK";
			my @lines = <IN>;
			if (scalar(@lines) > 1) {
				# region is split => Failed
				#print "Failure 2\n";
				return(0,'LiftOver Failed: Region is split in new build','','','');
			}
			else {
				my $line = $lines[0];
				chomp($line);
				my @parts = split(/\t/,$line);
				$parts[0] =~ s/chr//;
				#print "Success 2\n";
				return(1,'ok',$parts[0],($parts[1]+1),$parts[2]);
			}		
		}
	}	
}









