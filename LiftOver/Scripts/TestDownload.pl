#!/usr/bin/perl

use Getopt::Std;
use DBI;
# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../.LoadCredentials.pl");
require($credpath);

$|++;
# DEFINE VARIABLES
##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Target Database, mandatory.
getopts('D:a:', \%opts);  # option are in %opts

if (!$opts{'D'} || $opts{'D'} eq '') {
	print "FAILED: Build is mandatory";
	exit;
}
if (!$opts{'a'} || $opts{'a'} eq '') {
	print "FAILED: Annotation Type is mandatory";
	exit;
}

#$targetdb = "NGS-Variants".$opt{'D'};

#print "1/ Connecting to the Target database ($targetdb)\n";
#$connectionInfocnv="dbi:mysql:$targetdb:$host"; 
#$dbh = DBI->connect($connectionInfocnv,$userid,$userpass);

#if (!defined($dbh)) {
#	print "FAIL: Could not connect to database\n";
#	exit;
#}
#print "\t=> Done\n";

$build = $opts{'D'};

## CHECK ANNOVAR
if ($opts{'a'} eq 'ANNOVAR') {
	my $command = "cd $scriptdir/Annotations/ANNOVAR/ && perl annotate_variation.pl -buildver $build -downdb -webfrom annovar esp5400_aa /tmp/";
	my $result = `$command`;
	system("rm /tmp/$build"."_*");
	system("rm /tmp/annovar_downdb.log");
	if ($result =~ m/cannot be downloaded/) {
		print "FAIL: Annovar (webfrom annovar) download failed for build $build\n";
		exit;
	}
	else {
		print "OK\n";
		exit;
	}
}
elsif ($opts{'a'} eq 'snpEff') {
	my $command = "cd $scriptdir/Annotations/snpEff/ && ./sun-jre-7/bin/java -jar snpEff.jar databases | grep $build";
	my $result = `$command`;
	if ($result !~ m/$build/) {
		print "FAIL: $build is not available for the currently installed snpEff\n";
		exit;
	}
	else {
		print "OK\n";
		exit;
	}


}
elsif($opts{'a'} eq 'IGV') {
	if (!-e "$scriptdir/IGVTools_bin/genomes/$igvbuild.chrom.sizes") {
		## available at ucsc?
		$url = "http://genome.ucsc.edu/goldenPath/help/$build.chrom.sizes";
		my $output = `wget -O /tmp/chromsizes '$url' 2>&1`;
		if ($output =~ m/404 Not Found/) {
			print "FAIL: $build.chrom.sizes not available on your system, nor on the UCSC goldenPath pages.\n";
			exit;
		}
		else {
			my $out = `wget -O $scriptdir/IGVTools_bin/genomes/$igvbuild.chrom.sizes '$url'`;
			print "OK";
			exit;
		}
	}
	else {
		print "OK";
		exit;
	}
}
