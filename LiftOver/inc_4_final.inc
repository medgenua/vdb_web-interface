<?php
#################################################
#  LIFTOVER STEP 4 : Lift Critical TABLES	#
#	- List items to do manually		# 
#	- send out emails to users		#
#	- restore project permissions		#
#	- set new build to active.		#
#################################################
echo "<div class=section>\n";
echo "<h3>LiftOver Step 4: Final Configurations</h3>\n";
ob_end_flush();


## chromosome hash
for ($i = 1; $i <= 22; $i += 1) {
    $chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["25"] = "M";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["M"] = "25";


if (isset($_POST['Finish'])) {
    // db to use:
    $currentdb = "NGS-Variants" . $_SESSION['dbname'];
    $oldshortname = $_SESSION['dbname'];
    $shortname = $_POST['shortname'];
    $newdb = "NGS-Variants" . $shortname;
    // 0. PREPARE
    // details
    $row = runQuery("SELECT StringName, Description FROM `NGS-Variants-Admin`.NewBuild WHERE name = '$shortname'")[0];
    $longname = $row['StringName'];
    $descr = $row['Description'];

    // send out emails
    echo "<p>Sending out emails for manual curation of failed lifts:<ul>";
    echo "<li>Building email contents: ";
    flush();
    $subject = "VariantDB Genome Build Update Results";
    $rows = runQuery("SELECT v.id,v.chr, v.start, v.stop, v.RefAllele, v.AltAllele, l.entry, l.arguments, s.Name AS SName, p.Name AS PName, p.userID FROM `$newdb`.`Log` l JOIN `$currentdb`.`Samples` s JOIN `$currentdb`.`Variants` v JOIN `$currentdb`.`Projects_x_Samples` pxs JOIN `$currentdb`.`Projects` p ON l.sid = s.id AND v.id = l.vid AND pxs.pid = p.id AND pxs.sid = s.id WHERE l.arguments LIKE 'Lift$oldshortname$shortname%' ORDER BY p.userID, v.chr, v.start");

    $luid = '';
    $fh = '';
    $lvar = '';
    $uids = array();
    foreach ($rows as $row) {
        $uid = $row['userID'];
        // next user.
        if ($uid != $luid) {
            $uids[] = $uid;
            if ($luid != '') {
                fclose($fh);
            }
            $fh = fopen("/tmp/$uid.mail.txt", "w");
            $luid = $uid;
            $lvar = '';
        }
        // next variant
        if ($lvar != $row['id']) {
            // write out variant
            fwrite($fh, "\r\nchr" . $chromhash[$row['chr']] . ":" . $row['start'] . "-" . $row['stop'] . " : " . $row['RefAllele'] . "/" . $row['AltAllele'] . "\r\n");
            // write out reason & what happened
            $arg = "Action Taken: Variant was " . strtolower(str_replace("Lift$oldshortname$shortname Variant", "", $row['arguments']));
            $row['entry'] = str_replace(" Extra Comments", "\r\n - Extra Comments", $row['entry']);
            fwrite($fh, " - $arg \r\n - Reason : " . $row['entry'] . "\r\n");
            fwrite($fh, " - The following samples contained this variant\r\n");
            $lvar = $row['id'];
        }
        // print affected samples.
        fwrite($fh, "   - " . $row['PName'] . " : " . $row['SName'] . "\r\n");
    }
    if ($fh != '') {
        fclose($fh);
    }
    echo " done</li>";
    foreach ($uids as $uid) {
        $row = runQuery("SELECT LastName, FirstName, email FROM `$currentdb`.`Users` WHERE id = '$uid'")[0];
        $email = $row['email'];
        $name = $row['FirstName'] . " " . $row['LastName'];
        $message = "Dear $fname,\r\n\r\nVariantDB has been converted to the new reference genomebuild ($longname). In this process, as many data as possible have been converted automatically. However, several variants could not be transferred and were deleted or manually remapped. Below, these regions are listed, together with the reason why the conversion failed, any additional notes and the samples that contained these variants. Original data are kept in read-only mode and available through VariantDB (top right, next to the username). If you manually corrected some of the items below, please provide the new position to the system administrator.\r\n\r\nThis list is only sent to the user that originally inserted the listed samples. If you shared these samples with collaborators, please forward the relevant sections.\r\n\r\nBest Regards,\r\nThe VariantDB System Administrator\r\r\n\r\n";
        $message .= file_get_contents("/tmp/$uid.mail.txt");

        $headers = "From: $adminemail\r\n";
        $headers .= "To: $email\r\n";
        $headers .= "CC: $adminemail\r\n";
        // Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. 
        #if (mail($adminemail,"$subject",$message,$headers)) {
        if (mail($email, "$subject", $message, $headers)) {
            echo "<li>$name => Email successfully sent</li>";
        } else {
            echo "<li>$name => Sending Failed</li>";
        }
    }
    echo "</ul></p>";

    // Clear full memcache.
    if ($usemc == 1) {
        echo "<p>Clearing cached data</p>";
        $rows = runQuery("SELECT mckey FROM `NGS-Variants-Admin`.`Memcache_Keys`");
        foreach ($rows as $row) {
            $r = $memcache->delete($row['mckey']);
            $t = 0;
            while ($t < 100 && !$r) {
                $r = $memcache->delete($row['mckey']);
                $t++;
            }
        }
        doQuery("TRUNCATE TABLE `NGS-Variants-Admin`.`Memcache_Keys`");
    }

    echo "<p>Activating the new build</p>";
    // 1. Move Current Build to previous builds
    doQuery("INSERT IGNORE INTO `NGS-Variants-Admin`.`PreviousBuilds` SELECT * FROM `NGS-Variants-Admin`.`CurrentBuild`");

    // 2. Set New Build to Current 
    doQuery("TRUNCATE TABLE `NGS-Variants-Admin`.`CurrentBuild`");
    doQuery("INSERT INTO `NGS-Variants-Admin`.`CurrentBuild` VALUES ('$shortname', '$longname', '$descr')");

    // 3. Restore Permissions
    $sourcebuild = substr($oldshortname, 1);
    $targetbuild = substr($shortname, 1);
    doQuery("TRUNCATE TABLE `$newdb`.`Projects_x_Users`");
    doQuery("INSERT INTO `$newdb`.`Projects_x_Users` SELECT * FROM `NGS-Variants-Admin`.`$sourcebuild" . "To$targetbuild" . "UserPermissions`");

    // 4. Remove new build from 'in progress'
    doQuery("DELETE FROM `NGS-Variants-Admin`.`LiftsInProgress` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname'");

    // 5. Restore Platform status
    doQuery("UPDATE `NGS-Variants-Admin`.`SiteStatus` SET status = 'Operative' WHERE 1=1");


    if ($config['FTP'] == 1) {
        echo "<p><span class=strong>IMPORTANT: </span> Make sure you adapt the configuration of the FTP-MySQL connection to reflect the new database name!</p>";
    }
    // lauch annotations.
    $output  = system("(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/Annotations && echo 0 > update.txt && perl Annotate.pl >> $scriptdir/Annotations/Annotate.log 2>&1\" &) > /dev/null 2>&1");

    // Print Redirection.
    echo "<p>Congratulations, VariantDB has now been lifted to $longname.</p>";
    echo "<p>To reload VariantDB in the new genomebuild, you need to log out and log in again. </p>";
}
