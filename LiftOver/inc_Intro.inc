<div class=section>
<h3>Perform GenomeBuild LiftOver</h3>

<p>When the default genomic build for the majority of applications and databases has changed, it might become appropriate to lift over the entire platform to this new build. This process is typically complex and takes a lot of time and effort. To ease it at least a little bit, the major steps are automated and performed from here. Take note however that you <span class=nadruk>will need access to the UNIX commandline</span> to finalise the process.</p>

<p>Beware however that this process will make all current results READ-ONLY, and reactivating the current build is not supported. All data created so far is kept, so it will always be possible to retry the lift if something went wrong, and to browse the data in read-only mode in the old genomic build.</p>

<p>My recommendation is: as long as you can still get all data you need in the current build, and all external databases are available for this build, DON'T change. 
<form action='index.php?page=admin_pages&amp;type=lift&amp;step=1' method=POST>
<p>If you really want to do this now, press Start :<input type=submit class=button value=Start></form></p>
</div> 
