<?php
#################################################
#  LIFTOVER STEP 4 : Lift Critical TABLES	#
#	- Lifting and curatation		#
#	- Will cost you some time !		#
#						#
#	=> ACCESS TO CURRENT BUILD REMAINS OPEN	#
#################################################
echo "<div class=section>\n";
echo "<h3>LiftOver Step 4: Lifting Variants Table</h3>\n";
ob_end_flush();


################################
## LIFT Variant REGION TABLES ##
################################
if (isset($_POST['GoCurateTables'])) { // don't mind the name, it's a relic from CNV-WebStore
    echo "<p>We will now be lifting Variant position information. You will be presented with failed regions for manual curation. </p>";

    // db to use:
    $currentdb = "NGS-Variants" . $_SESSION['dbname'];
    $oldshortname = $_SESSION['dbname'];
    $shortname = $_POST['shortname'];
    $newdb = "NGS-Variants" . $shortname;
    echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=3' method=POST>\n";
    echo "<p><span id=continuebutton style='display:none;'>Variant location info was successfully lifted. You may now proceed to curation of the failed items. <br/><br/><input type=submit class=button name='CurateVariants' value=Proceed></span></p>\n";
    // pass on static tables
    $tables = $_POST['statictables'];
    foreach ($tables as $key => $tablename) {
        echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
    }
    // pass on  build name
    echo "<input type=hidden name=shortname value='$shortname'>\n";
    // UPDATE STATUS
    // start the update script 	
    $output  = system("(echo 1 > LiftOver/Files/UpdateDB.status && sleep 5 && COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/LiftOver/Scripts && perl LiftVariants.pl -d '$oldshortname' -D '$shortname' > $scriptdir/LiftOver/Files/UpdateDB.out 2>&1\" && echo 0 > LiftOver/Files/UpdateDB.status &) > /dev/null 2>&1");

    flush();
    echo "<script type='text/javascript'>interval1 = setInterval(\"reloadspan('UpdateDB')\",10000)</script>\n";
    echo "<p><span class=strong>Output:</span><pre id='UpdateDB' class=scrollbarbox style='height:400px'>Starting lifting process...</pre><hr>\n";
    flush();


    echo "<script type='text/javascript'>interval2 = setInterval(\"checkdownloadstatus('2')\",10000)</script>\n";
    echo " </form>";
    echo "</div>";
} elseif (isset($_POST['CurateVariants'])) {
    $oldshortname = $_SESSION['dbname'];
    $shortname = $_POST['shortname'];
    $currentdb = "NGS-Variants$oldshortname";
    $targetdb = "NGS-Variants$shortname";
    ## chromosome hash
    $chromhash = array();
    for ($i = 1; $i <= 22; $i += 1) {
        $chromhash["$i"] = "$i";
    }
    $chromhash["23"] = "X";
    $chromhash["24"] = "Y";
    $chromhash["25"] = "M";
    $chromhash["X"] = 23;
    $chromhash["Y"] = 24;
    $chromhash["M"] = 25;

    // FIRST : WERE THERE UPDATES TO SAVE?
    if ($_POST['CurateVariants'] == 'Save Curations') {
        // delete? 
        foreach ($_POST['del'] as $vid => $val) {
            // check if region is also defind
            if ($_POST['newpos'][$vid] != '') {
                echo "<span class=strong>Error: </span>You cannot specify both 'Delete' and new position (" . $_POST['newpos'][$vid] . ")<br/>";

                continue;
            }
            // comment set?
            $comment = $_POST['comment'][$vid];
            if ($comment != '') {
                $comment = "Extra Comments: $comment";
            }
            // reason
            $reason = $_POST['reason'][$vid];
            // select samples harbouring the variant.
            $rows = runQuery("SELECT sid FROM `$currentdb`.`Variants_x_Samples` WHERE vid = $vid");
            $uid = $_SESSION['userID'];
            foreach ($rows as $r) {
                doQuery("INSERT INTO `$targetdb`.`Log` (sid, vid, uid, entry, arguments) VALUES ('" . $r['sid'] . "','$vid','$uid','Automatic LiftOver Failed ($reason). $comment', 'Lift$oldshortname$shortname Variant Deleted')");
            }
            doQuery("DELETE FROM `NGS-Variants-Admin`.`FailedLifts` WHERE itemID = '$vid'");
        }
        // remap
        foreach ($_POST['newpos'] as $vid => $value) {
            if ($value == '') {
                continue;
            }
            if (array_key_exists($vid, $_POST['del'])) {
                // error printed above
                continue;
            }
            // parse new pos.
            $value = str_replace(',', '', $value);
            preg_match("/.*([0-9XYM]+):(\d+)-(\d+)/", $value, $matches);
            if (!array_key_exists($matches[1], $chromhash)) {
                echo "<span class=strong>Error: </span>Invalid Chromosome provided: $matches[1] <br/>";
                continue;
            }
            if (!is_numeric($matches[1])) {
                $matches[1] = $chromhash[$matches[1]];
            }
            if (!is_numeric($matches[2]) || !is_numeric($matches[3])) {
                echo "<span class=strong>Error: </span>Start and/or Stop position are not numeric. $matches[2] / $matches[3] <br/>";
                continue;
            }

            // reinsert variant with new location.
            doQuery("INSERT INTO `$targetdb`.`Variants` SELECT * FROM `$currentdb`.`Variants` WHERE id = '$vid'	");
            doQuery("UPDATE `$targetdb`.`Variants` SET chr = '" . $matches[1] . "', start = '" . $matches[2] . "', stop = '" . $matches[3] . "', LiftedFrom = '$oldshortname' WHERE id = '$vid'");
            // comment set?
            $comment = $_POST['comment'][$vid];
            if ($comment != '') {
                $comment = "Extra Comments: $comment";
            }
            // reason
            $reason = $_POST['reason'][$vid];
            // select samples harbouring the variant.
            $rows = runQuery("SELECT * FROM `$currentdb`.`Variants_x_Samples` WHERE vid = $vid");
            $uid = $_SESSION['userID'];
            foreach ($rows as $r) {
                doQuery("INSERT INTO `$targetdb`.`Log` (sid, vid, uid, entry, arguments) VALUES ('" . $r['sid'] . "','$vid','$uid','Automatic LiftOver Failed ($reason). $comment', 'Lift$oldshortname$shortname Variant Manually Remapped')");
                // reinsert variables 
                doQuery("INSERT INTO `$targetdb`.`Variants_x_Samples` SELECT * FROM `$currentdb`.`Variants_x_Samples` WHERE vid = '$vid'");
            }
            // delete from failed table
            doQuery("DELETE FROM `NGS-Variants-Admin`.`FailedLifts` WHERE itemID = '$vid'");
        }
    }
    $chr = 1;
    if (isset($_POST['chr'])) {
        $chr = $_POST['chr'];
    }

    $rows = runQuery("SELECT itemID, Reason FROM `NGS-Variants-Admin`.`FailedLifts` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname' AND tablename = 'Variants'");
    $nr = count($rows);
    echo "<p>$nr variants need manual lifting/curation. They are presented (one chromosome at a time) below for manual curation. Upon decision, users will be notified if they have samples containing these variants.</p>";
    // get variants.
    $reasons = array();
    $vids = array();
    foreach ($rows as $r) {
        $reasons[$r['itemID']] = $r['Reason'];
        $vids[] = $r['itemID'];
    }
    if (count($vids) == 0) {
        // update status
        doQuery("UPDATE `NGS-Variants-Admin`.`LiftsInProgress` SET step = 4 WHERE frombuild = '$oldshortname' AND tobuild = '$shortname'");
        // print continue button
        echo "<p>Manual Curation Finished, no variants left. </p>";
        echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=4' method=POST>\n";
        // pass on static tables
        $tables = $_POST['statictables'];
        foreach ($tables as $key => $tablename) {
            echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
        }
        // pass on  build name
        echo "<input type=hidden name=shortname value='$shortname'>\n";
        echo "<input type=submit class=button name='Finish' value='Continue'></form></div>";
        exit;
    }
    echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=3' method=POST>\n";
    // pass on static tables
    $tables = $_POST['statictables'];
    foreach ($tables as $key => $tablename) {
        echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
    }
    // pass on  build name
    echo "<input type=hidden name=shortname value='$shortname'>\n";
    echo "<p><span class=strong>Show variants for chromosome : </span><select name='chr'>";
    for ($i = 1; $i <= 25; $i++) {
        if ($i == $chr) {
            $s = 'SELECTED';
        } else {
            $s = '';
        }
        echo "<option value='$i' $s>" . $chromhash[$i] . "</option>";
    }
    echo "</select>	&nbsp; <input type=submit class=button name='CurateVariants' value='Select'>";
    echo "</p><p>";
    // get variants by chromosome
    $vars = array();
    $avids = array();
    $annos = array();
    for ($i = 0; $i <= count($reasons); $i += 500) {
        $els = 500;
        if (count($vids) < 500) {
            $els = count($vids);
        }
        $inlist = implode(',', array_slice($vids, $i, $els));
        $q = "SELECT id,start, stop, RefAllele, AltAllele, Stretch, StretchUnit, LiftedFrom FROM `$currentdb`.`Variants` WHERE id IN ($inlist) AND chr = '$chr'";
        $rows = runQuery($q);
        foreach ($rows as $r) {
            $vars[$r['start']] = $r;
            $avids[] = $r['id'];
            $annos[$r['id']] = '';
        }
    }
    if (count($vars) == 0) {
        echo "<p>No variants for chromosome " . $chromhash[$chr] . " left. Select the next chromosome above. </p>";
        echo "</form></div>";
        exit;
    }
    // get some annotations
    $avidlist = implode(",", $avids);
    $rows = runQuery("SELECT vid FROM `$currentdb`.`Variants_x_ANNOVAR_segdup` WHERE vid IN ($avidlist)");
    foreach ($rows as $r) {
        $annos[$r['vid']] .= "SegDup, ";
    }
    $codes = array();
    $rows = runQuery("SELECT id, Item_Value FROM `$currentdb`.`Value_Codes` WHERE `Table_x_Column` = 'Variants_x_ANNOVAR_refgene_GeneLocation'");
    foreach ($rows as $r) {
        $codes[$r['id']] = $r['Item_Value'];
    }
    $rows = runQuery("SELECT vid, GeneSymbol, GeneLocation FROM `$currentdb`.`Variants_x_ANNOVAR_refgene` WHERE vid IN ($avidlist)");
    foreach ($rows as $r) {
        if (!strstr($annos[$r['vid']], $r['GeneSymbol'] . "(" . $codes[$r['GeneLocation']] . ")")) {
            $annos[$r['vid']] .= $r['GeneSymbol'] . "(" . $codes[$r['GeneLocation']] . "), ";
        }
    }
    $rows = runQuery("SELECT vid, rsID FROM `$currentdb`.`Variants_x_ANNOVAR_snp137` WHERE vid IN ($avidlist)");
    foreach ($rows as $r) {
        if ($r['rsID'] != -1) {
            $annos[$r['vid']] .= $r['rsID'] . ", ";
        }
    }
    // print out table
    ksort($vars);
    echo "<table cellspacing=0 class=w100>";
    echo "<tr><th class=top>Delete</th><th class=top>Position$oldshortname</th><th class=top>Alleles</th><th class=top>Reason</th><th class=top>Position$shortname</th><th class=top>Comment</th><th class=top>Annotations</th></tr>";
    foreach ($vars as $pos => $r) {
        $anno = '';
        if ($r['Stretch'] == 1) {
            $anno .= "STR: " . $r['StretchUnit'] . ", ";
        }
        $anno .= $annos[$r['id']];
        $reason = str_replace("LiftOver Failed:", "", $reasons[$r['id']]);
        echo "<tr><td><input type=checkbox name='del[" . $r['id'] . "]' /></td><td NOWRAP>chr" . $chromhash[$chr] . ":$pos-" . $r['stop'] . "</td><td>" . $r['RefAllele'] . '/' . $r['AltAllele'] . '</td><td NOWRAP><input type=hidden name=reason[' . $r['id'] . "] value='$reason'>$reason</td><td><input type=text name='newpos[" . $r['id'] . "]' size=30 /></td><td><input type='text' name='comment[" . $r['id'] . "]' size=30 /></td><td>$anno&nbsp;</td></tr>";;
    }
    echo "<tr><td class=last colspan=7>&nbsp;</td></tr></table>";
    echo "</p><p><input type=submit class=button name=CurateVariants value='Save Curations'></p>";
    echo "</form>";
}
