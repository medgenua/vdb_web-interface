<?php
// check login
if (!isset($loggedin) || $loggedin != 1) {
	include('page_login.php');
	exit;
}

if (isset($_GET['t']) && $_GET['t'] == 'show') {
	include("includes/inc_genepanels_".$_GET['t'].".inc");
	exit;
}
if (!isset($_GET['t'])) {
	$_GET['t'] = 'manage';
}

echo "<div class=section>";
echo "<h3>Gene Panel Management</h3>";
echo "<p>";
echo "<form action='index.php' method=GET>";
echo "<input type=hidden name='page' value='genepanels'/>";
echo "Action : <select name='t'>";
$t = array('manage' => 'View/Share/Edit Gene Panels','new' => 'Define a New Gene Panel'); //,'edit' => 'Edit Gene Panels');
foreach($t as $key => $value) {
	$s = '';
	if ($_GET['t'] == $key) {
		$s = 'SELECTED';
	}
	echo "<option value='$key' $s>$value</option>";
}
echo "</select> : <input type=submit class=button name='ts' value='Select' /></form>\n";
echo "</div>";

if (!file_exists("includes/inc_genepanels_".$_GET['t'].".inc")) {
	include('page_404.php');
	exit;
}
else {
	include("includes/inc_genepanels_".$_GET['t'].".inc");
}
