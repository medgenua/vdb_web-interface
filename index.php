<?php
header("Content-Type: text/html; charset=utf-8");
///////////////////
// START SESSION //
///////////////////
$session_lifetime = 3600 * 8;
ini_set("session.cookie_lifetime", $session_lifetime); // session life set to 1/2 day
session_start(); //Allows you to use sessions
if (!isset($_SESSION['fresh_session'])) {
    $_SESSION['fresh_session'] = 1;
}
$_SESSION['timeout'] = $session_lifetime;
// Check if the session has timed out
if (isset($_GET['expire']) || (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity'] > $_SESSION['timeout']))) {
    // Destroy the session
    session_unset();
    session_destroy();
    // Redirect to the login page
    header('Location: index.php?page=expired');
    exit;
}
// Update the last activity timestamp
$_SESSION['last_activity'] = time();

/////////////////////////
// GET SERVER LOCATION //
/////////////////////////
// This allowes for local installations, as hostname is filled in dynamically
$domain = $_SERVER['HTTP_HOST'];
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="https://www.w3.org/1999/xhtml">
<?php
//#######################
//# CONNECT to database #
//#######################
$ok = include('.LoadCredentials.php');
if ($ok != 1) {
    include('inc_installation_problem.inc');
    exit();
}
include('includes/inc_logging.inc');
// SET DATABASE HERE. NOW LIFTOVER PROOF !!
$_SESSION['db'] = 'NGS-Variants' . $_SESSION['dbname'];

require("includes/inc_query_functions.inc");
/////////////////////////////////////
// CHECK USER INFORMATION ON LOGIN //
/////////////////////////////////////
$loaddata = 0;
if (isset($_POST['email']) &&  isset($_POST['password']) && !isset($_POST['register'])) { //If you entered an email and password (but not from request username) .
    $email = $_POST['email'];
    $password = $_POST['password'];
    $query = "SELECT `level`, `FirstName`, `LastName`, `id`,`cram_notified` FROM `Users` WHERE email = '" . addslashes($_POST['email']) . "' AND password=MD5('" . $_POST['password'] . "')";
    $auth = array_shift(...[runQuery($query, 'Users')]);
    if (!is_array($auth) || count($auth) == 0) {
        echo "wrong email/pass entered<br/>";
        $page = 'login';
    } else {
        $uid = $auth['id'];
        // first clear out remaining session stuff in database.
        doQuery("DELETE FROM `Cookies` WHERE ckey LIKE '$uid" . "_%'", "Cookies");
        // then destroy the session

        $_SESSION['logged_in'] = "true";
        $_SESSION['email'] = $_POST['email']; //Saves your username (email in this case).
        $_SESSION['level'] = $auth['level'];
        $_SESSION['FirstName'] = $auth['FirstName'];
        $_SESSION['LastName'] = $auth['LastName'];
        $_SESSION['userID'] = $auth['id'];
        $_SESSION['freshlogin'] = 1;
        $_SESSION['cram_notified'] = $auth['cram_notified'];
    }
}

// get the info on the logged-in user
if (isset($_SESSION['logged_in'])) {
    $loggedin = 1;
    $username = $_SESSION['email'];
    $email = $_SESSION['email'];
    $firstname = $_SESSION['FirstName'];
    $lastname = $_SESSION['LastName'];
    $level = $_SESSION['level'];
    $userid = $_SESSION['userID'];
    // check for new messages for this user.
    $inbox = runQuery("SELECT Inbox.id FROM `Inbox` WHERE Inbox.to = '$userid' AND Inbox.read = 0", 'Inbox');
    if (count($inbox) > 0) {
        $_SESSION['newmessages'] = 1;
    } else {
        $_SESSION['newmessages'] = 0;
    }
    // action_inbox? 
    $_SESSION['actions_pending'] = 0;
    $inbox = runQuery("SELECT ia.uid FROM `Inbox_actions` ia WHERE ia.uid='$userid'", 'Inbox_actions');
    if (count($inbox) > 0) {
        $_SESSION['newmessages'] = 1;
        $_SESSION['actions_pending'] = 1;
    }

    // update logged-in table.
    doQuery("UPDATE `Users` SET active = '" . date("Y-m-d H:i:s") . "' WHERE id = '$userid'", "Users");
} else {
    $loggedin = 0;
}
// create the html headers
?>

<head>
    <!-- <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"> -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <?php echo "<base href='https://$domain/$basepath/' />"; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Biomina/MedGen VariantDB: Annotation and filtering of variants detected using next-generation sequencing (<?php echo $page; ?>)</title>
    <meta name="description" content="VariantDB: Frontend to the database containing variants from next generation sequencing. Multiple filtering and annotation options are available to help in sieving through the results." />
    <meta name="keywords" content="NGS, next generation sequencing, database, filter, annotate, annovar, seatleseq, Centrum Medische Genetica, Universiteit Antwerpen, Biomina" />
    <!-- Bundled General Stylesheet file -->
    <script type='text/javascript'>
        var session_save_path = '<?php echo session_save_path() ?>'
    </script>
    <link rel='stylesheet' type='text/css' href='StyleSheets/All_Format.css?<?php echo $tip; ?>' />
    <!-- load jquery  -->
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <!-- Global Javascript functions -->
    <script type='text/javascript' src='javascripts/Global.js?<?php echo $tip; ?>'></script>





    <?php
    if (isset($_SESSION['freshlogin']) && $_SESSION['freshlogin'] == 1) {
        // set cookie on cram_notification on fresh login.
        echo "<script type='text/javascript'>\n";
        if ($_SESSION['cram_notified'] == 1) {
            // permanently set.
            echo "  setCookie('cram','1','7');\n";
        } else {
            // agreed, but not permanent, a 7 day cookie.
            echo "  var cram = getCookie('cram');\n";
            echo "  if (cram !== '1') {\n";
            echo "          setCookie('cram','0','7');\n";
            echo "  }\n";
        }
        echo "</script>\n";
    }
    // set userid in javascripts.
    if (isset($userid)) {
        echo "<script type='text/javascript'>var cuid = '$userid';</script>";
    } else {
        echo "<script type='text/javascript'>var cuid = '';</script>";
    }
    // page specific scripts & styles
    switch ($page) {
        case 'variants':
        case 'report':
            // jstree+jquery
            echo "<link rel='stylesheet' href='javascripts/jstree/themes/default/style.min.css'/>\n";
            echo "<script type='text/javascript' src='javascripts/jstree/jstree.min.js'></script>\n";
            //echo "<script type=\"text/javascript\" src='https://www.google.com/jsapi?autoload={\"modules\":[{\"name\":\"visualization\",\"version\":\"1\",\"packages\":[\"corechart\"]}]}' > </script>\n";
            echo "<script type='text/javascript' src='javascripts/bsn.AutoSuggest_2.1.3.js'></script>\n";
            echo "<link rel='stylesheet' type='text/css' href='StyleSheets/autosuggest.css' />\n";
            break;
        case 'samples':
        case 'classifier':
            echo "<script type='text/javascript' src='javascripts/bsn.AutoSuggest_2.1.3.js'></script>\n";
            echo "<link rel='stylesheet' type='text/css' href='StyleSheets/autosuggest.css' />\n";
            break;
        case 'tutorial':
            echo "<script type='text/javascript' src='javascripts/lightbox.js'></script>\n";
            echo "<link rel='stylesheet' type='text/css' href='StyleSheets/lightbox.css' />\n";
            break;
    }
    // CHECK SITE STATUS (operative / construction / LiftOver //
    $row = array_shift(...[runQuery("SELECT status FROM `NGS-Variants-Admin`.`SiteStatus`", "SiteStatus")]);
    $SiteStatus = $row['status'];
    switch ($SiteStatus) {
        case "Construction":
        case "LiftOver":
            echo "<link rel='stylesheet' type='text/css' href='StyleSheets/construction.css'/>\n";
            echo "<script type='text/javascript' src='javascripts/construction.js?$tip'></script>\n";
            break;
    }



    ?>
    <!-- Begin Cookie Consent plugin by Silktide - https://silktide.com/cookieconsent -->
    <script type="text/javascript">
        window.cookieconsent_options = {
            "message": "VariantDB uses cookies to ensure you get the best experience on our website - ",
            "dismiss": "Got it!",
            "learnMore": "<span onClick='Show_CC_Details();return false' id='see_cc_det'>Show Details</span><span id='hide_cc_det' style='display:none;' onClick='Hide_CC_Details(); return false'>Hide Details</span>",
            "link": "#",
            "theme": "dark-top"
        };
    </script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
    <!-- End Cookie Consent plugin -->

</head>

<!-- START OF THE BODY -->

<body <?php
        // browsing archived version ? (color switch disabled)
        //if (array_key_exists('bgcolor',$_SESSION)) {
        //	echo $_SESSION['bgcolor'];
        //}
        // load cookies (overlay set immediately, once ready, start populating)

        if (isset($_SESSION['freshlogin']) && $_SESSION['freshlogin'] == 1) {
            echo "onload=\"CheckServices()\"";
        }
        ?>>
    <?php
    if (isset($_SESSION['freshlogin']) && $_SESSION['freshlogin'] == 1 && isset($_SESSION['actions_pending']) && $_SESSION['actions_pending'] == 1) {
        echo "<div id='overlay' >\n";
        echo "<h3>Actions Required</h3>";
        echo "<p>There are pending actions, assigned to your account.</p>";
        echo "<p>To handle them later, go to your inbox, using the red envelop in the top right corner.</p>";
        echo "<p><input type=button class=button value='Go to inbox' onClick=\"javascript:location.href='index.php?page=inbox'\" /> &nbsp; <input type=button class=button value='Close' onClick=\"document.getElementById('overlay').style.display='none'\"/></p>";
        echo "</div>";
    } else {
        echo "<div id='overlay' style='display:none;'>";
        echo "<!-- general purpose overlay div --></div>\n";
    }
    $_SESSION['freshlogin'] = 0;
    // hidden div to host active build version.
    echo "<div style='display:none;' id=ActiveBuild>" . substr($_SESSION['dbname'], 1) . "</div>";
    ?>
    <div id='cc_details' style='display:none;'>
        We collect two types of cookies:<br />
        &nbsp; 1. Anonymous usage statistics to identify platform bottlenecks<br />
        &nbsp; 2. Activity tracking to restore your settings to the previous state when you return<br />
        <br />
        Stored information is not shared with third parties.<br />
        More information on why you see this message: <a style='font-style:italic' href='https://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm' target='_blank'>European Commission</a>
    </div>
    <div id="container">
        <!-- header -->
        <div id="header" style='position:relative;'>
            <span id="header-left">
                <span style='vertical-align:bottom;'>
                    <h1 style='float:left;'>VariantDB</h1>
                </span>
                <span style='position:absolute;bottom:0em;font-size:0.9em;font-style:italic' id='RevSpan'>
                    <!--Load by Ajax -->
                </span>
                <script type="text/javascript">
                    CheckRevision('tip', 'RevSpan');
                </script>
            </span>
            <span id="header-right">
                <?php
                // check for site-specific name
                if (file_exists('Local_Name.txt')) {
                    echo file_get_contents('Local_Name.txt');
                } else {
                    // check import status
                    $result = `ps aux | grep VariantDB_Importer.py | grep -v grep | wc -l`;
                    if ($result > 0) {
                        echo "<span title='System might become temporarily unresponsive' style='padding-right:1em;font-size:1.2em;position:relative;bottom:0.2em;'><font color='red' style='font-style:italic;'>DATA IMPORT RUNNING</font></span><img src='Images/layout/CMG.png' style='height:3.5em;margin-right:0.75em'><span style='font-size:3.5em;position:relative;bottom:0.2em'><font color='a0bf41'>bio</font><font color='5f7791'>min</font><font color='b10039'>a</font></span>";
                    } elseif (array_key_exists('Archived', $_SESSION) && $_SESSION['Archived'] == 1) {
                        echo "<span title='All Data is Read Only. Performance might be slower on archived data' style='padding-right:1em;font-size:1.8em;position:relative;bottom:0.2em;'><font color='red' style='font-style:italic;'>BROWSING ARCHIVED GENOME BUILD " . $_SESSION['dbstring'] . "</font></span><img src='Images/layout/CMG.png' style='height:3.5em;margin-right:0.75em'><span style='font-size:3.5em;position:relative;bottom:0.2em'><font color='a0bf41'>bio</font><font color='5f7791'>min</font><font color='b10039'>a</font></span>";
                    }
                    ## else date greetings
                    elseif (date('j') > 14 && date('j') <= 26 && date('n') == 12) {
                        echo "<img style='height:7em;margin-right:5em;margin-top:-0.4em;' src='Images/layout/santa.png'><span style='font-size:3.5em;position:relative;bottom:1em'><font color='a0bf41'>bio</font><font color='5f7791'>min</font><font color='b10039'>a</font></span>";
                    } elseif (date('j') > 26 && date('n') == 12) {
                        echo "<img style='height:7em;margin-right:5em;margin-top:-0.8em;' src='Images/layout/newyeardog.png'><span style='font-size:3.5em;position:relative;bottom:0.9em'><font color='a0bf41'>bio</font><font color='5f7791'>min</font><font color='b10039'>a</font></span>";
                    } elseif (date('j') <= 4 && date('n') == 1) {
                        echo "<img style='height:7em;margin-right:5em;margin-top:-0.8em;' src='Images/layout/newyeardog.png'><span style='font-size:3.5em;position:relative;bottom:0.9em'><font color='a0bf41'>bio</font><font color='5f7791'>min</font><font color='b10039'>a</font></span>";
                    } else {
                        echo "<img src='Images/layout/CMG.png' style='height:3.5em;margin-right:0.75em'><span style='font-size:3.5em;position:relative;bottom:0.2em'><font color='a0bf41'>bio</font><font color='5f7791'>min</font><font color='b10039'>a</font></span>";
                    }
                }
                ?>

            </span>
        </div>
        <!-- menu -->
        <div id="menu">
            <?php include('includes/inc_menu.inc'); ?>
        </div>

        <!-- Contents -->
        <div id="MainContents">
            <?php
            switch ($SiteStatus) {
                case "Install";
                    // include first-time run page for finalising setup
                    $page = 'freshinstall';
                    break;
                case "Construction":
                    if ($level < 3 && $loggedin == 1) {  # This shows the blocking overlay for all users with permissions level below 3 (non-admins)
                        echo "<div id='displaybox' ><br/><br/><br/><center style='font-size:18px;font-weight:bold;color:white;position:relative;top:25%' ><img src='Images/layout/under-construction.gif'><br/>The Platform is currently undergoing critical upgrades, making any analysis temporarily impossible. Please check back soon !<br/><br/>Thank you for your patience.";
                        if (file_exists("/tmp/VDB_DownTime_Reason.inc")) {
                            echo "<br/>";
                            include("/tmp/VDB_DownTime_Reason.inc");
                        }
                        echo "</center></div>";
                    }
                    break;
                    /*	
			case "LiftOver":
				if ($_SESSION['freshlogin'] == 1) {  # This shows the blocking overlay for all users with permissions level below 3 (non-admins)
					echo "<div id='displaybox' ><br/><br/><br/><center style='font-size:18px;font-weight:bold;color:white;position:relative;top:25%' ><img src='images/content/update.jpg'><br/>The Platform is currently being transferred to a new Genome Build. You can close this message and browse the data, but nothing can be changed !<br/><br/>Thank you for your patience.<br/><br/><a href='#' onclick='return clicker();'>CLOSE WINDOW</a></center></div>";
					$_SESSION['freshlogin'] = 0;
				}
				break;
			*/
            }
            if (!file_exists("page_$page.php")) {
                include('page_404.php');
            } else {
                include("page_$page.php");
            }
            // load page specific javascript if available in page.name.js format
            if (file_exists("javascripts/page.$page.js")) {
                echo "<script type='text/javascript' src='javascripts/page.$page.js?$tip'></script>";
            }

            ?>

        </div>
    </div>
    <!-- load google analytics -->
    <?php
    // other javascripts.
    switch ($page) {
        case 'variants':
        case 'report':
            echo "<script type=\"text/javascript\" src='https://www.google.com/jsapi?autoload={\"modules\":[{\"name\":\"visualization\",\"version\":\"1\",\"packages\":[\"corechart\"]}]}' > </script>\n";
            break;
    }
    include('Google_analytics.txt');
    ?>
    <script type='text/javascript'>
        $(document).ready(function() {
            if (<?php echo $loggedin; ?> == 1) {
                // Call the jQuery function on an interval 
                setInterval(function() {
                    // Your jQuery code goes here
                    //console.log("Checking session : ");
                    $.get("ajax_queries/CheckSession.php?timeout=<?php echo $session_lifetime; ?>", function(data) {
                        //console.log(data);
                        if (data == 'Expired') {
                            window.location.href = 'index.php?expire';
                        }
                    })
                    // 600 seconds : 10 minutes
                }, 600000);
            } //else {
            //console.log("Not logged in, no need to check session expiration");
            //}
        });
    </script>
    <!-- hidden div with iframe for alamut links -->
    <div style='display:none'><iframe name='at' src='page_404.php' /></div>
</body>

</html>