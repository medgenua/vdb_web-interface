<div class=section>
    <h3>Documentation : IGV </h3>
    <h4>1. Installing IGV</h4>
    <!--
<p class=indent>Download the IGV from the following website: <a href="https://www.broadinstitute.org/software/igv/download">Download</a>. There are multiple options here. It is recommended to use the 'launch' buttons, as these will always use the latest version. The binary distribution starts a little bit faster, but you will have to update manually.</p>
-->
    <p class=indent>Download the IGV from the following website: <a href="https://www.broadinstitute.org/software/igv/download_snapshot">Download</a>. This will install the BETA version of IGV, which might give some problems. However, since VariantDB switched to CRAM as its storage backend, using the BETA version is mandatory. Once the main IGV release supports CRAM files, we'll update this link.</p>

    <h4>2. Adding our Reference Genome</h4>
    <p class=indent>The Biomina/medgen galaxy server uses a cleaned up version of the UCSC genome build to convert BAM files to CRAM format. In particular, all chrXX_un and chrXX_random entries were removed. If the specified reference in the VCF/BAM file is not recognized as the same version, imported files are kept as BAM files. </p>
    <p>It is highly recommended to download and install this reference genome locally, as it will significantly improve IGV performance. This is done using the following steps, and is only needed once, as IGV will remember these settings:
    <ol>
        <li>Download the reference genome <a href='http://143.169.238.8/variantdb_files/Genomes/variantdb.genome.zip'> HERE </a></li>
        <li>Unzip the file. It will require approximately 3.5Gb of space. You can delete the zip file afterwards.</li>
        <li>Launch IGV</li>
        <li>From the file menu, select 'Load Genome from file'</li>
        <li>Browse to the folder you extracted the zipfile in, and select 'hg19.variantdb.genome'</li>
        <li>Press OK and wait a minutes</li>
        <li>If it works, you should now have the 'Human hg19 (VariantDB-CRAM)' entry in the dropdown list on the top left of the screen</li>
    </ol>
    </p>
    <p class=indent><a href="Images/Content/igv_genome.png" rel=lightbox><img class=simple style="width:15em;" src='Images/Content/igv_genome.png' rel=lightbox title='Click To Enlarge' /></a>
    </p>
    <h4>3. Get Sample Data into VariantDB</h4>
    <p class=indent>To load data into IGV from the VariantDB itself, there are multiple options. First, it can be imported from our Galaxy server using a dedicated tool on the Biomina Galaxy instance, following these steps:
    <ol>
        <li>Use galaxy on <a href="http://143.169.238.104/galaxy/">this location</a></li>
        <li>Analyze your sample, and call SNP/indels by GATK Unified genotyper. This is currently the only supported format. </li>
        <li>Select the "Send VCF file to the database" tool (under "Our Tools").</li>
        <li>Provide the GATK VCF file from the first drop-down list</li>
        <li>Select "Yes" in the second field to Store VCF and BAM files. This will send both files to the VariantDB storage location</li>
        <li>Finally, provide a BAM file and a sample name. The sample name and gender can be adapted later in VariantDB</li>
    </ol>
    <p class=indent> Second, it can be directly imported using <a href='index.php?page=upload'> the Web-UI </a> Instructions are listed on there. Third, data can be imported using API. This is documented <a href='index.php?page=tutorial&topic=api_import'> here</a>.</p>

    <p class=indent><a href="Images/Content/galaxy_send_vcf.png" rel=lightbox><img src="Images/Content/galaxy_send_vcf.png" class=simple style='width:30em;' title='Click To Enlarge' /></a></p>

    <h4>4. Load Sample Data into IGV</h4>
    <p class=indent>If the datafiles were sent to VariantDB from galaxy, you will now see a link on the top right of the "Variant Filter" page, next to the sample you have selected. Depending on what is available (VCF, BAM or both), this link will differ. Click on the link to load the data into a <span class=emph>running</span> IGV session, so make sure IGV was already started. As only fragments of data are loaded when visualized, this should go rather fast./p>
    <p class=indent><a href="Images/Content/igv_load_data.png" rel=lightbox><img src="Images/Content/igv_load_data.png" class=simple style='width:30em;' /></a></p>

    <h4>5. Navigating IGV through VariantDB</h4>
    <p>Results returned from the filter are presented in a tabular fashion. The first field in this table is always the genomic location of the variant. This location is a hyperlink to IGV. By clicking on the link, the display of IGV will jump to the location (+ 20bps on each side), and if present, the corresponding reads will be loaded from the BAM file. </p>
    <p class=indent><a href="Images/Content/igv_link_pos.png" rel=lightbox><img src="Images/Content/igv_link_pos.png" class=simple style='width:30em;' /></a></p>
</div>