<?php

echo "<div class=section>";
echo "<h3>Generate Reports</h3>";
echo "<div class='toleft w50'><span class=emph>Select Samples:</span><br/>";
$rows = runQuery("SELECT s.id,s.Name AS sname, p.Name AS pname FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects` p JOIN `Projects_x_Users` pu ON ps.pid = p.id AND ps.sid = s.id AND pu.pid = p.id WHERE pu.uid = '$userid' ORDER BY p.Name ASC, s.Name ASC", "Samples:Projects_x_Samples:Projects:Projects_x_Users");
$nr = count($rows);
if ($nr > 15) {
    $nr = 15;
}
if ($nr == 0) {
    echo "No samples available.";
} else {
    echo "<select name='samples' id='samples' multiple='multiple' size=$nr style='margin-left:2em;margin-top:1em;'>";
    $opt_group = '';
    foreach ($rows as $row) {
        if ($row['pname'] != $opt_group) {
            if ($opt_group != '') {
                echo "</optgroup>";
            }
            echo "<optgroup label='" . $row['pname'] . "'>";
            $opt_group = $row['pname'];
        }
        echo "<option value='" . $row['id'] . "'>" . $row['sname'] . "</option>";
    }
    echo "</optgroup></select>";
}
echo "</div>";
echo "<div class='toleft w50'><span class=emph>Select Report Template:</span> &nbsp; ";
$rows = runQuery("SELECT Name, rid FROM `Report_Definitions` WHERE Owner = '$userid' OR Public = 1", "Report_Definitions");
$report_defs = array();
foreach ($rows as $row) {
    $report_defs[$row['rid']] = $row['Name'];
}
$rows = runQuery("SELECT rd.Name, rd.rid FROM `Report_Definitions` rd JOIN `Users_x_Report_Definitions` urd ON urd.rid = rd.rid WHERE uid = '$userid' ", "Report_Definitions:Users_x_Report_Definitions");
foreach ($rows as $row) {
    $report_defs[$row['rid']] = $row['Name'];
}
if (count($report_defs) == 0) {
    echo "No Report Definitions available";
} else {
    echo "<select name='report' id='report' onchange='LoadReportDetails()'><option disabled='disabled' selected='selected' value=''></option>";
    asort($report_defs);

    foreach ($report_defs as $rid => $name) {
        echo "<option value='$rid'>$name</option>";
    }
    echo "</select>";
}
echo "<div id='ReportDetails'></div>";
echo "<div id='timerID' style='display:none'></div>";
echo "</div>";
echo "<br style='clear:both;'/><br/>";
echo "<input type=submit class=button value='Generate Reports' id=GenerateReport onClick='GenerateReport($userid)'/>";
echo "</div>";
echo "<div class=section id='report_links'></div>";
