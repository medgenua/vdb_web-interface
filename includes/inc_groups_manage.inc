<?php
if (isset($_POST['modify'])) {
    // vars 
    $groupid = $_POST['groupid'];
    $gname = $_POST['groupname'];
    $gdescr = $_POST['description'];
    $opengroup = $_POST['opengroup'];
    $confirmation = $_POST['confirmation'];
    $Affis = $_POST['Affis'];
    $affistring = '';
    foreach ($Affis as $name) {
        $affistring .= "$name,";
    }
    $affistring = substr($affistring, 0, -1);
    $administrator = $_POST['administrator'];
    $editvariant = $_POST['editvariant'];
    $editclinic = $_POST['editclinic'];
    $editsample = $_POST['editsample'];
    $row = runQuery("SELECT u.id, u.LastName, u.FirstName FROM Usergroups ug JOIN Users u ON u.id = ug.administrator WHERE ug.id = '$groupid'", "Usergroups:Users")[0];
    $oldadmin = $row['id'];
    $oldlname = $row['LastName'];
    $oldfname = $row['FirstName'];
    // update main properties
    if ($opengroup == 1) {
        doQuery("UPDATE Usergroups SET name='$gname', description='$gdescr', opengroup=1, administrator='$administrator', confirmation=$confirmation, editvariant=$editvariant, editclinic=$editclinic, editsample='$editsample' WHERE id = '$groupid'", "Usergroups");
    } else {
        doQuery("UPDATE Usergroups SET name='$gname', description='$gdescr', affiliation='$affistring', opengroup=0, confirmation=1,administrator='$administrator',editvariant=$editvariant,editclinic=$editclinic, editsample='$editsample' WHERE id = '$groupid'", "Usergroups");
    }
    echo "<div class=section>\n";
    echo "<h3>Usergroup Updated: $gname</h3>\n";
    echo "<p>The changes were successfully applied. </p>\n";
    // update users
    $addusers = isset($_POST['addusers']) ? $_POST['addusers'] : array();
    $delusers = isset($_POST['delusers']) ? $_POST['delusers'] : array();;
    foreach ($addusers as $uid) {
        // add user to group
        doQuery("INSERT INTO `Usergroups_x_User` (gid, uid) VALUES ('$groupid','$uid') ON DUPLICATE KEY UPDATE uid=uid", "Usergroups_x_User");
        // give access to projects
        $rows = runQuery("SELECT pid FROM `Projects_x_Usergroups` WHERE gid = '$groupid'", "Projects_x_Usergroups");
        foreach ($rows as $project) {
            $pid = $project['pid'];
            // has access ?
            $arow = array_shift(...[runQuery("SELECT editvariant,editclinic,editsample FROM `Projects_x_Users` WHERE pid = '$pid' AND uid = '$uid'", "Projects_x_Users")]);
            if (!is_array($arow) ||  count($arow) == 0) {
                // new data for user: update summary
                doQuery("INSERT INTO `Projects_x_Users` (pid, uid, editvariant, editclinic,editsample) VALUES ('$pid','$uid', '$editvariant', '$editclinic','$editsample') ", "Projects_x_Users:Variants_x_Users_Summary:Summary");
                doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$uid'", "Users");
            } else {
                // get max values
                $var = max($editvariant, $arow['editvariant']);
                $clinic = max($editclinic, $arow['editclinic']);
                $sam = max($editsample, $arow['editsample']);
                // update permissions, no new data. 
                doQuery("UPDATE `Projects_x_Users` SET editsample = '$sam', editvariant = '$var', editclinic = '$clinic' WHERE uid = '$uid' AND pid = '$pid'", "Projects_x_Users");
            }
        }
        // give access to filters
        $rows = runQuery("SELECT fid FROM `Usergroups_x_Shared_Filters` WHERE gid = '$groupid'", "Usergroups_x_Shared_Filters");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $fid = $row['fid'];
                doQuery("INSERT IGNORE INTO `Users_x_Shared_Filters` (uid,fid) VALUES ('$uid','$fid')", "Users_x_Shared_Filters");
            }
        }
        // give access to annotations
        $rows = runQuery("SELECT aid FROM `Usergroups_x_Shared_Annotations` WHERE gid = '$groupid'", "Usergroups_x_Shared_Annotations");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $aid = $row['aid'];
                doQuery("INSERT IGNORE INTO `Users_x_Shared_Annotations` (uid,aid) VALUES ('$uid','$aid')", "Users_x_Shared_Annotations");
            }
        }
        // give access to classifiers
        $rows = runQuery("SELECT cid, can_use,can_add,can_validate,can_remove,can_share FROM `Usergroups_x_Classifiers` WHERE gid = '$groupid'", "Usergroups_x_Classifiers");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $cid = $row['cid'];
                // has access ? 
                $arow = array_shift(...[runQuery("SELECT can_use,can_add,can_validate,can_remove,can_share FROM `Users_x_Classifiers` WHERE cid = '$cid' AND uid = '$uid'", "Users_x_Classifiers")]);
                // has access.
                if (count($arow) > 0) {
                    $use = max($arow['can_use'], $row['can_use']);
                    $add = max($arow['can_add'], $row['can_add']);
                    $val = max($arow['can_validate'], $row['can_validate']);
                    $del = max($arow['can_remove'], $row['can_remove']);
                    $sha = max($arow['can_share'], $row['can_share']);
                    doQuery("UPDATE `Users_x_Classifiers` SET `can_use` = '$use', `can_add` = '$add', `can_validate` = '$val', `can_remove` = '$del', `can_share` = '$sha' WHERE `uid` = '$uid' AND `cid` = '$cid'", "Users_x_Classifiers");
                }
                // new access
                else {
                    $use = $row['can_use'];
                    $add = $row['can_add'];
                    $val = $row['can_validate'];
                    $del = $row['can_remove'];
                    $sha = $row['can_share'];

                    doQuery("INSERT INTO `Users_x_Classifiers` (uid,cid,can_use,can_add,can_validate,can_remove,can_share,apply_on_import) VALUES ('$uid','$cid','$use','$add','$val','$del','$sha','0')", "Users_x_Classifiers");
                }
            }
        }

        // give access to reports
        $rows = runQuery("SELECT rid, edit,full FROM `Usergroups_x_Report_Definitions` WHERE gid = '$groupid'", "Usergroups_x_Report_Definitions");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $rid = $row['rid'];
                // has access ? 
                $arow = array_shift(...[runQuery("SELECT edit,full FROM `Users_x_Report_Definitions` WHERE rid = '$rid' AND uid = '$uid'", "Users_x_Report_Definitions")]);
                // has access.
                if (count($arow) > 0) {
                    $edit = max($arow['edit'], $row['edit']);
                    $full = max($arow['full'], $row['full']);
                    doQuery("UPDATE `Users_x_Report_Definitions` SET `edit` = '$edit', `full` = '$full' WHERE `uid` = '$uid' AND `rid` = '$rid'", "Users_x_Report_Definitions");
                }
                // new access
                else {
                    $edit = $row['edit'];
                    $full = $row['full'];
                    doQuery("INSERT INTO `Users_x_Report_Definitions` (uid,rid,edit,full) VALUES ('$uid','$rid','$edit','$full')", "Users_x_Report_Definitions");
                }
            }
        }
        // give access to report sections
        $rows = runQuery("SELECT rsid, edit,full FROM `Usergroups_x_Report_Sections` WHERE gid = '$groupid'", "Usergroups_x_Report_Sections");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $rsid = $row['rsid'];
                // has access ? 
                $arow = array_shift(...[runQuery("SELECT edit,full FROM `Users_x_Report_Sections` WHERE rsid = '$rsid' AND uid = '$uid'", "Users_x_Report_Sections")]);
                // has access.
                if (count($arow) > 0) {
                    $edit = max($arow['edit'], $row['edit']);
                    $full = max($arow['full'], $row['full']);
                    doQuery("UPDATE `Users_x_Report_Sections` SET `edit` = '$edit', `full` = '$full' WHERE `uid` = '$uid' AND `rsid` = '$rsid'", "Users_x_Report_Sections");
                }
                // new access
                else {
                    $edit = $row['edit'];
                    $full = $row['full'];
                    doQuery("INSERT INTO `Users_x_Report_Sections` (uid,rsid,edit,full) VALUES ('$uid','$rsid','$edit','$full')", "Users_x_Report_Sections");
                }
            }
        }
        // give access to report checkboxes
        $rows = runQuery("SELECT cid, edit,full FROM `Usergroups_x_Report_Sections_CheckBox` WHERE gid = '$groupid'", "Usergroups_x_Report_Sections_CheckBox");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $cid = $row['cid'];
                // has access ? 
                $arow = array_shift(...[runQuery("SELECT edit,full FROM `Users_x_Report_Sections_CheckBox` WHERE cid = '$cid' AND uid = '$uid'", "Users_x_Report_Sections_CheckBox")]);
                // has access.
                if (count($arow) > 0) {
                    $edit = max($arow['edit'], $row['edit']);
                    $full = max($arow['full'], $row['full']);
                    doQuery("UPDATE `Users_x_Report_Sections_CheckBox` SET `edit` = '$edit', `full` = '$full' WHERE `uid` = '$uid' AND `cid` = '$cid'", "Users_x_Report_Sections_CheckBox");
                }
                // new access
                else {
                    $edit = $row['edit'];
                    $full = $row['full'];
                    doQuery("INSERT INTO `Users_x_Report_Sections` (uid,cid,edit,full) VALUES ('$uid','$cid','$edit','$full')", "Users_x_Report_Sections_CheckBox");
                }
            }
        }
        // give access to gene panels.
        $rows = runQuery("SELECT gpid, rw,share FROM `GenePanels_x_Usergroups` WHERE gid = '$groupid'", "GenePanels_x_Usergroups");
        foreach ($rows as $row) {
            $gpid = $row['gpid'];
            $rw = $row['rw'];
            $share = $row['share'];
            // has access ?
            $arow = array_shift(...[runQuery("SELECT rw,share FROM `GenePanels_x_Users` WHERE uid = '$uid' AND gpid = '$gpid'", "GenePanels_x_Users")]);
            if (count($arow) == 0) {
                doQuery("INSERT INTO `GenePanels_x_Users` (uid, gpid,rw,share) VALUES ('$uid','$gpid','$rw','$share')", "GenePanels_x_Users");
            } else {
                $rw = max($rw, $arow['rw']);
                $share = max($rw, $arow['share']);
                doQuery("UPDATE `GenePanels_x_Users` SET rw = '$rw', share = '$share' WHERE uid = '$uid' AND gpid = '$gpid'", "GenePanels_x_Users");
            }
        }
    }
    foreach ($delusers as $uid) {
        doQuery("DELETE FROM Usergroups_x_User WHERE gid = '$groupid' AND uid = '$uid'", "Usergroups_x_User");
    }
    // update un-shared  projects
    $delprojects = isset($_POST['delprojects']) ? $_POST['delprojects'] : array();
    foreach ($delprojects as $pid) {
        doQuery("DELETE FROM `Projects_x_Usergroups WHERE gid='$groupid' AND pid = '$pid'", "Projects_x_Usergroups");
    }
    // update administrator & notify new one if specified	
    if ($administrator != $oldadmin) {
        // send mail to new admin.
        $row = runQuery("SELECT email, LastName, FirstName FROM Users WHERE id = '$administrator'", "Users");
        $email = $row['email'];
        $lname = $row['LastName'];
        $fname = $row['FirstName'];
        $message = "Message sent from https://$domain\r";
        $message .= "Subject: VariantDB: Usergroup supervision\r\r";
        $message .= "$oldfname $oldlname passed the supervision of the usergroup below on to you.  From now on you will recieve emails when new users wish to join this group. As supervisor you can also add or remove additionial users and set permissions. You can do this from the link below (after logging in)\r\r";
        $message .= "Usergroup Name: $gname\r";
        $message .= "Group details: https://$domain/$basepath/index.php?page=group&type=manage&gid=$groupid\r";
        $message .= "\r\rYour VariantDB Administrator\r\n";
        $headers = "From: no-reply@$domain\r\n";
        if (mail($email, "VariantDB: Usergroup supervision update", $message, $headers)) {
        } else {
            echo "mail was not sent, something went wrong<br>";
        }


        echo "<p>Since you have passed the supervision of this group on to $fname $lname, you will no longer be able to select this project below. The new adminstrator will be notified of this change by email.</p>\n";
    }
    echo "</div>\n";
}

# ONCE ALL IS PROCESSED, RELOAD THE INFROMATION FOR THE SELECTED GROUP.
// get posted var
$gid = '';
if (isset($_POST['groupid'])) {
    $gid = $_POST['groupid'];
}
if ($gid == '' && isset($_GET['gid'])) {
    $gid = $_GET['gid'];
}
if ($gid == '') {
    $gid = $groupid;
}

// group selection form
echo "<div class=section>\n";
echo "<h3>Manage usergroups</h3>\n";
echo "<p>Pick a usergroup from the list below to edit permissions, associated projects, add or remove users and so on.</p>\n";


//GET GROUPS THAT ARE MANAGED BY CURRENT USER
$rows = runQuery("SELECT id, name FROM Usergroups WHERE administrator = '$userid'", "Usergroups");
if (count($rows) == 0) {
    echo "<p>You don't supervise any usergroups.</p>\n";
    echo "</div>\n";
} else {
    echo "<form action='index.php?page=group&type=manage' method=POST>\n";
    echo "<p>Available usergroups:\n ";
    echo "<select name=groupid><option value=''></option>\n";
    foreach ($rows as $row) {
        $cgid = $row['id'];
        $gname = $row['name'];
        if ($gid == $cgid) {
            $selected = "SELECTED";
        } else {
            $selected = '';
        }
        echo "<option value='$cgid' $selected>$gname</option>\n";
    }
    echo "</select>\n";
    echo "</p>\n";
    echo "<p><input type=submit class=button name=submit value='Pick'></p>\n";
    echo "</form>\n";
    echo "</div>\n";
}

if ($gid != '') {
    // get group info
    $row = runQuery("SELECT name, description, affiliation, opengroup, administrator, confirmation, editvariant, editclinic,editsample FROM Usergroups WHERE id = '$gid'", "Usergroups")[0];
    $groupname = $row['name'];
    $description = $row['description'];
    $affistring = $row['affiliation'];
    $Affis = explode(',', $row['affiliation']);
    $opengroup = $row['opengroup'];
    $gadmin = $row['administrator'];
    $confirmation = $row['confirmation'];
    $editvariant = $row['editvariant'];
    $editclinic = $row['editclinic'];
    $editsample = $row['editsample'];
    $affihash = array();
    if ($Affis) {
        foreach ($Affis as $affid) {
            $affihash["$affid"] = 1;
        }
    }
    $problem = 0;
    $checked = array('', 'checked');
    $checkedzero = array('checked', '');
    // print form
    if (isset($_POST['modify'])) {
        echo "<div class=section>\n";
        echo "<h3>Updated Properties: $groupname</h3>\n";
    } else {
        echo "<div class=section>\n";
        echo "<h3>Update Properties: $groupname</h3>\n";
    }
    if ($gadmin != $userid) {
        echo "<p>You are not the administrator of this usergroup.</p>\n";
        echo "</div>\n";
    } else {
        echo "<form action='index.php?page=group&amp;type=manage&amp;groupid=$gid' method=POST>\n";
        echo "<input type=hidden name=modify value=1>\n";
        echo "<input type=hidden name=groupid value='$gid'>\n";
        echo "<p><table>\n";
        echo "<tr>\n";
        echo "<th title='e.g. CMG Antwerp Clinicians'>Group Name</th>\n";
        echo "<td ><input type=text name='groupname' size=50 value='$groupname'></td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<th  valign=top>Group Description</th>\n";
        echo "<td ><TEXTAREA NAME='description' COLS=48 ROWS=2>$description</TEXTAREA></td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<tr>\n";
        echo "<th title='If selected, all users can join this usergroup'>Open Group</th>\n";
        echo "<td >	<input type='radio' name='opengroup' value=0 " . $checkedzero[$opengroup] . " onclick=\"document.getElementById('SetAffi').style.display='';document.getElementById('SetConfirm').style.display='none';\"> No";
        echo " <input type='radio' name='opengroup' value=1 " . $checked[$opengroup] . " onclick=\"document.getElementById('SetAffi').style.display='none';document.getElementById('SetConfirm').style.display='';\"> Yes</td>\n";
        echo "</tr>\n";
        if ($opengroup == 0) {
            echo "<tr id='SetConfirm' style='display:none;' title='Is confirmation needed when a user joins an open group?'>\n";
        } else {
            echo "<tr id='SetConfirm' title='Is confirmation needed when a user joins an open group?'>\n";
        }
        echo "<th class=clear>Confirmation needed</th>\n";
        echo "<td class=clear>	<input type='radio' name='confirmation' value=0 " . $checkedzero[$confirmation] . "> No ";
        echo "	<input type='radio' name='confirmation' value=1 " . $checked[$confirmation] . "> Yes </td>\n";
        echo "</tr>\n";
        if ($opengroup == 0) {
            echo "<tr id='SetAffi'>\n";
        } else {
            echo "<tr id='SetAffi' style='display:none;'>\n";
        }
        echo "<th class=clear valign=top>Allowed Institutions</th>\n";
        echo "<td class=clear><select name='Affis[]' size=4 MULTIPLE>";
        // get own affiliation
        $row = runQuery("SELECT a.name, a.id FROM Affiliation a JOIN Users u ON u.Affiliation = a.id WHERE u.id = '$userid' LIMIT 1", "Users:Affiliation")[0];
        $ownaffi = $row['name'];
        $ownid = $row['id'];

        if ($affihash[$ownid] == 1) {
            $selected = 'SELECTED';
        } else {
            $selected = '';
        }
        echo "<option value='$ownid' $selected>$ownaffi</option>\n";
        // get others, order alfabetically
        $rows = runQuery("SELECT a.name, a.id FROM Affiliation a WHERE NOT a.id = '$ownid' ORDER BY a.name", "Affiliation");
        foreach ($rows as $row) {
            $currname = $row['name'];
            $currid = $row['id'];
            if ($affihash[$currid] == 1) {
                $selected = 'SELECTED';
            } else {
                $selected = '';
            }
            echo "<option value='$currid' $selected>$currname</option>\n";
        }
        echo "</SELECT>\n";
        echo "</td>\n";
        echo "</tr>";
        echo "<tr>\n";
        echo "<th  title='Will users from this group be able to classify, delete or modify variants?'>Change Variant information</th>\n";
        echo "<td ><input type=radio name=editvariant value=0 " . $checkedzero[$editvariant] . "> No <input type=radio name=editvariant value=1 " . $checked[$editvariant] . "> Yes</td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<th  title='Will users from this group be able to add or edit clinical information?'>Change Clinical Information</th>\n";
        echo "<td><input type=radio name=editclinic value=0 " . $checkedzero[$editclinic] . "> No <input type=radio name=editclinic value=1 " . $checked[$editclinic] . "> Yes</td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<th  title='Will users from this group be able to manage projects/samples (share, delete,set family)?'>Manage Sample/Projects</th>\n";
        echo "<td ><input type=radio name=editsample value=0 " . $checkedzero[$editsample] . "> No <input type=radio name=editsample value=1 " . $checked[$editsample] . "> Yes</td>\n";
        echo "</tr>\n";

        echo "<tr>\n";
        echo "<th  title='Make someone else administrator of the group'>Administrator</th>\n";
        if ($opengroup == 0) {
            $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM Users u JOIN Affiliation a ON u.Affiliation = a.id WHERE a.id IN ($affistring) ORDER BY u.Affiliation, u.LastName", "Users:Affiliation");
        } else {
            $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id ORDER BY u.Affiliation, u.LastName", "Users:Affiliation");
        }
        $currinst = '';
        echo "<td class=clear><select name=administrator>\n";
        foreach ($rows as $row) {
            $newid = $row['id'];
            $fname = $row['FirstName'];
            $lname = $row['LastName'];
            $newinst = $row['name'];
            if ($newinst != $currinst) {
                if ($currinst != '') {
                    echo "</OPTGROUP>\n";
                }
                echo "<OPTGROUP label='$newinst'>\n";
                $currinst = $newinst;
            }
            if ($newid == $gadmin) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            echo "<option value='$newid' $selected>$lname $fname</option>\n";
        }
        echo "</select></td>\n";
        echo "</tr>\n";
        // remove users
        $rows = runQuery("SELECT ugu.uid, u.LastName, u.FirstName, a.name FROM `Usergroups_x_User` ugu JOIN `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id AND ugu.uid = u.id WHERE gid = '$gid' ORDER BY u.Affiliation", "Usergroups_x_User:Users:Affiliation");
        $members = array();
        echo "<tr>\n";
        echo "<th  title='Remove Members from the group' valign=top>Remove Members</th>\n";
        echo "<td><select name='delusers[]' id='deluserlist' size=5 MULTIPLE>\n";
        $currinst = '';
        if (count($rows) == 0) {
            echo "<option value='-' disabled>Group has no members</option>";
        }
        foreach ($rows as $row) {
            $cuid = $row['uid'];
            $clname = $row['LastName'];
            $cfname = $row['FirstName'];
            $inst = $row['name'];
            if ($currinst != $inst) {
                if ($currinst != '') {
                    echo "</optgroup>";
                }
                echo "<optgroup label='$inst'>\n";
                $currinst = $inst;
            }
            echo "<option value='$cuid'>$clname $cfname</option>\n";
            $members[$row['uid']] = 1;
        }
        echo "</select>\n";
        echo "</td>\n";
        echo "</tr>\n";
        // add users
        echo "<tr>\n";
        echo "<th  title='Add Members to the group' valign=top>Add Members</th>\n";
        echo "<td><select name='addusers[]' id='userlist' size=5 MULTIPLE>\n";
        if ($opengroup == 1) {
            $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id ORDER BY a.name, u.LastName", "Users:Affiliation");
        } else {
            $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id WHERE a.id IN ($affistring) ORDER BY a.name, u.LastName", "Users:Affiliation");
        }
        $currinst = '';
        $listed = 0;
        foreach ($rows as $row) {
            $uid = $row['id'];
            $lastname = $row['LastName'];
            $firstname = $row['FirstName'];
            $institute = $row['name'];
            if ($currinst != $institute) {
                if ($currinst != '') {
                    echo "</optgroup>";
                }
                echo "<optgroup label='$institute'>\n";
                $currinst = $institute;
            }
            if (array_key_exists($uid, $members) && $members[$uid] == 1) {
                continue;
            }
            echo "<option value='$uid'>$lastname $firstname</option>\n";
            $listed++;
        }
        if ($listed == 0) {
            echo "<option value='-' disabled>None available</option>";
        }

        echo "</select></td>\n";
        echo "</tr>\n";

        // projects
        $rows = runQuery("SELECT p.id, p.Name, u.LastName, u.FirstName FROM Projects p JOIN Users u JOIN `Projects_x_Usergroups` ppg ON u.id = ppg.sharedby AND p.id = ppg.pid WHERE ppg.gid = '$gid' ORDER BY p.Name", "Projects:Users:Projects_x_Usergroups");
        if (count($rows) > 0) {
            echo "<tr>\n";
            echo "<th valign=top >Unshare Projects</th>\n";
            echo "<td >\n"; // containing cell
            echo " <table cellspacing=0>\n";
            echo " <tr>\n";
            echo " <th ><img src='Images/layout/icon_trash.gif' width=8px height=8px></th><th >Name</th><th >Shared By</th>\n";
            echo " </tr>\n";
            foreach ($rows as $row) {
                $pid = $row['id'];
                $sharedby = $row['LastName'] . " " . $row['FirstName'];
                $pname = $row['Name'];
                echo " <tr>\n";
                echo " <td ><input type=checkbox name='delprojects[]' value='$pid'></td>\n";
                echo " <td>$pname</td>\n";
                echo " <td>$sharedby</td>\n";
                echo " </tr>\n";
            }
            echo " </table>\n";
            echo " </td>\n";
            echo "</tr>\n";
        }


        echo "</table>\n";
        echo "<input type=submit class=button name=update value='Submit'>\n";
        echo "</form>\n";
        echo "</div>\n";
    }
}
