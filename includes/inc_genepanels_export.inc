<?php
// gpid set?
if (!isset($_GET['gpid']) || $_GET['gpid'] == '' || !is_numeric($_GET['gpid'])) {
    echo "<div class=section>";
    echo "<h3>Invalid Panel ID</h3>";
    echo "<p>The specified panel-id was not valid.</p>";
    echo "</div>";
    exit;
}
$gpid = $_GET['gpid'];
//allowed?
$rows = runQuery("SELECT rw FROM `GenePanels_x_Users` WHERE gpid = '$gpid' AND uid = '$userid'", "GenePanels_x_Users");
if (count($rows) == 0) {
    echo "<div class=section>";
    echo "<h3>Insufficient Privileges</h3>";
    echo "<p>You are not allowed to view this panel.</p>";
    echo "</div>";
    exit;
}



$rows = runQuery("SELECT gp.Name, gp.Description, u.LastName, u.FirstName, gp.LastEdit FROM `GenePanels` gp JOIN `Users` u ON gp.Owner = u.id WHERE gp.id = '$gpid' ", "GenePanels:Users")[0];
$gpname = $rows['Name'];
$gpdesc = $rows['Description'];
$gpowner = $rows['LastName'] . " " . $rows['FirstName'];
$gples = explode(" ", $rows['LastEdit']);
$gple  = $gples[0];
echo "<div class=section>";
echo "<h3>Panel Details: $gpname</h3>";
echo "<p><span class=emph>Owner:</span> $gpowner</p><p>";
echo "<span class=emph>Last Edit:</span> $gple</p><p>";
echo "<span class=emph>Name: </span><br/><span style='margin-left:1em;'> $gpname </span></p><p>";
echo "<span class=emph>Description:</span><br/>";
echo "<span style='margin-left:1em;' >$gpdesc</span>";
echo "</p><p><span class=emph>Included Genes:</span> <br/>";

//echo "<table style='width:90%;border-bottom:1px solid #333;margin-left:1em;' id=genetable cellspacing=0 ><tr><th class=top>RefSeq Symbol</th><th class=top>NCBI Gene_ID</th><th class=top>Comment</th><th class=top>Last Edit</th></tr>";
// get genes
$rows = runQuery("SELECT Symbol, gid, Comment, LastEdit FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = '$gpid' ORDER BY Symbol", "GenePanels_x_Genes_ncbigene");
$idx = -1;
$nrr = count($rows) + 2;
echo "<textarea style='margin-left:1em;' rows='$nrr' cols='75'>";
foreach ($rows as $key => $row) {
    $idx++;
    $slice = floor($idx / 1000);
    $gidx = $idx - $slice * 1000;
    //$line = "<tr >";
    $line = $row['Symbol'] . "\t";
    $line .= $row['gid'] . "\t";
    $line .= $row['Comment'] . "\t";
    $line .= $row['LastEdit'] . "\n";
    echo $line;
}
echo "</textarea>";
//$idx--;
// new genes
echo "</div>";
