<div class=section>
<h3>Plug-n-Play VariantDB using VirtualBox</h3>
<p>Follow the steps below to get a private VariantDB instance up and running with minimal effort. It requires the Oracle Virtual Machine Manager 'VirtualBox'. </p>

<p class=emph>Install VirtualBox</p>
<p>Oracle VM VirtualBox is freely downloadable from <a href="https://www.virtualbox.org/wiki/Downloads">their homepage</a>. Note: The VariantDB-VM requires a 64bit host! </p>

<p class=emph>Download and activate VariantDB_VM</p>
<p>The following steps were tested on Windows 7 SP1 as a host with VirtualBox v4.2.12.
<ol>
 <li>Download the VariantDB OVA File (15gb, MD5: 6720e259d6b3470036ef933d29be0c40): <a href="http://143.169.238.105/variantdb/Public_Files/VariantDB_VM.ova">Right-Click_Save-as to download</a></li>
 <li>Open VirtualBox Manager, select 'File'-&gt;'Import Appliance'. Browse to the downloaded variantdb.ova file.</li>
 <li>Right click the VM in the list and choose settings</li>
 <li>Set the number of RAM and CPUs to an appropriate value for your host. Note: to handle snpEff annotations, you need approximately 5-6gb of RAM.</li>

 </ol>
</p>

<p class=emph>Launch VariantDB</p>
<p><ol>
 <li>Launch the Virtual Machine by right-clicking and selecting 'Start'</li>
 <li>Log in. The VM system user is 'administrator' with a password 'default'. Make sure to change this upon first login ! (execute 'passwd')</li>
 <li>Write down the network address: run `ifconfig` and write down the 'inet addr' for 'eth1'. It should be in a 192.168.x.x range.</li>
 <li>open credentials file : 'sudo nano /VariantDB/.Credentials/.credentials' (provide your password when prompted)<ol>
	<li>Set &lt;ADMINMAIL&gt; to your email address. </li>
	<li>Set &lt;DATAHOST&gt; to the network address of the VM for IGVdata. This setting can be used to host BAM/VCF files on a remote server. </li>
	<li>Optional : enable FTP (FTP=1) and set the FTP_HOST value to the network address of the VM.</li>
	<li>Optional : USEMEMCACHED : if enabled, data are cached to improve performance. Be default, only localhost is used. More can be added as comma-seperated list</li>
	<li>Optional : Increase MYSQLTHREADS : allow multiple simultaneous connections to mysql to fetch filter & annotation data. Make sure to increase the number of CPUs in the VM appropriately.</li>
	<li>Optional : Enable HPC connection (USEHPC=1): if enabled, the VM will submit annotation scripts for new data to a high perforance cluster (torque-pbs). Make sure your HPC is configured to allow the VM to submit jobs</li>
   </ol></li> 
 <li>Set the correct smtp server for outgoing emails in /etc/mail/sendmail.cf (Replace the smtp.ua.ac.be entry). </li>
 <li>You can now close the VirtualBox Manager and minimize the VariantDB-VM Window. Recent VirtualBox systems can be started headless, in this case you can close the VM window completely.</li>
 
 <li>Note: The root password for the mysql database (accessible using phpmyadmin is also 'default'. </li>
</ol></p>
<p class=emph>Initialize and update VariantDB</p>
<p><ol>
	<li>In your browser, go to http://&lt;variantdb-vm.ip.address/variantdb</li>
	<li>Fill in details for the administrator user of VariantDB</li>
	<li>Submit the form and wait for the confirmation. This can take a while, since VariantDB now performs various initialisation steps.</li>
	<li>Upon completion, you will be redirected to the home page. Login using the link in the top right corner</li>
	<li>To update your VariantDB version to the latest and greatest, go to "Settings => Platform updates". Take note that first load of this page can take a few minutes:<ol>
		<li>First select all available options <span class=emph>except</span> the database revisions, and press submit</li>
		<li>Select 'Return to system updates'. Now update the database</li>
		<li>After confirmation that the database update was done, click the 'start update' link (if present) to update the annotation data sources</li>
	</ol></li>
		
</ol></p>
<p class=emph>Load Demonstration Data</p>
<p><ol>
	<li>Former DECIPHER employee, Manuel Corpas, has made exome data available for his family. A GATK-like analysis result is available at : <a href='http://143.169.238.105/VariantDB/Public_Files'>The Main VariantDB WebSite</a></li>
	<li>First Download the data to your local computer, then upload using FTP to your VariantDB Instance:<ol>
		<li>Log in on the VariantDB Website, go to 'New Data'</li>
		<li>Log-in details for your FTP server are listed there. If not, first provide your password to activate FTP access.</li>
		<li>Connect using an FTP client (eg FileZilla), and upload the BAM/VCF files to your root folder. Data in subdirectories can not be used by VariantDB</li>
		<li><span class=emph>Note:</span> Make sure you adjust the size fo the ftp-data folder in the virtual machine to accomodate future data uploads. It is highly recommended to mount a network-share as the ftp-location.</li>
	</ol></li>
	<li>Go to 'New Data'. Provide the sample details, and select the correct files from the drop-down menus. Multiple samples can be added at once using the 'Load More Samples' button.</li>
	<li>Press 'Load Samples Into Database' to start the data import.</li>
	<li>Once the import is completed, you will recieve a notification by email.</li>
</ol></p>

<p><span class=emph>VariantDB is now ready.</span> Please note that it might take some time for the annotations to finish (indicated in the menu bar). 

	 
</div>
