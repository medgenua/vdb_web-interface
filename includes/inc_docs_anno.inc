<?php
echo "<div class=section>";
echo "<h3>Documentation : Annotations Explained </h3>";

// load xml library
require_once('xmlLib2.php');

// read the filter config xml
$configuration = my_xml2array("Annotations/Config.xml");
$anno_conf = get_value_by_path($configuration,"Annotation_Configuration");
$categories = array() ; //get_value_by_path($configuration,"Annotation_Configuration");
// reorganise annotations by categories
foreach($anno_conf as $subkey => $source) {
	if (!is_numeric($subkey)) {
		continue;
	}
	if ($source['name'] == 'null') {
		continue;
	}
	if ($source['name'] == 'Custom_VCF_Fields') {
		$categories[$source['name']] = array();
		continue;
	}
	$name = $source['name'];
	$items = get_value_by_path($configuration,"Annotation_Configuration/".$source['name']);	
	foreach($items as $ssubkey => $item) {
		if (!is_numeric($ssubkey)) {
			continue;
		}
		$iname = $item['name'];
		if ($iname == 'null') {
			continue;
		}

		$license = get_value_by_path($configuration,"Annotation_Configuration/$name/$iname/license");
		$lic = '';
		if ($license['value'] != '') {
			$lic = $license['value'];	
		}
		$help = get_value_by_path($configuration,"Annotation_Configuration/$name/$iname/help");
		$category = get_value_by_path($configuration,"Annotation_Configuration/$name/$iname/category");
		$deprecated = 0;
		if (get_value_by_path($configuration,"Annotation_Configuration/$name/$iname/deprecated")) {
			$deprecated = 1;
		}
		if (substr($iname,0,1) == '_') {
			$iname = substr($iname,1);
		}
		$category = $category['value'];
		if (!array_key_exists($category,$categories)) {
			$categories[$category] = array();
		}
		$categories[$category][$iname] = array('source' => $name, 'deprecated' => $deprecated, 'license' => $lic);
		if ($help['value'] != '') {
			$categories[$category][$iname]['help'] = $help['value'];
		}
		else {
			$categories[$category][$iname]['help'] = '';
		}
		
	  }
}


foreach ($categories as $category => $items) {
	
	echo "<p><table class=w50 cellspacing=0>";
	echo "<tr><th colspan=3 class=top>".$category." Information</th></tr>";
	foreach ($items as $item => $info) {
		// name.
		echo "<tr style='padding-bottom:1.5em;'><td valign=top >".$item."</td>";
		// license / deprecated notes
		echo "<td valign=top NOWRAP style='padding-bottom:0.75em;'>";
		$info['license'] = ( $info['license'] != '' ? "License: ".$info['license'] : '');
		$info['deprecated'] = ( $info['deprecated'] == 1 ? "<span style='color:red' title='This annotation is no longer updated.'>Deprecated</span>" : "");
		if ($info['deprecated'] != '' && $info['license'] != '') {
			echo $info['deprecated']." / ".$info['license'] ;
		}
		else {
			echo $info['deprecated'].$info['license'];
		}
		echo "</td>";
		// help.
		echo "<td valign=top style='padding-bottom:0.75em;'>".$info['help']."</td></tr>";
	}
	echo "<tr><td colspan=3 class=last>&nbsp;</td></tr>";
	echo "</table></p>";
}

echo "</div>";
?>
