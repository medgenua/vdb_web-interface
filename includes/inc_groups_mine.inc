<?php 
if ($_GET['action'] == 'leave') {
	$leavegid = $_GET['gid'];
	$query = doQuery("DELETE FROM `Usergroups_x_User` WHERE gid = '$leavegid' AND uid = '$userid'",'Usergroups_x_User');
}
echo "<div class=section>\n";
echo "<h3>Membership overview</h3>\n";
echo "<h4>Click usergroups for details</h4>\n";
echo "<p>This page presents an overview of the usergroups you are part of, and associated projects.  Click the usergroup of your choice for more information.  If you want to leave a specific group, click the 'leave this group' links. </p>\n";

// get private groups
echo "<p><span class=emph>Private Groups:</span>\n";

$rows = runQuery("SELECT ug.id, ug.name FROM `Usergroups` ug JOIN `Usergroups_x_User` ugu ON ug.id = ugu.gid WHERE ugu.uid = '$userid' AND opengroup = 0 ORDER BY ug.name",'Usergroups:Usergroups_x_User');
if (count($rows) > 0) {
	echo "<ol style='margin-left:5px'>\n";
	foreach($rows as $row) {
		$ugid = $row['id'];
		$ugname = $row['name'];
		echo "<li style='font-weight:bold;color:black;' onclick=\"showinfo('$ugid','$userid')\">$ugname<span id='group_$ugid' ></span></li>\n";	
	}
	echo "</ol>\n";
	echo "</p>\n";

}
else {
	echo "<br/><span style='margin-left:5px'>You are not a member of any private usergroups</p>";
}
// get open groups
$rows = runQuery("SELECT ug.id, ug.name FROM `Usergroups` ug JOIN `Usergroups_x_User` ugu ON ug.id = ugu.gid WHERE ugu.uid = '$userid' AND opengroup = 1 ORDER BY ug.name",'Usergroups:Usergroups_x_User');
echo "<p><span class=emph>Public Groups:</span>\n";

if (count($rows) > 0) {
	echo "<ol style='margin-left:5px'>\n";
	foreach($rows as $row) {	
		$ugid = $row['id'];
		$ugname = $row['name'];
		echo "<li  style='font-weight:bold;color:black;' onclick=\"showinfo('$ugid','$userid')\">$ugname</span><span id='group_$ugid' ></li>\n";	
	}
	echo "</ol>\n";
	echo "</p>\n";
	
}
else {
	echo "<br/><span style='margin-left:5px'>You are not a member of any public usergroups</p>";
}

echo "</div>\n";
