<div class=section>
<h3>Documentation : API-based import </h3>
<h4>1. General</h4>
<p>This is still under beta functionality and for use in local installations only!</p>

<h4>2. Prerequisites</h4>
<ul>
<li>The galaxy-ftp server must be available as a local mount point.</li>
<li>ftp folder must be writable by the executing user.</li>
</ul>

<h4>3. The script</h4>
<div class=block><div class=code>Code</div>
<pre>
<?php
include("api/Example_Scripts/Import_To_VariantDB.py");
?>
</pre>
</div>
<h4>4. Usage</h4>
Parameters:<ul>
<li>Mandatory: -k : api-key of administrator user (can import for others), or of user (import into this user account)</li>
<li>Mandatory: -p : Project name.  (string).</li>
<li>Mandatory: -s : Sample Sheet (path).</li>
<li>Optional : -u : Import as user. This is only allowed for admin users on VariantDB. (email)</li>
</ul></p>
<p>Format of SampleSheet file (tab seperated, no header):<ul>
<li>sample name</li>
<li>gender (Male/Female/[undef])</li>
<li>path_to_VCF</li>
<li>path_to_BAM</li>
<li>store data in VariantDB ([0]/1)</li>
<li>formats:<ul>
<li>UG : Unified Genotyper</li>
<li>HC : Haplotype Caller</li>
<li>VS : Varscan</li>
<li>MT : MuTect</li>
<li>23 : 23 and Me, converted to VCF</li>
<li>IT : Ion Torrent Variant Caller</li>
</ul></li></ul>
</p>
</div>
