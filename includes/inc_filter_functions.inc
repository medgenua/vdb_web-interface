<?php

///////////////
// FUNCTIONS //
//////////////
// get top first level items by category. 
function GetSets($category, $params, $index)
{
    global $configuration;
    global $uid;
    global $sid;
    $return = array('status' => 'ok', 'result' => '');
    $settings = get_value_by_path($configuration, "FilterConfiguration/$category");
    // check for something else than options and settings.
    $sublevels = 0;
    foreach ($settings as $subkey => $setting) {
        if (!is_numeric($subkey)) {
            continue;
        }
        if ($setting['name'] != 'settings' && $setting['name'] != 'options') {
            $sublevels = 1;
            break;
        }
    }

    // special "custom fields" filter
    if ($category == 'Custom_VCF_Fields') {
        // only works for sample & project based filtering.
        if ($sid == 'project') {
            $rows = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$pid'", "Projects_x_Samples");
            $sid = '';
            foreach ($rows as $k => $row) {
                $sid .= $row['sid'] . ",";
            }
            if ($sid == '') {
                $return['status'] = 'skip';
                $return['result'] = 'no samples in this project. filter disabled: ';
                return ($return);
            }
            $sid = substr($sid, 0, -1);
        }
        // custom fields available? 
        $cf = runQuery("SELECT ca.aid, ca.field_name FROM `Custom_Annotations` ca JOIN `Custom_Annotations_x_Samples` cas ON ca.aid = cas.aid WHERE cas.sid IN ($sid) GROUP BY ca.field_name", "Custom_Annotations:Custom_Annotations_x_Samples");
        if (count($cv) == 0) {
            $return['status'] = 'skip';
            $return['result'] = 'No custom vcf fields available for this sample. ';
            return ($return);
        }
        $return['result'] .= "<select onchange=\"getArguments('$category','$index')\" id='Selection$index'>";
        foreach ($cf as $k => $r) {
            $select = '';
            if ($r['aid'] == $params) {
                $select = 'SELECTED';
            }
            $return['result'] .= "<option value='" . $r['aid'] . "' $select>" . $r['field_name'] . "</option>";
        }
        $return['result'] .=  "</select>";
        return ($result);
    }
    // sublevels == 1 : eg : Effect_on_transcript => Refseq, ensembl, ...
    elseif ($sublevels == 1) {
        // only return the select options. 
        $return['result'] .= "<select onchange=\"getArguments('$category','$index')\" id='Selection$index'>";
        $selected = 0;
        $out = '';
        foreach ($settings as $subkey => $setting) {
            if (!is_numeric($subkey)) {
                continue;
            }
            // check for license entry in the filter row.
            $name = $setting['name'];
            $deprecated = 0;
            // skip if deprecated AND param not set (== not from saved filter).
            if (get_value_by_path($configuration, "FilterConfiguration/$category/$name/deprecated")) {
                $deprecated = 1;
            }
            $license = get_value_by_path($configuration, "FilterConfiguration/$category/$name/license");
            if ($license && $license['value'] != '') {
                $license_rows = runQuery("SELECT l.lid FROM `License` l JOIN `Users_x_License` ul ON ul.lid = l.lid WHERE ul.uid = '$uid' AND l.LicenseName = '" . $license['value'] . "'", "License:Users_x_License");
                if (count($license_rows) == 0) {
                    //license failed. skip filter.
                    continue;
                }
            }
            $txtname = str_replace('null', '', $name);
            $txtname = str_replace('___', '_(', $txtname);
            $txtname = str_replace('__', ')', $txtname);
            $txtname = str_replace('_', ' ', $txtname);
            if ($txtname == '') {
                $txtname = '.';
            }
            //if (in_array($name,$params)) {
            // show deprecated filters if loaded from saved/cookie (params is filled in).

            if ($name == $params) {
                $return['result'] .= "<option selected value='$name'>$txtname</option>";
                $selected = 1;
            } else {
                if ($deprecated == 1) {
                    continue;
                } else {
                    $return['result'] .= "<option value='$name'>$txtname</option>";
                }
            }
        }
        if ($selected == 0) {
            $return['result'] .= "<option value='null' selected disabled='disabled'></option>\n";
        }
        $return['result'] .= "</select>";
        return ($return);
    } else {
        $arguments = get_value_by_path($configuration, "FilterConfiguration/$category/options");
        // process attributes.
        $attributes = get_value_by_path($configuration, "FilterConfiguration/$category/settings");
        foreach ($attributes as $key => $array) {
            if (!is_numeric($key)) {
                continue;
            }
            // allow multi?
            $multiple = '';
            if ($array['name'] == 'allowmulti' && $array['value'] == 'true') {
                $multiple = "MULTIPLE SIZE=";
            }
        }
        // return the selection list.
        $output = '';
        if ($multiple == '') {
            $output .= "<option value='null' selected disabled='disabled'></option>\n";
        }
        $options = 0;
        foreach ($arguments as $subkey => $setting) {
            if (!is_numeric($subkey)) {
                continue;
            }
            $options++;
            $name = $setting['name'];
            $txtname = str_replace('null', '', $name);
            $txtname = str_replace('_', ' ', $txtname);
            $txtname = str_replace('___', '_(', $txtname);
            $txtname = str_replace('__', ')', $txtname);
            if ($txtname == '') {
                $txtname = '.';
            }
            //if (in_array($name,$params)) {
            if ($name == $params) {
                $output .= "<option selected value='$name'>$txtname </option>";
            } else {
                $output .= "<option value='$name'>$txtname </option>";
            }
        }
        if ($options >= 4 && $multiple != '') {
            $multiple .= 4;
        } elseif ($options > 0 && $multiple != '') {
            $multiple .= $options;
        }
        $return['result'] .= "<select onchange=\"getArguments('$category','$index')\" id='Selection$index' $multiple>$output</select>";
        // no output yet
        if ($options == 0) {
            // perhaps they come from query (ArgumentsFromQuery and ArgumentQuery attributes of the parameter)
            $attributes = get_value_by_path($configuration, "FilterConfiguration/$category/settings");
            $argfromquery = 0;
            $query = '';
            foreach ($attributes as $key => $array) {
                if (!is_numeric($key)) {
                    continue;
                }
                if ($array['name'] == 'ArgumentsFromQuery' && $array['value'] == 'true') {
                    $argfromquery = 1;
                } elseif ($array['name'] == 'ArgumentQuery') {
                    $query = $array['value'];
                }
            }
            ## then get needed values
            if ($argfromquery == 1 && $query != '') {
                $query = str_replace('%sample', $sid, $query);
                $query = str_replace('%userid', $uid, $query);

                // extract tables.
                $pieces = explode(" ", $query);
                $table = '';
                foreach ($pieces as $k => $value) {
                    if (substr($value, 0, 1) == '`' && substr($value, -1) == '`') {
                        $table .= substr($value, 1, -1) . ":";
                    }
                }
                $table = substr($table, 0, -1);
                $result = runQuery($query, $table);
                if (count($result) == 0) {
                    $return['result'] = "No options available.";
                } else {
                    $output = '';
                    if ($multiple == '') {
                        $output .= "<option value='null' selected disabled='disabled'></option>\n";
                    } elseif (count($result) >= 4) {
                        $multiple .= 4;
                    } else {
                        $multiple .= (count($result) + 1);
                    }
                    foreach ($result as $k => $row) {
                        if ($row['Name'] == '') {
                            $row['Name'] = '.';
                        }
                        if (in_array($row['id'], $params)) {
                            $output .= "<option selected value='" . $row['id'] . "'>" . $row['Name'] . "</option>";
                        } else {
                            $output .= "<option value='" . $row['id'] . "'>" . $row['Name'] . "</option>";
                        }
                    }
                    $return['result'] .= "<select onchange=\"getValues('$category','$parameter','$index')\" id='Argument$index' $multiple>$output</select>";
                }
            } else {
                // no options, no query, but maybe a 'need' field. => add this field
                $needfound = 0;
                foreach ($attributes as $key => $array) {
                    if (!is_numeric($key)) {
                        continue;
                    }
                    if ($array['name'] == 'need') {
                        $needfound = 1;
                        $need = $array['value'];
                        switch ($need) {
                            case 'text':
                                $output = "<input onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' type='text' value='$values' size='15' id='Value$index' />";
                                break;
                            case 'SelectFromQuery':
                                $query = $attributes['selectquery'];
                                $output = $query;
                                break;
                            case 'SepQuery_Options':
                                $output = "<select onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' id='Value$index' title='$help'>";
                                $sqopts = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/SQ_options");
                                $nrsqopts = 0;
                                foreach ($sqopts as $subkey => $setting) {
                                    if (!is_numeric($subkey)) {
                                        continue;
                                    }
                                    $nrsqopts++;
                                    $name = $setting['name'];
                                    $txtname = str_replace('null', '', $name);
                                    $txtname = str_replace('___', '_(', $txtname);
                                    $txtname = str_replace('__', ')', $txtname);
                                    $txtname = str_replace('_', ' ', $txtname);
                                    if ($txtname == '') {
                                        $txtname = '.';
                                    }

                                    $output .= "<option value='" . $setting['value'] . "'>$txtname </option>";
                                }
                                if ($nrsqopts == 0) {
                                    $output = 'ERROR: No options found.';
                                } else {
                                    $output .= "</select>";
                                }
                                break;

                            default;
                                $output = "needed field '$need' not recognised, contact sytem admin!";
                                break;
                        }
                    }
                }
                if ($needfound == 0) {
                    $output = "--";
                }
                $return['result'] = $output;
            }
        }
    }

    return ($return);
}

// subselect fields
function GetSubSelect($category, $parameter, $index, $ssqv)
{
    global $configuration;
    global $uid;
    global $sid;
    $return = array('status' => 'ok', 'result' => '');
    $settings = get_value_by_path($configuration, "FilterConfiguration/$category");
    if (strpos($ssqv, '__') !== false) {
        $ssqvs = explode("__", $ssqv);
    } else {
        $ssqvs = explode("@@@", $ssqv);
    }
    $out = '';
    if ($ss = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/settings/subselect")) {
        // mysql query to get options.
        if (isset($ss['value'])) {
            $ssq = $ss['value'];
            $ssq = str_replace('%sample', $sid, $ssq);
            $ssq = str_replace('%userid', $uid, $ssq);
            $out .= "$ssq";
            // extract tables.
            $pieces = explode(" ", $ssq);
            $table = '';
            foreach ($pieces as $k => $value) {
                if (substr($value, 0, 1) == '`' && substr($value, -1) == '`') {
                    $table .= substr($value, 1, -1) . ":";
                }
            }
            $table = substr($table, 0, -1);
            $result = runQuery($ssq, $table);
            if (count($result) == 0) {
                $out = " (No options available.)";
            } else {

                $multiple = 'MULTIPLE SIZE=';
                if (count($result) >= 4) {
                    $multiple .= 4;
                } else {
                    $multiple .= (count($result) + 1);
                }
                foreach ($result as $k => $row) {
                    if ($row['qValue'] == '') {
                        continue;
                    }
                    if ($row['qName'] == '') {
                        $row['Name'] = '.';
                    }
                    if (in_array($row['qValue'], $ssqvs)) {
                        $out .= "<option SELECTED value='" . $row['qValue'] . "'>" . $row['qName'] . "</option>";
                    } else {
                        $out .= "<option value='" . $row['qValue'] . "'>" . $row['qName'] . "</option>";
                    }
                }
                $out = " Pick: <select id='ssqv$index' $multiple style='vertical-align:middle;'>$out</select>";
            }
        }
        // list with options.
        else {
            $output = "<select onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' id='ssqv$index'>";
            $nrsqopts = 0;
            foreach ($ss as $subkey => $setting) {
                if (!is_numeric($subkey)) {
                    continue;
                }
                $nrsqopts++;
                $name = $setting['name'];
                $txtname = str_replace('null', '', $name);
                $txtname = str_replace('___', '_(', $txtname);
                $txtname = str_replace('__', ')', $txtname);
                $txtname = str_replace('_', ' ', $txtname);
                if ($txtname == '') {
                    $txtname = '.';
                }
                $selected = (in_array($setting['value'], $ssqvs)) ? "selected" : '';
                $output .= "<option value='" . $setting['value'] . "' $selected>$txtname </option>";
            }
            if ($nrsqopts == 0) {
                $output = 'ERROR: No options found.';
            } else {
                $output .= "</select>";
            }
            if ($lab = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/settings/subselect_label")) {
                $out = "&nbsp; " . $lab['value'] . ": $output";
            } else {
                $out = "&nbsp; Select subset: $output";
            }
        }
        $return['result'] = $out;
    }
    // else { skip? }
    return ($return);
}

// get second layer info : arguments.
function GetArguments($category, $parameter, $arg, $values, $index)
{
    global $configuration;
    global $uid;
    global $sid;
    $return = array('status' => 'ok', 'result' => '', 'skip_values' => 0);
    $settings = get_value_by_path($configuration, "FilterConfiguration/$category");
    if (strpos($arg, '__') !== false) {
        $args = explode('__', $arg);
    } else {
        $args = explode('@@@', $arg);
    }
    $varray = array($values);
    $arguments = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/options");
    $attributes = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/settings");
    $multiple = '';
    if (is_array($attributes)) {
        foreach ($attributes as $key => $array) {
            if (!is_numeric($key)) {
                continue;
            }
            // allow multi?
            if ($array['name'] == 'allowmulti' && $array['value'] == 'true') {
                $multiple = "MULTIPLE SIZE=";
                $varray = explode(",", $values);
                break;
            }
        }
    }
    // help available ? 
    $help = '';
    if (is_array($attributes)) {
        foreach ($attributes as $key => $array) {
            if (!is_numeric($key)) {
                continue;
            }
            if ($array['name'] == 'help') {
                $help = $array['value'];
                break;
            }
        }
    }
    // return the selection list.
    $output = '';
    if ($multiple == '') {
        $output .= "<option value='null' selected disabled='disabled'></option>\n";
    }
    $options = 0;
    if (is_array($arguments)) {
        foreach ($arguments as $subkey => $setting) {
            if (!is_numeric($subkey)) {
                continue;
            }
            $options++;
            $name = $setting['name'];
            $txtname = str_replace('null', '', $name);
            $txtname = str_replace('___', '_(', $txtname);
            $txtname = str_replace('__', ')', $txtname);
            $txtname = str_replace('_', ' ', $txtname);
            if ($txtname == '') {
                $txtname = '.';
            }
            if (in_array($name, $args)) {
                $output .= "<option selected value='$name'>$txtname </option>";
            } else {
                $output .= "<option value='$name'>$txtname </option>";
            }
        }
    }
    if ($options >= 4 && $multiple != '') {
        $multiple .= 4;
    } elseif ($options > 0 && $multiple != '') {
        $multiple .= $options;
    }
    $output = "<select title='$help' onchange=\"getValues('$category','$parameter','$index')\" id='Argument$index' $multiple>$output</select>";
    if ($options == 0) {
        // perhaps they come from query (ArgumentsFromQuery and ArgumentQuery attributes of the parameter)
        //if (array_key_exists('attributes',$arguments)) {
        //$attributes = get_value_by_path($configuration,"FilterConfiguration/$category/$parameter/settings");
        $argfromquery = 0;
        $query = '';
        if (is_array($attributes)) {
            foreach ($attributes as $key => $array) {
                if (!is_numeric($key)) {
                    continue;
                }
                if ($array['name'] == 'ArgumentsFromQuery' && $array['value'] == 'true') {
                    $argfromquery = 1;
                } elseif ($array['name'] == 'ArgumentQuery') {
                    $query = $array['value'];
                }
            }
        }
        ## then get needed values
        if ($argfromquery == 1 && $query != '') {
            $query = str_replace('%sample', $sid, $query);
            $query = str_replace('%userid', $uid, $query);
            // extract tables.
            $pieces = explode(" ", $query);
            $table = '';
            foreach ($pieces as $k => $value) {
                if (substr($value, 0, 1) == '`' && substr($value, -1) == '`') {
                    $table .= substr($value, 1, -1) . ":";
                }
            }
            $table = substr($table, 0, -1);
            $result = runQuery($query, $table);
            if (count($result) == 0) {
                $output = "No options available.";
            } else {

                $output = '';
                if ($multiple == '') {
                    $output .= "<option value='null' selected disabled='disabled'></option>\n";
                } elseif (count($result) >= 4) {
                    $multiple .= 4;
                } else {
                    $multiple .= (count($result) + 1);
                }
                foreach ($result as $k => $row) {
                    if ($row['id'] == '') {
                        continue;
                    }
                    if ($row['Name'] == '') {
                        $row['Name'] = '.';
                    }
                    if (in_array($row['id'], $args)) {
                        $output .= "<option selected value='" . $row['id'] . "'>" . $row['Name'] . "</option>";
                    } else {
                        $output .= "<option value='" . $row['id'] . "'>" . $row['Name'] . "</option>";
                    }
                }
                $output = "<select title='$help' onchange=\"getValues('$category','$parameter','$index')\" id='Argument$index' $multiple>$output</select>";
            }
        } else {
            // no options, no query, but maybe a 'need' field. => add this field
            $needfound = 0;
            if (is_array($attributes)) {
                foreach ($attributes as $key => $array) {
                    if (!is_numeric($key)) {
                        continue;
                    }
                    if ($array['name'] == 'need') {
                        $needfound = 1;
                        $need = $array['value'];
                        switch ($need) {
                            case 'text':
                                $output = "<input onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' title='$help' type='text' value='$values' size='15' id='Value$index' />";
                                break;
                            case 'SelectFromQuery':
                                $query = $attributes['selectquery'];
                                $output = $query;
                                break;
                            case 'SepQuery_Options':
                                $output = "<select onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' id='Value$index' title='$help'>";
                                $sqopts = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/SQ_options");
                                $nrsqopts = 0;
                                foreach ($sqopts as $subkey => $setting) {
                                    if (!is_numeric($subkey)) {
                                        continue;
                                    }
                                    $nrsqopts++;
                                    $name = $setting['name'];
                                    $txtname = str_replace('null', '', $name);
                                    $txtname = str_replace('___', '_(', $txtname);
                                    $txtname = str_replace('__', ')', $txtname);
                                    $txtname = str_replace('_', ' ', $txtname);
                                    if ($txtname == '') {
                                        $txtname = '.';
                                    }
                                    $optselected = '';
                                    if (in_array($setting['value'], $varray)) {
                                        $optselected = 'selected';
                                    }
                                    $output .= "<option value='" . $setting['value'] . "' $optselected>$txtname</option>";
                                }
                                if ($nrsqopts == 0) {
                                    $output = 'ERROR: No options found.';
                                } else {
                                    $output .= "</select>";
                                }
                                break;

                            default;
                                $output = "needed field '$need' not recognised, contact sytem admin!";
                                break;
                        }
                    }
                }
            }
            if ($needfound == 0) {
                $output = "--";
            } else {
                $return['skip_values'] = 1;
            }
        }
    }
    $return['result'] = $output;
    return ($return);
}

// Dynamic Filters
function GetDynamic($category, $parameter, $stored_options, $index)
{
    global $configuration;
    global $uid;
    global $sid;
    $return = array('status' => 'ok', 'result' => '');
    $settings = get_value_by_path($configuration, "FilterConfiguration/$category");

    // help available ? 
    $help = '';
    $harray = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/settings");
    if (is_array($harray)) {
        foreach ($harray as $key => $array) {
            if (!is_numeric($key)) {
                continue;
            }
            if ($array['name'] == 'help') {
                $help = $array['value'];
                break;
            }
        }
    }
    // first level.
    $first = GetSets($category, $parameter, $index);
    $return['first'] = $first['result'];
    // second level.
    $options = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/options");
    //trigger_error("checking second level with stored options:");
    //trigger_error(json_encode($stored_options));
    //trigger_error("xml options are:");
    //trigger_error(json_encode($options));
    $second = '';
    if (is_array($options)) {
        $oo = array();
        $ooidx = 0;
        foreach ($options as $key => $array) {
            if (!is_numeric($key)) {
                continue;
            }
            $ooidx++;
            $option = $array['name'];
            // check DB for stored value.
            if (isset($stored_options[$option])) {
                $dbval = $stored_options[$option];
            } else {
                $dbval = 'undefined';
            }
            //trigger_error("dbval => $dbval");
            $oarray = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/options/$option");
            $text_val = 0;
            foreach ($oarray as $okey => $oval) {
                if (!is_numeric($okey)) {
                    continue;
                }
                if ($oval['name'] == 'type') {
                    if ($oval['value'] == 'checkbox') {
                        $checked = '';
                        if ($dbval == 1) {
                            $checked = 'checked';
                        }
                        $option .= ": <input type=hidden id='dyn_op$index" . "_$ooidx" . "_name' value='$option' />"; // hidden variable with option name
                        $option .= "<input onclick='FiltersToCookies($index)' type='checkbox' name='dyn_op$index" . "_$ooidx" . "_value' id='dyn_op$index" . "_$ooidx" . "_value' value='1' $checked />"; // actual input
                        break;
                    } elseif ($oval['value'] == 'text') {
                        $option .= ": <input type=hidden id='dyn_op$index" . "_$ooidx" . "_name' value='$option' />"; // hidden variable with option name
                        $option .= "<input  onkeyup='SetLoaded()' onchange='SetLoaded();FiltersToCookies($index)' type=text size=4 name='dyn_op$index" . "_$ooidx" . "_value' id='dyn_op$index" . "_$ooidx" . "_value' value='%textval%' />";
                    } elseif ($oval['value'] == 'select_multi') {
                        // dbval holds selected items => convert to array.
                        $dbvals = explode("__", $dbval);
                        // needed? $dbvals = explode("@@@",$dbval);
                        $option .= ": <input type=hidden id='dyn_op$index" . "_$ooidx" . "_name' value='$option' />";
                        $option .= "<select style='vertical-align:middle;' multiple='multiple' name='dyn_op$index" . "_$ooidx" . "_value' id='dyn_op$index" . "_$ooidx" . "_value' onkeyup='SetLoaded()' onchange='SetLoaded();FiltersToCookies($index)' >";
                        foreach (get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/options/Select/select_option") as $sokey => $soval) {
                            if (!is_numeric($sokey)) {
                                continue;
                            }
                            if (in_array($soval['name'], $dbvals)) {
                                $selected = "selected='selected'";
                            } else {
                                $selected = '';
                            }
                            $option .= "<option value='" . $soval['name'] . "' $selected>" . $soval['value'] . "</option>";
                        }
                        $option .= "</select>";
                    }
                } elseif ($oval['name'] == 'value') {
                    $text_val = $oval['value'];
                }
            }
            if ($dbval != 'undefined') {
                $text_val = $dbval;
            }
            $option = str_replace('%textval%', $text_val, $option);
            array_push($oo, $option);
        }

        $second .= "<span title='$help'>";
        $second .=  "<input type=hidden id='nr_dyn_op$index' value='$ooidx' />";
        $second .=  implode("<br/>", $oo);
        if ($ooidx == 0) {
            $second .=  "-hover for help-";
        }
        $second .=  "</span>";
    } else {
        $second .=  "--";
    }
    $return['second'] = $second;
    return ($return);
}


// custom VCF fields
function GetCustom($category, $parameter, $arguments, $values, $index)
{
    global $configuration;
    global $uid;
    global $sid;
    if (strpos($_GET['args'], '__') !== false) {
        $args = explode('__', $_GET['args']);
    } else {
        $args = explode('@@@', $_GET['args']);
    }
    $values = $_GET['values'];
    $varray = array($values);
    // the annotation ID is in param. Get some details.
    $r = runQuery("SELECT `value_type` FROM `Custom_Annotations`  WHERE aid = '$parameter'", "Custom_Annotations")[0];
    $type = $r['value_type'];
    // split by type.
    if ($type == 'list') {
        // get options.
        $opts = runQuery("SELECT `field_value`,`field_code` FROM `Custom_Annotations_x_Value_Codes` WHERE aid = '$parameter' ORDER BY `field_value`", "Custom_Annotations_x_Value_Codes");
        $options = 0;
        foreach ($opts as $k => $r) {
            $options++;
            if (in_array($r['field_code'], $args)) {
                $output .= "<option selected value='" . $r['field_code'] . "'>" . $r['field_value'] . "</option>";
            } else {
                $output .= "<option value='" . $r['field_code'] . "'>" . $r['field_value'] . "</option>";
            }
        }
        if ($options >= 4) {
            $multiple = 4;
        } elseif ($options > 0) {
            $multiple = $options;
        }
        $output = "<select title='Parameter imported from VCF.' onchange=\"getValues('$category','$parameter','$index')\" id='Argument$index' MULTIPLE SIZE='$multiple'>$output</select>";
        echo $output;
        exit;
    } elseif ($type == 'decimal' || $type == 'integer') {
        // aritmetic options.

        //$output = "<option value='null' disabled='disabled'></option>\n"; 
        $selected = '';
        $sel = 0;
        if (in_array('Equal', $args)) {
            $selected = 'selected';;
            $sel = 1;
        }
        $output .= "<option $selected value='Equal'>Equal</option>";
        $selected = '';
        if (in_array('Smaller_Than', $args)) {
            $selected = 'selected';;
            $sel = 1;
        }
        $output .= "<option $selected value='Smaller_Than'>Smaller Than</option>";
        $selected = '';
        if (in_array('Smaller_Or_Equal_Than', $args)) {
            $selected = 'selected';;
            $sel = 1;
        }
        $output .= "<option $selected value='Smaller_Or_Equal_Than'>Smaller Or Equal Than</option>";
        $selected = '';
        if (in_array('Bigger_Than', $args)) {
            $selected = 'selected';;
            $sel = 1;
        }
        $output .= "<option $selected value='Bigger_Than'>Bigger Than</option>";
        $selected = '';
        if (in_array('Bigger_Or_Equal_Than', $args)) {
            $selected = 'selected';
            $sel = 1;
        }
        $output .= "<option $selected value='Bigger_Or_Equal_Than'>Bigger Or Equal Than</option>";
        if ($sel == 0) {
            $output =  "<option value='null' selected disabled='disabled'></option>$output\n";
        }
        echo "<select title='Parameter imported from VCF.' onchange=\"getValues('$category','$parameter','$index')\" id='Argument$index'>$output</select>";
        exit;
    } elseif ($type == 'varchar') {
        // textfield.
        echo "<input  title='Parameter imported from VCF. Use percentage for wildcard matching'  value='$values' onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' type='text' size='15' id='Value$index' />";
        exit;
    }
}
function GetCustomValues($category, $parameter, $values, $item, $index)
{
    global $configuration;
    global $sid;
    global $uid;
    $return = array('status' => 'ok', 'result' => '');
    $settings = get_value_by_path($configuration, "FilterConfiguration/$category");
    $varray = array($values);
    // the annotation ID is in param. Get some details.
    $r = runQuery("SELECT `value_type` FROM `Custom_Annotations`  WHERE aid = '$parameter'", "Custom_Annotations")[0];
    $type = $r['value_type'];
    if ($type == 'integer' || $type == 'decimal') {
        // need a textfield.
        $return['result'] = "<input onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' title='Parameter imported from VCF. Numeric value expected' type='text' value='$values' size='15' id='Value$index' />";
    } else {
        $return['result'] = "--";
    }
    return ($return);
}


function GetValues($category, $parameter, $values, $index)
{
    global $configuration;
    global $uid;
    global $sid;
    $return = array('status' => 'ok', 'result' => '');
    $settings = get_value_by_path($configuration, "FilterConfiguration/$category");
    $varray = array($values);
    $arguments = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/options");
    $attributes = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/settings");
    $output = '';
    // help available ? 
    $help = '';
    if (is_array($attributes)) {
        foreach ($attributes as $key => $array) {
            if (!is_numeric($key)) {
                continue;
            }
            if ($array['name'] == 'help') {
                $help = $array['value'];
                continue;
            }
            if ($array['name'] == 'allowmulti' && $array['value'] == 'true') {
                $varray = explode(",", $values);
                continue;
            }
        }
    }
    // need some field ?
    if (is_array($attributes)) {
        foreach ($attributes as $key => $array) {
            if (!is_numeric($key)) {
                continue;
            }
            if ($array['name'] == 'need') {
                $need = $array['value'];
                switch ($need) {
                    case 'text':
                        $output = "<input onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' title='$help' type='text' value='$values' size='15' id='Value$index' />";
                        break;
                    case 'SelectFromQuery':
                        $query = $attributes['selectquery'];
                        $output = $query;
                        break;
                    case 'SepQuery_Options':
                        // there is a problem here, that values get split, although it's not allowed for values. (eg in_parents as any genotype has values 0,1,2)
                        // hence, put values as is back in array.
                        $varray = array($values);
                        $output = "<select onkeyup='SetLoaded()' onChange='SetLoaded();FiltersToCookies($index)' id='Value$index' title='$help'>";
                        $sqopts = get_value_by_path($configuration, "FilterConfiguration/$category/$parameter/SQ_options");
                        $nrsqopts = 0;
                        foreach ($sqopts as $subkey => $setting) {
                            if (!is_numeric($subkey)) {
                                continue;
                            }
                            $nrsqopts++;
                            $name = $setting['name'];
                            $txtname = str_replace('null', '', $name);
                            $txtname = str_replace('___', '_(', $txtname);
                            $txtname = str_replace('__', ')', $txtname);
                            $txtname = str_replace('_', ' ', $txtname);
                            if ($txtname == '') {
                                $txtname = '.';
                            }
                            $optselected = '';
                            if (in_array($setting['value'], $varray)) {
                                $optselected = 'selected';
                            }
                            // TODO fix the undefined 'value' here.

                            $output .= "<option value='" . $setting['value'] . "' $optselected>$txtname </option>";
                        }
                        if ($nrsqopts == 0) {
                            $output = 'ERROR: No options found.';
                        } else {
                            $output .= "</select>";
                        }
                        break;
                    default;
                        $output = "needed field '$need' not recognised, contact sytem admin!";
                        break;
                }
            }
        }
    }
    // return the selection list.
    $return['result'] = $output;
    return ($return);
}
