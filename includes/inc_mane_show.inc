<?php
// setid set?
if (!isset($_GET['setid']) || $_GET['setid'] == '' || !is_numeric($_GET['setid'])) {
    echo "<div class=section>";
    echo "<h3>Invalid Set ID</h3>";
    echo "<p>The specified panel-id was not valid.</p>";
    echo "</div>";
    exit;
}
$setid = $_GET['setid'];
//allowed?
$rows = runQuery("SELECT `edit` FROM `TranscriptSets_x_Users` WHERE `set_id` = '$setid' AND `uid` = '$userid'", "TranscriptSets_x_Users");
if (count($rows) == 0) {
    echo "<div class=section>";
    echo "<h3>Insufficient Privileges</h3>";
    echo "<p>You do not have access to this set.</p>";
    echo "</div>";
    exit;
}


// print current state.

$rows = runQuery("SELECT t.`name`, u.`LastName`, u.`FirstName` FROM `TranscriptSets` t JOIN `Users` u ON t.`owner` = u.`id` WHERE t.`set_id` = '$setid' ", "TranscriptSets:Users")[0];
$setname = $rows['name'];
$setowner = $rows['LastName'] . " " . $rows['FirstName'];
echo "<div class=section>";
echo "<h3>Set Details: $setname</h3>";
echo "<p><span class=emph>Owner:</span> $setowner<br/>";
echo "<span class=emph>Set Name: </span>$setname<br/>";
echo "</p><p><span class=emph>Included Transcripts:</span> <br/>";
echo "<table style='width:90%;border-bottom:1px solid #333;margin-left:1em;' id=genetable cellspacing=0 ><tr><th class=top>Transcript ID</th><th class=top>Comment</th><th class=top>Entry Type <img src='Images/layout/icon_info.gif' style='height:0.9em' title='Extra: Add transcript to NCBI/Select; Discard: Remove from NCBI/Select'/> </th></tr>";
// get transcripts
$rows = runQuery("SELECT `nm_id`, `Comments`, `favourite` FROM `TranscriptSets_Data` WHERE `set_id` = '$setid' ", "TranscriptSets_Data");
foreach ($rows as $key => $row) {
    // one line per transcript, linked to javascript for updates.
    $nm_id = $row['nm_id'];
    $line = "<tr id='row[" . $nm_id . "]'>";
    $line .= "<td>$nm_id</td>";
    $line .= "<td>" . $row['Comments'] . "</td>";

    if ($row['favourite'] == 1) {
        $line .= "<td>Extra</td>";
    } elseif ($row['favourite'] == -1) {
        $line .= "<td>Discard</td>";
    }
    $line .= "</tr>";
    echo $line;
}
echo "</table>";

echo "</div>";
