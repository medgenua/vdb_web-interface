<div class=section>
    <h3>Documentation : API access to VariantDB</h3>

    <div style='border:red solid 1pt'><span class=emph style='color:red'>DISCLAIMER</span> The VariantDB-API is under active development. Documentation is most likely not completely up-to-date.</div>

    <p>The API is designed to provide remote access to the filtering functions of VariantDB. Results are provided in JSON format. There is currently no functionality to change anything using the API. </p>

    <h4 class=underline>Preparation: Obtaining an API key</h4>
    <p class=indent>API access requires a valid api key. To obtain this key, go to the <a href='index.php?page=personal' target='_blank'>"My Details"</a> page (Accessible by clicking on your username in the top right corner). Check the box to create an API key and press 'Submit'. The presented API-key is a random string of 32-characters, and is valid for 1 week by default. Optionally, an API-key can be made valid indefinitely and can be disabled on demand.</p>

    <p class=indent>The API-key must be provided on all API calls as follows: </p>

    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/&lt;api-command&gt;?apiKey=&lt;YOUR-API-KEY&gt;
    </div>

    <p class=indent><span class=underline>NOTE:</span> Do not share your api key, as it represents your login credentials. Once the API will support editing of data, sharing the key can have serious consequences.</p>


    <h4 class=underline>API Command 'SavedFilters'</h4>
    <p class=indent>If no filter-id is provided, this method gets all stored filters. The result contains the following items for each stored filter:
    <ul>
        <li><span class=strong>fid</span>: Numeric Filter-ID. </li>
        <li><span class=strong>FilterName</span>: The name associated to the filterset</li>
        <li><span class=strong>Comments</span>: Optional extra information provided when saving the filterset</li>
        <li><span class=strong>nrRules</span>: The number of filtering criteria included in the filterset</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/SavedFilters?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>[{"fid":"122","FilterName":"Simple_Quality","Comments":"Contains a few Quality Metrics","nrRules":3},{"fid":"126","FilterName":"effects_or","Comments":"Filters on either ensembl or refseq effects","nrRules":2},{"fid":"128","FilterName":"ClinVar_exact","Comments":"Provdes exact ClinVar matches","nrRules":1}]
    </div>

    <h4 class=underline>API Command 'SavedFilters/&lt;filter_id&gt;'</h4>
    <p class=indent>Retrieve full details on the specified filter. Returned items are:
    <ul>
        <li><span class=strong>FilterName</span>: The name associated to the filterset</li>
        <li><span class=strong>Comments</span>: Optional extra information provided when saving the filterset</li>
        <li><span class=strong>Created</span>: Date that filterset was saved in the system</li>
        <li><span class=strong>FilterSettings</span>: String representation of filter details. Elements are delimited by '@@@', key-value pairs by '|||'. A filter rule consists of :<ol>
                <li>Category: First level section (family, ANNOvAR, ClinVar, ...)</li>
                <li>Negate: Include or exclude matching variants</li>
                <li>Param: Second level section (RefSeq_Variant_Type, RefSeq_Gene_Location, ...)</li>
                <li>Argument: Selected items in option-lists (exonic, intronic, 5'UTR, ...). These values are typically encoded to integers. See API 'GetValueCodes' for decoding options</li>
                <li>Value: Value provided by the user in a free text field (e.g. Minimal depth value)</li>
            </ol>
        </li>
        <li><span class=strong>FilterTree</span>: JSON encoded representation of the filtering logic. Note that this item is double-encoded, escaping all regular JSON characters.</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/SavedFilters/&lt;filter_id&gt;?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"FilterName":"effects_or","Created":"2015-02-18 18:30:25","FilterTree":"[{\\\"id\\\":\\\"j1_1\\\",\\\"text\\\":\\\"OR\\\",\\\"icon\\\":\\\"Images\/layout\/OR.png\\\",\\\"li_attr\\\":{\\\"id\\\":\\\"j1_1\\\",\\\"data-jstree\\\":\\\"{ \\\\\\\"opened\\\\\\\" : true, \\\\\\\"type\\\\\\\" : \\\\\\\"logic_AND\\\\\\\" }\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"j1_1_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":true,\\\"selected\\\":true,\\\"disabled\\\":false,\\\"type\\\":\\\"logic_AND\\\"},\\\"data\\\":{\\\"jstree\\\":{\\\"opened\\\":true,\\\"type\\\":\\\"logic_AND\\\"}},\\\"children\\\":[{\\\"id\\\":\\\"rule_13\\\",\\\"text\\\":\\\" Effect On Transcript: Match RefSeq GeneLocation: downstream, exonic, exonic\/splicing\\\",\\\"icon\\\":\\\"Images\/layout\/filter.png\\\",\\\"li_attr\\\":{\\\"id\\\":\\\"rule_13\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"rule_13_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{},\\\"children\\\":[],\\\"type\\\":\\\"default\\\"},{\\\"id\\\":\\\"rule_14\\\",\\\"text\\\":\\\" snpEff: Match Effect Impact: HIGH, MODERATE\\\",\\\"icon\\\":\\\"Images\/layout\/filter.png\\\",\\\"li_attr\\\":{\\\"id\\\":\\\"rule_14\\\"},\\\"a_attr\\\":{\\\"href\\\":\\\"#\\\",\\\"id\\\":\\\"rule_14_anchor\\\"},\\\"state\\\":{\\\"loaded\\\":true,\\\"opened\\\":false,\\\"selected\\\":false,\\\"disabled\\\":false},\\\"data\\\":{},\\\"children\\\":[],\\\"type\\\":\\\"default\\\"}],\\\"type\\\":\\\"logic_OR\\\"}]","Comments":"","FilterSettings":"category13|||Effect_On_Transcript@@@negate13|||@@@param13|||RefSeq_GeneLocation@@@argument13|||17__18__19__@@@category14|||snpEff@@@negate14|||@@@param14|||Effect_Impact@@@argument14|||123__125__"}
    </div>

    <h4 class=underline>API Command 'LoadFilter'</h4>
    <p class=indent>Import a JSON representation of a saved filter set. This method requires POST submission to the API. Posted variables are:
    <ul>
        <li><span class=strong>json_file</span>: Text file with json representation of the filter set. It's a dump of api-call SavedFilters/&lt;filter_id&gt</li>
        <li><span class=strong>apiKey</span>: Your api-userkey. </li>
    </ul>
    </p>
    <p class=indent>The method returns the ID of the newly created filter set (field: 'id'). See <a href='index.php?page=tutorial&topic=api_examples#LoadFilter'> example scripts </a> for more details.</p>


    <h4 class=underline>API Command 'SavedAnnotations'</h4>
    <p class=indent>If no annotationset-id is provided, this method gets all stored annotation sets. The result contains the following items for each stored set:
    <ul>
        <li><span class=strong>aid</span>: Numeric Annotation-ID. </li>
        <li><span class=strong>AnnotationName</span>: The name associated to the annotation set</li>
        <li><span class=strong>Comments</span>: Optional extra information provided when saving the set</li>
        <li><span class=strong>nrItems</span>: The number of items selected in the set</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/SavedAnnotations?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>[{"aid":"37","AnnotationName":"comprehensive","Comments":"","nrItems":82},{"aid":"38","AnnotationName":"Somatic_State","Comments":"Show ploidy and other oncology related stuff","nrItems":10}]
    </div>


    <h4 class=underline>API Command 'SavedAnnotations/&lt;annotation_id&gt;'</h4>
    <p class=indent>Retrieve full details on the specified annotation set. Returned items are:
    <ul>
        <li><span class=strong>AnnotationName</span>: The name associated to the annotation set</li>
        <li><span class=strong>Comments</span>: Optional extra information provided when saving the set</li>
        <li><span class=strong>Created</span>: Date that annotation set was saved in the system</li>
        <li><span class=strong>AnnotationSettings</span>: List of selected annotations. Items are delimeted by '@@@'. Each item consists of a &lt;Category&gt;;&lt;Annotation_Name&gt; combination.</li>

    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/SavedAnnotations/&lt;annotation_id&gt;?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"AnnotationName":"Somatic_State","Created":"2015-02-19 14:25:43","AnnotationSettings":"GATK_Annotations;AllelicRatio@@@GATK_Annotations;Alt_Allele_Depth@@@GATK_Annotations;DeltaPL @@@GATK_Annotations;Genotype@@@GATK_Annotations;Ploidy@@@GATK_Annotations;Ref_Allele_Depth@@@GATK_Annotations;Somatic_State@@@GATK_Annotations;Stretch_Length@@@GATK_Annotations;Stretch_Unit@@@GATK_Annotations;Tranches_Filter","Comments":"Show ploidy and other oncology related stuff"}
    </div>

    <h4 class=underline>API Command 'LoadAnnotations'</h4>
    <p class=indent>Import a JSON representation of a saved annotation set. This method requires POST submission to the API. Posted variables are:
    <ul>
        <li><span class=strong>json_file</span>: Text file with json representation of the annation set. It's a dump of api-call SavedAnnotations/&lt;annotation_id&gt</li>
        <li><span class=strong>apiKey</span>: Your api-userkey. </li>
    </ul>
    </p>
    <p class=indent>The method returns the ID of the newly created annotation set (field: 'id'). See <a href='index.php?page=tutorial&topic=api_examples#LoadAnnotation'> example scripts </a> for more details.</p>



    <h4 class=underline>API Command 'Samples'</h4>
    <p class=indent>Retrieve a list of all samples you have access to. Basic provided sample details include:
    <ul>
        <li><span class=strong>id</span>: Database identifier of the sample</li>
        <li><span class=strong>sample_name</span>: Provided sample name</li>
        <li><span class=strong>gender</span>: Either 'Male', 'Female', or 'Undef'</li>
        <li><span class=strong>project_name</span>: The name of the project in which the sample is stored.</li>
        <li><span class=strong>project_id</span>: Database identifier of the listed project.</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/Samples?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>[{"id":"1","sample_name":"sample3","gender":"Female","project_name":"2013.7.1_10:25:46","project_id":"3"},{"id":"3","sample_name":"sample1","gender":"Female","project_name":"2013.7.1_10:29:55","project_id":"4"},{"id":"4","sample_name":"haloplex","gender":"Undef","project_name":"2013.7.1_10:29:55","project_id":"4"},{"id":"5","sample_name":"sample2","gender":"Undef","project_name":"2013.7.1_10:29:55","project_id":"4"},{"id":"7","sample_name":"S1.affected.Index","gender":"Male","project_name":"2013.7.4_18:31:13","project_id":"6"},{"id":"9","sample_name":"F5_index","gender":"Undef","project_name":"2013.7.5_12:15:37","project_id":"8"}]
    </div>


    <h4 class=underline>API Command 'Samples/&lt;sample_id&gt;'</h4>
    <p class=indent>Retrieve full details on the specified sample. Returned items are:
    <ul>
        <li><span class=strong>id</span>: Database identifier of the sample</li>
        <li><span class=strong>sample_name</span>: Provided sample name</li>
        <li><span class=strong>gender</span>: Either 'Male', 'Female', or 'Undef'</li>
        <li><span class=strong>project_name</span>: The name of the project in which the sample is stored.</li>
        <li><span class=strong>project_id</span>: Database identifier of the listed project.</li>
        <li><span class=strong>project_created</span>: Date when project was initially created. This does not neccessarily reflect the creation date of the sample.</li>
        <li><span class=strong>clinical_freetext</span>: Provided free-text clinical information of the sample. </li>
        <li>
            <sapn class=strong>clinical_hpo</span>: Structured clinical information according to HPO. Included information per associated HPO term: <ul>
                    <li>hpo_id: HPO identifier (e.g. HP:0001263)</li>
                    <li>hpo_term: Short HPO name of the phenotype</li>
                    <li>hpo_definition: Definition of the phenotype according to HPO</li>
                    <li>hpo_comment: Further information regarding the term.</li>
                    <li>hpo_xref: External identifiers corresponding to the HPO-id</li>
                </ul>
        </li>
        <li><span class=strong>pedigree</span>: Sample relation to rebuild the pedigree. Contains a node (by sample_id) for each sample in the pedigree holding the following information<ul>
                <li>Replicates: The same sample, under additional ids</li>
                <li>Siblings: Brothers and/or sisters</li>
                <li>Parents: Father and/or Mother</li>
                <li>Children: If any, the children of the sample
                <li>
                <li>Each of the above is a list with sample_id, sample_name and sample_gender</li>
            </ul>
        </li>
        <li><span class=strong>nr_variants</span>: The number of variants for the current sample, without any applied filters.</li>
        <li><span class=strong>marked_as_finished</span>: 0/1. Samples can be marked as finished in VariantDB. </li>
        <li><span class=strong>control_sample</span>: 0/1. Indicates if sample is a control sample.</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/Samples/&lt;sample_id&gt;?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"sample_id":"5","sample_name":"sample2","gender":"Undef","clinical_freetext":"","marked_as_finished":"0","control_sample":"0", "project_id":"4","project_name":"2013.7.1_10:29:55","project_created":"2013-07-01 10:29:55","clinical_hpo":[{"hpo_term":"Intellectual disability","hpo_definition":"Subnormal intellectual functioning which originates during the developmental period. Intellectual disability, previously referred to as mental retardation, has been defined as an IQ score below 70.","hpo_comment":"This term should be used for children at least five years old. For younger children, consider the term Global developmental delay (HP:0001263).","hpo_xref":"MeSH:D008607 \"Intellectual Disability\",UMLS:C0025362 \"Mental Retardation\",UMLS:C0423903 \"Low intelligence\","}],"pedigree":{"5":{"Replicates":[],"Siblings":[],"Parents":[],"Children":{"1":{"sample_name":"sample3","sample_gender":"Female"}}},"1":{"Replicates":[],"Siblings":{"9":{"sample_name":"F5_index","sample_gender":"Undef"}},"Parents":{"5":{"sample_name":"sample2","sample_gender":"Undef"},"3":{"sample_name":"sample1","sample_gender":"Female"}},"Children":[]},"9":{"Replicates":[],"Siblings":{"1":{"sample_name":"sample3","sample_gender":"Female"}},"Parents":[],"Children":[]},"3":{"Replicates":[],"Siblings":{"8":{"sample_name":"test.b.a.final","sample_gender":"Male"}},"Parents":[],"Children":{"1":{"sample_name":"sample3","sample_gender":"Female"}}},"8":{"Replicates":[],"Siblings":{"3":{"sample_name":"sample1","sample_gender":"Female"}},"Parents":[],"Children":[]}},"nr_variants":"66828"}
    </div>


    <h4 class=underline>API Command 'SubmitQuery/Sample/&lt;sample_id&gt;'</h4>
    <p class=indent>Run Query on a specific sample. Available parameters are:
    <ul>
        <li><span class=strong>&lt;sample_id&gt;</span>: Mandatory. Numeric sample_id of the sample under study.</li>
        <li><span class=strong>fid=&lt;filter_id&gt;</span>: Optional. Filter setting to use. Only single filters are possible. Create and store filters using the web-interface to apply them here.</li>
        <li><span class=strong>aid=&lt;annotation_id&gt;</span>: Optional. Annotation sets to add to the variants. Multiple sets can be provided as a comma seperated list. </li>
        <li><span class=strong>slicestart=&lt;integer&gt;</span>: Optional. If provided, retrieve 100 variants (chromosome/position sorted), starting from the provided value. If absent, retrieve all variants.</li>
    </ul>
    </p>
    <p>The returned structure contains:
    <ul>
        <li><span class=strong>query_key</span>: Key to retrieve the results of the query</li>
        <li><span class=strong>queue_position</span>: Position on the processing queue</li>
        <li><span class=strong>hpc_job_id</span>: If queries are executed on a HPC-system, this value contains the internal hpc_job_id returned from the scheduler.</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/SubmitQuery/Sample/&lt;sample_id&gt;?fid=&lt;filter_id&gt;&amp;aid=&lt;annotation_id&gt;&amp;apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"hpc_job_id":"2634.devbox","query_key":"32","queue_position":"1"}
    </div>


    <h4 class=underline>API Command 'SubmitQuery/Region/&lt;region_description&gt;'</h4>
    <p class=indent>Run Query on a specific region. Available parameters are:
    <ul>
        <li><span class=strong>&lt;region_description&gt;</span>: Mandatory. This can be either a RefSeq gene sybmol (e.g. <span class=italic>ANK3</span>, or a UCSC region description (e.g. chr5:1545000-1546000).</li>
        <li><span class=strong>fid=&lt;filter_id&gt;</span>: Optional. Filter setting to use. Only single filters are possible. Create and store filters using the web-interface to apply them here.</li>
        <li><span class=strong>aid=&lt;annotation_id&gt;</span>: Optional. Annotation sets to add to the variants. Multiple sets can be provided as a comma seperated list. </li>
        <li><span class=strong>slicestart=&lt;integer&gt;</span>: Optional. If provided, retrieve 100 variants (chromosome/position sorted), starting from the provided value. If absent, retrieve all variants.</li>
    </ul>
    </p>
    <p>The returned structure contains:
    <ul>
        <li><span class=strong>query_key</span>: Key to retrieve the results of the query</li>
        <li><span class=strong>queue_position</span>: Position on the processing queue</li>
        <li><span class=strong>hpc_job_id</span>: If queries are executed on a HPC-system, this value contains the internal hpc_job_id returned from the scheduler.</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/SubmitQuery/Region/&lt;sample_id&gt;?fid=&lt;filter_id&gt;&amp;aid=&lt;annotation_id&gt;&amp;apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"hpc_job_id":"2791.devbox","query_key":"189","queue_position":"1"}
    </div>

    <h4 class=underline>API Command 'GetStatus/Query/&lt;query_id&gt;'</h4>
    <p class=indent>Get status of a query specified by its ID. The returned structure contains:
    <ul>
        <li><span class=strong>status</span>: Current status. Is one of queued/finished/running/error</li>
        <li><span class=strong>queue_position</span>: Provided if still queued</li>
        <li><span class=strong>message</span>: Provided if status is 'error', contains information on the problem</li>
        <li><span class=strong>stderr</span>: Further error messages, if available in case of 'error'</li>
        <li><span class=strong>ERROR</span>: Error message in case of failure to check query status</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetStatus/Query/&lt;query_id&gt;?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"status":"finished"}
    </div>

    <h4 class=underline>API Command 'GetStatus/Queue'</h4>
    <p class=indent>Get status of the queue for api-submitted queries. The returned structure contains the number of queued queries:
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetStatus/Queue?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"nr_queued":"17"}
    </div>

    <h4 class=underline>API Command 'GetStatus/Annotation'</h4>
    <p class=indent>Get the annotation status of VariantDB. Reports on pending and/or running annotation jobs for variants and modified gene panels</p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetStatus/Annotation?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>["Pending Variant Annotations", "Running Variant Annotations", "Running GenePanel Annotations"]
    </div>

    <h4 class=underline>API Command 'GetStatus/Annotation/&lt;sample_id&gt;'</h4>
    <p class=indent>Get the annotation status of a specified sample. Reports timestamp when annotation was finished.</p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetStatus/Annotation/&lt;sample_id&gt;?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"AnnotationStatus":"Finished on Fri Feb 20 15:14:36 CET 2015"}
    </div>


    <h4 class=underline>API Command 'GetStatus/Revision'</h4>
    <p class=indent>Get current version of VariantDB. Specified as the mercurial revision. The returned structure contains the output from "hg tip" for both the web-interface and the database backend.</p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetStatus/Revision?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"Site":["changeset: 463:f5e613a8b374","tag: tip","user: Geert Vandeweyer <geert.vandeweyer@uantwerpen;be>","date: Fri Feb 20 11:44:09 2015 +0100","summary: Added parsing support for the ArrogantRobot format of 23andMe data",""],"Database":["changeset: 463:f5e613a8b374","tag: tip","user: Geert Vandeweyer <geert.vandeweyer@uantwerpen;be>","date: Fri Feb 20 11:44:09 2015 +0100","summary: Added parsing support for the ArrogantRobot format of 23andMe data","","changeset: 74:d906b042eadc","tag: tip","user: Geert Vandeweyer <geert.vandeweyer@uantwerpen;be>","date: Mon Jan 12 16:00:42 2015 +0100","summary: Added ExAC annotation table",""]}
    </div>

    <h4 class=underline>API Command 'GetStatus/VariantDB'</h4>
    <p class=indent>Get the status of VariantDB itself. Status can be:
    <ul>
        <li><span class=strong>Operative</span>: System is fully operational.</li>
        <li><span class=strong>Construction</span>: System is down for maintenance. All queries are delayed (paused), access to the website is blocked.</li>
        <li><span class=strong>LiftOver</span>: A conversion to a new genome-build is performed. All queries are delayed, but (read-only) access to the website is possible.</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetStatus/VariantDB?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"status":"Operative"}
    </div>

    <h4 class=underline>API Command 'GetStatus/Stats'</h4>
    <p class=indent><span style='color:red'>ADMIN only</span>: Get database and server statistics. Output includes:
    <ul>
        <li><span class=strong>size_database</span>: Total size of VariantDB databases, according to mysql.</li>
        <li><span class=strong>size_datafiles</span>: Total size of BAM/VCF files stored on VariantDB by its users.</li>
        <li><span class=strong>system_df</span>: Full output "df -h", performed on the VariantDB web-server.</li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetStatus/Stats?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"size_database":[{"Data Base Name":"NGS-Variants-Admin","Data Base Size in MB":"0.51"},{"Data Base Name":"NGS-Variants-hg18","Data Base Size in MB":"606.35"},{"Data Base Name":"NGS-Variants-hg19","Data Base Size in MB":"3302.84"}],"size_datafiles":"36M","system_df":["Filesystem Size Used Avail Use% Mounted on","\/dev\/vda1 19G 9.6G 7.9G 55% \/",":\/dev/vdb1 57G 38G 20G 66% \/tmp"]}
    </div>



    <h4 class=underline>API Command 'GetQueryResults/&lt;Query_ID&gt;'</h4>
    <p class=indent>Get results of a query, specified by its query_id. Output on success includes, for sample based queries, a 'Variants' element, which is an array of all retained variants and the annotations, and a 'Comments' element, which holds encountered problems and/or summary information.</p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetQueryResults/&lt;query_id&gt;?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"Variants":[{"esp6500_aa":".","Stretch_Length":"7\/6","Quality_By_Depth":"0.87","set_inheritance":"Not Defined","Ensembl":[{"Ensembl_VariantType":"nonframeshift substitution","Ensembl_TranscriptID":"ENST00000377770","Ensembl_Exon":"1"},{"Ensembl_VariantType":"nonframeshift substitution","Ensembl_TranscriptID":"ENST00000406326","Ensembl_Exon":"1"}],"validation":"-","snp135_rsID":".","MutationTaster":"D (1.000);N (0.065);N (0.400)","Stretch_Unit":"GCG","diagnostic_class":"-","UCSCgene":[{"UCSC_cPointNT":"c.166_169C","UCSC_Transcript":"uc003wlk.3","UCSC_Symbol":"DPP6"},{"UCSC_cPointNT":"c.166_169C","UCSC_Transcript":"uc003wlj.3","UCSC_Symbol":"DPP6"}],"ljb_Sift":". (-1.000)","ljb_PolyPhen2":". (-1.000)","_1000g2012apr_all":".","ljb_MutTast":". (-1.000)","Strand_Bias":"0","validation_details":"","Fisher_Strand_Bias":"2.813","SIFT\/PROVEAN":[{"SIFT_Position":"57\/865","PROVEAN_Effect":"N","PROVEAN_Score":"0.068","SIFT_Score":"-1.000","SIFT_Protein":"ENSP00000367001","SIFT_Effect":".","SIFT_Type":".","SIFT_AA_Change":"G\/.","Grantham_Score":"0"},{"SIFT_Position":"57\/353","PROVEAN_Effect":"N","PROVEAN_Score":"0.420","SIFT_Score":"-1.000","SIFT_Protein":"ENSP00000384393","SIFT_Effect":".","SIFT_Type":".","SIFT_AA_Change":"G\/.","Grantham_Score":"0"}],"Genomic_SuperDups_Score":"0.98228","Alternative Allele Depth":"5","allelic_ratio":"0.1923","_1000g2012apr_eur":".","ref_allele":"CGCG","Tranches_Filter":".","snp130_rsID":".","variant_id":"169469","snp138_NrChr":"0","Stretch":"yes","Phred_Polymorphism":"399.33","Reference Allele Depth":"21","Base_Quality_Rank_Sum":"3.48","Mapping_Quality":"55.56","snp137_NrChr":"0","position":"153750071","esp6500_all":".","genotype":"Heterozygous","snp137_MAF":"0.00000","Phred_Genotype":"99","ljb_GERP":". (-1.000)","estimated_inheritance":"- (0p\/1s\/r)","DeltaPL":"109","Genomic_SuperDups_Location":"chr7:149735855","snp135_MAF":"0.00000","Read_Position_Rank_Sum":"-1.399","Mapping_Quality_Rank_Sum":"-5.421","chr":"chr7","snp138_MAF":"0.00000","ljb_LRT":". (-1.000)","esp5400_aa":".","snp137_Clinical":"0","RefSeq":[{"RefSeq_Symbol":"DPP6","RefSeq_cPointAA":".","RefSeq_Protein_Length_Difference":"0","RefSeq_cPointNT":"c.166_169C"},{"RefSeq_Symbol":"DPP6","RefSeq_cPointAA":".","RefSeq_Protein_Length_Difference":"0","RefSeq_cPointNT":"c.166_169C"}],"snp135_NrChr":"0","_1000g2012apr_afr":".","snp135_Clinical":"0","snp137_rsID":".","snp138_Clinical":"0","alt_allele":"C"},{"esp6500_aa":".","Stretch_Length":"0\/0","Quality_By_Depth":"4.94","set_inheritance":"Not Defined","Ensembl":[{"Ensembl_VariantType":"nonsynonymous SNV","Ensembl_TranscriptID":"ENST00000252321","Ensembl_Exon":"1"}],"validation":"-","snp135_rsID":".","MutationTaster":"D (1.000)","Stretch_Unit":"","diagnostic_class":"-","UCSCgene":[{"UCSC_cPointNT":"c.T1240C","UCSC_Transcript":"uc001qni.3","UCSC_Symbol":"KCNA5"}],"ljb_Sift":"D (1.000)","ljb_PolyPhen2":"D (0.998)","_1000g2012apr_all":".","ljb_MutTast":"D (1.000)","Strand_Bias":"0","validation_details":"","Fisher_Strand_Bias":"0","SIFT\/PROVEAN":[{"SIFT_Position":"414\/613","PROVEAN_Effect":"D","PROVEAN_Score":"-4.866","SIFT_Score":"1.000","SIFT_Protein":"ENSP00000252321","SIFT_Effect":"D","SIFT_Type":".","SIFT_AA_Change":"S\/P","Grantham_Score":"74"}],"Genomic_SuperDups_Score":".","Alternative Allele Depth":"1","allelic_ratio":"0.1667","_1000g2012apr_eur":".","ref_allele":"T","Tranches_Filter":".","snp130_rsID":".","variant_id":"169479","snp138_NrChr":"0","Stretch":"no","Phred_Polymorphism":"444.95","Reference Allele Depth":"5","Base_Quality_Rank_Sum":"-3.82","Mapping_Quality":"41.31","snp137_NrChr":"0","position":"5154553","esp6500_all":".","genotype":"Heterozygous","snp137_MAF":"0.00000","Phred_Genotype":"14","ljb_GERP":"NA (4.520)","estimated_inheritance":"- (0p\/1s\/r)","DeltaPL":"14","Genomic_SuperDups_Location":".","snp135_MAF":"0.00000","Read_Position_Rank_Sum":"-8.117","Mapping_Quality_Rank_Sum":"-0.49","chr":"chr12","snp138_MAF":"0.00000","ljb_LRT":"D (1.000)","esp5400_aa":"0.00054","snp137_Clinical":"0","RefSeq":[{"RefSeq_Symbol":"KCNA5","RefSeq_cPointAA":"p.S414P","RefSeq_Protein_Length_Difference":"0","RefSeq_cPointNT":"c.T1240C"}],"snp135_NrChr":"0","_1000g2012apr_afr":".","snp135_Clinical":"0","snp137_rsID":".","snp138_Clinical":"0","alt_allele":"C"}],"Comments":["2 out of 148 variants passed the used filter settings.","total runtime: 17s"]}
    </div>

    <p class=indent>For region based queries, the result contains a 'Variants' and 'Comments' element. The Variants element holds two child elements:
    <ul>
        <li><span class=strong>general_annotations</span>: Annotations that are not sample specific (e.g. Gene effect, population frequencies, ClinVar associations etc.)</li>
        <li><span class=strong>sample_specific_annotations</span>: One entry per sample harbouring the variant, identified by the sample_id. These entries hold:<ul>
                <li>sample_details: sample_name, sample_gender</li>
                <li>variant_details: All specified, sample specific annotations (e.g. Depth, Strand Bias, mapping quality, etc)</li>
            </ul>
        </li>
    </ul>
    </p>
    <div class=block>
        <div class=code>Code</div><?php echo "https://$domain/$basepath/"; ?>api/GetQueryResults/&lt;query_id&gt;?apiKey=&lt;key&gt;
    </div>
    <div class=block>
        <div class=example>Example output</div>{"Variants":[{"sample_specific_annotations":{"6":{"sample_details":{"sample_name":"test","sample_gender":"Undef"},"variant_details":{"strand_bias":"-454.52","mapping_quality_rank_sum":"1.008","tranches_filter":".","base_quality_rank_sum":"0.368","fisher_strand_bias":"6.747","deltapl":"496","genotype":"Heterozygous","read_position_rank_sum":"0.818","mapping_quality":"57.92","somatic_state":"1","quality_by_depth":"21.99","ploidy":"2","phred_polymorphism":"989.66","alt_allele_depth":"29","ref_allele_depth":"16","stretch_length":"0\/0"}},"8":{"sample_details":{"sample_name":"test.b.a.final","sample_gender":"Male"},"variant_details":{"strand_bias":"0","mapping_quality_rank_sum":"0.563","tranches_filter":"PASS","base_quality_rank_sum":"0.135","fisher_strand_bias":"0","deltapl":"287","genotype":"Heterozygous","read_position_rank_sum":"-0.832","mapping_quality":"59.92","somatic_state":"1","quality_by_depth":"15.92","ploidy":"2","phred_polymorphism":"366.16","alt_allele_depth":"14","ref_allele_depth":"9","stretch_length":"0\/0"}},"5":{"sample_details":{"sample_name":"sample2","sample_gender":"Undef"},"variant_details":{"strand_bias":"-454.52","mapping_quality_rank_sum":"1.008","tranches_filter":".","base_quality_rank_sum":"0.368","fisher_strand_bias":"6.747","deltapl":"0","genotype":"Heterozygous","read_position_rank_sum":"0.818","mapping_quality":"57.92","somatic_state":"1","quality_by_depth":"21.99","ploidy":"2","phred_polymorphism":"989.66","alt_allele_depth":"29","ref_allele_depth":"16","stretch_length":"0\/0"}}},"variant_id":"34481","general_annotations":{"esp6500_aa":"0.00817","Ensembl":[{"Ensembl_VariantType":"nonsynonymous SNV","Ensembl_TranscriptID":"ENST00000280772","Ensembl_Exon":"37"}],"snp135_rsID":"rs41274672","MutationTaster":"N (0.000)","Stretch_Unit":"","UCSCgene":[{"UCSC_cPointNT":"c.G8988C","UCSC_Transcript":"uc001jky.3","UCSC_Symbol":"ANK3"}],"ljb_Sift":"T (0.890)","ljb_PolyPhen2":"P (0.279)","ljb_MutTast":"NA (0.408)","1000g2012apr_all":"0.02","SIFT\/PROVEAN":[{"SIFT_Position":"2996\/4377","PROVEAN_Effect":"N","PROVEAN_Score":"-0.461","SIFT_Score":"0.826","SIFT_Protein":"ENSP00000280772","SIFT_Effect":"T","SIFT_Type":"Single AA Change","SIFT_AA_Change":"Q\/H","Grantham_Score":"24"}],"Genomic_SuperDups_Score":".","ref_allele":"C","snp130_rsID":"rs41274672","snp138_NrChr":"8052","Stretch":"no","position":"61831651","snp137_NrChr":"8058","esp6500_all":"0.01169","snp137_MAF":"0.01552","ljb_GERP":"NA (-1.300)","Genomic_SuperDups_Location":".","snp135_MAF":"0.01335","1000g2012apr_afr":"0.01","chr":"chr10","snp138_MAF":"0.01540","esp5400_aa":"0.00990","ljb_LRT":"N (0.929)","snp137_Clinical":"0","RefSeq":[{"RefSeq_Symbol":"ANK3","RefSeq_cPointAA":"p.Q2996H","RefSeq_Protein_Length_Difference":"0","RefSeq_cPointNT":"c.G8988C"}],"snp135_NrChr":"6740","snp135_Clinical":"0","1000g2012apr_eur":"0.01","snp137_rsID":"rs41274672","snp138_Clinical":"0","Comments":["Gene Based Query. Only variants annotated by ANNOVAR_refgene table will be listed (intronic variants might be missed!)","1 variants passed the used filter settings.","total runtime: 4s"]}
    </div>