<div class=section>
<h3>Documentation : Variant Filtering </h3>
<h4>1. General</h4>
<p>It is advised to read the following two sections before starting to browse variants.
<ul>
	<li><a href='index.php?page=tutorial&amp;topic=anno'>Available Annotations</a> Overview and explanation of all available annotations</li>
	<li><a href='index.php?page=tutorial&amp;topic=filters'>Available Filters</a> Overview and explanation of all available filtering schemes</li>
</ul>

<h4>2. Selecting Filters</h4>
<p class=indent>Go to <a href='index.php?page=variants'>Variant Filter</a> in the top menu:
	<ol>
		<li>Select a sample by either starting to type the sample name, or by switching to the selection list. Alternatively, provide a genomic region or Gene Symbol</li>
		<p class=indent>After selecting the sample, a link to load the VCF/BAM data (if available) into IGV will be shown on the right.</p>
		<li>Select the 'Filter Settings' tab</li>
		<li>Select Filter rules by pressing the green plus icons (<img src='Images/layout/plus-icon.png'/>) for the relevant category.</li>
		<li>For the newly added rule in the table, select all mandatory options.<ol>
			<li>Match/NotMatch : Include/Exclude variants based on this rule</li>
			<li>First selection box: filtering is based on this item</li>
			<li>Additional fields: used to refine the filter</li>
		</ul></li>
		<li>Note the following:<ul>
			<li>Selecting multiple entries in a single selection box (eg nonsense+stop_gain) means that either of both should be matched.</li>
			<li>Creating multiple rules with one entry each (eg rule for nonsense + rule for stop_gain) means that both rules should be matched.</li>
		</ul></li>
	<li>Save Filter settings for future usage using the 'Save Current Filter' button, and provide a meaningfull name.</li>
	<li>VariantDB uses cookies to save your settings for a while. If you close the browser and come back later, your filters will be restored</li>
	</ol>
</p>
<h4>3. Selecting Annotations</h4>
	<ol>
		<li>Select the 'Annotations' tab</li>
		<li>Select the needed annotatons by clicking the checkboxes.</li>
		<li>Sets of annotations can be saved by using the 'Save Current Annotations' button.</li>
	</ol></p>


<h4>4. Apply the filter and get annotations</h4>
<p class=indent>Click the 'execute' button to get the results. Results are presented in batches of 100 matching variants. On the 'export' tab, the button will start the generation and download of a csv file with all matching variants. </p>
	
<h4>5. Export</h4>
<p class=indent>Under the export tab, you can obtain a CSV file for further downstream annotation. It will contain all selected annotations for the filtered data.
</p>

<h4>6. Example Filtering Strategies</h4>
<p>See <a href='index.php?page=tutorial&topic=strategies'>here</a></p>
</div>
