<?php
// gpid set?
if (!isset($_GET['gpid']) || $_GET['gpid'] == '' || !is_numeric($_GET['gpid'])) {
    echo "<div class=section>";
    echo "<h3>Invalid Panel ID</h3>";
    echo "<p>The specified panel-id was not valid.</p>";
    echo "</div>";
    exit;
}
$gpid = $_GET['gpid'];
//allowed?
$rows = runQuery("SELECT gp.Name, gp.Description, u.LastName, u.FirstName, gp.LastEdit FROM `GenePanels` gp JOIN `Users` u ON gp.Owner = u.id WHERE gp.id = '$gpid' ", "GenePanels:Users")[0];
$gpname = $rows['Name'];
$gpdesc = $rows['Description'];
$gpowner = $rows['LastName'] . " " . $rows['FirstName'];
$gples = explode(" ", $rows['LastEdit']);
$gple  = $gples[0];
echo "<div class=section>";
echo "<h3>Panel Details: $gpname</h3>";
echo "<p><span class=emph>Owner:</span> $gpowner<br/>";
echo "<span class=emph>Last Edit:</span> $gple</p><p>";
echo "<span class=emph>Name: </span><br/><span style='margin-left:1em;'> $gpname </span><br/>";
echo "<span class=emph>Description:</span><br/>";
echo "<span style='margin-left:1em;' >$gpdesc</span>";
echo "</p><p><span class=emph>Included Genes:</span> <br/>";
echo "<table style='width:90%;border-bottom:1px solid #333;margin-left:1em;' id=genetable cellspacing=0 ><tr><th class=top>RefSeq Symbol</th><th class=top>NCBI Gene_ID</th><th class=top>Comment</th><th class=top>Last Edit</th></tr>";
// get genes
$rows = runQuery("SELECT Symbol, gid, Comment, LastEdit FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = '$gpid' ", "GenePanels_x_Genes_ncbigene");
$idx = -1;
foreach ($rows as $key => $row) {
    $idx++;
    $slice = floor($idx / 1000);
    $gidx = $idx - $slice * 1000;
    $line = "<tr >";
    $line .= "<td>" . $row['Symbol'] . "</td>";
    $line .= "<td>" . $row['gid'] . "</td>";
    $line .= "<td>" . $row['Comment'] . "</td>";
    $line .= "<td>" . $row['LastEdit'] . "</td>";
    $line .= "</tr>";
    echo $line;
}
echo "</table>";
//$idx--;
// new genes
echo "</div>";
