<div class=section>
<h3>Documentation : Example Filtering Strategies </h3>
<p>VariantDB allows filtering according to several strategies. Below are a few example setups.</p>
<h4>1. De Novo Trio:</h4>
<p class=indent>
    <a href='Images/Content/denovo.ped.png' rel='lightbox' style='float:left;padding-left:3em'><img class=simple style='width:8em' src='Images/Content/denovo.ped.png' alt='De Novo Pedigree' /></a><br style='clear:both;'/>
	<ol style='float:left;'>
		<li><span class=emph>Case:</span> Two healthy parents, no familial history. Expected model: De novo mutation. Sequencing data from all three indivuals is available.</li>
		<li><span class=emph>Strategy:</span> Select variants not present in parents, with effect on protein function</li>
		<li>Upload the three VCF (and BAM files to variantDB (see <a href='index.php?page=tutorial&topic=newdata'>here</a> for instructions)</li>
		<li>Set Family relations in the <a href='index.php?page=samples'>Sample Management</a> section: Assign obht parents to the offspring sample (see image below)<br/>
    			<a href='Images/Content/denovo.rel.png' rel='lightbox' style='float:left;padding-left:3em'><img class=simple style='width:15em' src='Images/Content/denovo.rel.png' alt='De Novo Set Relations' /></a> </li>
		<li style='clear:both;'>At 'Variant Filter', select the correct sample 'Affected Daughter'</li>
		<li>Set the following filters: <ol>
			<li>Filter On Family Information : 'Not Match', 'In Parent', 'Father + Mother', 'As Any Genotype'</li>
			<li>Filter On snpEff Information : 'Match', 'Effect Impact', 'High'<br/>
			<a href='Images/Content/denovo.filters.png' rel='lightbox' style='float:left;padding-left:1em;'><img class=simple style='width:15em;' src='Images/Content/denovo.filters.png' alt='De Novo Filter Settings'/></a></li>
		</ol></li>
		<li style='clear:both;'>Select some relevant annotations</li>
		<li>Execute Query</li>
	</ol></p>

<p class=indent>The above approach is a good starting point selecting high impact variants that are not present in the parents. We excluded all variants present in the parents, regardless of the exact genotype (heterozygous/homozygous) in either child or parents. The resulting variants have a severe impact (frameshift/stopgain/...) on at least one transcript as annotated by snpEff. To further restrict your result, you might add some quality thresholds. If no clear candidate remains, adjust your filters. For example: add 'moderate' effect impact from snpEff. Alternatively, remove the snpEff filter and activate filtering on CADD score, for example with a PhredScaled CADD score above 30. This relates to the 0.1% most likely pathogenic SNVs of all SNVs possible in the genome. </p> 


<h4>2. Autosomal Dominant Family: </h4>
<p class=indent>
	<a href='Images/Content/AD.ped.png' rel='lightbox' style='float:left;padding-left:3em'><img style='width:15em;' class=simple src='Images/Content/AD.ped.png' alt='Autosomal Dominant Pedigree' /></a><br style='clear:both;'/>
	<ol style='float:left;'>
		<li><span class=emph>Case:</span>Large pedigree with familial history. Expected model: Automsomal Dominant. Sequence data from three affected family members and one unaffected sibling (S2) of the index patient (S1).</li>
		<li><span class=emph>Strategy</span> Select variants shared amongst affected members, absent from healthy sibling</li>
		<li>Set Family relations. Although Cousin is not a direct option in VariantDB, you can assign S3 as a sibling of S1 without any other consequences<br/>
			<a href='Images/Content/AD.rel.png' rel='lightbox' style='float:left;padding-left:3em;'><img class=simple src='Images/Content/AD.rel.png' alt='Relation settings for Autosomal Dominant filtering' style='width:15em'/></a></li>
		<li style='clear:both;'>At 'Variant Filter', select the correct sample 'S1.Affected.Index'</li>
		<li style='clear:both;'>Set the following filters: <ol>
			<li>Filter On Family Information : 'Match', 'In Parent', 'S4.Affected.Mother'</li>
			<li>Filter On Family Information : 'Match', 'In Sibling', 'S3.Affected.Cousin'</li>
			<li>Filter On Family Information : 'Not Match', 'In Sibling', 'S2.Unaffected.sibling'<br/>
			<a href='Images/Content/AD.filters.png' rel='lightbox' style='float:left;padding-left:1em'><img class=simple style='width:15em;' src='Images/Content/AD.filters.png' alt='Automsomal Dominant Familial Filter Settings'/></a></li>
			</ol></li>
		<li style='clear:both;'>Select Filters with regard to function (eg RefSeq, snpEff, CADD, ...)</li>
		<li>Select Annotations</li>
		<li>Execute Query</li>
	</ol>
</p>
<p class=indent>The above approach selects variants shared amongst affected family members and not present in healthy family members. As we expect dominant inheritance, no criteria are set on the genotype, as any genotype (heterozygous or homozygous) causes the phenotype. </p>

<h4>3. Autosomal Recessive</h4>
	<p class=indent>
	<a href='Images/Content/AR.ped.png' rel='lightbox' style='float:left;padding-left:3em'><img style='width:15em;' class=simple src='Images/Content/AR.ped.png' alt='Autosomal Recessive Pedigree' /></a><br style='clear:both;'/>
	<ol style='float:left;'>
		<li><span class=emph>Case:</span>Pedigree with familial history. Expected model: Automsomal Recessive. Sequence data from affected index patient (S1), both parents (S3-4) (and unaffected sibling (S2)).</li>
		<li><span class=emph>Strategy</span> Select variants homozygous in index, heterozygous in both parents (and not homozygous in sibling)</li>
		<li>Set Family relations. </li>
		<li >At 'Variant Filter', select the correct sample 'S1.Affected.Index'</li>
		<li style='clear:both;'>Set the following filters: <ol>
			<li>Filter On Family Information : 'Match', 'In Parent', 'S4.Mother', 'Heterozygous'</li>
			<li>Filter On Family Information : 'Match', 'In Sibling', 'S3.Father', 'Heterozygous'</li>
			<li>Filter On Family Information : 'Not Match', 'In Sibling', 'S2.Unaffected.sibling', 'Homozgyous'<br/>
			<a href='Images/Content/AR.filters.png' rel='lightbox' style='float:left;padding-left:1em'><img class=simple style='width:15em;' src='Images/Content/AR.filters.png' alt='Automsomal Recessive Familial Filter Settings'/></a></li>
			</ol></li>
		<li style='clear:both;'>Select Filters with regard to function (eg RefSeq, snpEff, CADD, ...)</li>
		<li>Select Annotations</li>
		<li>Execute Query</li>
	</ol>
</p>
<p class=indent>Note the difference with the De novo filtering. For de novo variants, we set that variants in the index patient should not be present in either father or mother. This was combined in a single statement (multi-select box, not match : Translates to Not in Father OR Mother). In contrast, to ensure that the variant is present in both father and mother we need to set two rules, each specifying to match one parent. Furthermore, we specified the genotype to match. </p>
<p style='border:solid 1px red; padding:0.5em'><span class=emph style='color:red'>IMPORTANT: </span> Selecting multiple values in a single filtering rule matches if one of the values matches (eg: either non-synomous, or frameshift or stopgain).</p>

<h4>4. Linkage Based</h4>
	<p class=indent>
	<ol style='float:left;'>
		<li><span class=emph>Case:</span>Pedigree with familial history. SNP-array based linkage analysis resulted in a single region with high LOD score. Sequencing data from one affected indivual</li>
		<li><span class=emph>Strategy</span> Select variants within the linkage region</li>
		<li >At 'Variant Filter', select the correct sample </li>
		<li style='clear:both;'>Set the following filters: <ol>
			<li>Filter On Location : 'Match', 'Chromosome', 'Chromosome of linkage region'</li>
			<li>Filter On Location : 'Match', 'Position', 'Bigger than', 'linkage region start coordinate'</li>
			<li>Filter On Location : 'Match', 'Position', 'Smaller than', 'linkage region end coordinate'</li>
			</ol></li>
		<li >Select Filters with regard to function (eg RefSeq, snpEff, CADD, ...)</li>
		<li>Select Annotations</li>
		<li>Execute Query</li>
	</ol>
</p>
<p class=indent>This filtering method allows to select variants specific to a genomic region. If additional samples from the linkage study are available, results can be refined using the above described family based filters.</p>

<h4>5. Complex pedigree or Case/Control study</h4>
	<p class=indent>
	<a href='Images/Content/Complex_Pedigree.png' rel='lightbox' style='float:left;padding-left:3em'><img class=simple style='width:32em' src='Images/Content/Complex_Pedigree.png' alt='Complex Pedigree' /></a><br style='clear:both;'/>
	<ol style='float:left;'>
		<li><span class=emph>Case:</span>Large Cohort or family screened. Expected inheritance : complex or incomplete penetrance. </li>
		<li><span class=emph>Strategy</span> Select variants with minimal occurence ratio in cases, and maximal occurence ratio in controls</li>
		<li>Order your affected in unaffected samples into two seperate projects from the <a href='index.php?page=samples'>sample management </a>page. In this case:
			<ol>
				<li>Project Complex_Pedigree_Affected : Individuals 12, 14, 37, 1, 34 and 38</li>
				<li>Project Complex_Pedigree_Unaffected : All other available individuals</li>
			</ol>
		</li>
		<li >At 'Variant Filter', select an affected sample (eg. Indv0001) </li>
		<li style='clear:both;'>Set the following filters: <ol>
			<li>Filter On Occurence : 'Match', 'Relative Occurence By Project', 'Cases_Project', 'Bigger Than', '0.83'. This allows one false-negative amongst the 6 affected samples.</li>
			<li>Filter On Occurence : 'Match', 'Relative Occurence By Prroject', 'Control_Project', 'Smaller Than' '0.1'. This allows healthy family members to carry the mutation. Adjust this value to reflect the degree of penetrance. </li>
			</ol></li>
		<li >Select Filters with regard to function (eg RefSeq, snpEff, CADD, ...)</li>
		<li>At 'Filter Logic', further refine the filtering scheme if necessary. As shown in the example below, you might want to bypass quality filters, if a mutation is known in ClinVar to be pahogenic. As an example, the scheme now returns all high quality variants highly represented in affected family members and rare among the healthy siblings. In addition, it will return known pathogenic mutations from ClinVar, and high quality variants with a CADD-score over 40, which are also very likely to be pathogenic, regardless of frequencies in the pedigree.<br/>
			<a href='Images/Content/logic_complex.png' rel='lightbox' style='float:left;padding-left:3em'><img class=simple style='width:29em' src='Images/Content/logic_complex.png' alt='Complex Pedigree' /></a><br style='clear:both;'/>
		</li>
		<li>Select Annotations</li>
		<li>Execute Query</li>
	</ol>
</p>
<p class=indent>Here we say that 90% of the affected family members should harbour the mutant allele, while at most 10% of unaffected family members may harbour it. Setting the affected-ratio to less than 100% allows for false negative calls, while setting a maximal ratio for unaffected family members allows for incomplete penetrance. This filtering approach is also available using absolute occurence numbers, when the pedigree is too small for frequency measures. In case of large case/control cohorts, a similar approach with adequate frequencies can be applied.</p>
		
</div>
