<?php

global $config;

// Our custom error handler
function error_handler($number, $message, $file, $line, $vars = null)
{
    global $config;
    // what are the high impact errors : 
    $high_impact = array(1, 4, 16, 64, 256);
    $medium_impact = array(1, 2, 4, 16, 64, 256);
    // convert to number to string for printing
    $e_string = FriendlyErrorType($number);
    $email = "
        <p>Dear Administrator,</p>
        <p>An $e_string error occurred on line 
        $line in file: $file.</p> 
        <p><pre>$message </pre></p>";
    // backtrace
    $email .= "<p>Backtrace:<br/>";
    $bt = debug_backtrace();
    foreach ($bt as $idx => $layer) {
        $email .= $idx;
        if (isset($layer['file'])) {
            $line .= ": " . $layer['file'] . ": line " . $layer['line'];
        }
        if (isset($layer['function'])) {
            $email .= ": " . $layer['function'] . ": args(strs): [";
            foreach ($layer['args'] as $arg) {
                if (!is_array($arg)) {
                    $email .= "$arg, ";
                }
            }
            $email .= "]";
        }
        $email .= "<br/>";
    }
    // ERROR REDIRECTION: 
    // TO FILE : ALL ABOVE SET LEVEL.
    if (is_dir($config['LOG_PATH'])) {
        $logfile = $config['LOG_PATH'] . "/VariantDB_UI_php.log";
    } else {
        $logfile = $config['LOG_PATH'];
    }
    $log = fopen($logfile, 'a');
    fwrite($log, date("Y-m-d H:i:s") . " : $e_string : $file:$line : $message\n");
    fclose($log);

    // TO MAIL : HIGH IMPACT:
    if (in_array($number, $medium_impact) && strtoupper($config['LOG_LEVEL']) != 'DEBUG') {
        // Email the error to someone...
        trigger_error("Sending error to system admin email : $email");
        // set headers
        //$headers = "Content-type: text/html; charset=iso-8859-1\r\n" .
        $headers = "From: VariantDB-Logger <noreply@" . $_SERVER['HTTP_HOST'] . ">\r\n" .
            "Reply-To: System Admin <" . $config['ADMINMAIL'] . ">\r\n" .
            "MIME-Version: 1.0\r\n" .
            "Content-type: text/html; charset=iso-8859-1";
        $subject = "VariantDB/UI $e_string error encounted";
        mail($config['LOG_EMAIL'], $subject, $email, $headers);
        // for high impact : stop processing if not in api.
        if (in_array($number, $high_impact)) {
            global $myAPI;
            if (!$myAPI) {
                die("<dev class=section><h3>There was an error.</h3><p>The system administrator has been notified.</p><p><a href='javascript:window.location.assign(document.referrer)'>Go Back</a></p></div>");
            } else {
                trigger_error("Administrator has been notified of error, but API handles the error to the user.", E_USER_NOTICE);
            }
        }
    }
}

function FriendlyErrorType($type)
{
    switch ($type) {
        case E_ERROR: // 1 //
            return 'E_ERROR';
        case E_WARNING: // 2 //
            return 'E_WARNING';
        case E_PARSE: // 4 //
            return 'E_PARSE';
        case E_NOTICE: // 8 //
            return 'E_NOTICE';
        case E_CORE_ERROR: // 16 //
            return 'E_CORE_ERROR';
        case E_CORE_WARNING: // 32 //
            return 'E_CORE_WARNING';
        case E_COMPILE_ERROR: // 64 //
            return 'E_COMPILE_ERROR';
        case E_COMPILE_WARNING: // 128 //
            return 'E_COMPILE_WARNING';
        case E_USER_ERROR: // 256 //
            return 'E_USER_ERROR';
        case E_USER_WARNING: // 512 //
            return 'E_USER_WARNING';
        case E_USER_NOTICE: // 1024 //
            return 'E_USER_NOTICE';
        case E_STRICT: // 2048 //
            return 'E_STRICT';
        case E_RECOVERABLE_ERROR: // 4096 //
            return 'E_RECOVERABLE_ERROR';
        case E_DEPRECATED: // 8192 //
            return 'E_DEPRECATED';
        case E_USER_DEPRECATED: // 16384 //
            return 'E_USER_DEPRECATED';
    }
    return "";
}

function handleFatalPhpError()
{
    $last_error = error_get_last();
    if ($last_error) {
        error_handler($last_error['type'], $last_error['message'], $last_error['file'], $last_error['line']);
    }
}


// global error_handler (real errors like divide by zero etc)
set_error_handler('error_handler');
register_shutdown_function('handleFatalPhpError');

// set log level (log all by default)
ini_set('log_errors', E_ALL);

// print msg : levels are E_USER_NOTICE (default), E_USER_WARNING and E_USER_ERROR (triggers email and failure)
// trigger_error("Started logging module",E_USER_NOTICE);
