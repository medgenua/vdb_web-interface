<?php
// posted values?
$rsid = (isset($_POST['rsid'])) ? $_POST['rsid'] : '';
//submitted changes.
if (isset($_POST['DeleteSelected'])) {
    // delete reports
    $cmc = 0;
    foreach ($_POST['rids'] as $rid) {
        doQuery("DELETE FROM `Users_x_Report_Definitions` WHERE rid = '$rid'", "Users_x_Report_Definitions");
        doQuery("DELETE FROM `Report_Definitions` WHERE rid = '$rid'", "Report_Definitions");
        $cmc = 1;
    }
    if ($cmc == 1) {
        clearMemcache("Users_x_Report_Definitions:Report_Definitions");
    }
    // get sections by reports. will not delete sections included in a report.
    $rows = runQuery("SELECT Sections FROM `Report_Definitions`", "Report_Definitions");
    $sections = array();
    foreach ($rows as $row) {
        array_push($sections, explode(",", $row['Sections']));
    }
    $error = '';
    $cmc = 0;
    foreach ($_POST['rsids'] as $rsid) {
        if (in_array($rsid, $sections)) {
            $row = runQuery("SELECT Name FROM `Report_Sections` WHERE rsid = '$rsid'", "Report_Sections")[0];
            $error .= "<li>" . $row['Name'] . "</li>";
        } else {
            doQuery("DELETE FROM `Users_x_Report_Sections` WHERE rsid = '$rsid'", "Users_x_Report_Sections");
            doQuery("DELETE FROM `Report_Sections` WHERE rsid = '$rsid'", "Report_Sections");
            $cmc = 1;
        }
    }
    if ($cmc == 1) {
        clearMemcache("Users_x_Report_Sections:Report_Sections");
    }
    if ($error != '') {
        echo "<div style='border:red solid 2px'><span class=emph>ERROR: Cannot remove active sections</span><ol>$error</ol></div>";
    }
}
//////////////
// SECTIONS //
//////////////
// get all accessible sections
//  1. private & Public
$rows = runQuery("SELECT rsid, Name, Description,Owner FROM `Report_Sections` WHERE Owner = '$userid'", "Report_Sections");
$rsids = array();
foreach ($rows as $row) {
    $rsids['private'][$row['rsid']]['name'] = $row['Name'];
    $rsids['private'][$row['rsid']]['description'] = $row['Description'];
}
//  2. shared
$rows = runQuery("SELECT rs.rsid, rs.Name, rs.Description FROM `Report_Sections` rs JOIN `Users_x_Report_Sections` urs ON urs.rsid = rs.rsid WHERE urs.uid = '$userid' AND urs.full = 1", "Report_Sections:Users_x_Report_Sections");
foreach ($rows as $row) {
    $rsids['shared'][$row['rsid']]['name'] = $row['Name'];
    $rsids['shared'][$row['rsid']]['description'] = $row['Description'];
}

echo "<div class=section><h3>Delete report sections</h3>";
//if (count($rsids) == 0) {
//	echo "<p>No editable report sections found. Please create the first by <a href='index.php?page=report&t=new'>clicking here</a></p>";
//	exit;
//}
// get all report definitions shared to you.
$report_access = array();
$rows = runQuery("SELECT rid,full FROM `Users_x_Report_Definitions` WHERE uid = '$userid'", "Users_x_Report_Definitions");
foreach ($rows as $row) {
    $report_access[$row['rid']] = $row['full'];
}
// load all reports/section links.
$reports_by_section = array();
$reports_by_section['na'] = array();
$reports_by_section['a'] = array();
$rows = runQuery("SELECT Name, rid, Sections, Owner, Public FROM `Report_Definitions`", "Report_Definitions");
foreach ($rows as $row) {
    $sections = explode(",", $row['Sections']);
    if ($row['Public'] == 1 && !in_array($row['rid'], $report_access)) {
        $report_access[$row['rid']] = 0;
    }
    if ($row['Owner'] == "$userid") {
        $report_access[$row['rid']] = 1;
    }
    foreach ($sections as $sid) {

        if (!array_key_exists($row['rid'], $report_access)) {
            $reports_by_section['na'][$sid][$row['rid']] = 1;
        } else {
            $reports_by_section['a'][$sid][$row['rid']] = $row['Name'];
        }
    }
}
echo "<p><form action='index.php?page=report&t=delete' method=POST><span class=emph>Select Sections to delete:</span> &nbsp;</p>";
echo "<p><table style='width:90%' cellspacing=0>";
echo "<th class=top>Delete</th><th class=top>Name</th><th class=top style='width:30%'>Description</th><th class=top title='Private (created by you) or Shared'>Type</th><th class=top>In Reports</th></tr>";
$done = array();
foreach ($rsids['private'] as $key => $array) {
    $rsid =  $key;
    // in which reports?
    $list = implode(", ", array_values($reports_by_section['a'][$rsid]));
    if (count($reports_by_section['na'][$rsid]) > 0) {
        if ($list != '') {
            $list .= " + ";
        }
        $list .= count($reports_by_section['na'][$rsid]) . " without access";
    }
    if ($list == '') {
        $list = '-';
        $d = '';
    } else {
        $d = 'disabled="disabled"';
    }

    echo "<tr ><td style='padding-top:0.2em;'><input type=checkbox name='rsids[]' value='$rsid' $d></td><td style='padding-top:0.2em;'>" . $array['name'] . "</td><td style='padding-top:0.2em;'>" . $array['description'] . "</td><td style='padding-top:0.2em;'>Private</td>";
    echo "<td style='padding-top:0.2em;'>$list</td>";
    echo "</tr>";
    $done[$key] = 1;
}
foreach ($rsids['shared'] as $key => $array) {
    if (isset($done[$key])) continue;
    $rsid = $key;
    // in which reports?
    $list = implode(", ", array_values($reports_by_section['a'][$rsid]));
    if (count($reports_by_section['na'][$rsid]) > 0) {
        if ($list != '') {
            $list .= " + ";
        }
        $list .= count($reports_by_section['na'][$rsid]) . " without access";
    }
    if ($list == '') {
        $list = '-';
        $d = '';
    } else {
        $d = 'disabled="disabled"';
    }

    echo "<tr><td style='padding-top:0.2em;'><input type=checkbox name='rsids[]' value='$rsid' $d></td><td style='padding-top:0.2em;'>" . $array['name'] . "</td><td style='padding-top:0.2em;'>" . $array['description'] . "</td><td style='padding-top:0.2em;'>Private</td>";
    echo "<td style='padding-top:0.2em;'>$list</td>";
    echo "</tr>";
    $done[$key] = 1;
}
echo "<tr><td colspan=6 class=last>&nbsp;</td></tr></table></p>";
echo "</select> &nbsp; <input type=submit class=button value='Delete' name='DeleteSelected' /></p>";


//////////////
// REPORTS  //
//////////////
// get all accessible reports
//  1. private & Public
$rows = runQuery("SELECT rid, Name, Description,Owner FROM `Report_Definitions` WHERE Owner = '$userid'", "Report_Definitions");
$rids = array();
foreach ($rows as $row) {
    $rids['private'][$row['rid']]['name'] = $row['Name'];
    $rids['private'][$row['rid']]['description'] = $row['Description'];
}
//  2. shared
$rows = runQuery("SELECT rs.rid, rs.Name, rs.Description FROM `Report_Definitions` rs JOIN `Users_x_Report_Definitions` urs ON urs.rid = rs.rid WHERE urs.uid = '$userid' AND urs.full = 1", "Report_Definitions:Users_x_Report_Definitions");
foreach ($rows as $row) {
    $rids['shared'][$row['rid']]['name'] = $row['Name'];
    $rids['shared'][$row['rid']]['description'] = $row['Description'];
}

echo "<div class=section><h3>Delete report Definitions</h3>";
//if (count($rsids) == 0) {
//	echo "<p>No editable reports found. Please create the first by <a href='index.php?page=report&t=new'>clicking here</a></p>";
//	exit;
//}
echo "<p><table style='width:90%' cellspacing=0>";
echo "<th class=top>Delete</th><th class=top>Name</th><th class=top style='width:30%'>Description</th><th class=top title='Private (created by you) or Shared'>Type</th></tr>";
$done = array();
foreach ($rids['private'] as $key => $array) {
    $rid =  $key;
    echo "<tr ><td style='padding-top:0.2em;'><input type=checkbox name='rids[]' value='$rid'></td><td style='padding-top:0.2em;'>" . $array['name'] . "</td><td style='padding-top:0.2em;'>" . $array['description'] . "</td><td style='padding-top:0.2em;'>Private</td>";
    echo "</tr>";
    $done[$key] = 1;
}
foreach ($rids['shared'] as $key => $array) {
    if (isset($done[$key])) continue;
    $rid = $key;
    echo "<tr><td style='padding-top:0.2em;'><input type=checkbox name='rids[]' value='$rid'></td><td style='padding-top:0.2em;'>" . $array['name'] . "</td><td style='padding-top:0.2em;'>" . $array['description'] . "</td><td style='padding-top:0.2em;'>Private</td>";
    echo "</tr>";
    $done[$key] = 1;
}
echo "<tr><td colspan=5 class=last>&nbsp;</td></tr></table></p>";
echo "</select> &nbsp; <input type=submit class=button value='Delete' name='DeleteSelected' /></form></p>";
