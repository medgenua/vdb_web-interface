<?php
// for syntax...
?>
<!-- load the javascript -->
<script type="text/javascript" src="javascripts/menu.js?<?php echo $tip; ?>"></script>
<ul id="topmenu">
    <li><a href="index.php?page=main">Main</a></li>
    <li>&nbsp| <a href="javascript:void(0)" onmouseover="mopen('m1')" onmouseout="mclosetime()">New Data</a>
        <div class=submenu id='m1' onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
            <h3>Import options</h3>
            <a href="index.php?page=upload">VCF : Using FTP</a>
            <a href="index.php?page=tutorial&topic=api_import">VCF : Using API</a>
            <a href="index.php?page=import">tar.gz.gpg : Using import</a>
        </div>
    </li>
    <li>&nbsp| <a href="index.php?page=variants">Variant Filter</a></li>
    <li>&nbsp| <a href="index.php?page=samples">Manage Samples</a></li>
    <li>&nbsp| <a href="index.php?page=projects">Manage Projects</a></li>
    <li>&nbsp| <a href="index.php?page=recent">Recent Samples</a></li>
    <li>&nbsp| <a href="javascript:void(0)" onmouseover="mopen('m7')" onmouseout="mclosetime()">Reports</a>
        <div class=submenu id='m7' onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
            <h3>Configuration</h3>
            <a href="index.php?page=report&t=new">Define a report section</a>
            <a href="index.php?page=report&t=manage">View/Edit report sections</a>
            <a href="index.php?page=report&t=combine">Compose a report</a>
            <a href="index.php?page=report&t=checkboxes">Manage checkbox lists</a>
            <h3>Manage access</h3>
            <a href="index.php?page=report&t=share">Share configurations</a>
            <a href="index.php?page=report&t=delete">Delete configurations</a>
            <h3>Generate PDF</h3>
            <a href="index.php?page=report&t=run">Generate reports</a>
        </div>
    </li>

    <li>&nbsp| <a href="javascript:void(0)" onmouseover="mopen('m6')" onmouseout="mclosetime()">Beta-Functions</a>
        <div class=submenu id='m6' onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
            <h3>Use our BETA server</h3>
            <a href='http://143.169.238.105/variantdb_beta/index.php'>BETA-server</a>
        </div>
    </li>
    <li>&nbsp| <a href="javascript:void(0)" onmouseover="mopen('m8')" onmouseout="mclosetime()">Settings</a>
        <div class=submenu id='m8' onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
            <?php
            if (isset($_SESSION['level']) && $_SESSION['level'] >= 3) {
                echo "<h3>VariantDB Administrator</h3>";
                echo "<a href=\"index.php?page=admin_pages\">Overview</a>\n";
                echo "<a href=\"index.php?page=admin_pages&amp;type=SiteStatus\">Platform Status</a>\n";
                echo "<a href=\"index.php?page=admin_pages&amp;type=update\">Platform Updates</a>\n";
                echo "<a href=\"index.php?page=admin_pages&amp;type=users\">User Management</a>\n";
                echo "<a href=\"index.php?page=admin_pages&amp;type=license\">License Management</a>\n";
                echo "<a href=\"index.php?page=admin_pages&amp;type=impersonate\">Impersonate User</a>\n";
                echo "<a href=\"index.php?page=admin_pages&amp;type=mcstats\">MemCache Statistics</a>\n";
                echo "<a href=\"index.php?page=admin_pages&amp;type=newsbrief\">Send Newsbrief</a>\n";
                echo "<a href=\"index.php?page=admin_pages&amp;type=lift\">Genomic Build Update</a>\n";
                echo "<a href=\"index.php?page=errortests\">Error Testing</a>\n";
            }
            echo "<h3>Platform Settings</h3>\n";
            echo "<a href=\"index.php?page=version\">Version History</a>\n";
            echo "<a href=\"index.php?page=quota\">Quota Overview</a>\n";
            echo "<h3>Gene Related Settings</h3>\n";
            echo "<a href=\"index.php?page=genepanels\">Gene Panels</a>\n";
            echo "<a href=\"index.php?page=mane\">Transcripts sets</a>\n";
            echo "<h3>Variant Classifiers</h3>";
            echo '<a href="index.php?page=classifier&t=new">New Classifier</a>';
            echo '<a href="index.php?page=classifier&t=manage">Manage Classifiers</a>';
            echo '<!--<a href="index.php?page=classifier&t=vars">List Variants</a>-->';
            echo '<a href="index.php?page=classifier&t=activate">Active Classifiers</a>';
            echo "<h3>Checkbox Lists</h3>";
            echo '<a href="index.php?page=report&t=checkboxes">Manage Lists</a>';
            echo '<a href="index.php?page=report&t=checkboxes&cid=new">New List</a>';


            echo "<h3>Usergroup Settings</h3>";
            echo "<a href=\"index.php?page=group&type=mine\">Current memberships</a>\n";
            echo "<a href=\"index.php?page=group&type=join\">Join a group</a>\n";
            echo "<a href=\"index.php?page=group&type=create\">Create a group</a>\n";
            echo "<a href=\"index.php?page=group&type=manage\">Group administration</a>\n";
            ?>

        </div>
    </li>

    <li>&nbsp| <a href="index.php?page=tutorial">Documentation</a></li>
    <li>&nbsp| <a href="index.php?page=contact" title="Contact">Contact</a></li>
    <!-- <li>&nbsp;| <a href='javascript:void(0)' onclick="window.print();">Print</a></li> -->
    <?php
    if ($usehpc == 1) {
        $annorunning = exec("qstat -u $scriptuser | grep 'Ann.' | wc -l");
    } else {
        $annorunning = file_get_contents("Annotations/global.update.running.txt");
    }
    $annorunning = rtrim($annorunning);
    $panelrunning = rtrim(file_get_contents("Annotations/panel.update.running.txt"));
    $panelpending = rtrim(file_get_contents("Annotations/update.panel.txt"));
    $globalpending = rtrim(file_get_contents("Annotations/update.txt"));
    $rmessage = $pmessage = '';
    // running?
    if ($panelrunning == 1) {
        $rmessage = '- GenePanel Annotations &#013;';
    }
    if ($annorunning > 0) {
        $rmessage .= "- New Variant Annotations";
    }
    // pending?
    if ($panelpending != "") {
        $p = explode("\n", $panelpending);
        $pmessage = "- GenePanel Annotations for " . count($p) . ' panels &#013;';
    }
    if ($globalpending != "0") {
        $pmessage .= "- New Variant Annotations";
    }
    // print?
    $out = "";
    if ($rmessage != "") {
        $out =  "Annotations <span title='$rmessage'>Running</span> ";
    }
    if ($pmessage != "") {
        if ($out == "") {
            $out =  "Annotations <span title='$pmessage'>Pending</span> ";
        } else {
            $out .= "+ <span title='$pmessage'>Pending</span>";
        }
    }
    if ($out != "") {
        $out = "<li>&nbsp| <span style='font-weight:bold;color:red;'>" . $out . "</span></li>";
        echo $out;
    }
    ?>
</ul>

<!-- THE USERMENU -->
<ul id=topmenu style='position:absolute;left:auto;right:0em;'>

    <?php
    echo "<li class=italic title='" . $_SESSION['dbdescription'] . "'>" . $_SESSION['dbstring'];

    // only output option to change IF other builds are available AND logged-in
    $nrbuilds = 0;
    $output = " <a href='javascript:void(0)' title='Change Genomic Build' onmouseover=\"mopen('build')\" onmouseout=\"mclosetime()\"><img src='Images/layout/wrench.png' height=10px style='margin-bottom:-2px'></a>";
    $output .= "<div class=submenu id='build' onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">\n";
    // current build
    $query = "SELECT name, StringName FROM `NGS-Variants-Admin`.`CurrentBuild`";
    $row = RunQuery($query)[0];
    $short = $row['name'];
    $long = $row['StringName'];
    $nrbuilds++;
    if ("$short" == $_SESSION['dbname']) {
        $active = '* ';
    } else {
        $active = '';
    }
    $output .= "<a href='javascript:void()' onClick=\"setBuild('$short')\">$active$long</a>\n";
    // previous builds
    $query = "SELECT name, StringName FROM `NGS-Variants-Admin`.`PreviousBuilds`";
    $rows = RunQuery($query);
    foreach ($rows as $row) {
        $nrbuilds++;
        $short = $row['name'];
        $long = $row['StringName'];
        if ("$short" == $_SESSION['dbname']) {
            $active = '* ';
        } else {
            $active = '';
        }
        $output .= "<a href='javascript:void()' onClick=\"setBuild('$short')\">$active$long</a>\n";
    }
    $output .= "</div>";
    if ($nrbuilds > 1 && isset($loggedin) && $loggedin == 1) {
        echo "$output";;
    }
    echo " |&nbsp;</li>";

    if (isset($loggedin) && $loggedin == 1) {
        // are there new messages?
        if ($_SESSION['newmessages'] == 1) {
            $env = "<img src='Images/layout/mailred.png' style='height:0.9em;margin-bottom:-0.2em'>";
        } else {
            $env = "<img src='Images/layout/mailgrey.png' style='height:0.9em;margin-bottom:-0.2em''>";
        }
        // create context specific link to the documentation.
        $type = '';
        if (array_key_exists('type', $_GET)) $type = $_GET['type'];
        $doc = "|<a href='index.php?page=tutorial&amp;topic=$page&t=$type' target='_blank' class=img> ? </a>";
        // If administrator, check for platform updates.

    ?>
        <li><a href='javascript:void(0)' title='Set Personal Settings' onmouseover="mopen('m5')" onmouseout="mclosetime()"><?php echo $firstname; ?>
                <div class=submenu id='m5' onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
                    <a href="index.php?page=group&amp;type=mine">My Usergroups</a>
                    <a href="index.php?page=projects">My Projects</a>
                    <a href="index.php?page=recent">Recent Samples</a>
                    <a href="index.php?page=personal">My Details</a>
                    <a href="index.php?page=inbox">Inbox</a>
                    <a href='logout.php' title=Logout>Log Out</a>
                </div>
        </li>
        <li>|<a href='index.php?page=inbox' class=img>
            <?php
            echo "$env</a></li><li>&nbsp;$doc&nbsp;&nbsp;</li>\n";
        } else {
            echo "<li><a href=\"index.php?page=login\">Log In</a></li> &nbsp; \n";
        }
            ?>
</ul>