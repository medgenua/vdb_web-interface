<?php
// setid set?
if (!isset($_GET['setid']) || $_GET['setid'] == '' || !is_numeric($_GET['setid'])) {
    echo "<div class=section>";
    echo "<h3>Invalid Set ID</h3>";
    echo "<p>The specified panel-id was not valid.</p>";
    echo "</div>";
    exit;
}
$setid = $_GET['setid'];
//allowed?
$rows = runQuery("SELECT `edit` FROM `TranscriptSets_x_Users` WHERE `set_id` = '$setid' AND `uid` = '$userid'", "TranscriptSets_x_Users")[0];
if ($rows['edit'] != 1) {
    echo "<div class=section>";
    echo "<h3>Insufficient Privileges</h3>";
    echo "<p>You are not allowed to edit this set.</p>";
    echo "</div>";
    exit;
}
// process
if (isset($_POST['Update'])) {
    // general info
    $setname = addslashes($_POST['setname']);

    doQuery("UPDATE `TranscriptSets` SET `name` = '$setname' WHERE `set_id` = '$setid'", "TranscriptSets");

    // novel genes.
    $issues = '';
    $newgenes = explode("\n", $_POST['ExtraTranscripts']);
    $gidx = -1;
    foreach ($newgenes as $key => $rowline) {
        $gidx++;
        $rowline = rtrim($rowline);
        if ($rowline == "") {
            $gidx--;
            continue;
        }
        $row = preg_split("/[,\t]/", $rowline, 3);
        $row[0] = str_replace(" ", "", $row[0]);
        $row[0] = strtoupper($row[0]);
        $NM = $row[0];

        if (isset($row[1])) {
            $row[1] = str_replace(" ", "", $row[1]);
        }
        if (strtolower($row[1]) == 'discard') {
            $nm_type = '-1';
        } else {
            $nm_type = '1';
        }
        if (isset($row[2])) {
            $comments = addslashes($row[2]);
        } else {
            $comments = "";
        }
        // valid format ? 
        if (!preg_match('/^[A-Z]{2}_\d+(\.\d+)*$/', $NM)) {
            $issues .= "<li>$NM : Invalid format: Transcript must follow NM_012345.1 syntax (two prefix chars, version is optional).</li>";
            continue;
        }


        // already a member of the set?
        $rows = runQuery("SELECT nm_id FROM `TranscriptSets_Data` WHERE set_id = '$setid' AND nm_id = '$NM'", "TranscriptSets_Data");
        if (count($rows) > 0) {
            $issues .= "<li>$NM : Duplicate entry: Transcript already exists in this set, and was skipped.</li>";
            continue;
        }
        // insert
        doQuery("INSERT INTO `TranscriptSets_Data` (set_id, nm_id, favourite, added_by, Comments) VALUES ('$setid','$NM','$nm_type','$userid','$comments')", "TranscriptSets_Data");
        $updated = 1;
        //clearMemcache("TranscriptSets_Data");
    }
    if ($issues != '') {
        echo "<div class=section>";
        echo "<h3 style='color:red'>Update issues were reported</h3>";
        echo "<p>The following issues were reported by the update script:<ol>$issues</ol></p></div>";
    }

    if ($issues == '') {
        echo "<meta http-equiv='refresh' content='0;URL=index.php?page=mane&t=edit&setid=$setid'>\n";
    }
}

// print current state.

$rows = runQuery("SELECT t.`name`, u.`LastName`, u.`FirstName` FROM `TranscriptSets` t JOIN `Users` u ON t.`owner` = u.`id` WHERE t.`set_id` = '$setid' ", "TranscriptSets:Users")[0];
$setname = $rows['name'];
$setowner = $rows['LastName'] . " " . $rows['FirstName'];
echo "<div class=section>";
echo "<h3>Edit Set Details: $setname</h3>";
echo "<form action='index.php?page=mane&t=edit&setid=$setid' method=POST />\n";
echo "<p><span class=emph>Owner:</span> $setowner<br/>";
echo "<span class=emph>Name: </span><br/><input style='margin-left:1em;' type=text size=30 name='setname' value='$setname' /><br/>";
echo "</p><p><span class=emph>Included Transcripts:</span> <br/>";
echo "<p><span class=strong>Note:</span> Changes are stored immediately !</p>";
echo "<table style='width:90%;border-bottom:1px solid #333;margin-left:1em;' id=genetable cellspacing=0 ><tr><th class=top>Delete</th><th class=top>Transcript ID</th><th class=top>Comment</th><th class=top>Entry Type <img src='Images/layout/icon_info.gif' style='height:0.9em' title='Extra: Add transcript to NCBI/Select; Discard: Remove from NCBI/Select'/> </th></tr>";
// get transcripts
$rows = runQuery("SELECT `nm_id`, `Comments`, `favourite` FROM `TranscriptSets_Data` WHERE `set_id` = '$setid' ", "TranscriptSets_Data");
foreach ($rows as $key => $row) {
    // one line per transcript, linked to javascript for updates.
    $nm_id = $row['nm_id'];
    $line = "<tr id='row[" . $nm_id . "]'>";
    $line .= "<td><span title='Delete Transcript From Set' onClick=\"DeleteSetTranscript('$setid','$nm_id')\" ><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></span></td>";
    $line .= "<td>$nm_id</td>";
    $line .= "<td><input onChange=\"UpdateSetComment('$setid','$nm_id')\" type=text id='comment[" . $nm_id . "]' value=\"" . str_replace('"', '&quot;', $row['Comments']) . "\" size=60 /><input type=hidden id='orig_comment[" . $nm_id . "]' value=\"" . str_replace('"', '&quot;', $row['Comments']) . "\" /></td>";
    $line .= "<td><select onChange=\"UpdateSetType('$setid','$nm_id')\" id='type[" . $nm_id . "]'>";

    if ($row['favourite'] == 1) {
        $line .= "<option value=1 selected>Extra</option><option value='-1'>Discard</option>";
    } elseif ($row['favourite'] == -1) {
        $line .= "<option value=1 >Extra</option><option value='-1' selected>Discard</option>";
    } else {
        $line .= "<option value=0 disabled selected></option><option value=1>Extra</option><option value='-1'>Discard</option>";
    }
    $line .= "</select></td>";
    $line .= "</tr>";
    echo $line;
}
echo "</table>";
// new Transcripts
echo "</p><p><span class=emph>Add Extra Transcripts:</span> </p>";
echo "<p class=indent>The format is tab/comma seperated, with at least the first column.<br/>&nbsp;Column Order is : NM_ID.version (RefSeq), Extra[default]/Discard, Comment (text/url/...). <br/>&nbsp;Comma's in the comments column are allowed. <br/>&nbsp;Results will be presented in the next step for validation. <br/>";
echo "<textarea style='margin-left:1em;' name=ExtraTranscripts rows=15 cols=50></textarea>";
echo "</p><p><input type=submit class=button name='Update' Value='Save'  /></form></p>";
echo "</div>";
