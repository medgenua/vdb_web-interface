<?php
// posted values?
$rsid = (isset($_POST['rsid'])) ? $_POST['rsid'] : '';
if ($rsid == '') {
    // try get
    $rsid = (isset($_GET['rsid'])) ? $_GET['rsid'] : '';
}
//submitted changes.
if (isset($_POST['SaveRSchanges'])) {
    if ($rsid == '') {
        echo "problem: rsid not set. please report.<br/>";
        exit;
    }
    // posted values.
    $rsname = addslashes($_POST['rsname']);
    $rsdesc = addslashes($_POST['rsdesc']);
    $rspublic = (isset($_POST['rspublic'])) ? 1 : 0;
    $fset = (isset($_POST['fset'])) ? $_POST['fset'] : '';
    $asets = implode(",", array_keys($_POST['asets']));
    $csets = implode(",", array_keys($_POST['checkboxes']));
    $vspace = $_POST['vspace'];
    doQuery("UPDATE `Report_Sections` SET Name = '$rsname', Description = '$rsdesc', Public = '$rspublic', FilterSet = '$fset', Annotations = '$asets', checkboxes = '$csets',vspace = '$vspace' WHERE rsid = '$rsid'", "Report_Sections");
}

// get all accessible sections
//  1. private & Public
$rows = runQuery("SELECT rsid, Name, Description,Owner FROM `Report_Sections` WHERE Owner = '$userid' OR Public = 1", "Report_Sections");
$rsids = array('private' => array(), 'public' => array(), 'shared' => array());
foreach ($rows as $row) {
    if ($row['Owner'] == $userid) {
        $rsids['private'][$row['rsid']]['name'] = $row['Name'];
        $rsids['private'][$row['rsid']]['description'] = $row['Description'];
    } else {
        $rsids['public'][$row['rsid']]['name'] = $row['Name'];
        $rsids['public'][$row['rsid']]['description'] = $row['Description'];
    }
}
//  2. shared
$rows = runQuery("SELECT rs.rsid, rs.Name, rs.Description FROM `Report_Sections` rs JOIN `Users_x_Report_Sections` urs ON urs.rsid = rs.rsid WHERE urs.uid = '$userid'", "Report_Sections");
foreach ($rows as $row) {
    $rsids['shared'][$row['rsid']]['name'] = $row['Name'];
    $rsids['shared'][$row['rsid']]['description'] = $row['Description'];
}

echo "<div class=section><h3>Manage report section</h3>";
if (count($rsids) == 0) {
    echo "<p>No accessible report sections found. Please create the first by <a href='index.php?page=report&t=new'>clicking here</a></p>";
    exit;
}
echo "<p><form action='index.php?page=report&t=manage' method=POST><span class=emph>Pick a section:</span> &nbsp; <select name=rsid>";
$done = array();
if (count($rsids['private']) > 0) {
    echo "<optgroup label='Private Sections'>";
    foreach ($rsids['private'] as $key => $array) {
        $rsid = ($rsid == '') ? $key : $rsid;
        $selected = ($key == $rsid) ? 'selected' : '';
        echo "<option value='$key' $selected>" . $array['name'] . "</option>";
        $done[$key] = 1;
    }
    echo "</optgroup>";
}
if (count($rsids['shared']) > 0) {
    echo "<optgroup label='Shared Sections'>";
    foreach ($rsids['shared'] as $key => $array) {
        if (isset($done[$key])) continue;
        $rsid = ($rsid == '') ? $key : $rsid;
        $selected = ($key == $rsid) ? 'selected' : '';
        echo "<option value='$key' $selected>" . $array['name'] . "</option>";
        $done[$key] = 1;
    }
    echo "</optgroup>";
}
if (count($rsids['public']) > 0) {
    echo "<optgroup label='Public Sections'>";
    foreach ($rsids['public'] as $key => $array) {
        if (isset($done[$key]));
        $rsid = ($rsid == '') ? $key : $rsid;
        $selected = ($key == $rsid) ? 'selected' : '';
        echo "<option value='$key' $selected>" . $array['name'] . "</option>";
        $done[$key] = 1;
    }
    echo "</optgroup>";
}
echo "</select> &nbsp; <input type=submit class=button value='Select' name='SelectRSID' /></form></p>";

if ($rsid == '') {
    exit;
}
// for selected: 
// 0. get details & permissions
$edit = $share = 0;
$section_details = runQuery("SELECT Name, Description, Owner, Public, FilterSet, Annotations,checkboxes,vspace FROM `Report_Sections` WHERE rsid = '$rsid'", "Report_Sections")[0];
if ($section_details['Owner'] == $userid) {
    $edit = 1;
    $share = 1;
} else {
    $row = runQuery("SELECT edit, full FROM  `Users_x_Report_Sections` WHERE rsid = '$rsid' AND uid = '$userid'", "Users_x_Report_Sections")[0];
    $edit = $row['edit'];
    $share = $row['full'];
}
/////////////
// 1. edit //
/////////////
if ($edit == 1) {
    // general
    echo "<form action='index.php?page=report&t=manage' method=POST><input type=hidden name=rsid value='$rsid'/>";
    echo "<p><span class=emph>Section details:</span><ul>";
    echo "<li title='Short, informative title'> Section Title: <br/>&nbsp;<input type=text name='rsname' value='" . stripslashes($section_details['Name']) . "' size=50 /></li>";
    echo "<li title='Some additional information on the intended contents of the section'>Description: <br/>&nbsp;<textarea name='rsdesc' rows=3 cols=50>" . stripslashes($section_details['Description']) . "</textarea></li>";
    $checked = ($section_details['Public'] == 1) ? "checked='checked'" : '';
    echo "<li title='Shoud the section be accessible by all users'>Public: <input type=checkbox name='rspublic' $checked></li>";
    echo "</ul>";
    echo "</p>";
    //FILTER TO APPLY
    echo "<p><span class=emph>Applied filter settings:</span> <br>&nbsp;- Filtersettings listed here are predefined on, and saved from the 'Variant Filter' page. <br/>&nbsp;- If an included filter is deleted, this will be mentioned in the report. <br/>";
    $table = ''; // "<table style='margin-left:1em'><tr><th class=top>Include><th>Name</th><th>Description</th><th>Created</th></tr>";
    // get private filters
    $rows = runQuery("SELECT fid, FilterName, Comments, Created FROM `Users_x_FilterSettings` WHERE uid = '$userid' ORDER BY FilterName", "Users_x_FilterSettings");
    foreach ($rows as $row) {
        if ($row['Comments'] == '') {
            $row['Comments'] = '-';
        }
        $selected = ($row['fid'] == $section_details['FilterSet']) ? 'checked="checked"' : '';
        $table .= "<tr><td><input type=radio name='fset' value='" . $row['fid'] . "' $selected></td><td>" . $row['FilterName'] . "</td><td>" . $row['Comments'] . "</td><td>" . substr($row['Created'], 0, 10) . "</td></tr>";
    }
    // get shared filters
    $rows = runQuery("SELECT uf.fid, uf.FilterName, uf.Comments, uf.Created FROM `Users_x_FilterSettings` uf JOIN `Users_x_Shared_Filters` usf ON uf.fid = usf.fid WHERE usf.uid = '$userid' ORDER BY FilterName", "Users_x_FilterSettings");
    foreach ($rows as $row) {
        if ($row['Comments'] == '') {
            $row['Comments'] = '-';
        }
        $selected = ($row['fid'] == $section_details['FilterSet']) ? 'checked="checked"' : '';
        $table .= "<tr><td><input type=radio name='fset' value='" . $row['fid'] . "' $selected></td><td>" . $row['FilterName'] . "</td><td>" . $row['Comments'] . "</td><td>" . substr($row['Created'], 0, 10) . "</td></tr>";
    }

    if ($table != '') {
        echo "<table style='margin-left:1em;width:50%' cellspacing=0><tr><th class=top >Include</th><th class=top>Name</th><th class=top>Description</th><th class=top>Created</th></tr>$table<tr><td colspan=4 class=last>&nbsp;</td></tr></table>";
    } else {
        echo "<span class=emph>PROBLEM:</span> No filtersettings available. Please create a filter set first on <a href='index.php?page=variants'>the 'Variant Filter'<a> page.";
    }
    echo "</p><p>";
    // ANNOTATIONS TO INCLUDE
    echo "<span class=emph>Included annotations:</span> <br>&nbsp;- Annotation sets listed here are predefined on, and saved from the 'Variant Filter' page. <br/>&nbsp;- If an included set is deleted, this will be mentioned in the report. <br/>";
    $table = '';
    // get private annos
    $annos = explode(",", $section_details['Annotations']);
    $rows = runQuery("SELECT aid, AnnotationName, Comments, Created FROM `Users_x_Annotations` WHERE uid = '$userid' ORDER BY AnnotationName", "Users_x_Annotations");
    foreach ($rows as $row) {
        if ($row['Comments'] == '') {
            $row['Comments'] = '-';
        }
        $checked = (in_array($row['aid'], $annos)) ? 'checked="checked"' : '';
        $table .= "<tr><td><input type=checkbox name='asets[" . $row['aid'] . "]' $checked></td><td>" . $row['AnnotationName'] . "</td><td>" . $row['Comments'] . "</td><td>" . substr($row['Created'], 0, 10) . "</td></tr>";
    }
    // get shared annotations
    $rows = runQuery("SELECT ua.aid, ua.AnnotationName, ua.Comments, ua.Created FROM `Users_x_Annotations` ua JOIN `Users_x_Shared_Annotations` usa ON ua.aid = usa.aid WHERE usa.uid = '$userid' ORDER BY AnnotationName", "Users_x_Annotations");
    foreach ($rows as $row) {
        if ($row['Comments'] == '') {
            $row['Comments'] = '-';
        }
        $checked = (in_array($row['aid'], $annos)) ? 'checked="checked"' : '';
        $table .= "<tr><td><input type=checkbox name='asets[" . $row['aid'] . "]' $checked></td><td>" . $row['AnnotationName'] . "</td><td>" . $row['Comments'] . "</td><td>" . substr($row['Created'], 0, 10) . "</td></tr>";
    }

    if ($table != '') {
        echo "<table style='margin-left:1em;width:50%' cellspacing=0><tr><th class=top >Include</th><th class=top>Name</th><th class=top>Description</th><th class=top>Created</th></tr>$table<tr><td colspan=4 class=last>&nbsp;</td></tr></table>";
    } else {
        echo "<span class=emph>PROBLEM:</span> No filtersettings available. Please create a filter set first on <a href='index.php?page=variants'>the 'Variant Filter'<a> page.";
    }
    echo "</p><p>";
    // CHECKBOXES.
    echo "<span class=emph>Included checkbox lists:</span> <br>&nbsp;- Checkbox lists are defined and managed on the <a href='index.php?page=report&type=checkboxes'>'Manage CheckBox Lists'</a> page . <br/>&nbsp;- CheckBoxes are listed in PDF reports below each variant entry.<br/>";
    $activated = explode(",", $section_details['checkboxes']);
    $table = '';
    // get private checkboxes
    $chbxs = array();
    $rows = runQuery("SELECT cid,Title,Options FROM `Report_Section_CheckBox` WHERE uid = '$userid' ORDER BY Title", "Report_Section_CheckBox");
    foreach ($rows as $row) {
        if ($row['Options'] == '') {
            $row['Options'] = '-';
        }
        $row['Options'] = str_replace("||", "; ", $row['Options']);
        $checked = (in_array($row['cid'], $activated)) ? 'checked="checked"' : '';
        $table .= "<tr><td><input type=checkbox name='checkboxes[" . $row['cid'] . "]' $checked></td><td>" . $row['Title'] . "</td><td>" . $row['Options'] . "</td></tr>";
        $chbxs[] = $row['cid'];
    }
    // get shared checkboxes
    $rows = runQuery("SELECT rsc.cid,rsc.Title,rsc.Options FROM `Report_Section_CheckBox` rsc JOIN `Users_x_Report_Sections_CheckBox` ursc ON rsc.cid = ursc.cid WHERE ursc.uid = '$userid' ORDER BY rsc.Title", "Report_Section_CheckBox");
    foreach ($rows as $row) {
        // seen in private
        if (in_array($row['cid'], $chbxs)) {
            continue;
        }
        if ($row['Options'] == '') {
            $row['Options'] = '-';
        }
        $row['Options'] = str_replace("||", "; ", $row['Options']);
        $checked = (in_array($row['cid'], $activated)) ? 'checked="checked"' : '';
        $table .= "<tr><td><input type=checkbox name='checkboxes[" . $row['cid'] . "]' $checked></td><td>" . $row['Title'] . "</td><td>" . $row['Options'] . "</td></tr>";
        $chbxs[] = $row['cid'];
    }
    // public checkboxes
    $rows = runQuery("SELECT cid,Title,Options FROM `Report_Section_CheckBox` WHERE Public = 1 ORDER BY Title", "Report_Section_CheckBox");
    foreach ($rows as $row) {
        // seen in private
        if (in_array($row['cid'], $chbxs)) {
            continue;
        }
        if ($row['Options'] == '') {
            $row['Options'] = '-';
        }
        $row['Options'] = str_replace("||", "; ", $row['Options']);
        $checked = (in_array($row['cid'], $activated)) ? 'checked="checked"' : '';
        $table .= "<tr><td><input type=checkbox name='checkboxes[" . $row['cid'] . "]' $checked></td><td>" . $row['Title'] . "</td><td>" . $row['Options'] . "</td></tr>";
        $chbxs[] = $row['cid'];
    }

    if ($table != '') {
        echo "<table style='margin-left:1em;width:50%' cellspacing=0><tr><th class=top >Include</th><th class=top>Title</th><th class=top>Options</th></tr>$table<tr><td colspan=4 class=last>&nbsp;</td></tr></table>";
    } else {
        echo "<br/><span class=emph style='margin-left:1em;'>PROBLEM:</span> No checkboxes available. Please create a checkbox list first on <a href='index.php?page=report&type=checkboxes'>'Manage CheckBox Lists'<a>.";
    }
    // include vspace.
    echo "<span class=emph>Include whitespace:</span> <br>&nbsp;- Create room for notes below each variant / checkbox list.<br/>&nbsp;- Select the number of blank lines to include: &nbsp; <select id=vspace name=vspace>";
    for ($i = 0; $i <= 5; $i++) {
        $checked = ($i == $section_details['vspace']) ? 'selected' : '';
        echo "<option value=$i $checked >$i</option>";
    }
    echo "</select>";


    // submit.
    echo "</p><p><input type=submit class=button name='SaveRSchanges' value='Save Changes' /></form></p>";
}
/////////////////
// 2. READONLY //
/////////////////
else {
    // general
    echo "<form action='index.php?page=report&t=manage' method=POST><input type=hidden name=rsid value='$rsid'/>";
    echo "<p><span class=emph>Section details:</span><ul>";
    echo "<li title='Short, informative title'> Section Title: " . stripslashes($section_details['Name']) . "</li>";
    echo "<li title='Some additional information on the intended contents of the section'>Description: <br/>&nbsp;<textarea readonly rows=3 cols=50>" . stripslashes($section_details['Description']) . "</textarea></li>";
    $checked = ($section_details['Public'] == 1) ? 'Yes' : 'No';
    echo "<li title='Shoud the section be accessible by all users'>Public: $checked</li>";
    echo "</ul>";
    echo "</p>";
    echo "<p><span class=emph>Applied filter settings:</span> <br>";
    // get filter details
    $row = array_shift(...[runQuery("SELECT fid, FilterName, Comments, Created FROM `Users_x_FilterSettings` WHERE fid = '" . $section_details['FilterSet'] . "'", "Users_x_FilterSettings")]);
    if (empty($row)) {
        echo " &nbsp <span class=emph>PROBLEM:</span> Applied filter was deleted, and you don't have permission to select an alternative.";
    } else {
        if ($row['Comments'] == '') {
            $row['Comments'] = '-';
        }
        echo " &nbsp; - Name: " . $row['FilterName'] . "<br/>";
        echo " &nbsp; - Comments: " . $row['Comments'] . "<br/>";
        echo " &nbsp; - Created: " . substr($row['Created'], 0, 10);
    }
    echo "</p><p>";
    echo "<span class=emph>Included annotations:</span> <br/>";
    $annos = explode(",", $section_details['Annotations']);
    if (count($annos) == 0) {
        echo " &nbsp; None.";
    } else {
        echo "<table style='margin-left:1em;width:50%' cellspacing=0><tr><th class=top>Name</th><th class=top>Description</th><th class=top>Created</th></tr>";
        foreach ($annos as $key => $aid) {
            $row = runQuery("SELECT AnnotationName, Comments, Created FROM `Users_x_Annotations` WHERE aid = '$aid'", "Users_x_Annotations")[0];
            if ($row['Comments'] == '') {
                $row['Comments'] = '-';
            }
            echo "<tr><td>" . $row['AnnotationName'] . "</td>";
            echo "<td>" . $row['Comments'] . "</td>";
            echo "<td>" . substr($row['Created'], 0, 10) . "</td></tr>";
        }
        echo "<tr><td colspan=3 class=last>&nbsp;</td></tr></table>";
    }
    echo "</p><p>";
}
