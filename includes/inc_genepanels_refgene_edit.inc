<?php
// gpid set?
if (!isset($_GET['gpid']) || $_GET['gpid'] == '' || !is_numeric($_GET['gpid'])) {
    echo "<div class=section>";
    echo "<h3>Invalid Panel ID</h3>";
    echo "<p>The specified panel-id was not valid.</p>";
    echo "</div>";
    exit;
}
$gpid = $_GET['gpid'];
//allowed?
$rows = runQuery("SELECT rw FROM `GenePanels_x_Users` WHERE gpid = '$gpid' AND uid = '$userid'", "GenePanels_x_Users");
if ($rows[0]['rw'] != 1) {
    echo "<div class=section>";
    echo "<h3>Insufficient Privileges</h3>";
    echo "<p>You are not allowed to delete this panel.</p>";
    echo "</div>";
    exit;
}
// process
if (isset($_POST['Update'])) {
    // general info
    $gpname = addslashes($_POST['gpname']);
    //$gpid = $_POST['gpid'];
    $gpdesc = addslashes($_POST['gpdesc']);
    doQuery("UPDATE `GenePanels` SET Name = '$gpname', Description = '$gpdesc' WHERE id = '$gpid'", "GenePanels");
    // Current Genes
    $nrgenes = $_POST['nrrows'];
    $reflinkread = 0;
    $reflink = array();
    $issues = '';
    $symbols = $_POST['symbol'];
    $origs = $_POST['orig'];
    $allgids = $_POST['gid'];
    $comments = $_POST['comment'];
    $updated = 0;
    // get annovar build name.
    $r = runQuery("SELECT Value FROM `Annotation_DataValues` WHERE Annotation = 'ANNOVAR'", "Annotation_DataValues")[0];
    $abuild = $r['Value'];
    // process genes
    for ($i = 0; $i <= $nrgenes; $i++) {
        $slice = floor($i / 1000);
        $idx = $i - $slice * 1000;
        // if removed, the values are not present for this $i
        if (isset($symbols[$slice][$idx])) {
            $sym = $symbols[$slice][$idx]; //$_POST["symbol$i"];
            $origsym = $origs[$slice][$idx]; //$_POST["orig$i"];
            $gid = $allgids[$slice][$idx]; //$_POST["gid$i"];
            $comment = addslashes($comments[$slice][$idx]); // $_POST["comment$i"]);
            if ($gid == '' || !is_numeric($gid)) {
                if ($reflinkread == 0) {
                    exec("cut -f 2,13 Annotations/ANNOVAR/humandb/$abuild" . "_refGene.txt", $refgenes);
                    //exec("cut -f 2,13 Annotations/ANNOVAR/humandb/$abuild" . "_ncbiGene.txt", $refgenes);
                    foreach ($refgenes as $key => $value) {
                        $value = strtoupper($value);
                        $a = explode("\t", $value);
                        if (!array_key_exists($a[1], $reflink)) {
                            $reflink[$a[1]] = array();
                            $reflink[$a[1]]['NM'] = array();
                            $reflink[$a[1]]['gid'] = array();
                        }
                        $reflink[$a[1]]['NM'][$a[0]] = '1';
                    }
                    echo "nr in reflink: " . count($reflink) . "<br/>";
                    $refgenes = array();
                    exec("cut -f 1,3,7  Annotations/ANNOVAR/humandb/$abuild" . "_refLink.txt", $rl);
                    foreach ($rl as $key => $value) {
                        $value = strtoupper($value);
                        $a = explode("\t", $value);
                        if (!array_key_exists($a[0], $reflink)) {
                            continue;
                        }
                        $ar = $reflink[$a[0]]['NM'];
                        if (array_key_exists($a[1], $ar)) {
                            //valid (human) nm => store geneid
                            $reflink[$a[0]]['gid'][$a[2]] = $a[1];
                        }
                    }
                    $rl = array();
                    $reflinkread = 1;
                }
                if (array_key_exists($sym, $reflink)) {
                    $gids = $reflink[$sym]['gid'];
                    if (count($gids) == 1) {
                        foreach ($gids as $key => $value) {
                            $gid = $key;
                        }
                        $issues .= "<li>$sym : Not-Provided GeneID found in RefLink as $gid</li>";
                    }
                    // more hits
                    elseif (count($gids) > 1) {
                        $gpgids = '';
                        foreach ($$gids as $key => $value) {
                            $gpgids .= $key . ",";
                        }
                        $issues .= "<li>$sym : GeneID not provided, multiple hits in RefLink (" . substr($gpgids, 0, -1) . ") : Update skipped</li>";
                        continue;
                    } else {
                        $issues .= "<li>$sym : GeneID not provided, no hit in refLink : Update skipped</li>";
                        continue;
                    }
                } else {
                    $issues .= "<li>$sym : GeneID not provided, no hit in refLink : Update skipped</li>";
                    continue;
                }
            }
            // update
            doQuery("UPDATE `GenePanels_x_Genes` SET Symbol = '$sym', gid = '$gid', Comment = '$comment' WHERE gpid = '$gpid' AND Symbol = '$origsym'", "GenePanels_x_Genes");
            $updated = 1;
        }
    } // end of foreach current genes.
    clearMemcache("GenePanels_x_Genes");
    // novel genes.
    $newgenes = explode("\n", $_POST['ExtraGenes']);
    $gidx = -1;
    foreach ($newgenes as $key => $rowline) {
        $gidx++;
        $rowline = rtrim($rowline);
        if ($rowline == "") {
            $gidx--;
            continue;
        }
        $row = preg_split("/[,\t]/", $rowline, 3);
        $row[0] = str_replace(" ", "", $row[0]);
        $row[0] = strtoupper($row[0]);
        $sym = $row[0];
        $gid = '';
        if (isset($row[1])) {
            $row[1] = str_replace(" ", "", $row[1]);
        }
        if (isset($row[2])) {
            $comments = addslashes($row[2]);
        } else {
            $comments = "";
        }
        if (count($row) >= 2 && $row[1] != '' && is_numeric($row[1])) {
            $gid = $row[1];
        } else {
            if ($reflinkread == 0) {
                exec("cut -f 2,13 Annotations/ANNOVAR/humandb/$abuild" . "_refGene.txt", $refgenes);
                foreach ($refgenes as $key => $value) {
                    $value = strtoupper($value);
                    $a = explode("\t", $value);
                    if (!array_key_exists($a[1], $reflink)) {
                        $reflink[$a[1]] = array();
                        $reflink[$a[1]]['NM'] = array();
                        $reflink[$a[1]]['gid'] = array();
                    }
                    $reflink[$a[1]]['NM'][$a[0]] = '1';
                }
                $refgenes = array();
                exec("cut -f 1,3,7  Annotations/ANNOVAR/humandb/$abuild" . "_refLink.txt", $rl);
                foreach ($rl as $key => $value) {
                    $value = strtoupper($value);
                    $a = explode("\t", $value);
                    if (!array_key_exists($a[0], $reflink)) {
                        continue;
                    }
                    $ar = $reflink[$a[0]]['NM'];
                    if (array_key_exists($a[1], $ar)) {
                        //valid (human) nm => store geneid
                        $reflink[$a[0]]['gid'][$a[2]] = $a[1];
                    }
                }
                $rl = array();
                $reflinkread = 1;
            }
            // defined, one hit on geneid : ok
            if (array_key_exists($row[0], $reflink)) {
                $gids = $reflink[$row[0]]['gid'];
                if (count($gids) == 1) {
                    foreach ($gids as $key => $value) {
                        $gid = $key;
                    }
                    $issues .= "<li>$sym : Not-Provided GeneID found in RefLink as $gid</li>";
                }
                // more hits
                elseif (count($gids) > 1) {
                    $gpgids = '';
                    foreach ($$gids as $key => $value) {
                        $gpgids .= $key . ",";
                    }
                    $issues .= "<li>$sym : GeneID not provided, multiple hits in RefLink (" . substr($gpgids, 0, -1) . ") : Gene skipped</li>";
                    continue;
                } else {
                    $issues .= "<li>$sym : GeneID not provided, no hit in refLink : Update skipped</li>";
                    continue;
                }
            } else {
                $issues .= "<li>$sym : GeneID not provided, no hit in refLink : Update skipped</li>";
                continue;
            }
        }
        // does it exist ?
        $rows = runQuery("SELECT Symbol FROM `GenePanels_x_Genes` WHERE gpid = '$gpid' AND Symbol = '$sym'", "GenePanels_x_Genes");
        if (count($rows) > 0) {
            $issues .= "<li>$sym : Duplicate entry: Gene already exists in this genepanel, and was skipped.</li>";
            continue;
        }
        // insert
        $doQuery("INSERT INTO `GenePanels_x_Genes` (gpid, Symbol, gid, Comment) VALUES ('$gpid','$sym','$gid','$comments')", "GenePanels_x_Genes");
        $updated = 1;
    }
    if ($issues != '') {
        echo "<div class=section>";
        echo "<h3 style='color:red'>Update issues were reported</h3>";
        echo "<p>The following issues were reported by the update script:<ol>$issues</ol></p></div>";
    }
    // launch annotation update.
    if ($updated == 1) {
        // set gene panel update trigger
        $command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/Annotations && echo '$gpid' >> update.panel.txt \") >/dev/null 2>&1";
        system($command);
        // remove bed files.
        $command = "rm -f $scriptdir/BED_Files/$gpid.full.* $scriptdir/BED_Files/$gpid.cds.*";
        system($command);
    }
    if ($issues == '') {
        echo "<meta http-equiv='refresh' content='0;URL=index.php?page=genepanels&t=edit&gpid=$gpid'>\n";
    }
}

// print current state.

$rows = runQuery("SELECT gp.Name, gp.Description, u.LastName, u.FirstName, gp.LastEdit FROM `GenePanels` gp JOIN `Users` u ON gp.Owner = u.id WHERE gp.id = '$gpid' ", "GenePanels:Users")[0];
$gpname = $rows['Name'];
$gpdesc = $rows['Description'];
$gpowner = $rows['LastName'] . " " . $rows['FirstName'];
$gples = explode(" ", $rows['LastEdit']);
$gple  = $gples[0];
echo "<div class=section>";
echo "<h3>Edit Panel Details: $gpname</h3>";
echo "<form action='index.php?page=genepanels&t=edit&gpid=$gpid' method=POST />\n";
echo "<p><span class=emph>Owner:</span> $gpowner<br/>";
echo "<span class=emph>Last Edit:</span> $gple</p><p>";
echo "<span class=emph>Name: </span><br/><input style='margin-left:1em;' type=text size=30 name='gpname' value='$gpname' /><br/>";
echo "<span class=emph>Description:</span><br/>";
echo "<textarea style='margin-left:1em;' rows=3 cols=28 name=gpdesc>$gpdesc</textarea>";
echo "</p><p><span class=emph>Included Genes:</span> <br/>";
echo "<table style='width:90%;border-bottom:1px solid #333;margin-left:1em;' id=genetable cellspacing=0 ><tr><th class=top>Delete</th><th class=top>RefSeq Symbol</th><th class=top>NCBI Gene_ID</th><th class=top>Comment</th><th class=top>Last Edit</th></tr>";
// get genes
$rows = runQuery("SELECT Symbol, gid, Comment, LastEdit FROM `GenePanels_x_Genes` WHERE gpid = '$gpid' ", "GenePanels_x_Genes");
$idx = -1;
foreach ($rows as $key => $row) {
    $idx++;
    $slice = floor($idx / 1000);
    $gidx = $idx - $slice * 1000;
    $line = "<tr id='row[$slice][$gidx]'>";
    $line .= "<td id='trash[$slice][$gidx]'><input type=hidden name='orig[$slice][$gidx]' id='orig[$slice][$gidx]' value='" . $row['Symbol'] . "' /><span id='del[$slice][$gidx]' title='Delete Gene From Panel' onClick=\"DeletePanelGene('$gpid','$slice','$gidx')\" ><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></span></td>";
    $line .= "<td><input name='symbol[$slice][$gidx]' value='" . $row['Symbol'] . "' size=20 /></td>";
    $line .= "<td><input name='gid[$slice][$gidx]' value='" . $row['gid'] . "' size=10 /></td>";
    $line .= "<td><input name='comment[$slice][$gidx]' value='" . $row['Comment'] . "' size=60 /></td>";
    $line .= "<td>" . $row['LastEdit'] . "</td>";
    $line .= "</tr>";
    echo $line;
}
echo "</table>";
//$idx--;
echo "<input type=hidden name='nrrows' value='$idx' />";
// new genes
echo "</p><p><span class=emph>Add Extra Genes:</span> </p>";
echo "<p class=indent>The format is tab/comma seperated, with at least the first column.<br/>&nbsp;Column Order is : Gene Symbol (RefSeq), GeneID (ncbi), Comment (text/url/...). <br/>&nbsp;Comma's in the comments column are allowed. <br/>&nbsp;Results will be presented in the next step for validation. In case of missing GeneIDs, these will be suggested based on the RefSeq symbol. <br/>";
echo "<textarea style='margin-left:1em;' name=ExtraGenes rows=15 cols=50></textarea>";
echo "</p><p><input type=submit class=button name='Update' Value='Save' /></form></p>";
echo "</div>";
