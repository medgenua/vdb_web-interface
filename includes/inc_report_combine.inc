<?php
// NEW  submission? 
if (isset($_POST['SubmitReport'])) {
    $rname = addslashes($_POST['rname']);
    $rdesc = addslashes($_POST['rdesc']);
    if (isset($_POST['rpublic'])) {
        $rp = 1;
    } else {
        $rp = 0;
    }
    $sections = $_POST['sections'];
    doQuery("INSERT INTO `Report_Definitions` (Name, Description, Owner, Public, Sections) VALUES ('$rname','$rdesc','$userid','$rp','$sections')", "Report_Definitions");
    echo "<p><span class=emph>Report layout saved.</span></p>";
    exit;
}


// GENERAL
echo "<div class=section>";
echo "<h3>New report layout definition:</h3>";
echo "<p>";
echo "<form action='index.php?page=report&t=combine' method=POST>";
echo "<span class=emph>Specify the report details:</span>";
echo "<ul>";
echo "<li title='Short, informative title'>Report Title: <br/>&nbsp;<input name='rname' type=text size=50></li>";
echo "<li title='Some additional information on the intended contents of the report'>Description: <br/>&nbsp;<textarea name='rdesc' rows=3 cols=50></textarea></li>";
echo "<li title='Shoud the report be accessible by all users'>Public: <input type=checkbox name='rpublic'></li>";
echo "</ul>";
echo "</p>";
// included sections.
echo "<div class='toleft w50' style='padding-right:3em'>";
echo "<p><span class=emph>Included Sections:</span></p>";
echo "<p style='padding-left:1em;'>Add or remove sections by the plus and trash icons respectively. Use the arrows to reorder the included sections.</p>";
// here comes the content .
echo "<div id='included_sections' style='padding-left:3em;'></div></div>";
// available sections
echo "<div class='toleft w50' id='available_sections'>";
echo "<p><span class=emph>Available sections:</span><br/>";
$seen = array();
$rows = runQuery("SELECT rsid, Name, Description FROM `Report_Sections` WHERE Public = '1' OR Owner = '$userid' ORDER BY Name");
foreach ($rows as $row) {
    $seen[$row['rsid']] = 1;
    echo "<div id='available_" . $row['rsid'] . "'> <img style='margin-bottom:-0.1em;height:1em;' src='Images/layout/plus-icon.png' onclick=\"AddSection('" . $row['rsid'] . "');\" /> <span id='rs_" . $row['rsid'] . "' title='" . stripslashes($row['Description']) . "' style='padding-left:0.5em;'> " . stripslashes($row['Name']) . "</span></div>";
}
echo "</p>";
echo "</div>";
echo "<br style='clear:both;'/>";
echo "<input type=hidden name=sections value='' id=sections />";
echo "<p><input type=submit class=button name='SubmitReport' value='submit' /></form></p>";
?>

<script type='text/javascript'>
    function AddSection(rsid) {
        // get dom_element
        var span = document.getElementById('rs_' + rsid);
        // remove from the availabe_div side
        document.getElementById('available_' + rsid).style.display = 'none';
        // add container div
        if (!document.getElementById('included_' + rsid)) {
            var node = document.createElement('div');
            node.id = 'included_' + rsid;
            document.getElementById('included_sections').appendChild(node);
        } else {
            // move to bottom, make visible
            var node = document.getElementById('included_' + rsid);
            var parent = node.parentNode;
            var oldChild = parent.removeChild(node);
            parent.appendChild(oldChild);
            node.style.display = '';
        }
        var node = document.getElementById('included_' + rsid);
        node.innerHTML = '<img src="Images/layout/up.png" style="height:0.9em;padding-bottom:-0.1em;" onclick="MoveUp(' + rsid + ')"/><img src="Images/layout/down.png" style="height:0.9em;padding-bottom:-0.1em;"  onclick="MoveDown(' + rsid + ')"/> <img src="Images/layout/icon_trash.gif" style="height:1em;;padding-bottom:-0.1em;" onclick="RemoveSection(' + rsid + ');"/>';
        node.appendChild(span);
        getOrder();
    }

    function RemoveSection(rsid) {
        // get dom_element
        var span = document.getElementById('rs_' + rsid);
        // take from included to availabe.
        document.getElementById('available_' + rsid).appendChild(span);
        document.getElementById('available_' + rsid).style.display = '';
        document.getElementById('included_' + rsid).style.display = 'none';
        getOrder();
    }

    function getOrder() {
        var elements = document.getElementById('included_sections').childNodes;
        var order = '';
        for (var i = 0; i < elements.length; i++) {
            order = order + elements[i].getAttribute('id').substring(9) + ',';
        }
        if (order !== '') {
            order = order.substring(0, order.length - 1)
        }
        document.getElementById('sections').value = order;
    }

    function MoveUp(rsid) {
        var node = document.getElementById('included_' + rsid);
        var parent = node.parentNode;
        var prev = node.previousSibling;

        if (prev !== null) {
            var oldChild = parent.removeChild(node);
            parent.insertBefore(oldChild, prev);
        }
        getOrder();

    }

    function MoveDown(rsid) {
        var node = document.getElementById('included_' + rsid);
        var parent = node.parentNode;
        var next = node.nextSibling;
        if (next === null) {
            return;
        }
        var next = next.nextSibling; // only insert before possible, so move two nodes down.
        if (next !== null) {
            var oldChild = parent.removeChild(node);
            parent.insertBefore(oldChild, next);
        } else {
            var oldChild = parent.removeChild(node);
            parent.appendChild(oldChild);
        }
        getOrder();
    }
</script>