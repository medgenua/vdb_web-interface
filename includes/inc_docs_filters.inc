<?php
echo "<div class=section>";
echo "<h3>Documentation : Filtering Criteria Explained </h3>";

// load xml library
require_once('xmlLib2.php');

// read the filter config xml
$configuration = my_xml2array("Filter/Filter_Options.xml");
$categories = get_value_by_path($configuration,"FilterConfiguration");
// loop main categories

foreach ($categories as $subkey => $setting) { // e.g. Family
	if (!is_numeric($subkey)) {continue;}
	//$setting = get_value_by_path($setting,"settings");
	echo "<p><table class=w75 cellspacing=0>";
	echo "<tr><th colspan=2 class=top>".$setting['name']."</th></tr>";
	foreach ($setting as $sskey => $sarray) {	// e.g. In_Parents
		if (!is_numeric($sskey)) {continue;}
		$attributes =  get_value_by_path($configuration,"FilterConfiguration/".$setting['name']."/".$sarray['name']."/settings");
		$deprecated = '';
		if (get_value_by_path($configuration,"FilterConfiguration/".$setting['name']."/".$sarray['name']."/deprecated")) {
			$deprecated = "<span style='color:red' title='This filter is based on annotations which are no longer updated for new samples'>DEPRECATED</span> ";
		}	
		$sarray['name'] = preg_replace('/^_+/','',$sarray['name']);
		$sarray['name'] = preg_replace('/___/',' (',$sarray['name']);
		$sarray['name'] = preg_replace('/__/',')',$sarray['name']);
		$sarray['name'] = preg_replace('/_/',' ',$sarray['name']);

		echo "<tr style='padding-bottom:1.5em;'><td valign=top NOWRAP>".$sarray['name']."</td>";
		$help = 'No description available';
		
		if (is_array($attributes)) {
	 		 foreach ($attributes as $key => $array) {
				if (!is_numeric($key)) {continue;}
				if ($array['name'] == 'help' ) {
					$help = $array['value'];
					//$help = preg_replace('/___/',' (',$help);
					//$help = preg_replace('/__/',')',$help);
					//$help = preg_replace('/_/',' ',$help);
					
					//break;
				}
				if ($array['name'] == 'deprecated') {
					$deprecated = "<span style='color:red' title='This filter is based on annotations which are no longer updated for new samples'>DEPRECATED</span> ";
				}
				
	  		}
		}

		echo "<td valign=top style='padding-bottom:0.75em;'>$deprecated $help</td></tr>";
	}
	echo "<tr><td colspan=2 class=last>&nbsp;</td></tr>";
	echo "</table></p>";
	#if ($setting['name'] != 'settings' && $setting['name'] != 'options') {
	#	$sublevels = 1;
	#	break;
	#}
}

echo "</div>";
?>
