<?php
// NEW  submission? 
if (isset($_POST['CreateClassifier'])) {
    $cname = addslashes($_POST['cname']);
    $cdesc = addslashes($_POST['cdesc']);
    $users = $_POST['add_users'];
    $can_add = $_POST['can_add'];
    $can_validate = $_POST['can_validate'];
    $can_remove = $_POST['can_remove'];
    $can_use = $_POST['can_use'];
    $can_share = $_POST['can_share'];

    $cid = insertQuery("INSERT INTO `Classifiers` (Name, Description, Owner) VALUES ('$cname','$cdesc','$userid')", "Classifiers");

    foreach ($users as $key => $uid) {
        $add = (in_array($uid, $can_add) ? 1 : 0);
        $val = (in_array($uid, $can_validate) ? 1 : 0);
        $del = (in_array($uid, $can_remove) ? 1 : 0);
        $use = (in_array($uid, $can_use) ? 1 : 0);
        $sha = (in_array($uid, $can_share) ? 1 : 0);

        doQuery("INSERT INTO `Users_x_Classifiers` (`uid`, `cid`, `can_use`, `can_add`,`can_validate`,`can_remove`,`can_share`,`apply_on_import`) VALUES ('$uid','$cid','$use','$add','$val','$del','$sha','0')", "Users_x_Classifiers");
    }

    echo "<p><span class=emph>Classifier Saved.</span></p>";
    exit;
}

echo "<div class=section>";
echo "<h3>Classifier definition:</h3>";
echo "<p>A classifier is a list of variants, with associated diagnostic class. If a variant in a newly imported VCF is present in an active classifier, the diagnostic class will automatically be assigned. Permissions are set up in multiple levels, being: ";
echo "<ol><li>Use classifier: read-only</li>";
echo "<li>Add variants: include new variants for review </li>";
echo "<li>Validate variants: review and (de-)activate variants in classifiers</li>";
echo "<li>Share classifier: Manage access of additional users</li></ol>";
echo "</p><p>";
echo "<form action='index.php?page=classifier&t=new' method=POST>";
echo "<span class=emph>Specify the classifier details:</span>";
echo "<ul>";
echo "<li title='Short, informative title'>Classifier Title: <br/>&nbsp;<input name='cname' type=text size=50></li>";
echo "<li title='Some additional information on the intended contents of the classifier'>Description: <br/>&nbsp;<textarea name='cdesc' rows=3 cols=50></textarea></li>";
echo "</ul>";
echo "</p>";

echo "<p><span class=emph>Specify the users who should have access. </span></p> <p>Type a name: <input type=hidden name=AddUserID id=AddUserID value='' /> <input type=text size='50' value='' name='usersource' id='searchfield' /> <input type=button  class=button name='AddUser' value='Add User' onClick='AddUsers($userid);' /> &nbsp; <span id='comment_field' style='color:red; font-style:italic;'></span>";
echo "</p>";

// permissions table.
echo "<p><table id='permtable' cellspacing=0 class=w50 style='text-align:center'>";
echo "<tr><td>&nbsp;</td><th colspan=3 class=btop>Variants</th><th class=btop colspan=2>Classifier</th></tr>";
echo "<tr><th class=btop>User</th><th class=btop>Add</th><th class=btop>Validate</th><th class=btop>Remove</th><th class=btop>Apply</th><th class=btop>Manage</th></tr>";
// you as owner.
echo "<tr id='ownerrow'><td ><input type=hidden name='add_users[]' value='$userid'/>" . $_SESSION['LastName'] . " " . $_SESSION['FirstName'] . "</td><td><input type='checkbox' name='can_add[]' value='$userid' checked /></td><td><input type='checkbox' name='can_validate[]' value='$userid' checked /></td><td><input type='checkbox' name='can_remove[]' value='$userid' checked /></td><td><input type='checkbox' name='can_use[]' value='$userid' checked /></td><td><input type='checkbox' name='can_share[]' value='$userid' checked /></td></tr>";
echo "<tr><td class=last colspan=6>&nbsp;</td></tr>";
echo "</table>";
echo "<p><input type=submit class=button name='CreateClassifier' value='Create Classifier' /></form></p>";
echo "</div>";
