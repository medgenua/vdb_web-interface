<?php
// NEW  submission? LIST AND CONFIRM.
if (isset($_POST['SubmitPanel'])) {
	$gpname = addslashes($_POST['gpname']);
	$gpdesc = addslashes($_POST['gpdesc']);
	if (isset($_POST['gppublic'])) {
		$gpp = 1;
	}
	else {
		$gpp = 0;
	}
	// posted gene list.
	$gpgenes = explode("\n",$_POST['gpgenes']);
	echo "<div class=section>";
	echo "<h3>Confirm New Gene Panel Details: $gpname</h3>\n";
	$gpid = insertQuery("INSERT INTO `GenePanels` (Name, Description, Owner, Public) VALUES ('$gpname','$gpdesc','$userid','$gpp')","GenePanels");
	doQuery("INSERT INTO `GenePanels_x_Users` (gpid, uid, rw, share) VALUES ('$gpid', '$userid', 1, 1)","GenePanels_x_Users");
	// get annovar build name.
	$r = runQuery("SELECT Value FROM `Annotation_DataValues` WHERE Annotation = 'ANNOVAR'","Annotation_DataValues")[0];
	$abuild = $r['Value'];
	

	echo "<form action='index.php?page=genepanels&t=new' method=POST><input type=hidden name='gpid' value='$gpid' />";
	echo "<input type=hidden name='gpname' value='$gpname' />";
	echo "<p class=emph>Panel Descripton:</p><p class=indent>$gpdesc</p>";
	echo "<p class=emph>Panel Genes:</p>";
	echo "<table class=indent style='width:75em' cellspacing=0><tr><th class=top>RefSeq Symbol</th><th class=top>NCBI Gene ID</th><th class=top>Comments</th></tr>\n";
	$reflink = array();
	$reflinkread = 0;
	$gidx = -1;
	// try to get gene_id from annovar refgene tables.
	foreach($gpgenes as $key => $rowline) {
		$gidx++;
		$rowline = rtrim($rowline);
		if ($rowline == "") {
			$gidx--;
			continue;
		}
		// we use two-dimensional ID-array to prevent breaking the limit of php variables.
		$slice = floor($gidx/1000);
		$row = preg_split("/[,\t]/",$rowline,3);
		$row[0] = str_replace(" ","",$row[0]);
		$row[0] = strtoupper($row[0]);
		if (isset($row[1])) {
			$row[1] = str_replace(" ","",$row[1]);
		}
		$tr = "<tr><td><input type=text size=15 name='symbol[$slice][".($gidx-$slice*1000)."]' value='".$row[0]."' /></td>";
		if (count($row) >= 2 && $row[1] != '' && is_numeric($row[1])) {
			$tr .= "<td><input type=text size=10 name='gid[$slice][".($gidx-$slice*1000)."]' value='".$row[1]."' /></td>";
		}
		else {
			// read the annovar tables once and put int array.
			 if ($reflinkread == 0) {
				exec("cut -f 2,13 $scriptdir/Annotations/ANNOVAR/humandb/$abuild"."_ncbiGene.txt",$refgenes);
				foreach($refgenes as $key => $value) {
					$value = strtoupper($value);
					$a = explode("\t",$value);
					if (!array_key_exists($a[1],$reflink)) {
						$reflink[$a[1]] = array();
						$reflink[$a[1]]['NM'] = array();
						$reflink[$a[1]]['gid'] = array();
					}
					// store as <symbol>NM<nm_id>
					// but strip the .<ver>
					$a[0] = preg_replace('/(.*)\.\d+/','$1',$a[0]);
					$reflink[$a[1]]['NM'][$a[0]] = '1';
				}
				$refgenes = array();
				exec("cut -f 1,3,7  $scriptdir/Annotations/ANNOVAR/humandb/$abuild"."_refLink.txt",$rl);
				foreach($rl as $key => $value) {
					$value = strtoupper($value);
					$a = explode("\t",$value);
					if (!array_key_exists($a[0],$reflink)) {
						continue;
					}
					$ar = $reflink[$a[0]]['NM'];
					if (array_key_exists($a[1],$ar)) {
						//valid (human) nm => store geneid
						$reflink[$a[0]]['gid'][$a[2]] = $a[1];
					}
				}
				$rl = array();
				$reflinkread = 1;
				
			}
			// defined hit on symbol : ok
			if (array_key_exists($row[0],$reflink)) {
				$gids = $reflink[$row[0]]['gid'];
				// single hit : ok.
				if (count($gids) == 1) {
					$gpgid = '';
					foreach($gids as $key => $value) {
						$gpgid = $key;
					}
					$tr .= "<td><input type=text name='gid[$slice][".($gidx-$slice*1000)."]' size=10 style='color:green;' value='".$gpgid."' /></td>";
				}
				// more hits :: highlight this entry for fixing.
				elseif (count($gids) > 1) {
					$gpgids = '';
					foreach($$gids as $key => $value) {
						$gpgids .= $key.",";
					}
					$tr .= "<td title='Multiple GeneIDs for this symbol'><input type=text size=10 style='background-color:#FF6F6F;' name='gid[$slice][".($gidx-$slice*1000)."]' value='".substr($gpgids,0,-1)."' /></td>";
				}
				// no gene ids : highlight for fixing.
				else {
					$tr .= "<td title='Symbol not found'><input type=text size=10 style='background-color:#FF6F6F;' name='gid[$slice][".($gidx-$slice*1000)."]' value='--' /></td>";
				}
			}
			// symbol not found. highlight for fixing.
			else {
				$tr .= "<td title='Symbol not found'><input type=text size=10 style='background-color:#FF6F6F;' name='gid[$slice][".($gidx-$slice*1000)."]' value='--' /></td>";
			}
		}
		if (count($row) >= 3) {
			$tr .= "<td><input type=text size=80 value='".$row[2]."' name='comment[$slice][".($gidx-$slice*1000)."]' /></td></tr>";
		}
		else {
			$tr .= "<td><input type=text size=80 value='' name='comment[$slice][".($gidx-$slice*1000)."]' /></td></tr>";
		}
		echo "$tr";
	}
	//$gidx--;
	
	echo "<tr><td class=last colspan=3>&nbsp;</td></tr>";
	echo "</table>";
	echo "</p>";
	echo "<input type=hidden name=nrgenes value='$gidx' />";
	echo "<input type=submit class=button name='CGP' Value='Confirm' /></form>";
	echo "</div>";

	exit;
}
// CONFIRMING PROVIDED VALUS AND STORE THE PANEL.
elseif (isset($_POST['CGP'])) {
	$gpid = $_POST['gpid'];
	$gpname = $_POST['gpname'];
	if (!is_numeric($gpid)) {
		echo "<div class=section>";
		echo "<h3>Invalid GenePanel-ID</h3>";
		echo "A non-numeric GenePanel ID was provided. If you believe this is an error, please contact the system administrator.</p>";
		echo "</div>";
		exit;
	}
	$skipped = '';
	$skipnr = 0;
	$nrgenes = $_POST['nrgenes'];
	$geneids = $_POST['gid'];
	$symbols = $_POST['symbol'];
	$comments = $_POST['comment'];
	for ($i = 0; $i <= $nrgenes ; $i++) {
		$slice = floor($i/1000);
		$idx = $i - $slice*1000;
		$symbol = $symbols[$slice][$idx]; //$_POST["symbol$i"];
		$gid = $geneids[$slice][$idx] ; //$_POST["gid$i"];
		$comment = addslashes($comments[$slice][$idx]); //addslashes($_POST["comment$i"]);
		if ($gid == '' || !is_numeric($gid)) {
			$skipped .= "<li>$symbol : invalid gene ID: '$gid'</li>";
			$skipnr++;
			continue;
		}
		doQuery("INSERT DELAYED INTO `GenePanels_x_Genes` (gpid, gid, Symbol, Comment) VALUES ('$gpid','$gid','$symbol','$comment')","GenePanels_x_Genes");
	}
	
	echo "<div class=section>";
	echo "<h3>Gene Panel Created : $gpname</h3>";
	$nrgenes++;
	echo "<p>$nrgenes genes were added to the panel.";
	// anything skipped? 
	if ($skipped != '') {
		echo " The following $skipnr genes were skipped:<ol>$skipped</ol>";
	}
	echo "</p>";
	echo "<p><a href='index.php?page=genepanels&t=edit&p=$gpid'>Edit Panel</a> or <a href='index.php?page=genepanels&t=manage&a=s&p=$gpid'>Share Panel</a></p>";
	echo "</div>";
	$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/Annotations && echo '$gpid' >> update.panel.txt \") >/dev/null 2>&1";
	system($command);
	exit;

}

// INITIAL FORM TO CREATE A PANEL
echo "<div class=section>";
echo "<h3>New Panel Definition:</h3>";
echo "<p>";
echo "<form action='index.php?page=genepanels&t=new' method=POST>";
echo "<span class=emph>Specify the panel details:</span>";
echo "<ul>";
echo "<li title='Short, informative Name'>Panel Name: <br/>&nbsp;<input name='gpname' type=text size=50></li>";
echo "<li title='Some additional information on how the panel was composed'>Description: <br/>&nbsp;<textarea name='gpdesc' rows=3 cols=50></textarea></li>";
echo "<li title='Should the panel be accessible by all users'>Public: <input type=checkbox name='gppublic'></li>";
echo "</ul>";
echo "</p>";

echo "<span class=emph>Specify the panel genes:</span> <br>&nbsp;The format is tab/comma seperated, with at least the first column.<br/>&nbsp;Column Order is : Gene Symbol (RefSeq), GeneID (ncbi), Comment (text/url/...). <br/>&nbsp;Comma's in the comments column are allowed. <br/>&nbsp;Results will be presented in the next step for validation. In case of missing GeneIDs, these will be suggested based on the RefSeq symbol. <br/>";
echo "<textarea name='gpgenes' rows=20 cols=50 style='margin-left:4em;margin-top:1em;'></textarea></li>";
echo "</p>";

echo "<p><input type=submit class=button name='SubmitPanel' value='submit' /></form></p>";
echo "</div>";
