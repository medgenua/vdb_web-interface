<?php
// NEW  submission? LIST AND CONFIRM.
if (isset($_POST['SubmitSet'])) {
    $setname = addslashes($_POST['setname']);

    // posted gene list.
    $setNM = explode("\n", $_POST['setNM']);
    echo "<div class=section>";
    echo "<h3>New Transcript Set Created: $setname</h3>\n";
    $setid = insertQuery("INSERT INTO `TranscriptSets` (`name`, `owner`) VALUES ('$setname','$userid')", "TranscriptSets");
    doQuery("INSERT INTO `TranscriptSets_x_Users` (`set_id`, `uid`, `edit`) VALUES ('$setid', '$userid', 1)", "TranscriptSets_x_Users");

    echo "Set Created. Redirecting to Set Management in 3 seconds.";
    echo "<meta http-equiv='refresh' content='3;url=index.php?page=mane&t=edit&setid=$setid'/>";

    echo "</div>";

    exit;
}

// INITIAL FORM TO CREATE A PANEL
echo "<div class=section>";
echo "<h3>New Transcript Set Definition:</h3>";
echo "<p>";
echo "<form action='index.php?page=mane&t=new' method=POST>";
echo "<span class=emph>Specify the set details:</span>";
echo "<p>";
echo "Set Name: &nbsp;<input name='setname' type=text size=50>";
echo "";
echo "</p>";

echo "<p><input type=submit class=button name='SubmitSet' value='submit' /></form></p>";
echo "</div>";
