<?php
$roles_updated = '';
$group_roles_updated = '';

// new checkbox made.
if (isset($_POST['SubmitCBlist'])) {
    $nr = $_POST['nrFields'];
    $options = '';
    for ($i = 1; $i <= $nr; $i++) {
        if (array_key_exists($i, $_POST['cbitems'])) {
            $value = str_replace('||', '_', $_POST['cbitems'][$i]);
            if ($value == '') {
                continue;
            }
            $options .= $value . '||';
        }
    }
    if ($options == '') {
        echo "<div class=section><h3>ERROR</h3><p>No values found for checkboxes. The list is not stored.</p></div>";
    } else {
        $options = addslashes($options);
        $title = addslashes($_POST['cbname']);
        if (array_key_exists('cbpublic', $_POST)) {
            $public = 1;
        } else {
            $public = 0;
        }
        if (array_key_exists('cbdefault', $_POST)) {
            $default = 1;
        } else {
            $default = 0;
        }
        $cid = insertQuery("INSERT INTO `Report_Section_CheckBox` (`Title`,`Options`,`uid`,`Public`,`Default`) VALUES ('$title','$options','$userid','$public','$default')", "Report_Section_CheckBox");
        // SET get.
        $_POST['cid'] = $cid;
        // add permissions.
        doQuery("INSERT INTO `Users_x_Report_Sections_CheckBox` (uid,cid,edit,full) VALUES ('$userid','$cid',1,1)", "Users_x_Report_Sections_CheckBox");
    }
}
// Updates made.
if (isset($_POST['UpdateCBlist'])) {
    $cid = $_POST['cid'];
    $nr = $_POST['nrFields'];
    $options = '';
    for ($i = 1; $i <= $nr; $i++) {
        if (array_key_exists($i, $_POST['cbitems'])) {
            $value = str_replace('||', '_', $_POST['cbitems'][$i]);
            if ($value == '') {
                continue;
            }
            $options .= $value . '||';
        }
    }
    if ($options == '') {
        echo "<div class=section><h3>ERROR</h3><p>No values found for checkboxes. The list is not stored.</p></div>";
    } else {
        $options = addslashes(substr($options, 0, -2));
        $title = addslashes($_POST['cbname']);
        if (array_key_exists('cbpublic', $_POST)) {
            $public = 1;
        } else {
            $public = 0;
        }
        if (array_key_exists('cbdefault', $_POST)) {
            $default = 1;
        } else {
            $default = 0;
        }

        doQuery("REPLACE INTO `Report_Section_CheckBox` (`cid`,`Title`,`Options`,`uid`,`Public`,`Default`) VALUES ('$cid','$title','$options','$userid','$public','$default')", "Report_Section_CheckBox");
    }
    // Set permissions.
    // users
    $users = isset($_POST['add_users']) ? $_POST['add_users'] : array();
    $can_edit = isset($_POST['can_edit']) ? $_POST['can_edit'] : array();
    $can_share = isset($_POST['can_share']) ? $_POST['can_share'] : array();
    $cid = $_POST['cid'];
    // existing entries.
    $rows = runQuery("SELECT uid FROM `Users_x_Report_Sections_CheckBox` WHERE cid = '$cid'", "Users_x_Report_Sections_CheckBox");
    $entries = array();
    foreach ($rows as $row) {
        $entries[$row['uid']] = 1;
    }
    foreach ($users as $key => $uid) {
        $edit = (in_array($uid, $can_edit)) ? "1" : "0";
        $sha = (in_array($uid, $can_share)) ? "1" : "0";
        // entry exists?
        if (array_key_exists($uid, $entries)) {
            doQuery("UPDATE `Users_x_Report_Sections_CheckBox` SET  `edit` = '$edit' , `full` = '$sha' WHERE uid = '$uid' AND cid = '$cid'", "Users_x_Report_Sections_CheckBox");
        }
        // new entry
        else {
            doQuery("INSERT INTO `Users_x_Report_Sections_CheckBox` (uid,cid,`edit`,`full`) VALUES ($uid,$cid,$edit,$sha)", "Users_x_Report_Sections_CheckBox");
            $entries[$uid] = 1;
        }
    }
    // groups.
    $groups = isset($_POST['add_groups']) ? $_POST['add_groups'] : array();
    $can_share = isset($_POST['g_can_share']) ? $_POST['g_can_share'] : array();
    $can_edit = isset($_POST['g_can_edit']) ? $_POST['g_can_edit'] : array();

    // existing entries.
    $rows = runQuery("SELECT gid FROM `Usergroups_x_Report_Sections_CheckBox` WHERE cid = '$cid'", "Usergroups_x_Report_Sections_CheckBox");
    $g_entries = array();
    foreach ($rows as $row) {
        $g_entries[$row['gid']] = 1;
    }
    foreach ($groups as $key => $gid) {
        $edit = (in_array($gid, $can_edit) ? 1 : 0);
        $sha = (in_array($gid, $can_share) ? 1 : 0);
        // entry exists?
        if (array_key_exists($gid, $g_entries)) {
            doQuery("UPDATE `Usergroups_x_Report_Sections_CheckBox` SET  `edit` = '$edit' , `full` = '$sha' WHERE gid = '$gid' AND cid = '$cid'", "Usergroups_x_Report_Sections_CheckBox");
        }
        // new entry
        else {
            doQuery("INSERT INTO `Usergroups_x_Report_Sections_CheckBox` (gid,cid,`edit`,`full`) VALUES ($gid,$cid,$edit,$sha)", "Usergroups_x_Report_Sections_CheckBox");
            $g_entries[$gid] = 1;
        }
        // process users.
        $rows = runQuery("SELECT uid FROM `Usergroups_x_User` WHERE gid = '$gid'", 'Usergroups_x_Users');
        foreach ($rows as $row) {
            $uid = $row['uid'];
            if (array_key_exists($uid, $entries)) {
                // get current permissions.
                $sr = array_shift(...[runQuery("SELECT edit,full FROM `Users_x_Report_Sections_CheckBox` WHERE cid = $cid AND uid = $uid", "Users_x_Report_Sections_CheckBox")]);
                $l_edit = max($sr['edit'], $edit);
                $l_sha = max($sr['full'], $sha);
                doQuery("UPDATE `Users_x_Report_Sections_CheckBox` SET `edit` = '$l_edit', `full` = '$l_sha' WHERE cid = '$cid' AND uid = '$uid'", "Users_x_Report_Sections_CheckBox");
            } else {
                doQuery("INSERT INTO `Users_x_Report_Sections_CheckBox` (uid, cid, edit,full) VALUES ('$uid','$cid',$l_edit,$l_sha)", "Users_x_Report_Sections_CheckBox");
            }
        }
    }
}
/////////////////
// EDIT A LIST //
/////////////////
echo "<div class=section ><h3>Manage CheckBox Lists</h3>";
echo "<p>Checkbox lists can be added to PDF reports and 'saved result sets', to facilitate reviewing and commenting in a collaborative setting. To add a list to a PDF report, specify it in the report configuration. To add it to a saved result set, either specify it while saving, or specify it here to be added to each result set you save.</p>";
echo "<form action='index.php?page=report&t=checkboxes' method=POST>";
echo "<p>Select a list to edit: <select id='cid' name='cid'><option value='0' disabled selected> </option>";
// private
$rows = runQuery("SELECT rsc.Title,rsc.cid,rsc.Options,rsc.Public FROM `Report_Section_CheckBox` rsc WHERE rsc.uid = '$userid'", "Report_Section_CheckBox");
$done = array();
$options = '';
foreach ($rows as $row) {
    // skip seen.
    if (isset($done[$row['cid']])) {
        continue;
    }
    $selected = (isset($_POST['cid']) && $_POST['cid'] == $row['cid']) ? 'selected' : '';
    $done[$row['cid']] = 1;
    $options .= "<option value='" . $row['cid'] . "' $selected>" . $row['Title'] . "</option>";
}

$rows = runQuery("SELECT ursc.edit, ursc.full,rsc.Title,rsc.cid,rsc.Options,rsc.Public FROM `Report_Section_CheckBox` rsc JOIN `Users_x_Report_Sections_CheckBox` ursc ON rsc.cid = ursc.cid WHERE ursc.uid = '$userid'", "Report_Section_CheckBox:Users_x_Report_Sections_Checkbox");
foreach ($rows as $row) {
    // skip seen.
    if (isset($done[$row['cid']])) {
        continue;
    }
    $selected = (isset($_POST['cid']) && $_POST['cid'] == $row['cid']) ? 'selected' : '';
    $done[$row['cid']] = 1;
    $options .= "<option value='" . $row['cid'] . "' $selected>" . $row['Title'] . "</option>";
}
if ($options != '') {
    echo "<optgroup label='Private Lists'>$options</optgroup>";
    $options = '';
}
// public
$rows = runQuery("SELECT rsc.Title,rsc.cid,rsc.Options,rsc.Public FROM `Report_Section_CheckBox` rsc WHERE rsc.Public = '1'", "Report_Section_CheckBox");
$options = '';
foreach ($rows as $row) {
    // skip seen.
    if (isset($done[$row['cid']])) {
        continue;
    }
    $selected = (isset($_POST['cid']) && $_POST['cid'] == $row['cid']) ? 'selected' : '';
    $done[$row['cid']] = 1;
    $options .= "<option value='" . $row['cid'] . "' $selected>" . $row['Title'] . "</option>";
}
if ($options != '') {
    echo "<optgroup label='Public Lists'>$options</optgroup>";
    $options = '';
}
$selected = ((isset($_GET['cid']) && $_GET['cid'] == 'new') || (isset($_POST['cid']) && $_POST['cid'] == 'new')) ? 'selected' : '';
echo "<optgroup label='New List'><option value='new' $selected>Create a new list</option></optgroup><select>";
echo " &nbsp; <input type=submit class=button name='SelectCBList' value='Select'></form></p></div>";

if ((isset($_POST['cid']) &&  $_POST['cid'] == 'new') || (isset($_GET['cid']) &&  $_GET['cid'] == 'new')) {
    // INITIAL FORM TO CREATE A PANEL
    echo "<div class=section id=NewCheckBoxList>";
    echo "<h3>New CheckBox List: </h3>";
    echo "<p>";
    echo "<form action='index.php?page=report&t=checkboxes' method=POST>";
    echo "<span class=emph>Specify the details:</span>";
    echo "<ul>";
    echo "<li title='Short, informative Name'>List Title: <br/>&nbsp;<input name='cbname' type=text size=50></li>";
    //echo "<li title='Some additional information on how the panel was composed'>Description: <br/>&nbsp;<textarea name='gpdesc' rows=3 cols=50></textarea></li>";
    echo "<li>Checkbox labels:<ol id='cblist'>";
    echo "   <li><input type=text size=50 name='cbitems[1]' /></li>";
    echo "   </ol><span style='padding-left:4em;font-size:0.9em;font-style:italic' onclick='AddCBitem()'>(add item)</span><input type=hidden id='nrFields' name='nrFields' value='1' /></li>";
    echo "<li title='Should the panel be accessible by all users'><input type=checkbox name='cbpublic'> : Public</li>";
    echo "<li title='Should the panel be added to each saved set of results'> <input type=checkbox name='cbdefault'> : Default</li>";
    echo "</ul>";
    echo "</p>";

    echo "<p><input type=submit class=button name='SubmitCBlist' value='submit' /></form></p>";
    echo "</div>";
} elseif (isset($_POST['cid'])) {
    $cid = $_POST['cid'];
    // give some details && options to edit.
    $rw = $full = $r = 0;
    $row = array_shift(...[runQuery("SELECT `Title`,`Options`,`uid`,`Public`,`Default` FROM `Report_Section_CheckBox` WHERE cid = '$cid'", "Report_Section_CheckBox")]);
    // valid id?
    if (count($row) == 0) {
        echo "<div class=section><h3>ERROR: Invalid list id provided</h3><p>The provided id '$cid' was not found in the database</p></div>";
        exit;
    }
    $title = stripslashes($row['Title']);
    $options = explode('||', stripslashes($row['Options']));
    $r = ($row['Public'] == 1) ? '1' : '';
    $public = ($row['Public'] == 1) ? 'checked' : '';
    $default = ($row['Default'] == 1) ? 'checked' : '';
    if ($row['uid'] == $userid) {
        // owner has full access.
        $rw = $full = $r = 1;
    } else {
        // check permissions.
        $row = array_shift(...[runQuery("SELECT edit, full FROM `Users_x_Report_Sections_CheckBox` WHERE uid = '$userid' AND cid = '$cid'", "Users_x_Report_Sections_CheckBox")]);
        if (count($row) > 0) {
            $rw = $row['edit'];
            $full = $row['full'];
        }
    }
    if ($r == 0) {
        echo "<div class=section><h3>ERROR: No access to requested list</h3><p>You have insufficient permission to access this resource.</p></div>";
        exit;
    }
    // list details.
    echo "<div class=section>";
    echo "<h3>Edit CheckBox List: </h3>";
    echo "<p>";
    echo "<form action='index.php?page=report&t=checkboxes' method=POST>";
    echo "<input type=hidden name='cid' value='$cid' />";
    echo "<span class=emph>List details:</span>";
    echo "<ul>";
    echo "<li title='Short, informative Name'>List Title: &nbsp; ";
    if ($rw == 0) {
        echo "$title</li>";
    } else {
        echo "<input name='cbname' type=text size=50 value='$title' ></li>";
    }
    echo "<li>Checkbox labels:<ol id='cblist'>";
    $i = 0;
    foreach ($options as $key => $value) {
        $i++;
        if ($rw == 0) {
            echo "<li id='item$i'>$value</li>";
        } else {
            $minus = ($i > 1) ? "<img src='Images/layout/minus-icon.png' style='height:0.9em;margin-left:1em;' onClick=\"RemoveCBitem($i)\" title='Remove this item'/>" : '';
            echo "   <li id='item$i'><input type=text size=50 name='cbitems[$i]' value='$value' />$minus</li>";
        }
    }
    echo "</ol>";
    if ($rw == 1) {
        echo "   <span style='padding-left:4em;font-size:0.9em;font-style:italic' onclick='AddCBitem()'>(add item)</span><input type=hidden id='nrFields' name='nrFields' value='$i' />";
    }
    echo "</li>";

    echo "<li title='Should the checkboxes be accessible by all users'>";
    if ($rw == 1) {
        echo "<input type=checkbox name='cbpublic' value=1 $public> : Public</li>";
    } else {
        $yesno = array(0 => 'no', 1 => 'yes');
        echo $yesno[$public] . "</li>";
    }
    echo "<li title='Should the checkboxes be added to each saved set of results'>";
    if ($rw == 1) {
        echo "<input type=checkbox name='cbdefault' value=1 $default> : Default</li>";
    } else {
        $yesno = array(0 => 'no', 1 => 'yes');
        echo $yesno[$default] . "</li>";
    }

    echo "</ul>";
    echo "</p>";

    // sharing.

    if ($full == 1) {
        echo "<p><span class=emph>Set access levels</span></p>";
        echo "<p> Permissions are set up in multiple levels, being: ";
        echo "<ol><li>Use: read-only usage</li>";
        echo "<li>Edit: Change list names, add or remove entries </li>";
        echo "<li>Share: Manage access of additional users</li></ol>";
        echo "</p><p>";
        ////////////
        // GROUPS //
        ////////////
        echo "<span class=emph>Specify additional usergroups who should have access. </span></p>";
        echo " <p><select id='AddGroupSelect'><option value='-' selected disabled></option>";
        // get my affiliation.
        $row = runQuery("SELECT a.id, a.name FROM `Affiliation` a JOIN `Users` u ON u.Affiliation = a.id WHERE u.id = $userid", "Affiliation:Users")[0];
        $myaffi = $row['id'];
        // groups
        $rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug JOIN `Usergroups_x_Report_Sections_CheckBox` us ON ug.id = us.gid WHERE us.cid = $cid", "Usergroups:Usergroups_x_Report_Sections_CheckBox");
        $shared_groups = array();
        foreach ($rows as $k => $row) {
            $shared_groups[$row['id']] = $row['name'];
        }

        $rows = runQuery("SELECT ug.name, ug.id, ug.affiliation,ug.opengroup FROM `Usergroups` ug ORDER BY name ASC", "Usergroups");
        $usergroupstring = '';
        foreach ($rows as $k => $row) {
            if (isset($shared_groups[$row['id']])) {
                continue;
            }
            $affis = explode(",", $row['affiliation']);
            if ($row['opengroup'] == 0 && !in_array($myaffi, $affis)) {
                continue;
            }
            $usergroupstring .= "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
        }
        if ($usergroupstring != '') {
            echo "$usergroupstring";
        } else {
            echo "<option value='-' disabled>No groups available</option>";
        }
        echo "</select> <input type=button class=button name='Add_Group' value='Add Group' onClick='AddGroup($userid)' />  &nbsp; <span id='group_comment_field' style='color:red; font-style:italic;'>$group_roles_updated</span>";

        echo "<p><table id='gpermtable' cellspacing=0 class=w50 style='text-align:center'>";
        echo "<tr ><th class=top>User</th><th class=top>Use</th><th class=top>Edit</th><th class=top>Share</th></tr>";
        $rows = runQuery("SELECT u.name, u.id, ursc.edit, ursc.full FROM `Usergroups` u JOIN `Usergroups_x_Report_Sections_CheckBox` ursc ON u.id = ursc.gid WHERE ursc.cid = '$cid' ORDER BY u.name DESC", "Usergroups:Usergroups_x_Report_Sections_CheckBox");
        foreach ($rows as $k => $row) {
            $line = "<tr id='access_" . $row['id'] . "'><td><input type=hidden name='add_groups[]' value='" . $row['id'] . "'/>" . $row['name'] . "</td>";
            $line .= "<td><input type='checkbox' name='g_can_use[]' value='" . $row['id'] . "' checked /></td>";
            $checked = ($row['edit'] == 1) ? 'checked' : '';
            $line .= "<td><input type='checkbox' name='g_can_edit[]' value='" . $row['id'] . "' $checked /></td>";
            $checked = ($row['full'] == 1) ? 'checked' : '';
            $line .= "<td><input type='checkbox' name='g_can_share[]' value='" . $row['id'] . "' $checked /></td>";
            $line .= "</tr>";
            echo $line;
        }
        echo "<tr id='ownerrowGroup'><td class=last colspan=6>&nbsp;</td></tr>";
        echo "</table></p>";

        ///////////
        // USERS //
        ///////////
        echo "<span class=emph>Specify additional users who should have access. </span></p>";

        //echo "<form action='index.php?page=report&t=checkboxes&c=$cid' method=POST>";
        //echo "<input type=hidden name=cid value='$cid'/>";
        echo "<p>Type a name: <input type=hidden name=AddUserID id=AddUserID value='' /> <input title='Specify additional users who should have access.' type=text size='50' value='' name='usersource' id='searchfield' /> <input type=button  class=button name='AddUser' value='Add User' onClick='AddUsers($userid);' /> &nbsp; <span id='comment_field' style='color:red; font-style:italic;'>$roles_updated</span>";
        echo "</p>";


        echo "<p><table id='permtable' cellspacing=0 class=w50 style='text-align:center'>";
        echo "<tr ><th class=top>User</th><th class=top>Use</th><th class=top>Edit</th><th class=top>Share</th></tr>";
        $rows = runQuery("SELECT u.LastName, u.FirstName, ursc.uid, ursc.edit, ursc.full FROM `Users` u JOIN `Users_x_Report_Sections_CheckBox` ursc ON u.id = ursc.uid WHERE ursc.cid = '$cid' ORDER BY u.LastName DESC", "Users:Users_x_Report_Sections_CheckBox");
        foreach ($rows as $k => $row) {
            $line = "<tr id='access_" . $row['uid'] . "'><td><input type=hidden name='add_users[]' value='" . $row['uid'] . "'/>" . $row['LastName'] . " " . $row['FirstName'] . "</td>";
            $line .= "<td><input type='checkbox' name='can_use[]' value='" . $row['uid'] . "' checked /></td>";
            $checked = ($row['edit'] == 1) ? 'checked' : '';
            $line .= "<td><input type='checkbox' name='can_edit[]' value='" . $row['uid'] . "' $checked /></td>";
            $checked = ($row['full'] == 1) ? 'checked' : '';
            $line .= "<td><input type='checkbox' name='can_share[]' value='" . $row['uid'] . "' $checked /></td>";

            $line .= "</tr>";
            echo $line;
        }
        echo "<tr id='ownerrow'><td class=last colspan=6>&nbsp;</td></tr>";
        echo "</table>";
        //echo "<p><input type=submit class=button name='SaveChanges' value='Save Changes' /></form></p>";
        echo "</div>";
    }
    // no sharing permissions. just list the users with access.
    else {
        echo "list users with access : TODO";
    }

    if ($rw == 1 || $full == 1) {
        echo "<p><input type=submit class=button name='UpdateCBlist' value='submit' /></form></p>";
    }

    echo "</div>";
}
