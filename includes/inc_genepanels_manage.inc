<?php


// DELETE Panel ? //
if (isset($_GET['a']) && $_GET['a'] == 'd' && isset($_GET['gpid']) && $_GET['gpid'] != '') {
    $gpid = $_GET['gpid'];
    //allowed?
    $rows = runQuery("SELECT rw FROM `GenePanels_x_Users` WHERE gpid = '$gpid' AND uid = '$userid'", "GenePanels_x_Users");
    if ($rows[0]['rw'] != 1) {
        echo "<div class=section>";
        echo "<h3>Insufficient Privileges</h3>";
        echo "<p>You are not allowed to delete this panel.</p>";
        echo "</div>";
    }
    // ok, allowed.
    else {
        // first present a confirmation.
        if (!isset($_GET['confirm']) || $_GET['confirm'] != 'yes') {
            $rows = runQuery("SELECT Name FROM `GenePanels` WHERE id = '$gpid'", 'GenePanels');
            $gpname = $rows[0]['Name'];
            echo "<div class=section>";
            echo "<h3>Delete Gene Panel:  '$gpname' </h3>";
            echo "<p>Are you sure you want to delete this genepanel ?&nbsp;&nbsp;&nbsp; ";
            echo "<a href='index.php?page=genepanels&t=manage&a=d&gpid=$gpid&confirm=yes' class=emph>Yes</a> / <a href='index.php?page=genepanels&t=manage' class=emph>No</a></p>";
            echo "</div>";
            exit;
        } else {
            // delete from panel_genes
            doQuery("DELETE FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = '$gpid'", "GenePanels_x_Genes_ncbigene");
            // delete from panel_users
            doQuery("DELETE FROM `GenePanels_x_Users` WHERE gpid = '$gpid'", "GenePanels_x_Users");
            // delete from panel_usergroups
            doQuery("DELETE FROM `GenePanels_x_Usergroups` WHERE gpid = '$gpid'", "GenePanels_x_Usergroups");
            // delete from Variants_panel
            doQuery("DELETE FROM `Variants_x_GenePanels_ncbigene` WHERE gpid = '$gpid'", "Variants_x_GenePanels_ncbigene");
            // delete from GenePanels
            doQuery("DELETE FROM `GenePanels` WHERE id = '$gpid'", "GenePanels");
            // keep & add to log
            doQuery("INSERT INTO `GenePanels_Log` (gpid, uid, message) VALUES ('$gpid','$userid','GenePanel Deleted')");
        }
    }
}
// SHARE panel ?
elseif (isset($_GET['a']) && $_GET['a'] == 's' && isset($_GET['p']) && $_GET['p'] != '') {
    $gpid = $_GET['p'];
    //allowed?
    $rows = runQuery("SELECT share FROM `GenePanels_x_Users` WHERE gpid = '$gpid' AND uid = '$userid'", "GenePanels_x_Users");
    if ($rows[0]['share'] != 1) {
        echo "<div class=section>";
        echo "<h3>Insufficient Privileges</h3>";
        echo "<p>You are not allowed to share this panel.</p>";
        echo "</div>";
        exit;
    }
    include('includes/inc_genepanels_share.inc');
    exit;
}

echo "<div class=section>";
echo "<h3>Private Panels:</h3>";

// get gene panels accessible by user.
$query = "SELECT `GenePanels`.id, `GenePanels`.Name, `GenePanels`.Description, `GenePanels`.LastEdit,`GenePanels_x_Users`.rw, `GenePanels_x_Users`.share FROM `GenePanels` JOIN `GenePanels_x_Users` ON `GenePanels`.id = `GenePanels_x_Users`.gpid WHERE `GenePanels_x_Users`.uid = '$userid' ORDER BY Name";
$rows = runQuery($query, "GenePanels:GenePanels_x_Users");

if (count($rows) == 0) {
    echo "<p class=indent>No private panels available.</p>";
} else {
    echo "<p ><table cellspacing=0 class=indent style='min-width:60%'><tr><th class=top>Panel</th><th class=top>Description</th><th class=top>Nr.Genes</th><th class=top>Last Edit</th><th class=top>Action</th></tr>";
    foreach ($rows as $key => $row) {
        $id = $row['id'];
        $name = $row['Name'];
        $desc = $row['Description'];
        $le = explode(" ", $row['LastEdit']);
        $le = $le[0];
        $rw = $row['rw'];
        $share = $row['share'];
        // nr of genes
        $sr = runQuery("SELECT gid, Symbol, Comment, LastEdit FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = '$id'", "GenePanels_x_Genes_ncbigene");
        $nrgenes = count($sr);
        $action = '';
        if ($rw == 1) {
            $action .= " <a href='index.php?page=genepanels&t=edit&gpid=$id'>Edit Genes</a> /";
        } else {
            $action .= " <a href='index.php?page=genepanels&t=show&gpid=$id'>Show Genes</a> /";
        }
        if ($share == 1) {
            $action .= " <a href='index.php?page=genepanels&t=manage&a=s&p=$id'>Share</a> /";
        }
        if ($rw == 1) {
            $action .= " <a href='index.php?page=genepanels&t=manage&a=d&gpid=$id'>Delete</a> /";
        }
        $action .= " <a href='index.php?page=genepanels&t=export&gpid=$id'>Export Gene List</a> /"; //substr($action,1,-2);
        $action .= " <a href='index.php?page=genepanels&t=log&gpid=$id'>Show Log</a>";
        echo "<tr><td>$name</td><td>$desc</td><td>$nrgenes</td><td>$le</td><td>$action</td></tr>";
    }
    echo "<tr><td colspan=5 class=last>&nbsp;</td></tr>";
    echo "</table>";
    echo "</p>";
}


echo "<h3>Public Panels:</h3>";
#TODO 
echo "<p class=indent>... future work ..</p>";



echo "</div>";
