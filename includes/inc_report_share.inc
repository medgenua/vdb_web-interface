<?php

// posted?
$sel_id = (isset($_POST['sel_id'])) ? $_POST['sel_id'] : '';
$sel_id = ($sel_id == '' && isset($_GET['i'])) ? $_GET['i'] : $sel_id;
// shared.
if (isset($_POST['ShareSelection'])) {
    // get  details
    if ($sel_id == '') {
        echo "Problem: variable sel-id not set. please report<br/>";
        exit;
    }
    $col = $table = $prefix = $title = '';
    if (substr($sel_id, 0, 2) == 'rs') {
        $col = 'rsid';
        $table = 'Report_Sections';
        $prefix = 'rs_';
        $title = 'Section';
    } else {
        $col = 'rid';
        $table = 'Report_Definitions';
        $prefix = 'rd_';
        $title = '';
    }
    $id = substr($sel_id, 3);
    $row = runQuery("SELECT Name FROM `$table` WHERE $col = '$id'", "$table")[0];
    $rsname = $row['Name'];
    // get posted vars
    $addgroups = isset($_POST['usergroups']) ? $_POST['usergroups'] : array();
    $addusers = isset($_POST['users']) ? $_POST['users'] : array();
    echo "<div class=section>\n";
    echo " <h3>Report $title '$rsname' Shared</h3>\n";
    echo "<form action='index.php?page=report&t=share' method=POST><input type=hidden name='sel_id' value='$prefix$id'/>\n";
    echo "<input type=hidden name=finalise value=1>\n";

    // process usergroups.
    if (count($addgroups) > 0) {
        echo "<p>Please specify the permissions for the usergroups you want to share with, and press 'Finish'.</p>\n";
        echo "<table cellspacing=0 style='width:30%'>\n";
        echo "<th class=top>Usergroup</th>\n";
        echo "<th class=top>Edit</th>\n";
        echo "<th class=top>Share/Delete</th>\n";
        echo "</tr>\n";
        foreach ($addgroups as $gid) {
            // get usergroup info 
            $row = runQuery("SELECT name FROM `Usergroups` WHERE id = '$gid'", "Usergroups")[0];
            $name = $row['name'];
            // print table row
            echo "<tr>\n";
            echo "<td ><input type=hidden name='groups[]' value='$gid'>$name</td>\n";
            echo "<td><input type=radio name='grw_$gid' value=1 > Yes <input type=radio name='grw_$gid' value=0 checked> No</td>\n";
            echo "<td><input type=radio name='gshare_$gid' value=1 > Yes <input type=radio name='gshare_$gid' checked value=0> No</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n";
        echo "</p>\n";
    }


    // process single users
    if (count($addusers) > 0) {
        echo "<p>Please specify the permissions for the users you want to share with, and press 'Finish'.</p>\n";
        echo "<table cellspacing=0 style='width:30%'>\n";
        echo "<th class=top>User</th>\n";
        echo "<th class=top>Edit</th>\n";
        echo "<th class=top>Share/Delete</th>\n";
        echo "</tr>\n";
        foreach ($addusers as $uid) {
            // get userinfo 
            $row = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '$uid'", "Users")[0];
            $fname = $row['FirstName'];
            $lname = $row['LastName'];
            // print table row
            echo "<tr>\n";
            echo "<td ><input type=hidden name='users[]' value='$uid'>$lname $fname</td>\n";
            echo "<td><input type=radio name='rw_$uid' value=1 > Yes <input type=radio name='rw_$uid' value=0 checked> No</td>\n";
            echo "<td><input type=radio name='share_$uid' value=1 > Yes <input type=radio name='share_$uid' checked value=0> No</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n";
        echo "</p>\n";
    }
    if (count($addusers) > 0 || count($addgroups) > 0) {
        echo "<input type=submit class=button name=next value='Finish'>\n";
        echo "</form>\n";
    }
    echo "<p><a href='index.php?page=report&t=share&i=$prefix$id'>Go Back</a></p>\n";
    echo "</div>\n";
    exit;
}
// finalize sharing.
if (isset($_POST['finalise'])) {
    // get  details
    if ($sel_id == '') {
        echo "Problem: variable sel-id not set. please report<br/>";
        exit;
    }
    $table = $col = $title = $prefix = '';
    if (substr($sel_id, 0, 2) == 'rs') {
        $table = 'Report_Sections';
        $col = 'rsid';
        $prefix = 'rs_';
        $title = 'section';
    } elseif (substr($sel_id, 0, 2) == 'rd') {
        $table = 'Report_Definitions';
        $col = 'rid';
        $prefix = 'rd_';
        $title = '';
    }

    $id = substr($sel_id, 3);
    $row = runQuery("SELECT Name FROM `$table` WHERE $col = '$id'", "$table")[0];
    $rsname = $row['Name'];
    // groups
    $addgroups = isset($_POST['groups']) ? $_POST['groups'] : array();
    foreach ($addgroups as $gid) {
        $editrw = $_POST["grw_$gid"];
        $editshare = $_POST["gshare_$gid"];
        // INSERT or UPDATE permissions
        doQuery("INSERT INTO `Usergroups_x_$table` ($col, gid, edit, full) VALUES ('$id','$gid', '$editrw', '$editshare') ON DUPLICATE KEY UPDATE edit = if(VALUES(edit) < '$editrw', VALUES(edit),'$editrw'), full = if(VALUES(full) < '$editshare', VALUES(full),'$editshare')", "Usergroups_x_$table");
        // handle members.
        $rows = runQuery("SELECT uid FROM `Usergroups_x_User` WHERE gid = '$gid'", "Usergroups_x_User");
        foreach ($rows as $row) {
            $uid = $row['uid'];
            // current access ? 
            $arow = array_shift(...[runQuery("SELECT edit, full FROM `Users_x_$table` WHERE uid = '$uid' AND `$col` = '$id'", "Users_x_$table")]);
            if (count($arow) == 0) {
                doQuery("INSERT INTO `Users_x_$table` ($col, uid, edit, full) VALUES ('$id','$uid', '$editrw', '$editshare')", "Users_x_$table");
                $subject = "New report $title made available ($rsname)";
                doQuery("INSERT INTO `Inbox` (Inbox.from, Inbox.to, Inbox.subject, Inbox.body, Inbox.type, Inbox.values, Inbox.date) VALUES ('$userid','$uid','$subject','','shared_report_$title','$id',NOW())");
                $inbox[$uid] = 1;
            } else {
                $l_editrw = max($editrw, $arow['edit']);
                $l_editshare = max($editshare, $arow['full']);
                doQuery("UPDATE `Users_x_$table` SET `edit` = '$l_editrw', `full` = '$l_editshare' WHERE `$col` = '$id' AND `uid` = '$uid'", "Users_x_$table");
            }
        }
    }


    // users
    $addusers = isset($_POST['users']) ? $_POST['users'] : array();
    foreach ($addusers as $uid) {
        $editrw = $_POST["rw_$uid"];
        $editshare = $_POST["share_$uid"];
        // current access ? 
        $arow = runQuery("SELECT edit, full FROM `Users_x_$table` WHERE uid = '$uid' AND `$col` = '$id'", "Users_x_$table");
        if (count($arow) == 0) {
            doQuery("INSERT INTO `Users_x_$table` ($col, uid, edit, full) VALUES ('$id','$uid', '$editrw', '$editshare')", "Users_x_$table");
            if (!isset($inbox[$uid])) {
                $subject = "New report $title made available ($rsname)";
                doQuery("INSERT INTO `Inbox` (Inbox.from, Inbox.to, Inbox.subject, Inbox.body, Inbox.type, Inbox.values, Inbox.date) VALUES ('$userid','$uid','$subject','','shared_report_$title','$id',NOW())");
            }
        } else {
            $l_editrw = max($editrw, $arow['edit']);
            $l_editshare = max($editshare, $arow['full']);
            doQuery("UPDATE `Users_x_$table` SET `edit` = '$l_editrw', `full` = '$l_editshare' WHERE `$col` = '$id' AND `uid` = '$uid'");
        }
    }
    clearMemcache("Users_x_$table:Inbox");
}


if ($sel_id == '') {
    $options = "<select name='sel_id'><option value='' disabled='disabled' selected='selected'></option>";
} else {
    $options = "<select name='sel_id'>";
}
// get reports
$options .= "<optgroup label='Reports'>";
$rows = runQuery("SELECT rid, Name FROM `Report_Definitions` WHERE Owner = '$userid' ORDER BY Name", "Report_Definitions");
$rseen = array();
foreach ($rows as $row) {
    $selected = ($sel_id == 'rd_' . $row['rid']) ? 'selected' : '';
    $options .= "<option value='rd_" . $row['rid'] . "' $selected>" . $row['Name'] . "</option>";
    $rseen[$row['rid']] = 1;
}
$rows = runQuery("SELECT rd.rid, rd.Name FROM `Report_Definitions` rd JOIN `Users_x_Report_Definitions` urd ON urd.rid = rd.rid WHERE urd.uid = '$userid' ORDER BY Name", "Report_Definitions:Users_x_Report_Definitions");
foreach ($rows as $row) {
    if (in_array($row['rid'], $rseen)) continue;
    $selected = ($sel_id == 'rd_' . $row['rid']) ? 'selected' : '';
    $options .= "<option value='rd_" . $row['rid'] . "' $selected >" . $row['Name'] . "</option>";
    $rseen[$row['rid']] = 1;
}

$options .= "</optgroup>";

// get sections
$options .= "<optgroup label='Sections'>";
$rows = runQuery("SELECT rsid, Name FROM `Report_Sections` WHERE Owner = '$userid' ORDER BY Name", "Report_Sections");
$sseen = array();
foreach ($rows as $row) {
    $selected = ($sel_id == 'rs_' . $row['rsid']) ? 'selected' : '';
    $options .= "<option value='rs_" . $row['rsid'] . "' $selected>" . $row['Name'] . "</option>";
    $sseen[$row['rsid']] = 1;
}
$rows = runQuery("SELECT rs.rsid, rs.Name FROM `Report_Sections` rs JOIN `Users_x_Report_Sections` urs ON urs.rsid = rs.rsid WHERE urs.uid = '$userid' ORDER BY Name", "Report_Sections:Users_x_Report_Sections");
foreach ($rows as $row) {
    if (in_array($row['rsid'], $sseen)) continue;
    $selected = ($sel_id == 'rs_' . $row['rsid']) ? 'selected' : '';
    $options .= "<option value='rs_" . $row['rsid'] . "' $selected >" . $row['Name'] . "</option>";
    $sseen[$row['rsid']] = 1;
}
$options .= "</optgroup>";
echo "<div class=section><h3>Share Report/Section Definitions</h3>";
echo "<form action='index.php?page=report&t=share' method=POST>";
echo "<p><span class=emph>Pick item:</span> &nbsp;$options</select> &nbsp; <input type=submit name=select value=Select></form></p>";
// end here if no selection.
if ($sel_id == '') exit;

///////////////////////////////////
// selected a section or report //
/////////////////////////////////
$col = $table = $title = $prefix = '';
if (substr($sel_id, 0, 2) == 'rs') {
    $col = 'rsid';
    $table = 'Report_Sections';
    $title = 'Section';
    $prefix = "rs_";
} elseif (substr($sel_id, 0, 2) == 'rd') {
    $col = 'rid';
    $table = 'Report_Definitions';
    $title = 'Definition';
    $prefix = "rd_";
}
$id = substr($sel_id, 3);
// get details
$row = runQuery("SELECT Name, Owner FROM `$table` WHERE $col = '$id'", "$table")[0];
$rsname = $row['Name'];
$rsowner = $row['Owner'];
// print sharing page.
echo "<div class='toleft w50' style='padding-right:3em;'>";
echo "<p><span class=emph>Share Report $title '$rsname':</span> <br/>Select the users  or usergroups you want to share with from the lists below and press 'Share'. Detailed settings can be set on the following page.</p>";
echo "<form action='index.php?page=report&t=share' method=POST><input type=hidden name='sel_id' value='$prefix$id'/>";
echo "<p><table cellspacing=20>\n";
// usergroups
echo "<tr>\n";
echo "<td class=clear valign=top>\n";
echo "<span class=emph>Select Usergroups</span><br>\n";
echo "<select name='usergroups[]' size=10 MULTIPLE>\n";
// get groups with access.
$rows = runQuery("SELECT u.id, u.name, urs.edit, urs.full FROM `Usergroups` u JOIN `Usergroups_x_$table` urs ON u.id = urs.gid WHERE urs.$col = '$id' ORDER BY u.name", "Usergroups:Usergroups_x_$table");
$ugwa = array();
foreach ($rows as $k => $row) {
    $name = $row['name'];
    $currid = $row['id'];
    $editrw = $row['edit'];
    $editshare = $row['full'];
    $ugwa[$currid] = "$name@@@$editrw@@@$editshare";
}
// get my affiliation.
$row = runQuery("SELECT a.id, a.name FROM `Affiliation` a JOIN `Users` u ON u.Affiliation = a.id WHERE u.id = $userid", "Affiliation:Users")[0];
$myaffi = $row['id'];
$myaffiname = $row['name'];
// list groups without access
echo "<OPTGROUP label='Private'>";
$rows = runQuery("SELECT ug.name, ug.id, ug.affiliation FROM `Usergroups` ug WHERE ug.opengroup = 0 ORDER BY name ASC", "Usergroups");
$printed = 0;
foreach ($rows as $k => $row) {
    if (isset($ugwa[$row['id']])) {
        continue;
    }
    $affis = explode(",", $row['affiliation']);
    if (!in_array($myaffi, $affis)) {
        continue;
    }
    $printed++;
    echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
}

if ($printed == 0) {
    echo "<option value='' DISABLED>No usergroups available</option>";
}
echo " </OPTGROUP>";
// open
echo "<OPTGROUP label='Public'>";
$rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug WHERE ug.opengroup = 1 ORDER BY name ASC", "Usergroups");
$printed = 0;
foreach ($rows as $k => $row) {
    if (isset($ugwa[$row['id']])) {
        continue;
    }
    $printed++;
    echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
}

if ($printed == 0) {
    echo "<option value='' DISABLED>No usergroups available</option>";
}
echo " </OPTGROUP>";

echo "</select>\n";
echo "</td>\n";

// single users
// get users with access.
$rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name, urs.edit, urs.full FROM `Users`u JOIN `Users_x_$table` urs JOIN `Affiliation` a ON u.Affiliation = a.id AND u.id = urs.uid WHERE urs.$col = '$id' ORDER BY a.id, u.LastName", "Users:Users_x_$table:Affiliation");
$uwa = array();
foreach ($rows as $k => $row) {
    $lname = $row['LastName'];
    $currid = $row['id'];
    $fname = $row['FirstName'];
    $affi = $row['name'];
    $editrw = $row['edit'];
    $editshare = $row['full'];
    $uwa[$currid] = "$lname $fname@@@$affi@@@$editrw@@@$editshare";
}
// list users with no or restricted access
echo "<td class=clear valign=top>\n";
echo "<span class=emph>Select Users</span><br>\n";
echo "<select name='users[]' size=10 MULTIPLE>\n";
$rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id ORDER BY a.name, u.LastName", "Users:Affiliation");
$currinst = '';
$printed = 0;
foreach ($rows as $k => $row) {
    $uid = $row['id'];
    // skip users with full access
    if (array_key_exists($uid, $uwa)) {
        $pieces = explode('@@@', $uwa[$uid]);
        if ($pieces[2] == 1 && $pieces[3] == 1) {
            continue;
        }
    }
    $lastname = $row['LastName'];
    $firstname = $row['FirstName'];
    $institute = $row['name'];
    if ($currinst != $institute) {
        if ($currinst != '') {
            echo "</optgroup>";
        }
        echo "<optgroup label='$institute'>\n";
        $currinst = $institute;
    }
    echo "<option value='$uid'>$lastname $firstname</option>\n";
    $printed++;
}
if ($printed == 0) {
    echo "<option value='' DISABLED>No users available</option>";
}
echo "</select>\n";
echo "</td>\n";
echo "</tr>\n";
echo "</table>\n";
echo "<input type=submit class=button name=ShareSelection value='Share'></p>\n";
echo "</form>\n";
echo "</div><div class='toleft w50'>";

// output user(group)s with access.
echo "<p><span class=emph>Users with access:</span><br/>'Edit' means that users can edit report details. 'Read-only' means no information can be changed. 'Full Control' means this user can share with additional users.</p>\n";
echo "<p><table cellspacing=20 >\n";
// usergroups
echo "<tr>\n";
echo "<td valign=top >\n";
echo "<span class=emph>Granted Usergroups:</span><br>\n";
echo "<ul id=nodisc>\n";
if (count($ugwa) == 0) {
    echo "<li>None.</li>";
}
foreach ($ugwa as $gid => $value) {
    $pieces = explode('@@@', $value);
    $gname = $pieces[0];
    $editrw = $pieces[1];
    $editshare = $pieces[2];
    // check permissions
    if ($editrw == 1) {
        $perm = 'Edit';
    } else {
        $perm = 'Read-only';
    }
    if ($editshare == 1) {
        $perm .= " / Full Control";
    }

    echo "<li>$gname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?rsid=$prefix$id&u=$gid&type=group&from=share_report\"><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></a></li>\n";
}
echo "</ol></ul>\n";
//echo "</select>\n";
echo "</td>\n";
echo "</tr>\n";


// users
echo "<tr>\n";
echo "<td valign=top >\n";
echo "<span class=emph>Granted Users:</span><br>\n";
$currinst = '';
echo "<ul id=nodisc>\n";
if (count($uwa) == 0) {
    echo "<li>None.</li>";
}
foreach ($uwa as $uid => $value) {
    $pieces = explode('@@@', $value);
    $uname = $pieces[0];
    $institute = $pieces[1];
    $editrw = $pieces[2];
    $editshare = $pieces[3];
    // check institution
    if ($currinst != $institute) {
        if ($currinst != '') {
            echo "</ol>";
        }
        echo "<li><span class=italic>&nbsp;&nbsp;$institute</span><ol>\n";
        $currinst = $institute;
    }
    // check permissions
    if ($editrw == 1) {
        $perm = 'Edit';
    } else {
        $perm = 'Read-only';
    }
    if ($editshare == 1) {
        $perm .= " / Full Control";
    }

    echo "<li>$uname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?rsid=$prefix$id&u=$uid&type=user&from=share_report\"><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></a></li>\n";
}
echo "</ol></ul>\n";
//echo "</select>\n";
echo "</td>\n";
echo "</tr>\n";
echo "</table>\n";
echo "</p></div>";
echo "<br style='clear:both;'/>";
