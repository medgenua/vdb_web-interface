<?php
// NEW  submission? 
if (isset($_POST['SubmitSection'])) {
    $rsname = addslashes($_POST['rsname']);
    $rsdesc = addslashes($_POST['rsdesc']);
    if (isset($_POST['rspublic'])) {
        $rsp = 1;
    } else {
        $rsp = 0;
    }
    // filters & annotations
    $fset = $_POST['fset'];
    $asets = implode(",", array_keys($_POST['asets']));
    $csets = implode(",", array_keys($_POST['checkboxes']));
    $vspace = $_POST['vspace'];
    $rsid = insertQuery("INSERT INTO `Report_Sections` (Name, Description, Owner, Public, FilterSet,Annotations, checkboxes, vspace) VALUES ('$rsname','$rsdesc','$userid','$rsp','$fset','$asets','$csets','$vspace')", "Report_Sections");
    echo "<div class=section><p><h3>Report Section Saved.</h3></p>";
    echo "<ul>";
    echo "<li>Name: $rsname</li>";
    echo "<li>Description: $rsdesc</li>";
    echo "</li></div>";
}

echo "<div class=section>";
echo "<h3>New report section definition:</h3>";
echo "<p>A report consists of one or more predefined filter/annotation combinations, included as independent sections. To define a report, first generate these combinations. Next, decide which combinations will be included into report. A set of combinations can then be saved as a report template.</p><p>";
echo "<form action='index.php?page=report&t=new' method=POST>";
echo "<span class=emph>Specify the section details:</span>";
echo "<ul>";
echo "<li title='Short, informative title'>Section Title: <br/>&nbsp;<input name='rsname' type=text size=50></li>";
echo "<li title='Some additional information on the intended contents of the section'>Description: <br/>&nbsp;<textarea name='rsdesc' rows=3 cols=50></textarea></li>";
echo "<li title='Shoud the section be accessible by all users'>Public: <input type=checkbox name='rspublic'></li>";
echo "</ul>";
echo "</p>";

echo "<span class=emph>Specify the applied filter settings:</span> <br>&nbsp;- Filtersettings listed here are predefined on, and saved from the 'Variant Filter' page. <br/>&nbsp;- If an included filter is deleted, this will be mentioned in the report. <br/>";
$table = ''; // "<table style='margin-left:1em'><tr><th class=top>Include><th>Name</th><th>Description</th><th>Created</th></tr>";
// get private filters
$rows = runQuery("SELECT fid, FilterName, Comments, Created FROM `Users_x_FilterSettings` WHERE uid = '$userid' ORDER BY FilterName", "Users_x_FilterSettings");
if (count($rows) > 0) {
    $table .= "<tr><td colspan=4><span class=emph>Private</span></td></tr>";
}
foreach ($rows as $row) {
    if ($row['Comments'] == '') {
        $row['Comments'] = '-';
    }
    $table .= "<tr><td><input type=radio name='fset' value='" . $row['fid'] . "'></td><td>" . $row['FilterName'] . "</td><td>" . $row['Comments'] . "</td><td>" . substr($row['Created'], 0, 10) . "</td></tr>";
}
// get shared filters
$rows = runQuery("SELECT uf.fid, uf.FilterName, uf.Comments, uf.Created FROM `Users_x_FilterSettings` uf JOIN `Users_x_Shared_Filters` usf ON uf.fid = usf.fid WHERE usf.uid = '$userid' ORDER BY FilterName", "Users_x_FilterSettings:Users_x_Shared_Filters");
if (count($rows) > 0) {
    $table .= "<tr><td colspan=4><span class=emph>Shared</span></td></tr>";
}
foreach ($rows as $row) {
    if ($row['Comments'] == '') {
        $row['Comments'] = '-';
    }
    $table .= "<tr><td><input type=radio name='fset' value='" . $row['fid'] . "'></td><td>" . $row['FilterName'] . "</td><td>" . $row['Comments'] . "</td><td>" . substr($row['Created'], 0, 10) . "</td></tr>";
}

if ($table != '') {
    echo "<table style='margin-left:1em;width:50%' cellspacing=0><tr><th class=top >Include</th><th class=top>Name</th><th class=top>Description</th><th class=top>Created</th></tr>$table</table>";
} else {
    echo "<span class=emph>PROBLEM:</span> No filtersettings available. Please create a filter set first on <a href='index.php?page=variants'>the 'Variant Filter'<a> page.";
}
echo "</p><p>";
echo "<span class=emph>Specify the included annotations:</span> <br>&nbsp;- Annotation sets listed here are predefined on, and saved from the 'Variant Filter' page. <br/>&nbsp;- If an included set is deleted, this will be mentioned in the report. <br/>";
$table = '';
// get private annos
$rows = runQuery("SELECT aid, AnnotationName, Comments, Created FROM `Users_x_Annotations` WHERE uid = '$userid' ORDER BY AnnotationName", "Users_x_Annotations");
foreach ($rows as $row) {
    if ($row['Comments'] == '') {
        $row['Comments'] = '-';
    }
    $table .= "<tr><td><input type=checkbox name='asets[" . $row['aid'] . "]'></td><td>" . $row['AnnotationName'] . "</td><td>" . $row['Comments'] . "</td><td>" . substr($row['Created'], 0, 10) . "</td></tr>";
}
// get shared annotations
$rows = runQuery("SELECT ua.aid, ua.AnnotationName, ua.Comments, ua.Created FROM `Users_x_Annotations` ua JOIN `Users_x_Shared_Annotations` usa ON ua.aid = usa.aid WHERE usa.uid = '$userid' ORDER BY AnnotationName", "Users_x_Annotations:Users_x_Shared_Annotations");
foreach ($rows as $row) {
    if ($row['Comments'] == '') {
        $row['Comments'] = '-';
    }
    $table .= "<tr><td><input type=checkbox name='asets[" . $row['aid'] . "]'></td><td>" . $row['AnnotationName'] . "</td><td>" . $row['Comments'] . "</td><td>" . substr($row['Created'], 0, 10) . "</td></tr>";
}

if ($table != '') {
    echo "<table style='margin-left:1em;width:50%' cellspacing=0><tr><th class=top >Include</th><th class=top>Name</th><th class=top>Description</th><th class=top>Created</th></tr>$table</table>";
} else {
    echo "<span class=emph>PROBLEM:</span> No filtersettings available. Please create a filter set first on <a href='index.php?page=variants'>the 'Variant Filter'<a> page.";
}


echo "</p>";

// CHECKBOXES.
echo "<p><span class=emph>Included checkbox lists:</span> <br>&nbsp;- Checkbox lists are defined and managed on the <a href='index.php?page=report&type=checkboxes'>'Manage CheckBox Lists'</a> page . <br/>&nbsp;- CheckBoxes are listed in PDF reports below each variant entry.<br/>";
$table = '';
// get private checkboxes
$chbxs = array();
$rows = runQuery("SELECT cid,Title,Options FROM `Report_Section_CheckBox` WHERE uid = '$userid' ORDER BY Title", "Report_Section_CheckBox");
foreach ($rows as $row) {
    if ($row['Options'] == '') {
        $row['Options'] = '-';
    }
    $row['Options'] = str_replace("||", "; ", $row['Options']);
    $table .= "<tr><td><input type=checkbox name='checkboxes[" . $row['cid'] . "]' ></td><td>" . $row['Title'] . "</td><td>" . $row['Options'] . "</td></tr>";
    $chbxs[] = $row['cid'];
}
// get shared checkboxes
$rows = runQuery("SELECT rsc.cid,rsc.Title,rsc.Options FROM `Report_Section_CheckBox` rsc JOIN `Users_x_Report_Sections_CheckBox` ursc ON rsc.cid = ursc.cid WHERE ursc.uid = '$userid' ORDER BY rsc.Title", "Report_Section_CheckBox");
foreach ($rows as $row) {
    // seen in private
    if (in_array($row['cid'], $chbxs)) {
        continue;
    }
    if ($row['Options'] == '') {
        $row['Options'] = '-';
    }
    $row['Options'] = str_replace("||", "; ", $row['Options']);
    $table .= "<tr><td><input type=checkbox name='checkboxes[" . $row['cid'] . "]' ></td><td>" . $row['Title'] . "</td><td>" . $row['Options'] . "</td></tr>";
    $chbxs[] = $row['cid'];
}
// public checkboxes
$rows = runQuery("SELECT cid,Title,Options FROM `Report_Section_CheckBox` WHERE Public = 1 ORDER BY Title", "Report_Section_CheckBox");
foreach ($rows as $row) {
    // seen in private
    if (in_array($row['cid'], $chbxs)) {
        continue;
    }
    if ($row['Options'] == '') {
        $row['Options'] = '-';
    }
    $row['Options'] = str_replace("||", "; ", $row['Options']);
    $table .= "<tr><td><input type=checkbox name='checkboxes[" . $row['cid'] . "]' ></td><td>" . $row['Title'] . "</td><td>" . $row['Options'] . "</td></tr>";
    $chbxs[] = $row['cid'];
}

if ($table != '') {
    echo "<table style='margin-left:1em;width:50%' cellspacing=0><tr><th class=top >Include</th><th class=top>Title</th><th class=top>Options</th></tr>$table<tr><td colspan=4 class=last>&nbsp;</td></tr></table>";
} else {
    echo "<br/><span class=emph style='margin-left:1em;'>PROBLEM:</span> No checkboxes available. Please create a checkbox list first on <a href='index.php?page=report&type=checkboxes'>'Manage CheckBox Lists'<a>.";
}
// include vspace.
echo "<span class=emph>Include whitespace:</span> <br>&nbsp;- Create room for notes below each variant / checkbox list.<br/>&nbsp;- Select the number of blank lines to include: &nbsp; <select id=vspace name=vspace>";
for ($i = 0; $i <= 5; $i++) {
    echo "<option value=$i >$i</option>";
}
echo "</select>";


echo "<p><input type=submit class=button name='SubmitSection' value='submit' /></form></p>";
echo "</div>";
