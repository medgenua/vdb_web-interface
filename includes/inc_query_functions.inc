<?php
//require_once("inc_logging.inc");
// escape values for use in SQL
function SqlEscapeValues($data)
{
    $mysqli = GetConnection();
    if (is_array($data)) {
        for ($i = 0; $i < count($data); $i = $i + 1) {
            $data[$i] = $mysqli->real_escape_string($data[$i]);
        }
    } else {
        $data = $mysqli->real_escape_string($data);
    }
    return ($data);
}

///////////////////////////
// THE MEMCACHE ROUTINES //
///////////////////////////
function getMCidx($table)
{
    global $usemc, $memcache;
    // get table_memcache_idx
    $idx_string = '';
    $t = explode(":", $table);
    #$mysqli = GetConnection();
    foreach ($t as $k => $tname) {

        $result = array_shift(...[runQuery("SELECT `Table_IDX` FROM `NGS-Variants-Admin`.`memcache_idx` WHERE `Table_Name` = '$tname'")]);
        if (empty($result)) {
            $idx_string .= "0:";
            insertQuery("INSERT INTO `NGS-Variants-Admin`.`memcache_idx` (`Table_Name`,`Table_IDX`) VALUES ('$tname','0')", "NGS-Variants-Admin");
        } else {
            // $result = $sth->fetch_array();
            $idx_string .= $result['Table_IDX'] . ":";
        }
    }
    if (strlen($idx_string) > 0) {
        $idx_string = substr($idx_string, 0, -1);
    }
    return ($idx_string);
}
// run query without feedback (create / drop /truncate)
function doQuery($query, $table = null)
{
    $mysqli = GetConnection();
    global $usemc, $memcache;
    $idx_string = '';

    // run query
    $result = $mysqli->query($query);
    if (!$result) {
        trigger_error($mysqli->error, E_USER_ERROR);
        return false;
    }
    // strip memcache
    if (defined($table) && $usemc == 1) {
        $idx_string = getMCidx($table);
        $db = "NGS-Variants" . $_SESSION['dbname'];
        $result = $memcache->get("ngs:$db:$table:$idx_string:php:" . md5($query));
        clearMemcache($table);
    }
    // strip views
    clearViews($table);
    return true;
}
// insert query : return insert id
function insertQuery($query, $table)
{
    $mysqli = GetConnection();
    global $usemc, $memcache;
    $idx_string = '';
    // run query
    $result = $mysqli->query($query);
    if (!$result) {
        trigger_error($mysqli->error, E_USER_ERROR);
        return false;
    }
    $id = $mysqli->insert_id;
    // strip memcache
    if (defined($table) && $usemc == 1) {
        $idx_string = getMCidx($table);
        $db = "NGS-Variants" . $_SESSION['dbname'];
        $result = $memcache->get("ngs:$db:$table:$idx_string:php:" . md5($query));
        clearMemcache($table);
    }
    // strip views
    clearViews($table);
    return ($id);
}
// arguments are $query,$table,$var. $var is a comma-seperated list of variantIDs (optional).
function runQuery($query, $table = null, $var = null)
{
    $mysqli = GetConnection();
    global $usemc, $memcache;
    $idx_string = '';
    // make select queries high_priority
    if (!stristr($query, ' high_priority ')) {
        $query = preg_replace("/^select/i", "SELECT HIGH_PRIORITY", $query);
    }
    if ($usemc == 1) {
        $idx_string = getMCidx($table);
        $db = "NGS-Variants" . $_SESSION['dbname'];
        $result = $memcache->get("ngs:$db:$table:$idx_string:php:" . md5($query));
    }
    // all memcached items stored by VariantDB are rows with mysql results.
    // add check for array to differentiate with false (no key) or empty result (valid key)
    if ($usemc != 1 || (!$result &&  !is_array($result))) {
        $sth = $mysqli->query($query);
        if (!$sth) {
            trigger_error($mysqli->error, E_USER_ERROR);
            return array();
        }
        $result = array();
        /*if ($sth->num_rows == 1) {
            $result = $sth->fetch_array();
        } elseif ($sth->num_rows == 0) {
            $result = array();
        } else {
            $result = array();
        */
        while ($row = $sth->fetch_array()) {
            array_push($result, $row);
        }
        //}
        if ($usemc == 1) {
            $db = "NGS-Variants" . $_SESSION['dbname'];
            $r = $memcache->replace("ngs:$db:$table:$idx_string:php:" . md5($query), $result, 0, 0);
            if ($r == false) {
                $r = $memcache->set("ngs:$db:$table:$idx_string:php:" . md5($query), $result, 0, 0);
            }
        }
    }
    return ($result);
}

// arguments are $query,$table,$var. $var is a comma-seperated list of variantIDs (optional).
function runBatchedQuery($query, $size = 1000)
{
    // no support for memcached here (yet)
    $mysqli = GetConnection();
    // run query.
    $sth = $mysqli->query($query);
    $result = array();
    if (!$sth) {
        trigger_error($mysqli->error, E_USER_ERROR);
        return $result;
    }
    if ($sth->num_rows == 1) {
        array_push($result, $sth->fetch_array());
    } elseif ($sth->num_rows <= $size) {
        $result = $sth->fetch_all(MYSQLI_BOTH);
    } else {
        $idx = 0;
        $result = array();
        while ($idx < $size) {
            $idx++;
            $row = $sth->fetch_array();
            array_push($result, $row);
        }
    }

    return array($sth, $result);
}
function GetNextBatched($sth, $size = 1000)
{
    $result = array();
    if (!$sth) {
        trigger_error("No SQL handler provided", E_USER_ERROR);
        return $result;
    }
    if ($sth->num_rows == 1) {
        array_push($result, $sth->fetch_array());
    } elseif ($sth->num_rows <= $size) {
        $result = $sth->fetch_all(MYSQLI_BOTH);
    } else {
        $idx = 0;
        $result = array();
        while ($idx < $size) {
            $idx++;
            $row = $sth->fetch_array();
            array_push($result, $row);
        }
    }

    return array($result);
}

function runSlicedQuery($query, $table, $var = null)
{
    global $usemc, $memcache;
    $db = "NGS-Variants" . $_SESSION['dbname'];
    if ($var != null) {
        $allvids = explode(",", $var);
    }
    $slices = array();
    if ($var == null) {
        $slices[1] = '';
    } else {
        // created slices based on VariantID for ranges per 10,000
        foreach ($allvids as $vid) {
            $s = floor($vid / 10000);
            if (!array_key_exists("$s", $slices) || !is_array($slices[$s])) {
                $slices[$s] = array();
            }
            array_push($slices[$s], $vid);
        }
    }
    $result = array();
    foreach ($slices as $key => $vids) {
        $sresult = false;
        $sstart = $key * 10000;
        $sstop = ($key + 1) * 10000 - 1;
        $idx_string = '';
        $lq = $query;
        // if empty var, no ? should be in the query, so this is safe.
        if (preg_match('/ IN \(\s*\?\s*\)/i', $lq)) {
            $lq = preg_replace('/ IN \(\s*\?\s*\)/i', " BETWEEN $sstart AND $sstop ", $lq);
        } else {
            $qpart = implode(",", range($sstart, $sstop));
            $lq = str_replace('?', $qpart, $lq);
        }
        if ($usemc == 1) {
            $idx_string =  getMCidx($table);
            $sresult = $memcache->get("ngs:$db:$table:$idx_string:php:" . md5($lq));
        }

        // all memcached items stored by VariantDB are rows with mysql results.
        // add check for array to differentiate with false (no key) or empty result (valid key)
        if (!$sresult && !is_array($sresult)) {
            // fetch from db, because not in memcache
            $sresult = runQuery($lq, $table, $var);
            if ($usemc == 1) {
                $r = $memcache->replace("ngs:$db:$table:$idx_string:php:" . md5($lq), $sresult, 0, 2000000);
                if ($r == false) {
                    $r = $memcache->set("ngs:$db:$table:$idx_string:php:" . md5($lq), $sresult, 0, 2000000);
                }
            }
        }
        // add slice results to main results
        if ($var == null) {
            $result = array_merge($result, $sresult);
        } else {
            // only needed items! if var is specified, there will be VariantID in the results (should be)
            $vids = array_flip($vids);
            foreach ($sresult as $row) {
                if (array_key_exists($row['VariantID'], $vids)) {
                    array_push($result, $row);
                }
            }
        }
    }
    return ($result);
}

// create AoA from array in case of a single result
function OneToMulti($a)
{
    $result = array();
    if (empty($a)) {
        return ($a);
    }
    array_push($result, $a);
    return ($result);
}
// arguments are "table". Deletes all ngs:$db:table:* keys.
function clearMemcache($table)
{
    global $usemc, $memcache;
    if ($usemc != 1) {
        return;
    }
    $t = explode(":", $table);
    foreach ($t as $key => $tname) {
        doQuery("UPDATE `NGS-Variants-Admin" . $_SESSION['dbsuffix'] . "`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` = '$tname'");
    }
}
// arguments are "table". Deletes views
function clearViews($table, $uid = 'all')
{
    if (!isset($table) || $table == '') {
        return;
    }
    $t = explode(":", $table);
    if (!isset($uid) || $uid == 'all') {
        $uid = '%';
    }
    foreach ($t as $key => $tname) {
        $query = "SELECT `name` FROM `VIEWS` WHERE `name` LIKE 'VIEW_$uid" . "_%_$tname'";
        $names = runQuery($query, "VIEWS");
        foreach ($names as $row) {
            $name = $row['name'];
            doQuery("DELETE FROM `VIEWS` WHERE `name` = '$name'");
            doQuery("DROP TABLE IF EXISTS `$name`");
        }
    }
}

function GetConnection()
{
    global $config;

    // get db version
    $db = "NGS-Variants" . $_SESSION['dbname'];
    $mysqli = new mysqli($config['DBHOST'], $config['DBUSER'], $config['DBPASS'], $db);
    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli->connect_error;
        error_log("Failed to connect to MySQL: " . $mysqli->connect_error);
        exit();
    }
    return $mysqli;
}
