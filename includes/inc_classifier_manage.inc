<?php
// permissions submitted? 
$roles_updated = '';
$group_roles_updated = '';
if (isset($_POST['SaveChanges'])) {
    // users
    $users = isset($_POST['add_users']) ? $_POST['add_users'] : array();
    $can_add = isset($_POST['can_add']) ? $_POST['can_add'] : array();
    $can_validate = isset($_POST['can_validate']) ? $_POST['can_validate'] : array();
    $can_remove = isset($_POST['can_remove']) ? $_POST['can_remove'] : array();
    $can_use = isset($_POST['can_use']) ? $_POST['can_use'] : array();
    $can_share = isset($_POST['can_share']) ? $_POST['can_share'] : array();
    $cid = $_POST['cid'];
    // existing entries.
    $rows = runQuery("SELECT uid FROM `Users_x_Classifiers` WHERE cid = '$cid'", "Users_x_Classifiers");
    $entries = array();
    foreach ($rows as $row) {
        $entries[$row['uid']] = 1;
    }
    foreach ($users as $key => $uid) {
        $add = in_array($uid, $can_add) ? 1 : 0;
        $val = in_array($uid, $can_validate) ? 1 : 0;
        $del = in_array($uid, $can_remove) ? 1 : 0;
        $use = in_array($uid, $can_use) ? 1 : 0;
        $sha = in_array($uid, $can_share) ? 1 : 0;
        // entry exists?
        if (array_key_exists($uid, $entries)) {
            doQuery("UPDATE `Users_x_Classifiers` SET `can_use` = '$use' , `can_add` = '$add' , `can_validate` = '$val' , `can_remove` = '$del' , `can_share` = '$sha' WHERE uid = '$uid' AND cid = '$cid'", "Users_x_Classifiers");
        }
        // new entry
        else {
            doQuery("INSERT INTO `Users_x_Classifiers` (uid,cid,`can_use`,`can_add`,`can_validate`,`can_remove`,`can_share`,`apply_on_import`) VALUES ($uid,$cid,$use,$add,$val,$del,$sha,0)", "Users_x_Classifiers");
            $entries[$uid] = 1;
        }
    }
    // groups.
    $groups = isset($_POST['add_groups']) ? $_POST['add_groups'] : array();
    $can_add = isset($_POST['g_can_add']) ? $_POST['g_can_add'] : array();
    $can_validate = isset($_POST['g_can_validate']) ? $_POST['g_can_validate'] : array();
    $can_remove = isset($_POST['g_can_remove']) ? $_POST['g_can_remove'] : array();
    $can_use = isset($_POST['g_can_use']) ? $_POST['g_can_use'] : array();
    $can_share = isset($_POST['g_can_share']) ? $_POST['g_can_share'] : array();

    // existing entries.
    $rows = runQuery("SELECT gid FROM `Usergroups_x_Classifiers` WHERE cid = '$cid'", "Usergroups_x_Classifiers");
    $g_entries = array();
    foreach ($rows as $row) {
        $g_entries[$row['gid']] = 1;
    }
    foreach ($groups as $key => $gid) {
        $add = (in_array($gid, $can_add) ? 1 : 0);
        $val = (in_array($gid, $can_validate) ? 1 : 0);
        $del = (in_array($gid, $can_remove) ? 1 : 0);
        $use = (in_array($gid, $can_use) ? 1 : 0);
        $sha = (in_array($gid, $can_share) ? 1 : 0);
        // entry exists?
        if (array_key_exists($gid, $g_entries)) {
            doQuery("UPDATE `Usergroups_x_Classifiers` SET `can_use` = '$use' , `can_add` = '$add' , `can_validate` = '$val' , `can_remove` = '$del' , `can_share` = '$sha' WHERE gid = '$gid' AND cid = '$cid'", "Usergroups_x_Classifiers");
        }
        // new entry
        else {
            doQuery("INSERT INTO `Usergroups_x_Classifiers` (gid,cid,`can_use`,`can_add`,`can_validate`,`can_remove`,`can_share`,`apply_on_import`) VALUES ($gid,$cid,$use,$add,$val,$del,$sha,0)", "Usergroups_x_Classifiers");
            $g_entries[$gid] = 1;
        }
        // process users.
        $rows = runQuery("SELECT uid FROM `Usergroups_x_User` WHERE gid = '$gid'", 'Usergroups_x_Users');
        foreach ($rows as $row) {
            $uid = $row['uid'];
            if (array_key_exists($uid, $entries)) {
                // get current permissions.
                $sr = array_shift(...[runQuery("SELECT can_use,can_add,can_validate,can_remove,can_share FROM `Users_x_Classifiers` WHERE cid = $cid AND uid = $uid", "Users_x_Classifiers")]);
                $l_add = max($sr['can_add'], $add);
                $l_val = max($sr['can_validate'], $val);
                $l_del = max($sr['can_remove'], $del);
                $l_use = max($sr['can_use'], $use);
                $l_sha = max($sr['can_share'], $sha);
                doQuery("UPDATE `Users_x_Classifiers` SET `can_use` = '$l_use', `can_add` = '$l_add', `can_validate` = '$l_val', `can_remove` = '$l_del', `can_share` = '$l_sha' WHERE cid = '$cid' AND uid = '$uid'", "Users_x_Classifiers");
            } else {
                doQuery("INSERT INTO `Users_x_Classifiers` (uid, cid, can_use,can_add,can_validate,can_remove,can_share) VALUES ('$uid','$cid',$use,$add,$val,$del,$sha)", "Users_x_Classifiers");
            }
        }
    }




    clearMemcache("Users_x_Classifiers:Usergroups_x_Classifiers");
    $roles_updated = 'Access Levels Saved';
}

echo "<div class=section>";
echo "<h3>Classifier Sharing:</h3>";
// get classifiers with access
$rows = runQuery("SELECT c.id,c.Name, c.Description, c.Last_Edit,u.LastName,u.FirstName,uc.can_share,uc.can_remove,uc.can_validate FROM `Classifiers` c JOIN `Users` u JOIN `Users_x_Classifiers` uc ON c.id = uc.cid AND c.Owner = u.id WHERE uc.uid = '$userid'", "Classifiers:Users:Users_x_Classifiers");
$out = "";
foreach ($rows as $k => $r) {
    $nrVars = array_shift(...[runQuery("SELECT COUNT(1) AS nrVars FROM `Classifiers_x_Variants` WHERE cid = '" . $r['id'] . "'", "Classifiers_x_Variants")])['nrVars'];

    $out .= "<tr class=vrow><td>" . $r['Name'] . "</td><td>" . $r['Description'] . "</td><td>" . $r['LastName'] . " " . substr($r['FirstName'], 0, 1) . ".</td><td>" . $r['Last_Edit'] . "</td><td>$nrVars</td><td>";
    $actions = "";
    if ($r['can_remove'] == 1 || $r['can_validate'] == 1) {
        $actions = "<a href='index.php?page=inbox_actions&a=1&c=" . $r['id'] . "' class=image><img title='manage included variants' src='Images/layout/edit.gif' style='margin-bottom:-0.1em;height:0.9em;'/></a>";
        if ($r['can_remove'] == 1) {
            $actions .= " / <img title='Delete the classifier' src='Images/layout/icon_trash.gif' style='margin-bottom:-0.1em;height:0.9em;' onClick='DeleteClassifier(\"" . $r['id'] . "\",\"$userid\")' />";
        }
    } else {
        $actions = "<a href='index.php?page=inbox_actions&a=1&c=" . $r['id'] . "' class=image><img title='list included variants' src='Images/layout/eye.png' style='margin-bottom:-0.1em;height:0.9em;'/></a>";
    }
    if ($r['can_share'] == 1) {
        $actions .= " / <a href='index.php?page=classifier&t=manage&c=" . $r['id'] . "' class=image><img title='manage users with access' src='Images/layout/share_icon.png' style='margin-bottom:-0.1em;height:0.9em;'/></a>";
    }
    if ($actions == "") {
        $actions = "&nbsp;";
    }
    $out .= "$actions</td></tr>";
}
if ($out != "") {
    echo "<p>Select one of the classifiers below to edit the access permissions. </p>";
    echo "<p><table cellspacing=0 class='w75'>";
    echo "<tr><th class=top>Classifier Name</th><th class=top>Description</th><th class=top>Created by</th><th class=top>Last Edit</th><th class=top>Nr.Variants</th><th class=top>Actions</th></tr>";
    echo $out;
    echo "<tr><td colspan=6 class=last>&nbsp;</td></tr></table></p>";
}
echo "</div>";

if (isset($_GET['c']) && is_numeric($_GET['c'])) {
    $cid = $_GET['c'];
    //get the name
    $rows = runQuery("SELECT Name FROM `Classifiers` WHERE id = '$cid'", "Classifiers");
    $cname = $rows[0]['Name'];
    echo "<div class=section>";
    echo "<h3>Set classifier access levels for : '$cname'</h3>";
    echo "<p>A classifier is a list of variants, with associated diagnostic class. If a variant in a newly imported VCF is present in an active classifier, the diagnostic class will automatically be assigned. Permissions are set up in multiple levels, being: ";
    echo "<ol><li>Use: read-only usage</li>";
    echo "<li>Add: include new variants for review </li>";
    echo "<li>Validate: review and (de-)activate variants in classifiers</li>";
    echo "<li>Remove: Delete variants from the classifier, or delete complete classifiers</li>";
    echo "<li>Share: Manage access of additional users</li></ol>";
    echo "</p><p>";
    echo "<form action='index.php?page=classifier&t=manage&c=$cid' method=POST>";
    echo "<input type=hidden name=cid value='$cid'/>";
    echo "<span class=emph>Specify additional usergroups who should have access. </span></p>";
    echo " <p><select id='AddGroupSelect'><option value='-' selected disabled></option>";
    // get my affiliation.
    $row = runQuery("SELECT a.id, a.name FROM `Affiliation` a JOIN `Users` u ON u.Affiliation = a.id WHERE u.id = $userid", "Affiliation:Users")[0];
    $myaffi = $row['id'];
    // groups
    $rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug JOIN `Usergroups_x_Classifiers` us ON ug.id = us.gid WHERE us.cid = $cid", "Usergroups:Usergroups_x_Classifiers");
    $shared_groups = array();
    foreach ($rows as $k => $row) {
        $shared_groups[$row['id']] = $row['name'];
    }

    $rows = runQuery("SELECT ug.name, ug.id, ug.affiliation,ug.opengroup FROM `Usergroups` ug ORDER BY name ASC", "Usergroups");
    $usergroupstring = '';
    foreach ($rows as $k => $row) {
        if (isset($shared_groups[$row['id']])) {
            continue;
        }
        $affis = explode(",", $row['affiliation']);
        if ($row['opengroup'] == 0 && !in_array($myaffi, $affis)) {
            continue;
        }
        $usergroupstring .= "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
        //$usergroupstring .= " - ".$row['name']." <img src='Images/layout/plus-icon.png' style='margin-bottom:-0.1em;height:0.9em' onClick=\"ShareWithGroup('Annotation','$id','$uid','".$row['id']."')\" /><br/>"; 
    }
    if ($usergroupstring != '') {
        echo "$usergroupstring";
    } else {
        echo "<option value='-' disabled>No groups available</option>";
    }
    echo "</select> <input type=button class=button name='Add_Group' value='Add Group' onClick='AddGroup($userid)' />  &nbsp; <span id='group_comment_field' style='color:red; font-style:italic;'>$group_roles_updated</span>";


    // permissions table.
    echo "<p><table id='permtable' cellspacing=0 class=w50 style='text-align:center'>";
    echo "<tr><td>&nbsp;</td><th colspan=3 class=btop>Variants</th><th class=btop colspan=2>Classifier</th></tr>";
    echo "<tr ><th class=btop>Usergroup</th><th class=btop>Add</th><th class=btop>Validate</th><th class=btop>Remove</th><th class=btop>Apply</th><th class=btop>Share</th></tr>";
    $rows = runQuery("SELECT u.name, uc.gid, uc.can_use, uc.can_add, uc.can_validate, uc.can_remove, uc.can_share FROM `Usergroups` u JOIN `Usergroups_x_Classifiers` uc ON u.id = uc.gid WHERE uc.cid = '$cid' ORDER BY u.name", "Usergroups:Usergroups_x_Classifiers");
    foreach ($rows as $row) {
        $line = "<tr id='access_" . $row['gid'] . "'><td><input type=hidden name='add_groups[]' value='" . $row['gid'] . "'/>" . $row['name'] . "</td>";
        $checked = ($row['can_add'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='g_can_add[]' value='" . $row['gid'] . "' $checked /></td>";
        $checked = ($row['can_validate'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='g_can_validate[]' value='" . $row['gid'] . "' $checked /></td>";
        $checked = ($row['can_remove'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='g_can_remove[]' value='" . $row['gid'] . "' $checked /></td>";
        $checked = ($row['can_use'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='g_can_use[]' value='" . $row['gid'] . "' $checked /></td>";
        $checked = ($row['can_share'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='g_can_share[]' value='" . $row['gid'] . "' $checked /></td>";
        $line .= "</tr>";
        echo $line;
    }
    echo "<tr id='ownerrowGroup'><td class=last colspan=6>&nbsp;</td></tr>";
    echo "</table>";
    echo "<p><input type=submit class=button name='SaveChanges' value='Save Changes' /></p>";
    // users

    echo "<span class=emph>Specify additional users who should have access. </span></p>";
    echo " <p>Type a name: <input type=hidden name=AddUserID id=AddUserID value='' /> <input type=text size='50' value='' name='usersource' id='searchfield' /> <input type=button  class=button name='AddUser' value='Add User' onClick='AddUsers($userid);' /> &nbsp; <span id='comment_field' style='color:red; font-style:italic;'>$roles_updated</span>";
    echo "</p>";

    // permissions table.
    echo "<p><table id='permtable' cellspacing=0 class=w50 style='text-align:center'>";
    echo "<tr><td>&nbsp;</td><th colspan=3 class=btop>Variants</th><th class=btop colspan=2>Classifier</th></tr>";
    echo "<tr ><th class=btop>User</th><th class=btop>Add</th><th class=btop>Validate</th><th class=btop>Remove</th><th class=btop>Apply</th><th class=btop>Share</th></tr>";
    // current users.
    $rows = runQuery("SELECT u.LastName, u.FirstName, uc.uid, uc.can_use, uc.can_add, uc.can_validate, uc.can_remove, uc.can_share,uc.cid FROM `Users` u JOIN `Users_x_Classifiers` uc ON u.id = uc.uid WHERE uc.cid = '$cid' ORDER BY u.LastName DESC", "Users:Users_x_Classifiers");
    foreach ($rows as $k => $row) {
        $line = "<tr id='access_" . $row['uid'] . "'><td><input type=hidden name='add_users[]' value='" . $row['uid'] . "'/>" . $row['LastName'] . " " . $row['FirstName'] . "</td>";
        $checked = ($row['can_add'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='can_add[]' value='" . $row['uid'] . "' $checked /></td>";
        $checked = ($row['can_validate'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='can_validate[]' value='" . $row['uid'] . "' $checked /></td>";
        $checked = ($row['can_remove'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='can_remove[]' value='" . $row['uid'] . "' $checked /></td>";
        $checked = ($row['can_use'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='can_use[]' value='" . $row['uid'] . "' $checked /></td>";
        $checked = ($row['can_share'] == 1) ? 'checked' : '';
        $line .= "<td><input type='checkbox' name='can_share[]' value='" . $row['uid'] . "' $checked /></td>";
        $line .= "</tr>";
        echo $line;
    }
    echo "<tr id='ownerrow'><td class=last colspan=6>&nbsp;</td></tr>";
    echo "</table>";
    echo "<p><input type=submit class=button name='SaveChanges' value='Save Changes' /></form></p>";
    echo "</div>";
}
