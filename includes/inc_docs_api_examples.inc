<?php 
// syntax 
?>
<div class=section>
<h3>Documentation : API access to VariantDB : Example Scripts</h3>

<a name="LoadFilter"></a>
<h4 class=underline >Import/Export Filter Sets</h4>
<p class=indent>Store a set of filters to a text file for import into a different VariantDB instance. <a href='api/Example_Scripts/Import_Export_Filter.py'>Right-click</a> to download the script.</p>
<div class=block><div class=code>Code</div>
<pre>
<?php
include("api/Example_Scripts/Import_Export_Filter.py");
?>
</pre>
</div>


<a name="LoadAnnotations"></a>
<h4 class=underline >Import/Export Annotation Sets</h4>
<p class=indent>Store a set of annotations to a text file for import into a different VariantDB instance. <a href='api/Example_Scripts/Import_Export_Annotation.py'>Right-click</a> to download the script.</p>
<div class=block><div class=code>Code</div>
<pre>
<?php
include("api/Example_Scripts/Import_Export_Annotation.py");
?>
</pre>
</div>

<a name="RunBatchQuery"></a>
<h4 class=underline >Submit Batch Queries & Retrieve results</h4>
<p class=indent>Run the same provided filter set, and optional annotation set on multiple samples. Results are fetched in json format for downstream analysis. <a href='api/Example_Scripts/Run_Batch_Query.py'>Right-click</a> to download the script.</p>
<div class=block><div class=code>Code</div>
<pre>
<?php
include("api/Example_Scripts/Run_Batch_Query.py");
?>
</pre>
</div>

