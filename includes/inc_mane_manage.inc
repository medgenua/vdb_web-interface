<?php


// action DELETE Set ? //
if (isset($_GET['a']) && $_GET['a'] == 'd' && isset($_GET['setid']) && $_GET['setid'] != '') {
    $setid = $_GET['setid'];
    //allowed (only owner)?
    $rows = runQuery("SELECT `owner` FROM `TranscriptSets` WHERE set_id = '$setid'", "TranscriptSets");
    if ($rows[0]['owner'] != $userid) {
        echo "<div class=section>";
        echo "<h3>Insufficient Privileges</h3>";
        echo "<p>You are not allowed to delete this set of transcripts.</p>";
        echo "</div>";
    }
    // ok, allowed.
    else {
        // first present a confirmation.
        if (!isset($_GET['confirm']) || $_GET['confirm'] != 'yes') {
            $rows = runQuery("SELECT `name` FROM `TranscriptSets` WHERE set_id = '$setid'", 'TranscriptSets');
            $setname = $rows[0]['name'];
            echo "<div class=section>";
            echo "<h3>Delete Transcript Set:  '$setname' </h3>";
            echo "<p>Are you sure you want to delete this set ?&nbsp;&nbsp;&nbsp; ";
            echo "<a href='index.php?page=mane&t=manage&a=d&setid=$setid&confirm=yes' class=emph>Yes</a> / <a href='index.php?page=mane&t=manage' class=emph>No</a></p>";
            echo "</div>";
            exit;
        } else {
            // delete 
            doQuery("DELETE FROM `TranscriptSets_Data` WHERE set_id = '$setid'", "TranscriptSets_Data");
            doQuery("DELETE FROM `TranscriptSets_x_UserGroups` WHERE set_id = '$setid'", "TranscriptSets_x_UserGroups");
            doQuery("DELETE FROM `TranscriptSets_x_Users` WHERE set_id = '$setid'", "TranscriptSets_x_Users");
            doQuery("DELETE FROM `TranscriptSets` WHERE set_id = '$setid'", "TranscriptSets");
        }
    }
}
// SHARE panel ?
elseif (isset($_GET['a']) && $_GET['a'] == 's' && isset($_GET['setid']) && $_GET['setid'] != '') {
    $setid = $_GET['setid'];
    //allowed (only owner)?
    $rows = runQuery("SELECT `owner` FROM `TranscriptSets` WHERE set_id = '$setid'", "TranscriptSets");
    if ($rows[0]['owner'] != $userid) {
        echo "<div class=section>";
        echo "<h3>Insufficient Privileges</h3>";
        echo "<p>You are not allowed to share this set of transcripts.</p>";
        echo "</div>";
    }
    include('includes/inc_mane_share.inc');
    exit;
}

// list them.
echo "<div class=section>";
echo "<h3>Available Transcript Sets:</h3>";

// get sets accessible by user.
$query = "SELECT t.`set_id`, t.`name`, t.`owner`, tu.`edit` FROM `TranscriptSets` t JOIN `TranscriptSets_x_Users` tu ON t.`set_id` = tu.`set_id` WHERE tu.uid = '$userid' ORDER BY t.`name`";
$rows = runQuery($query, "TranscriptSets:TranscriptSets_x_Users");

if (count($rows) == 0) {
    echo "<p class=indent>No transcript sets available.</p>";
} else {
    echo "<p ><table cellspacing=0 class=indent style='min-width:60%'><tr><th class=top>Set Name</th><th class=top>Set Owner</th><th class=top>Nr.Transcripts</th><th class=top>Action</th></tr>";
    foreach ($rows as $key => $row) {
        $id = $row['set_id'];
        $name = $row['name'];
        $rw = $row['edit'];
        $owner_id = $row['owner'];
        // nr of transcripts
        $sr = runQuery("SELECT `nm_id` FROM `TranscriptSets_Data` WHERE `set_id` = '$id'", "TranscriptSets_Data");
        $nrNM = count($sr);
        $action = '';
        if ($rw == 1) {
            $action .= " <a href='index.php?page=mane&t=edit&setid=$id'>Edit Transcripts</a> /";
        } else {
            $action .= " <a href='index.php?page=mane&t=show&setid=$id'>Show Transcripts</a> /";
        }
        if ($owner_id == $userid) {
            $action .= " <a href='index.php?page=mane&t=manage&a=s&setid=$id'>Share</a> /";
            $action .= " <a href='index.php?page=mane&t=manage&a=d&setid=$id'>Delete</a> /";
        }
        $action = substr($action, 1, -2);
        // owner name
        $sr = runQuery("SELECT LastName, FirstName FROM `Users` WHERE `id` = '$owner_id'", "Users");
        $owner = $sr[0]["LastName"] . " " . substr($sr[0]["FirstName"], 0, 1) . ".";
        echo "<tr><td>$name</td><td>$owner</td><td>$nrNM</td><td>$action</td></tr>";
    }
    echo "<tr><td colspan=5 class=last>&nbsp;</td></tr>";
    echo "</table>";
    echo "</p>";
}





echo "</div>";
