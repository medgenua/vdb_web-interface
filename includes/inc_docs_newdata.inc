<div class=section>
    <h3>Documentation : Load Samples Into VariantDB </h3>
    <p>Data can be loaded either by uploading it to an FTP server, or by sending it directly from a galaxy server.</p>
    <h4>1. FTP upload</h4>
    <p class=indent>
    <ol>
        <li>Go to : <a href='index.php?page=upload'>New Data</a> in the top menu.</li>
        <li>If your system administrator has enabled ftp upload, you should see the instructions to connect to the server. It might look like: <br /><img style='width:40em;padding-left:3em;' src='Images/Content/ftp_instructions.png' alt='instructions for ftp access' /></li>
        <li>After uploading VCF/BAM files to FTP server, come back to the New Data page. You will now see a selection box on top containing the uploaded data:<br /><img style='width:40em;padding-left:3em;' src='Images/Content/upload_select.png' alt='selection menu for vcf upload' /></li>
        <li>Provide the requested information and select the data files to store in VariantDB. If 'Store Datafiles' is checked, data can be loaded directly to IGV from VariantDB</li>
        <li>To load more samples at the same time, select 'Load More Samples'</li>
        <li>Select 'Load Samples Into Database'. A status screen is shown. You can safely close this screen</li>
        <li>Upon completion of the import, you will recieve an email notification</li>

    </ol>
    </p>
    <h4>2. Import from Galaxy</h4>
    <p class=indent>
    <ol>
        <li>Install the VariantDB tool for galaxy. This is available from the toolshed <a href='https://toolshed.g2.bx.psu.edu/view/geert-vandeweyer/vcf_to_variantdb'>here</a>.</li>
        <li>A publicly available galaxy server with this tool installed is available <a href='http://143.169.238.104/galaxy'>here</a></li>
        <li>Make sure you use the EXACT same username in galaxy and VariantDB</li>
        <li>Generate a VCF file (preferably with GATK Unified Genotyper</li>
        <li>Open the 'VCF To VariantDB' tool:<br /><img style='width:40em;padding-left:3em;' src='Images/Content/vcf2VariantDB.png'></li>
        <li>Provide necessary information and select execute. Import will be finished when the job finishes.</li>
    </ol>
    </p>
    </p>

</div>