<div class=section>
<h3>Install VariantDB on local hardware</h3>

<p class=emph>Prerequisites</p>
<p><ol>
<li>Operating system: Installation was tested on Ubuntu 12.04 LTS, 64bit. </li>
<li>LAMP server with Perl: MySQL-databases can be located on remote servers.</li>
<li>Mercurial : Used for installing the system</li>
<li>Memcache (optional)</li>
<li>ProFTPd (optional)</li>
</ol>

</p>

<p><span class=emph>Install the VariantDB basics</span>
<ol>
 <li>Clone the Installation Scripts: `hg clone https://bitbucket.org/medgenua/vdb_installer`</li>
 <li>Enter the cloned directory : `cd vdb_installer`</li>
 <li>Install databases: `sudo ./Install_databases.sh`
	<ol>
		<li>Provide a mysql username and password to access VariantDB data. By default, a new user with random password is generated.</li>
		<li>Provide the mysql host that will host the VariantDB data</li>
		<li>Wild card access can be allowed to the VariantDB databases by using %</li>
		<li>Enter your mysql root password to generate the user.</li>
		<li>The program will now install a base version of the database</li>
		<li>DB-credentials are written to a file in the current folder: .dbpass.txt. Note them down for the next step.</li>
	</ol>
  </li>
  <li>Install the web-interface: `sudo ./Install_Web_Interface.sh`
	  <ol>
		<li>Install dependencies : tofrodos is done automatically, others you have to agree for</li>
		<li>Install cpan Modules: done automatically. They are only installed if missing, not updated. Make sure to use the 'sudo' approach of cpan instead of local::lib if initial configuration is run. Otherwise, the modules might not be available to the dedicated runtime user!</li>
		<li>Provide Installation options (all documented in installer output)</li>
		<li>The system is downloaded (by mercurial) and installed</li>
		<li>The annotation databases are downloaded (from ucsc/annovar/snpEff). This totals in approximately 500Gb ! The first run installs a core set of annotations. Afterwards you get information on how to enable old and very large annotation sests, or how to install them anyway.</li>
		<li>Provide an email to perform administrative tasks.</li>
		<li>Provide a user to execute the annotation tasks. All VariantDB data will be owned by this user. By default, a new user with random password is generated.</li>
		<li>Provide the mysql user created in the previous step (install_databases) if asked. If you install databases and web from the same machine/folder, credentials are read in automatically. The credentials are available in the file .dbpass.txt </li>
		<li>An edit to sudoers.d is made to allow www-user to switch to the script user</li>
		<li>cgi-bin configuration is updated</li>
		<li>php.ini is updated</li>
		<li>You're done.</li>
	  </ol>
  </li>
</ol>
</p>
<p class=emph>Tune Database</p>
<p><ol>
	<li>VariantDB uses MyISAM databases. The following variables optimize the mysql server for large MyISAM tables.</li>
	<li>Open mySQL config file (/etc/mysql/my.cnf) on the databases server</li>
	<li>Edit the following settings to settings appriate for your server:<ol>
		<li>[mysqld]</li>
		<li>key_buffer_size = 4048M</li>
		<li>max_allowed_packet = 512M</li>
		<li>thread_stack = 256K</li>
		<li>thread_cache_size = 8</li>
		<li>tmp_table_size = 2048M</li>
		<li>max_heap_table_size = 2048M</li>
		<li>bulk_insert_buffer_size = 128M</li>
		<li>connect_timeout         = 15</li>
		<li><span style='color:red'>local_infile  </span> &lt;= MANDATORY</li>
		<li>myisam_repair_threads   = 4</li>
		<li>myisam_sort_buffer_size = 4048M</li>
		<li>open_files_limit        = 8000</li>
		<li>query_cache_limit       = 12M</li>
		<li>query_cache_size        = 128M</li>
		<li>read_buffer_size        = 16M</li>
		<li>read_rnd_buffer_size    = 8M</li>
		<li>open_files_limit        = 8000</li>
	</ol></li>
	<li>Restart MySQL server </li>
</ol>
</p>
<p class=emph>Start Annotation Monitor</p>
<p><ol>
	<li>The following script monitors triggers for new data and launches annotation jobs.<li>
	<li>open /etc/rc.local for editing (sudo vim /etc/rc.local)</li>
	<li>Just before 'exit 0', add the following line (%scriptuser% is listed in VariantDB/.Credentials/.credentials file):<br/>
		&nbsp; su %scriptuser% -c 'cd /path_to/VariantDB/Web-Interface/Annotations/ && perl monitor.pl > /dev/null 2>&1 &'</li>
	<li>Reboot the system</li>
</ol></p>
<p class=emph>Initialize VariantDB</p>
<p><ol>
	<li>Go to your installation : http://&lt;domain&gt;/&lt;path&gt;</li>

	<li>Create the first user. This user will be a platform administrator</li>
	<li>Once you submit this form, access to VariantDB is activated for all users.</li>
</ol>
</p>
<p class=emph>Set up ProFTPD </p>
<p>Note: Guidelines apply to Ubuntu 14.04 LTS<ol>
	<li>Install Proftp (version &ge; 1.3.4) and mysql module : apt-get install proftpd proftpd-mod-mysql</li>
	<li>Activate mysql modules in /etc/proftpd/proftpd.conf:<pre>
LoadModule mod_sql.c
LoadModule mod_sql_mysql.c
LoadModule mod_sql_passwd.c</pre></li>
	<li>Add to the /etc/proftpd/proftpd.conf:<pre>
# chroot users to their homes
DefaultRoot ~
# Create home directories as needed, using chmod 755
# chmod 755 is set to make them www-user readable. Adapt to use groups
CreateHome on 755 dirmode 755
#activate mysql backend
SQLEngine on
SQLBackend mysql
# Set up mod_sql_password - VariantDB passwords are stored as sha1-encoded 
SQLPasswordEngine  on
SQLPasswordEncoding hex 
AuthOrder mod_sql.c
# adapt 'NGS-Variants".$_SESSION['dbname']."' after a Genome Build update
SQLConnectInfo NGS-Variants<?php echo $_SESSION['dbname'];?>@&lt;DB-HOST&gt; &lt;DBUSER&gt; &lt;DBPASS&gt;
SQLAuthTypes SHA1
SQLAuthenticate users
# set default homedir to some existing folder. It's not actually used.
SQLDefaultHomedir /tmp
# define the query to authenticate users against VariantDB database.
SQLUserInfo custom:/LookupVariantDBUser
SQLNamedQuery LookupVariantDBUser SELECT "email, password_sha1, '&lt;scriptuser_UID&gt;', '&lt;scriptuser_GID&gt;', '&lt;path_to_variant_db_ftp-data&gt;/%U', '/bin/bash' FROM Users WHERE email = '%U'"</pre></li>
	<li>The parameters to replace are found in /&lt;VariantDB&gt;/.Credentials/.credentials file </li>
	<li>Restart proftpd : sudo /etc/init.d/proftpd restart</li>
	<li>Set the following lines in <?php echo $file ?><pre>
FTP=1
FTP_HOST=YOUR FTP SERVER IP/NAME
FTP_PORT=YOUR FTP SERVER PORT
FTP_DATA=/path/to/the/ftp/data/base/dir</pre></li>
</ol></p>

</div>		
		
		
