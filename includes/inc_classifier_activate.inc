<?php
// permissions submitted? 
$roles_updated = '';
if (isset($_POST['SaveChanges'])) {
    $active = $_POST['active'];
    // get the ones with access.
    $rows = runQuery("SELECT cid FROM `Users_x_Classifiers` WHERE uid = $userid AND `can_use` = 1", "Users_x_Classifiers");
    foreach ($rows as $k => $r) {
        $a = (in_array($r['cid'], $active) ? 1 : 0);
        doQuery("UPDATE `Users_x_Classifiers` SET `apply_on_import` = $a WHERE uid = $userid AND cid = '" . $r['cid'] . "'", "Users_x_Classifiers");
    }

    clearMemcache("Users_x_Classifiers");
    $active_updated = 'Active Classifiers Updated';
}

echo "<div class=section>";
echo "<h3>Classifier Activation:</h3>";
// get classifiers with access
$rows = runQuery("SELECT c.id,c.Name, c.Description, c.Last_Edit,u.LastName,u.FirstName,uc.apply_on_import FROM `Classifiers` c JOIN `Users` u JOIN `Users_x_Classifiers` uc ON c.id = uc.cid AND c.Owner = u.id WHERE uc.uid = '$userid' AND uc.can_use = 1", "Classifiers:Users:Users_x_Classifiers");
$out = "";
foreach ($rows as $k => $r) {
    $a = ($r['apply_on_import'] == 1) ? 'checked=checked' : '';
    $out .= "<tr class=vrow><td>" . $r['Name'] . "</td><td>" . $r['Description'] . "</td><td>" . $r['LastName'] . " " . substr($r['FirstName'], 0, 1) . ".</td><td>" . $r['Last_Edit'] . "</td><td> <input type=checkbox name='active[]' value='" . $r['id'] . "' $a >";
    if ($actions == "") {
        $actions = "&nbsp;";
    }
    $out .= "$actions</td></tr>";
}
if ($out != "") {
    echo "<form action='index.php?page=classifier&t=activate' method=POST>";
    echo "<p>Select classifiers to apply on new sample import, then submit the changes. </p>";
    echo "<p><table cellspacing=0 class='w75'>";
    echo "<tr><th class=top>Classifier Name</th><th class=top>Description</th><th class=top>Created by</th><th class=top>Last Edit</th><th class=top>Active</th></tr>";
    echo $out;
    echo "<tr><td colspan=5 class=last>&nbsp;</td></tr></table></p>";
    echo "<p><input type=submit class=button name='SaveChanges' value='Save Changes' /></form></p>";
    echo "</div>";
} else {
    echo "<p>No available classifiers.</p>";
}
