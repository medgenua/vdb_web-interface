<?php
// set id
if (!isset($_GET['setid']) || !is_numeric($_GET['setid'])) {
    trigger_error("Invalid/No set id reporterd", E_USER_ERROR);
    echo "<p>Invalid Set ID provided.</p=>";
    exit;
}
$setid = $_GET['setid'];

//////////////////////////
// INITIAL SHARING FORM //
//////////////////////////
if (!isset($_POST['share']) && !isset($_POST['finalise'])) {
    // get set details
    $row = runQuery("SELECT name, owner FROM `TranscriptSets` WHERE set_id = '$setid'", "TranscriptSets")[0];
    $setname = $row['name'];
    $setown = $row['owner'];
    // get my own affiliation
    $row = runQuery("SELECT Affiliation FROM `Users` WHERE id = '$userid'", "Affiliation")[0];
    $myaffi = $row['Affiliation'];

    echo "<div class=section>\n";
    echo "<h3>Share Transcript Set: '$setname'</h3>\n";
    echo "<p>Select the users or usergroups you want to share this set with from the lists below and press 'Share'. Detailed settings can be set on the following page.</p>\n";
    echo "<p>\n";
    echo "<form action='index.php?page=mane&t=manage&a=s&setid=$setid' method=POST>\n";
    echo "<p><table cellspacing=20>\n";
    echo "<tr>\n";
    // GROUPS

    // get groups with access
    $groups = runQuery("SELECT ug.id, ug.name, tsg.edit FROM `Usergroups` ug JOIN `TranscriptSets_x_UserGroups` tsg ON ug.id = tsg.gid WHERE tsg.set_id = '$setid'", "Usergroups:TranscriptSets_x_UserGroups");
    echo "<ol>\n";
    $gwa = array(); // groups with access
    foreach ($groups as $k => $row) {
        $ugname = $row['name'];
        $ugid = $row['id'];
        $ugrw = $row['edit'];
        $gwa[$ugid] = "$ugname@@@$ugrw";
    }
    // list groups without access 
    echo "<td class=clear valign=top>\n";
    echo "<span class=emph>Share With Usergroups</span><br>\n";
    echo "<select name='groups[]' size=10 MULTIPLE>\n";
    // get my affiliation.
    $row = runQuery("SELECT a.id, a.name FROM `Affiliation` a JOIN `Users` u ON u.Affiliation = a.id WHERE u.id = $userid", "Affiliation:Users")[0];
    $myaffi = $row['id'];
    $myaffiname = $row['name'];
    // list groups without access
    echo "<OPTGROUP label='Private'>";
    $rows = runQuery("SELECT ug.name, ug.id, ug.affiliation FROM `Usergroups` ug WHERE ug.opengroup = 0 ORDER BY name ASC", "Usergroups");
    $printed = 0;
    foreach ($rows as $k => $row) {
        if (isset($gwa[$row['id']])) {
            continue;
        }
        $affis = explode(",", $row['affiliation']);
        if (!in_array($myaffi, $affis)) {
            continue;
        }
        $printed++;
        echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
    }

    if ($printed == 0) {
        echo "<option value='' DISABLED>No usergroups available</option>";
    }
    echo " </OPTGROUP>";
    // open
    echo "<OPTGROUP label='Public'>";
    $rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug WHERE ug.opengroup = 1 ORDER BY name ASC", "Usergroups");
    $printed = 0;

    foreach ($rows as $k => $row) {
        if (isset($gwa[$row['id']])) {
            continue;
        }
        $printed++;
        echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
    }

    if ($printed == 0) {
        echo "<option value='' DISABLED>No usergroups available</option>";
    }
    echo "</OPTGROUP>";

    echo "</select>\n";
    echo "</td>\n";

    // get seperate users with full access
    $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name, ts.edit FROM `Users`u JOIN `TranscriptSets_x_Users` ts JOIN `Affiliation` a ON u.Affiliation = a.id AND u.id = ts.uid WHERE ts.set_id = '$setid' ORDER BY a.id, u.LastName", "Users:TranscriptSets_x_Users:Affiliation");
    $uwa = array();
    foreach ($rows as $k => $row) {
        $lname = $row['LastName'];
        $currid = $row['id'];
        $fname = $row['FirstName'];
        $affi = $row['name'];
        $editrw = $row['edit'];
        $uwa[$currid] = "$lname $fname@@@$affi@@@$editrw";
    }
    // list users with no or restricted access
    echo "<td class=clear valign=top>\n";
    echo "<span class=emph>Share With Users</span><br>\n";
    echo "<select name='users[]' size=10 MULTIPLE>\n";
    $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id ORDER BY a.name, u.LastName", "Users:Affiliation");
    $currinst = '';
    $printed = 0;
    foreach ($rows as $k => $row) {
        $uid = $row['id'];
        // skip users with full access
        if (array_key_exists($uid, $uwa)) {
            $pieces = explode('@@@', $uwa[$uid]);
            if ($pieces[2] == 1) {
                continue;
            }
        }
        $lastname = $row['LastName'];
        $firstname = $row['FirstName'];
        $institute = $row['name'];
        if ($currinst != $institute) {
            if ($currinst != '') {
                echo "</optgroup>";
            }
            echo "<optgroup label='$institute'>\n";
            $currinst = $institute;
        }
        echo "<option value='$uid'>$lastname $firstname</option>\n";
        $printed++;
    }
    if ($printed == 0) {
        echo "<option value='' DISABLED>No users available</option>";
    }
    echo "</select>\n";
    echo "</td>\n";
    echo "</tr>\n";
    echo "</table>\n";
    echo "<input type=submit class=button name=share value='Share'></p>\n";
    echo "</form>\n";
    echo "</div>\n";
    // show who has access now.
    echo "<div class=section>\n";
    echo "<h3>Users with access</h3>\n";
    echo "<p>The transcript set is currently accessible by the following users. 'Full' means that users can edit the set details and/or transcripts, or delete the set altogether. 'Read-only' means no information can be changed. </p>\n";
    echo "You can remove access by clicking on the garbage bin.</p>\n";
    echo "<p><table cellspacing=20 >\n";
    echo "<tr>\n";

    echo "<td valign=top>\n";

    echo "<span class=emph>Usergroups with access:</span><br>\n";
    echo "<ol>\n";
    $printed = 0;
    foreach ($gwa as $gid => $value) {
        $values = explode('@@@', $value);
        $gname = $values[0];
        $editrw = $values[1];
        if ($editrw == 1) {
            $perm = 'Full';
        } else {
            $perm = 'Read-only';
        }
        $printed++;
        echo "<li>$gname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?setid=$setid&u=$gid&type=group&from=share_transcriptsets\"><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></a></li>\n";
    }
    if ($printed == 0) {
        echo "</li> - none</li>";
    }
    echo "</ol>\n";
    echo "</td>\n";

    // seperate users
    echo "<td valign=top >\n";
    echo "<span class=emph>Users with access:</span><br>\n";
    $currinst = '';
    echo "<ul id=nodisc>\n";
    foreach ($uwa as $uid => $value) {
        $pieces = explode('@@@', $value);
        $uname = $pieces[0];
        $institute = $pieces[1];
        $editrw = $pieces[2];
        // check institution
        if ($currinst != $institute) {
            if ($currinst != '') {
                echo "</ol>";
            }
            echo "<li><span class=italic>&nbsp;&nbsp;$institute</span><ol>\n";
            $currinst = $institute;
        }
        // check permissions
        if ($editrw == 1) {
            $perm = 'Full';
        } else {
            $perm = 'Read-only';
        }

        echo "<li>$uname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?setid=$setid&u=$uid&type=user&from=share_transcriptsets\"><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></a></li>\n";
    }
    echo "</ol></ul>\n";
    //echo "</select>\n";
    echo "</td>\n";
    echo "</tr>\n";
    echo "</table>\n";
}
////////////////////////////////////////
// FIRST ROUND OF SHARING : SET PERMS //
////////////////////////////////////////
elseif (isset($_POST['share'])) {
    // get project details
    $row = runQuery("SELECT name FROM `TranscriptSets` WHERE set_id = '$setid'", "TranscriptSets")[0];
    $sname = $row['name'];

    // get posted vars
    $addgroups = isset($_POST['groups']) ? $_POST['groups'] : array();
    $addusers = isset($_POST['users']) ? $_POST['users'] : array();
    echo "<div class=section>\n";
    echo " <h3>Transcript Set '$sname' Shared</h3>\n";
    echo "<form action='index.php?page=mane&t=manage&a=s&setid=$setid' method=POST>\n";
    echo "<input type=hidden name=finalise value=1>\n";

    // process groups
    if (count($addgroups) > 0) {

        echo "<p><span class=emph>Usergroups:</span> Set permissions</p>\n";
        echo "<table cellspacing=0>\n";
        echo "<th class=top>Group</th>\n";
        echo "<th class=top>Read/Write</th>\n";
        echo "</tr>\n";
        $yesno = array('Not Allowed', 'Allowed');
        foreach ($addgroups as $gid) {
            // get userinfo 
            $row = runQuery("SELECT name FROM `Usergroups` WHERE id = '$gid'", "Usergroups")[0];
            $name = $row['name'];

            // print table row
            echo "<tr>\n";
            echo "<td ><input type=hidden name='groups[]' value='$gid'>$name</td>\n";
            echo "<td><input type=radio name='g_rw_$gid' value=1 > Yes <input type=radio name='g_rw_$gid' value=0 checked> No</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n";
        echo "</p>\n";
    }

    // process single users
    if (count($addusers) > 0) {

        echo "<p><span class=emph>Users:</span> Set permissions</p>\n";
        echo "<table cellspacing=0>\n";
        echo "<th class=top>User</th>\n";
        echo "<th class=top>Edit/Delete Panel</th>\n";
        echo "</tr>\n";
        foreach ($addusers as $uid) {
            // get userinfo 
            $row = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '$uid'", "Users")[0];
            $fname = $row['FirstName'];
            $lname = $row['LastName'];
            // print table row
            echo "<tr>\n";
            echo "<td ><input type=hidden name='users[]' value='$uid'>$lname $fname</td>\n";
            echo "<td><input type=radio name='rw_$uid' value=1 > Yes <input type=radio name='rw_$uid' value=0 checked> No</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n";
        echo "</p>\n";
    }
    if (count($addusers) + count($addgroups) > 0) {
        echo "<input type=submit class=button name=finalise value='Finish'>\n";
        echo "</form>\n";
    }
    echo "<p><a href='index.php?page=mane&t=manage&a=s&setid=$setid'>Go Back</a></p>\n";
    echo "</div>\n";
} elseif (isset($_POST['finalise'])) {
    // get project details
    $row = runQuery("SELECT name FROM `TranscriptSets` WHERE set_id = '$setid'", "TranscriptSets")[0];
    $sname = $row['name'];
    echo "<div class=section>\n";
    echo " <h3>Transcript Set '$sname' Shared</h3>\n";

    // get vars: 
    $addusers = isset($_POST['users']) ? $_POST['users'] : array();
    $addgroups = isset($_POST['groups']) ? $_POST['groups'] : array();
    $yesno = array('Not Allowed', 'Allowed');

    // GROUPS
    echo "<p>The following usergroups (and users) gained access to the transcript set.</p>\n";
    echo "<p><table cellspacing=0>\n";
    echo "<th class=top>User</th>\n";
    echo "<th class=top>Edit/Delete Panel</th>\n";
    echo "</tr>\n";
    $printed = 0;
    foreach ($addgroups as $gid) {
        $printed++;
        $editrw = $_POST["g_rw_$gid"];
        // user info
        $row = runQuery("SELECT name FROM `Usergroups` WHERE id = '$gid'", "Usergroups")[0];
        $name = $row['name'];
        // print table row
        echo "<tr>\n";
        echo "<td>$name</td>\n";
        echo "<td>" . $yesno[$editrw] . "</td>\n";
        echo "</tr>\n";
        // INSERT or UPDATE permissions
        doQuery("INSERT INTO `TranscriptSets_x_UserGroups` (set_id, gid, edit) VALUES ('$setid','$gid', '$editrw') ", "TranscriptSets_x_UserGroups");
        // users in the group	
        $rows = runQuery("SELECT uid FROM `Usergroups_x_User` WHERE gid = '$gid'", "Usergroups_x_User");
        foreach ($rows as $k => $row) {
            $uid = $row['uid'];
            // check if has access 
            $arow = array_shift(...[runQuery("SELECT edit FROM `TranscriptSets_x_Users` WHERE set_id = '$setid' AND uid = '$uid'", "TranscriptSets_x_Users")]);
            if (!$arow || count($arow) == 0) {
                doQuery("INSERT INTO `TranscriptSets_x_Users` (set_id, uid, edit) VALUES ('$setid','$uid', '$editrw')", "TranscriptSets_x_Users");
                // notify the user (to Inbox)
                $subject = "New Transcript Set made available ($sname)";
                doQuery("INSERT INTO `Inbox` (Inbox.from, Inbox.to, Inbox.subject, Inbox.body, Inbox.type, Inbox.values, Inbox.date) VALUES ('$userid','$uid','$subject','','shared_transcriptset','$setid',NOW())");
            } else {
                $rw = max($editrw, $arow['edit']);
                doQuery("UPDATE `TranscriptSets_x_Users` SET edit = '$rw' WHERE set_id = '$setid' AND uid = '$uid'", "TranscriptSets_x_Users");
            }
        }
    }
    if ($printed == 0) {
        echo "<tr><td colspan=3>None.</td></tr>";
    }
    echo "</table></p>";


    // USERS
    // print table
    echo "<p>The following users gained access to the Transcript Set.</p>\n";
    echo "<p><table cellspacing=0>\n";
    echo "<th class=top>User</th>\n";
    echo "<th class=top>Edit/Delete Panel</th>\n";
    echo "</tr>\n";
    $printed = 0;
    foreach ($addusers as $uid) {
        $printed++;
        $editrw = $_POST["rw_$uid"];
        // user info
        $row = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '$uid'", "Users")[0];
        $fname = $row['FirstName'];
        $lname = $row['LastName'];
        // print table row
        echo "<tr>\n";
        echo "<td>$lname $fname</td>\n";
        echo "<td>" . $yesno[$editrw] . "</td>\n";
        echo "</tr>\n";
        // INSERT or UPDATE permissions
        doQuery("INSERT INTO `TranscriptSets_x_Users` (set_id, uid, edit) VALUES ('$setid','$uid', '$editrw') ON DUPLICATE KEY UPDATE edit = if(VALUES(edit) < '$editrw', VALUES(edit),'$editrw' )", "GenePanels_x_Users");
        // notify the user (to Inbox)
        $subject = "New Transcript Set made available ($sname)";
        doQuery("INSERT INTO `Inbox` (Inbox.from, Inbox.to, Inbox.subject, Inbox.body, Inbox.type, Inbox.values, Inbox.date) VALUES ('$userid','$uid','$subject','','shared_transcriptset','$setid',NOW())", "Inbox");
    }
    if ($printed == 0) {
        print "<tr><td colspan=3>None.</td></tr>";
    }
    echo "</table></P>";
    echo "<p><a href='index.php?page=mane&t=manage&a=s&setid=$setid'>Go Back</a></p>\n";
    echo "</div>\n";
}
