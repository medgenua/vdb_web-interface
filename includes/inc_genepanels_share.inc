<?php
//////////////////////////
// INITIAL SHARING FORM //
//////////////////////////
if (!isset($_POST['share']) && !isset($_POST['finalise'])) {
    // get panel details
    $row = runQuery("SELECT Name, Owner FROM `GenePanels` WHERE id = '$gpid'", "GenePanels")[0];
    $gpname = $row['Name'];
    $gpown = $row['Owner'];
    // get my own affiliation
    $row = runQuery("SELECT Affiliation FROM `Users` WHERE id = '$userid'", "Affiliation")[0];
    $myaffi = $row['Affiliation'];

    echo "<div class=section>\n";
    echo "<h3>Share Gene Panel: '$gpname'</h3>\n";
    echo "<p>Select the users or usergroups you want to share this project with from the lists below and press 'Share'. Detailed settings can be set on the following page.</p>\n";
    echo "<p>\n";
    echo "<form action='index.php?page=genepanels&t=manage&a=s&p=$gpid' method=POST>\n";
    //echo" <input type=hidden name=gpid value='$gpid'></p>\n";
    echo "<p><table cellspacing=20>\n";
    echo "<tr>\n";
    // GROUPS

    // get groups with access
    $groups = runQuery("SELECT ug.id, ug.name, gpg.rw, gpg.share FROM `Usergroups` ug JOIN `GenePanels_x_Usergroups` gpg ON ug.id = gpg.gid WHERE gpg.gpid = '$gpid'", "Usergroups:GenePanels_x_Usergroups");
    echo "<ol>\n";
    $gwa = array(); // groups with access
    foreach ($groups as $k => $row) {
        $ugname = $row['name'];
        $ugid = $row['id'];
        $ugshare = $row['share'];
        $ugrw = $row['rw'];
        $gwa[$ugid] = "$ugname@@@$ugrw@@@$ugshare";
    }
    // list groups without access 
    echo "<td class=clear valign=top>\n";
    echo "<span class=emph>Share With Usergroups</span><br>\n";
    echo "<select name='groups[]' size=10 MULTIPLE>\n";
    // get my affiliation.
    $row = runQuery("SELECT a.id, a.name FROM `Affiliation` a JOIN `Users` u ON u.Affiliation = a.id WHERE u.id = $userid", "Affiliation:Users")[0];
    $myaffi = $row['id'];
    $myaffiname = $row['name'];
    // list groups without access
    echo "<OPTGROUP label='Private'>";
    $rows = runQuery("SELECT ug.name, ug.id, ug.affiliation FROM `Usergroups` ug WHERE ug.opengroup = 0 ORDER BY name ASC", "Usergroups");
    $printed = 0;
    foreach ($rows as $k => $row) {
        if (isset($gwa[$row['id']])) {
            continue;
        }
        $affis = explode(",", $row['affiliation']);
        if (!in_array($myaffi, $affis)) {
            continue;
        }
        $printed++;
        echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
    }

    if ($printed == 0) {
        echo "<option value='' DISABLED>No usergroups available</option>";
    }
    echo " </OPTGROUP>";
    // open
    echo "<OPTGROUP label='Public'>";
    $rows = runQuery("SELECT ug.name, ug.id FROM `Usergroups` ug WHERE ug.opengroup = 1 ORDER BY name ASC", "Usergroups");
    $printed = 0;

    foreach ($rows as $k => $row) {
        if (isset($gwa[$row['id']])) {
            continue;
        }
        $printed++;
        echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
    }

    if ($printed == 0) {
        echo "<option value='' DISABLED>No usergroups available</option>";
    }
    echo "</OPTGROUP>";

    echo "</select>\n";
    echo "</td>\n";

    // get seperate users with full access
    $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name, gp.rw, gp.share FROM `Users`u JOIN `GenePanels_x_Users` gp JOIN `Affiliation` a ON u.Affiliation = a.id AND u.id = gp.uid WHERE gp.gpid = '$gpid' ORDER BY a.id, u.LastName", "Users:GenePanels_x_Users:Affiliation");

    $uwa = array();
    foreach ($rows as $k => $row) {
        $lname = $row['LastName'];
        $currid = $row['id'];
        $fname = $row['FirstName'];
        $affi = $row['name'];
        $editrw = $row['rw'];
        $editshare = $row['share'];
        $uwa[$currid] = "$lname $fname@@@$affi@@@$editrw@@@$editshare";
    }
    // list users with no or restricted access
    echo "<td class=clear valign=top>\n";
    echo "<span class=emph>Share With Users</span><br>\n";
    echo "<select name='users[]' size=10 MULTIPLE>\n";
    $rows = runQuery("SELECT u.id, u.LastName, u.FirstName, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id ORDER BY a.name, u.LastName", "Users:Affiliation");
    $currinst = '';
    $printed = 0;
    foreach ($rows as $k => $row) {
        $uid = $row['id'];
        // skip users with full access
        if (array_key_exists($uid, $uwa)) {
            $pieces = explode('@@@', $uwa[$uid]);
            if ($pieces[2] == 1 && $pieces[3] == 1) {
                continue;
            }
        }
        $lastname = $row['LastName'];
        $firstname = $row['FirstName'];
        $institute = $row['name'];
        if ($currinst != $institute) {
            if ($currinst != '') {
                echo "</optgroup>";
            }
            echo "<optgroup label='$institute'>\n";
            $currinst = $institute;
        }
        echo "<option value='$uid'>$lastname $firstname</option>\n";
        $printed++;
    }
    if ($printed == 0) {
        echo "<option value='' DISABLED>No users available</option>";
    }
    echo "</select>\n";
    echo "</td>\n";
    echo "</tr>\n";
    echo "</table>\n";
    echo "<input type=submit class=button name=share value='Share'></p>\n";
    echo "</form>\n";
    echo "</div>\n";
    // show who has access now.
    echo "<div class=section>\n";
    echo "<h3>Users with access</h3>\n";
    echo "<p>The genepanel is currently accessible by the following users. 'Full' means that users can edit the panel details and/or genes, or delete the panel altogether. 'Read-only' means no information can be changed. 'Panel Control' means this user can share the panel with additional users.</p>\n";
    echo "You can remove access by clicking on the garbage bin.</p>\n";
    echo "<p><table cellspacing=20 >\n";
    echo "<tr>\n";

    echo "<td valign=top>\n";

    echo "<span class=emph>Usergroups with access:</span><br>\n";
    echo "<ol>\n";
    $printed = 0;
    foreach ($gwa as $gid => $value) {
        $values = explode('@@@', $value);
        $gname = $values[0];
        $editrw = $values[1];
        $editshare = $values[2];
        if ($editrw == 1) {
            $perm = 'Full';
        } else {
            $perm = 'Read-only';
        }
        if ($editshare == 1) {
            $perm .= " / Project Control";
        }
        $printed++;
        echo "<li>$gname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?gpid=$gpid&u=$gid&type=group&from=share_genepanels\"><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></a></li>\n";
    }
    if ($printed == 0) {
        echo "</li> - none</li>";
    }
    echo "</ol>\n";
    echo "</td>\n";

    // seperate users
    echo "<td valign=top >\n";
    echo "<span class=emph>Users with access:</span><br>\n";
    $currinst = '';
    echo "<ul id=nodisc>\n";
    foreach ($uwa as $uid => $value) {
        $pieces = explode('@@@', $value);
        $uname = $pieces[0];
        $institute = $pieces[1];
        $editrw = $pieces[2];
        $editshare = $pieces[3];
        // check institution
        if ($currinst != $institute) {
            if ($currinst != '') {
                echo "</ol>";
            }
            echo "<li><span class=italic>&nbsp;&nbsp;$institute</span><ol>\n";
            $currinst = $institute;
        }
        // check permissions
        if ($editrw == 1) {
            $perm = 'Full';
        } else {
            $perm = 'Read-only';
        }
        if ($editshare == 1 || $pown == $uid) {
            $perm .= " / Panel Control";
        }

        echo "<li>$uname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?gpid=$gpid&u=$uid&type=user&from=share_genepanels\"><img src='Images/layout/icon_trash.gif' style='width:1.1em;'></a></li>\n";
    }
    echo "</ol></ul>\n";
    //echo "</select>\n";
    echo "</td>\n";
    echo "</tr>\n";
    echo "</table>\n";
}
////////////////////////////////////////
// FIRST ROUND OF SHARING : SET PERMS //
////////////////////////////////////////
elseif (isset($_POST['share'])) {
    // get project details
    $row = runQuery("SELECT Name FROM `GenePanels` WHERE id = '$gpid'", "GenePanels")[0];
    $pname = $row['Name'];

    // get posted vars
    $addgroups = isset($_POST['groups']) ? $_POST['groups'] : array();
    $addusers = isset($_POST['users']) ? $_POST['users'] : array();
    echo "<div class=section>\n";
    echo " <h3>GenePanel '$pname' Shared</h3>\n";
    echo "<form action='index.php?page=genepanels&t=manage&a=s&p=$gpid' method=POST>\n";
    echo "<input type=hidden name=finalise value=1>\n";

    // process groups
    if (count($addgroups) > 0) {

        echo "<p><span class=emph>Usergroups:</span> Set permissions</p>\n";
        echo "<table cellspacing=0>\n";
        echo "<th class=top>Group</th>\n";
        echo "<th class=top>Read/Write</th>\n";
        echo "<th class=top>Panel Control</th>\n";
        echo "</tr>\n";
        $yesno = array('Not Allowed', 'Allowed');
        foreach ($addgroups as $gid) {
            // get userinfo 
            $row = runQuery("SELECT name FROM `Usergroups` WHERE id = '$gid'", "Usergroups")[0];
            $name = $row['name'];

            // print table row
            echo "<tr>\n";
            echo "<td ><input type=hidden name='groups[]' value='$gid'>$name</td>\n";
            echo "<td><input type=radio name='g_rw_$gid' value=1 > Yes <input type=radio name='g_rw_$gid' value=0 checked> No</td>\n";
            echo "<td><input type=radio name='g_share_$gid' value=1 > Yes <input type=radio name='g_share_$gid' checked value=0> No</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n";
        echo "</p>\n";
    }

    // process single users
    if (count($addusers) > 0) {

        echo "<p><span class=emph>Users:</span> Set permissions</p>\n";
        //echo "<input type=hidden name=pid value='$pid'>\n";
        echo "<table cellspacing=0>\n";
        echo "<th class=top>User</th>\n";
        echo "<th class=top>Edit/Delete Panel</th>\n";
        echo "<th class=top>Share Control</th>\n";
        echo "</tr>\n";
        foreach ($addusers as $uid) {
            // get userinfo 
            $row = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '$uid'", "Users")[0];
            $fname = $row['FirstName'];
            $lname = $row['LastName'];
            // print table row
            echo "<tr>\n";
            echo "<td ><input type=hidden name='users[]' value='$uid'>$lname $fname</td>\n";
            echo "<td><input type=radio name='rw_$uid' value=1 > Yes <input type=radio name='rw_$uid' value=0 checked> No</td>\n";
            echo "<td><input type=radio name='share_$uid' value=1 > Yes <input type=radio name='share_$uid' checked value=0> No</td>\n";
            echo "</tr>\n";
        }
        echo "</table>\n";
        echo "</p>\n";
    }
    if (count($addusers) + count($addgroups) > 0) {
        echo "<input type=submit class=button name=finalise value='Finish'>\n";
        echo "</form>\n";
    }
    echo "<p><a href='index.php?page=genepanels&t=manage&a=s&p=$gpid'>Go Back</a></p>\n";
    echo "</div>\n";
} elseif (isset($_POST['finalise'])) {
    // get project details
    $row = runQuery("SELECT Name FROM `GenePanels` WHERE id = '$gpid'", "GenePanels")[0];
    $pname = $row['Name'];
    echo "<div class=section>\n";
    echo " <h3>GenePanel '$pname' Shared</h3>\n";

    // get vars: 
    $addusers = isset($_POST['users']) ? $_POST['users'] : array();
    $addgroups = isset($_POST['groups']) ? $_POST['groups'] : array();
    $yesno = array('Not Allowed', 'Allowed');

    // GROUPS
    echo "<p>The following usergroups (and users) gained access to the project.</p>\n";
    echo "<p><table cellspacing=0>\n";
    echo "<th class=top>User</th>\n";
    echo "<th class=top>Edit/Delete Panel</th>\n";
    echo "<th class=top>Share Panel</th>\n";
    echo "</tr>\n";
    $printed = 0;
    foreach ($addgroups as $gid) {
        $printed++;
        $editrw = $_POST["g_rw_$gid"];
        $editshare = $_POST["g_share_$gid"];
        // user info
        $row = runQuery("SELECT name FROM `Usergroups` WHERE id = '$gid'", "Usergroups")[0];
        $name = $row['name'];
        // print table row
        echo "<tr>\n";
        echo "<td>$name</td>\n";
        echo "<td>" . $yesno[$editrw] . "</td>\n";
        echo "<td>" . $yesno[$editshare] . "</td>\n";
        echo "</tr>\n";
        // INSERT or UPDATE permissions
        doQuery("INSERT INTO `GenePanels_x_Usergroups` (gpid, gid, rw, share,sharedby) VALUES ('$gpid','$gid', '$editrw', '$editshare','$userid') ", "GenePanels_x_Usergroups");
        // users in the group	
        $rows = runQuery("SELECT uid FROM `Usergroups_x_User` WHERE gid = '$gid'", "Usergroups_x_User");
        foreach ($rows as $k => $row) {
            $uid = $row['uid'];
            // check if has access 
            $arow = array_shift(...[runQuery("SELECT rw,share FROM `GenePanels_x_Users` WHERE gpid = '$gpid' AND uid = '$uid'", "GenePanels_x_Users")]);
            if (!$arow || count($arow) == 0) {
                doQuery("INSERT INTO `GenePanels_x_Users` (gpid, uid, rw, share) VALUES ('$gpid','$uid', '$editrw', '$editshare')", "GenePanels_x_Users");
                // notify the user (to Inbox)
                $subject = "New GenePanel made available ($gname)";
                doQuery("INSERT INTO `Inbox` (Inbox.from, Inbox.to, Inbox.subject, Inbox.body, Inbox.type, Inbox.values, Inbox.date) VALUES ('$userid','$uid','$subject','','shared_genepanel','$gpid',NOW())");
            } else {
                $rw = max($editrw, $arow['rw']);
                $share = max($editshare, $arow['share']);
                doQuery("UPDATE `GenePanels_x_Users` SET rw = '$rw', share = '$share' WHERE gpid = '$gpid' AND uid = '$uid'", "GenePanels_x_Users");
                error_log("UPDATE `GenePanels_x_Users` SET rw = '$rw', share = '$share' WHERE gpid = '$gpid' AND uid = '$uid'");
            }
        }
    }
    if ($printed == 0) {
        echo "<tr><td colspan=3>None.</td></tr>";
    }
    echo "</table></p>";


    // USERS
    // print table
    echo "<p>The following users gained access to the project.</p>\n";
    echo "<p><table cellspacing=0>\n";
    echo "<th class=top>User</th>\n";
    echo "<th class=top>Edit/Delete Panel</th>\n";
    echo "<th class=top>Share Panel</th>\n";
    echo "</tr>\n";
    $printed = 0;
    foreach ($addusers as $uid) {
        $printed++;
        $editrw = $_POST["rw_$uid"];
        $editshare = $_POST["share_$uid"];
        // user info
        $row = runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '$uid'", "Users");
        $fname = $row['FirstName'];
        $lname = $row['LastName'];
        // print table row
        echo "<tr>\n";
        echo "<td>$lname $fname</td>\n";
        echo "<td>" . $yesno[$editrw] . "</td>\n";
        echo "<td>" . $yesno[$editshare] . "</td>\n";
        echo "</tr>\n";
        // INSERT or UPDATE permissions
        doQuery("INSERT INTO `GenePanels_x_Users` (gpid, uid, rw, share) VALUES ('$gpid','$uid', '$editrw', '$editshare') ON DUPLICATE KEY UPDATE rw = if(VALUES(rw) < '$editrw', VALUES(rw),'$editrw'), share = if(VALUES(share) < '$editshare', VALUES(share),'$editshare')", "GenePanels_x_Users");
        // notify the user (to Inbox)
        $subject = "New GenePanel made available ($pname)";
        doQuery("INSERT INTO `Inbox` (Inbox.from, Inbox.to, Inbox.subject, Inbox.body, Inbox.type, Inbox.values, Inbox.date) VALUES ('$userid','$uid','$subject','','shared_panel','$gpid',NOW())", "Inbox");
    }
    if ($printed == 0) {
        print "<tr><td colspan=3>None.</td></tr>";
    }
    echo "</table></P>";
    clearMemcache("GenePanels_x_Users:Inbox:GenePanels_x_Usergroups");
    echo "<p><a href='index.php?page=genepanels&t=manage&a=s&p=$gpid'>Go Back</a></p>\n";
    echo "</div>\n";
}
