<div class=section>
<h3>Documentation : Getting Started </h3>
<h4>0. Register a user / log in</h4>
<p class=indent>Go to <a href='index.php?page=login'>the login page</a> in the top right corner:
	<ol>
		<li>Register your own username using <a href='http://devbox/variantdb/index.php?page=request_user'> this </a>form</li>
		<li>Or: Try out VariantDB on our demonstration server <a href='http://ngs.ua.ac.be/variantdb'>here</a></li>
	</ol>

<h4>1. Selecting a sample</h4>
<p class=indent>Go to <a href='index.php?page=variants'>Variant Filter</a> in the top menu. You have two options top select a sample:
	<ol>
		<li>Select sample from list: This shows all your samples, organised by project. Select the sample you need.</li>
		<li>Type samplename : If you know the sample you need, start typing the name. Matching names will be shown as a dropdown list.</li>
	</ol></p>
<p class=indent>After selecting the sample, a link to load the VCF/BAM data (if available) into IGV will be shown on the right.</p>

<h4>2. Setting Filters</h4>
<p class=indent>Select the filters to apply on the variants of the sample from the categories in the 'filter' tab. Click the plus signs to add filters, remove them using the minus sign.</p>
<p class=indent>An overview of the available filters is available <a href='index.php?page=tutorial&topic=filters'>here</a>. A set of filters can be saved using the options on the right. 

<h4>3. Setting Annotations</h4>
<p class=indent>The second tab shows all available annotations. Select them using the checkboxes. An overview of the annotations is available <a href='index.php?page=tutorial&topic=anno'>here</a></p>


<h4>4. Apply the filter and get annotations</h4>
<p class=indent>Click the 'execute' button to get the results. Results are presented in batches of 100 matching variants. On the 'export' tab, the button will start the generation and download of a csv file with all matching variants. </p>
	

</div>
