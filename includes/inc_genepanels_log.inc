<?php

if (!isset($_GET['gpid']) || $_GET['gpid'] == '' || !is_numeric($_GET['gpid'])) {
    echo "<p>Error : invalid genepanel provided";
    exit;
}
$gpid = $_GET['gpid'];

//allowed?
$rows = runQuery("SELECT rw FROM `GenePanels_x_Users` WHERE gpid = '$gpid' AND uid = '$userid'", "GenePanels_x_Users");
if (count($rows) == 0) {
    echo "<div class=section>";
    echo "<h3>Insufficient Privileges</h3>";
    echo "<p>You are not allowed to access this panel.</p>";
    echo "</div>";
    exit;
}

// info
$row = runQuery("SELECT Name FROM GenePanels WHERE id = '$gpid'")[0];
$gp_name = $row['Name'];

echo "<div class=section>";
echo "<h3> GenePanel Log for : $gp_name</h3>";

$entries = runQuery("SELECT u.LastName, u.FirstName, gl.message, gl.date FROM Users u JOIN GenePanels_Log gl ON gl.uid = u.id WHERE gl.gpid = '$gpid' ORDER BY gl.id DESC");
if (count($entries) == 0) {
    echo "<p>No log entries for this panel</p>";
    exit;
}
echo "<p>";
foreach ($entries as $entry) {
    echo "<span class=italic>" . $entry['date'] . "</span> : " . $entry['LastName'] . " " . substr($entry['FirstName'], 0, 1) . '. : ' . $entry['message'] . "<br/>";
}
echo "</br>";




echo "</div>";
