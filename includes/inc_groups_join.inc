<?php

//get posted vars
$motivation = (isset($_POST['motivation'])) ? $_POST['motivation'] : '';

// get users institution
$row = runQuery("SELECT Affiliation FROM Users WHERE id = '$userid'", "Users")[0];
$myaffi = $row['Affiliation'];

if (isset($_POST['submit'])) {
    $joinid = $_POST['joinid'];
} elseif (isset($_GET['jid'])) {
    $joinid = $_GET['jid'];
} else {
    $joinid = 'none';
}

if (!isset($_POST['submit'])  || $joinid == 'none') {
    echo "<div class=section>\n";
    echo "<h3>Join a Usergroup</h3>";
    echo "<p>Please select a group to join from the list below. You can add a motivation if needed in the textfield. If confirmation is needed, an email will be sent to the usergroup administrator upon submission.</p>\n";
} else {
    // process request
    $row = runQuery("SELECT affiliation, confirmation, administrator,name,editvariant, editclinic,editsample,opengroup FROM `Usergroups` WHERE id = '$joinid'", "Usergroups")[0];
    $allowedaffis = $row['affiliation'];
    $confirmation = $row['confirmation'];
    $administrator = $row['administrator'];
    $editvariant = $row['editvariant'];
    $editclinic = $row['editclinic'];
    $editsample = $row['editsample'];
    $name = $row['name'];
    $opengroup = $row['opengroup'];
    $affis = explode(',', $allowedaffis);
    $found = 0;
    if ($opengroup == 1) {
        $found = 1;
    } else {
        foreach ($affis as $value) {
            if ($value == $myaffi) {
                $found = 1;
                break;
            }
        }
    }
    if ($found == 0) {
        echo "<div class=section>\n";
        echo "<h3>Problem detected</h3>\n";
        echo "<p>Somehow, you don't seem to be allowed to join this group. please select another one.</p>\n";
        echo "</div>\n";
    } elseif ($confirmation == 0) {
        // join the group.
        insertQuery("INSERT INTO `Usergroups_x_User` (uid, gid) VALUES ('$userid','$joinid') ON DUPLICATE KEY UPDATE uid = '$userid'", "Usergroups_x_User");
        // update permissions on projects shared with groups. 
        $rows = runQuery("SELECT pid FROM `Projects_x_Usergroups` WHERE gid = '$joinid'", "Projects_x_Usergroups");
        $projects = array();
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $pid = $row['pid'];
                $projects[$pid] = 1;
                $insquery = insertQuery("INSERT INTO `Projects_x_Users` (pid, uid, editvariant, editclinic,editsample) VALUES ('$pid','$userid', '$editvariant', '$editclinic','$editsample') ON DUPLICATE KEY UPDATE editvariant = if(VALUES(editvariant) < '$editvariant', VALUES(editvariant),'$editvariant'), editclinic = if(VALUES(editclinic) < '$editclinic', VALUES(editclinic),'$editclinic'),editsample = if(VALUES(editsample) < '$editsample', VALUES(editsample),'$editsample')", "Projects_x_Users");
            }
            doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$userid'", "Users");
        }

        echo "<div class=section>\n";

        echo "<h3>Request processed</h3>\n";
        echo "<p>You are now a member of the group '$name'. When new projects are shared with this group, you will gain access to them.</p>\n";

        if (count($projects) > 0) {
            echo "<p>In addition, you gained access to the following previously shared projects: </p>\n";
            echo "<table cellspacing=0>\n";
            echo "<tr><th>Name</th><th>Nr.Samples</th></tr>\n";
            foreach ($projects as $pid => $value) {
                $row = runQuery("SELECT p.Name,COUNT(ps.sid) AS NrSamples FROM `Projects` p JOIN `Projects_x_Samples` ps ON p.id = ps.pid WHERE p.id = '$pid'", "Projects:Projects_x_Samples")[0];
                $pname = $row['Name'];
                $nrS = $row['NrSamples'];
                echo "<tr>\n";
                echo "<td >$pname</td>\n";
                echo "<td>$nrS</td>\n";
                echo "</tr>\n";
            }
            echo "</table>\n";
        }

        echo "</div>\n";
    } else {
        // check if request is still open. 
        $ok = 1;
        $rows = runQuery("SELECT linkkey FROM `Usergroups_requests` WHERE uid = '$userid' AND gid = '$joinid'", "Usergroups_requests");
        if (count($rows) > 0 && $_POST['resend'] != 1) {
            echo "<div class=section>\n";
            echo "<h3>Problem Detected</h3>\n";
            echo "<p>You already requested access to this usergroup. If you want to resend the email to the system admininstrator, click continue.</p>\n";
            echo "<form action='index.php?page=group&type=join' method=POST>\n";
            echo "<input type=hidden name=motivation value='$motivation'>\n";
            echo "<input type=hidden name=joinid value='$joinid'>\n";
            echo "<input type=hidden name=resend value=1>\n";
            echo "<input type=submit class=button value='Resend Request' name=submit>\n";
            echo "</form></p></div>\n";
            $ok = 0;
        }
        if ($ok == 1 || ($ok == 0 && $_POST['resend'] == 1)) {
            // send email to administrator with join request
            function random_gen($length)
            {
                $random = "";
                srand((float)microtime() * 1000000);
                $char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $char_list .= "abcdefghijklmnopqrstuvwxyz";
                $char_list .= "1234567890";
                // Add the special characters to $char_list if needed
                for ($i = 0; $i < $length; $i++) {
                    $random .= substr($char_list, (rand() % (strlen($char_list))), 1);
                }
                return $random;
            }

            // step 1: get request user details
            $row = runQuery("SELECT u.LastName, u.FirstName, a.name, u.email FROM Users u JOIN Affiliation a ON u.Affiliation = a.id WHERE u.id = '$userid'", "Users:Affiliation")[0];
            $reqfname = $row['FirstName'];
            $reqlname = $row['LastName'];
            $reqinstitute = $row['name'];
            $reqemail = $row['email'];
            // step 2: get group admin user details. 
            $row = runQuery("SELECT u.LastName, u.FirstName, u.email FROM Users u JOIN Affiliation a ON u.Affiliation = a.id WHERE u.id = '$administrator'", "Users:Affiliation")[0];
            $admfname = $row['FirstName'];
            $admlname = $row['LastName'];
            $admemail = $row['email'];
            // step 3: compose message, etc
            $linkkey = random_gen(20);
            $link = "https://$domain/$basepath/index.php?page=usergrouprequest&key=$linkkey";
            $message = "Message sent from https://$domain\r";
            $message .= "Sent by: $reqemail\r";
            $message .= "Subject: CNV-analysis Page: Usergroup Request\r\r";
            $message .= "$reqfname $reqlname requested to join the '$name' usergroup at VariantDB .\r\r";
            if ($motivation != '') {
                $message .= "The following motivation was included with the request:\r$motivation\r\r";
            }
            $message .= "You can approve or deny this request at the following page (after logging in):\r";
            $message .= "$link\r";
            $message .= "\r\rYour VariantDB Administrator\r\n";
            $headers = "From: no-reply@$domain\r\n";
            $headers .= "BCC: $admemail, $reqemail\r\n";
            /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
            // step 4: send the email
            if (mail($email, "VariantDB: Usergroup Request", $message, $headers)) {
                // message sent, now store request. 
                $ins_id = insertQuery("INSERT INTO `Usergroups_requests` (uid, gid, linkkey,motivation) VALUES ('$userid', '$joinid','$linkkey','$motivation') ON DUPLICATE KEY UPDATE linkkey = '$linkkey', motivation = '$motivation'", "Usergroups_requests");
                echo "<div class=section>\n";
                echo "<h3>Usergroup Request Sent To Admin</h3>\n";
                echo "<p>An email has been sent to the usergroup administrator ($admfname $admlname) to approve your request. The email has also been sent your own specified address for reference. You will be notified when the request has been approved.</p>";
                echo " </div>\n";
            } else {
                echo "<div class=section>\n";
                echo "<h3>Can't send email</h3>\n";
                echo "<p>I'm sorry but we couldn't send the confirmation email to the usergroup administrator.  Please report this problem to us by the contact form.</p>\n";
                echo "</div>\n";
            }
        }
    }
    $joinid = 'none';
    // print form to join another group	
    echo "<div class=section>\n";
    echo "<h3>Join Another Usergroup</h3>\n";
    echo "<p>Please select a group to join from the list below. You can add a motivation if needed in the textfield. If confirmation is needed, an email will be sent to the usergroup administrator upon submission.</p>\n";
}


// print the form
echo "<p><form action='index.php?page=group&type=join' method=POST>\n";
echo "<table>\n";
echo "<tr>\n";
echo "<th >Select Group</th>\n";
echo "<td ><select id='JoinSelect' name=joinid onchange=\"LoadGroupInfo('$userid')\">";
if ($joinid == 'none') {
    echo "<option value=none selected></option>\n";
} else {
    echo "<option value=none></option>\n";
}
// non-public groups
$any_found = 0;
$rows = runQuery("SELECT ug.id, ug.name, ug.description, ug.affiliation FROM `Usergroups` ug WHERE ug.opengroup =0 AND ug.id NOT IN ( SELECT ugu.gid FROM `Usergroups_x_User` ugu WHERE ugu.uid = '$userid')", "Usergroups_x_User:Usergroups");
if (count($rows) > 0) {
    echo "<OPTGROUP label='Private Groups'>\n";
    foreach ($rows as $row) {
        $uga = $row['affiliation'];
        if (!preg_match("/(^$myaffi,)|(,$myaffi,)|(,$myaffi$)|(^$myaffi$)/", $uga)) {
            #echo "$myaffi not found in $uga<br>";
            continue;
        }
        $ugid = $row['id'];
        $ugname = $row['name'];
        $ugdescr = $row['description'];
        if ($ugid == $joinid) {
            echo "<option value='$ugid' title='$ugdescr' SELECTED>$ugname</option>\n";
        } else {
            echo "<option value='$ugid' title='$ugdescr'>$ugname</option>\n";
        }
        $any_found = 1;
    }
}
// public groups
$rows = runQuery("SELECT ug.id, ug.name, ug.description FROM `Usergroups` ug WHERE ug.opengroup = 1 AND ug.id NOT IN ( SELECT ugu.gid FROM `Usergroups_x_User` ugu WHERE ugu.uid = '$userid')", "Usergroups:Usergroups_x_User");
if (count($rows) > 0) {
    echo "<OPTGROUP label='Public Groups'>\n";
    foreach ($rows as $row) {
        $ugid = $row['id'];
        $ugname = $row['name'];
        $ugdescr = $row['description'];
        if ($ugid == $joinid) {
            echo "<option value='$ugid' title='$ugdescr' SELECTED>$ugname</option>\n";
        } else {
            echo "<option value='$ugid' title='$ugdescr'>$ugname</option>\n";
        }
        $any_found = 1;
    }
}
if ($any_found == 0) {
    echo "<option value='-' disabled >No Groups Available</option>";
}

echo "</select></td>\n";
echo "</tr>\n";
if ($any_found == 1) {
    echo "<tr>\n";
    echo "<th label='Short Motivation, if needed'>Motivation</td>\n";
    echo "<td ><input type=text name=motivation size=40 value='$motivation'></td>\n";
    echo "</tr>\n";
}
echo "</table></p>\n";
echo "<div id='GroupDetails'>";

echo "</div>";

echo "<p>\n";
if ($any_found == 1) {
    echo "<input type=submit name=submit class=button value='Join'>\n";
}
echo "</form>\n";
echo "</div>\n";
