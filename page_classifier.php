<?php
// check login
if (!isset($loggedin) || $loggedin != 1) {
	include('page_login.php');
	exit;
}

//if (isset($_GET['t']) && $_GET['t'] == 'show') {
//	include("includes/inc_classifier_".$_GET['t'].".inc");
//	exit;
//}
if (!isset($_GET['t'])) {
	$_GET['t'] = 'run';
}
if (!file_exists("includes/inc_classifier_".$_GET['t'].".inc")) {
	include('page_404.php');
	exit;
}
else {
	include("includes/inc_classifier_".$_GET['t'].".inc");
}
