from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import get_logger

from dataclasses import dataclass, field
from typing import Dict, Any, Optional, List


# error classes
class DataError(OSError):
    pass


class ValueError(OSError):
    pass


log = get_logger()


@dataclass
class Filter:
    # arguments : sample, filter-idx, .credentials , loaded_filters, type (sample/region/project)
    sid: int
    f_idx: str
    config: object
    filters: object
    filter_type: str
    build: str = None
    upstream_table: str = None
    # output :
    comments: list = field(default_factory=list)
    # internal variables
    upstream_where: str = ""

    # run filter on creation.
    def __post_init__(self):
        # not allowed for project/region filters.
        if self.filter_type != "sample":
            log.warning("De Novo filter is only valid in sample-based queries. Skipping.")
            self.comments.append(
                "WARNING : De Novo filter is only valid in sample-based queries. Skipping."
            )
            return self
        # extract values of this filter:
        self.values = self.filters["dynamic"][self.f_idx]["values"]
        # get DB connection.
        self._GetDBConnection()
        # load mapping of criteria
        self.mapping = self._LoadMapping()
        # determine upstream table
        self._setUpstream()
        # make tmp table
        self._makeCountsTable()
        # run filters.
        self._RunFilters()
        # prune variants below threshold
        self._ApplyThreshold()
        # set remaining variants as results
        self.results_table = self.tmp_table

    def _setUpstream(self):
        # not set : use v_x_s + sid criterium
        if not self.upstream_table:
            self.upstream_table = "Variants_x_Samples"
            self.upstream_where = " AND `Variants_x_Samples`.sid = '{}'".format(self.sid)
        log.debug("Upstream table : {} : {}".format(self.upstream_table, self.upstream_where))

    def _makeCountsTable(self):
        ## make a table to hold counts:
        self.tmp_table = "{}-{}-path_count".format(self.config["RUNTIME"]["tmp_prefix"], self.f_idx)
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY, `counter` INT(2) DEFAULT 1 ) ENGINE=MEMORY CHARSET=latin1".format(
                    self.tmp_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(self.tmp_table))
        except Exception as e:
            raise e
        log.debug("Counts table : {}".format(self.tmp_table))

    def _RunFilters(self):
        ## for each select tool : add variants to the tmp-table, incrementing count.
        tools = (
            self.values["Select"].replace("__", "@@@").replace(" ", "").rstrip("@@@").split("@@@")
        )
        # filter_settings = self.filters["phase1"][f_idx]
        # filter_root = self.filters["phase1"][self.f_idx]["config"]
        for tool in tools:
            table = self.mapping["{}_table".format(tool)]
            column = self.mapping["{}_col".format(tool)]
            criterium = self.mapping["{}_crit".format(tool)]
            # where = filter_root.find("options/Select/select_option/{}".format(tool)).text
            query = "SELECT HIGH_PRIORITY `{}`.vid FROM `{}` JOIN `{}` ON `{}`.vid = `{}`.vid WHERE `{}`.`{}` {} {}".format(
                table,
                table,
                self.upstream_table,
                table,
                self.upstream_table,
                table,
                column,
                criterium,
                self.upstream_where,
            )
            try:
                ins_query = "INSERT INTO `{}` (`vid`) {} ON DUPLICATE KEY UPDATE `counter` = `counter` + 1".format(
                    self.tmp_table, query
                )
                self.dbh.doQuery(ins_query)

            except Exception as e:
                print(
                    "INSERT INTO `{}` (`vid`) {} ON DUPLICATE KEY UPDATE `counter` = `counter` + 1".format(
                        self.tmp_table, query
                    )
                )
                raise (e)

    def _ApplyThreshold(self):
        minThresh = self.values["Min_Patho"]
        try:
            self.dbh.doQuery(
                "DELETE FROM `{}` WHERE `counter` < '{}'".format(self.tmp_table, minThresh)
            )
        except Exception as e:
            raise (e)

    def _LoadMapping(self):
        mapping = {
            "Variants_x_ANNOVAR_revel_lenient_table": "Variants_x_ANNOVAR_revel",
            "Variants_x_ANNOVAR_revel_stringent_table": "Variants_x_ANNOVAR_revel",
            "Variants_x_CADD_v1.4_table": "Variants_x_CADD_v1.4",
            "Variants_x_ANNOVAR_ljb26_lrt_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_mt_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_pp2hdiv_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_pp2hvar_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_sift_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_ma_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_fathmm_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_radialsvm_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_lr_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_phylop46_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_phylop100_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_cadd_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_gerp_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_vest3_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_ljb26_siphy_table": "Variants_x_ANNOVAR_ljb26_all",
            "Variants_x_ANNOVAR_dbnsfp30a_lrt_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_mt_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_pp2hdiv_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_pp2hvar_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_sift_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_ma_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_provean_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_fathmm_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_metasvm_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_metalr_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_vest3_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_cadd_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_dbnsfp30a_gerp_table": "Variants_x_ANNOVAR_dbnsfp30a",
            "Variants_x_ANNOVAR_CADD_table": "Variants_x_ANNOVAR_CADD",
            "Variants_x_SIFT_sift_table": "Variants_x_SIFT",
            "Variants_x_SIFT_provean_table": "Variants_x_SIFT",
            "Variants_x_ANNOVAR_revel_lenient_col": "Score",
            "Variants_x_ANNOVAR_revel_stringent_col": "Score",
            "Variants_x_CADD_v1.4_col": "Phred_Score",
            "Variants_x_ANNOVAR_ljb26_lrt_col": "LRT_pred",
            "Variants_x_ANNOVAR_ljb26_mt_col": "MutationTaster_pred",
            "Variants_x_ANNOVAR_ljb26_pp2hdiv_col": "Polyphen2_HDIV_pred",
            "Variants_x_ANNOVAR_ljb26_pp2hvar_col": "Polyphen2_HVAR_pred",
            "Variants_x_ANNOVAR_ljb26_sift_col": "SIFT_pred",
            "Variants_x_ANNOVAR_ljb26_ma_col": "MutationAssessor_pred",
            "Variants_x_ANNOVAR_ljb26_fathmm_col": "FATHMM_pred",
            "Variants_x_ANNOVAR_ljb26_radialsvm_col": "RadialSVM_pred",
            "Variants_x_ANNOVAR_ljb26_lr_col": "LR_pred",
            "Variants_x_ANNOVAR_ljb26_phylop46_col": "phyloP46way_placental",
            "Variants_x_ANNOVAR_ljb26_phylop100_col": "phyloP100way_vertebrate",
            "Variants_x_ANNOVAR_ljb26_cadd_col": "CADD_phred",
            "Variants_x_ANNOVAR_ljb26_gerp_col": "GERP_RS",
            "Variants_x_ANNOVAR_ljb26_vest3_col": "VEST3_score",
            "Variants_x_ANNOVAR_ljb26_siphy_col": "SiPhy_29way_logOdds",
            "Variants_x_ANNOVAR_dbnsfp30a_lrt_col": "LRT_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_mt_col": "MutationTaster_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_pp2hdiv_col": "Polyphen2_HDIV_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_pp2hvar_col": "Polyphen2_HVAR_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_sift_col": "SIFT_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_ma_col": "MutationAssessor_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_provean_col": "PROVEAN_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_fathmm_col": "FATHMM_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_metasvm_col": "MetaSVM_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_metalr_col": "MetaLR_pred",
            "Variants_x_ANNOVAR_dbnsfp30a_vest3_col": "VEST3_score",
            "Variants_x_ANNOVAR_dbnsfp30a_cadd_col": "CADD_phred",
            "Variants_x_ANNOVAR_dbnsfp30a_gerp_col": "GERP_RS",
            "Variants_x_ANNOVAR_CADD_col": "Phred_Score",
            "Variants_x_SIFT_sift_col": "SIFTEffect",
            "Variants_x_SIFT_provean_col": "PROVEANEffect",
            "Variants_x_ANNOVAR_revel_lenient_crit": "> 0.5",
            "Variants_x_ANNOVAR_revel_stringent_crit": ">= 0.70",
            "Variants_x_CADD_v1.4_crit": "> 20",
            "Variants_x_ANNOVAR_ljb26_lrt_crit": "= 'D'",
            "Variants_x_ANNOVAR_ljb26_mt_crit": "IN ('A','D')",
            "Variants_x_ANNOVAR_ljb26_pp2hdiv_crit": "= 'D'",
            "Variants_x_ANNOVAR_ljb26_pp2hvar_crit": "= 'D'",
            "Variants_x_ANNOVAR_ljb26_sift_crit": "='D'",
            "Variants_x_ANNOVAR_ljb26_ma_crit": "IN ('H','M')",
            "Variants_x_ANNOVAR_ljb26_fathmm_crit": "= 'D'",
            "Variants_x_ANNOVAR_ljb26_radialsvm_crit": "= 'D'",
            "Variants_x_ANNOVAR_ljb26_lr_crit": "= 'D'",
            "Variants_x_ANNOVAR_ljb26_phylop46_crit": "> 1.6",
            "Variants_x_ANNOVAR_ljb26_phylop100_crit": "> 1.6",
            "Variants_x_ANNOVAR_ljb26_cadd_crit": "> 20",
            "Variants_x_ANNOVAR_ljb26_gerp_crit": "> 4.4",
            "Variants_x_ANNOVAR_ljb26_vest3_crit": "> 0.65",
            "Variants_x_ANNOVAR_ljb26_siphy_crit": "> 12.17",
            "Variants_x_ANNOVAR_dbnsfp30a_lrt_crit": "= 'D'",
            "Variants_x_ANNOVAR_dbnsfp30a_mt_crit": "IN ('A','D')",
            "Variants_x_ANNOVAR_dbnsfp30a_pp2hdiv_crit": "= 'D'",
            "Variants_x_ANNOVAR_dbnsfp30a_pp2hvar_crit": "= 'D'",
            "Variants_x_ANNOVAR_dbnsfp30a_sift_crit": "= 'D'",
            "Variants_x_ANNOVAR_dbnsfp30a_ma_crit": "IN ('H','M')",
            "Variants_x_ANNOVAR_dbnsfp30a_provean_crit": "= 'D'",
            "Variants_x_ANNOVAR_dbnsfp30a_fathmm_crit": "= 'D'",
            "Variants_x_ANNOVAR_dbnsfp30a_metasvm_crit": "= 'D'",
            "Variants_x_ANNOVAR_dbnsfp30a_metalr_crit": "= 'D'",
            "Variants_x_ANNOVAR_dbnsfp30a_vest3_crit": "> 0.65",
            "Variants_x_ANNOVAR_dbnsfp30a_cadd_crit": "> 20",
            "Variants_x_ANNOVAR_dbnsfp30a_gerp_crit": "> 4.4",
            "Variants_x_ANNOVAR_CADD_crit": "> 20",
            "Variants_x_SIFT_sift_crit": "IN ('D','*')",
            "Variants_x_SIFT_provean_crit": "IN ('D','*')",
        }
        return mapping

    def _GetFinalVariants(self, affected_table, unaffected_table):
        final_table = "{}-{}-path-final-vars".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx
        )
        query = "SELECT HIGH_PRIORITY `{}`.vid FROM `{}` LEFT OUTER JOIN `{}` ON `{}`.vid = `{}`.vid WHERE `{}`.vid IS NULL".format(
            affected_table,
            affected_table,
            unaffected_table,
            affected_table,
            unaffected_table,
            unaffected_table,
        )

        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    final_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(final_table))
            log.debug(query)
            self.dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(final_table, query))
        except Exception as e:
            raise e

        return final_table

    def _IntersectUpstream(self, affected_table):
        if not self.upstream_table:
            log.debug("No upstream table found.")
            return affected_table
        intersected_table = "{}-{}-path-insec-upstr".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx
        )
        query = (
            "SELECT HIGH_PRIORITY `{}`.vid FROM `{}` INNER JOIN `{}` ON `{}`.vid = `{}`.vid".format(
                affected_table,
                affected_table,
                self.upstream_table,
                affected_table,
                self.upstream_table,
            )
        )
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    intersected_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(intersected_table))
            log.debug(query)
            self.dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(intersected_table, query))
        except Exception as e:
            raise e

        return intersected_table

    def _GetDBConnection(self, quiet=True):

        try:
            db_suffix = self.config["DATABASE"].get("DBSUFFIX", "")
            if db_suffix:
                db_suffix = f"_{db_suffix}"
            dbh = MySQL(
                user=self.config["DATABASE"]["DBUSER"],
                password=self.config["DATABASE"]["DBPASS"],
                host=self.config["DATABASE"]["DBHOST"],
                database=f"NGS-Variants-Admin{db_suffix}",
                allow_local_infile=True,
            )
            row = dbh.runQuery("SELECT HIGH_PRIORITY `name`, `StringName` FROM `CurrentBuild`")
            db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
            # use statement doesn't work with placeholder. reason unknown
            dbh.select_db(db)
            if not quiet:
                log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

        except Exception as e:
            raise e
        # switch build?
        # if self.build:
        #    # should exist
        #    try:
        #        dbh.select_db("NGS-Variants-{}".format(self.build))
        #        if not quiet:
        #            log.info("Selected provided build: NGS-Variants-{}".format(self.build))
        #    except MySqlError as e:
        #        raise ValueError("Invalid GenomeBuild '{}' : {}".format(self.build, e))
        self.dbh = dbh
