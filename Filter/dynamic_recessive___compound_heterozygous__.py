from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import get_logger
from CMG.Utils import Re, CheckVariableType

import xml.etree.ElementTree as ET
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Dict, Any, Optional, List
from pprint import pprint
from contextlib import suppress


# error classes
class DataError(OSError):
    pass


class ValueError(OSError):
    pass


log = get_logger()
re = Re()


@dataclass
class Filter:
    # arguments : sample, filter-idx, .credentials , loaded_filters, type (sample/region/project)
    sid: int
    f_idx: str
    config: object
    filters: object
    filter_type: str
    build: str = None
    upstream_table: str = None
    # output :
    comments: list = field(default_factory=list)
    # internal variables
    qc_part: str = ""
    family: dict = field(default_factory=dict)

    # run filter on creation.
    def __post_init__(self):
        # not allowed for project/region filters.
        if self.filter_type != "sample":
            log.warning(
                "Compound Heterozygous filter is only valid in sample-based queries. Skipping."
            )
            self.comments.append(
                "WARNING : Compound Heterozygous filter is only valid in sample-based queries. Skipping."
            )
            return self
        # extract values of this filter:
        self.values = self.filters["dynamic"][self.f_idx]["values"]

        # get DB connection.
        self._GetDBConnection()
        # run.
        self._RunFilter()

    def _RunFilter(self):
        ## PREPARE
        ##########
        # affected Family:
        self.affected_members = self._GetFamily(affected=True)
        if len(self.affected_members) == 0:
            log.warning("No affected family members found. Skipping.")
            self.comments.append("WARNING : No affected family members found. Skipping.")
            return self
        # index validation : not affected : skip.
        if self.sid not in self.affected_members:
            log.warning("Provided index sample is not affected. Skipping.")
            self.comments.append("WARNING : Provided index sample is not affected. Skipping.")
            return self
        # index validation : missing parents : skip.
        if len(self._GetParents(self.sid)) < 2:
            log.warning("Provided index sample is missing parents. Skipping.")
            self.comments.append("WARNING : Provided index sample is is missing parents. Skipping.")
            return self
        # Extract affected members with parents
        self._CheckParents()
        if len(self.affected_with_parents) == 0:
            log.warning(
                "This filter requires both parents for all affected members. Some were missing, filter was skipped"
            )
            self.comments.append(
                "This filter requires both parents for all affected members. Some were missing, filter was skipped"
            )
            return self
        else:
            log.info(
                f"Found {len(self.affected_with_parents)} affected samples with parents to start analysis"
            )
        # load QC criteria?
        self._SetQC()

        # for each affected : get compound het variants
        affected_tables = self._ProcessAffectedWithParents()

        # intersect valid pairs accross affected samples.
        self.results_table = self._IntersectAffected(affected_tables)

        # TODO : for valid pairs : is each pair also present in affecteds without parents ?

    def _ProcessAffectedWithParents(self):
        affected_tables = list()
        for sid in self.affected_with_parents:
            affected_tables.append(self._GetValidPairs(sid))
        return affected_tables

    def _GetValidPairs(self, sid):
        # 1. get heterozygous variants in sample
        het_table = "{}-{}-ch-{}-het".format(self.config["RUNTIME"]["tmp_prefix"], self.f_idx, sid)
        if not self.upstream_table:
            query = "SELECT HIGH_PRIORITY vid FROM `Variants_x_Samples` WHERE AltCount = 1 AND sid = '{}'".format(
                sid
            )
        else:
            query = "SELECT HIGH_PRIORITY `Variants_x_Samples`.vid FROM `Variants_x_Samples` JOIN `{}` ON `Variants_x_Samples`.vid = `{}`.vid WHERE `Variants_x_Samples`.AltCount = 1 AND `Variants_x_Samples`.sid = '{}'".format(
                self.upstream_table, self.upstream_table, sid
            )
        try:
            self.dbh.doQuery(
                "CREATE TABLE  IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    het_table
                )
            )
            self.dbh.doQuery("Truncate TABLE `{}`".format(het_table))
            query = "INSERT IGNORE INTO `{}` (`vid`) {}".format(het_table, query)
            log.debug(query)
            self.dbh.doQuery(query)
            # nr of het variants...
            query = f"SELECT COUNT(1) AS nrVar FROM `{het_table}`"
            nrV = self.dbh.runQuery(query)[0]["nrVar"]
            sname = self.family[int(sid)]["name"]
            log.info(f"Starting from {nrV} heterozygous variants in affected sample {sname}")
        except Exception as e:
            log.error(
                "Could not fill heterozygous variants table {} for sample {} : {}".format(
                    het_table, sid, e
                )
            )
        # 2. get parental genotypes
        parents = self._GetParents(sid)
        if not parents:
            log.warn(f"Problem with parents for {sid}")
        # all variants, need genotype to exclude invalid variants later on.
        query = "SELECT HIGH_PRIORITY `Variants_x_Samples`.`vid`, `Variants_x_Samples`.`sid`, `Variants_x_Samples`.`AltCount` FROM `Variants_x_Samples` JOIN `{}` ON `Variants_x_Samples`.vid = `{}`.vid WHERE `Variants_x_Samples`.sid IN ({}) {}".format(
            het_table, het_table, ",".join(parents), self.qc_part
        )
        par_table = "{}-{}-ch-{}-par".format(self.config["RUNTIME"]["tmp_prefix"], self.f_idx, sid)
        try:
            self.dbh.doQuery(
                "CREATE TABLE  IF NOT EXISTS `{}` (`vid` INT(11), `sid` INT(5), `AltCount` INT(1), PRIMARY KEY (`vid`,`sid`)) ENGINE=MEMORY CHARSET=latin1".format(
                    par_table
                )
            )
            self.dbh.doQuery("Truncate TABLE `{}`".format(par_table))
            query = "INSERT IGNORE INTO `{}` (`vid`, `sid`, `AltCount`) {}".format(par_table, query)
            log.debug(query)
            self.dbh.doQuery(query)
            # nr of par variants...
            query = f"SELECT COUNT(1) AS nrVar FROM `{par_table}`"
            nrV = self.dbh.runQuery(query)[0]["nrVar"]
            log.debug(f"{nrV} heterozygous variants in parental samples")
        except Exception as e:
            log.error(
                "Could not fill parental variants table {} for sample {} : {}".format(
                    het_table, sid, e
                )
            )
        # 3. if exlclude non-covered : use gVCF as well : todo

        # 4. get affected genes & transcripts
        query = "SELECT HIGH_PRIORITY `Variants_x_ANNOVAR_ncbigene`.`vid`, `Variants_x_ANNOVAR_ncbigene`.`GeneSymbol`, `Variants_x_ANNOVAR_ncbigene`.`Transcript` FROM `Variants_x_ANNOVAR_ncbigene` JOIN `{}` ON `Variants_x_ANNOVAR_ncbigene`.`vid` = `{}`.vid".format(
            het_table, het_table
        )
        gene_table = "{}-{}-ch-{}-genes".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx, sid
        )
        try:
            self.dbh.doQuery(
                "CREATE TABLE  IF NOT EXISTS `{}` (`vid` INT(11), `GeneSymbol` VARCHAR(25), `Transcript` VARCHAR(25),  PRIMARY KEY (`vid`,`GeneSymbol`, `Transcript`)) ENGINE=MEMORY CHARSET=latin1".format(
                    gene_table
                )
            )
            self.dbh.doQuery("Truncate TABLE `{}`".format(gene_table))
            query = "INSERT IGNORE INTO `{}` (`vid`, `GeneSymbol`, `Transcript`) {}".format(
                gene_table, query
            )
            log.debug(query)
            self.dbh.doQuery(query)
            # delete intergenic:
            self.dbh.doQuery(
                "DELETE FROM `{}` WHERE `GeneSymbol` IN ('','.') AND `Transcript` IN ('','.')".format(
                    gene_table
                )
            )
            # nr of het variants...
            query = f"SELECT COUNT(1) AS nrVar FROM `{gene_table}`"
            nrV = self.dbh.runQuery(query)[0]["nrVar"]
            log.debug(f"{nrV} entries in gene table")
        except Exception as e:
            log.error(
                "Could not fill transcript variants table {} for sample {} : {}".format(
                    het_table, sid, e
                )
            )
        # 5. Process by transcript: vids are heterozygous calls in affected, grouped by transcript.
        query = "SELECT HIGH_PRIORITY GROUP_CONCAT(`vid`),Transcript FROM `{}` WHERE 1 GROUP BY GeneSymbol, Transcript".format(
            gene_table
        )
        valids_table = "{}-{}-ch-{}-valid".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx, sid
        )
        try:
            self.dbh.doQuery(
                "CREATE TABLE  IF NOT EXISTS `{}` (`vid-pair` VARCHAR(25),  PRIMARY KEY (`vid-pair`)) ENGINE=MEMORY CHARSET=latin1".format(
                    valids_table
                )
            )
            self.dbh.doQuery("Truncate TABLE `{}`".format(valids_table))

        except Exception as e:
            log.error(
                "Could not create valid-pairs table {} for sample {} : {}".format(
                    valids_table, sid, e
                )
            )
        log.debug("Evaluate the pairs.")
        par_1 = int(parents[0])
        par_2 = int(parents[1])
        outer_dbh = self._GetDBConnection(returnHandler=True)
        rows = outer_dbh.runQuery(query, size=5000, as_dict=False)
        nrR = len(rows)
        log.debug(f"Evaluating {nrR} potential sets")
        row_idx = 0
        while len(rows) > 0:
            valids = set()
            for row in rows:
                row_idx += 1
                if row[0] == "":
                    log.debug("Empty results...")
                    continue
                vids = [int(x) for x in row[0].split(",") if not x == ""]
                vids.sort()
                # need at least two variants / transcript for a valid pair.
                if len(vids) < 2:
                    continue
                log.debug(f"Evaluate row : {row_idx} : {row}")
                # first evaluation against parents : remove homozygous variants
                try:
                    # rejoin the vids, after filtering the empty values.
                    prws = self.dbh.runQuery(
                        "SELECT HIGH_PRIORITY `vid`, `sid`, `AltCount` FROM `{}` WHERE `vid` IN ({})".format(
                            par_table, ",".join([str(x) for x in vids])
                        )
                    )
                except Exception as e:
                    log.error("Could not fetch parental genotypes : {}".format(e))
                    raise e
                p_genotypes = dict()
                for pr in prws:
                    # invalid : remove from vids
                    if pr["AltCount"] == 2:
                        if pr["vid"] in vids:
                            log.debug(f"vid {pr['vid']} homAlt in parent {pr['sid']}")
                            vids.remove(pr["vid"])
                        continue
                    # track
                    if pr["vid"] not in p_genotypes:
                        p_genotypes[pr["vid"]] = {}
                    p_genotypes[pr["vid"]][pr["sid"]] = pr["AltCount"]
                log.debug("Vids after removing parental homs: ")
                log.debug(vids)
                # evaluate again : remove variants present in both parents as het
                for vid in vids:
                    try:
                        if p_genotypes[vid][par_1] == 1 and p_genotypes[vid][par_2] == 1:
                            log.debug(f"vid {vid} is het in both parents")
                            vids.remove(vid)
                    except KeyError as e:
                        pass
                # recheck : need at least two variants / transcript for a valid pair.
                log.debug("Vids after removing biparental hets")
                log.debug(vids)
                if len(vids) < 2:
                    continue

                # logic :
                # 	- two variants can not be heterozygous in a single parent.
                # 	- a variant cannot be heterozygous in both parents (one parent will be het.zyg for the second variant as well).
                # 	- two variants must originate from different parents (if available)
                # 	- variants must be called heterozygous in parents, or missing (no homref calls).
                # 	- more than two variants can be present, check pairwise.
                for i in range(0, (len(vids) - 1)):
                    # shortcuts for easy access
                    vid_1 = vids[i]
                    log.debug(f" - vid1 : {vid_1}")
                    # both heterozygous : skip (tested above)
                    # try:
                    #    if p_genotypes[vid_1][par_1] == 1 and p_genotypes[vid_1][par_2] == 1:
                    #        continue
                    # except KeyError:
                    #    # ok if not present in either parent
                    #    pass
                    # loop the other variants
                    for j in range(i + 1, len(vids)):
                        vid_2 = vids[j]
                        log.debug(f"    - vid2 {vid_2}")
                        log.debug(p_genotypes.get(vid_1, "nan"))
                        log.debug(p_genotypes.get(vid_2, "nan"))
                        # both heterozygous : skip : tested above...
                        # try:
                        #    if p_genotypes[vid_2][par_1] == 1 and p_genotypes[vid_2][par_2] == 1:
                        #        log.info("     => vid2 het in both parents")
                        #        continue
                        # except KeyError:
                        #    # ok if not present in either parent
                        #    pass
                        # both in single parent?
                        try:
                            if p_genotypes[vid_1][par_1] == 1 and p_genotypes[vid_2][par_1] == 1:
                                log.debug("    => Both present in parent 1")
                                continue
                        except KeyError:
                            # ok if vid not set for parent
                            pass
                        try:
                            if p_genotypes[vid_1][par_2] == 1 and p_genotypes[vid_2][par_2] == 1:
                                log.debug("    => Both present in parent 2")
                                continue
                        except KeyError:
                            # ok if vid not set for parent
                            pass
                        # so : vid_1 and vid_2 originate from different parents, or not called in either.
                        # a. both called
                        try:
                            if p_genotypes[vid_1][par_1] == 1 and p_genotypes[vid_2][par_2] == 1:
                                # valid combo
                                log.debug("   => Valid combo !")
                                valids.add("{}-{}".format(vid_1, vid_2))
                                continue
                        except KeyError:
                            # ok if vid not set for parent
                            pass
                        try:
                            if p_genotypes[vid_1][par_2] == 1 and p_genotypes[vid_2][par_1] == 1:
                                # valid combo
                                log.debug("   => Valid combo !")
                                valids.add("{}-{}".format(vid_1, vid_2))
                                continue
                        except KeyError:
                            # ok if vid not set for parent
                            pass
                        # b. if non-covered positions are not allowed : stop here.
                        if self.values["Exclude_Non_Covered"] == "1":
                            log.info("    => not considering non-covered positions")
                            continue
                        # c. second variant not called in other/either parent.
                        try:
                            if p_genotypes[vid_1][par_1] == 1 and (
                                vid_2 not in p_genotypes or par_2 not in p_genotypes[vid_2]
                            ):
                                # valid
                                log.debug("   => valid non-covered combo")
                                valids.add("{}-{}".format(vid_1, vid_2))
                                continue
                        except KeyError:
                            pass
                        try:
                            if p_genotypes[vid_1][par_2] == 1 and (
                                vid_2 not in p_genotypes or par_1 not in p_genotypes[vid_2]
                            ):
                                # valid
                                log.debug("   => valid non-covered combo")
                                valids.add("{}-{}".format(vid_1, vid_2))
                                continue
                        except KeyError:
                            pass
                        # d. first variant not called in other parent.
                        try:
                            if p_genotypes[vid_2][par_1] == 1 and (
                                vid_1 not in p_genotypes or par_2 not in p_genotypes[vid_1]
                            ):
                                # valid
                                log.debug("   => valid non-covered combo")
                                valids.add("{}-{}".format(vid_1, vid_2))
                                continue
                        except KeyError:
                            pass
                        try:
                            if p_genotypes[vid_2][par_2] == 1 and (
                                vid_1 not in p_genotypes or par_1 not in p_genotypes[vid_1]
                            ):
                                # valid
                                log.debug("   => valid non-covered combo")
                                valids.add("{}-{}".format(vid_1, vid_2))
                                continue
                        except KeyError:
                            pass
                        # e. neither variant is called in parents.
                        if vid_1 not in p_genotypes and vid_2 not in p_genotypes:
                            # valid
                            log.debug("   => valid, not seen at all...")
                            valids.add("{}-{}".format(vid_1, vid_2))
                            continue
                        # remaining cases are invalid:
                        #  - c/d : one heterozygous for var_1, other homRef for var_2
                        #  - e : both variants are called as homRef in both parents.
            log.debug("Valids:")
            log.debug(valids)
            # valids found : add to table
            if len(valids):
                query = "INSERT IGNORE INTO `{}` (`vid-pair`) VALUES ('{}')".format(
                    valids_table, "'),('".join(valids)
                )
                try:
                    self.dbh.doQuery(query)
                except Exception as e:
                    log.error("Could not insert valid pairs : {}".format(e))
            rows = outer_dbh.GetNextBatch()
        return valids_table

    def _IntersectAffected(self, affected_tables):
        # create the table
        joined_table = "{}-{}-ch_final".format(self.config["RUNTIME"]["tmp_prefix"], self.f_idx)
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    joined_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(joined_table))
        except Exception as e:
            raise e
        # construct query:
        query = "SELECT HIGH_PRIORITY `{}`.`vid-pair` FROM `{}`".format(
            affected_tables[0], affected_tables[0]
        )
        join = ""
        on = ""
        for i in range(1, len(affected_tables)):
            join = "{} JOIN `{}`".format(join, affected_tables[i])
            on = "{} AND `{}`.`vid-pair` = `{}`.`vid-pair`".format(
                on, affected_tables[0], affected_tables[i]
            )
        if join:
            query = "{} {} ON {}".format(query, join, on.replace(" AND ", "", 1))
            log.debug(query)
        outer_dbh = self._GetDBConnection(returnHandler=True)
        rows = outer_dbh.runQuery(query, size=5000)
        while len(rows) > 0:
            vids = set()
            for row in rows:
                vids.update(row["vid-pair"].split("-"))
            try:
                print(
                    "INSERT IGNORE INTO `{}` (`vid`) VALUES ({})".format(
                        joined_table, "),(".join(vids)
                    )
                )
                self.dbh.doQuery(
                    "INSERT IGNORE INTO `{}` (`vid`) VALUES ({})".format(
                        joined_table, "),(".join(vids)
                    )
                )
            except Exception as e:
                print(
                    "INSERT IGNORE INTO `{}` (`vid`) VALUES ({})".format(
                        joined_table, "),(".join(vids)
                    )
                )
                log.error("Could not fill joned_table : {}".format(e))
            rows = outer_dbh.GetNextBatch()
        return joined_table

    def _SetQC(self):
        qc_part = ""
        ## APPLY QC filters to parents ?
        if (
            "Apply_QC_to_parents" not in self.values
            or not self.values["Apply_QC_to_parents"] == "1"
        ):
            log.debug("Application of QC-criteria to parental variants is disabled.")
            self.qc_part = qc_part
            return

        for f_idx in self.filters["phase1"]:
            filter_settings = self.filters["phase1"][f_idx]
            filter_root = self.filters["phase1"][f_idx]["config"]
            negate = (
                filter_settings["values"]["negate"] if "negate" in filter_settings["values"] else ""
            )
            args = list(filter(None, filter_settings["values"]["argument"].split("@@@")))

            # arguments taken from query (eg Filter field) : implies multi-select + IN ()
            if ET.iselement(filter_root.find("settings/ArgumentsFromQuery")):
                where = filter_root.find("settings/where").text.replace(
                    r"%replace", "'{}'".format("','".join(args))
                )
                qc_part = "{} AND {} {}".format(qc_part, negate, where)

            # options provided ? <, =, >
            if ET.iselement(filter_root.find("options")):
                # %replace => selected option (first arg element)
                where = filter_root.find("settings/where").text
                where = where.replace(
                    r"%replace", filter_root.find("options/{}".format(args[0])).text
                )
                # %value => provided value
                where = where.replace(r"%value", filter_settings["values"]["value"])

                qc_part = "{} AND {} {}".format(qc_part, negate, where)

        # if TRANCHE qc-items : add GGA tranche.
        qc_part = re.sub(r"Filter IN (", "Filter IN ('GGA',", qc_part)
        # store the result.
        self.qc_part = qc_part

    def _CheckParents(self):
        self.affected_with_parents = []
        for ind in self.affected_members:
            # get parents
            log.debug(f"Checking parents for {ind}")
            p = self._GetParents(ind)
            if len(p) == 2:
                # raise ValueError("Compound Heterozygous filter requires two parents.")
                self.affected_with_parents.append(ind)
            else:
                log.info(f"Individual {ind} doesn't have two parents. excluding from filter")

        return True

    def _GetParents(self, ind):
        # get parents
        rows = self.dbh.runQuery(
            "SELECT HIGH_PRIORITY sid1 FROM `Samples_x_Samples` WHERE sid2 = %s AND Relation = 2",
            ind,
        )
        return [str(x["sid1"]) for x in rows]

    def _GetFamily(self, affected=False):
        # retrieve all family members
        self._GetRelatives(int(self.sid))
        ## extract affected/nonaffected
        if affected:
            return [str(x) for x in self.family if self.family[x]["affected"] == 1]
        else:
            return [str(x) for x in self.family if self.family[x]["affected"] == 0]

    def _GetRelatives(self, sid):
        if sid in self.family:
            return
        self.family[sid] = dict()
        # sample itself.
        rows = self.dbh.runQuery(
            "SELECT HIGH_PRIORITY affected, Name FROM `Samples` WHERE id = %s", sid
        )
        if not rows:
            log.error(f"Sample {sid} not found in Samples table during GetRelatives call")
            return
        self.family[sid]["affected"] = rows[0]["affected"]
        self.family[sid]["name"] = rows[0]["Name"]
        log.info(f"set name of {sid} to {self.family[sid]['name']}")
        # parents
        rows = self.dbh.runQuery(
            "SELECT HIGH_PRIORITY sid1 FROM `Samples_x_Samples` WHERE sid2 = %s AND Relation = 2",
            sid,
        )
        for row in rows:
            self._GetRelatives(int(row["sid1"]))
        # siblings & children
        rows = self.dbh.runQuery(
            "SELECT HIGH_PRIORITY sid2 FROM `Samples_x_Samples` WHERE sid1 = %s AND Relation IN (2,3)",
            sid,
        )
        for row in rows:
            self._GetRelatives(int(row["sid2"]))

    def _GetDBConnection(self, quiet=True, returnHandler=False):
        try:
            db_suffix = self.config["DATABASE"].get("DBSUFFIX", "")
            if db_suffix:
                db_suffix = f"_{db_suffix}"
            dbh = MySQL(
                user=self.config["DATABASE"]["DBUSER"],
                password=self.config["DATABASE"]["DBPASS"],
                host=self.config["DATABASE"]["DBHOST"],
                database=f"NGS-Variants-Admin{db_suffix}",
                allow_local_infile=True,
            )
            row = dbh.runQuery("SELECT HIGH_PRIORITY `name`, `StringName` FROM `CurrentBuild`")
            db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
            # use statement doesn't work with placeholder. reason unknown
            dbh.select_db(db)
            if not quiet:
                log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

        except Exception as e:
            raise e
        # switch build?
        # if self.build:
        #    # should exist
        #    try:
        #        dbh.select_db("NGS-Variants-{}".format(self.build))
        #        if not quiet:
        #            log.info("Selected provided build: NGS-Variants-{}".format(self.build))
        #    except MySqlError as e:
        #        raise ValueError("Invalid GenomeBuild '{}' : {}".format(self.build, e))
        if returnHandler:
            return dbh
        else:
            self.dbh = dbh
