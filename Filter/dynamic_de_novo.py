from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import get_logger
from CMG.Utils import Re
import pprint
import sys
import xml.etree.ElementTree as ET
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Dict, Any, Optional, List

# error classes
class DataError(OSError):
    pass


class ValueError(OSError):
    pass


log = get_logger()

re = Re()


@dataclass
class Filter:
    # arguments : sample, filter-idx, .credentials , loaded_filters, type (sample/region/project)
    sid: int
    f_idx: str
    config: object
    filters: object
    filter_type: str
    build: str = None
    upstream_table: str = None
    # output :
    comments: list = field(default_factory=list)
    # internal variables
    do_ref_calls: bool = False
    do_gvcf: bool = False
    qc_part: str = ""

    # run filter on creation.
    def __post_init__(self):
        # not allowed for project/region filters.
        if self.filter_type != "sample":
            log.warning("De Novo filter is only valid in sample-based queries. Skipping.")
            self.comments.append(
                "WARNING : De Novo filter is only valid in sample-based queries. Skipping."
            )
            return self
        # extract values of this filter:
        self.values = self.filters["dynamic"][self.f_idx]["values"]
        # get DB connection.
        self._GetDBConnection()
        # run.
        self._RunFilter()

    def _RunFilter(self):
        ## PREPARE
        ##########
        # parents:
        self._GetParents()
        # activate gvcf handling ?
        self._ActivateGVCF()
        # load QC criteria?
        self._SetQC()
        # load parental variants.
        parents_alts = self._LoadParentalVariants()
        # step 1 :
        # ########
        # exclude variants called as alt in parents.
        tmp_index = "{}-{}-denovo-idx".format(self.config["RUNTIME"]["tmp_prefix"], self.f_idx)
        query = "SELECT HIGH_PRIORITY `_upstream_`.vid FROM `_upstream_` LEFT OUTER JOIN `_parents_` ON `_upstream_`.vid = `_parents_`.vid WHERE `_parents_`.vid IS NULL"
        query = query.replace("_parents_", parents_alts)
        if self.upstream_table:
            query = query.replace("_upstream_", self.upstream_table)
        else:
            query = "{} AND `Variants_x_Samples`.sid = {}".format(
                query.replace("_upstream_", "Variants_x_Samples"), self.sid
            )
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    tmp_index
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(tmp_index))
            self.dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(tmp_index, query))
        except Exception as e:
            raise e

        # step 2 :
        # ########
        # exclude variants *not* called as reference in both parents (as vcf or gvcf).
        if self.do_ref_calls or self.do_gvcf:
            log.debug("Exclude variants not called as ref in both parents")
            tmp_index_full = tmp_index
            tmp_index = "{}-ref_calls".format(tmp_index)
            parents_refs = self._LoadParentalRefCalls(tmp_index_full)
            try:
                self.dbh.doQuery(
                    "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                        tmp_index
                    )
                )
                self.dbh.doQuery("TRUNCATE TABLE `{}`".format(tmp_index))
                query = "SELECT  HIGH_PRIORITY `{}`.vid FROM `{}` JOIN `{}` ON `{}`.vid = `{}`.vid".format(
                    tmp_index_full, tmp_index_full, parents_refs, tmp_index_full, parents_refs
                )
                log.debug(query)
                self.dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(tmp_index, query))
            except Exception as e:
                raise e
        # result table with resulting vids.
        self.results_table = tmp_index

    def _SetQC(self):
        qc_part = ""
        ## APPLY QC filters to parents ?
        if (
            not "Apply_QC_to_parents" in self.values
            or not self.values["Apply_QC_to_parents"] == "1"
        ):
            log.info("Application of QC-criteria to parental variants is disabled.")
            self.qc_part = qc_part
            return
        for f_idx in self.filters["phase1"]:
            filter_settings = self.filters["phase1"][f_idx]
            filter_root = self.filters["phase1"][f_idx]["config"]
            negate = (
                filter_settings["values"]["negate"] if "negate" in filter_settings["values"] else ""
            )
            args = list(filter(None, filter_settings["values"]["argument"].split("@@@")))

            # arguments taken from query (eg Filter field) : implies multi-select + IN ()
            if ET.iselement(filter_root.find("settings/ArgumentsFromQuery")):
                where = filter_root.find("settings/where").text.replace(
                    r"%replace", "'{}'".format("','".join(args))
                )
                qc_part = "{} AND {} {}".format(qc_part, negate, where)

            # options provided ? <, =, >
            if ET.iselement(filter_root.find("options")):
                # %replace => selected option (first arg element)
                where = filter_root.find("settings/where").text
                where = where.replace(
                    r"%replace", filter_root.find("options/{}".format(args[0])).text
                )
                # %value => provided value
                where = where.replace(r"%value", filter_settings["values"]["value"])

                qc_part = "{} AND {} {}".format(qc_part, negate, where)
        # if TRANCHE qc-items : add GGA tranche.
        qc_part = re.sub(r"Filter IN (", "Filter IN ('GGA',", qc_part)
        # store the result.
        self.qc_part = qc_part
        log.info("QC part for parents in De Novo : {}".format(qc_part))

    def _GetParents(self):
        self.mother = None
        self.father = None
        rows = self.dbh.runQuery(
            "SELECT HIGH_PRIORITY s.gender, ss.sid1 FROM `Samples_x_Samples` ss JOIN `Samples` s ON ss.sid1 = s.id WHERE `sid2` = %s AND `Relation` = 2",
            self.sid,
        )
        for r in rows:
            if r["gender"] == "Male":
                self.father = r["sid1"]
            elif r["gender"] == "Female":
                self.mother = r["sid1"]

        if not self.mother and not self.father:
            log.warning("No parents specified. Skipping filter.")
            self.comments.append(
                "WARNING: No parents specified for this sample. Skipping DeNovo Filter"
            )
            return self
        elif not self.mother or not self.father:
            log.warning(
                "Single parent specified for this sample. DeNovo filtering on one parent will produce an overestimate."
            )
            self.comments.append(
                "WARNING: Single parent specified for this sample. DeNovo filtering on one parent will produce an overestimate."
            )

    def _ActivateGVCF(self):
        if (
            not "Exclude_Non_Covered" in self.values
            or not self.values["Exclude_Non_Covered"] == "1"
        ):
            log.info("Exclusion of sites not called as ref in both parents is disabled.")
            return

        # only valid if both parents are specified.
        if not self.mother or not self.father:
            log.warning(
                "Exclusion of non-covered positions is not applied in case of a single available parent."
            )
            self.comments.append(
                "WARNING: Exclusion of non-covered positions is not applied in case of a single available parent."
            )
            return
        # activate reference-call handling.
        self.do_ref_calls = True
        # second : should we process gvcf data as well ?
        rows = self.dbh.runQuery(
            "SELECT HIGH_PRIORITY SUM(gvcf) AS nrGVCF FROM `Samples` WHERE id IN (%s,%s)",
            [self.father, self.mother],
        )
        if rows[0]["nrGVCF"] < 2:
            log.warning("GVCF data not available for both parents: GVCF handling disabled.")
            self.comments.append(
                "WARNING: GVCF data not available for both parents: GVCF handling disabled."
            )
            return
        self.do_gvcf = True
        return

    def _LoadParentalVariants(self):
        # alternate calls.
        alts_table = "{}-{}-denovo-par-alts".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx
        )
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    alts_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(alts_table))
            # insert ignore : skip duplicates eg when multiple transcripts for one variant : faster than group by / replace into.
            #   => ignore risk : silent errors, but here : just vid, no foreign constraints etc.

            query = "SELECT HIGH_PRIORITY vid FROM `Variants_x_Samples` WHERE sid IN ('{}') AND AltCount IN (1,2) {}".format(
                "','".join([str(x) for x in filter(None, [self.mother, self.father])]), self.qc_part
            )
            log.debug(query)
            self.dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(alts_table, query))
        except Exception as e:
            raise e

        return alts_table

    def _LoadParentalRefCalls(self, tmp_index):
        # ref calls
        refs_table = "{}-{}-denovo-par-refs".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx
        )
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY, `nrP` INT(1)) ENGINE=MEMORY CHARSET=latin1".format(
                    refs_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(refs_table))
        except Exception as e:
            raise e

        if self.do_ref_calls:
            try:
                query = "SELECT HIGH_PRIORITY vid, COUNT(1) AS `NrP` FROM `Variants_x_Samples` WHERE sid IN ('{}') AND AltCount = 0 {} GROUP BY vid".format(
                    "','".join([str(x) for x in [self.mother, self.father]]), self.qc_part
                )
                self.dbh.doQuery("INSERT INTO `{}` (`vid`, `nrP`) {}".format(refs_table, query))
            except Exception as e:
                raise e

        # gvcf blocks : simulate as ref calls if quality is ok.
        if self.do_gvcf:
            try:
                # work per chromosome
                for c in list(range(1, 26)):
                    # variants in the tmp table on this chromosome.
                    variants = self.dbh.runQuery(
                        "SELECT HIGH_PRIORITY `{}`.vid, `Variants`.start FROM `{}` JOIN `Variants` ON `{}`.vid = `Variants`.id WHERE `Variants`.chr = {} ORDER BY start".format(
                            tmp_index, tmp_index, tmp_index, c
                        )
                    )
                    # process both parents
                    for p in [self.mother, self.father]:
                        # ref blocks for this parent
                        blocks = self.dbh.runQuery(
                            "SELECT HIGH_PRIORITY start, stop FROM `Samples_x_Reference_Blocks_chr{}` WHERE `sid` = {} AND `genotype_quality` > 20 ORDER BY start, stop".format(
                                c, p
                            )
                        )

                        for variant in variants:
                            # go over blocks untill past the variant.
                            for block in blocks:
                                if (
                                    variant["pos"] >= block["start"]
                                    and variant["pos"] <= block["stop"]
                                ):
                                    # add / increment count.
                                    log.info(
                                        "Adding variant {}, inside {}:{}-{}".format(
                                            variant["vid"], c, block["start"], block["stop"]
                                        )
                                    )
                                    self.dbh.doQuery(
                                        "INSERT INTO `{}` (vid, nrP) VALUES ('{}','1') ON DUPLICATE KEY nrP = nrP+1".format(
                                            refs_table, variant["vid"]
                                        )
                                    )
                                # blocks past variant : skip to next variant.
                                elif block["start"] > variant["pos"]:
                                    break

            except Exception as e:
                raise e
        # only keep variant seen in both parents.
        self.dbh.doQuery("DELETE FROM `{}` WHERE `nrP` < 2".format(refs_table))

        return refs_table

    def _GetDBConnection(self, quiet=True):

        try:
            db_suffix = self.config["DATABASE"].get("DBSUFFIX", "")
            if db_suffix:
                db_suffix = f"_{db_suffix}"
                log.info(f"Using db_suffix : {db_suffix}")
            else:
                log.info("no suffix")
            dbh = MySQL(
                user=self.config["DATABASE"]["DBUSER"],
                password=self.config["DATABASE"]["DBPASS"],
                host=self.config["DATABASE"]["DBHOST"],
                database=f"NGS-Variants-Admin{db_suffix}",
                allow_local_infile=True,
            )
            row = dbh.runQuery("SELECT HIGH_PRIORITY `name`, `StringName` FROM `CurrentBuild`")
            db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
            # use statement doesn't work with placeholder. reason unknown
            dbh.select_db(db)
            if not quiet:
                log.info(
                    "Connected to Database using Genome Build {}{}".format(
                        row[0]["StringName"], db_suffix
                    )
                )

        except Exception as e:
            raise e
        # switch build : disabled.
        # if self.build:
        #    log.info(f"SEtting provided build in denovo module: {self.build}")
        #    # should exist
        #    try:
        #        dbh.select_db("NGS-Variants-{}".format(self.build))
        #        if not quiet:
        #            log.info("Selected provided build: NGS-Variants-{}".format(self.build))
        #    except MySqlError as e:
        #        raise ValueError("Invalid GenomeBuild '{}' : {}".format(self.build, e))
        self.dbh = dbh
