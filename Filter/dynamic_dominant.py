from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import get_logger
from CMG.Utils import Re, CheckVariableType

import sys
import xml.etree.ElementTree as ET
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Dict, Any, Optional, List
from pprint import pprint

# error classes
class DataError(OSError):
    pass


class ValueError(OSError):
    pass


log = get_logger()
re = Re()


@dataclass
class Filter:
    # arguments : sample, filter-idx, .credentials , loaded_filters, type (sample/region/project)
    sid: int
    f_idx: str
    config: object
    filters: object
    filter_type: str
    build: str = None
    upstream_table: str = None
    # output :
    comments: list = field(default_factory=list)
    # internal variables
    qc_part: str = ""
    family: dict = field(default_factory=dict)

    # run filter on creation.
    def __post_init__(self):
        # not allowed for project/region filters.
        if self.filter_type != "sample":
            log.warning("De Novo filter is only valid in sample-based queries. Skipping.")
            self.comments.append(
                "WARNING : De Novo filter is only valid in sample-based queries. Skipping."
            )
            return self
        # extract values of this filter:
        self.values = self.filters["dynamic"][self.f_idx]["values"]

        # get DB connection.
        self._GetDBConnection()
        # run.
        self._RunFilter()

    def _RunFilter(self):
        ## PREPARE
        ##########
        # affected Family:
        affected_members = self._GetFamily(affected=True)
        if len(affected_members) == 0:
            log.warning("No affected family members found. Skipping.")
            self.comments.append("WARNING : No affected family members found. Skipping.")
            return self

        # load QC criteria?
        self._SetQC()

        # AltCount Settings ?
        self.alt_count = "AltCount IN (1,2) AND "
        if "Exclude_homozygous" in self.values and self.values["Exclude_homozygous"] == "1":
            self.alt_count = "AltCount = 1 AND "

        # load shared variants in affected.
        affected_table = self._LoadAffected(affected_members)

        # load variants in unaffected.
        unaffected_members = self._GetFamily(affected=False)
        unaffected_table = self._LoadUnaffected(unaffected_members)

        # intersect affected with upstream if specified
        affected_table = self._IntersectUpstream(affected_table)

        # then take left outer join to exclude variants in uaffected.
        self.results_table = self._GetFinalVariants(affected_table, unaffected_table)

    def _GetFinalVariants(self, affected_table, unaffected_table):
        final_table = "{}-{}-dominant-final-variants".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx
        )
        query = "SELECT HIGH_PRIORITY `{}`.vid FROM `{}` LEFT OUTER JOIN `{}` ON `{}`.vid = `{}`.vid WHERE `{}`.vid IS NULL".format(
            affected_table,
            affected_table,
            unaffected_table,
            affected_table,
            unaffected_table,
            unaffected_table,
        )

        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    final_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(final_table))
            log.debug(query)
            self.dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(final_table, query))
        except Exception as e:
            raise e

        return final_table

    def _IntersectUpstream(self, affected_table):
        if not self.upstream_table:
            log.debug("No upstream table found.")
            return affected_table
        intersected_table = "{}-{}-dominant-intersected-upstream".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx
        )
        query = (
            "SELECT HIGH_PRIORITY `{}`.vid FROM `{}` INNER JOIN `{}` ON `{}`.vid = `{}`.vid".format(
                affected_table,
                affected_table,
                self.upstream_table,
                affected_table,
                self.upstream_table,
            )
        )
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    intersected_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(intersected_table))
            log.debug(query)
            self.dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(intersected_table, query))
        except Exception as e:
            raise e

        return intersected_table

    def _LoadAffected(self, affected_members):
        nr_affected = len(affected_members)
        query = "SELECT HIGH_PRIORITY vid AS VariantID, COUNT(sid) AS Occurence FROM `Variants_x_Samples` WHERE {} sid IN ({}) {} GROUP BY vid HAVING Occurence = '{}'".format(
            self.alt_count, ",".join(affected_members), self.qc_part, nr_affected
        )
        affected_table = "{}-{}-dominant-affected".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx
        )
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY, `Occurence` INT(2)) ENGINE=MEMORY CHARSET=latin1".format(
                    affected_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(affected_table))
            # insert ignore : skip duplicates eg when multiple transcripts for one variant : faster than group by / replace into.
            #   => ignore risk : silent errors, but here : just vid, no foreign constraints etc.
            log.debug(query)
            self.dbh.doQuery(
                "INSERT IGNORE INTO `{}` (`vid`, `Occurence`) {}".format(affected_table, query)
            )
        except Exception as e:
            raise e

        return affected_table

    def _LoadUnaffected(self, unaffected_members):
        # no members to process.
        if len(unaffected_members) == 0:
            return
        # set allowed carrier number
        if "Max_unaff_carriers" in self.values and CheckVariableType(
            self.values["Max_unaff_carriers"], int
        ):
            max_carriers = self.values["Max_unaff_carriers"]
            log.info(
                "Allowing {}/{} unaffected carriers".format(max_carriers, len(unaffected_members))
            )
        else:
            log.info(
                "Max unaffected carriers not set or invalid value ({}) : setting to zero".format(
                    self.values["Max_unaff_carriers"]
                )
            )
            max_carriers = 0

        query = "SELECT HIGH_PRIORITY vid AS VariantID, COUNT(sid) AS Occurence FROM `Variants_x_Samples` WHERE AltCount IN (1,2) AND sid IN ({}) {} GROUP BY vid HAVING Occurence > '{}'".format(
            ",".join(unaffected_members), self.qc_part, max_carriers
        )
        unaffected_table = "{}-{}-dominant-unaffected".format(
            self.config["RUNTIME"]["tmp_prefix"], self.f_idx
        )
        try:
            self.dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY, `Occurence` INT(2)) ENGINE=MEMORY CHARSET=latin1".format(
                    unaffected_table
                )
            )
            self.dbh.doQuery("TRUNCATE TABLE `{}`".format(unaffected_table))
            # insert ignore : skip duplicates eg when multiple transcripts for one variant : faster than group by / replace into.
            #   => ignore risk : silent errors, but here : just vid, no foreign constraints etc.
            log.debug(query)
            self.dbh.doQuery(
                "INSERT IGNORE INTO `{}` (`vid`, `Occurence`) {}".format(unaffected_table, query)
            )
        except Exception as e:
            raise e

        return unaffected_table

    def _SetQC(self):
        qc_part = ""
        ## APPLY QC filters to parents ?
        if (
            not "Apply_QC_to_parents" in self.values
            or not self.values["Apply_QC_to_parents"] == "1"
        ):
            log.debug("Application of QC-criteria to parental variants is disabled.")
            self.qc_part = qc_part
            return

        for f_idx in self.filters["phase1"]:
            filter_settings = self.filters["phase1"][f_idx]
            filter_root = self.filters["phase1"][f_idx]["config"]
            negate = (
                filter_settings["values"]["negate"] if "negate" in filter_settings["values"] else ""
            )
            args = list(filter(None, filter_settings["values"]["argument"].split("@@@")))

            # arguments taken from query (eg Filter field) : implies multi-select + IN ()
            if ET.iselement(filter_root.find("settings/ArgumentsFromQuery")):
                where = filter_root.find("settings/where").text.replace(
                    r"%replace", "'{}'".format("','".join(args))
                )
                qc_part = "{} AND {} {}".format(qc_part, negate, where)

            # options provided ? <, =, >
            if ET.iselement(filter_root.find("options")):
                # %replace => selected option (first arg element)
                where = filter_root.find("settings/where").text
                where = where.replace(
                    r"%replace", filter_root.find("options/{}".format(args[0])).text
                )
                # %value => provided value
                where = where.replace(r"%value", filter_settings["values"]["value"])

                qc_part = "{} AND {} {}".format(qc_part, negate, where)
        # if TRANCHE qc-items : add GGA tranche.
        qc_part = re.sub(r"Filter IN (", "Filter IN ('GGA',", qc_part)
        # store the result.
        self.qc_part = qc_part

    def _GetFamily(self, affected=False):
        # retrieve all family members
        self._GetRelatives(int(self.sid))
        ## extract affected/nonaffected
        if affected:
            return [str(x) for x in self.family if self.family[x]["affected"] == 1]
        else:
            return [str(x) for x in self.family if self.family[x]["affected"] == 0]

    def _GetRelatives(self, sid):
        if sid in self.family:
            return
        self.family[sid] = dict()
        # sample itself.
        rows = self.dbh.runQuery("SELECT HIGH_PRIORITY affected FROM `Samples` WHERE id = %s", sid)
        self.family[sid]["affected"] = rows[0]["affected"]
        # parents
        rows = self.dbh.runQuery(
            "SELECT HIGH_PRIORITY sid1 FROM `Samples_x_Samples` WHERE sid2 = %s AND Relation = 2",
            sid,
        )
        for row in rows:
            self._GetRelatives(int(row["sid1"]))
        # siblings & children
        rows = self.dbh.runQuery(
            "SELECT HIGH_PRIORITY sid2 FROM `Samples_x_Samples` WHERE sid1 = %s AND Relation IN (2,3)",
            sid,
        )
        for row in rows:
            self._GetRelatives(int(row["sid2"]))

    def _GetDBConnection(self, quiet=True):

        try:
            db_suffix = self.config["DATABASE"].get("DBSUFFIX", "")
            if db_suffix:
                db_suffix = f"_{db_suffix}"
            dbh = MySQL(
                user=self.config["DATABASE"]["DBUSER"],
                password=self.config["DATABASE"]["DBPASS"],
                host=self.config["DATABASE"]["DBHOST"],
                database=f"NGS-Variants-Admin{db_suffix}",
                allow_local_infile=True,
            )
            row = dbh.runQuery("SELECT HIGH_PRIORITY `name`, `StringName` FROM `CurrentBuild`")
            db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
            # use statement doesn't work with placeholder. reason unknown
            dbh.select_db(db)
            if not quiet:
                log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

        except Exception as e:
            raise e
        # switch build?
        # if self.build:
        #    # should exist
        #    try:
        #        dbh.select_db("NGS-Variants-{}".format(self.build))
        #        if not quiet:
        #            log.info("Selected provided build: NGS-Variants-{}".format(self.build))
        #    except MySqlError as e:
        #        raise ValueError("Invalid GenomeBuild '{}' : {}".format(self.build, e))
        self.dbh = dbh
