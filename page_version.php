<?php
if ($loggedin != 1) {
	echo "<div class=section><h3>Access Denied</h3>\n";
	include('login.php');
	echo "</div>";
	exit();
}
echo "<div class=section>";
echo "<p><form action='index.php' METHOD=GET>";
echo "<input type=hidden name=page value=version />";

if (!isset($_GET['item']) || $_GET['item'] == 'w') {
	$_GET['item'] = 'w';
	echo "Show Version Log : <select id=item name=item><option value=w SELECTED>Web Interface</option><option value='d'>Database</option></select>";
}
else {
	echo "Show Version Log : <select id=item name=item><option value=w>Web Interface</option><option value='d' SELECTED>Database</option></select>";
}

echo " &nbsp; <input type=submit class=button value='submit'/></form></p>";
if (!isset($_GET['item'])) {
	echo "<h3>Server Version History</h3>";
	echo "<p>Please select a history section from the menu.</p>";
	echo" </div>";
}
elseif ($_GET['item'] == 'w') {
	echo "<h3>Web-Interface Version History</h3>";
	echo "<p>";
	exec("cd $sitedir && hg log ",$output,$exit);
	$printed = 0;
	$rev = '';
	$date = '';
	$summary = '';
	echo "<ul class=indent>";
	foreach($output as $key => $line) {
		if ($line == '') {
			echo "<li style='text-indent:-5px;' >Rev: $rev : $date : $summary</li>";
			$rev = '';
			$date = '';
			$summary = '';
			$printed++;
		}
		elseif (preg_match('/changeset:/',$line)) {
			$rev = preg_replace('/(changeset:\s+\d+:)(\S*)$/',"$2",$line);
		}
		elseif (preg_match('/date:/',$line)) {
			$date = preg_replace('/(date:\s+)(.*)$/',"$2",$line);
		}
		elseif (preg_match('/summary:/',$line)) {
			$summary = preg_replace('/(summary:\s+)(.*)$/',"$2",$line);
		}
	}
	echo "</ul></p>";


}
elseif ($_GET['item'] == 'd') {
	echo "<h3>Database Version History</h3>";
	echo "<p>";
	exec("cd $scriptdir/../Database_Revisions && hg log ",$output,$exit);
	$printed = 0;
	$rev = '';
	$date = '';
	$summary = '';
	echo "<ul class=indent>";
	foreach($output as $key => $line) {
		if ($line == '') {
			echo "<li style='text-indent:-5px;' >Rev: $rev : $date : $summary</li>";
			$rev = '';
			$date = '';
			$summary = '';
			$printed++;
		}
		elseif (preg_match('/changeset:/',$line)) {
			$rev = preg_replace('/(changeset:\s+\d+:)(\S*)$/',"$2",$line);
		}
		elseif (preg_match('/date:/',$line)) {
			$date = preg_replace('/(date:\s+)(.*)$/',"$2",$line);
		}
		elseif (preg_match('/summary:/',$line)) {
			$summary = preg_replace('/(summary:\s+)(.*)$/',"$2",$line);
		}
	}
	echo "</ul></p>";


}

echo "</div>";	
