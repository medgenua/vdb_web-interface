<?php
// check login
if (!isset($loggedin) || $loggedin != 1) {
	include('page_login.php');
	exit;
}

if (isset($_GET['t']) && $_GET['t'] == 'show') {
	include("includes/inc_report_".$_GET['t'].".inc");
	exit;
}
if (!isset($_GET['t'])) {
	$_GET['t'] = 'run';
}
/*
echo "<div class=section>";
echo "<h3>Report Management</h3>";
echo "<p>A report consists of one or more predefined filter/annotation combinations, included as independent sections. To define a report, first generate these combinations. Next, decide which combinations will be included into report. A set of combinations can then be saved as a report template.</p><p> ";
echo "<form action='index.php' method=GET>";
echo "<input type=hidden name='page' value='report'/>";
echo "Action : <select name='t'>";
$t = array('new' => '1. Define a report section','combine' => '2. Compose report','run' => '3. Generate sample reports');
$m = array('manage' => 'a. View/Edit report sections', 'share' => 'b. Share report/section configurations', 'delete' => 'c. Delete report/section configurations'); //,'edit' => 'Edit Gene Panels');

echo "<optgroup label='Building a Report'>";
foreach($t as $key => $value) {
	$s = '';
	if ($_GET['t'] == $key) {
		$s = 'SELECTED';
	}
	echo "<option value='$key' $s>$value</option>";
}
echo "</optgroup><optgroup label='Managing configurations'>";
foreach($m as $key => $value) {
	$s = '';
	if ($_GET['t'] == $key) {
		$s = 'SELECTED';
	}
	echo "<option value='$key' $s>$value</option>";
}
echo "</optgroup>";
echo "</select> : <input type=submit class=button name='ts' value='Select' /></form>\n";
echo "</div>";
*/
if (!file_exists("includes/inc_report_".$_GET['t'].".inc")) {
	include('page_404.php');
	exit;
}
else {
	include("includes/inc_report_".$_GET['t'].".inc");
}
