<html>
<body>
<h1>CVN-WebStore Installation Problem Detected</h1>
<p> There were problems initialising CNV-WebStore. Please make sure you have completed all the steps below: </p>
<p><ol>
	<li>Clone the web interface :  "hg clone http://hg.cnv-webstore.be/Web-Interface"</li>
	<li>Clone the Installation scripts : "hg clone http://hg.cnv-webstore.be/Installation_Scripts"</li>
	<li>Run the scripts to setup the database, users and credentials: "./setup.cnv.webstore.pl"</li>
	<li>Optionally: clone and install the CNV-analysis scripts</li>
</ol></p>
<p>Please refer to the installation documentation for full instructions. </p>
</body>
</html>
	
