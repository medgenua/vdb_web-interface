<?php
// check login
if (!isset($loggedin) || $loggedin != 1) {
    include('page_login.php');
    exit;
}


if (!isset($_GET['t'])) {
    $_GET['t'] = 'manage';
}

echo "<div class=section>";
echo "<h3>Mane/Favourite Transcripts Management</h3>";
echo "<p>Using transcript sets, individual transcripts can be added to or removed from the MANE/Select set from NCBI. The status of a transcript as 'favourite' is only used to highlight the transcript, and has no effect on the filtering. </p>";
echo "<p><form action='index.php' method=GET>";
echo "<input type=hidden name='page' value='mane'/>";
echo "Action : <select name='t'>";
$t = array('manage' => 'View/Share/Edit Transcript Sets', 'new' => 'Define a New Transcript Set'); //,'edit' => 'Edit Gene Panels');
foreach ($t as $key => $value) {
    $s = '';
    if ($_GET['t'] == $key) {
        $s = 'SELECTED';
    }
    echo "<option value='$key' $s>$value</option>";
}
echo "</select> : <input type=submit class=button name='ts' value='Select' /></form>\n";
echo "</div>";

if (!file_exists("includes/inc_mane_" . $_GET['t'] . ".inc")) {
    include('page_404.php');
    exit;
} else {
    include("includes/inc_mane_" . $_GET['t'] . ".inc");
}
