<?php

if ($loggedin != 1) {
    include('page_login.php');
    exit();
}

// VARIABLES FOR LAYOUT
$readicon = array('<img src=Images/layout/mailred.png>', '<img src=Images/layout/mailgrey.png>');
$inh = array(0 => 'ND', 1 => 'P', 2 => 'M', 3 => 'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
# CHROM HASH
for ($i = 1; $i <= 22; $i += 1) {
    $chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";


//check for page settings?
if (isset($_GET['part'])) {
    $part = $_GET['part'];
    if ($part < 1) {
        $part = 1;
    }
    $subpage = $part - 1;
    $start = $subpage * 20;
    $limit = "LIMIT $start,20";
} else {
    $part = 1;
    $limit = '';
}


// process delete action
if (isset($_GET['d'])) {
    doQuery("DELETE FROM `Inbox` WHERE id = '" . $_GET['d'] . "'", "Inbox");
    // check if any unread remain
    $rows = runQuery("SELECT Inbox.id FROM `Inbox` WHERE Inbox.to = '$userid' AND Inbox.read = 0", 'Inbox');
    if (count($rows) > 0) {
        $_SESSION['newmessages'] = 1;
    } else {
        $_SESSION['newmessages'] = 0;
    }
}
// if a message is to be shown, mark as read
if (isset($_GET['s'])) {
    doQuery("UPDATE `Inbox` SET Inbox.read = '1' WHERE Inbox.id = '" . $_GET['s'] . "'", "Inbox");
    $rows = runQuery("SELECT Inbox.id FROM `Inbox` WHERE Inbox.to = '$userid' AND Inbox.read = 0", 'Inbox');
    if (count($rows) > 0) {
        $_SESSION['newmessages'] = 1;
    } else {
        $_SESSION['newmessages'] = 0;
    }
}
// GET ACTIONS RELATED MESSAGES.
$actions = '';
$rows = runQuery("SELECT action_type, LastUpdate FROM `Inbox_actions` WHERE uid = '$userid' ORDER BY LastUpdate DESC", 'Inbox_actions');
if (count($rows) > 0) {
    foreach ($rows as $k => $row) {
        $type = $row['action_type'];
        $date = $row['LastUpdate'];

        // columns : 
        // - read/unread 
        // - from 
        // - topic
        // - date
        // - show/trash
        $readimg = $readicon[0];
        // validation of variant classifiers.
        if ($type == 'Validate Classifier') {
            // 1. Get classifiers allowed to validate.
            $srows = runQuery("SELECT uc.cid,c.Name FROM `Users_x_Classifiers` uc JOIN `Classifiers` c ON c.id = uc.cid WHERE uid = '$userid' AND `can_validate` = 1", 'Users_x_Classifiers:Classifiers');
            foreach ($srows as $k => $srow) {
                $cid = $srow['cid'];
                $name = $srow['Name'];
                // 2. Get nr of variants to validate.
                $v = runQuery("SELECT COUNT(cv.vid) AS nr FROM `Classifiers_x_Variants` cv WHERE cid = '$cid' AND validate_reject = 0", "Classifiers_x_Variants");
                // 3. add to table.
                if ($v[0]['nr'] > 0) {
                    $nr = $v[0]['nr'];
                    $actions .= "<tr ><td >$readimg</td><td >&nbsp; <span class=italic>system</span> &nbsp;</td><td >$nr classifier validations for '$name'</td><td>$date</td><td><a href='index.php?page=inbox_actions&c=$cid' class=img target='_blank'><img src='Images/layout/eye.png' style='height:1em;'></a></td></tr>";
                }
            }
        }
    }
}

// show messages
echo "<div class=section>";
echo "<h3>User Inbox</h3>\n";
if ($part <= 1) {
    echo "<p><a href='index.php?page=inbox&part=2'>Next 20 messages</a></p>";
} else {
    echo "<p><a href='index.php?page=inbox&part=" . ($part - 1) . "'>Previous 20 messages</a> / <a href='index.php?page=inbox&part=" . ($part + 1) . "'>Next 20 messages</a></p>";
}

// actions.
$table_started = 0;
if ($actions != '') {
    $table_started = 1;
    echo "<table cellspacing=0>";
    echo "<tr><th class=top>&nbsp;</th><th class=top>From</th><th class=top>Subject</th><th class=top>Date</th><th class=top>&nbsp;</th></tr>";
    echo $actions;
}

// regular messages
$rows = runQuery("SELECT i.id, i.subject, i.date, i.read, u.FirstName, u.LastName FROM `Inbox` i JOIN `Users` u ON i.from = u.id WHERE i.to = '$userid' ORDER BY i.date DESC $limit", "Inbox:Users");
if (count($rows) > 0) {
    if ($table_started == 0) {
        echo "<table cellspacing=0>";
        echo "<tr><th class=top>&nbsp;</th><th class=top>From</th><th class=top>Subject</th><th class=top>Date</th><th class=top>&nbsp;</th></tr>";
        $table_started = 1;
    }
    foreach ($rows as $k => $row) {
        $mid = $row['id'];
        $subject = $row['subject'];
        $date = $row['date'];
        $read = $row['read'];
        $readimg = $readicon[$read];
        $from = $row['FirstName'] . ' ' . $row['LastName'];
        $url = "index.php?page=inbox&amp;s=$mid&amp;part=$part";
        echo "<tr ><td onmouseover=\"this.style.cursor='pointer'\"  onClick=\"location.href='$url'\">$readimg</td><td onmouseover=\"this.style.cursor='pointer'\"  onClick=\"location.href='$url'\">$from</td><td onmouseover=\"this.style.cursor='pointer'\"  onClick=\"location.href='$url'\">$subject</td><td onmouseover=\"this.style.cursor='pointer'\"  onClick=\"location.href='$url'\">$date</td><td><a href='index.php?page=inbox&d=$mid&part=$part' class=img><img src='Images/layout/icon_trash.gif'></a></td></tr>";
    }
}


if ($table_started == 1) {
    echo "</table></p>";
} else {
    echo "no messages available.</p>";
}
echo "</div>";

// show selected message content  
if (isset($_GET['s']) && $_GET['s'] != '') {
    $mid = $_GET['s'];
    $row = array_shift(...[runQuery("SELECT i.`subject`, i.`date`, i.`read`, u.`FirstName`, u.`LastName`, i.`body`, i.`type`, i.`values` FROM `Inbox` i JOIN `Users` u ON i.`from` = u.`id` WHERE i.`id` = $mid", "Inbox:Users")]);
    $subject = $row['subject'];
    $date = $row['date'];
    $from = $row['FirstName'] . ' ' . $row['LastName'];
    $body = $row['body'];
    $type = $row['type'];
    $values = $row['values'];
    echo "<div class=section>";
    echo "<h3>Message From $from</h3>";
    echo "<p><span class=emph>Subject:</span> $subject</p>";
    if ($type == '') {
        echo "<p><span class=emph>Message:</span>";
        echo "<pre class=scrollbarbox style='height:250px;padding:5px'>$body</pre>";
        echo "</p></div>";
    } elseif ($type == 'shared_project') {
        $yesno = array('Not Allowed', 'Allowed');
        $permrow = runQuery("SELECT pp.`editvariant`, pp.`editclinic`, pp.`editsample`,p.`created`, p.`name` FROM `Projects_x_Users` pp JOIN `Projects` p ON pp.pid = p.id WHERE pp.uid = '$userid' AND p.id = '$values'", "Projects_x_Users:Projects")[0];
        $editcnv = $yesno[$permrow['editvariant']];
        $editclinic = $yesno[$permrow['editclinic']];
        $editsample = $yesno[$permrow['editsample']];
        $created = $permrow['created'];
        $pname = $permrow['name'];
        $rows = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = $values", "Projects_x_Samples");
        $nrsamples = count($rows);
        echo "<p>$from shared a project with you. Some details are listed below:</p>";
        echo "<table cellspacing=0>";
        echo "<tr><th class=top colspan=2>Project Details</th></tr>";
        echo "<tr><th class=left>Project Name </th><td>$pname</td></tr>";
        echo "<tr><th class=left>Creation Date</th><td>$created</td></tr>";
        //echo "<tr><th class=left>Collection</th><td>$collection</td></tr>";
        echo "<tr><th class=left>Nr. Samples</th><td>$nrsamples</td></tr>";
        echo "<tr><td colspan=2>&nbsp;</td></tr>";
        echo "<tr><th class=top colspan=2>Permission Settings</th></tr>";
        echo "<tr><th class=left>Edit Variants</th><td>$editcnv</td></tr>";
        echo "<tr><th class=left>Edit Clinic</th><td>$editclinic</td></tr>";
        echo "<tr><th class=left>Project Control</th><td>$editsample</td></tr>";
        echo "</table></p>";
        echo "<p></p>";
        echo "</div>";
    } elseif ($type == 'shared_genepanel' || $type == 'shared_panel') { // legacy naming compatibility
        $permrow = runQuery("SELECT pp.`rw`, pp.`share`, p.`LastEdit`, p.`Name`, p.`Description`,u.`LastName`, u.`FirstName` FROM `GenePanels_x_Users` pp JOIN `GenePanels` p JOIN `Users` u ON pp.gpid = p.id AND p.Owner = u.id WHERE pp.uid = '$userid' AND p.id = '$values'", "GenePanels_x_Users:GenePanels:Users")[0];
        $yesno = array('Not Allowed', 'Allowed');
        $rw = $yesno[$permrow['rw']];
        $share = $yesno[$permrow['share']];
        $le = $permrow['LastEdit'];
        $owner = $permrow['LastName'] . " " . substr($permrow['FirstName'], 0, 1) . ".";
        $desc = $permrow['Description'];
        $name = $permrow['Name'];
        $nr = runQuery("SELECT COUNT(1) AS `nr` FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = '$values'")[0];
        $nrgenes = $nr['nr'];
        echo "<p>$from shared a GenePanel with you. Some details are listed below:</p>";
        echo "<table cellspacing=0>";
        echo "<tr><th class=top colspan=2>Panel Details</th></tr>";
        echo "<tr><th class=left>Name </th><td>$name</td></tr>";
        echo "<tr><th class=left>Description</th><td>$desc</td></tr>";
        echo "<tr><th class=left>Last Update Date</th><td>$le</td></tr>";
        //echo "<tr><th class=left>Collection</th><td>$collection</td></tr>";
        echo "<tr><th class=left>Nr. Genes</th><td>$nrgenes</td></tr>";
        echo "<tr><td colspan=2>&nbsp;</td></tr>";
        echo "<tr><th class=top colspan=2>Permission Settings</th></tr>";
        echo "<tr><th class=left>Edit</th><td>$rw</td></tr>";
        echo "<tr><th class=left>Sharing</th><td>$share</td></tr>";
        echo "</table></p>";
        echo "<p>";
        if ($rw == 'Allowed') {
            echo "<a href='index.php?page=genepanels&t=edit&gpid=$values'>Edit details of included genes</a>";
        } else {
            echo "<a href='index.php?page=genepanels&t=show&gpid=$values'>Go to details of included genes</a>";
        }
        // export link
        echo "<a href='index.php?page=genepanels&t=export&gpid=$values'>Export Gene List</a></p>";
        echo "</div>";
    } elseif ($type == 'shared_report_sectio') { // max 20 chars truncates contents
        $permrow = runQuery("SELECT pp.`edit`, pp.`full`, p.`Name`, p.`Description`,u.`LastName`, u.`FirstName`,p.FilterSet,p.Annotations,p.checkboxes FROM `Users_x_Report_Sections` pp JOIN `Report_Sections` p JOIN `Users` u ON pp.rsid = p.rsid AND p.Owner = u.id WHERE pp.uid = '$userid' AND p.rsid = '$values'", "Users_x_Report_Sections:Report_Sections:Users")[0];
        $yesno = array('Not Allowed', 'Allowed');
        $rw = $yesno[$permrow['edit']];
        $share = $yesno[$permrow['full']];
        $owner = $permrow['LastName'] . " " . substr($permrow['FirstName'], 0, 1) . ".";
        $desc = $permrow['Description'];
        $name = $permrow['Name'];
        // content.
        $filter = runQuery("SELECT FilterName FROM `Users_x_FilterSettings` WHERE fid = '" . $permrow['FilterSet'] . "'", "Users_x_FilterSettings")[0];
        $filter = $filter['FilterName'];
        $arows = runQuery("SELECT AnnotationName FROM `Users_x_Annotations` WHERE aid IN (" . $permrow['Annotations'] . ")", "Users_x_Annotations");

        echo "<p>$from shared a Report Section configuration with you. Some details are listed below:</p>";
        echo "<table cellspacing=0>";
        echo "<tr><th class=top colspan=2>Section Details</th></tr>";
        echo "<tr><th class=left>Name </th><td>$name</td></tr>";
        echo "<tr><th class=left>Description</th><td>$desc</td></tr>";
        //echo "<tr><th class=left>Collection</th><td>$collection</td></tr>";
        echo "<tr><th class=left>Applied Filterset</th><td>$filter</td></tr>";
        echo "<tr><th class=left style='vertical-align:top'>Included Annotations</th><td style='vertical-align:top'>";
        foreach ($arows as $arow) {
            echo " - " . $arow['AnnotationName'] . "<br/>";
        }
        echo "<tr><td colspan=2>&nbsp;</td></tr>";
        echo "<tr><th class=top colspan=2>Permission Settings</th></tr>";
        echo "<tr><th class=left>Edit</th><td>$rw</td></tr>";
        echo "<tr><th class=left>Sharing</th><td>$share</td></tr>";
        echo "</table></p>";
        if ($rw == 'Allowed') {
            echo "<p><a href='index.php?page=report&t=manage&rsid=$values'>Edit Section details</a></p>";
        }
        echo "<p>You can now use this section in your report configurations.</p>";
        echo "</div>";
    } elseif ($type == 'shared_report_') {
        $permrow = runQuery("SELECT pp.`edit`, pp.`full`, p.`Name`, p.`Description`,u.`LastName`, u.`FirstName`,p.Sections FROM `Users_x_Report_Definitions` pp JOIN `Report_Definitions` p JOIN `Users` u ON pp.rid = p.rid AND p.Owner = u.id WHERE pp.uid = '$userid' AND p.rid = '$values'", "Users_x_Report_Definitions:Report_Definitions:Users")[0];
        $yesno = array('Not Allowed', 'Allowed');
        $rw = $yesno[$permrow['edit']];
        $share = $yesno[$permrow['full']];
        $owner = $permrow['LastName'] . " " . substr($permrow['FirstName'], 0, 1) . ".";
        $desc = $permrow['Description'];
        $name = $permrow['Name'];
        // content.
        $srows = runQuery("SELECT Name FROM `Report_Sections` WHERE rsid IN (" . $permrow['Sections'] . ")", "Report_Sections");

        echo "<p>$from shared a Report configuration with you. Some details are listed below:</p>";
        echo "<table cellspacing=0>";
        echo "<tr><th class=top colspan=2>Section Details</th></tr>";
        echo "<tr><th class=left>Name </th><td>$name</td></tr>";
        echo "<tr><th class=left>Description</th><td>$desc</td></tr>";
        //echo "<tr><th class=left>Collection</th><td>$collection</td></tr>";
        echo "<tr><th class=left style='vertical-align:top'>Included Sections</th><td style='vertical-align:top'>";
        foreach ($srows as $srow) {
            echo " - " . $srow['Name'] . "<br/>";
        }
        echo "<tr><td colspan=2>&nbsp;</td></tr>";
        echo "<tr><th class=top colspan=2>Permission Settings</th></tr>";
        echo "<tr><th class=left>Edit</th><td>$rw</td></tr>";
        echo "<tr><th class=left>Sharing</th><td>$share</td></tr>";
        echo "</table></p>";
        echo "<p>You can now generate reports based on this template.</p>";
        echo "</div>";
    }
}
