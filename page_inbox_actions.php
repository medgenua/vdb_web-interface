<?php

if ($loggedin != 1) {
    include('login.php');
    exit();
}

// VARIABLES FOR LAYOUT
$readicon = array('<img src=Images/layout/mailred.png>', '<img src=Images/layout/mailgrey.png>');
$inh = array(0 => 'ND', 1 => 'P', 2 => 'M', 3 => 'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$gts = array("0" => "Homozygous Reference", "1" => "Heterozygous", "2" => "Homozygous Alternate", "1,2" => "Any Alternate", "0,1,2" => "All Genotypes");
$classes = array(0 => '-', 1 => 'Pathogenic', 2 => 'UVKL4', 3 => 'UVKL3', 4 => 'UVKL2', 5 => 'Benign', 6 => 'False Positive');
$inhm = array('0' => '-', '1' => 'Dominant', '2' => 'Recessive', '3', 'Unknown');

# CHROM HASH
for ($i = 1; $i <= 22; $i += 1) {
    $chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
if (array_key_exists("t", $_GET)) {
    $type = $_GET['t'];
} else {
    $type = 'classifier';
}


// classifier actions.
if ($type = 'classifier') {
    // specific variant provided? 
    $get_vid = (isset($_GET['vid']) ? $_GET['vid'] : 0);
    // check access to Selected classifier 
    $cid = $_GET['c'];
    $row = array_shift(...[runQuery("SELECT uc.cid,c.name,uc.can_validate,uc.can_remove FROM `Users_x_Classifiers` uc JOIN `Classifiers` c ON c.id = uc.cid WHERE uc.cid = '$cid' AND uc.uid = '$userid' ")]);
    if (empty($row)) {
        echo "<div class=section>";
        echo "<h3>Access denied</h3>";
        echo "<p>You don't have sufficient permissions to access this classifier.</p>";
        echo "</p>";
        echo "</div>";
        exit;
    }

    $classifier_name = $row['name'];
    $can_validate = $row['can_validate'];
    $can_remove = $row['can_remove'];
    ///////////////////////////////////
    // TOP TABLE : VALIDATE VARIANTS //
    ///////////////////////////////////
    if ($can_validate == 1) {
        // Get variants to validate.
        $v = runQuery("SELECT cv.vid, cv.added_on, cv.genotype,cv.class, cv.inheritance_mode, cv.comments , u.LastName, u.FirstName, v.chr,v.start,v.RefAllele,v.AltAllele,cv.sid, cv.cbs FROM `Classifiers_x_Variants` cv JOIN `Users` u JOIN `Variants` v ON cv.vid = v.id AND cv.added_by = u.id WHERE cid = '$cid' AND validate_reject = 0 ORDER BY v.chr,v.start", "Classifiers_x_Variants:Users:Variants");
        // add to table.
        $out = '';
        $nr = 0;
        foreach ($v as $key => $row) {
            $vid = $row['vid'];
            $var = "chr" . $chromhash[$row['chr']] . ":" . $row['start'] . " : " . $row['RefAllele'] . "/" . $row['AltAllele'];
            $added_by = $row['LastName'] . " " . substr($row['FirstName'], 0, 1) . ".";
            $added_on = $row['added_on'];
            $genotype = $gts[$row['genotype']];
            $class = $classes[$row['class']];
            $inh_mode = $inhm[$row['inheritance_mode']];
            $comments = $row['comments'];
            $sid = $row['sid'];
            $cbs = $row['cbs'];
            // sample details.
            $s = array_shift(...[runQuery("SELECT s.Name AS sname, p.Name AS pname FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects` p ON s.id = ps.sid AND p.id = ps.pid WHERE s.id = '$sid'")]);
            if (!$s) {
                //if (count($s) == 0) {
                $sname = '-';
                $pname = '';
            } else {
                $sname = $s['sname'];
                $pname = "(" . $s['pname'] . ")";
            }
            // settings depending on provided variantID
            if ($get_vid == $vid) {
                $display = '';
                $onClick = "collapseVar('$vid','n')";
                $info_style = "style='text-align:left;border:1px solid #aeaeae;'";
                ob_start();
                include("ajax_queries/LoadVariantAnnotations.php");
                $info_content = ob_get_clean();
                $span_class = "class='strong red'";
                $get_vid = 0; // reset, so shown only once (in case of $_GET['a'] == 1)

            } else {
                $display = "style='display:none'";
                $info_style = "style='text-align:left;'";
                $info_content = "<img src='Images/layout/ajax-loader.gif' style='height:3em' /><br/>Loading annotations... ";
                $span_class = "class='strong'";
                $onClick = "expandVar('$vid','$userid','n')";
            }
            // general info
            $out .= "<tr class=vrow id='general_$vid' onmouseover=\"this.style.cursor='pointer'\"  onClick=\"$onClick\" >";
            $out .= "<td><span id='varpos_$vid' $span_class>$var</span></td><td>$genotype</td><td>$class</td><td>$inh_mode</td><td>$sname <span class=italic>$pname</span></td></tr>";
            // details && fields
            $out .= "<tr id='details_$vid' $display><td colspan=2 style='padding-left:2em;padding-bottom:1em;'>";
            if ($cbs != '') {
                $cbs = stripslashes($cbs);
                //$cbs = preg_replace("/(NoteBox[\"']*)/", "$1"." disabled",$cbs);
                $out .= "<br/><table cellspacing=0 class='cb_vc' id='cb_vc_$cid" . "_$vid'>$cbs</table><br/>";
            }
            $out .= "<span class=emph>Added By:</span> $added_by on $added_on<br/>";
            $out .= "<span class=emph>Comments:</span> <span class=italic>(edit as needed)</span> <br/> &nbsp; <textarea style='padding-left:1em;' id='comments_$vid' rows=3 cols=50>$comments</textarea><br/>";
            $out .= "<span class=emph>Action:</span> <input type=radio id='a_$vid.a' name='action_$vid' value=1>Accept &nbsp; <input type=radio id='a_$vid.r' name='action_$vid' value=0>Reject &nbsp; <input type=radio id='a_$vid.u' name='action_$vid' value=2>Undecided <br/>";
            $out .= " &nbsp; <button value='Submit' style='margin-top:0.5em;' onclick=\"ApproveClassifier('$vid','$userid','$sid')\">Submit</button> &nbsp; <button value='Hide' onclick=\"collapseVar('$vid','n')\">Hide</button>";
            $out .= "</td>";
            // some links to more details.
            $out .= "<td colspan=3 id='anno_$vid' $info_style>$info_content</td>";
            $out .= "</tr>";
            $nr++;
        }
        if ($out != '') {
            echo "<div class=section>";
            echo "<h3> Pending Validation of  $nr variants in classifier '$classifier_name'</h3>";
            echo "<input type=hidden id='classifier_id' value='$cid'/>";

            echo "<p><table cellspacing=0 class=w100>";
            echo "<tr><th class=top>Variant</th><th class=top>Seen as Genotype</th><th class=top>Applied Class</th><th class=top>Applied Inheritance Mode</th><th class=top>Source Sample</th></tr>";
            echo $out;
            echo "</tr></table></p></div>";
        } else {
            echo "<div class=section>";
            echo "<h3> Pending Validation of 0 variants in classifier '$classifier_name'</h3>";
            echo "<p>No variants to validate.</p>";
        }
    }
    $out = '';
    ////////////////////////////////////////////////////
    // BOTTOM TABLE : LIST ALL VARIANTS IN CLASSIFIER //
    ////////////////////////////////////////////////////
    // if 'list_all' : this is called from the classifier management page.
    if (isset($_GET['a']) && $_GET['a'] == 1) {
        // subset : batches of 250 variants.
        if (isset($_GET['sp'])) {
            $sp = $_GET['sp'];
        } else {
            $sp = 1;
        }
        $offset = ($sp - 1) * 250;
        $msg = "Variants " . (($sp - 1) * 250 + 1) . " to " . ($sp * 250);
        // nrVars
        $nrVars = array_shift(...[runQuery("SELECT COUNT(1) AS nrVars FROM `Classifiers_x_Variants` WHERE cid = '$cid'", "Classifiers_x_Variants")])['nrVars'];
        // variant details
        $v = runQuery("SELECT cv.vid, cv.added_on, cv.validate_reject,cv.validated_by,cv.validated_on,cv.genotype,cv.class, cv.inheritance_mode, cv.comments , u.LastName, u.FirstName, v.chr,v.start,v.RefAllele,v.AltAllele,cv.cbs FROM `Classifiers_x_Variants` cv JOIN `Users` u JOIN `Variants` v ON cv.vid = v.id AND cv.added_by = u.id WHERE cid = '$cid' ORDER BY v.chr,v.start LIMIT $offset,250", "Classifiers_x_Variants:Users:Variants");
        // add to table.
        $nr = 0;
        $vstats = array("v-1" => "Rejected", "v0" => "Undecided", "v1" => "Accepted");
        foreach ($v as $key => $row) {
            $vid = $row['vid'];
            // limit max allele lenght
            if (strlen($row['RefAllele']) > 20) {
                $ref_t = "title='Ref:" . $row['RefAllele'] . "'";
                $ref_allele = substr($row['RefAllele'], 0, 20) . '...';
            } else {
                $ref_allele = $row['RefAllele'];
                $ref_t = "";
            }
            if (strlen($row['AltAllele']) > 20) {
                $alt_t = "title='Alt:" . $row['AltAllele'] . "'";
                $alt_allele = substr($row['AltAllele'], 0, 20) . '...';
            } else {
                $alt_allele = $row['AltAllele'];
                $alt_t = "";
            }

            $var = "chr" . $chromhash[$row['chr']] . ":" . $row['start'] . " : <span $ref_t>" . $ref_allele . "</span>/<span $alt_t>" . $alt_allele . "</span>";
            $var_js = "chr" . $chromhash[$row['chr']] . ":" . $row['start'] . " : " . $row['RefAllele'] . "/" . $row['AltAllele'];
            $added_by = $row['LastName'] . " " . substr($row['FirstName'], 0, 1) . ".";
            $added_on = $row['added_on'];
            $genotype = $gts[$row['genotype']];
            $class = $classes[$row['class']];
            $inh_mode = $inhm[$row['inheritance_mode']];
            $comments = $row['comments'];
            $validation_status = $row['validate_reject'];
            $validated_by = $row['validated_by'];
            $validated_on = $row['validated_on'];
            $cbs = $row['cbs'];
            if ($validated_by != 0) {
                $sv = runQuery("SELECT LastName,FirstName FROM `Users` WHERE id = '$validated_by'", "Users");
                $validated_by = $sv[0]['LastName'] . " " . substr($sv[0]['FirstName'], 0, 1) . ".";
            } else {
                $validated_by = '-';
                $validated_on = '-';
            }
            if ($validation_status == 0 && $can_validate == 1) {
                continue; // these are shown above
            }
            $vstat = $vstats["v" . $validation_status];
            //actions 
            $actions = '&nbsp;';
            if ($can_validate == 1) {
                $actions .= "<image onmouseover=\"this.style.cursor='pointer'\" title='Undo Status' src='Images/layout/undo.png' style='height:1.2em' onClick=\"ResetValidation('$cid','$vid','$userid')\" />";
            }
            if ($can_remove = 1) {
                $actions .= "<image onmouseover=\"this.style.cursor='pointer'\" title='Remove Variant From Classifier' src='Images/layout/icon_trash.gif' style='height:1.2em' onClick=\"DeleteVariant('$cid','$vid','$userid','$var_js')\" />";
            }
            // settings depending on provided variantID
            if ($get_vid == $vid) {
                $display = '';
                $onClick = "collapseVar('$vid','s')";
                $info_style = "style='text-align:left;border:1px solid #aeaeae;'";
                ob_start();
                include("ajax_queries/LoadVariantAnnotations.php");
                $info_content = ob_get_clean();
                $span_class = "class='strong red'";
                $get_vid = 0; // reset, so shown only once (in case of $_GET['a'] == 1)

            } else {
                $display = "style='display:none'";
                $info_style = "style='text-align:left;'";
                $span_class = "class='strong'";
                $info_content = "<img src='Images/layout/ajax-loader.gif' style='height:3em' /><br/>Loading annotations... ";
                $onClick = "expandVar('$vid','$userid','s')";
            }
            // general
            $out .= "<tr class=vrow id='general_$vid' >";
            $out .= "<td id='var_$vid' onmouseover=\"this.style.cursor='pointer'\"  onClick=\"$onClick\"><span id='varpos_$vid' $span_class>$var</span></td><td>$genotype</td><td>$class</td><td>$inh_mode</td><td>$vstat</td><td>$actions</td></tr>";
            // details && fields

            $out .= "<tr id='details_$vid' $display><td colspan=2 style='padding-left:2em;padding-bottom:1em;'>";
            if ($cbs != '') {
                $cbs = stripslashes($cbs);
                $cbs = preg_replace("/(NoteBox[\"']*)/", "$1" . " disabled", $cbs);
                $out .= "<br/><table cellspacing=0 class=cb_vc>$cbs</table><br/>";
            }
            $out .= "<span class=emph>Added By:</span> $added_by <span class=italic>on</span> $added_on<br/>";
            $out .= "<span class=emph>Status Set By:</span> $validated_by <span class=italic>on</span> $validated_on<br/>";
            $out .= "<span class=emph>Comments:</span>&nbsp; $comments<br/>";


            $out .= "</td>";
            // some links to more details.
            $out .= "<td colspan=4 id='anno_$vid' $info_style>$info_content</td>";
            $out .= "</tr>";
            $nr++;
        }
    }
    if ($out != '') {
        echo "<div class=section>";
        echo "<h3>Variants in classifier '$classifier_name' : $msg</h3>";
        // page selection.
        if ($nrVars > 250) {
            echo "<p><span class=emph>Select Page:</span> <select id='sp' onChange='LoadVariantPage()'>";
            $nr_pages = floor(($nrVars - 1) / 250) + 1;
            foreach (range(1, $nr_pages) as $pagenr) {
                $selected = ($pagenr == $sp) ? 'SELECTED' : '';
                echo "<option value='$pagenr' $selected>Variant " . (($pagenr - 1) * 250 + 1) . "-" . (($pagenr) * 250) . "</option>";
            }
            echo "</select></p>";
        }
        if ($can_validate == 1) {
            echo "<p>Variants with pending validation shown in the table above.</p>";
        }
        echo "<p><table cellspacing=0 class=w100>";
        echo "<tr><th class=top>Variant</th><th class=top>Seen as Genotype</th><th class=top>Applied Class</th><th class=top>Applied Inheritance Mode</th><th class=top>Status</th><th class=top>Actions</th></tr>";
        echo $out;
        echo "</tr></table></p></div>";
    } elseif (isset($_GET['a']) && $_GET['a'] == 1) {
        echo "<div class=section>";
        echo "<h3>Variants in classifier '$classifier_name'</h3>";
        echo "<p>No variants found.</p>";
    }
}
