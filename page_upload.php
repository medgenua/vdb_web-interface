<?php
if ($loggedin != 1) {
    include('page_login.php');
    exit;
}
if (array_key_exists('Archived', $_SESSION) && $_SESSION['Archived'] == 1) {
    echo "<div class=section><h3>Upload Disabled For Archived Builds</h3>";
    echo "<p>It is not possible to add data to this genome build. Please switch back to the current genome build to import new data</p>";
    echo "</div>";
    exit;
}
//////////////////////////
// FORM TO LOAD SAMPLES //
//////////////////////////
echo "<div class=section>";
echo "<h3>Add data from FTP upload</h3>";
// check for the sha1 password
$row = runQuery("SELECT passwd_sha1 FROM `Users` WHERE id = '$userid'", "Users")[0];
if ($row['passwd_sha1'] != '') {
    // check for files in ftp-dir
    //$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username";
    $ftpdir = $config['FTP_DATA'] . "/$username";
    $Files = array();
    $VCFs = array();
    //$BAMs = array();
    function TraverseDir($dir)
    {
        $It =  opendir("$dir");
        if ($It) {
            #echo "Traversing $dir<br/>";
            while ($Filename = readdir($It)) {
                if ($Filename == '.' || $Filename == '..') {
                    continue;
                }
                //$LastModified = filemtime($dir . $Filename);
                if (substr($Filename, -3) == "vcf" || substr($Filename, -3) == "bcf") {
                    return 1;
                } elseif (is_dir("$dir/$Filename")) {
                    $result = TraverseDir("$dir/$Filename");
                    if ($result == 1) {
                        return 1;
                    }
                }
            }
            return 0;
        } else {
            return 0;
        }
    }
    $VCFpresent = TraverseDir($ftpdir);
    //sort($VCFs);
    //sort($BAMs);
    #$num = count($VCFs) - 1 ;
    #$numbam = count($BAMs) - 1;
    #if (count($VCFs) == 0) {
    if ($VCFpresent != 1) {
        echo "<p>No VCF/BCF files present. (Make sure to add public-read access on your ftp files!) </p>";
    } else {

        $out = "<form action='cgi-bin/galaxy_communication.cgi' method=POST>";
        // more samples are added using appendChild to this div
        $out .= "<div id='sampleselects'>";
        $out .= "</div>";
        $out .= "<input type=hidden id=nrsample value=0>";
        $out .= "<input type=hidden name=uid id=uid value=$userid>";
        $out .= "<input type=hidden name=LocalUpload value=1>";
        $out .= "<p> <input class=button type=submit value='Load Samples Into Database' name='LoadSamplesFromLocal'></form>";
        $out .= "<input type=submit class=button onClick=\"AddSampleField('$userid')\" value='Load More Samples'>";
        $out .= "</p>";
        echo $out;
    }
}

echo "</div>";

//////////////////
// INSTRUCTIONS //
//////////////////
echo "<div class=section>";
echo "<h3>How to use the FTP server</h3>";
// Check if the FTP configuration is active
if (array_key_exists('FTP', $config) && $config['FTP'] == 1) {
    // was the form submitted ? 
    if (array_key_exists('activate', $_POST)) {
        // check if password is correct.
        $row = runQuery("SELECT password FROM `Users` WHERE id = '$userid'", 'Users')[0];
        if (MD5($_POST['passwd']) != $row['password']) {
            // wrong password. 
            echo "<p><span class=emph style='color:red'>Wrong password provided: </span>Please try again.</p>";
        } else {
            // update mysql table
            $passwd = $_POST['passwd'];
            $sha1 = SHA1($passwd);
            doQuery("UPDATE `Users` SET passwd_sha1 = '$sha1' WHERE id = '$userid'", "Users");
            // disable memcache keys.
            //clearMemcache("Users");
            // create ftp location
            //if (! is_dir("/home/".$config['SCRIPTUSER']."/ftp-data/")) {
            if (!is_dir($config['FTP_DATA'])) {
                //$ftpdir = "/home/".$config['SCRIPTUSER']."/ftp-data/";
                $ftpdir = $config['FTP_DATA'];
                $command = " ( COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && mkdir '$ftpdir' && chmod 755 '$ftpdir'\"  ) ";
                system($command);
            }
            //if (! is_dir("/home/".$config['SCRIPTUSER']."/ftp-data/$username")) {
            if (!is_dir($config['FTP_DATA'] . "/$username")) {
                //$ftpdir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username";
                $ftpdir = $config['FTP_DATA'] . "/$username";
                $command = " ( COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && mkdir '$ftpdir' && chmod 755 '$ftpdir'\"  ) ";
                system($command);
            }
        }
    }
    // check for the sha1 password
    $row = runQuery("SELECT passwd_sha1 FROM `Users` WHERE id = '$userid'", "Users")[0];
    if ($row['passwd_sha1'] == '') {
        // no sha1 passwd => needs to activate FTP access
        echo "<p><span class=emph>Activate your FTP access:</span></p>";
        echo "<p> Using FTP will allow you to upload data to VariantDB, without access to a Galaxy server. After providing your password to activate access to the VariantDB FTP account, login details will be provided.</p>";
        echo "<form METHOD='POST' action='index.php?page=upload'>";
        echo "<p>Password: <input name='passwd' type='password' size=20></p>";
        echo "<p><input type=submit class=button name=activate></p>";
        echo "</form>";
    } else {
        // were there files submitted to delete/unpack
        if (array_key_exists('process', $_POST)) {
            echo "<p><span class=emph>Processing file changes</span></p>";
            echo "<p><ol>";
            flush();
            // delete?
            //$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username";
            #$dir = $config['SCRIPTDIR'] . "/../ftp-data/";
            if (array_key_exists('delete', $_POST)) {
                foreach ($_POST['delete'] as $key => $filename) {
                    echo "<li>Deleting $filename</li>";
                    flush();
                    $command = " ( COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $dir && rm -f '$filename' \"  ) ";
                    system($command);
                }
            }
            // unpack
            if (array_key_exists('unpack', $_POST)) {
                foreach ($_POST['unpack'] as $key => $filename) {
                    echo "<li>Unpacking $filename<br/>";
                    flush();
                    $command = " ( COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $dir && $maintenancedir/FTP_unpack/ftp_unpack.pl '$filename' \"  ) ";
                    #$output = shell_exec($command);
                    #echo "<textarea cols=50 rows=5>$output</textarea></li>";
                    echo "todo: activate unpacking</li>";
                    flush();
                }
            }
            echo "</ol></p>";
        }
        // print instructions to log in
        echo "<p><span class=emph>VariantDB FTP details</span></p>";
        echo "<p><ul style='padding-left:1em;'>";
        echo "<li><span class=underline>Server:</span> " . $config['FTP_HOST'] . "</li>";
        echo "<li><span class=underline>Port:</span> " . $config['FTP_PORT'] . "</li>";
        echo "<li><span class=underline>username:</span> $username</li>";
        echo "<li><span class=underline>Password:</span> Your VariantDB Password</li>";
        echo " </ul></p>";
        echo "<p><span class=emph>Remarks:</span>";
        echo "<ul style='padding-left:1em;'>";
        echo "<li> The allowed file types are .bam, .vcf, .bcf, .rar, .zip and (tar).gz.</li>";
        //echo "<li> All files must be in the root folder. Files in subdirectories can not be imported!</li>";
        echo "<li> If you upload compressed files, come back here after upload to unpack them. (not active yet!)</li>";
        echo "<li> Permissions on the FTP files should be at least r-xr-xr-x for both files AND folders to enable import. If you get permission errors, check them using an FTP client.</li>";
        if ($config['FTP_HOST'] == '143.169.238.104') {
            echo "<li><span class=emph style='color:red'>NOTE:</span> This VariantDB servers shares its FTP server with the Biomina Galaxy server, and takes user/pass combinations from the galaxy database. <br/>Hence, you need to create an account with the <span class=emph style='red'>exact same username and password</span> at <a href='http://143.169.238.104/galaxy/'>Our Galaxy server</a> to access the FTP server</li>";
        }
        echo "</ul>";
        echo "</p>";
        // list files uploaded by the current user
        echo "<p><span class=emph>Uploaded Files</p>";
        //$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username";
        $dir = $config['FTP_DATA'] . "/$username";
        $Files = array();
        $It =  opendir($dir);
        if ($It) {
            while ($Filename = readdir($It)) {
                if ($Filename == '.' || $Filename == '..')
                    continue;
                //$LastModified = filemtime($dir . $Filename);
                $Files[] = $Filename;
            }
        }
        sort($Files);
        $num = count($Files) - 1;
        if ($num == 0) {
            echo "<p>No files uploaded. </p>";
        } else {
            echo "<p><form action='index.php?page=upload' method=POST>";
            echo "<table cellspacing=0 class=w100>";
            echo "<tr><th class=top>File</th><th class=top>Delete</th><th class=top>Unpack</th></tr>";
            for ($i = 0; $i <= $num; $i += 1) {
                //$filename = substr($Files[$i], (strlen($userid)+1));
                $filename = $Files[$i];
                // check mime-type
                $mime = shell_exec("cd $dir && file --mime-type -b '$filename'");
                $mime = rtrim($mime);
                // all non-text/plain that are allowed need to be unpacked
                $unpack = '';
                if ($mime != 'text/plain') {
                    if (substr($filename, -3) != "bam" && substr($filename, -3) != "bcf") {
                        $unpack = "<input type=checkbox name='unpack[]' value='$filename'> ($mime)";
                    }
                }
                echo "<tr><td >$filename</td><td><input type=checkbox name='delete[]' value='$filename'></td><td>$unpack&nbsp;</td></tr>\n";
            }
            echo "<tr><td class=last colspan=3> &nbsp;</td></tr>";
            echo "</table></p>";
            echo "<p><input class=button type=submit value='Process' name='process'></form></p>";
        }
    }
}
// else, print notification and tutorial (if admin)
else {
    if ($level == 3) {
        echo "<p>VariantDB supports the uploading of datafiles using FTP. To use this feature, perform the following configuration steps:\n";
        echo "<ol>\n";
        echo "<li>Install an ftp server with mysql support. The host should have access to both the VariantDB mysql database and the subdir 'ftp-data' in the homedirectory of the VariantDB script user (/home/" . $config['SCRIPTUSER'] . ")</li>";
        echo "<li>The following example steps are for proftpd, with the modules 'sql, sql-mysql AND sql-mysql-password' activated (this requires version >=  1.3.3d-8)</li>"; // and sql-passwd'</li>\n";
        // load modules
        echo "<li>Load The needed modules in /etc/proftpd/modules.conf<ul style='padding-left:1em;'>";
        echo "<li>LoadModule mod_sql.c</li>";
        echo "<li>LoadModule mod_sql_passwd.c</li>";
        echo "<li>LoadModule mod_sql_mysql.c</li>";
        echo "</ul></li>";
        // configure mysql access
        echo "<li>Set the following items in the /etc/proftpd/proftpd.conf file.<ul style='padding-left:1em'>";
        echo "<li>#Jail users to their homes</li>";
        echo "<li>DefaultRoot &nbsp;&nbsp;&nbsp;~</li>";
        echo "<li># Create home directories as needed, using chmod 755</li>";
        echo "<li># chmod 755 is set to make them www-user readable. Adapt to use groups</li>";
        echo "<li>CreateHome on 755 dirmode 755</li>";
        echo "<li># Set up mod_sql_password - VariantDB passwords are stored as sha1-encoded </li>";
        echo "<li>SQLPasswordEngine &nbsp;&nbsp;&nbsp; on</li>";
        echo "<li>SQLPasswordEncoding &nbsp;&nbsp;&nbsp;   hex </li>";
        echo "<li># Set up mod_sql to authenticate against the VariantDB database</li>";
        echo "<li>SQLEngine&nbsp;&nbsp;&nbsp;on</li>";
        echo "<li>AuthOrder&nbsp;&nbsp;&nbsp;&nbsp;mod_sql.c</li>";
        echo "<li>SQLBackend&nbsp;&nbsp;&nbsp;     mysql</li>";
        echo "<li># copy DBHOST, DBUSER and DBPASS from .credentials file !</li>";
        echo "<li># adapt 'NGS-Variants" . $_SESSION['dbname'] . "' after a Genome Build update</li>";
        echo "<li>SQLConnectInfo&nbsp;&nbsp;&nbsp;NGS-Variants" . $_SESSION['dbname'] . "@DBHOST DBUSER DBPASS </li>";
        echo "<li>SQLAuthTypes &nbsp;&nbsp;&nbsp;  SHA1</li>";
        echo "<li>SQLAuthenticate&nbsp;&nbsp;&nbsp; users</li>";
        echo "<li># An empty directory in case chroot fails</li>";
        echo "<li>SQLDefaultHomedir &nbsp;&nbsp;&nbsp; /tmp</li>";
        echo "<li># Define a custom query for lookup that returns a passwd-like entry.  UID (here 1002) and GID (here 1003) should match your VariantDB Script user (user holding the program tree in its homedir).</li>";
        echo "<li>SQLUserInfo&nbsp;&nbsp;&nbsp; custom:/LookupVariantDbUser</li>";
        echo "<li>#SQLNamedQuery&nbsp;&nbsp;&nbsp;  LookupVariantDbUser SELECT \"email,passwd_sha1,'1002','1003','/home/scriptuser/ftp-data/%U','/bin/bash' FROM `Users` WHERE email='%U'\"</li>";
        echo "</ul></li>";
        // set .credentials
        echo "<li>Add the following lines to '$file':<ul style='padding-left:1em;'>";
        echo "<li>FTP=1</li>";
        echo "<li>FTP_HOST=YOUR FTP SERVER IP/NAME</li>";
        echo "<li>FTP_PORT=YOUR FTP SERVER PORT</li>";
        echo "<li>FTP_DATA=/path/to/the/ftp/data/base/dir</li></ul>";
        echo "</ol></p>";

        echo "<p>FTP access is provided on a per user basis. Each user will have to activate access by providing their password here.</p>";
    } else {
        echo "<p>VariantDB supports the uploading of datafiles using FTP, but this feature is not activated yet. Ask your system administrator to perform the additional configuration. If a VariantDB Administrator visits this page, the configuration steps will be shown here. </p>";
    }
}

?>

</div>

<div class=section>
    <h3>Add data from Galaxy</h3>
    <p>VariantDB supports direct export from a Galaxy server using our dedicated export tool. The tool is installed on the Galaxy server of the Department of Medical Genetics of the University of Antwerp, accessible at <a href='http://143.169.238.104/galaxy'>http://143.169.238.104/galaxy</a>. </p>

    <p>You can ask your system administrator to install the tool on a local Galaxy instance if needed.</p>
</div>