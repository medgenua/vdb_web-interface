<?php
if (!isset($loggedin) || $loggedin != 1) {
    $key = $_GET['key'];
    $_SESSION['linkkey'] = $key;

    include('page_login.php');
    exit;
} elseif (!isset($_POST['approve']) && !isset($_POST['deny'])) {
    $ok = 1;
    if (isset($_GET['key'])) {
        $key = $_GET['key'];
    } elseif (isset($_SESSION['linkkey'])) {
        $key = $_SESSION['linkkey'];
    } else {
        echo "no key found. page will not function !";
        $ok = 0;
    }
    // get request details
    $row = runQuery("SELECT uid, gid, motivation FROM `Usergroups_requests` WHERE linkkey = '$key'", "Usergroups_requests")[0];
    $uid = $row['uid'];
    $gid = $row['gid'];
    $motivation = $row['motivation'];
    // get requesting user details
    $row = runQuery("SELECT u.LastName, u.FirstName, a.name FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id WHERE u.id = '$uid'", "Users:Affiliation")[0];
    $reqfname = $row['FirstName'];
    $reqlname = $row['LastName'];
    $reqinst = $row['name'];
    // get usergroup details
    $row = runQuery("SELECT name, description, administrator FROM `Usergroups` WHERE id = '$gid'", "Usergroups")[0];
    $groupname = $row['name'];
    $groupdescr = $row['description'];
    $groupadmin = $row['administrator'];
    if ($groupadmin != $userid) {
        echo "You are not the administrator for this usergroup. You are not allowed to process this usergroup request.";
        $ok = 0;
    }
    if ($ok == 1) {
        // print the form
        echo "<div class=section>\n";
        echo "<h3>Usergroup Access Requested</h3>\n";
        echo "<form action='index.php?page=usergrouprequest' method=POST>\n";
        echo "<input type=hidden name=key value='$key'>\n";
        echo "<p>\n";
        echo "<table cellspacing=0>\n";
        echo "<tr>\n";
        echo "<th colspan=2 class=top>Requesting User</th>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<th >User</th>\n";
        echo "<td>$reqfname $reflname</td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<th >Institute</th>\n";
        echo "<td>$reqinst</td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<th colspan=2 class=top>Requested Usergroup</th>\n";
        echo "</tr>\n";
        echo "<th >Group Name</th>\n";
        echo "<td>$groupname</td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<th >Group Info</th>\n";
        echo "<td>$groupdescr</td>\n";
        echo "</tr>\n";
        if ($motivation != '') {
            echo "<tr>\n";
            echo "<th >Motivation</th>\n";
            echo "<td>$motivation</td>\n";
        }
        echo "<tr><td class=last colspan=2>&nbsp;</td></tr>";
        echo "</table>\n";
        echo "</p><p>\n";
        echo "<input type=submit class=button value=Approve name=approve>&nbsp;&nbsp;&nbsp;<input type=submit class=button value=Deny name=deny></p>\n";
        echo "</div>\n";
    }
} elseif (isset($_POST['approve'])) {
    // get details
    $ok = 1;
    $key = $_POST['key'];
    // get request details
    $row = runQuery("SELECT uid, gid, motivation FROM `Usergroups_requests` WHERE linkkey = '$key'", "Usergroups_requests")[0];
    $uid = $row['uid'];
    $gid = $row['gid'];
    $motivation = $row['motivation'];
    // get requesting user details
    $row = runQuery("SELECT u.LastName, u.FirstName, a.name, u.email FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id WHERE u.id = '$uid'", "Users:Affiliation")[0];
    $reqfname = $row['FirstName'];
    $reqlname = $row['LastName'];
    $reqinst = $row['name'];
    $email = $row['email'];
    // get usergroup details
    $row = runQuery("SELECT name, description, administrator,editvariant,editsample, editclinic FROM Usergroups WHERE id = '$gid'", "Usergroups")[0];
    $groupname = $row['name'];
    $groupdescr = $row['description'];
    $groupadmin = $row['administrator'];
    $editvariant = $row['editvariant'];
    $editsample = $row['editsample'];
    $editclinic = $row['editclinic'];
    if ($groupadmin != $userid) {
        echo "You are not the administrator for this usergroup. You are not allowed to process this usergroup request.";
        $ok = 0;
    }
    if ($ok == 1) {
        // join the usergroup
        doQuery("INSERT INTO `Usergroups_x_User` (uid, gid) VALUES ('$uid','$gid') ON DUPLICATE KEY UPDATE uid = '$uid'", "Usergroups_x_User");
        // update permissions on projects shared with groups. 
        $rows = runQuery("SELECT pid FROM `Projects_x_Usergroups` WHERE gid = '$gid'", "Projects_x_Usergroups");
        $projects = array();
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $pid = $row['pid'];
                $projects[$pid] = 1;
                // has permission?
                $arow = array_shift(...[runQuery("SELECT editvariant,editclinic,editsample FROM `Projects_x_Users` WHERE `pid` = '$pid' AND `uid` = '$uid'", "Projects_x_Users")]);
                if (is_array($arow) && count($arow) > 0) {
                    $edit = max($arow['editvariant'], $editvariant);
                    $clin = max($arow['editclinic'], $editclinic);
                    $shar = max($arow['editsample'], $editsample);
                    doQuery("UPDATE `Projects_x_Users` SET `editvariant` = '$edit', `editclinic` = '$clin', `editsample` = '$shar' WHERE `uid` = '$uid' AND `pid` = '$pid'"); // no impact on summary tables.
                } else {
                    // project for user ; reset SummaryStatus & insert permissions.
                    doQuery("INSERT INTO `Projects_x_Users` (pid,uid,editvariant,editclinic,editsample) VALUES ('$pid','$uid', '$editvariant', '$editclinic','$editsample')", "Projects_x_Users");
                    doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$uid'", "Users:Variants_x_Users_Summary:Summary");
                }
            }
        }
        // update permissions on the classifiers
        $rows = runQuery("SELECT cid, can_use,can_add,can_validate,can_remove,can_share FROM `Usergroups_x_Classifiers` WHERE gid = '$gid'", "Usergroups_x_Classifiers");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $cid = $row['cid'];
                // has access ? 
                $arow = array_shift(...[runQuery("SELECT can_use,can_add,can_validate,can_remove,can_share FROM `Users_x_Classifiers` WHERE cid = '$cid' AND uid = '$uid'", "Users_x_Classifiers")]);
                // has access.
                if (count($arow) > 0) {
                    $use = max($arow['can_use'], $row['can_use']);
                    $add = max($arow['can_add'], $row['can_add']);
                    $val = max($arow['can_validate'], $row['can_validate']);
                    $del = max($arow['can_remove'], $row['can_remove']);
                    $sha = max($arow['can_share'], $row['can_share']);
                    doQuery("UPDATE `Users_x_Classifiers` SET `can_use` = '$use', `can_add` = '$add', `can_validate` = '$val', `can_remove` = '$del', `can_share` = '$sha' WHERE `uid` = '$uid' AND `cid` = '$cid'", "Users_x_Classifiers");
                }
                // new access
                else {
                    $use = $row['can_use'];
                    $add = $row['can_add'];
                    $val = $row['can_validate'];
                    $del = $row['can_remove'];
                    $sha = $row['can_share'];

                    doQuery("INSERT INTO `Users_x_Classifiers` (uid,cid,can_use,can_add,can_validate,can_remove,can_share,apply_on_import) VALUES ('$uid','$cid','$use','$add','$val','$del','$sha','0')", "Users_x_Classifiers");
                }
            }
        }
        // update permissions on the reports.
        $rows = runQuery("SELECT rid, edit,full FROM `Usergroups_x_Report_Definitions` WHERE gid = '$gid'", "Usergroups_x_Report_Definitions");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $rid = $row['rid'];
                // has access ? 
                $arow = array_shift(...[runQuery("SELECT edit,full FROM `Users_x_Report_Definitions` WHERE rid = '$rid' AND uid = '$uid'", "Users_x_Report_Definitions")]);
                // has access.
                if (count($arow) > 0) {
                    $edit = max($arow['edit'], $row['edit']);
                    $full = max($arow['full'], $row['full']);
                    doQuery("UPDATE `Users_x_Report_Definitions` SET `edit` = '$edit', `full` = '$full' WHERE `uid` = '$uid' AND `rid` = '$rid'", "Users_x_Report_Definitions");
                }
                // new access
                else {
                    $edit = $row['edit'];
                    $full = $row['full'];
                    doQuery("INSERT INTO `Users_x_Report_Definitions` (uid,rid,edit,full) VALUES ('$uid','$rid','$edit','$full')", "Users_x_Report_Definitions");
                }
            }
        }
        $rows = runQuery("SELECT rsid, edit,full FROM `Usergroups_x_Report_Sections` WHERE gid = '$gid'", "Usergroups_x_Report_Sections");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $rsid = $row['rsid'];
                // has access ? 
                $arow = array_shift(...[runQuery("SELECT edit,full FROM `Users_x_Report_Sections` WHERE rsid = '$rsid' AND uid = '$uid'", "Users_x_Report_Sections")]);
                // has access.
                if (count($arow) > 0) {
                    $edit = max($arow['edit'], $row['edit']);
                    $full = max($arow['full'], $row['full']);
                    doQuery("UPDATE `Users_x_Report_Sections` SET `edit` = '$edit', `full` = '$full' WHERE `uid` = '$uid' AND `rsid` = '$rsid'", "Users_x_Report_Sections");
                }
                // new access
                else {
                    $edit = $row['edit'];
                    $full = $row['full'];
                    doQuery("INSERT INTO `Users_x_Report_Sections` (uid,rsid,edit,full) VALUES ('$uid','$rsid','$edit','$full')", "Users_x_Report_Sections");
                }
            }
        }
        $rows = runQuery("SELECT cid, edit,full FROM `Usergroups_x_Report_Sections_CheckBox` WHERE gid = '$gid'", "Usergroups_x_Report_Sections_CheckBox");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $cid = $row['cid'];
                // has access ? 
                $arow = array_shift(...[runQuery("SELECT edit,full FROM `Users_x_Report_Sections_CheckBox` WHERE cid = '$cid' AND uid = '$uid'", "Users_x_Report_Sections_CheckBox")]);
                // has access.
                if (count($arow) > 0) {
                    $edit = max($arow['edit'], $row['edit']);
                    $full = max($arow['full'], $row['full']);
                    doQuery("UPDATE `Users_x_Report_Sections_CheckBox` SET `edit` = '$edit', `full` = '$full' WHERE `uid` = '$uid' AND `cid` = '$cid'", "Users_x_Report_Sections_CheckBox");
                }
                // new access
                else {
                    $edit = $row['edit'];
                    $full = $row['full'];
                    doQuery("INSERT INTO `Users_x_Report_Sections` (uid,cid,edit,full) VALUES ('$uid','$cid','$edit','$full')", "Users_x_Report_Sections_CheckBox");
                }
            }
        }
        // update permissions on filters & annotation sets.
        $rows = runQuery("SELECT fid FROM `Usergroups_x_Shared_Filters` WHERE gid = '$gid'", "Usergroups_x_Shared_Filters");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $fid = $row['fid'];
                doQuery("INSERT IGNORE INTO `Users_x_Shared_Filters` (uid,fid) VALUES ('$uid','$fid')", "Users_x_Shared_Filters");
            }
        }
        $rows = runQuery("SELECT aid FROM `Usergroups_x_Shared_Annotations` WHERE gid = '$gid'", "Usergroups_x_Shared_Annotations");
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $aid = $row['aid'];
                doQuery("INSERT IGNORE INTO `Users_x_Shared_Annotations` (uid,aid) VALUES ('$uid','$aid')", "Users_x_Shared_Annotations");
            }
        }
        // genepanels
        $rows = runQuery("SELECT gpid, rw,share FROM `GenePanels_x_Usergroups` WHERE gid = '$gid'", "GenePanels_x_Usergroups");
        foreach ($rows as $row) {
            $gpid = $row['gpid'];
            $rw = $row['rw'];
            $share = $row['share'];
            // has access ?
            $arow = array_shift(...[runQuery("SELECT rw,share FROM `GenePanels_x_Users` WHERE uid = '$uid' AND gpid = '$gpid'", "GenePanels_x_Users")]);
            if (count($arow) == 0) {
                doQuery("INSERT INTO `GenePanels_x_Users` (uid, gpid,rw,share) VALUES ('$uid','$gpid','$rw','$share')", "GenePanels_x_Users");
            } else {
                $rw = max($rw, $arow['rw']);
                $share = max($rw, $arow['share']);
                doQuery("UPDATE `GenePanels_x_Users` SET rw = '$rw', share = '$share' WHERE uid = '$uid' AND gpid = '$gpid'", "GenePanels_x_Users");
            }
        }
        // output.
        echo "<div class=section>\n";

        echo "<h3>Request processed: $groupname</h3>\n";
        echo "<p>$reqfname $reqlname is now a member of the group '$groupname'. When new items are shared with this group, the user will gain access to them.</p>\n";

        // send mail to user to notify
        $message = "Message sent from https://$domain\r";
        $message .= "Subject: VariantDB: Usergroup Request Approved\r\r";
        $message .= "Your request to join the '$groupname' usergroup at VariantDB was approved.\r\r";
        $message .= "\r\rThe VariantDB System-administrator\r\n";
        $headers = "From: no-reply@$domain\r\n";
        /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
        // step 4: send the email
        mail($email, "VariantDB: Usergroup Request Approved", $message, $headers);
        doQuery("DELETE FROM `Usergroups_requests` WHERE linkkey = '$key'", "Usergroups_requests");
    }
} elseif (isset($_POST['deny'])) {
    // get details
    $ok = 1;
    $key = $_POST['key'];
    // get request details
    $row = runQuery("SELECT uid, gid, motivation FROM `Usergroups_requests` WHERE linkkey = '$key'", "Usergroups_requests")[0];
    $uid = $row['uid'];
    $gid = $row['gid'];
    $motivation = $row['motivation'];
    // get requesting user details
    $row = runQuery("SELECT u.LastName, u.FirstName, a.name, u.email FROM `Users` u JOIN `Affiliation` a ON u.Affiliation = a.id WHERE u.id = '$uid'", "Users:Affiliation")[0];
    $reqfname = $row['FirstName'];
    $reqlname = $row['LastName'];
    $reqinst = $row['name'];
    $email = $row['email'];
    // get usergroup details
    $row = runQuery("SELECT name, description, administrator,editvariant,editsample, editclinic FROM Usergroups WHERE id = '$gid'", "Usergroups")[0];
    $groupname = $row['name'];
    $groupdescr = $row['description'];
    $groupadmin = $row['administrator'];
    $editvariant = $row['editvariant'];
    $editsample = $row['editsample'];
    $editclinic = $row['editclinic'];
    if ($groupadmin != $userid) {
        echo "You are not the administrator for this usergroup. You are not allowed to process this usergroup request.";
        $ok = 0;
    }
    if ($ok == 1) {
        // remove from requests
        doQuery("DELETE FROM `Usergroups_requests` WHERE linkkey = '$key'", "Usergroups_requests");
        // send mail to user to notify
        $message = "Message sent from https://$domain\r";
        $message .= "Subject: VariantDB: Usergroup Request Denied\r\r";
        $message .= "We are sorry to inform you your request to join the '$groupname' usergroup at VariantDB was denied.\r\r";
        $message .= "If you believe this is a mistake, feel free to reapply and include a clear motivation statement.\r\r";
        $message .= "\r\rThe VariantDB System-administrator\r\n";
        $headers = "From: no-reply@$domain\r\n";
        /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
        // step 4: send the email
        mail($email, "VariantDB: Usergroup Request Denied", $message, $headers);
        echo "<div class=sectio>\n";
        echo "<h3>Request Denied</h3>\n";
        echo "<p>You denied the request of $reqlname $reqfname to join the '$groupname' usergroup.</p>\n";
        echo "</div>\n";
    }
}
