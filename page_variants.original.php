<?php
if (!isset($loggedin) || $loggedin != 1) {
	include('page_login.php');
	exit;
}
?>
<!-- load the javascript -->
<script type='text/javascript' src='javascripts/ajax_variants.js'></script>
<div class='section'>
<!-- Left Panel: Select Query options -->
<div class='toleft w75 h40em' id='filterbuilder'>
<h3>Build your query</h3>
<table id=querytable cellspacing=0 style='width:95%'>
<?php
// load the main categories in seperate table sections
require_once('xmlLib2.php');
$configuration = my_xml2array("Filter/Filter_Options.xml");
$categories = get_value_by_path($configuration,"FilterConfiguration");
//echo "<pre>";print_r($categories);echo "</pre>";
$output = "<thead id='SourceSample'>\n";
$output .= "  <tr>\n";
$output .= "    <td colspan=4 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Use This Sample</td>\n";
$output .= "  </tr>\n";
$output .= "  <tr>\n";
$output .= "    <td colspan=1><select id='selecttype' onChange=\"SetSampleSelect('$userid')\" name='selecttype'><option value='Type' selected>Type the sample name</option><option value='List'>Select From List</option></select></td>";
$output .= "    <td colspan=3 id=sampleselect><input type=hidden name=sampleid id=sampleid value=''><input type=text style='width:98%;' value='' name=samplesource id=searchfield></td>";
$output .= "  </tr>\n";
$output .= "</thead>";
echo $output;
$output = '';
?>

<tbody >
<tr >
   <!--<th class=top>Inter-Group Logic</th>-->
   <th class=top style='padding-top:1em;'>Negate</th>
   <th class=top style='padding-top:1em;'>Filter On</th>
   <th class=top style='padding-top:1em;'>Argument</th>
   <th class=top style='padding-top:1em;'>Values</th>
<!--   <th class=top style='padding-top:1em;'>Intra-Group Logic</th>-->
</tr>
</tbody>

<?
foreach($categories as $subkey => $category) {
	if (!is_numeric($subkey)) {
		continue;
	}
	$name = $category['name'];
	if ($name == 'null') {
		continue;
	}
	$output .= "<tbody id='FilterBy$name'>\n";
	$output .= "<tr class='spacer' ><td></td></tr>";
	$output .= "  <tr >\n";
	$output .= "    <td colspan=4 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic;'>Filter On $name Information &nbsp;\n";
	$output .= "    <span title='Add Filter Option' onClick=\"AddCategory('$name')\"><img src='Images/layout/plus-icon.png' style='height:1em;'></span></td>\n";
	$output .= "  </tr>\n";
	$output .= "</tbody>\n";
}
//$output = str_replace('rowspan=1',"rowspan=$catcount",$output);
echo $output;
?>
<tr><td colspan=5 class=last>&nbsp;</td></tr>
</table>
<p><input type=button onClick="RunQuery();" value="Execute"></p>
</div>

<!-- right panel: comments from the query buildver.-->
<div class='toright w25 h40em' style='overflow:auto;'>
	<form name='annotationform' >
	<h3>Select Annotations To show</h3>
	<table id=annotable cellspacing=0 style='width:99%'>

<?php
	// load the main categories in seperate table sections
	$configuration = my_xml2array("Annotations/Config.xml");
	$categories = get_value_by_path($configuration,"Annotation_Configuration");
	$output = "\n";
	foreach($categories as $subkey => $category) {
		if (!is_numeric($subkey)) {
			continue;
		}
		$name = $category['name'];
		if ($name == 'null') {
			continue;
		}
		$output .= "<tbody id='AnnotateBy$name'>\n";
		#$output .= "<tr class='spacer' ><td></td></tr>";
		$output .= "  <tr >\n";
		$output .= "    <th colspan=3 class=top style='padding-left:0em;text-align:left;background:#cecece;font-style:italic;'>$name Information &nbsp;<input type=checkbox id='CheckAll_$name' onChange=\"CheckAll('$name');\"></td></tr>\n";
		$items = get_value_by_path($configuration,"Annotation_Configuration/$name");
		$colcount = 0;
		$inames = array();
		foreach($items as $ssubkey => $item) {
			if (!is_numeric($ssubkey)) {
				continue;
			}
			$iname = $item['name'];
			if ($iname == 'null') {
				continue;
			}
			if (substr($iname,0,1) == '_') {
				$iname = substr($iname,1);
			}
			$inames[] = $iname;
		}
		sort($inames);
		foreach ($inames as $key => $iname) {
			$colcount++;
			if ($colcount > 2) {
				$output .= "</tr>";
				$colcount = 1;
			}
			if ($colcount == 1) {
				$output .= "<tr>";
			}
			$output .= "<td><input type=checkbox name='annotations' value='$name;$iname'> $iname</td>";
		}
		while ($colcount < 2) {
			$colcount++;
			echo "<td>&nbsp;</td>";
		}
		echo "</tr>";
		$output .= "</tbody>\n";
	}
	$output .= "</table>";
	$outpu .= "</form>";
	echo $output;
	$output = '';
?>
</div>
<div style="clear: both;"></div>
</div>

<div class=section class='w100'>
	<h3>Query Results</h3>
	<div id=QueryResults>
		<p>Query was not yet executed</p>
	</div>
</div>



<!-- Start autocomplete function -->
<script type='text/javascript'>

var options = {
		script:"ajax_queries/samplesearch.php?json=true&limit=10&",
		varname:"input",
		json:true,
		shownoresults:true,
		delay:200,
		cache:false,
		timeout:10000,
		minchars:2,
		callback: function (obj) { document.getElementById('searchfield').value = obj.value;document.getElementById('sampleid').value = obj.id; }
	};
	var as_json = new bsn.AutoSuggest('searchfield', options);
	

// checkall by annotation name
function CheckAll(name) {
	// to do.	
}

</script>
