<div class='section' >
<h3>Quota Overview</h3>
<p>Budget is tight for everyone in academics, and we are no different. The good news is, that we have received budget to hire a developer to improve VariantDB and make it WGS proof. The bad news is, that we had to reassign ICT budget to wet-lab investments. This means that we cannot provide the full budget required to upgrade our hardware to keep VariantDB a completely free service. Consequently, we are forced to apply quota, to encourage people to invest in this service....</p>

<p>In practice, this means access to the filtering page and api will be restricted if you exceeded the free quota. Quota calculations are as follows:<br style='clear:both'/>
<ul>
  <li>Each user receives 100 free credits.</li>
  <li>Each sample accounts as 0.02 credits</li>
  <li>100K variants account to 1 credit</li>
</ul>
</p>

<p>Please consider supporting the maintainance of VariantDB. A minimal investment is 500 euro, which accounts for 2500 credits. A single investments is taken into account for each user in a usergroup the investment is assigned to (e.g. A university).</p>


<p><a href='index.php?page=contact'>Contact us for more information.</a></p>

<?php 
if (!isset($loggedin) || $loggedin != 1) {
	echo "</div>";
	exit;
}

// load current data

$fh = fopen(".Credentials/quota.txt","r");
$score = 0;
$nrs = 0;
$rnv = 0;
$nrc = 0;
while ($line = fgets($fh)) {
	$line = rtrim($line);
	list($uid,$nrs,$nrv,$nrc,$score) = explode("\t",$line);
	if ($uid == $userid) {
		break;
	}
}
fclose($fh);
// read usergroup contributions
$fh = fopen(".Credentials/Investments.txt","r");
$line = fgets($fh);
$investments = array();
$i_comment = '';
$i_budget = '';
while ($line = fgets($fh)) {
	$line = rtrim($line);
	list($ugroup,$budget,$MaxScore,$comment) = explode("\t",$line);
	// get members
	$q = "SELECT uid FROM `Usergroups_x_User` WHERE gid = '$ugroup' AND uid = '$userid'";
	$rs = runQuery($q,"Usersgroups_x_User");
	foreach($rs as $r) {
		if (!array_key_exists($r[0],$investments) || $investments[$r[0]] < $MaxScore) {
			$investments[$r[0]] = $MaxScore;
			$i_comment = $comment;
			$i_budget = $budget;
		}
	}
	
}
fclose($fh);
?>



<h3>Current Quota overview</h3>
<p>Your current credit status is: <?php echo "$score"; ?>, calculated as: <br style='clear:both;'/><ul>
 <li >Nr.Samples:<?php echo "$nrs";?></li>
<li>Nr.Variants: <?php echo $nrv;?></li>
<li>Total : samples*0.2 + Variants/100K</li>
</ul></p>
			
<?php
if ($i_budget == -1) {
?>
<p><span style='color:green'>Quota are not yet (and hopefully never) applied to members of CMG UA/UZA.</span> However, please consider to allocate budgets to help our group in maintaining this project. We thank you :-)</p>

<?php
}
elseif (array_key_exists($userid,$investments)) {
?>
<p>Your department made an investment in VariantDB, entitling you to <?php echo $MaxScore;?> credits. Investment notes were : '<?php echo $i_comment;?>'</p>
<?php
}
?>

</div>	
