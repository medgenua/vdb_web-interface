#!/usr/bin/env python
from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger

import configparser
import argparse
import os
import sys
import fcntl
import subprocess
import json
import tempfile
import time

# libraries for cgi (form handler)
# error handler not loaded, handled by UZALogger
import cgi


# error classes
class DataError(OSError):
    pass


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    # set path :
    if "PATH" in config["PATH"]:
        config["PATH"]["PATH"] = "{}:{}".format(config["PATH"]["PATH"], os.environ["PATH"])
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    if "CONDA_ENV" in config["PATH"]:
        config["PATH"]["PATH"] = "{}/bin:{}".format(
            config["PATH"]["CONDA_ENV"], config["PATH"]["PATH"]
        )
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    # command line arguments
    help_text = """
        GOAL:
        #####
            - organize api query request
            - prepare files to run request
            - launch request.
        """
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    args = parser.parse_args()

    return config, args


def GetQueryKey(config):
    # initialize if not existing.
    if not os.path.exists(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter")
    ):
        try:
            with open(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter"),
                "w",
            ) as fh:
                fh.write("0")
        except Exception as e:
            Fail("Could not initialize api job_counter file: {}".format(e))
    if not os.path.exists(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter.lck")
    ):
        try:
            open(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter.lck"
                ),
                "w",
            ).close()
        except Exception as e:
            Fail("Could not initialize api job_counter file: {}".format(e))
    # get lock on counter value.
    lock = open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter.lck"), "r+"
    )
    fcntl.lockf(lock, fcntl.LOCK_EX)
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter"), "r"
    ) as fh:
        queryKey = int(fh.readline().rstrip()) + 1
        while os.path.exists(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/{}".format(queryKey))
        ):
            queryKey += 1
    os.makedirs(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/{}".format(queryKey))
    )
    # update counter
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter"), "w"
    ) as fh:
        fh.write(str(queryKey))
    # close lock.
    lock.close()
    # return key
    return queryKey


# write settings to file, add status etc.
def PrepareJob(form, sid, queryKey, config):
    wd = os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/{}".format(queryKey))
    # put status file
    with open(os.path.join(wd, "status"), "w") as fh:
        fh.write("Queued")
    # print the form.
    with open(os.path.join(wd, "form"), "w") as fh:
        for k in form:
            fh.write("{}={}\n".format(k, form[k].value))
        # add the SID variable.
        fh.write("sid={}\n".format(sid))
    # local call ? (so not from external /api/SubmitQuery)
    if "local_call" in form:
        open(os.path.join(wd, "local_call"), "w").close()
    # qsub file (script file, no actual qsub used)
    #  => created per section, not per record, by the VariantDB_Report_Runner.py
    # print apiKey to file
    with open(os.path.join(wd, "uid"), "w") as fh:

        fh.write(form["uid"].value)

    # open permissions to allow scriptuser access.
    subprocess.check_call(["chmod", "-R", "777", wd])

    return True


def SubmitJobs(queryKeys, config, form):
    # lock queue file.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue.lck"), "w+"
    ) as qh:
        fcntl.lockf(qh, fcntl.LOCK_EX)
        # create if not existing
        if not os.path.exists(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue")
        ):
            open(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue"),
                "w",
            ).close()
        # write to queue file:
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue"), "r"
        ) as q:
            queue = q.readlines()
            # strip empty lines if any
            queue = [x.rstrip() for x in queue if x.rstrip()]
        # add new item
        queue.extend([str(x) for x in queryKeys.values()])
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue"), "w"
        ) as q:
            q.write("{}\n".format("\n".join(queue)))

    # get queue position of final item.
    queue_position = sum(
        1
        for line in open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue")
        )
    )
    queue_start = queue_position - len(queryKeys) + 1
    log.info("queue pos : {} to {}".format(queue_start, queue_position))
    return "{} to {}".format(queue_start, queue_position), len(queryKeys)


# check if user has access to required resources. Should be ok, but double check.
def CheckAccess(uid, rid, sids):
    # valid user
    u = dbh.runQuery("SELECT id FROM `Users` WHERE id = %s", uid)
    if not u:
        raise ValueError("User {} not found".format(uid))
    # access to report.
    r = dbh.runQuery(
        "SELECT rid FROM `Report_Definitions` WHERE `rid` = %s AND (`Owner` = %s OR `Public` = 1)",
        (rid, uid),
    )
    if not r:
        r = dbh.runQuery(
            "SELECT rid FROM `Users_x_Report_Definitions` WHERE `rid` = %s AND `uid` = %s",
            (rid, uid),
        )
    if not r:
        raise ValueError("User {} has no access to report {}".format(uid, rid))
    # Access to sids
    for sid in sids:
        s = dbh.runQuery(
            "SELECT ps.sid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu on ps.pid = pu.pid WHERE ps.sid = %s AND pu.uid = %s",
            (sid, uid),
        )
        if not s:
            raise ValueError("User {} has no access to sample {}".format(uid, sid))
    # ok.
    return True


def GetDBConnection(quiet=True):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)

    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)
    if not quiet:
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    return dbh


def Fail(msg):
    log.error(msg)
    print("Content-type: application/json\r\n")
    if "sudo" in msg:
        msg = "Error messsage masked"
    print(json.dumps({"ERROR": msg, "status": "error"}))
    sys.exit(1)


def LaunchMonitors(config, statusfile):
    # return the number of services launched (checked against statusfile)
    nr_launched = 0
    # report runner
    log.info("Check if Report runner is active")
    pid_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs/VariantDB_Report_Runner.pid"
    )
    if not os.path.exists(pid_file):
        with open(pid_file, "w") as fh:
            fh.write("-1")
    with open(pid_file, "r") as fh:
        pid = fh.readline().rstrip()
        if not pid:
            pid = -1
    if not os.path.exists("/proc/{}".format(pid)):
        # launch the process.
        nr_launched += 1
        try:
            fork_pid = os.fork()
        except Exception as e:
            raise e

        if fork_pid == 0:
            # configure to keep running.
            os.chdir("/")
            os.setsid()
            os.umask(0)
            log_file = os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs/VariantDB_Report_Runner.log"
            )
            cmd = "(echo {} | sudo -u {} -S bash -c \"cd {}/cgi-bin && ./Service_Launcher.sh '{}/cgi-bin' 'VariantDB_Report_Runner.py' '{}' \" ) 2>>{}".format(
                config["USERS"]["SCRIPTPASS"],
                config["USERS"]["SCRIPTUSER"],
                config["LOCATIONS"]["SCRIPTDIR"],
                config["LOCATIONS"]["SCRIPTDIR"],
                log_file,
                log_file,
            )
            try:
                pid = subprocess.check_output(cmd, shell=True)
                with open(pid_file, "w") as fh:
                    fh.write(pid.decode("utf-8"))
                log.info("Report_Runner launched")
                # exit child.
                with open(statusfile, "a") as fh:
                    fh.write("SUCCESS\n")
                sys.exit(0)
            except Exception as e:
                with open(statusfile, "a") as fh:
                    fh.write("FAIL\n")

                raise e
    # query_runner handler.  launch if needed.
    log.info("Check if query runner is active")
    pid_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs/VariantDB_Query_Runner.pid"
    )
    if not os.path.exists(pid_file):
        with open(pid_file, "w") as fh:
            fh.write("-1")
    with open(pid_file, "r") as fh:
        pid = fh.readline().rstrip()
        if not pid:
            pid = -1
    if not os.path.exists("/proc/{}".format(pid)):
        # launch the process
        nr_launched += 1
        try:
            fork_pid = os.fork()
        except Exception as e:
            log.error("Could not fork : {}".format(e))
            raise e
        if fork_pid == 0:
            # configure to keep running.
            os.chdir("/")
            os.setsid()
            os.umask(0)
            log_file = os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs/VariantDB_Query_Runner.log"
            )
            cmd = "(echo {} | sudo -u {} -S bash -c \"cd {}/cgi-bin && ./Service_Launcher.sh '{}/cgi-bin' 'VariantDB_Query_Runner.py' '{}' \" ) 2>>{}".format(
                config["USERS"]["SCRIPTPASS"],
                config["USERS"]["SCRIPTUSER"],
                config["LOCATIONS"]["SCRIPTDIR"],
                config["LOCATIONS"]["SCRIPTDIR"],
                log_file,
                log_file,
            )
            try:
                pid = subprocess.check_output(cmd, shell=True)
                with open(pid_file, "w") as fh:
                    fh.write(pid.decode("utf-8"))
                log.info("Query_Runner launched")
                with open(statusfile, "a") as fh:
                    fh.write("SUCCESS\n")
                # exit child.
                sys.exit(0)
            except Exception as e:
                with open(statusfile, "a") as fh:
                    fh.write("FAIL\n")
                raise e
    return nr_launched


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)
    # start logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_Report_Wrapper",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        quiet=True,
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)

    # error handler
    # cgitb.enable(display=1, logdir=config["LOGGING"]["LOG_PATH"])

    # launch the monitor(s) :
    #  There's a race condition if launching fails. Set triggers in statusfile.
    sfd, sfn = tempfile.mkstemp()

    try:
        nr_launched = LaunchMonitors(config, sfn)
    except Exception as e:
        Fail("Failed to launch Report Runners: {}".format(e))
    # wait for launchers
    while True:
        with open(sfn, "r") as sfh:
            lines = sfh.readlines()
        # any failures ?
        if any("FAIL" in x for x in lines):
            # failure json is already constructed. Just exit.
            sys.exit(1)
        # all ok ?
        if len(lines) == nr_launched:
            break
        # next
        time.sleep(0.5)

    # PAYLOAD : handles both GET and POST options.
    form = cgi.FieldStorage()
    uid = form["uid"].value
    rid = form["rid"].value
    sids = form["sids"].value.split(",")
    # todo : add the build
    dbh = GetDBConnection()

    # check access
    try:
        CheckAccess(uid, rid, sids)
    except Exception as e:
        Fail("Access problem for uid {} ; rid {} and sids {} : {}".format(uid, rid, sids, e))

    # process the sids.
    queryKeys = {}
    for sid in sids:
        # get job key.
        try:
            queryKey = GetQueryKey(config)
            PrepareJob(form, sid, queryKey, config)
            queryKeys[sid] = queryKey

        except Exception as e:
            Fail("Could not prepare report generation for sid {} : {}".format(sid, e))

    # queue job.
    try:
        queue_position, nr_submitted = SubmitJobs(queryKeys, config, form)
    except Exception as e:
        Fail("Could not queue requests : {}".format(e))

    # format results.
    result = {
        "qids": queryKeys,
        "queue_position": queue_position,
        "nr_submitted_reports": nr_submitted,
    }

    print("Content-type: application/json\r\n")
    print(json.dumps(result))
