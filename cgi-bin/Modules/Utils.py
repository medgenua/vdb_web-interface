#!/usr/bin/env python3

import os
import re
import sys
import shutil
import tempfile
import subprocess
import json
import time
from argparse import Namespace
import pprint

from CMG.UZALogger import setup_logging, get_logger
from CMG.DataBase import MySQL, MySqlError
from CMG.Utils import Re, AutoVivification, CheckVariableType

from datetime import datetime
from dataclasses import dataclass, field
from typing import Dict, List, Type

# activate the logger
log = get_logger()

# regex handler
re = Re()


class DataError(Exception):
    pass


# class used in annotation routines, with shared functions.
@dataclass
class Annotator:
    method: str
    annotation: str
    config: Dict
    args: Namespace = field(default_factory=Namespace)
    codes: Dict = field(default_factory=dict)
    files: List = field(default_factory=list)

    def __post_init__(self):
        self.GetDatabaseConnection()
        self.dbh2 = self.GetDatabaseConnection(return_handle=True)
        # if args provided : validate them
        if len(self.args._get_args()) + len(self.args._get_kwargs()):
            log.info("Validating options")
            self.ValidateOptions()

    ######################
    ## DATABASE HELPERS ##
    ######################

    # connect to database
    def GetDatabaseConnection(self, return_handle=False, silent=False):
        try:
            # db suffix set ?
            db_suffix = self.config["DATABASE"].get("DBSUFFIX", "")
            if db_suffix:
                self.db_suffix = "_{}".format(db_suffix)
            else:
                self.db_suffix = ""

            dbh = MySQL(
                user=self.config["DATABASE"]["DBUSER"],
                password=self.config["DATABASE"]["DBPASS"],
                host=self.config["DATABASE"]["DBHOST"],
                database="NGS-Variants-Admin{}".format(self.db_suffix),
                allow_local_infile=True,
            )
            row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
            db = "NGS-Variants{}{}".format(row[0]["name"], self.db_suffix)
            # use statement doesn't work with placeholder. reason unknown
            dbh.select_db(db)
            if not silent:
                log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

            if return_handle:
                return dbh
            else:
                self.db = db
                self.dbh = dbh
        except Exception as e:
            log.error("Could not connect to VariantDB Database : {}".format(e))
            sys.exit(1)

    # Check Value types for mysql
    def CheckTypes(self, values, silent=False):
        for i in range(len(self.table_info["python_types"]) - 1):
            # value i in values corresponds to column i+1 in mysql (vids is prepended)
            if not CheckVariableType(values[i], self.table_info["python_types"][i + 1]):
                if not silent:
                    pprint.pprint(self.table_info)
                    pprint.pprint(values)
                    # set to default.
                    log.info(
                        "wrong type for column {}:{} : type({}) !== {} : setting to default: {}".format(
                            i,
                            self.table_info["names"][i + 1],
                            values[i],
                            self.table_info["python_types"][i + 1],
                            self.table_info["defaults"][i + 1],
                        )
                    )
                log.debug(
                    f"Type Mismatch at column {i}: setting {values[i]} to {self.table_info['defaults'][i+1]}"
                )
                values[i] = self.table_info["defaults"][i + 1]
        return values

    # get table structure
    def GetTableInfo(self):
        if not hasattr(self, "table_info"):
            table_info = self.dbh.getColumns(self.args.table_name)
            # remove the aid column (either first or second column)
            for i in range(len(table_info["names"]) - 1):
                if table_info["names"][i] == "aid":
                    table_info["names"].pop(i)
                    table_info["python_types"].pop(i)
                    table_info["mysql_types"].pop(i)
                    table_info["defaults"].pop(i)
                    break
            self.table_info = table_info

    # load a table into the database.
    def LoadDataTable(self, infile, field_names=None, add_default_fieldnames=True):
        # make sure table info is present.
        self.GetTableInfo()
        # construct query
        query = "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}{}`".format(
            infile, self.args.prefix, self.args.table_name
        )
        log.debug(query)
        # add provided field names:
        if field_names:
            query += " (`{}`) ".format("`,`".join([str(x) for x in field_names]))
        # add default field names (all but aid)
        elif add_default_fieldnames:
            query += " (`{}`) ".format("`,`".join([str(x) for x in self.table_info["names"]]))
        # else : assume all columns are provided.
        log.debug(query)
        self.dbh.doQuery(query)

    def ValidateOptions(self, args=Namespace()):
        if not self.args:
            log.info("using provided args")
            self.args = args

        # Validate the provided options.
        # not all scripts have infile/outfile
        if not hasattr(self.args, "infile"):
            self.args.infile = False
        # if file base : no need to go to database.
        if self.args.infile:
            # file must exist.
            if not os.path.isfile(self.args.infile):
                raise DataError("Provided input VCF does not exist: {}".format(self.args.infile))
            # output file must be given.
            if not self.args.outfile:
                raise ValueError(
                    "Output file (--outfile) is mandatory when input vcf is provided. "
                )
            # issue warning if outfile exists.
            if os.path.isfile(self.args.outfile):
                # writable?
                if not os.access(self.args.outfile, os.W_OK):
                    raise DataError(
                        "No permission to overwrite existing file: {}".format(self.args.outfile)
                    )

                log.warning(
                    "Output file exists and will be overwritten: {}".format(self.args.outfile)
                )
                log.warning("Abort now to prevent this")
                time.sleep(5)
        # else:
        # build provided ?
        if self.args.build:
            # should exist
            try:
                self.dbh.select_db("NGS-Variants-{}{}".format(self.args.build, self.db_suffix))
            except MySqlError as e:
                raise ValueError("Invalid GenomeBuild '{}' : {}".format(self.args.build, e))
        else:
            # use/get current.
            row = self.dbh.runQuery(
                "SELECT `name`, `StringName` FROM `NGS-Variants-Admin`.`CurrentBuild`"
            )
            self.args.build = row[0]["name"][1:]

        return True

    def ValidateTable(self):
        # validation:
        if not self.args.infile and self.args.validate:
            try:
                self.dbh2.doQuery(
                    "DROP TABLE IF EXISTS `{}{}`".format(self.args.prefix, self.args.table_name)
                )
                self.dbh2.doQuery(
                    "CREATE TABLE `{}{}` LIKE `{}`".format(
                        self.args.prefix, self.args.table_name, self.args.table_name
                    )
                )
            except Exception as e:
                raise MySqlError(
                    "Failed to create validation table : {}{} : {}".format(
                        self.args.prefix, self.args.table_name, e
                    )
                )
        # no validation
        elif not self.args.infile and not self.dbh.tableExists(self.args.table_name):
            raise MySqlError("Required Table does not exists: {}".format(self.args.table_name))

    def GetVariants(self, normalize=False):
        # table => self.args.table_name
        query = "SELECT v.* FROM `Variants` v"
        # tmp table became too big, revert back to exist syntax. 
        #  tested, performance is similar
        if not self.args.validate:
            query += " WHERE NOT EXISTS ( SELECT 1 FROM `{}` va WHERE va.vid = v.id )".format(
                self.args.table_name
            )
            #query += " WHERE v.id NOT IN (SELECT va.vid FROM `{}` va )".format(self.args.table_name)
        results = self.dbh.runQuery(query, size=100000)
        self.vcf = os.path.join(
            self.config["LOCATIONS"]["SCRIPTDIR"],
            f"Annotations/{self.method}/Input_Files/",
            "{}.{}.list.vcf".format(self.args.table_name, time.time()),
        )

        # open file
        try:
            fh = open(self.vcf, "w")
        except Exception as e:
            raise e
        while len(results):
            for variant in results:
                variant["chr"] = self.ConvertChr(variant["chr"])

                # skip non_ref
                if re.isearch(r"non_ref", variant["AltAllele"]):
                    continue
                # skip spanning dels.
                if "*" in variant["AltAllele"]:
                    continue
                # normalize -N- to -A-
                variant["AltAllele"] = re.isub(r"N", "A", variant["AltAllele"], count=0)
                # strip redundant nucleotides.
                if normalize:
                    variant = self.NormalizeVariant(variant)
                # invalid variant format : exit
                if (
                    not variant["chr"]
                    or not len(variant["RefAllele"])
                    or not len(variant["AltAllele"])
                ):
                    log.error(f"Invalid variant format in VCF : {variant}")
                    sys.exit(1)
                # add variantID in id col and in info field.
                fh.write(
                    "{}\t{}\t{}\t{}\t{}\t99\tPASS\tid={}\tGT\t0/1\n".format(
                        variant["chr"],
                        variant["start"],
                        variant["id"],
                        variant["RefAllele"],
                        variant["AltAllele"],
                        variant["id"],
                    )
                )

            results = self.dbh.GetNextBatch()
        # close VCF
        fh.close()

    def NormalizeVariant(self, variant):
        newstart = int(variant["start"])
        newref = variant["RefAllele"].upper()
        newalt = variant["AltAllele"].upper()
        newend = newstart + len(newref) - 1

        # trim 5'
        while newref[0:1] == newalt[0:1] and len(newref) > 1 and len(newalt) > 1:
            newref = newref[1:]
            newalt = newalt[1:]
            newstart += 1
        # trim 3'
        while newref[-1] == newalt[-1] and len(newref) > 1 and len(newalt) > 1:
            newref = newref[:-1]
            newalt = newalt[:-1]
            newend -= 1

        # return
        variant["RefAllele"] = newref
        variant["AltAllele"] = newalt
        variant["start"] = newstart
        return variant

    def PrepareInputVCF(self, normalize=False, strip_chr=False):
        self.vcf = os.path.join(
            self.config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/{}/Input_Files/".format(self.method),
            "{}.{}.list.vcf".format(self.args.table_name, time.time()),
        )
        log.info(f"Writing vcf : {self.vcf}")
        log.info(f"Reading vcf : {self.args.infile}")
        try:
            line_idx = 0
            with open(self.vcf, "w") as vcf_out, open(self.args.infile, "r") as vcf_in:
                for line in vcf_in:
                    if line.startswith("#"):
                        vcf_out.write(line)
                        continue
                    # increment counter
                    line_idx += 1
                    cols = line.rstrip().split("\t")
                    if cols[2] == ".":
                        vid = f"line_{line_idx}"
                    else:
                        vid = cols[2]
                    try:
                        if strip_chr:
                            cols[0] = self.ConvertChr(re.sub(r"^chr", "", cols[0]))
                        else:
                            # re-add 'chr' prefix
                            if cols[0].startswith("chr"):
                                cols[0] = f'chr{self.ConvertChr(re.sub(r"^chr", "", cols[0]))}'
                            else:
                                cols[0] = self.ConvertChr(str(cols[0]))
                    except Exception as e:
                        log.error(
                            f"Invalid chromosome format in VCF : {','.join([str(x) for x in cols])} : {e}"
                        )
                        sys.exit(1)
                    if normalize:
                        v = self.NormalizeVariant(
                            {"start": cols[1], "RefAllele": cols[3], "AltAllele": cols[4]}
                        )
                        cols[1] = v["start"]
                        cols[3] = v["RefAllele"]
                        cols[4] = v["AltAllele"]
                    # invalid variant format : exit
                    if not len(cols[3]) or not len(cols[4]):
                        log.error(
                            f"Invalid variant format in VCF : {','.join([str(x) for x in cols])}"
                        )
                        sys.exit(1)
                    line_out = "{}\t{}\t{}\t{}\t{}\t99\tPASS\tid={}\tGT\t0/1\n".format(
                        cols[0], cols[1], vid, cols[3], cols[4], vid
                    )
                    vcf_out.write(line_out)

        except Exception as e:
            log.error("Could not parse input vcf: {}".format(e))
            sys.exit(1)

    def ConvertChr(self, v):
        # as int
        chrom_dict = {str(x): str(x) for x in range(1, 23)}
        # as string
        chrom_dict.update({str(x): x for x in range(1, 23)})
        chrom_dict.update(
            {"X": "23", "23": "X", "Y": "24", "24": "Y", "M": "25", "MT": "25", "25": "M"}
        )
        return chrom_dict.get(str(v), False)

    def GetCode(self, column, value=False):
        # get all codes if no value (explicit) or dict is empty (implicit)
        if not value or not any(self.codes):
            rows = self.dbh.runQuery(
                "SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = %s",
                "{}_{}".format(self.args.table_name, column),
            )
            for row in rows:
                self.codes["{}_{}".format(column, row["Item_Value"])] = row["id"]

        # get specific code.
        elif "{}_{}".format(column, value) not in self.codes:
            self.codes["{}_{}".format(column, value)] = self.dbh2.insertQuery(
                "INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES (%s,%s)",
                ["{}_{}".format(self.args.table_name, column), value],
            )
        return self.codes.get("{}_{}".format(column, value), None)

    def ConvertCodons(self, v):
        codons = {
            "G": "Gly",
            "A": "Ala",
            "L": "Leu",
            "M": "Met",
            "F": "Phe",
            "W": "Trp",
            "K": "Lys",
            "Q": "Gln",
            "E": "Glu",
            "S": "Ser",
            "P": "Pro",
            "V": "Val",
            "I": "Ile",
            "C": "Cys",
            "Y": "Tyr",
            "H": "His",
            "R": "Arg",
            "N": "Asn",
            "D": "Asp",
            "T": "Thr",
            "X": "Ter",
        }
        if v not in codons:
            raise DataError("Invalid one letter codon provided : {}".format(v))
        return codons[v]

    def cleanup(self):
        for file in self.files:
            log.info(f"Cleaning up file : {file}")
            try:
                os.remove(file)
            except FileNotFoundError:
                log.info("File did not exist.")
            except Exception as e:
                log.error(f"Failed to remove file : {file} : {e}")


# setup logging.
def StartLogger(config, method):
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name=f"VariantDB_LoadVariants_{method}",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
    )
    log = get_logger("main")
    log.info(msg)
    return log


# also in CMG-Package.
def PopList(oldlist, indices):
    indices = set(indices)
    newlist = [v for i, v in enumerate(oldlist) if i not in indices]
    return newlist
