package Database;

use strict;
use warnings;
use DBI;

#use ErrorHandler;
use Credentials;

sub new {
	my $pkg = shift;
	my $self = {	
		'credentials' => Credentials->new(),
		'eh' => ErrorHandler->new(),
	};
        bless $self,$pkg;
        return $self;
}

sub handle_error {
    my $message = shift;
    my $h = shift;
    # don't now how to pass the $self to the dbi->handle_error attribute, so generate it locally
    my $c = Credentials->new();
    my $e = ErrorHandler->new();
    my $logfile = $c->{SCRIPTDIR}."/Query_Logs/mysql.err";
    if (!-f $logfile) {
	system("touch '$logfile'") == 0 or $e->die("Could not create file: $logfile");
	system("chmod 777 '$logfile'") == 0 or $e->die("Could not set permisions on: $logfile");
    }
    if ((!defined($message) || $message eq '') && (!defined($h->{statement}) || $h->{statement} eq '')) {
	# nothing to report. (happens on connection errors, but those are catched by the eh->die().)
	return;
    }
    open OUT, ">>$logfile" or $e->die("Could not open mysql log for writing: $logfile\nWanted to write : $message : $h->{statement}");
    my $date = `date`;
    chomp($date);
    print OUT "$date: '$message'\n";
    if (defined($h->{statement}) && $h->{statement} ne '') {
	    print OUT $h->{statement};
	    $message .= "\n".$h->{statement}."\n";
    }
    my $i = 0;
    print OUT " => Stack Trace : \n";
    while ( (my @call_details = (caller($i++))) ){
	print OUT "\t".$call_details[1].": line ".$call_details[2]." called function ".$call_details[3]."\n";
    }
    print OUT "\n\n";
    close OUT;
    # and send out the problem by email.
    
    $e->die("MYSQL problem : $message \n");
}

sub Connect {
	my ($self,$dbname) = @_;
	# connection settings
	my %attr = ( PrintError => 0, RaiseError => 0, mysql_auto_reconnect => 1, mysql_local_infile => 1, HandleError => \&handle_error);
	# get database version to use
	if (!defined($dbname)) {
		my $connectionInfo="dbi:mysql:NGS-Variants-Admin:".$self->{credentials}->{DBHOST};
		my $dbh = DBI->connect($connectionInfo,$self->{credentials}->{DBUSER},$self->{credentials}->{DBPASS},\%attr) or $self->{eh}->die('Could not connect to database: '.$DBI::errstr);
		my $currentdb = $dbh->selectcol_arrayref("SELECT name FROM `CurrentBuild`") ;
		$dbname = "NGS-Variants".$currentdb->[0];
		#print "Setting DB name to : $dbname\n";
	}
	# connect to production
	my $connectionInfo="dbi:mysql:$dbname:".$self->{credentials}->{DBHOST};
	my $dbh = DBI->connect($connectionInfo,$self->{credentials}->{DBUSER},$self->{credentials}->{DBPASS},\%attr) or $self->{eh}->die('Could not connect to database: '.$DBI::errstr);	
	$self->{dbh} = $dbh;
}

sub Disconnect {
	my $self = shift;
	if (defined($self->{dbh})) {
		$self->{dbh}->disconnect();
	}
}

sub Count {
	my $self = shift;
	my $query = shift;
	# prepared call
	if (ref($query) eq 'DBI::st') {
		$query = $query->{Statement};
	}
	else {
		if (!defined($query) || $query eq '') {
			$self->{eh}->warn("No Query provided to fetch results\n");
			return 0;
		}
		if (!defined($self->{dbh})) {
			$self->Connect();
		}
	}	 
	## rewrite the query, do counting in DB.
	if ($query =~ m/^SELECT .* FROM (.*)/i) {
		$query = "SELECT COUNT(1) FROM $1";
		 #print "rewritten counting query to :\n  $query\n";
	}
	my $r = $self->GetOne($query,@_);
	if (!defined($r)) {
		# invalid query returns undef.
		return 0;
	}
	return ($r);
}

sub Get {
	my $self = shift;
	my $query = shift;
	my $sth = undef;
	my $placeholders = 0;
	# prepared call
	if (ref($query) eq 'DBI::st') {
		$sth = $query;
		$placeholders = $self->{prepared}->{$query->{Statement}};
	}
	else {
		if (!defined($query) || $query eq '') {
			$self->{eh}->warn("No Query provided to fetch results\n");
			return [];
		}
		if (!defined($self->{dbh})) {
			$self->Connect();
		}
		$sth = $self->{dbh}->prepare($query);
		$placeholders = () = $query =~ /\?/g;
	}	
	my @items = @_;
	if (scalar(@items) > 0) {
		if (scalar(@items) != $placeholders) {
			$self->{eh}->warn("Incorrect number of placeholders provided:\n - Placeholders: $placeholders\n - Provided values: ".scalar(@items)."\n");
			return [];
		}
	}
	$sth->execute(@items);
	my $result = $sth->fetchall_arrayref();
	if (ref($query) ne 'DBI::st') {
		$sth->finish();
	}
	return $result;
}

# simplify output if a single column AND single row is needed/expected.
sub GetOne {
	my $self = shift;
	my $query = shift;
	# if a prepped statement is provided, go back to the query.
	if (ref($query) eq 'DBI::st') {
		$query = $query->{Statement};
	}
	if (!defined($query) || $query eq '') {
		$self->{eh}->warn("No Query provided to fetch results\n");
		return undef;
	}
	my $r = $self->Get($query,@_);
	if (scalar(@$r) == 0) {
		return undef;
	}
	return $r->[0]->[0];
}
# from an prepared statement, get a defined number of variants (rowcache of huge sets)
sub GetSlice {
	my $self = shift;
	my $sth = shift;
	if (ref($sth) ne 'DBI::st') {
		$self->{eh}->die("No statement handle provided for GetSlice.\n");
	}
	my $max = shift;
	if ($max !~ m/^\d+$/) {
		$self->{eh}->warn("Invalid slice size provided. Setting to 10000\n");
		$max = 10000;
	}
	# placeholders ?	
	my @items = @_;
	if (scalar(@items) != $self->{prepared}->{$sth->{Statement}}) {
		 $self->{eh}->die("Invalid number of placeholders provided: - Placeholders: ".$self->{prepared}->{$sth->{Statement}}."\n - Provided values: ".scalar(@items)."\n");
	}
	# already executed ? 
	if (!defined($self->{executed}->{$sth->{Statement}}) || $self->{executed}->{$sth->{Statement}} ne join("-",@items)) {
		# placeholders don't match current set. re-execute.
		$sth->execute(@items);
		$self->{executed}->{$sth->{Statement}} = join("-",@items);
	}
	my $set = $sth->fetchall_arrayref(undef,$max);
	return $set;
}

# query without fetching (update/insert/...)
sub do {
	my $self = shift;
	my $query = shift;
	my $sth = undef;
	my $placeholders = 0;
	# prepared call
	if (ref($query) eq 'DBI::st') {
		$sth = $query;
		$placeholders = $self->{prepared}->{$query->{Statement}};
	}
	else {
		if (!defined($query) || $query eq '') {
			$self->{eh}->warn("No Query provided to fetch results\n");
			return ;
		}
		if (!defined($self->{dbh})) {
			$self->Connect();
		}
		$sth = $self->{dbh}->prepare($query);
		$placeholders = () = $query =~ /\?/g;
	}	
	my @items = @_;
	if (scalar(@items) > 0) {
		if (scalar(@items) != $placeholders) {
			$self->{eh}->warn("Incorrect number of placeholders provided:\n - Placeholders: $placeholders\n - Provided values: ".scalar(@items)."\n");
			return;
		}
	}
	$sth->execute(@items);
	if (ref($query) ne 'DBI::st') {
		$sth->finish();
	}
	return;
}
# insert query, return the id.
sub insert {
	my $self = shift;
	my $query = shift;
	my $sth = undef;
	my $placeholders = 0;
	# prepared call
	if (ref($query) eq 'DBI::st') {
		$sth = $query;
		$placeholders = $self->{prepared}->{$query->{Statement}};
	}
	else {
		if (!defined($query) || $query eq '') {
			$self->{eh}->warn("No Query provided to fetch results\n");
			return undef;
		}
		if (!defined($self->{dbh})) {
			$self->Connect();
		}
		$sth = $self->{dbh}->prepare($query);
		$placeholders = () = $query =~ /\?/g;
	}	
	my @items = @_;
	if (scalar(@items) > 0) {
		if (scalar(@items) != $placeholders) {
			$self->{eh}->warn("Incorrect number of placeholders provided:\n - Placeholders: $placeholders\n - Provided values: ".scalar(@items)."\n");
			return undef;
		}
	}
	$sth->execute(@items);
	my $id = $self->{dbh}->last_insert_id(undef,undef,undef,undef);
	if (ref($query) ne 'DBI::st') {
		$sth->finish();
	}
	return $id;
}
# prepare a statement
sub prepare {
	my $self = shift;
	my $query = shift;
	if (!defined($query) || $query eq '') {
		$self->{eh}->die("No Query provided to prepare\n");
	}
	my $placeholders = () = $query =~ /\?/g;
	if (!defined($self->{dbh})) {
		$self->Connect();
	}
	my $sth = $self->{dbh}->prepare($query);
	$self->{prepared}->{"$query"} = $placeholders; 
	return $sth;
}
# close a statement
sub finish {
	my $self = shift;
	my $sth = shift;
	if (ref($sth) eq 'DBI::st') {
		$sth->finish();
	}
	else {
		$self->{eh}->warn("Provided a non-statementhandler to db->finish");
	}
	return;
}
1;
