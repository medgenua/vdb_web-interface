package Credentials;

# usage :
# use Credentials;
# $credentials = new Credentials('path_to_credentials_file');

use strict;
use warnings;
use Carp;
use Cwd qw/abs_path/;
use File::Basename;

sub new {
    my $pkg = shift;
    my $path = shift;
  	my $caller = caller(); # module that invoked this import
	# path not provided, try to locate by default
	if (!$path) {
		my @dirs = split(/\//, dirname(abs_path($0)));
		# strip until a 'Web-Interface' is found.
		my $piece = pop (@dirs);
		while(scalar(@dirs) > 0 && $piece ne 'Web-Interface') {
			$piece = pop @dirs;
		}
		if (scalar(@dirs) > 0) {
			$path = join("/",@dirs)."/.Credentials/.credentials";
		}
		#carp("No credentials path provided from $caller.\nUsing default path : $path\n");
	}
	# path provided : must be valid.
	if ($path eq '' || !-f $path) {
		# cannot use default errorHandler here, because email needs credentials.
		confess("Could not load credentials from $caller.\nPath is invalid : $path\n");
	}
	# load in the credentials file.
	my $self = {};
	open IN, $path or croak("Could not open credentials file for reading: $path.\nCalled from : $caller\n");
	while (<IN>) {
		chomp;
		next if (substr($_,0,1) eq '#');
		next if ($_ !~ m/=/);
		my ($k,$v) = split(/=/,$_,2);
		$self->{$k} = $v;
	}
	close IN;
	# check some key values.
	if (!$self->{DBHOST} || !$self->{DBUSER} || !$self->{ADMINMAIL} || !$self->{SCRIPTDIR}) {
		use Data::Dumper;
		print Dumper($self);
		confess("Credentials file is missing critical information : $path\n");
	}
	# clean up some variables.
	$self->{SITEDIR} =~ s/\/$//;
	$self->{SCRIPTDIR} =~ s/\/$//;
	$self->{DATADIR} =~ s/\/$//;
	$self->{QUEUE} = defined($self->{QUEUE}) ? $self->{QUEUE} : 'batch';
	$self->{MYSQLTHREADS} =  defined($self->{MYSQLTHREADS}) ? $self->{MYSQLTHREADS} : 1;
    $self->{ACCOUNT} = defined($self->{ACCOUNT}) ? $self->{ACCOUNT} : '';
	# disable memcached.
	$self->{USEMEMCACHED} = 0;

    # SET PATH
    my $env_path = `echo \$PATH`;
    chomp($env_path);
    $self->{PATH} = defined($self->{PATH}) ? $self->{PATH}.":$env_path" : $env_path;

    bless $self,$pkg;
	return $self;

}
1;
