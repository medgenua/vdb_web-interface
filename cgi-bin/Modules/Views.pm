package Views;

use strict;
use warnings;

use ErrorHandler;
use Credentials;
use Database;

sub new {
	my $pkg = shift;
	my $self = {	'credentials' => Credentials->new(),
			'eh' => ErrorHandler->new()
	};
        bless $self,$pkg;
        return $self;
}

sub Clear {
	my ($self,$tables) = @_;
	if (!defined($tables) || $tables eq '') {
		$self->{eh}->warn("No tables provided while clearing views.\n");
		return;
	}
	if (!defined($self->{db})) {
		$self->{db} = Database->new();
	}
	my @t = split(/:/,$tables);
	my $query = "SELECT `name` FROM `VIEWS` WHERE `name` LIKE ?";	
	foreach my $tname (@t) {
		# format : VIEW_uid_sid_name
		my $rows = $self->{db}->Get($query,"VIEW\\_%\\_%\\_$tname");
		foreach my $row (@$rows){
			$self->{db}->do("DELETE FROM `VIEWS` WHERE `name` = '$row->[0]'");
			$self->{db}->do("DROP TABLE IF EXISTS `$row->[0]`");
		
		}
	}

}
1;
