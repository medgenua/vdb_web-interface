package ErrorHandler;

# usage : 
# use ErrorHandler
# replace 'die/warn/...()' by : ErrorHandler->$type($msg,$user_email);
# with $type being either:
#	- die : email and quit
#	- warn : email and continue

use strict;
use warnings;
use Carp;  
use Mail; 



# initialize
sub new{
	my ($pkg) = shift; 
	# get credentials.
	use Credentials; 
	my $credentials = new Credentials();
	my $self = {credentials => $credentials,WD => undef, TMP => undef}; 
	bless $self,$pkg;
	return $self;
}

sub die {
	my ($self,$msg,$notify) = @_;
	# send mail to admin.
	my $mail = Mail->new($self->{credentials}->{ADMINMAIL},
			     $self->{credentials}->{ADMINMAIL},
			     "VariantDB: Critical Problem");
	my $body = "Dear VariantDB Administrator,\n\n";
	$body	.= "VariantDB encountered a critical error which caused a process to die.\n";
	$body	.= "The following message was provided :\n\n";  
	$body 	.= "$msg\n\n";
	$body	.= "We retrieved the following stack trace:\n";
	my $i = 0;
	while ( (my @call_details = (caller($i++))) ){
		$body .= " ".$call_details[1].": line ".$call_details[2]." called function ".$call_details[3]."\n";
	}
	$mail->addtext($body);
	$mail->sendmail;
	# also notify the user? 
	if ($notify && $notify ne '') {
		my $mail = Mail->new($self->{credentials}->{ADMINMAIL},
			     $notify,
			     "VariantDB: Critical Problem");
		my $body = "Dear VariantDB User,\n\n";
		$body	.= "A critical error was encountered, which caused your action to end prematurely.\n";
		$body	.= "The system administrator was also informed, and will investigate the problem.\n";
		$body	.= "The following message was provided :\n\n";  
		$body 	.= "$msg\n\n";
	
		$mail->addtext($body);
		$mail->sendmail;
	}
	# check WD for status files ? 
	if ($self->{WD}){
		# status file ? 
		if (-f $self->{WD}."/status") {
			open OUT, ">".$self->{WD}."/status";
			print OUT "Error";
			close OUT;
		}
		print "opening $self->{WD}/error\n";
		open(my $fh, ">",$self->{WD}."/error");
		print $fh $msg;
		close($fh);
	}		
	# remove tmp dir ? 
	if ($self->{TMP}) {
		system("rm -Rf '".$self->{TMP}."'");
	}
	# die with stack trace.
	confess("Critical Error encountered. Email was sent to the system administrator. Message was : \n $msg\n");
}
sub warn {
	my ($self,$msg,$notify) = @_;
	# send mail to admin.
	my $mail = Mail->new($self->{credentials}->{ADMINMAIL},
			     $self->{credentials}->{ADMINMAIL},
			     "VariantDB: Non-Fatal Problem");
	my $body = "Dear VariantDB Administrator,\n\n";
	$body	.= "VariantDB encountered a non-fatal error. The following message was provided :\n\n";  
	$body 	.= "$msg\n\n";
	$body	.= "We retrieved the following stack trace:\n";
	my $i = 0;
	print STDERR "WARNING : a non-fatal problem was encountered.\n";
	print STDERR " Message: $msg\n";
	print STDERR " Stack Trace: \n";
	while ( (my @call_details = (caller($i++))) ){
		$body .= " ".$call_details[1].": line ".$call_details[2]." called function ".$call_details[3]."\n";
		print STDERR  "  ".$call_details[1].": line ".$call_details[2]." called function ".$call_details[3]."\n";
	}
	$mail->addtext($body);
	$mail->sendmail;
	# also notify the user? 
	if ($notify && $notify ne '') {
		my $mail = Mail->new($self->{credentials}->{ADMINMAIL},
			     $notify,
			     "VariantDB: Non-Fatal Problem");
		my $body = "Dear VariantDB User,\n\n";
		$body	.= "A non-fatal error was encountered. Please use results with care.\n";
		$body	.= "The system administrator was also informed, and will investigate the problem.\n";
		$body	.= "The following message was provided :\n\n";  
		$body 	.= "$msg\n\n";
		$mail->addtext($body);
		$mail->sendmail;
	}
}
sub StoreWD {
	my ($self,$wd) = @_;
	$self->{WD} = $wd;

}
	
sub StoreTMP {
	my ($self,$tmp) = @_;
	$self->{TMP} = $tmp;
}
1;
