package Cat;

# usage :
# use Cat;
# my cat = Cat->new();

# $first_line = $cat->first($file);
# $last_line = $cat->last($file);
# $all_lines = $cat->all($file);

# all entries are chomped.
# files are locked while reading.
use strict;
use warnings;
use Carp;
use ErrorHandler;

sub new {
	my $pkg = shift;
	my $caller = caller();
	my $self = {
		'eh' => ErrorHandler->new()
	};
	bless $self,$pkg;
	return $self;

}
sub first {
	my $self = shift;
	if (scalar(@_) == 0) {
		$self->{'eh'}->die("No path provided to cat module\n");
	}
	my $path = shift;
	if (!-f $path) { # symlink ?
		$self->{'eh'}->die("Provided path is invalid : '$path'\n");
	}
	open IN, $path or $self->{'eh'}->die("Could not open file for reading: '$path'\n");
	flock(IN,2) or $self->{'eh'}->die("Could not lock file: '$path'\n"); # lock is released on closing.
	my $line = <IN>;
	close IN;
	chomp $line;
	return $line;
}
sub last {
	my $self = shift;
	if (scalar(@_) == 0) {
		$self->{'eh'}->die("No path provided to cat module\n");
	}
	my $path = shift;
	if (!-f $path) { # symlink ?
		$self->{'eh'}->die("Provided path is invalid : '$path'\n");
	}
	open IN, $path or $self->{'eh'}->die("Could not open file for reading: '$path'\n");
	flock(IN,2) or $self->{'eh'}->die("Could not lock file: '$path'\n");
	my $line = <IN>;
	$line = $_ while <IN>;
	close IN;
	chomp $line;
	return $line;
}
sub all {
	my $self = shift;
	if (scalar(@_) == 0) {
		$self->{'eh'}->die("No path provided to cat module\n");
	}
	my $path = shift;
	if (!-f $path) { # symlink ?
		$self->{'eh'}->die("Provided path is invalid : '$path'\n");
	}
	open IN, $path or $self->{'eh'}->die("Could not open file for reading: '$path'\n");
	flock(IN,2) or $self->{'eh'}->die("Could not lock file: '$path'\n");
	chomp(my @lines = <IN>);
	close IN;
	return \@lines;
}
sub files {
	my $self = shift;
	if (scalar(@_) == 0) {
		$self->{'eh'}->die("No path provided to cat module\n");
	}
	my $path = shift;
	if (!-d $path) { # symlink ?
		$self->{'eh'}->die("Provided path is invalid (must be directory): '$path'\n");
	}
	my $pattern = shift;
	my @files;
	opendir my $dirh, $path or $self->{'eh'}->die("Could read dir '$path'\n");
	if ($pattern) {
		@files = grep {$_ =~ m/$pattern/ } readdir $dirh;
	}
	else {
		@files = readdir $dirh;
	}
	chomp(@files);
	closedir($dirh);
	return \@files;
}

1;
