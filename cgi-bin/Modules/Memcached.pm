package Memcached; 

use strict;
use warnings;

# use modules
use Cache::Memcached::Fast;
use ErrorHandler;
use Credentials;
use Database;


sub new {
	my $pkg = shift;
	my $self = {	'credentials' => Credentials->new(),
			'eh' => ErrorHandler->new()
	};
        bless $self,$pkg;
        return $self;
}

sub clear {
	my $self = shift;
	my $db = Database->new();
	my $sth = $db->prepare("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` = ?");
	while (my $tables = shift) {
		my @t = split(/:/,$tables);
		foreach my $tname (@t) {
			$db->do($sth,$tname);
		}
	}
	$sth->finish();
}

sub getMCidx {
	my $self = shift;
	my $idx_string = '';
	my $db = Database->new();
	my $sth = $db->prepare("SELECT `Table_IDX` FROM `NGS-Variants-Admin`.`memcache_idx` WHERE `Table_Name` = ?");
	while (my $tables = shift) {
		my @t = split(/:/,$tables);
		foreach my $tname (@t) {
			my $idx = $db->GetOne($sth,$tname);
			if (!defined($idx)) {
				$db->do("INSERT INTO `NGS-Variants-Admin`.`memcache_idx` (`Table_Name`,`Table_IDX`) VALUES (?,'0')",$tname);
				$idx = 0;
			}
			$idx_string .= "$idx:";
			
		}
	}
	if (length($idx_string) > 0) {
		$idx_string = substr($idx_string,0,-1);
	}
	return($idx_string);
}
1	
