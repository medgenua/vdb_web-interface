package Bash;

# create :
#my $bash = Bash->new()

# use :
# $bash->run("command -a option -b option2");
# or :
# $bash->run("command","-a","option","-b","option2");
#   => i.e. similar to the system() syntax.

## what it does:
# - use real bash instead of sh to run system call.
# - error handling : die on failure, with sending emails.

use strict;
use warnings;
use ErrorHandler;


sub new {
	my ($pkg) = shift;
	my $self = {
		'bash' => '/usr/bin/env bash',
		'eh' => ErrorHandler->new(),
	};
	bless $self, $pkg;
	return($self);
}
sub run {
	my $self = shift;
	my $cmd = undef;
	if (scalar(@_) == 0) {
		$self->{'eh'}->die("No program string provided to bash module\n");
	}
	elsif (scalar(@_) == 1) {
		$cmd = shift;
		if ($cmd =~ m/'/) {
			$cmd =~ s/'/'"'"'/g;
		}
		$cmd = $self->{bash}." -c 'set -o pipefail ; $cmd'";
	}
	else {
		$cmd = $self->{bash}." -c 'set -o pipefail ; ";

		foreach my $item (@_) {
			# leading/trailing whitespace
			$item =~ s/^\s+//g;
			$item =~ s/\s+$//g;
			# if no non-word or pure pipe, no quotes.
			if ($item eq '|' || $item =~ m/^[a-zA-Z0-9_\/\-\.]+$/) {
				$cmd .= "$item ";
			}
			# else if already quoted :
			elsif ($item =~ m/^".*"$/) {
				$cmd .= "$item ";
			}
			# else if already single quoted : error (not supported)
			elsif ($item =~ m/^'.*'$/) {
				if ($item !~  m/"/) {
					$self->{eh}->warn("Command syntax not supported : list-context with single quotes:\n".join(" ",@_)."\n"."Quotes were replaced by double quotes automatically, but this is not safe!\n");
					$item = '"'.substr($item,1,-1).'"';
				}
				else {
					$self->{eh}->die("Command syntax not supported : list-context with single quotes and double quotes:\n".join(" ",@_)."\n");
				}
				$cmd .= "$item ";
			}
			# else : quotes.
			else {
				$item =~ s/"/\\"/g;
				$cmd .= '"'.$item.'" ';
			}
		}
		$cmd .= "'";
	}
	#print "Executing command: \n";
	#print $cmd."\n";
	my $return = system($cmd);
	#print " => return value: $return\n";;
	if ($return != 0) {
		$self->{'eh'}->die("Error executing command. The following command was executed:\n$cmd\n => Exit status was $return\n");
	}
	return $return;
}

# same as above, but return the command STDOUT as an array
sub GetOutput {
	my $self = shift;
	my $cmd = undef;
	if (scalar(@_) == 0) {
		$self->{'eh'}->die("No program string provided to bash module\n");
	}
	elsif (scalar(@_) == 1) {
		$cmd = shift;
		if ($cmd =~ m/'/) {
			$cmd =~ s/'/'"'"'/g;
		}
		$cmd = $self->{bash}." -c 'set -o pipefail ; $cmd'";
	}
	else {
		$cmd = $self->{bash}." -c 'set -o pipefail ; ";

		foreach my $item (@_) {
			# leading/trailing whitespace
			$item =~ s/^\s+//g;
			$item =~ s/\s+$//g;
			# if no non-word or pure pipe, no quotes.
			if ($item eq '|' || $item =~ m/^[a-zA-Z0-9_\/\-\.]+$/) {
				$cmd .= "$item ";
			}
			# else if already quoted :
			elsif ($item =~ m/^".*"$/) {
				$cmd .= "$item ";
			}
			# else if already single quoted : error (not supported)
			elsif ($item =~ m/^'.*'$/) {
				if ($item !~  m/"/) {
					$self->{eh}->warn("Command syntax not supported : list-context with single quotes:\n".join(" ",@_)."\n"."Quotes were replaced by double quotes automatically, but this is not safe!\n");
					$item = '"'.substr($item,1,-1).'"';
				}
				else {
					$self->{eh}->die("Command syntax not supported : list-context with single quotes and double quotes:\n".join(" ",@_)."\n");
				}
				$cmd .= "$item ";
			}
			# else : quotes.
			else {
				$item =~ s/"/\\"/g;
				$cmd .= '"'.$item.'" ';
			}
		}
		$cmd .= "'";
	}
	#print "Executing command: \n";
	#print $cmd."\n";
	my @output = `$cmd`;
    my $return = ${^CHILD_ERROR_NATIVE};
	if ($return != 0) {
		$self->{'eh'}->die("Error executing command. The following command was executed:\n$cmd\n => Exit status was $return\n");
	}
    chomp(@output);
	return \@output;
}
1;
