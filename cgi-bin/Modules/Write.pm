package Write;

# usage :
# use Write;
# my writer = Write->new();

# write into file, overwrite current content.
# $writer->write($file,$data);
# append data to contents of file.
# writer->append($file,$data);

## note : no line-endings are added, they should be in $data.
## files are locked during writing.

## if $data is array_ref, each item is written to file (again, on line endings are added);
## for write() with empty $data, the file is emptied.


use strict;
use warnings;
use Carp;
use ErrorHandler;

sub new {
	my $pkg = shift;
	my $caller = caller();
	my $self = {
		'eh' => ErrorHandler->new()
	};
	bless $self,$pkg;
	return $self;

}

sub PrepareData {
	my $self = shift;
	# arrays of arguments, concat into a single array-reference
	if (scalar(@_) > 1) {
		my @tmp = @_;
		$_[0] = \@tmp;
	}
	# if no data : create empty value
	my $data;
	if (scalar(@_)) {
		$data = shift;
	}
	else {
		$data = '';
	}

	# hash_ref not allowed
	if (ref($data) eq 'HASH') {
		$self->{'eh'}->die("Invalid data type: Write Module takes a single referenced array or scalar as second argument");
	}
	if (ref($data) eq 'SCALAR' || ref($data) eq '') {
		# nothting to do.
		#$data = $data;
	} elsif (ref($data) eq 'ARRAY') {
		$data = join("",@$data);
	}
	else {
		$self->{eh}->die("Unknown reference type: ".ref($data));
	}
	return $data;
}

sub write {
	my $self = shift;
	if (scalar(@_) == 0) {
		$self->{'eh'}->die("No path provided to module\n");
	}
	my $path = shift;
	#if (-f $path) { # symlink ?
	#	$self->{'eh'}->warn("Overwriting existing file : '$path'\n");
	#}
	my $data = $self->PrepareData(@_);
	# open output file
	open OUT, ">$path" or $self->{'eh'}->die("Could not open file for writing: '$path'\n");
	flock(OUT,2) or $self->{'eh'}->die("Could not lock file: '$path'\n"); # lock is released on closing.
	print OUT $data;
	close OUT;
}


sub append {
	my $self = shift;
	if (scalar(@_) == 0) {
		$self->{'eh'}->die("No path provided to module\n");
	}
	my $path = shift;
	my $data = $self->PrepareData(@_);

	# open output file
	open OUT, ">>$path" or $self->{'eh'}->die("Could not open file for appending: '$path'\n");
	flock(OUT,2) or $self->{'eh'}->die("Could not lock file: '$path'\n"); # lock is released on closing.
    # scalar : write
	print OUT $data;
	close OUT;
}
1;
