#!/usr/bin/env python
from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger

import configparser
import argparse
import os
import sys
import subprocess
import json
import time

# libraries for cgi (form handler)
import cgi

# error handler
import cgitb


# error classes
class DataError(OSError):
    pass


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    # set path :
    if "PATH" in config["PATH"]:
        config["PATH"]["PATH"] = "{}:{}".format(config["PATH"]["PATH"], os.environ["PATH"])
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    # command line arguments
    help_text = """
        GOAL:
        #####
            - organize api query request
            - prepare files to run request
            - launch request.
        """
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    args = parser.parse_args()

    return config, args


def GetQueryKey(config, form):
    # initialize if not existing.
    os.makedirs(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", form["uid"].value),
        exist_ok=True,
    )
    while True:
        try:
            t = str(int(time.time()))
            os.makedirs(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", form["uid"].value, t
                ),
                exist_ok=False,
            )
            return t
        except Exception as e:
            time.sleep(1)
            log.info("Could not make query folder: {} : {}".format(t, e))
            continue


# write settings to file, add status etc.
def PrepareJob(form, queryKey, config):
    wd = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", form["uid"].value, queryKey
    )
    # put status file
    with open(os.path.join(wd, "status"), "w") as fh:
        fh.write("0")

    # print the form.
    with open(os.path.join(wd, "form"), "w") as fh:
        for k in form:
            fh.write("{}={}\n".format(k, form[k].value))
        # UI calls : put uid in apiKey field.
        fh.write("apiKey={}".format(form["uid"].value))

    # preloading of tables enabled ?
    if "pls" in form:
        with open(os.path.join(wd, "preload"), "w") as fh:
            for t in form["pls"].value.split("|"):
                fh.write("{}\n".format(t))
    # print apiKey to file
    # with open(os.path.join(wd, "apiKey"), "w") as fh:
    #    # in UI : "apiKey" holds the userid
    #    fh.write(form["uid"].value)

    # open permissions to allow scriptuser access.
    subprocess.check_call(["chmod", "-R", "777", wd])


def GetDBConnection(quiet=True):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)
    if not quiet:
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    return dbh


def ValidateUser(uid):
    r = dbh.runQuery("SELECT level FROM `Users` WHERE id = %s", uid)
    if not r:
        raise ValueError("User {} was not found".format(uid))
    elif int(r[0]["level"]) < 1:
        raise PermissionError("User {} is inactive".format(uid))
    return True


def RunQuery(form, queryKey, config):
    # cmd
    if "region" in form and form["region"].value != "false" and form["region"].value != "":
        cmd = "python Run.Region.Query.py -i {} -s UI -u {} ".format(queryKey, form["uid"].value)
    elif "project" in form and form["project"].value != "false" and form["project"].value != "":
        cmd = "python Run.Region.Query.py -i {} -s UI -u {} ".format(queryKey, form["uid"].value)
    else:
        cmd = "python Run.Sample.Query.py -i {} -s UI -u  {} ".format(queryKey, form["uid"].value)
    log_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs/VariantDB_Query_Wrapper.log"
    )
    # add conda env ?
    # try:
    #    cmd = "conda run -p {} {}".format(config["PATH"]["CONDA_ENV"], cmd)
    # except:
    #    pass

    # go to correct folder.
    cmd = "(cd {}/cgi-bin && {} >>{} 2>&1) &".format(
        config["LOCATIONS"]["SCRIPTDIR"],
        cmd,
        log_file,
    )
    log.info(cmd)

    # fork
    try:
        fork_pid = os.fork()
    except Exception as e:
        log.error("Could not fork : {}".format(e))
        raise e
    if fork_pid == 0:
        # configure to keep running.
        os.chdir("/")
        os.setsid()
        os.umask(0)
        log.debug("Forked & detached for query {}".format(queryKey))
        try:
            subprocess.check_call(cmd, shell=True)
            log.debug("Query {} processed".format(queryKey))
            # exit child.
            sys.exit(0)
        except Exception as e:
            log.error("Failed to run Query {} : {}".format(queryKey, e))
            sys.exit(1)
    else:
        return


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)
    # start logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_UI_Query_Wrapper",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        quiet=True,
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)
    conda = subprocess.check_output("echo $PATH", shell=True)
    log.info("Running path : {}".format(conda))
    # error handler
    cgitb.enable(display=0, logdir=config["LOGGING"]["LOG_PATH"])

    # PAYLOAD : handles both GET and POST options.
    form = cgi.FieldStorage()

    # connect to db
    dbh = GetDBConnection()

    # valid used specified ?
    try:
        ValidateUser(form["uid"].value)
    except Exception as e:
        log.error("Invalid userid provided :  {}".format(e))
        sys.exit(1)

    # get job key.
    try:
        queryKey = GetQueryKey(config, form)
    except Exception as e:
        log.error("Could not fetch queryKey: {}".format(e))
        sys.exit(1)
    log.info("query key :  {}".format(queryKey))
    # prepare job
    try:
        PrepareJob(form, queryKey, config)
    except Exception as e:
        log.error("Could no prepare query folder for {} : {}".format(queryKey, e))
        sys.exit(1)

    # Run job
    try:
        RunQuery(form, queryKey, config)
    except Exception as e:
        log.error("Could not launch query: {} : {}".format(queryKey, e))
        sys.exit(1)

    log.debug("Wrapping up the Query_Wrapper for query {}".format(queryKey))
    # print output.
    result = {"queryid": queryKey, "status": "Running"}
    print("Content-type: application/json\r\n")
    print(json.dumps(result))
    log.info("exiting parent script")
    sys.exit(0)
