#!/usr/bin/env python
from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import KillProcessTree
import configparser
import argparse
import os
import sys
import fcntl
import hashlib
import subprocess
import json
import time
from multiprocessing import Pool

# error classes
class DataError(OSError):
    pass


def GetDBConnection(quiet=True):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)
        if not quiet:
            log.info(
                "Connected to Database using Genome Build {}{}".format(
                    row[0]["StringName"], db_suffix
                )
            )
    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        sys.exit(1)

    return dbh


def ErrorCallback(result):
    # report but don't quit.
    log.error(result)
    # KillProcessTree(timeout=15,include_parent=True)


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    # set path :
    if "PATH" in config["PATH"]:
        config["PATH"]["PATH"] = "{}:{}".format(config["PATH"]["PATH"], os.environ["PATH"])
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Monitor query Queue 
            - execute requests.
        """
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    args = parser.parse_args()

    return config, args


def RunQuery(queryKey):
    log.info(f"Starting job {queryKey}")
    try:
        subprocess.check_call(
            [
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "api",
                    "query_results",
                    queryKey,
                    "qsub.sh",
                )
            ]
        )
    except Exception as e:
        log.error("Could not run api-job {} : {}".format(queryKey, e))
        # do not exit, process next item.


def WritePID(name, type="w"):
    pid = str(os.getpid())
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs", "{}.pid".format(name)), type
    ) as fh:
        fh.write(pid)


def CheckSampleStatus(queryKeys):
    backlog = set()
    ready_for_submission = set()
    for jobKey in queryKeys:
        log.info(f"Evaluate status for job {jobKey}")
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results", jobKey, "form"), "r"
        ) as form:
            info = dict()
            for line in form:
                k, v = line.rstrip().split("=", 1)
                info[k] = v
        # not a sample query :
        if not "sid" in info:
            ready_for_submission.add(jobKey)
            continue
        # sample not done yet
        sample_status = dbh.runQuery(
            "SELECT AnnotationStatus FROM `Samples` WHERE id = %s", info["sid"]
        )[0]["AnnotationStatus"]
        log.debug(f"Sample Status == {sample_status}")
        if not sample_status.startswith("Finished"):
            log.info(f"Sample {info['sid']} not finished yet : {sample_status}. Adding to backog")
            backlog.add(jobKey)
            continue
        # summary for user not done yet.
        if "apiKey" not in info or info["apiKey"] == "":
            log.error(f"No apikey provided for job {jobKey}")
            continue
        try:
            if "local_call" in info:
                user_status = dbh.runQuery(
                    "SELECT u.SummaryStatus FROM `Users` u WHERE u.id = %s", info["apiKey"]
                )[0]["SummaryStatus"]
            else:
                user_status = dbh.runQuery(
                    "SELECT u.SummaryStatus FROM `Users` u WHERE u.apiKey = %s", info["apiKey"]
                )[0]["SummaryStatus"]
        except Exception as e:
            log.error(f"Failed to identify user for job {jobKey}: {e}")
            continue
        log.debug(f"User Summary status == {user_status}")
        if user_status < 2:
            log.info(f"Summary Data not ready for user {info['apiKey']}. Add {jobKey} to backlog")
            backlog.add(jobKey)
            continue
        # ok.
        ready_for_submission.add(jobKey)
    return ready_for_submission, backlog


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # write out the pid.
    try:
        WritePID("VariantDB_Query_Runner")
    except Exception as e:
        print("Unable to set pid. Exiting : {}".format(e))
        sys.exit(1)

    # start logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_Query_Runner",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        quiet=True,
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)
    conda = subprocess.check_output("echo $PATH", shell=True)
    log.debug("Running PATH : {}".format(conda))

    dbh = GetDBConnection()

    # setup the workers pool
    pool = Pool(processes=4)

    # endless loop.
    backlog = set()
    while True:
        # read queue
        try:
            if not os.path.exists(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue")
            ):
                log.info(
                    "Queue file not found: {}".format(
                        os.path.join(
                            config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue"
                        )
                    )
                )
                time.sleep(30)
                continue
            # get queue:
            ## lcok queue file.
            log.debug("Requesting lock.")
            with open(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue.lck"),
                "w+",
            ) as qh:
                fcntl.lockf(qh, fcntl.LOCK_EX)
                log.debug("Lock obtained.")
                # write to queue file:
                with open(
                    os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue"),
                    "r",
                ) as q:
                    queryKeys = q.readlines()
                    queryKeys = [x.rstrip() for x in queryKeys if x.rstrip()]
                # add backlog to list
                queryKeys.extend(backlog)
                backlog = set()
                # check annotation/summary status of samples :
                queryKeys, backlog = CheckSampleStatus(queryKeys)

                # flush queue. items are either queued for processing (queryKeys) or in the backlog
                open(
                    os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue"),
                    "w",
                ).close()
                # nothing to do in this cycle. everything is backlogged
                if len(queryKeys) == 0:
                    # nothing to
                    fcntl.lockf(qh, fcntl.LOCK_UN)
                    time.sleep(5)
                    continue

        except Exception as e:
            log.error(f"Failed to process queue file: {e}")
            sys.exit(1)
        # queue updated & released. process fetched item.
        log.info(f"Launching {len(queryKeys)} api jobs ")
        # launch processes
        pool.map_async(RunQuery, list(queryKeys), chunksize=1, error_callback=ErrorCallback)
        time.sleep(1)
