#!/usr/bin/env python
from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger, add_filestream
from CMG.Utils import Re

import configparser
import argparse
import os
import sys
import fcntl
import subprocess
import json
import time
import urllib
from dataclasses import dataclass, field

# libraries for cgi (form handler)
# error handler not loaded, handled by UZALogger
import cgi


# error classes
class DataError(OSError):
    pass


# classes are globab by default
@dataclass
class Variables:
    uid: int = None
    tmpdir: str = None
    datadir: str = None
    queryKey: int = None
    nr_samples: int = 0


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    # set path :
    if "PATH" in config["PATH"]:
        config["PATH"]["PATH"] = "{}:{}".format(config["PATH"]["PATH"], os.environ["PATH"])
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    if "CONDA_ENV" in config["PATH"]:
        config["PATH"]["PATH"] = "{}/bin:{}".format(
            config["PATH"]["CONDA_ENV"], config["PATH"]["PATH"]
        )
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    # command line arguments
    help_text = """
        GOAL:
        #####
            - Import VCF files into Database
            
        """
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    args = parser.parse_args()

    return config, args


# get unique key of the job.
def GetQueryKey(config):
    # initialize if not existing.
    if not os.path.exists(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/import_jobs/.job_counter")
    ):
        try:
            with open(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/import_jobs/.job_counter"),
                "w",
            ) as fh:
                fh.write("0")
        except Exception as e:
            log.error("Could not initialize api import-job_counter file: {}".format(e))
            sys.exit(1)
    # get lock on counter value.
    lock = open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/import_jobs/.job_counter.lck"), "r+"
    )
    fcntl.lockf(lock, fcntl.LOCK_EX)
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/import_jobs/.job_counter"), "r"
    ) as fh:
        queryKey = int(fh.readline().rstrip()) + 1
        while os.path.exists(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/import_jobs/{}".format(queryKey))
        ):
            queryKey += 1
    os.makedirs(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/import_jobs/{}".format(queryKey))
    )
    # update counter
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/import_jobs/.job_counter"), "w"
    ) as fh:
        fh.write(str(queryKey))
    # close lock.
    lock.close()
    # return key
    return queryKey


# check if user has access to required resources. Should be ok, but double check.
def CheckAccess():
    # wd must exists
    if not os.path.isdir(variables.datadir):
        raise ValueError("Provided working directory not found: {}".format(variables.datadir))
    # wd must be writable.
    if not os.access(variables.datadir, os.W_OK):
        raise ValueError("Provided working directory is not writable: {}".format(variables.datadir))

    # provided uid must be equal to uid in WD
    with open(os.path.join(variables.datadir, "uid.txt"), "r") as fh:
        wd_uid = fh.readline().rstrip()
    if not wd_uid == variables.uid:
        raise ValueError("Mismatch in userd : {} != {}".format(wd_uid, variables.uid))

    # valid user
    u = dbh.runQuery("SELECT id FROM `Users` WHERE id = %s", variables.uid)
    if not u:
        raise ValueError("User {} not found".format(variables.uid))

    # ok.
    return True


def GetDBConnection(quiet=True):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)

    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)
    if not quiet:
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    return dbh


def Fail(msg):
    log.error(msg)
    print("Content-type: application/json\r\n")
    if "sudo" in msg:
        msg = "Error messsage masked"
    print(json.dumps({"ERROR": msg, "status": "error"}))
    sys.exit(1)


def SetStatus(status, msg=None):
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "api/import_jobs/{}".format(variables.queryKey),
            "status",
        ),
        "w",
    ) as fh:
        fh.write(status)
    if msg:
        with open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "api/import_jobs/{}".format(variables.queryKey),
                "msg",
            ),
            "w",
        ) as fh:
            fh.write(status)


if __name__ == "__main__":
    # PAYLOAD : handles both GET and POST options.
    form = cgi.FieldStorage()
    try:
        config, args = LoadConfig()
    except DataError as e:
        print("Content-type: application/json\r\n")
        print(json.dumps(e))
        sys.exit(1)
    except Exception as e:
        raise (e)
    # start logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="Import_Wrapper",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        quiet=True,
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)

    # class to hold some globaly used variables
    variables = Variables()

    source = form["s"].value
    variables.datadir = form["datadir"].value
    variables.uid = form["uid"].value
    # todo : add the build
    dbh = GetDBConnection()

    # check access
    try:
        CheckAccess()
    except Exception as e:
        Fail("Access problem for uid {} ; wd {} : {}".format(variables.uid, variables.datadir, e))

    # get a key
    try:
        variables.queryKey = GetQueryKey(config)
        # PrepareJob(form, sid, queryKey, config)
        variables.tmpdir = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "api",
            "import_jobs",
            str(variables.queryKey),
            "scratch",
        )
        os.makedirs(variables.tmpdir, exist_ok=True)
    except Exception as e:
        Fail("Could not get import key for wd {} : {}".format(variables.datadir, e))

    try:
        pid = os.fork()
    except OSError as e:
        log.error("Could not fork : {}".format(e))
        raise e

    if pid == 0:
        # in child.
        os.chdir("/")
        os.setsid()
        os.umask(0)
        cmd = "(cd {} && python VariantDB_Importer.py -d '{}' -u {} -q {} > /dev/null 2>&1) & ".format(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "cgi-bin"),
            variables.datadir,
            variables.uid,
            variables.queryKey,
        )
        log.debug(cmd)
        try:
            subprocess.check_call(cmd, shell=True, stdout=None, stdin=None)
        except Exception as e:
            # log.error("Could not run Process wd {} : {}".format(datadir, e))
            SetStatus("error", e)
            Fail("Could not run Process wd {} : {}".format(variables.datadir, e))
        sys.exit(0)

    # continue parent.
    log.debug("Constructing output json")
    # format results.
    result = {
        "job_key": variables.queryKey,
        "result": "started",
    }
    print("Content-type: application/json\r\n")
    print(json.dumps(result))
    sys.exit(0)
