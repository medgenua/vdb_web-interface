from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger, add_filestream
from CMG.Utils import Re, CheckVariableType

import configparser
import argparse
import os
import sys
import fcntl
from multiprocessing import Pool
import xml.etree.ElementTree as ET
import json
import pprint
from copy import deepcopy
import importlib
from functools import partial
import time
import datetime
from operator import getitem
import html

# error classes


class DataError(Exception):
    pass


# class ValueError(OSError):
#    pass

# TODO :
#   - saved
#   - export as table
#   - fetching next slice ?
#   - dynamic dominant
#   - dynamic recessive
#   - dynamic compound
#   - aftermatch


# comments class (classes are global)
class Comments:
    def __init__(self):
        self.list = list()

    def add(self, entry):
        if isinstance(entry, list):
            self.list.extend(entry)
        else:
            self.list.append(entry)

    def all(self):
        return self.list

    def filter(self, term):
        f = [x for x in self.list if term in x]
        return f


# start the comments
comments = Comments()

# inhouse regex class
re = Re()

# STRATEGY :
# 1. sample_vids into TMP table
# 2. filter variants into NEW TMP TABLE
# 3. extract annotations in THIRD TMP TABLE
# 4. return & format data depending out requested format (UI, SAVE, API).


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    # command line arguments
    help_text = """
        GOAL:
        #####
            - run a SAMPLE based query
            - add requested annotations
            - return data in requested format.
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # working directory
    parser.add_argument(
        "-i",
        "--item",
        required=True,
        help="Query ID, numeric value.",
    )

    parser.add_argument("-u", "--user", required=False, help="User ID, numeric, only for UI-calls.")
    parser.add_argument("-s", "--source", required=True, help="Query Source. Values : UI, API")
    parser.add_argument("-l", "--loglevel", required=False, help="Override loglevel")
    args = parser.parse_args()
    # if loglevel set : overrule value in config.
    if args.loglevel:
        config["LOGGING"]["LOG_LEVEL"] = args.loglevel
    # defined the prefix for this query for tmp_results
    config.add_section("RUNTIME")
    config.set("RUNTIME", "tmp_prefix", "QueryResults-{}-{}".format(args.source, args.item))
    return config, args


def ReadForm(directory):
    # folder must exist
    if not os.path.exists(directory):
        raise ("provided form-directory does not exist: {}".format(directory))
    # form data must exist:
    if not os.path.exists(os.path.join(directory, "form")):
        raise ("form-file does not exists in : {}".format(directory))
    # read in the form.
    form = dict()
    with open(os.path.join(directory, "form"), "r") as fh:
        for line in fh:
            varname, data = line.rstrip().split("=", 1)
            form[varname] = data
    return form


def PruneQueue(config, args):
    # lock:
    lock = open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue.lck"),
        "w+",
    )
    fcntl.lockf(lock, fcntl.LOCK_EX)
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue"), "r"
    ) as fh:
        queue = list()
        for line in fh:
            if str(line.rstrip()) != str(args.item):
                queue.append(line.rstrip())
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue"), "w"
    ) as fh:
        fh.write("\n".join(queue))


def GetDBConnection(quiet=False, build=None):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)

    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)
    # switch build? (deactivate for now)
    if build:
        log.info(f"Setting build in main query script: {build}")
        # should exist ; includes suffix if needed.
        try:
            dbh.select_db("NGS-Variants-{}".format(build))
            comments.add("NOTICE: Using Genome Build {}".format(build))
            if not quiet:
                log.info("Connected to Database using provided Genome Build {}".format(build))

        except MySqlError as e:
            log.error("Invalid Genome build : {} : {}".format(build, e))
            # set status
            with open(os.path.join(args.directory, "status"), "w") as fh:
                fh.write("error")
            sys.exit(1)
    elif not quiet:
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    return dbh


def CheckPermissions(args, form):
    if "apiKey" not in form or form["apiKey"] == "":
        raise ValueError("No apikey provided")

    # internal api call or UI call:
    if "local_call" in form or args.source.upper() == "UI":
        rows = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  u.id, u.FirstName, u.LastName, u.level,u.Affiliation, SummaryStatus, LastSummaryUpdate FROM `Users` u WHERE u.id = %s",
            form["apiKey"],
        )
        if not rows:
            raise ValueError("User ID does not respond to valid user: {}".format(form["apiKey"]))
    # external api call:
    elif args.source.upper() == "API":
        rows = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  u.id, u.FirstName, u.LastName, u.level, u.Affiliation , SummaryStatus, LastSummaryUpdate FROM `Users` u WHERE u.apiKey = %s",
            form["apiKey"],
        )
        if not rows:
            raise ValueError("Api Key does not respond to valid user: {}".format(form["apiKey"]))
    form.update(
        {
            "uid": rows[0]["id"],
            "fname": rows[0]["FirstName"],
            "lname": rows[0]["LastName"],
            "ulevel": rows[0]["level"],
            "username": "{} {}".format(rows[0]["LastName"], rows[0]["FirstName"]),
            "affiliation": rows[0]["Affiliation"],
        }
    )
    # access to provided sample?
    u_s = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  pu.editvariant, pu.editclinic, pu.editsample FROM `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON ps.pid = pu.pid WHERE pu.uid = %s AND ps.sid = %s",
        (form["uid"], form["sid"]),
    )
    if not u_s:
        raise ValueError("No access to sample {} by user {}".format(form["sid"], form["uid"]))
    form.update({x: u_s[0][x] for x in u_s[0].keys()})
    # add uid+sid to args.
    args.userid = rows[0]["id"]
    args.sid = form["sid"]

    # summary ok ?
    if rows[0]["SummaryStatus"] < 2:
        if "vf_ackn" in form and str(form["vf_ackn"]) == "1":
            log.warning(
                "Variant Frequency data is not up to date. User was aware and acknowledged this during querying"
            )
            log.warning(f"Last Update of frequency data was : {rows[0]['LastSummaryUpdate']}")
        else:
            log.warning(
                f"Variant Frequency data is not up to date. Last Update of frequency data was : {rows[0]['LastSummaryUpdate']} "
            )
        args.summary = False
        comments.add("WARNING: Variant Frequencies are not up to date!")
    else:
        args.summary = True
    return form, args


def CheckFilterValidity(i, form, filter_config):
    filter_root = filter_config.getroot()
    # not set (deleted)
    if "category{}".format(i) not in form:
        return False
    category = form["category{}".format(i)]
    # params not set (incomplete)
    if "param{}".format(i) not in form:
        return False
    param = form["param{}".format(i)]
    # first level argument not set.
    if not ET.iselement(filter_root.find("{}/{}".format(category, param))):
        return False
    # text field not set && needed
    if (
        ET.iselement(filter_root.find("{}/{}/settings/need".format(category, param)))
        and filter_root.find("{}/{}/settings/need".format(category, param)).text == "text"
    ):
        if f"value{i}" not in form:
            return False

    # deprecated ?
    if ET.iselement(filter_root.find("{}/{}/deprecated".format(category, param))):
        log.warning(
            "Filter {}/{} is deprecated. Annotations are no longer updated.".format(category, param)
        )
        comments.add(
            "WARNING: Filter {}/{} is deprecated. Annotations are no longer updated.".format(
                category, param
            )
        )
    # license needed?
    if ET.iselement(filter_root.find("{}/{}/license".format(category, param))):
        license = filter_root.find("{}/{}/license".format(category, param)).text
        l_r = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  l.lid FROM `License` l JOIN `Users_x_License` ul ON ul.lid = l.lid WHERE ul.uid = %s AND l.LicenseName = %s",
            (form["uid"], license),
        )
        if not l_r:
            log.warning(
                "Filter {}/{} is licensed, and user is not allowed to use it".format(
                    category, param
                )
            )
            comments.add(
                "WARNING: Filter {}/{} is licensed and you are not allowed to use it.".format(
                    category, param
                )
            )
            return False
    if ET.iselement(filter_root.find("{}/license".format(category))):
        license = filter_root.find("{}/license".format(category)).text
        l_r = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  l.lid FROM `License` l JOIN `Users_x_License` ul ON ul.lid = l.lid WHERE ul.uid = %s AND l.LicenseName = %s",
            (form["uid"], license),
        )
        if not l_r:
            log.warning(
                "Filter {}/{} is licensed, and user is not allowed to use it".format(
                    category, param
                )
            )
            comments.add(
                "WARNING: Filter {}/{} is licensed and you are not allowed to use it.".format(
                    category, param
                )
            )
            return False
    # Gene Symbol filter, check if it exists.
    if "_Gene_Symbol" in param or "_GeneID" in param:
        log.debug(f"Working on {category}/{param}")
        # the data file
        try:
            datafile, column = filter_root.find(f"{category}/{param}/settings/datafile").text.split(
                ":"
            )
            datafile = datafile.replace("%scriptdir", config["LOCATIONS"]["SCRIPTDIR"]).replace(
                "%build", form["build"]
            )
            db_suffix = config["DATABASE"].get("DBSUFFIX", "")
            if db_suffix:
                datafile = datafile.replace(f"_{db_suffix}", "")
            value = form[f"value{i}"]
            # none provided : error.
            if value.strip() == "":
                log.warning(
                    "Filter {}/{} requires a Symbol, but none was provided".format(category, param)
                )
                comments.add(
                    "WARNING: Filter {}/{} requires a Symbol, but none was provided".format(
                        category, param
                    )
                )
                return False
            log.debug(f"Checking {datafile} : column {column} for presence of {value}")
            # load genes
            genes = set()
            with open(datafile, "r") as fh:
                for line in fh:
                    gene = line.rstrip().split("\t")[int(column) - 1]
                    genes.add(gene.upper())
        except Exception as e:
            log.warning(e)
            return False
        # check provided values
        valid_genes = 0
        for gene in value.strip().split(","):
            if gene.strip().upper() not in genes:
                log.warning(f"Provided gene not recognized: {gene}")
                comments.add(f"WARNING: Provided gene not recognized: {gene}")
            else:
                valid_genes += 1
        if not valid_genes:
            log.warning(f"No valid gene symbols left.")
            comments.add(f"WARNING: No valid gene symbols left.")
            return False

    return True


def AddDynamic(filter_values, filters, filter_config, i):
    negate = filter_values["negate"] if "negate" in filter_values else ""
    param = filter_values["param"]
    filter_root = filter_config.getroot()
    # missing config : skip
    if not os.path.exists(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Filter/dynamic_{}.py".format(param.lower()))
    ):
        log.warning("Filter Dynamic/{} is not defined: skipping".format(param))
        comments.add("WARNING: Filter Dynamic/{} is not defined: skipping.".format(param))
        return filters
    # dynamic + aftermatch ?
    if ET.iselement(filter_root.find("Dynamic_Filters/{}/settings/Match_Afterwards".format(param))):
        filters["after"][str(i)] = {
            "style": "dynamic",
            "negate": negate,
            "param": param,
            "values": filter_values,
            "settings": filter_config,
        }
        return filters
    else:
        filters["dynamic"][str(i)] = {
            "param": param,
            "negate": negate,
            "values": filter_values,
            "settings": filter_config,
        }
        return filters


# same as ConstructFilter, but without the SELECT/FROM
def ConstructQualityFilter(filter_settings):
    filter_root = filter_settings["config"]
    negate = filter_settings["values"]["negate"] if "negate" in filter_settings["values"] else ""
    query_args = list(filter(None, filter_settings["values"]["argument"].split("__")))
    result = ""
    where = filter_root.find("settings/where").text
    # arguments taken from query (eg Filter field) : implies multi-select + IN ()
    if ET.iselement(filter_root.find("settings/ArgumentsFromQuery")):
        where = where.replace(r"%replace", "'{}'".format("','".join(query_args)))
        result = "{} {}".format(negate, where)

    # options provided ? <, =, >
    if ET.iselement(filter_root.find("options")):
        # no multi-select
        if (
            not ET.iselement(filter_root.find("settings/allowmulti"))
            or filter_root.find("settings/allowmulti").text.lower() != "true"
        ):
            # %replace => selected option (first arg element)
            where = where.replace(
                r"%replace", filter_root.find("options/{}".format(query_args[0])).text
            )

        # multi-select
        else:
            # replace is the "in" string : from the args-array.
            a_list = []
            for a in query_args:
                a_list.append(filter_root.find("options/{}".format(a)).text)
            if ET.iselement(filter_root.find("settings/raw_options")) and filter_root.find("settings/raw_options").text.lower() == "true":
                a_str = "{}".format(" ".join(a_list))
            else:
                a_str = "'{}'".format("','".join(a_list))
            where = where.replace(r"%replace", a_str)

    # for Separate query options : the SQ_option contains a part of the sql data, and must go into the %value placeholder.
    if ET.iselement(filter_root.find("SQ_options")):
        where = where.replace(r"%value", filter_settings["values"]["value"])

    # text field with value(s) ?
    if (
        ET.iselement(filter_root.find("settings/need"))
        and filter_root.find("settings/need").text == "text"
    ):
        # where part contains a %value to replace with contents of input field.
        values = filter_settings["values"]["value"]
        # split? => add quotes
        if (
            ET.iselement(filter_root.find("settings/split_on"))
            and filter_root.find("settings/split_on").text != ""
        ):
            # split & rejoin with quotes, removing empty values.
            values = "','".join(
                list(
                    filter(
                        None,
                        values.replace(" ", "").split(filter_root.find("settings/split_on").text),
                    )
                )
            )

        # fill in.
        where = where.replace(r"%value", values)

    # get user details (#samples etc)
    where = GetUserNumbers(where)

    result = "{} {}".format(negate, where)
    return result


def ConstructFilter(filter_settings, logic_type, sid, parent_table=None, last_tmp_table=None):
    filter_root = filter_settings["config"]
    negate = filter_settings["values"]["negate"] if "negate" in filter_settings["values"] else ""
    query_args = (
        list(filter(None, filter_settings["values"]["argument"].split("__")))
        if "argument" in filter_settings["values"]
        else list()
    )
    where = filter_root.find("settings/where").text
    # arguments taken from query (eg Filter field) : implies multi-SELECT + IN ()
    if ET.iselement(filter_root.find("settings/ArgumentsFromQuery")):
        where = where.replace(r"%replace", "'{}'".format("','".join(query_args)))

    # options provided ? <, =, >
    elif ET.iselement(filter_root.find("options")):
        # no multi-select
        if (
            not ET.iselement(filter_root.find("settings/allowmulti"))
            or filter_root.find("settings/allowmulti").text.lower() != "true"
        ):
            # %replace => selected option (first arg element)
            where = where.replace(
                r"%replace", filter_root.find("options/{}".format(query_args[0])).text
            )

        # multi-select
        else:
            # replace is the "in" string : from the args-array.
            a_list = []
            for a in query_args:
                a_list.append(filter_root.find("options/{}".format(a)).text)
            if ET.iselement(filter_root.find("settings/raw_options")) and filter_root.find("settings/raw_options").text.lower() == "true":
                a_str = "{}".format(" ".join(a_list))
            else:
                a_str = "'{}'".format("','".join(a_list))
            where = where.replace(r"%replace", a_str)

    # for Separate query options : the SQ_option contains a part of the sql data, and must go into the %value placeholder.
    if ET.iselement(filter_root.find("SQ_options")):
        where = where.replace(r"%value", filter_settings["values"]["value"])

    # text field with value(s) ?
    if (
        ET.iselement(filter_root.find("settings/need"))
        and filter_root.find("settings/need").text == "text"
    ):
        # where part contains a %value to replace with contents of input field.
        if "value" not in filter_settings["values"]:
            # missing fields.
            log.warning(
                "Incomplete filter rule : {}/{} : Skipping".format(
                    filter_settings["values"]["category"], filter_settings["values"]["param"]
                )
            )
            return False
        values = filter_settings["values"]["value"]
        # split? => add quotes
        if (
            ET.iselement(filter_root.find("settings/split_on"))
            and filter_root.find("settings/split_on").text != ""
        ):
            # split & rejoin with quotes, removing empty values.
            values = "','".join(
                list(
                    filter(
                        None,
                        values.replace(" ", "").split(filter_root.find("settings/split_on").text),
                    )
                )
            )

        # fill in.
        where = where.replace(r"%value", values)

    # subselect query values (ssqv) ?
    if r"%ssqv" in where:
        filter_settings["values"]["ssqv"] = filter_settings["values"]["ssqv"].rstrip("__")
        where = where.replace(r"%ssqv", filter_settings["values"]["ssqv"])

    # get user details (#samples etc)
    where = GetUserNumbers(where)

    # a single `from` :
    if "," not in filter_root.find("settings/from").text:
        # combine with prior variants :
        query = "SELECT  HIGH_PRIORITY  `{}`.vid FROM `{}` JOIN `%join%` ON `{}`.vid = `%join%`.vid WHERE {} {}".format(
            filter_root.find("settings/from").text,
            filter_root.find("settings/from").text,
            filter_root.find("settings/from").text,
            negate,
            where,
        ).replace(
            "`Variants`.vid", "`Variants`.id"
        )
    else:

        from_tables = filter_root.find("settings/from").text.replace(" ", "").split(",")
        from_string = "`{}`".format(f"` JOIN `".join(from_tables))
        # mandatory : the first table listed contains `vid` column for joining.
        query = "SELECT  HIGH_PRIORITY  `{}`.vid FROM {} JOIN `%join%` ON `{}`.vid = `%join%`.vid AND {} WHERE {} {}".format(
            from_tables[0],
            from_string,
            from_tables[0],
            filter_root.find("settings/join").text,
            negate,
            where,
        ).replace(
            "`Variants`.vid", "`Variants`.id"
        )
    # notes above:
    #   - only table that doesn't follow this logic is Variants => replace vid to id
    #   - NEGATE only affects first part of WHERE if there is "AND" in the where part in filter config. use brackets if needed.
    # AND :
    if logic_type == "AND":
        # last tmp takes precedence of "full" parental
        if last_tmp_table:
            query = query.replace("%join%", last_tmp_table)
        elif parent_table:
            query = query.replace("%join%", parent_table)
        # some queries look accross samples (eg DC == patho in all samples) : these cannot be joined to V_x_S in the regular way.
        # they are identified as "no_phase1" queries in the config.
        elif ET.iselement(filter_root.find("settings/no_phase1")):
            query = re.sub(r"JOIN .*? WHERE", "WHERE", query)
            from_tables = filter_root.find("settings/from").text.replace(" ", "").split(",")
            query += " AND `{}`.vid IN (SELECT   vid FROM `Variants_x_Samples` WHERE sid = '%sid' )".format(
                from_tables[0]
            )
        else:
            # join with Variants_x_Samples.
            query = query.replace("%join%", "Variants_x_Samples")
            query += " AND `Variants_x_Samples`.sid = {}".format(sid)
    elif logic_type == "OR":
        # only parent table
        if parent_table:
            query = query.replace("%join%", parent_table)
        # some queries look accross samples (eg DC == patho in all samples) : these cannot be joined to V_x_S in the regular way.
        # they are identified as "no_phase1" queries in the config.
        elif ET.iselement(filter_root.find("settings/no_phase1")):
            query = re.sub(r"JOIN .*? WHERE", "WHERE", query)
            from_tables = filter_root.find("settings/from").text.replace(" ", "").split(",")
            query += " AND `{}`.vid IN (SELECT   vid FROM `Variants_x_Samples` WHERE sid = '%sid' )".format(
                from_tables[0]
            )
        else:
            # join with Variants_x_Samples.
            query = query.replace("%join%", "Variants_x_Samples")
            query += " AND `Variants_x_Samples`.sid = {}".format(sid)

    # finalize query
    if r"%userid" in query:
        query = query.replace(r"%userid", str(args.userid))
    if r"%sid" in query:
        query = query.replace(r"%sid", sid)
    log.debug(query)
    return query


def GetUserNumbers(where):
    urow = dbh.runQuery("SELECT  HIGH_PRIORITY  * FROM `Users` u WHERE u.id = %s", args.userid)[0]
    # nr of controls
    if r"%nrcontrols" in where:
        where = where.replace(r"%nrcontrols", str(urow["nrControls"]))
    # nr of samples
    if r"%mynrsamples" in where:
        where = where.replace(r"%mynrsamples", str(urow["nrSamples"]))
    # nr of females
    if r"%mynrfemales" in where:
        where = where.replace(r"%mynrfemales", str(urow["nrFemales"]))
    # nr of males
    if r"%mynrmales" in where:
        where = where.replace(r"%mynrmales", str(urow["nrMales"]))
    # live subqueries:
    if r"%mysamples" in where:
        sq = "SELECT   `Projects_x_Samples`.sid FROM `Projects_x_Users` JOIN `Projects_x_Samples` "
        sq += "ON `Projects_x_Users`.pid = `Projects_x_Samples`.pid "
        sq += "WHERE `Projects_x_Users`.uid = {}".format(args.userid)
        where = where.replace(r"%mysamples", sq)

    return where


def GetSettings(form, i):
    result = {}
    for k in form:
        # dynamic options:
        # if re.search(r"^do{}_(.+)$".format(i), k):
        if k == f"nr_do_{i}":
            # loop dyn ops.
            for j in range(1, (int(form[f"nr_do_{i}"]) + 1)):
                result[form[f"do{i}_{j}_name"]] = form[f"do{i}_{j}_value"]

        # regular items.
        elif re.search(r"(\D+){}$".format(i), k):
            result[re.last_match.group(1)] = form[k]
    return result


def ParseLogic(tree):

    # make a nested dict per tree level
    result = {
        "type": "",
        "filters": [],
        "children": [],
    }
    # root level
    result["type"] = tree["type"]  # logic type
    for child in tree["children"]:
        if child["id"].startswith("rule_"):
            result["filters"].append(child["id"][5:])
        elif child["id"].startswith("j1_"):
            result["children"].append(ParseLogic(child))
    # return
    return result


def FlattenLogic(logic, l_idx=0):
    logic_new = deepcopy(logic)
    # for logic branches without children, l_idx = None
    if not l_idx:
        l_idx = 0
    l_idx += 1
    log.debug(f"Logic Level : {l_idx}")
    logic_new["level_id"] = l_idx
    del_idx = []
    if len(logic["children"]) == 0 and len(logic["filters"]) == 0:
        log.debug(f"No children in logic for level {l_idx}")
        return None, None
    for idx, child in enumerate(logic["children"]):
        if child["type"].startswith("logic"):
            # recursive
            child, l_idx = FlattenLogic(child, l_idx)
            if not child:
                del_idx.append(idx)
                continue
            # if same type as parent : move up.
            if child["type"] == logic["type"]:
                # filters : append
                logic_new["filters"].extend(child["filters"])
                # children : append
                logic_new["children"].extend(child["children"])
                # delete child
                del_idx.append(idx)
            # else: keep flattened version
            logic_new["children"][idx] = child
    # pop the moved children, reverse to maintain idx.
    for idx in sorted(del_idx, reverse=True):
        logic_new["children"].pop(idx)
    return logic_new, l_idx


def PrepareFilters(config, form):
    # read the filter xml.
    filter_config = ET.parse(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Filter/Filter_Options.xml")
    )
    filter_root = filter_config.getroot()

    # read the tree => levels and logic.
    logic = ParseLogic(json.loads(form["logic"])[0])
    log.debug(logic)
    logic, l_idx = FlattenLogic(logic)
    log.debug(logic)
    # filters to do :
    #    phase 1 : straight on V_x_S ;
    #    phase 2 : normal filters ;
    #    phase3 : custom annotations;
    #    after : aftermatch queries.:
    filters = {
        "dynamic": {},
        "phase1": {},
        "phase2": {},
        "phase3": {},
        "after": {},
    }
    for i in range(0, int(form["listed"]) + 1):
        log.debug(f"Preparing filter {i}")
        # should filter be skipped ?
        valid = CheckFilterValidity(i, form, filter_config)
        if not valid:
            continue
        # filter details:
        filter_values = GetSettings(form, i)

        # dynamic filters
        if filter_values["category"] == "Dynamic_Filters":
            filters = AddDynamic(filter_values, filters, filter_config, i)
            continue

        # CUSTOM VCF FILTERS (phase 3) :
        #  => TODO

        # aftermatch queries :
        if ET.iselement(
            filter_root.find(
                "{}/{}/settings/Match_Afterwards".format(
                    filter_values["category"], filter_values["param"]
                )
            )
        ):
            filters["after"][str(i)] = {
                "values": filter_values,
                "config": filter_root.find(
                    "{}/{}".format(filter_values["category"], filter_values["param"])
                ),
            }
            continue

        # regular filters : phase 1 : QC category:
        if (
            filter_values["category"] == "Quality"
            and "Variants_x_Samples"
            in filter_root.find(
                "{}/{}/settings/where".format(filter_values["category"], filter_values["param"])
            ).text
        ):
            filters["phase1"][str(i)] = {
                "values": filter_values,
                "config": filter_root.find(
                    "{}/{}".format(filter_values["category"], filter_values["param"])
                ),
            }
            continue
        # another phase 1 filter : genotype / GT_Ratio :
        if (
            (
                filter_values["category"] == "Genotype_Composition"
                or filter_values["category"] == "Oncology_Specific"
                or filter_values["category"] == "User_Provided"
            )
            and "Variants_x_Samples"
            in filter_root.find(
                "{}/{}/settings/where".format(filter_values["category"], filter_values["param"])
            ).text
            and not ET.iselement(
                filter_root.find(
                    "{}/{}/settings/no_phase1".format(
                        filter_values["category"], filter_values["param"]
                    )
                )
            )
        ):
            filters["phase1"][str(i)] = {
                "values": filter_values,
                "config": filter_root.find(
                    "{}/{}".format(filter_values["category"], filter_values["param"])
                ),
            }
            continue
        # regular filters : others : phase2
        filters["phase2"][str(i)] = {
            "values": filter_values,
            "config": filter_root.find(
                "{}/{}".format(filter_values["category"], filter_values["param"])
            ),
        }
    log.debug(filters)
    return filters, logic


def ProcessFilters(filters, logic, args, sid):
    # each level : take parental level (or VxS) into account.
    # - AND : each phase/f_idx in level_id takes previous phase/f_idx as parental
    # => new tmp_table for each f_idx
    # - OR : all filters in level_id take parental into account
    # => REPLACE INTO level_tmp_table for all f_idx
    # - NOR : not supported anymore
    # - NAND : not supported anymore
    ##
    # i flogic is None, then it was an invalid filter (no child elements)
    if not logic:
        return None
    # recursive function.
    table_name = ProcessFilterLevel(filters, logic, sid)
    return table_name


def ProcessFilterLevel(filters, logic, sid, parent_table=None):
    # global tmp_table for this level
    level_tmp_table = None
    log.debug(f"Processing filter level {logic['level_id']}, with parent_table : {parent_table}")
    log.debug(f"Logic for this level: ")
    log.debug(logic)
    # phase 1 : DATA FROM VARIANTS_X_SAMPLES
    ########################################
    query = ""
    for f_idx in logic["filters"]:
        if f_idx in filters["phase1"]:
            query += " {} ( {} )".format(
                logic["type"][6:], ConstructQualityFilter(filters["phase1"][f_idx])
            )

    if query:
        # assign phase 1 tmp table:
        level_tmp_table = "{}-{}".format(config["RUNTIME"]["tmp_prefix"], logic["level_id"])
        # strip the front OR/AND
        query = query.lstrip(f" {logic['type'][6:]} ")
        # set parent table.
        if parent_table:
            query = "SELECT  HIGH_PRIORITY  `Variants_x_Samples`.vid FROM `Variants_x_Samples` INNER JOIN `{}` ON `Variants_x_Samples`.vid = `{}`.vid WHERE `Variants_x_Samples`.sid = {} AND ( {} )".format(
                parent_table, parent_table, sid, query
            )
        else:
            query = "SELECT HIGH_PRIORITY  `Variants_x_Samples`.vid FROM `Variants_x_Samples` WHERE sid = {} AND ( {} )".format(
                sid, query
            )

        # replace other occurences of sid
        query = query.replace(r"%sid", sid)
        log.debug(query)
        try:

            dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    level_tmp_table
                )
            )
            dbh.doQuery("TRUNCATE TABLE `{}`".format(level_tmp_table))
            # insert ignore : skip duplicates eg when multiple transcripts for one variant : faster than group by / replace into.
            #   => ignore risk : silent errors, but here : just vid, no foreign constraints etc.
            dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(level_tmp_table, query))
            r = dbh.runQuery(
                "SELECT  HIGH_PRIORITY  COUNT(1) AS NrVariants FROM `{}`".format(level_tmp_table)
            )
            log.info(
                "{} Variants passed level {} QC filter".format(
                    r[0]["NrVariants"], logic["level_id"]
                )
            )
        except Exception as e:
            log.error("Failed to run level {} QC filter: {}".format(logic["level_id"], e))
            # set status
            with open(os.path.join(args.directory, "status"), "w") as fh:
                fh.write("error")
            sys.exit(1)

    # phase 2: data from other tables
    #################################
    for f_idx in logic["filters"]:

        if f_idx in filters["phase2"]:
            # filter specific tmp table:
            tmp_table = "{}-{}-{}".format(config["RUNTIME"]["tmp_prefix"], logic["level_id"], f_idx)
            # tmp_table = "{}-{}".format(level_tmp_table, f_idx)
            query = ConstructFilter(
                filters["phase2"][f_idx], logic["type"][6:], sid, parent_table, level_tmp_table
            )
            # some error encountered & logged
            if not query:
                continue
            log.debug(query)
            # AND => new table
            if logic["type"][6:] == "AND":
                try:
                    log.debug(f"Creating tmp table for phase 2 / AND : {tmp_table}")
                    dbh.doQuery(
                        "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                            tmp_table
                        )
                    )
                    dbh.doQuery("TRUNCATE TABLE `{}`".format(tmp_table))
                    # insert ignore : skip duplicates eg when multiple transcripts for one variant : faster than group by / replace into.
                    #   => ignore risk : silent errors, but here : just vid, no foreign constraints etc.
                    dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(tmp_table, query))
                    # assign to use in next query.
                    level_tmp_table = tmp_table

                except Exception as e:
                    log.error(
                        "Failed to run level {} filter on {}:{} : {}".format(
                            logic["level_id"],
                            filters["phase2"][f_idx]["values"]["category"],
                            filters["phase2"][f_idx]["values"]["param"],
                            e,
                        )
                    )
                    # set status
                    with open(os.path.join(args.directory, "status"), "w") as fh:
                        fh.write("error")
                    sys.exit(1)
            # OR => add to level tmp table
            elif logic["type"][6:] == "OR":
                try:
                    level_tmp_table = "{}-{}".format(
                        config["RUNTIME"]["tmp_prefix"], logic["level_id"]
                    )
                    log.debug(f"Creating tmp table for phase 2 / OR : {level_tmp_table}")
                    # this one can exist already.
                    dbh.doQuery(
                        "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                            level_tmp_table
                        )
                    )
                    # insert ignore : skip duplicates eg when multiple transcripts for one variant : faster than group by / replace into.
                    #   => ignore risk : silent errors, but here : just vid, no foreign constraints etc.
                    dbh.doQuery("INSERT IGNORE INTO `{}` (`vid`) {}".format(level_tmp_table, query))
                except Exception as e:
                    log.error(
                        "Failed to run level {} filter on {}:{} : {}".format(
                            logic["level_id"],
                            filters["phase2"][f_idx]["values"]["category"],
                            filters["phase2"][f_idx]["values"]["param"],
                            e,
                        )
                    )  # set status
                    with open(os.path.join(args.directory, "status"), "w") as fh:
                        fh.write("error")
                    sys.exit(1)
            # logs.
            r = dbh.runQuery(
                "SELECT  HIGH_PRIORITY  COUNT(1) AS NrVariants FROM `{}`".format(level_tmp_table)
            )
            log.info(
                "{} Variants passed level {} filter on  {}:{}".format(
                    r[0]["NrVariants"],
                    logic["level_id"],
                    filters["phase2"][f_idx]["values"]["category"],
                    filters["phase2"][f_idx]["values"]["param"],
                )
            )

    # dynamic filters
    ##################
    for f_idx in logic["filters"]:
        if f_idx in filters["dynamic"]:
            # use importlib to allow loading modules specified as strings.
            # only load the required dynamic filter.
            dyn_mod = importlib.import_module(
                "dynamic_{}".format(filters["dynamic"][f_idx]["param"].lower())
            )
            # pass variants to work on
            upstream = None
            if logic["type"][6:] == "AND":
                if level_tmp_table:
                    upstream = level_tmp_table
                elif parent_table:
                    upstream = parent_table
            elif logic["type"][6:] == "OR":
                if parent_table:
                    upstream = parent_table
            # run the filter
            try:
                dyn_results = dyn_mod.Filter(
                    sid,
                    f_idx,
                    config,
                    filters,
                    "sample",
                    build=form["build"],
                    upstream_table=upstream,
                )
                # if configuration problems are encountered, there is no results_table
                #   => use the upstream table as results.
                if not hasattr(dyn_results, "results_table"):
                    dyn_results.results_table = upstream

            except Exception as e:
                log.error(
                    "Failed to run level {} filter on {}:{} : {}".format(
                        logic["level_id"],
                        filters["dynamic"][f_idx]["values"]["category"],
                        filters["dynamic"][f_idx]["values"]["param"],
                        e,
                    )
                )
                # set status
                with open(os.path.join(args.directory, "status"), "w") as fh:
                    fh.write("error")
                sys.exit(1)
            # add comments
            if len(dyn_results.comments):
                comments.add(dyn_results.comments)

            # process results.
            if logic["type"][6:] == "AND":
                # take results to next filter.
                level_tmp_table = dyn_results.results_table
            elif logic["type"][6:] == "OR":
                # add to the current level tmp (if any)
                try:
                    level_tmp_table = "{}-{}".format(
                        config["RUNTIME"]["tmp_prefix"], logic["level_id"]
                    )
                    # this one can exist already.
                    dbh.doQuery(
                        "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                            level_tmp_table
                        )
                    )
                    dbh.doQuery(
                        "INSERT IGNORE INTO `{}` (`vid`) SELECT   vid FROM `{}`".format(
                            level_tmp_table, dyn_results.results_table
                        )
                    )
                except Exception as e:
                    log.error(
                        "Failed to run level {} filter on {}:{} : {}".format(
                            logic["level_id"],
                            filters["dynamic"][f_idx]["values"]["category"],
                            filters["dynamic"][f_idx]["values"]["param"],
                            e,
                        )
                    )
                    # set status
                    with open(os.path.join(args.directory, "status"), "w") as fh:
                        fh.write("error")
                    sys.exit(1)
            # logs.
            r = dbh.runQuery(
                "SELECT  HIGH_PRIORITY  COUNT(1) AS NrVariants FROM `{}`".format(level_tmp_table)
            )
            log.info(
                "{} Variants passed level {} filter on  {}:{}".format(
                    r[0]["NrVariants"],
                    logic["level_id"],
                    filters["dynamic"][f_idx]["values"]["category"],
                    filters["dynamic"][f_idx]["values"]["param"],
                )
            )

    # phase 3: custom
    # ########

    # children.
    for child in logic["children"]:
        try:
            # pass variants to work on
            upstream = None
            # current level == XXX
            if logic["type"][6:] == "AND":
                if level_tmp_table:
                    upstream = level_tmp_table
                elif parent_table:
                    upstream = parent_table
            elif logic["type"][6:] == "OR" and parent_table:
                upstream = parent_table
            # process the child level.
            child_table = ProcessFilterLevel(filters, child, sid, upstream)
            if not child_table:
                continue
            log.debug(f"Obtained {child_table} after processing level with upstream {upstream}")
        except Exception as e:
            log.error(
                "Failed to processed child logic in level {}: {} ".format(logic["level_id"], e)
            )
            with open(os.path.join(args.directory, "status"), "w") as fh:
                fh.write("error")
            sys.exit(1)
        # combine with this level.
        if logic["type"][6:] == "AND":
            level_tmp_table = child_table
        elif logic["type"][6:] == "OR":
            try:
                # this one can exist already.
                level_tmp_table = "{}-{}".format(config["RUNTIME"]["tmp_prefix"], logic["level_id"])
                dbh.doQuery(
                    "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                        level_tmp_table
                    )
                )
                # insert ignore : skip duplicates eg when multiple transcripts for one variant : faster than group by / replace into.
                #   => ignore risk : silent errors, but here : just vid, no foreign constraints etc.
                dbh.doQuery(
                    "INSERT IGNORE INTO `{}` (`vid`) SELECT   `vid` FROM `{}`".format(
                        level_tmp_table, child_table
                    )
                )

            except Exception as e:
                log.error(
                    "Failed to add child-query results in level {}: {}: {}".format(
                        logic["level_id"], child_table, e
                    )
                )
                # set status
                with open(os.path.join(args.directory, "status"), "w") as fh:
                    fh.write("error")
                sys.exit(1)

    # aftermatch :
    # ############
    for f_idx in logic["filters"]:
        if f_idx in filters["after"]:
            # for AND : works on current level variant list.
            # for OR : works on the parent level variant list.

            # dynamic ?

            if filters["after"][f_idx]["style"] == "dynamic":
                level_tmp_table = ProcessAfterMatchDynamic(
                    filters, logic, f_idx, level_tmp_table, parent_table
                )
            else:

                level_tmp_table = ProcessAfterMatchRegular(
                    filters, logic, f_idx, level_tmp_table, parent_table
                )

    # nr of resulting variants :
    if level_tmp_table:
        r = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  COUNT(1) as NrVariants FROM `{}`".format(level_tmp_table)
        )
        log.info("{} Variants passed filter level {}".format(r[0]["NrVariants"], logic["level_id"]))
    else:
        log.debug("No filters processed in level {}".format(logic["level_id"]))
    # return the final table with variants from this level.
    return level_tmp_table


def ProcessAfterMatchDynamic(filters, logic, f_idx, level_tmp_table=None, parent_table=None):
    # use importlib to allow loading modules specified as strings.
    # only load the required dynamic filter.
    # depending on regular/after : replace items. regular is already processed, do no risk of conflicts.
    if f_idx not in filters["dynamic"]:
        filters["dynamic"][f_idx] = filters["after"][f_idx]

    dyn_mod = importlib.import_module(
        "dynamic_{}".format(filters["dynamic"][f_idx]["param"].lower())
    )
    # pass variants to work on
    upstream = None
    if logic["type"][6:] == "AND":
        if level_tmp_table:
            upstream = level_tmp_table
        elif parent_table:
            upstream = parent_table
    elif logic["type"][6:] == "OR":
        if parent_table:
            upstream = parent_table
    # run the filter
    try:
        dyn_results = dyn_mod.Filter(
            args.sid,
            f_idx,
            config,
            filters,
            "sample",
            build=form["build"],
            upstream_table=upstream,
        )
        # if configuration problems are encountered, there is no results_table
        #   => use the upstream table as results.
        if not hasattr(dyn_results, "results_table"):
            dyn_results.results_table = upstream

    except Exception as e:
        log.error(
            "Failed to run level {} filter on {}:{} : {}".format(
                logic["level_id"],
                filters["dynamic"][f_idx]["values"]["category"],
                filters["dynamic"][f_idx]["values"]["param"],
                e,
            )
        )
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)
    # process results.
    if logic["type"][6:] == "AND":
        # take results to next filter.
        level_tmp_table = dyn_results.results_table
    elif logic["type"][6:] == "OR":
        # add to the current level tmp (if any)
        try:
            level_tmp_table = "{}-{}".format(config["RUNTIME"]["tmp_prefix"], logic["level_id"])
            # this one can exist already.
            dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    level_tmp_table
                )
            )
            dbh.doQuery(
                "INSERT IGNORE INTO `{}` (`vid`) SELECT   vid FROM `{}`".format(
                    level_tmp_table, dyn_results.results_table
                )
            )
        except Exception as e:
            log.error(
                "Failed to run level {} filter on {}:{} : {}".format(
                    logic["level_id"],
                    filters["dynamic"][f_idx]["values"]["category"],
                    filters["dynamic"][f_idx]["values"]["param"],
                    e,
                )
            )
            # set status
            with open(os.path.join(args.directory, "status"), "w") as fh:
                fh.write("error")
            sys.exit(1)
    # logs.
    r = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  COUNT(1) AS NrVariants FROM `{}`".format(level_tmp_table)
    )
    log.info(
        "{} Variants passed level {} filter on  {}:{}".format(
            r[0]["NrVariants"],
            logic["level_id"],
            filters["dynamic"][f_idx]["values"]["category"],
            filters["dynamic"][f_idx]["values"]["param"],
        )
    )
    return level_tmp_table


def ProcessAfterMatchRegular(filters, logic, f_idx, level_tmp_table=None, parent_table=None):

    filter_config = filters["after"][f_idx]["config"]
    filter_values = filters["after"][f_idx]["values"]
    logic_type = logic["type"][6:]
    table_in_where = ""
    # define input variants.
    if logic_type == "AND":
        if level_tmp_table:
            table_in = level_tmp_table
        elif parent_table:
            table_in = parent_table
        else:
            table_in = "Variants_x_Samples"
            table_in_where = "AND `Variants_x_Samples`.sid = '{}'".format(args.sid)
    elif logic_type == "OR":
        if parent_table:
            table_in = parent_table
        else:
            table_in = "Variants_x_Samples"
            table_in_where = "AND `Variants_x_Samples`.sid = '{}'".format(args.sid)
    # where is in multiple steps:
    qidx = 0
    tmp_tables = dict()
    while ET.iselement(filter_config.find("settings/SeparateQuery/q{}".format(qidx))):
        # expects a <t0> to make tmp table.
        if not ET.iselement(filter_config.find("settings/SeparateQuery/t{}".format(qidx))):
            log.error(
                "Configuration error for {}/{} level {}".format(
                    filter_values["category"], filter_values["param"], qidx
                )
            )
            sys.exit(1)
        try:
            tname = "{}-{}-after-{}-{}".format(
                config["RUNTIME"]["tmp_prefix"], logic["level_id"], f_idx, qidx
            )
            create_cols = filter_config.find("settings/SeparateQuery/t{}".format(qidx)).text
            if ET.iselement(filter_config.find("settings/SeparateQuery/u{}".format(qidx))):
                create_cols += ", `Counter` INT(3) DEFAULT '1'"
            dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(tname))
            log.debug(
                "CREATE TABLE IF NOT EXISTS `{}` ({})  ENGINE=MEMORY CHARSET=latin1".format(
                    tname, create_cols
                )
            )
            dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` ({})  ENGINE=MEMORY CHARSET=latin1".format(
                    tname, create_cols
                )
            )
            tmp_tables[qidx] = tname
            dbh.doQuery("TRUNCATE TABLE `{}`".format(tname))
        except Exception as e:
            log.error(
                "Could not create tmp table for {}/{} level {} : {}".format(
                    filter_values["category"], filter_values["param"], qidx, e
                )
            )
            sys.exit(1)
        # q0 : the where part
        query = filter_config.find("settings/SeparateQuery/q{}".format(qidx)).text

        if "_upstream_" in query:
            query = "{} {}".format(query, table_in_where)
        # strip for filling : keys
        fill_cols = re.sub(r",\s*PRIMARY KEY.*", "", create_cols)
        # types
        fill_cols = ",".join(
            [x.lstrip().split(" ")[0] for x in fill_cols.split(",") if x.lstrip().startswith("`")]
        )
        if not ET.iselement(filter_config.find("settings/SeparateQuery/u{}".format(qidx))):
            query = "INSERT IGNORE INTO `{}` ({}) {} ".format(tname, fill_cols, query)
        else:
            query = "INSERT INTO `{}` ({}) {} ON DUPLICATE KEY UPDATE `{}`.`Counter` = `{}`.`Counter` + 1".format(
                tname, fill_cols, query, tname, tname
            )

        # fill in placeholders:
        # # table names.
        query = query.replace("_upstream_", table_in)
        for i in range(0, (qidx + 1)):
            # _t0_ placeholders : refer to previous/current table
            query = query.replace("_t{}_".format(i), tmp_tables[i])

        # text field with value(s) ?
        if (
            ET.iselement(filter_config.find("settings/need"))
            and filter_config.find("settings/need").text == "text"
        ):
            # where part contains a %value to replace with contents of input field.
            values = filter_values["value"]
            # split? => add quotes
            if (
                ET.iselement(filter_config.find("settings/split_on"))
                and filter_config.find("settings/split_on").text != ""
            ):
                # split & rejoin with quotes, removing empty values.
                values = "','".join(
                    list(
                        filter(
                            None,
                            values.replace(" ", "").split(
                                filter_config.find("settings/split_on").text
                            ),
                        )
                    )
                )

            # fill in.
            query = query.replace(r"%value", values)

        # arguments ?
        if ET.iselement(filter_config.find("settings/ArgumentsFromQuery")):
            query_args = list(filter(None, filter_values["argument"].split("@@@")))
            query = query.replace(r"%replace", "'{}'".format("','".join(query_args)))

        # get user details (#samples etc)
        query = query.replace(r"%userid", str(args.userid)).replace(r"%sid", str(args.sid))
        query = GetUserNumbers(query)
        try:
            dbh.doQuery(query)
        except Exception as e:
            log.error(
                "Could not fill tmp table for {}/{} level {} : {}".format(
                    filter_values["category"], filter_values["param"], qidx, e
                )
            )
            sys.exit(1)

        # next level.
        qidx += 1
    # reduce qidx
    qidx -= 1
    # intersect again with <table_in> :
    # return final table.
    tname = "{}-{}-after-{}-{}".format(
        config["RUNTIME"]["tmp_prefix"], logic["level_id"], f_idx, qidx
    )
    # logs.
    r = dbh.runQuery("SELECT  HIGH_PRIORITY  COUNT(1) AS NrVariants FROM `{}`".format(tname))
    log.info(
        "{} Variants passed level {} filter on  {}:{}".format(
            r[0]["NrVariants"],
            logic["level_id"],
            filters["dynamic"][f_idx]["values"]["category"],
            filters["dynamic"][f_idx]["values"]["param"],
        )
    )
    return tname


def Filter(config, form, args):
    # prepare filters
    filters, logic = PrepareFilters(config, form)
    log.info("Processing Filters....")
    table_name = ProcessFilters(filters, logic, args, form["sid"])
    # no filters run : put all variants into tmp table.
    if not table_name:
        try:
            table_name = "{}-0".format(config["RUNTIME"]["tmp_prefix"])
            dbh.doQuery(
                "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                    table_name
                )
            )
            # insert ignore : skip duplicates eg when multiple transcripts for one variant : faster than group by / replace into.
            #   => ignore risk : silent errors, but here : just vid, no foreign constraints etc.
            dbh.doQuery(
                "INSERT IGNORE INTO `{}` (`vid`) SELECT   `vid` FROM `Variants_x_Samples` WHERE sid = '{}'".format(
                    table_name, form["sid"]
                )
            )
        except Exception as e:
            log.error(
                "Could not extract varians from sample (needed as no filters were run) : {}".format(
                    e
                )
            )
            # set status
            with open(os.path.join(args.directory, "status"), "w") as fh:
                fh.write("error")
            sys.exit(1)

    return table_name


def AnnotateVariants(config, form, args, filtered_table):
    # use a json table
    variants_table = "{}-final".format(config["RUNTIME"]["tmp_prefix"])
    titles_table = "{}-titles".format(config["RUNTIME"]["tmp_prefix"])

    # CONSTRUCT/COLLAPSE ANNOTATIONS
    annotations_config = ET.parse(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/Config.xml")
    )
    annotations = GetAnnotations(form, annotations_config)

    # format for each variant:
    #    - top level: individual entries
    #    - subtables : as nested arrays of dict_per_line
    try:
        # use INNODB because JSON not supported in MEMORY + can become very big (WGS+JSON)
        dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(variants_table))
        dbh.doQuery(
            "CREATE TABLE `{}` (`vid` INT(11) PRIMARY KEY, `chr` INT(2), `position` INT(11), `data` JSON) ENGINE=INNODB CHARSET=latin1".format(
                variants_table
            )
        )
    except Exception as e:
        log.error(
            "Could not create table for annotated variants : {} : {}".format(variants_table, e)
        )
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)
    # titles table:
    try:
        dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(titles_table))
        dbh.doQuery(
            "CREATE TABLE `{}` ( `idx` INT(2) NOT NULL AUTO_INCREMENT UNIQUE , `title` VARCHAR(50) NOT NULL , `type` VARCHAR(10) NOT NULL , `parent` VARCHAR(2) NOT NULL , PRIMARY KEY (`title`,`type`)) ENGINE=MEMORY CHARSET=latin1".format(
                titles_table
            )
        )
        # Add the default fields:
        for t in ["location", "ref_allele", "alt_allele"]:
            t_idx = AddTitle(titles_table, t, "main", "-")

        for t in [
            "Genotype",
            "Ref_Allele_Depth",
            "Alt_Allele_Depth",
            "AllelicRatio",
            "Tranches_Filter",
            "Occurence_All_Samples_Heterozygous",
            "Occurence_All_Samples_HomAlt",
        ]:
            if (
                t in annotations["Variants_x_Samples"]
                or t in annotations["Variants_x_Users_Summary"]
            ):
                t_idx = AddTitle(titles_table, t, "main", "-")

    except Exception as e:
        log.error(
            "Could not create table for tracking annotation structure : {} : {}".format(
                titles_table, e
            )
        )
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)

    select = (
        "SELECT  HIGH_PRIORITY  `Variants`.id, `Variants`.chr, `Variants`.start, JSON_OBJECT("
        "'variant_id', `Variants`.id, "
        "'chr', `Variants`.chr, 'position', `Variants`.start, 'location', CONCAT(`Variants`.chr,',',`Variants`.start), "
        "'ref_allele', `Variants`.RefAllele, 'alt_allele', `Variants`.AltAllele, 'InStretch', IF(`Variants`.Stretch = 1,\"yes\",\"no\"), "
        "'StretchUnit', `Variants`.StretchUnit) "
        "FROM `{}` JOIN `Variants` ON `{}`.vid = `Variants`.id ".format(
            filtered_table, filtered_table
        )
    )

    log.debug(select)
    query = "INSERT INTO `{}` (`vid`, `chr`, `position`, `data`) {}".format(variants_table, select)
    try:
        dbh.doQuery(query)
    except Exception as e:
        log.error("Could not extract Variant info: {}".format(e))

        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)
    # if variant-level annotations were selected, add them to titles (slightly rewritten, legacy...)
    if "Variants" in annotations:
        if "Stretch" in annotations["Variants"]:
            t_idx = AddTitle(titles_table, "InStretch", "main", "-")
        if "Stretch_Unit" in annotations["Variants"]:
            t_idx = AddTitle(titles_table, "StretchUnit", "main", "-")
        # then remove the annotations, as they were fetched above.
        del annotations["Variants"]

    # add annotations to the table (in parallel)
    # for table in annotations.keys():
    pool.map(
        partial(
            LoadAnnotations,
            annotations=annotations,
            variants_table=variants_table,
            a_config=annotations_config,
            form=form,
        ),
        list(annotations.keys()),
    )
    # launch queries to get parental data.
    # info on parents.
    p_rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  ss.sid1 AS sid, s.gender FROM `Samples_x_Samples` ss JOIN `Samples` s ON ss.sid1 = s.id WHERE sid2 = %s AND Relation = 2",
        form["sid"],
    )
    pool.map(partial(ParentalInfo, variants_table=variants_table, form=form), p_rows)
    # LoadAnnotations(table, annotations, variants_table, annotations_config, form)
    pool.close()
    pool.join()

    # then add position index for outputting.
    dbh.doQuery("ALTER TABLE `{}` ADD INDEX `pos`(`chr`, `position`)".format(variants_table))
    # add default annotations to list for print-formatting.
    # config["RUNTIME"]["tmp_prefix"]
    return variants_table


def AddTitle(t_table, title, type="main", parent="-"):
    dbh = GetDBConnection(quiet=True)

    try:
        t_idx = dbh.insertQuery(
            "INSERT INTO `{}` (`title`,`type`,`parent`) VALUES (%s,%s,%s)".format(t_table),
            [title, type, parent],
        )
    except ValueError as e:
        if "Error 1062" in repr(e):
            r = dbh.runQuery(
                "SELECT  HIGH_PRIORITY  `idx` FROM `{}` WHERE `title` = %s AND `type` = %s".format(
                    t_table
                ),
                [title, type],
            )
            t_idx = r[0]["idx"]
        else:
            raise e
    except Exception as e:
        raise e

    return t_idx


def GetAnnotations(form, a_config):
    a_config = a_config.getroot()
    annotations = dict()
    # add a few defaults to the form.
    defaults = [
        "GATK_Annotations;Genotype",
        "GATK_Annotations;Alt_Allele_Depth",
        "GATK_Annotations;Ref_Allele_Depth",
        "GATK_Annotations;Tranches_Filter",
        "GATK_Annotations;AllelicRatio",
        "GATK_Annotations;Occurence_All_Samples_Heterozygous",
        "GATK_Annotations;Occurence_All_Samples_HomAlt",
    ]
    for idx, v in enumerate(defaults):
        form["anno_def{}".format(idx)] = v
    # process user-provided items
    for k in form.keys():
        if k.startswith("anno"):
            category, item = form[k].split(";")
            if not ET.iselement(a_config.find("{}/{}".format(category, item))):
                comments.add("WARNING: Annotation {}/{} is not defined".format(category, item))
                log.warning("Annotation {}/{} is not defined".format(category, item))
                continue
            # license needed?
            if ET.iselement(a_config.find("{}/{}/license".format(category, item))):
                license = a_config.find("{}/{}/license".format(category, item)).text
                l_r = dbh.runQuery(
                    "SELECT  HIGH_PRIORITY  l.lid FROM `License` l JOIN `Users_x_License` ul ON ul.lid = l.lid WHERE ul.uid = %s AND l.LicenseName = %s",
                    (form["uid"], license),
                )
                if not l_r:
                    comments.add(
                        "WARNING: Annotation {}/{} is licensed, and user is not allowed to use it".format(
                            category, item
                        )
                    )
                    log.warning(
                        "Annotation {}/{} is licensed, and user is not allowed to use it".format(
                            category, item
                        )
                    )
                    continue
            # deprecated annotation
            if ET.iselement(a_config.find("{}/{}/deprecated".format(category, item))):
                comments.add(
                    "WARNING: Annotation {}/{} is not deprecated and no longer updated for novel variants".format(
                        category, item
                    )
                )
                log.warning(
                    "Annotation {}/{} is not deprecated and no longer updated for novel variants".format(
                        category, item
                    )
                )
            table = a_config.find("{}/{}/from".format(category, item)).text.strip().split(" ")[0]
            # variants is fetched in full
            # if table == "Variants":
            #    continue
            # select which field ?
            select = a_config.find("{}/{}/select".format(category, item)).text.split(" AS ")[0]
            # is there a user defined column order?
            if ET.iselement(a_config.find("{}/{}/column_order".format(category, item))):
                column_order = a_config.find(
                    "{}/{}/column_order".format(category, item)
                ).text.strip()
            else:
                column_order = 0  # dummy value
            log.debug("column order for {} : {}".format(select, column_order))
            # add to dict
            if table not in annotations:
                annotations[table] = {
                    item: {"select": select, "category": category, "order": column_order}
                }
            else:
                annotations[table][item] = {
                    "select": select,
                    "category": category,
                    "order": column_order,
                }

    # add the "subbox" info:
    subbox = {
        "sid": {"category": "GATK_Annotations", "select": "`Variants_x_Samples`.sid"},
        "set_inheritance": {
            "category": "GATK_Annotations",
            "select": "`Variants_x_Samples`.inheritance",
        },
        "inheritance_mode": {
            "category": "GATK_Annotations",
            "select": "`Variants_x_Samples`.InheritanceMode",
        },
        "diagnostic_class": {
            "category": "GATK_Annotations",
            "select": "`Variants_x_Samples`.class",
        },
        "validation": {"category": "GATK_Annotations", "select": "`Variants_x_Samples`.validation"},
        "validation_details": {
            "category": "GATK_Annotations",
            "select": "`Variants_x_Samples`.validationdetails",
        },
        "autoclassified": {
            "category": "GATK_Annotations",
            "select": "`Variants_x_Samples`.autoclassified",
        },
    }
    try:
        annotations["Variants_x_Samples"].update(subbox)
    except KeyError:
        annotations["Variants_x_Samples"] = subbox
    return annotations


def ParentalInfo(parent, variants_table, form):
    query = "SELECT  HIGH_PRIORITY  `{}`.vid, JSON_OBJECT('%parent', `Variants_x_Samples`.AltCount) FROM `{}` JOIN `Variants_x_Samples` ON `{}`.vid =  `Variants_x_Samples`.vid WHERE `Variants_x_Samples`.sid = '{}'".format(
        variants_table, variants_table, variants_table, parent["sid"]
    )
    if parent["gender"] == "Male":
        query = query.replace(r"%parent", "father")
    elif parent["gender"] == "Female":
        query = query.replace(r"%parent", "mother")
    else:
        query = query.replace(r"%parent", "unknown")
    # threading safe connections to DB
    dbh = GetDBConnection(quiet=True)
    updater = GetDBConnection(quiet=True)
    rows = dbh.runQuery(query, size=5000, as_dict=False)
    while rows:
        # update json table.
        for row in rows:
            data_string = '{{"{}": {}}}'.format("parents", row[1])
            updater.doQuery(
                "UPDATE `{}` SET `data` = JSON_MERGE_PATCH(`data`, %s) WHERE vid = %s".format(
                    variants_table
                ),
                [data_string, row[0]],
            )
        rows = dbh.GetNextBatch()


def LoadAnnotations(table, annotations, variants_table, a_config, form):

    # get settings
    titles_table = variants_table.replace("-final", "-titles")
    a_config = a_config.getroot()
    category = annotations[table][list(annotations[table].keys())[0]]["category"]
    # local database connections:
    dbh = GetDBConnection(quiet=True)
    updater = GetDBConnection(quiet=True)
    # SINGLE ENTRY PER VARIANT
    if not ET.iselement(
        a_config.find("{}/{}/subtable".format(category, list(annotations[table].keys())[0]))
    ):
        aggregate_open = "JSON_OBJECT("
        aggregate_close = ")"
        grouping = ""
        subtable = None
    # multiple entries per variant (eg transcripts)
    else:
        aggregate_open = "JSON_ARRAYAGG(JSON_OBJECT("
        aggregate_close = "))"
        grouping = "GROUP BY  `{}`.vid".format(variants_table)
        subtable = a_config.find(
            "{}/{}/subtable".format(category, list(annotations[table].keys())[0])
        ).text
        # add to titles table.
        subtable_idx = AddTitle(titles_table, subtable, "subtable", "-")

    # construct the json statement
    query = "SELECT  HIGH_PRIORITY  `{}`.vid, {}".format(variants_table, aggregate_open)
    try:
        sorted_annotations = dict(
            sorted(annotations[table].items(), key=lambda x: getitem(x[1], "order"))
        )
    except Exception as e:
        # order field missing : use unsorted entries
        sorted_annotations = annotations[table]

    for item in sorted_annotations:
        query = "{} '{}', {},".format(query, item, annotations[table][item]["select"])
        # add to titles table
        if subtable:
            item_idx = AddTitle(titles_table, item, "child", subtable_idx)
        else:
            item_idx = AddTitle(titles_table, item, "main", "-")

    query = "{}{} AS data FROM `{}`".format(query.rstrip(","), aggregate_close, variants_table)
    on = ""
    where = ""

    # add sid if needed.
    if "Variants_x_Samples" in query:
        # added as "variants_table" : needed in FROM
        if "Variants_x_Samples" not in table:
            query = "{} STRAIGHT_JOIN `Variants_x_Samples`".format(query)
            on = "`Variants_x_Samples`.vid = `{}`.vid".format(variants_table)
        # add sid
        where = "WHERE `Variants_x_Samples`.sid = {}".format(form["sid"])

    # add from  & join.

    if "," in table:
        for t in table.split(","):
            query = "{} JOIN `{}`".format(query, t)
            if t.startswith("Variants_x_"):
                if on:
                    on = "{} AND `{}`.vid = `{}`.vid".format(on, variants_table, t)
                else:
                    on = "`{}`.vid = `{}`.vid".format(variants_table, t)
        # add ON taken from last category/item in loop above.
        on = "{} AND {}".format(
            on, a_config.find("{}/{}/join".format(category, item)).text.replace(",", " AND ")
        )
    else:
        # add table.
        query = "{} STRAIGHT_JOIN  `{}`".format(query, table)
        if on:
            on = "{} AND `{}`.vid = `{}`.vid".format(on, variants_table, table)
        else:
            on = "`{}`.vid = `{}`.vid".format(variants_table, table)

    # add other required items to where:
    if "Variants_x_Users_Summary" in query:
        if where:
            where = "{} AND `Variants_x_Users_Summary`.uid = '{}'".format(where, r"%userid")
        else:
            where = "WHERE `Variants_x_Users_Summary`.uid = '{}'".format(r"%userid")

    # finalize query:
    query = "{} ON {} {} {}".format(query, on, where, grouping)
    if r"%userid" in query:
        query = query.replace(r"%userid", str(form["uid"]))
    if r"%sid" in query:
        query = query.replace(r"%sid", form["sid"])

    log.debug(query)
    # fetch annotations
    rows = dbh.runQuery(query, size=5000, as_dict=False)
    while rows:
        # update json table.
        for row in rows:

            # if grouping => subtable
            if subtable:
                data_string = '{{"{}": {}}}'.format(subtable, row[1])
                q = "UPDATE `{}` SET `data` = JSON_MERGE_PATCH(`data`, %s) WHERE vid = '%s'".format(
                    variants_table
                )
                updater.doQuery(q, [data_string, row[0]])
            else:
                updater.doQuery(
                    "UPDATE `{}` SET `data` = JSON_MERGE_PATCH(`data`, %s) WHERE vid = %s".format(
                        variants_table
                    ),
                    [row[1], row[0]],
                )
        rows = dbh.GetNextBatch()


def BuildOutput(variants_table, form, args):
    # api => json
    if args.source.upper() == "API" and not form["slicestart"].startswith("save|"):
        PrintApiJSON(variants_table, args)
    # UI => export
    elif args.source.upper() == "UI" and form["slicestart"] == "all":
        ExportResults(variants_table, args)
    # default output (100 vars table)
    elif args.source.upper() == "UI" or form["slicestart"].startswith(
        "save|"
    ):  # and form["slicestart"].isnumeric():
        # generate the result page(s)
        PrintRegularTable(variants_table, args, form)
        # need to save them?
        if form["slicestart"].startswith("save|"):
            SaveResults(variants_table, args, form)
        else:
            # print comment
            with open(os.path.join(args.directory, "comments.txt"), "w") as fh:
                for line in comments.all():
                    line = line.replace("ERROR", "<span class=red>ERROR</span>").replace(
                        "WARNING", "<span class=red>WARNING</span>"
                    )
                    fh.write("{}<br/>".format(line))

    else:
        log.error("source/format not supported : {}:{}".format(args.source.upper(), "format"))
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)


def SaveResults(variants_table, args, form):
    set_id = form["slicestart"].split("|")[1]
    # get log.
    log_contents = "STDOUT:\n=======\n"
    with open(os.path.join(args.directory, "stdout"), "r") as fh:
        log_contents += fh.read()
    log_contents += "\nSTDERR:\n=======\n"
    with open(os.path.join(args.directory, "stderr"), "r") as fh:
        log_contents += fh.read()
    # get vids.
    rows = dbh.runQuery("SELECT  HIGH_PRIORITY  vid FROM `{}`".format(variants_table))
    vids = ",".join([str(x["vid"]) for x in rows])
    # set logs & resulting vids
    dbh.doQuery(
        "UPDATE `Samples_x_Saved_Results` SET `set_vids` = %s, `query_log` = %s WHERE `set_id` = %s",
        (vids, log_contents, set_id),
    )
    # save pages.
    page = 1
    while os.path.isfile(os.path.join(args.directory, "results.{}".format((page - 1) * 100))):
        file = os.path.join(args.directory, "results.{}".format((page - 1) * 100))
        with open(file, "r") as fh:
            page_contents = fh.read()
            dbh.insertQuery(
                "INSERT INTO `Samples_x_Saved_Results_Pages` (`set_id`, `page`, `contents`) VALUES (%s, %s, %s)",
                (set_id, page, page_contents),
            )
        page = page + 1


def PrintApiJSON(variants_table, args):
    # load decoding info
    coded_fields, codes = LoadDecodingInfo()
    # for ncbiGene : are there mane / user specific "favourite" transcripts.
    mane_nm = GetManeTranscripts(form, args)
    # open the output file.
    # note : it's safer to use json.dumps from a full-result dict, but for WGS data this might become problematic
    fh = open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results", args.item, "results"),
        "w",
    )
    # open json
    fh.write('{"Variants":[')
    first = True
    rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  `data` FROM `{}` ORDER BY `chr`, `position`".format(variants_table),
        size=5000,
    )
    while rows:
        # each row is a variant
        for row in rows:
            data = json.loads(row["data"])
            # set parental info.
            data = DetermineInheritance(data)
            # decode.
            data = DecodeData(data, coded_fields, codes)
            # Convert values where needed
            data = ReWriteValues(data)
            # Add MANE annotation:
            for tr_idx, tr in enumerate(data.get("RefSeq", [])):
                if "RefSeq_Transcript" in tr:
                    versionless_nm = tr["RefSeq_Transcript"].split(".")[0]
                    if versionless_nm in mane_nm:
                        # MANE Select/Clinical : remove mane
                        # UserKB : ... : remove comment
                        tr["MANE"] = (
                            mane_nm[versionless_nm]
                            .replace("MANE ", "")
                            .replace("Plus ", "")
                            .split(" ")[0]
                        )
                    else:
                        tr["MANE"] = ""

                    # update in data
                    data["RefSeq"][tr_idx] = tr
                log.debug(tr)

            # print
            if first:
                first = False
            else:
                fh.write(",")
            fh.write(json.dumps(data))
        rows = dbh.GetNextBatch()
    # add duration to comments.
    global start_time
    comments.add(
        "NOTICE: Total Runtime (h:m:s) : {}".format(
            str(datetime.timedelta(seconds=int(time.time() - start_time)))
        )
    )

    fh.write('],"Comments": ["{}"]}}'.format('","'.join(comments.all())))
    fh.close()


def ExportResults(variants_table, args):
    # table holding field titles.
    titles_tables = variants_table.replace("-final", "-titles")
    # how many extra rows are needed per variant in the table
    # rowspan = 0
    # for ncbiGene : are there mane / user specific "favourite" transcripts.
    mane_nm = GetManeTranscripts(form, args)
    # fields to skip.
    skip_fields = [
        "sid",
        # "set_inheritance",
        # "diagnostic_class",
        # "validation",
        # "validation_details",
        # "autoclassified",
        # "validation_details",
        # "favourite",
    ]
    # what is the format:
    quote = '"' if form["quote"] == "y" else ""
    sep = "\t" if form["format"] == "tab" else ","
    ext = "txt" if form["format"] == "tab" else "csv"

    # get headers for main annotations:
    table_headers = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  `title` FROM `{}` WHERE type = 'main' AND `parent` = '-' AND `title` NOT IN ('{}') ORDER BY idx ".format(
            titles_tables, "','".join(skip_fields)
        )
    )
    headers = quote + f"{quote}{sep}{quote}".join([x["title"] for x in table_headers]) + quote
    # track column order for printing.
    columns = headers.replace(quote, "").split(sep)
    # get headers for subtables
    subtable_rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  `idx`, `title` FROM `{}` WHERE type = 'subtable' ORDER BY idx".format(
            titles_tables
        )
    )
    # get subtable entries.
    subtables = dict()
    for sr in subtable_rows:
        # dicts retain sorting order by default...
        subtables[sr["title"]] = list()
        st_fields = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  `title` FROM `{}` WHERE type = 'child' AND `parent` = %s ORDER BY idx".format(
                titles_tables
            ),
            sr["idx"],
        )
        for st_f in st_fields:
            subtables[sr["title"]].append(st_f["title"])
            # subtables : add st_name to each column.
            headers = f"{headers}{sep}{quote}{sr['title']}({st_f['title']}){quote}"
            # track order
            columns.append(f"{sr['title']}|{st_f['title']}")

    # load decoding info
    coded_fields, codes = LoadDecodingInfo()

    # get classifier variants
    # classifier_variants = GetClassifierVariants()

    # how many variants ?
    # row = dbh.runQuery("SELECT  HIGH_PRIORITY  COUNT(1) AS NrRows FROM `{}`".format(variants_table))
    # nrVariants = row[0]["NrRows"]

    # open the output file.
    fh = open(
        os.path.join(
            args.directory,
            f"results.{ext}",
        ),
        "w",
    )
    # output header
    fh.write(headers + "\n")
    # get results
    rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  `data` FROM `{}` ORDER BY `chr`, `position` ".format(variants_table)
    )
    for row in rows:
        data = json.loads(row["data"])
        # set parental info.
        data = DetermineInheritance(data)

        # decode.
        data = DecodeData(data, coded_fields, codes)
        # Convert values where needed
        data = ReWriteValues(data)
        data["location"] = "chr" + data["location"].replace(",", ":")

        # go over columns && add to line. follow order in
        line = ""

        for c in columns:
            # subtable?
            if "|" in c:
                t, f = c.split("|")
                if t not in data:
                    line += f".{sep}"
                else:
                    # collaps rows => column for subtable
                    v = ""
                    for st_row in data[t]:
                        if f not in st_row:
                            v + ".|"
                        else:
                            # mane?
                            if (
                                "RefSeq_Transcript" in st_row
                                and st_row["RefSeq_Transcript"].split(".")[0] in mane_nm
                            ):
                                fav = "*"
                            else:
                                fav = ""
                            v += f"{st_row[f]}{fav}|"
                    # strip last sep && newlines
                    v = v.rstrip("|").replace("\n", " ")
                    line += f"{quote}{v}{quote}{sep}"
            else:
                v = str(data[c]).replace("\n", "")
                line += f"{quote}{v}{quote}{sep}"
        fh.write(line.rstrip(sep) + "\n")
    fh.close()


def ReWriteValues(data):
    for k in data:
        if k in ["RefSeq", "RefGene"]:
            # rows in the table
            for i in range(len(data[k])):
                for j, v in data[k][i].items():
                    # cPointNT => regex rewrite
                    if "cPointNT" in j:
                        data[k][i][j] = re.sub(r"c\.([ACGT]+)(\d+)([ACGT]+)", r"c.\2\1>\3", v)
                    if "cPointAA" in j:
                        AA_codes = GetAACodes()
                        new_value = ""
                        for p in range(0, len(v)):
                            if v[p : (p + 1)] in AA_codes:
                                new_value += AA_codes[v[p : (p + 1)]]
                            else:
                                new_value += v[p : (p + 1)]
                        data[k][i][j] = new_value

    return data


def GetAACodes():
    return {
        "G": "Gly",
        "A": "Ala",
        "L": "Leu",
        "M": "Met",
        "F": "Phe",
        "W": "Trp",
        "K": "Lys",
        "Q": "Gln",
        "E": "Glu",
        "S": "Ser",
        "P": "Pro",
        "V": "Val",
        "I": "Ile",
        "C": "Cys",
        "Y": "Tyr",
        "H": "His",
        "R": "Arg",
        "N": "Asn",
        "D": "Asp",
        "T": "Thr",
        "X": "X",
    }


def GetCheckboxes(form):
    cbs = ""
    if form["slicestart"].startswith("save|"):
        set_id = form["slicestart"].split("|")[1]
        cbr = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  cbs FROM `Samples_x_Saved_Results` WHERE `set_id` = %s", set_id
        )
        if cbr[0]["cbs"] != "":
            cbxs = dbh.runQuery(
                "SELECT  HIGH_PRIORITY  `Title`,`Options`,`cid` FROM `Report_Section_CheckBox` WHERE cid IN ({})".format(
                    cbr[0]["cbs"]
                )
            )
            for cbx in cbxs:
                name = cbx["Title"]
                items = cbx["Options"].split("||")
                cid = cbx["cid"]
                idx = 0
                if len(items) > 0:
                    cbs += "<tr><th colspan=6><span class=emph>{}</span></th></tr><tr>".format(name)
                for k in items:
                    if k == "":
                        continue
                    idx += 1
                    if idx > 6:
                        cbs += "</tr><tr>"
                        idx = 1
                    cbs += "<td ><input type=checkbox class='NoteBox' title='N/A' id='%vid%_{}_{}_{}'>".format(
                        set_id, cid, k
                    )
                    cbs += "<label for='%vid%_{}_{}_{}'>".format(set_id, cid, k)
                    cbs += "{}</label></td>".format(k)
                while idx > 0 and idx < 6:
                    idx += 1
                    cbs += "<td>&nbsp;</td>"
                if idx > 0:
                    cbs += "</tr>"
    if cbs != "":
        cbs = "<table id='cb_table_%vid%' cellspacing=0 style='width:100%'>{}</table>".format(cbs)
    return cbs


# MANE transcripts are the reference transcripts as defined by NCBI/ENSEMBL.
def GetManeTranscripts(form, args):
    # transcript => level.
    mane_transcripts = dict()
    # step one : taken from the annotation file if exists
    select_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/ANNOVAR/humandb/{}_ncbi.mane.txt".format(form["build"]),
    )
    db_suffix = config["DATABASE"].get("DBSUFFIX", "")
    if db_suffix:
        select_file = select_file.replace(f"_{db_suffix}", "")
    if os.path.exists(select_file):
        with open(select_file, "r") as fh:
            for line in fh:
                nm, info = line.rstrip().split("\t")
                nm = nm.split(".")[0]
                mane_transcripts[nm] = info
    else:
        log.warning("mane file not found: {}".format(select_file))
    # step two : overrule with DB entries.
    rows = dbh.runQuery(
        "SELECT `nm_id`, `favourite`, `Comments` FROM `TranscriptSets_Data` td JOIN `TranscriptSets_x_Users` tu ON td.`set_id` = tu.`set_id` WHERE tu.`uid` = %s",
        args.userid,
    )
    for row in rows:
        # strip version
        row["nm_id"] = row["nm_id"].split(".")[0]
        if row["favourite"] == -1:
            del mane_transcripts[row["nm_id"]]
        elif row["favourite"] == 1:
            mane_transcripts[row["nm_id"]] = "UserKB :  {}".format(html.escape(row["Comments"]))
        else:
            log.warning(
                "Invalid favourite value. Ignoring entry: {} : {}".format(
                    row["nm_id"], row["favourite"]
                )
            )
    return mane_transcripts


def PrintRegularTable(variants_table, args, form):
    # table holding field titles.
    titles_tables = variants_table.replace("-final", "-titles")
    # how many extra rows are needed per variant in the table
    rowspan = 0
    # for ncbiGene : are there mane / user specific "favourite" transcripts.
    mane_nm = GetManeTranscripts(form, args)
    # get the build for alamut
    try:
        alamut_build = dbh.runQuery(
            "SELECT `Value` FROM `Annotation_DataValues` WHERE `Annotation` = 'Alamut'"
        )[0]["Value"]
    except Exception:
        alamut_build = "GRCh37"

    # get the build for franklin
    try:
        franklin_build = dbh.runQuery(
            "SELECT `Value` FROM `Annotation_DataValues` WHERE `Annotation` = 'Franklin'"
        )[0]["Value"]
    except Exception:
        franklin_build = "hg19"

    # fields to skip. (some go to validity box)
    skip_fields = [
        "sid",
        "set_inheritance",
        "diagnostic_class",
        "validation",
        "validation_details",
        "autoclassified",
        "validation_details",
        "favourite",
        "inheritance_mode",
    ]
    # get top-headers :
    top_rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  `title` FROM `{}` WHERE type = 'main' AND `parent` = '-' AND `title` NOT IN ('{}') ORDER BY idx LIMIT 1,9".format(
            titles_tables, "','".join(skip_fields)
        )
    )
    # other 'main' annotations (1000000 is a workaround. limit is mandatory, just OFFSET is not possible)
    main_rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  `title` FROM `{}` WHERE type = 'main' AND `parent` = '-' AND `title` NOT IN ('{}') ORDER BY `idx` LIMIT 10, 1000000".format(
            titles_tables, "','".join(skip_fields)
        )
    )
    if main_rows:
        rowspan += 1
        main_rows = sorted(main_rows, key=lambda x: x["title"].lower())
    # get subtables
    subtable_rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  `idx`, `title` FROM `{}` WHERE type = 'subtable' ORDER BY idx".format(
            titles_tables
        )
    )
    rowspan += len(subtable_rows)
    # get subtable entries.
    subtables = dict()
    for sr in subtable_rows:
        subtables[sr["title"]] = list()
        st_fields = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  `title` FROM `{}` WHERE type = 'child' AND `parent` = %s ORDER BY idx".format(
                titles_tables
            ),
            sr["idx"],
        )
        for st_f in st_fields:
            subtables[sr["title"]].append(st_f["title"])

    # load decoding info
    coded_fields, codes = LoadDecodingInfo()

    # get classifier variants
    classifier_variants = GetClassifierVariants()

    # subbox colors
    colors = {
        "-": "#aeaeae",
        "Pathogenic": "red",
        "UVKL4": "red",
        "UVKL3": "orange",
        "UVKL2": "green",
        "Benign": "blue",
        "False Positive": "grey",
    }
    # saving ? => checkboxes
    cbs = GetCheckboxes(form)
    # REGULAR TABLE : 100 VARIANTS PER PAGE. CONSTRUCT THEM ALL
    # how many variants ?
    row = dbh.runQuery("SELECT  HIGH_PRIORITY  COUNT(1) AS NrRows FROM `{}`".format(variants_table))
    nrVariants = row[0]["NrRows"]
    start = 0
    # if no variants, create a dummy page to prevent issues in api with missing files.
    if nrVariants == 0:
        open(
            os.path.join(
                args.directory,
                "results.null",
            ),
            "w",
        ).close()
    while start < nrVariants:
        # open the output file.
        fh = open(
            os.path.join(
                args.directory,
                "results.{}".format(start),
            ),
            "w",
        )
        # page header
        PrintHeader(fh, start)
        warnings = ""
        # warning if not enough annotations for subbox.
        if not main_rows and not subtable_rows:
            warnings += "<span style='color:red;'>WARNING:</span>SELECT more annotations to show validity and significance<br/>"
            # fh.write(
            #    "<p><span style='color:red;'>WARNING:</span>SELECT more annotations to show validity and significance<br/></p>"
            # )
        # warning on wrong gene symbols
        for f in comments.filter("gene not recognized"):
            # fh.write("<p>" + f + "<br/></p>")
            warnings += f.replace("WARNING:", "<span style='color:red;'>WARNING:</span>") + "<br/>"
        if warnings:
            fh.write(f"<p>{warnings}</p>")
        # table header
        fh.write("<p><table class=w100 cellspacing=0 id=VariantTable>")
        # fh.write(
        #    "<thead id='fixed_header'><tr><td style='width:1.5em'>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"
        # )
        fh.write("<thead><tr><th class=top colspan=2 style='width:10em'>Location</th>")

        # other 9 columns : default width.
        for r in top_rows:
            fh.write("<th class=top>{}</th>".format(ShortenTitle(r["title"])))
        fh.write("</tr></thead>")

        # just 100 variants per page.
        rows = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  `data` FROM `{}` ORDER BY `chr`, `position` LIMIT {},100".format(
                variants_table, start
            )
        )
        for row in rows:
            data = json.loads(row["data"])
            # set parental info.
            data = DetermineInheritance(data)
            # get raw subbox values (saving)
            raw_sb_values = "<span id='sv_{}' style='display:none'>{}|{}|{}|{}|{}</span>".format(
                data["variant_id"],
                data["diagnostic_class"],
                data["set_inheritance"],
                data["inheritance_mode"],
                data["validation"],
                data["validation_details"],
            )
            # decode.
            data = DecodeData(data, coded_fields, codes)
            # Convert values where needed
            data = ReWriteValues(data)
            # warning on alt-allele:
            if "N" in data["alt_allele"]:
                data[
                    "alt_allele"
                ] = "<span id=altwarning title='Alt.Allele contains -N-. This was replaced by -A- for ANNOVAR annotations.' style='color:red;'>{}</span>".format(
                    data["alt_allele"]
                )
            # franklin
            if form["affiliation"] in [1, 20]:
                franklin = f"<a target='_blank' href='https://uza.genoox.com/clinical-db/variant/snp/chr{data['chr']}-{data['position']}-{data['ref_allele']}-{data['alt_allele']}-{franklin_build}'><img style='height:1em;margin-bottom:-0.2em;' src='Images/layout/franklin.png'/></a>"
            else:
                franklin = f"<a target='_blank' href='https://franklin.genoox.com/clinical-db/variant/snp/chr{data['chr']}-{data['position']}-{data['ref_allele']}-{data['alt_allele']}-{franklin_build}'><img style='height:1em;margin-bottom:-0.2em;' src='Images/layout/franklin.png'/></a>"

            # first row : loc and main titles.
            igv = "chr{}:{}-{}".format(
                data["chr"], int(data["position"]) - 20, int(data["position"]) + 20
            )
            line = "<tr><td colspan=2>{} <a href='http://localhost:60151/goto?locus={}' class=igvlink onClick=\"setVisited('link{}')\" id='link{}'>chr{}:{}</a></td>".format(
                franklin, igv, data["variant_id"], data["variant_id"], data["chr"], data["position"]
            )
            # main columns.
            for r in top_rows:
                if r["title"] in data:
                    line = "{}<td>{}</td>".format(line, FormatValue(data[r["title"]], r["title"]))
                else:
                    line = "{}<td>.</td>".format(line)
            line = "{}</tr>".format(line)
            fh.write(line)

            # skip subbox if not enough annotations for subbox (no second row needed).
            if not rowspan:
                continue

            # fill the subbox:
            line = "<tr><td rowspan={} valign=top style='padding-top:0.5em'>".format(rowspan)
            line += "<div style='width:15em;border:1px solid {};padding:0.3em;font-size:0.75em' id='box_{}'>".format(
                colors[data["diagnostic_class"]], data["variant_id"]
            )
            line += "<span class=emph>Validity &amp; Significance</span>"
            # icon to samples with variants overlay
            line += "<span style='float:right' title='Show samples with this variant' onClick=\"SamplesWithVariant('{}','{}','{}')\" id='InfoDiv_{}'>".format(
                data["variant_id"], args.userid, args.sid, data["variant_id"]
            )
            line += "<img src='Images/layout/icon_info.gif' style='height:1.25em' /></span>"
            # icon to add to classifier (if allowed)
            line += ConstructAddToClassifier(data["variant_id"], classifier_variants)
            # icon to edit
            line += "<span style='float:right' title='Change Details' onClick=\"setValidity('{}','{}','{}')\">".format(
                data["variant_id"], args.sid, args.userid
            )
            line += "<img src='Images/layout/edit.gif' style='height:1.25em'/></span>"
            # linebreak
            line += "<br/>"
            # hidden info for saved result editing
            if form["slicestart"].startswith("save|"):
                # dc, $inh, $ihm, $val, $vd
                line += raw_sb_values
            # actual info
            line += "<span class=italic>Diagn.Class:</span> <span id='dc_{}'>{}</span><br/>".format(
                data["variant_id"], data["diagnostic_class"]
            )
            line += (
                "<span class=italic>Auto.Class'd:</span> <span id='ac_{}'>{}</span><br/>".format(
                    data["variant_id"], data["autoclassified"]
                )
            )
            # if data["diagnostic_class"] == "-":
            line += "<span class=italic >Inh.Mode:</span> <span id='im_{}' > {}</span><br/>".format(
                data["variant_id"], data["inheritance_mode"]
            )
            line += (
                "<span class=italic>Set Inheritance:</span> <span id='ih_{}'>{}</span><br/>".format(
                    data["variant_id"], data["set_inheritance"]
                )
            )
            line += "<span class=italic>Family Info:</span> {}<br/>".format(
                data["estimated_inheritance"]
            )
            line += "<span class=italic>Validation:</span> <span id='vm_{}'>{}</span>".format(
                data["variant_id"], data["validation"]
            )
            # todo : there used to be escape regex on this.. not sure why...
            line += "<br/><span class='italic indent' id='vd_{}'>{}</span>".format(
                data["variant_id"], data["validation_details"]
            )
            line += "</div></td>"
            fh.write(line)

            # regular annotations.
            if not main_rows:
                # trigger to include <tr>
                newline = False
            else:
                newline = True
                fh.write("<td colspan=10 class='sub cols-four'>")
                for mr in main_rows:
                    field = mr["title"]
                    if field in data:
                        value = FormatValue(data[field], field)
                    else:
                        value = "."
                    # fh.write("<span class='toleft w25 italic'>{}: {}</span>".format(ShortenTitle(field), value))
                    fh.write(
                        "<span class='italic'>{}: {}</span><br/>".format(ShortenTitle(field), value)
                    )
                fh.write("</td></tr>")
            # subtables
            for st in sorted(subtables):
                # start new table row
                if newline:
                    fh.write("<tr>")
                newline = True
                # cell & table header
                fh.write("<td colspan=10 class=sub>")
                fh.write("<table cellspacing=0 style='width:100%;'>")
                fh.write(
                    "<tr><th colspan={} class=subtable>{}</th></tr>".format(len(subtables[st]), st)
                )
                fh.write("<tr>")
                for t in subtables[st]:
                    fh.write("<th class=subtable>{}</th>".format(ShortenTitle(t)))
                fh.write("</tr>")
                # items
                if st not in data:
                    fh.write(
                        "<tr><td colspan={} class='italic'>no data</td></tr>".format(
                            len(subtables[st])
                        )
                    )
                else:
                    log.debug("Printing subtable")
                    for st_row in data[st]:
                        # entry marked as primary/MANE/favourite ?
                        if (
                            "RefSeq_Transcript" in st_row
                            and st_row["RefSeq_Transcript"].split(".")[0] in mane_nm
                        ):
                            versionless_nm = st_row["RefSeq_Transcript"].split(".")[0]
                            # if mane select/clinical entry : filled star
                            if mane_nm[versionless_nm] in ["MANE Plus Clinical", "MANE Select"]:
                                fav_icon = (
                                    "<img src='Images/layout/fav_full.png' style='height:1em;' />"
                                )
                            # user kb
                            else:
                                fav_icon = (
                                    "<img src='Images/layout/fav_empty.png' style='height:1em;' />"
                                )
                            fav_title = f"title='{mane_nm[versionless_nm]}'"
                        else:
                            fav_icon = fav_title = ""
                        fh.write("<tr {}>".format(fav_title))
                        # alamut
                        alamut = ""
                        if "RefSeq_Transcript" in st_row and not st_row["RefSeq_Transcript"] == ".":
                            alamut_txt = "{}({})".format(st_row["RefSeq_Transcript"], alamut_build)
                            # cpoint loaded ?
                            if "RefSeq_cPointNT" in st_row and not st_row["RefSeq_cPointNT"] == ".":
                                alamut_txt = "{}:{}".format(alamut_txt, st_row["RefSeq_cPointNT"])
                            alamut = f"<a target='at' href='http://localhost:10000/show?request={alamut_txt}'><img style='height:1.2em;margin-bottom:-0.2em;' src='Images/layout/alamut-visual-icon.png'/></a>"

                            if form["affiliation"] in [1, 20]:
                                alamut = f"<a target='at' href='http://localhost:10000/search?institution=abe0021&apikey=49244089&request={alamut_txt}'><img style='height:1.2em;margin-bottom:-0.2em;' src='Images/layout/alamut-visual-plus-icon.png'/></a>"
                        for t in subtables[st]:
                            if t in st_row:
                                value = FormatValue(st_row[t], t)
                            else:
                                value = "."
                            fh.write("<td>{} {} {}</td>".format(value, fav_icon, alamut))
                            # only print mane & alamut once.
                            fav_icon = ""
                            alamut = ""
                        fh.write("</tr>")
                fh.write(
                    "<tr><td class=last colspan={}>&nbsp;</td></tr>".format(len(subtables[st]))
                )
                fh.write("</table></td></tr>")
            # checkboxes if save
            if form["slicestart"].startswith("save|") and cbs != "":
                fh.write("<tr><td>&nbsp;</td><td colspan='10' class='sub'>")
                fh.write(cbs.replace("%vid%", str(data["variant_id"])))
                fh.write("<td></tr>")
        fh.write("</table>")
        start = start + 100


# a routine to shorten some annotation titles for display.


def ShortenTitle(t):
    # map
    map = {
        "Occurence_All_Samples_Heterozygous": "Occ.All.Samples_Het",
        "Occurence_All_Samples_HomAlt": "Occ.All.Samples_HomAlt",
        "Occurence_All_Samples_Any.Alternate": "Occ.All.Samples_anyAlt",
        "Mapping_Quality_Rank_Sum": "MQ.RankSum",
        "Genomic_SuperDups_Location": "SegDup.Loc",
        "Base_Quality_Rank_Sum": "BaseQ.RankSum",
    }
    return map.get(t, t)


def ConstructAddToClassifier(vid, classifier_variants):
    # variant not in classifier
    if not vid in classifier_variants:
        title = "Add variant to classifier"
        img = "Images/layout/add_to_classifier.png"
        # result = "<span style='float:right' title='{}' onClick=\"AddToClassifier('{}','{}')\" >".format(vid,args.sid)
        # result += "<img src='{}' style='height:1.25em' /></span>"
    # else : validated in a classifier?
    elif any([x["s"] == 1 for x in classifier_variants[vid].values()]):
        title = "Variant in classifier"
        img = "Images/layout/added_to_classifier.png"
    elif any([x["a"] == 1 for x in classifier_variants[vid].values()]):
        title = "Variant needs validation"
        img = "Images/layout/validate_classifier.png"
    else:
        title = "Variant validation is pending"
        img = "Images/layout/validate_classifier.png"

    result = "<span style='float:right' title='{}' onClick=\"AddToClassifier('{}','{}')\" ><img src='{}' style='height:1.25em' /></span>".format(
        title, vid, args.sid, img
    )
    return result


def GetClassifierVariants():
    # get classifiers with access:
    rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  cid, can_validate FROM `Users_x_Classifiers` WHERE uid = %s AND ( `can_use` + `can_add` + `can_validate` + `can_remove` + `can_share` ) > 0",
        args.userid,
    )
    if not rows:
        return {}
    classifiers = {x["cid"]: x["can_validate"] for x in rows}
    rows = dbh.runQuery(
        "SELECT  HIGH_PRIORITY  cid, vid, validate_reject, comments FROM `Classifiers_x_Variants` WHERE cid IN ('{}')".format(
            "','".join([str(x) for x in classifiers.keys()])
        )
    )
    result = {}
    for variant in rows:
        # skip rejected
        if variant["validate_reject"] == -1:
            continue
        if not variant["vid"] in result:
            result[variant["vid"]] = {}
        result[variant["vid"]][variant["cid"]] = {
            "s": variant["validate_reject"],
            "a": classifiers[variant["cid"]],
            "c": variant["comments"],
        }
    return result


def FormatValue(value, title=""):

    if CheckVariableType(value, float):
        # AltDepth/RefDepth should not be formatted.
        if title in ["Alt_Allele_Depth", "Ref_Allele_Depth"]:
            return value
        # occurences should not be formatted
        if "Occurence" in title:
            return value
        # exons should not be formatted
        if "Exon" in title:
            return value
        # length values should not be formatted
        if "length" in title.lower():
            return value

        # cast to float here : '-1' passed test.
        value = round(float(value), 3)
    return value


def PrintHeader(fh, start, type="default"):
    if type == "default":
        # fh.write(
        #    "<div id='QueryLog'></p><h4>Query Logs</h4><span class=indent onmouseover='' style='cursor: pointer;' OnClick=\"LoadQueryLog('{}')\"> Load runtime details.</span></div>".format(
        #        os.path.join(args.directory, "stdout")
        #    )
        # )
        page = int(start / 100) + 1
        fh.write("<h4>Results Table : Page {}</h4>".format(page))
        if not args.summary:
            fh.write(
                "<p><span class='emph red'>WARNING: </span> Variant frequencies are not up to date!</p>"
            )


# what fields are coded + map of codes to txt
def LoadDecodingInfo():
    # config settings : holds info on what fields are coded.
    ac = ET.parse(os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/Config.xml"))
    # parent map (find out parent based on child node)
    parent_map = {c: p for p in ac.getroot().iter() for c in p}

    # subtable + item : Table_x_Column  (all coded fields are in subtables).
    coded_fields = {}
    for c in ac.getroot().findall(".//coded"):
        # parent node
        p_node = parent_map[c]
        p_node_subtable = p_node.find("subtable").text
        p_node_tag = p_node.tag
        # default : take table from the SELECT   string
        table, column = (
            p_node.find("select").text.split(" ")[0].replace("`", "").split(".")
        )  # replace(".", "_")
        # in some cases, joined queries mess up coding assignment, so use the DecodeTable field to override this.
        if ET.iselement(p_node.find("DecodeTable")):
            table = p_node.find("DecodeTable").text

        coded_fields["{}:{}".format(p_node_subtable, p_node_tag)] = "{}_{}".format(table, column)

    # value codings:
    rows = dbh.runQuery(
        "SELECT HIGH_PRIORITY id, `Table_x_Column`, `Item_Value` FROM `Value_Codes`"
    )
    codes = dict()
    for row in rows:
        # map column + ID to text : rewrite "table" to "subtable"
        codes["{}_{}".format(row["Table_x_Column"], row["id"])] = row["Item_Value"]

    # hard-coded items (not in annotation checkboxes for the user, or default)
    coded_fields.update(
        {
            x: x
            for x in {
                "chr",
                "diagnostic_class",
                "set_inheritance",
                "inheritance_mode",
                "autoclassified",
                "Genotype",
            }
        }
    )
    codes.update(
        {
            "diagnostic_class_0": "-",
            "diagnostic_class_1": "Pathogenic",
            "diagnostic_class_2": "UVKL4",
            "diagnostic_class_3": "UVKL3",
            "diagnostic_class_4": "UVKL2",
            "diagnostic_class_5": "Benign",
            "diagnostic_class_6": "False Positive",
            "set_inheritance_0": "ND",
            "set_inheritance_1": "P",
            "set_inheritance_2": "M",
            "set_inheritance_3": "DN",
            "set_inheritance_4": "Both",
            "set_inheritance_ND": "Not Defined",
            "set_inheritance_P": "Paternal",
            "set_inheritance_M": "Maternal",
            "set_inheritance_DN": "De Novo",
            "set_inheritance_Both": "Both Parents",
            "inheritance_mode_0": "-",
            "inheritance_mode_1": "Dominant",
            "inheritance_mode_2": "Recessive",
            "inheritance_mode_3": "Uknown",
            "autoclassified_": "no",
            "autoclassified_0": "no",
            "autoclassified_1": "yes",
            "Genotype_0": "Hom.Reference",
            "Genotype_1": "Heterozygous",
            "Genotype_2": "Hom.Alternative",
        }
    )
    codes.update({"chr_{}".format(x): x for x in range(1, 23)})
    codes.update({"chr_23": "X", "chr_24": "Y", "chr_25": "MT"})
    # hard-coded overrides for items fetched from joined tables (needs to address this)
    for f in coded_fields:
        # clinvar_db
        if f.startswith("ClinVar:"):
            coded_fields[f] = coded_fields[f].replace("ClinVar_db_", "Variants_x_ClinVar_ncbigene_")
    return coded_fields, codes


# decode encoded fields in the json
def DecodeData(data, coded_fields, codes):
    for k in data:
        # plain coded (subbox data)
        if k in coded_fields:
            try:
                data[k] = codes["{}_{}".format(coded_fields[k], data[k])]
            except KeyError as e:
                log.warning(
                    "Missing single decoding info for {} : {}_{} : {}".format(
                        k, coded_fields[k], data[k], e
                    )
                )
                data[k] = "."
            except Exception as e:
                log.error("Decoding error : {}".format(e))
                sys.exit(1)
        # subtable
        elif isinstance(data[k], list):
            # go over items in list.
            for i in range(len(data[k])):
                # each item is dict. can be coded.
                for sk in data[k][i]:
                    log.debug(data[k][i])
                    if "{}:{}".format(k, sk) in coded_fields:
                        try:

                            data[k][i][sk] = codes[
                                "{}_{}".format(coded_fields["{}:{}".format(k, sk)], data[k][i][sk])
                            ]
                        except KeyError as e:
                            log.warning(
                                "Missing list decoding info for {}_{} : {}".format(
                                    coded_fields["{}:{}".format(k, sk)], data[k][i][sk], k
                                )
                            )
                            data[k][i][sk] = "."
                        except Exception as e:
                            log.error("Decoding error : {}".format(e))
                            sys.exit(1)

    return data


def DetermineInheritance(data):
    if "parents" in data:
        # log.info("parents found")
        # both present :
        if "mother" in data["parents"] and "father" in data["parents"]:
            # variant in both.
            if data["parents"]["mother"] > 0 and data["parents"]["father"] > 0:
                data["estimated_inheritance"] = "in both parents"
            elif data["parents"]["mother"] > 0:
                data["estimated_inheritance"] = "in mother"
            elif data["parents"]["father"] > 0:
                data["estimated_inheritance"] = "in father"
            else:
                # present but zero => called as ref.
                data["estimated_inheritance"] = "de novo"
        # one parent has data:
        elif "mother" in data["parents"]:
            if data["parents"]["mother"] > 0:
                data["estimated_inheritance"] = "in mother"
            else:
                data["estimated_inheritance"] = "not in mother"
        elif "father" in data["parents"]:
            if data["parents"]["father"] > 0:
                data["estimated_inheritance"] = "in father"
            else:
                data["estimated_inheritance"] = "not in father"
        # parent with unknown gender.
        elif "unknown" in data["parents"]:
            if data["parents"]["unknown"] > 0:
                data["estimated_inheritance"] = "in parent"
            else:
                data["estimated_inheritance"] = "not in parent"
        # unclear entry, ignore it
        else:
            data["estimated_inheritance"] = " no data"
    else:
        data["estimated_inheritance"] = " no data"

    return data


def CleanDB(config):
    prefix = config["RUNTIME"]["tmp_prefix"]
    log.debug("Cleanup up temporary tables with prefix '{}'".format(prefix))
    tables = [
        x[0] for x in dbh.runQuery("SHOW TABLES LIKE %s", "{}-%".format(prefix), as_dict=False)
    ]
    for t in tables:
        log.debug("  - Clean :  {}".format(t))
        dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(t))


def SetStatus(status):
    with open(
        os.path.join(args.directory, "status"),
        "w",
    ) as fh:
        fh.write(status)


if __name__ == "__main__":
    # track start time
    global start_time
    start_time = time.time()
    # load config
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # start logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging : on rare occasions, the rollover causes permission errors. retry 5 times.

    try:
        if config["LOGGING"]["LOG_LEVEL"] == "DEBUG":
            quiet = False
        else:
            quiet = True
        setup_logging(
            name="VariantDB_SampleQuery",
            level=config["LOGGING"]["LOG_LEVEL"],
            log_dir=config["LOGGING"]["LOG_PATH"],
            to_addrs=config["LOGGING"]["LOG_EMAIL"],
            quiet=quiet,
            permissions=777,
        )
    except Exception as e:
        # no rights on specified log file. use user home dir.
        print(f"WARNING: Logger setup failed ({e}). Retry using default locations")
        try:
            setup_logging(
                name="VariantDB_SampleQuery",
                level=config["LOGGING"]["LOG_LEVEL"],
                to_addrs=config["LOGGING"]["LOG_EMAIL"],
                quiet=quiet,
            )
        except Exception as e:
            print(f"Got error : {e}")
            raise e
    log = get_logger("main")
    log.info(msg)
    log.debug("DEBUG channel active")
    # if UI : needs user
    if args.source.upper() == "UI" and not args.user:
        # SetStatus(args.item, "error")
        log.error("No userID provided for UI call")
        sys.exit(1)

    # add the filter modules path:
    sys.path.append(os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Filter"))

    # locate working dir.
    if args.source.upper() == "API":
        args.directory = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "api/query_results", args.item
        )

    elif args.source.upper() == "UI":
        args.directory = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", args.user, args.item
        )
    else:
        log.error("Source type not valid: {}. Must be API or UI".format(args.source))
        # set status
        # SetStatus("error")
        # with open(os.path.join(args.directory, "status"), "w") as fh:
        #    fh.write("error")
        sys.exit(1)

    # add log streams to working dir with reduced output in stdout, but default format in stderr
    fmt = "%(asctime)s {app} %(levelname)-8s - %(message)s."
    add_filestream(
        name=args.source,
        fmt=fmt,
        file=os.path.join(args.directory, "stdout"),
        min_level="INFO",
        max_level="WARN",
    )
    add_filestream(name=args.source, file=os.path.join(args.directory, "stderr"), min_level="ERROR")

    # prune queue if API:
    if args.source.upper() == "API":

        try:
            PruneQueue(config, args)
        except Exception as e:
            log.error("Failed to remove api job from queue: {}".format(e))
            # set status
            SetStatus("error")
            sys.exit(1)

    # read runtime data
    try:
        form = ReadForm(args.directory)
    except Exception as e:
        log.error("Failed to extract form data from {} : {}".format(args.directory, e))
        # set status
        SetStatus("error")
        sys.exit(1)

    # get database connection.
    try:
        dbh = GetDBConnection(quiet=False)
    except Exception as e:
        log.error("Could not connect to database : {}".format(e))
        # set status
        SetStatus("error")
        sys.exit(1)

    # validate permissions.
    try:
        form, args = CheckPermissions(args, form)
    except Exception as e:
        log.error("Permission problem: {}".format(e))
        # set status
        SetStatus("error")
        sys.exit(1)

    # start multiprocessing:
    if "MYSQLTHREADS" in config["DATABASE"] and int(config["DATABASE"]["MYSQLTHREADS"]) > 2:
        log.debug("Starting {} worker threads".format(config["DATABASE"]["MYSQLTHREADS"]))
        pool = Pool(processes=int(config["DATABASE"]["MYSQLTHREADS"]))
    else:
        log.info("Using a single worker thread")
        pool = Pool(processes=1)
    # set status :
    SetStatus("running")
    # log total number of variants.
    try:
        nrv = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  COUNT(1) as NrVars FROM `Variants_x_Samples`  WHERE sid = '{}'".format(
                form["sid"]
            )
        )
        log.info("Total variants in sample: {}".format(nrv[0]["NrVars"]))
        comments.add("NOTICE: Total number of variants : {}".format(nrv[0]["NrVars"]))
        total_nrv = nrv[0]["NrVars"]
    except Exception as e:
        log.error("Failed to count full variants set: {}".format(e))
        # set status
        SetStatus("error")
        sys.exit(1)

    # Filter Variants.
    try:
        filtered_table = Filter(config, form, args)
    except Exception as e:
        log.error("Failed to run variant filters : {}".format(e))
        # set status
        SetStatus("error")
        sys.exit(1)

    # log number of retained variants.
    try:
        final_nrv = dbh.runQuery(
            "SELECT  HIGH_PRIORITY  COUNT(1) FROM `{}`".format(filtered_table), as_dict=False
        )[0][0]
        log.info("Retained variants: {}".format(final_nrv))
        comments.add("NOTICE: {} variants were retained".format(final_nrv))
    except Exception as e:
        log.error("Failed to count retained variants : {}".format(e))
        # set status
        SetStatus("error")
        sys.exit(1)
    # write out stats.
    with open(os.path.join(args.directory, "stats"), "w") as fh:
        fh.write("Total={}\n".format(total_nrv))
        fh.write("Passed={}\n".format(final_nrv))

    # annotate.
    log.info("Annotating variants")
    try:
        variants_table = AnnotateVariants(config, form, args, filtered_table)
    except Exception as e:
        log.error("Failed to annotate retained variants : {}".format(e))
        # set status
        SetStatus("error")
        sys.exit(1)

    # Build output.
    log.info("Constructing output")
    try:
        BuildOutput(variants_table, form, args)
    except Exception as e:
        log.error("Failed to build results structure : {}".format(e))
        # set status
        SetStatus("error")
        sys.exit(1)

    # clean up DB.
    try:
        if not config["LOGGING"]["LOG_LEVEL"] == "DEBUG":
            CleanDB(config)
    except Exception as e:
        log.error(
            "Failed to clean up the database for {}: {}".format(config["RUNTIME"]["tmp_prefix"], e)
        )

    # set status
    SetStatus("finished")
