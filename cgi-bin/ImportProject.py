from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger, close_logger

import configparser
import argparse
import os
import sys

# error classes
class DataError(OSError):
    pass


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Import a project dump into VariantDB
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # working directory
    parser.add_argument(
        "-d",
        "--directory",
        required=True,
        help="Working directory, extracted from GPG encrypted archive",
    )
    args = parser.parse_args()
    return config, args


def ValidateWorkingDir(wd):
    # wd present
    if not os.path.isdir(wd):
        raise DataError("Provided working directory does not exist: {}.".format(wd))

    # mandatory files present
    for f in ["project.txt", "samples.txt", "uid", "variants.txt"]:
        if not os.path.isfile(os.path.join(wd, f)):
            raise DataError("Missing mandatory file: {}".format(os.path.join(wd, f)))

    return True


# get user details based on uid.
def GetUser(dbh, wd):
    try:
        with open(os.path.join(wd, "uid")) as f:
            uid = f.readline()
        user_details = dbh.runQuery(
            "SELECT id, LastName, FirstName, email, level FROM `Users` WHERE id = %s", uid
        )
    except Exception as e:
        raise MySqlError(e)
    if len(user_details) == 0:
        raise ValueError("Invalid userid provided : {}. User not found in VariantDB".format(uid))
    if user_details[0]["level"] == 0:
        raise ValueError("Inactive userid : {}. This user is disabled".format(uid))
    return user_details[0]


# create project, set permissions, return project_id
def CreateProject(dbh, wd, uid):
    project_info = dict()
    try:
        with open(os.path.join(wd, "project.txt")) as f:
            for line in f:
                k, v = line.rstrip().split("=")
                project_info[k] = v
    except Exception as e:
        raise DataError("Could not read project info: {}".format(e))
    project_info["uid"] = uid
    try:
        # create project, summary status default to zero
        pid = dbh.insertQuery(
            "INSERT INTO `Projects` (Name, created, userID, SummaryStatus) VALUES (%(name)s,%(date)s,%(uid)s, 1)",
            project_info,
        )
        dbh.doQuery(
            "INSERT INTO `Projects_x_Users` (pid, uid, editvariant,editclinic,editsample) VALUES (%s,%s,1,1,1)",
            [pid, uid],
        )
        # clear summarystatus of the user.
        dbh.doQuery("UPDATE `Users` SET SummaryStatus = 1 WHERE id = %s", uid)
    except Exception as e:
        raise MySqlError(e)
    project_info["pid"] = pid
    return project_info


# create samples && assign to project
def LoadSamples(dbh, wd, project_info):
    sample_info = dict()
    # individual samples
    with open(os.path.join(wd, "samples.txt")) as f:
        for line in f:
            # base info
            s_cols = line.rstrip().split("\t")
            try:
                sid = dbh.insertQuery(
                    "INSERT INTO `Samples` (Name, gender, IsControl, affected) VALUES (%s,%s,%s,%s)",
                    s_cols[0:4],
                )
            except Exception as e:
                raise MySqlError(e)
            sample_info[s_cols[0]] = {
                "gender": s_cols[1],
                "control": s_cols[2],
                "affected": s_cols[3],
                "sid": sid,
            }
            # link to project
            try:
                dbh.doQuery(
                    "INSERT INTO `Projects_x_Samples` (pid,sid) VALUES (%s,%s)",
                    [project_info["pid"], sid],
                )
            except Exception as e:
                raise MySqlError(e)
            # clinical info?
            if os.path.isfile(os.path.join(wd, "clin_{}".format(s_cols[0]))):
                with open(os.path.join(wd, "clin_{}".format(s_cols[0]))) as cf:
                    clinic = cf.read().rstrip()
                try:
                    dbh.doQuery("UPDATE `Samples` SET `clinical` = %s WHERE id = %s", [clinic, sid])
                except Exception as e:
                    raise MySqlError(e)
    # relations
    if os.path.isfile(os.path.join(wd, "relations.txt")):
        with open(os.path.join(wd, "relations.txt")) as f:
            for line in f:
                s1, s2, rel = line.rstrip().split("\t")
                try:
                    dbh.doQuery(
                        "INSERT INTO `Samples_x_Samples` (sid1,sid2,Relation) VALUES (%s,%s,%s)",
                        [sample_info[s1]["sid"], sample_info[s2]["sid"], rel],
                    )
                except Exception as e:
                    raise MySqlError(e)
    # hpo
    if os.path.isfile(os.path.join(wd, "relations.txt")):
        with open(os.path.join(wd, "hpo.txt")) as f:
            for line in f:
                s, tid = line.rstrip().split("\t")
                try:
                    dbh.doQuery(
                        "INSERT INTO `Samples_x_HPO_Terms` (sid,tid) VALUES (%s,%s)",
                        [sample_info[s]["sid"], tid],
                    )
                except Exception as e:
                    raise MySqlError(e)

    return sample_info


def LoadVariants(dbh, wd, project, samples):
    # track vids
    variants = dict()
    # process.
    with open(os.path.join(wd, "variants.txt")) as vf:
        headers = vf.readline().rstrip().split("\t")
        nr_vs_fields = len(headers) - 7 + 1
        qm = "%s," * nr_vs_fields
        qm = qm.rstrip(",")
        vs_iquery = "INSERT INTO `Variants_x_Samples` VALUES ({})".format(qm)
        for line in vf:
            cols = line.rstrip().split("\t")
            vid, variants = GetVariantID(variants, dbh, cols)
            # get sample_id
            cols[7] = samples[cols[7]]["sid"]
            # insert.
            try:
                dbh.insertQuery(vs_iquery, [vid] + cols[7:])
            except Exception as e:
                raise MySqlError(e)
            # limit memory usage:
            if len(variants) > 1000000:
                variants = dict()
    return True


def GetVariantID(variants, dbh, cols):
    # queries:
    v_query = "SELECT id,Stretch,StretchUnit FROM `Variants` WHERE start = %s AND stop = %s AND chr = %s AND RefAllele = %s AND AltAllele = %s"
    v_iquery = "INSERT INTO `Variants` (start, stop, chr, RefAllele, AltAllele, Stretch, StretchUnit) VALUES (%s,%s,%s,%s,%s,%s,%s)"
    v_uquery = "UPDATE `Variants` SET Stretch = %, StretchUnit = % WHERE id = %"
    if "{}:{}:{}:{}".format(cols[2], cols[0], cols[3], cols[4]) not in variants:
        try:
            var_info = dbh.runQuery(v_query, cols[0:5])
            if len(var_info) == 0:
                vid = dbh.insertQuery(v_iquery, cols[0:7])
            else:
                vid = var_info[0]["id"]
                # repeat info known?
                if var_info[0]["Stretch"] == 0 and cols[5] > 0:
                    dbh.doQuery(v_uquery, [cols[5], cols[6], vid])
            variants["{}:{}:{}:{}".format(cols[2], cols[0], cols[3], cols[4])] = vid
        except Exception as e:
            raise MySqlError(e)
    else:
        vid = variants["{}:{}:{}:{}".format(cols[2], cols[0], cols[3], cols[4])]
    return vid, variants


# main routine : does the multi stage import.
def main(wd, config):
    # get database connection:
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))
    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        sys.exit(1)

    # get user details
    try:
        user_details = GetUser(dbh, wd)
    except Exception as e:
        log.error("Failed to get User details: {}".format(e))
        sys.exit(1)
    log.info(
        "Loading data into account of {} {}".format(
            user_details["LastName"], user_details["FirstName"]
        )
    )

    # create project.
    try:
        project_info = CreateProject(dbh, wd, user_details["id"])
    except Exception as e:
        log.error("Failed to create project: {}".format(e))
        sys.exit(1)
    log.info("Created project: {}".format(project_info["name"]))

    # load samples
    try:
        samples = LoadSamples(dbh, wd, project_info)
    except Exception as e:
        log.error("Failed to create samples: {}".format(e))
        sys.exit(1)
    log.info("Created {} samples".format(len(samples)))

    # load variants
    try:
        LoadVariants(dbh, wd, project_info, samples)
    except Exception as e:
        log.error("Failed to load Variants: {}".format(e))
        sys.exit(1)
    log.info("Variants loaded.")

    # custom annotations.
    # try:
    #    LoadCustomAnnotations(dbh,wd,project_info,samples)

    # queue Summary
    dbh.doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id = %s", project_info["pid"])
    dbh.doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = %s", project_info["uid"])


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)
    # locate log files
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_UI_python",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)

    ## Validate WD
    try:
        ValidateWorkingDir(args.directory)
    except DataError as e:
        log.error(e)

    ## Main Import
    main(args.directory, config)
