#!/usr/bin/env python

# inhouse modules
from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger
from CMG.API import Connection
from CMG.Utils import CheckVariableType

# modules for pdf generation
from CMG.LaTeX import Document, Environments, Commands
import pylatex as pl
import pylatex.utils as pu
import pylatex.package as pp
import pylatex.base_classes as pb

# general modules
import configparser
import argparse
import os
import sys
import fcntl
import hashlib
import subprocess
import json
import time
import datetime
from collections import defaultdict, OrderedDict, Counter
import math

# custom regex module
from CMG.Utils import Re

re = Re()
# error classes


class DataError(OSError):
    pass


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    # set path :
    if "PATH" in config["PATH"]:
        config["PATH"]["PATH"] = "{}:{}".format(config["PATH"]["PATH"], os.environ["PATH"])
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    # command line arguments
    help_text = """
        GOAL:
        #####
            - Monitor report Queue 
            - execute requests through internal api calls.
            - compose the latex files.
        """

    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # working directory
    parser.add_argument(
        "-r",
        "--report_id",
        required=False,
        help="Report Query ID, numeric value. If specified, only performing PDF formatting of request item (for debugging)",
    )
    args = parser.parse_args()

    return config, args


def GetDBConnection(quiet=True):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)

    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        # set status
        # not possible, db connection is generalized over reports
        # with open(
        #    os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/", queryKey, "status"),
        #    "w",
        # ) as fh:
        #    fh.write('error')
        return False
    if not quiet:
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    return dbh


def GetQueueItem(config):
    if not os.path.exists(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue")
    ):
        log.info(
            "Queue file not found: {}".format(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue")
            )
        )
        time.sleep(30)
        return False
    # get queue:
    # lcok queue file.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue.lck"),
        "w+",
    ) as qh:
        fcntl.lockf(qh, fcntl.LOCK_EX)
        # write to queue file:
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue"),
            "r",
        ) as q:
            queryKeys = q.readlines()
            queryKeys = [x.rstrip() for x in queryKeys if x.rstrip()]
        # nothing to do?
        try:
            # get first entry
            queryKey = queryKeys.pop(0)
        except IndexError:
            # nothing to do
            fcntl.lockf(qh, fcntl.LOCK_UN)
            return False
        # rewrite remainder of the queue to the file.
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.report_queue"),
            "w",
        ) as q:
            q.write("{}\n".format("\n".join(queryKeys)))
    # return the key.
    return queryKey


def SetStatus(queryKey, status):
    log.info("Setting status of {} to {}".format(queryKey, status))
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/", queryKey, "status"),
        "w",
    ) as fh:
        fh.write(status)


def ProcessItem(config, queryKey):
    # get report details:
    details = GetReportDetails(queryKey)

    # set status
    SetStatus(queryKey, "Running")

    # submit sections to api
    query_ids = {}  # section_id => queryKey
    for sidx, section_id in enumerate(details["report_sections"].split(",")):
        # build API call
        endpoint = "SubmitQuery/sample/{}".format(details["form"]["sid"])
        # parameters
        params = {
            "fid": details["sections"][section_id]["FilterSet"],
            "aid": details["sections"][section_id]["Annotations"],
            "uid": details["form"]["uid"],
        }
        # checkbox details, if any
        if details["sections"][section_id]["checkboxes"]:
            params["cbs"] = details["sections"][section_id]["checkboxes"]

        # submit
        #   => use VariantDB module ? no, does not support local_call without apikey.
        #   => use the general API module
        try:
            response = vdb.get(endpoint, params).json()
            query_ids[sidx] = response["results"]["query_key"]
            log.info(f"Submitted report {queryKey} section {sidx} as {query_ids[sidx]}")
        except Exception as e:
            SetStatus(queryKey, "Failed")
            raise Exception(
                "Failed to submit section query to API for section id = {} :  {}".format(
                    section_id, e
                )
            )

        # submit again if saving is requested.
        if "save" in details["form"]:
            # check DB for alternate AID.
            save_aids = params["aid"].split(",")
            save_aid = list()
            for idx in range(0, len(save_aids)):
                # original name
                a_name = dbh.runQuery(
                    "SELECT `AnnotationName` FROM `Users_x_Annotations` WHERE aid = %s",
                    save_aids[idx],
                ).pop()["AnnotationName"]
                # **_short in annotation sset name => strip to **
                a_name_full = a_name
                a_name = a_name.replace("_short", "")
                # search accessible annotation sets as ** or **_long : more info in online version.
                sets = dbh.runQuery(
                    """
                    SELECT ua.`aid`, ua.`AnnotationName` FROM `Users_x_Annotations` ua LEFT JOIN `Users_x_Shared_Annotations` usa ON ua.aid = usa.aid 
                    WHERE (ua.uid = %s OR usa.uid = %s ) AND (ua.`AnnotationName` = %s OR ua.`AnnotationName` = %s) AND NOT ua.`aid` = %s
                    """,
                    (
                        params["uid"],
                        params["uid"],
                        a_name,
                        "{}_long".format(a_name),
                        save_aids[idx],
                    ),
                )
                if sets:
                    log.debug(sets)
                    for set in sets:
                        log.info(
                            "Replacing {} by {} for saved results".format(
                                a_name_full, set["AnnotationName"]
                            )
                        )
                        save_aid.append(str(set["aid"]))
                else:
                    log.info(f"No alternate found for {a_name_full}")
                    save_aid.append(str(save_aids[idx]))
            params["aid"] = ",".join(save_aid)

            params["save"] = details["form"]["save"]
            if "addfiltername" in details["form"] and details["form"]["addfiltername"]:
                filtername = dbh.runQuery(
                    "SELECT FilterName FROM `Users_x_FilterSettings` WHERE fid = %s",
                    details["sections"][section_id]["FilterSet"],
                ).pop()["FilterName"]
                filtername = filtername.replace(" ", "_")
                params["save"] += "_{}".format(filtername)
            if "comment" in details["form"]:
                params["comment"] = details["form"]["comment"]
            # submit, no need to track the id
            try:
                response = vdb.get(endpoint, params).json()
                if not response["status"] == "ok":
                    raise Exception("Submission Failed: {}".format(response["msg"]))
                log.info(
                    "Submitted save-request : '{}', query ID = {}".format(
                        params["save"], response["results"]["query_key"]
                    )
                )
            except Exception as e:
                SetStatus(queryKey, "Failed")
                raise Exception(
                    "Failed to submit section query to API for section id = {} : {} : {}".format(
                        section_id, response, e
                    )
                )

    # write out the query_ids
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/", queryKey, "section_query_ids"
        ),
        "w",
    ) as fh:
        for k, v in query_ids.items():
            fh.write("{}={}\n".format(k, v))
    return details, query_ids


def GetReportDetails(reportKey):
    # read form.
    form = {}
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/", reportKey, "form"), "r"
    ) as fh:
        for line in fh:
            k, v = line.rstrip().split("=", 1)
            form[k] = v
    # details from DB
    try:
        report_details = dbh.runQuery(
            "SELECT Name AS report_name, Description AS report_description, Sections AS report_sections FROM `Report_Definitions` WHERE rid = %s",
            form["rid"],
        ).pop()
    except Exception as e:
        SetStatus(reportKey, "Failed")
        raise Exception(
            "Could fetch report details for queryKey {} : report_id {} : {}".format(
                reportKey, form["rid"], e
            )
        )

    # add form
    report_details["form"] = form
    # details of sections
    report_details["sections"] = {}
    for sidx, section_id in enumerate(report_details["report_sections"].split(",")):
        log.info(
            "Getting section {} (id:{}) for report {}, from call {}".format(
                sidx, section_id, form["rid"], reportKey
            )
        )
        # section details from DB.
        try:
            section_details = dbh.runQuery(
                "SELECT rs.Name, rs.Description, rs.FilterSet, rs.Annotations, rs.checkboxes, rs.vspace, f.FilterName FROM `Report_Sections`  rs JOIN `Users_x_FilterSettings` f ON f.fid = rs.FilterSet WHERE rsid = %s",
                section_id,
            ).pop()
        except Exception as e:
            SetStatus(reportKey, "Failed")
            raise Exception(
                "Failed to extract section details for rsid = {} : {}".format(section_id, e)
            )

        # checkbox details, if any
        if section_details["checkboxes"]:
            section_details["cbx"] = {}
            longest = ""
            try:
                checkboxes = dbh.runQuery(
                    "SELECT Title,Options FROM `Report_Section_CheckBox` WHERE cid IN ({})".format(
                        section_details["checkboxes"]
                    )
                )
            except Exception as e:
                SetStatus(reportKey, "Failed")
                raise Exception(
                    "Failed to extract checkbox details for rsid = {} : {}".format(section_id, e)
                )

            for cb in checkboxes:
                items = cb["Options"].split("||")
                # track longest string.
                longest = max(items + [longest], key=len)
                section_details["cbx"][cb["Title"]] = items
            section_details["longest_checkbox"] = longest

        # add section details to the runningreport info.
        report_details["sections"][section_id] = section_details
    return report_details


def FormatValue(value, title=""):
    ignore_formatting = [
        "^Ref_Allele_Depth",
        "^Alt_Allele_Depth",
        "^Total_Depth",
        "AN$",
        "^Occurence",
    ]
    if any(re.search(x, title) for x in ignore_formatting):
        return value
    if CheckVariableType(value, float):
        # cast to float here : '-1' passed test.
        value = round(float(value), 3)
    return value


def CheckQueryStatus(query_id):
    try:
        with open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/", str(query_id), "status"
            ),
            "r",
        ) as fh:
            status = fh.readline().rstrip()
        return status
    except Exception as e:
        log.error("Could not read status file for {} : {}".format(query_id, e))
        return "Error"


def CheckReportStatus(running_reports, config):
    # loop over queryKeys
    for reportKey in list(running_reports.keys()):
        # report has sections: check associated api-query_ids
        status = list()
        for section_id in list(running_reports[reportKey]["query_ids"].keys()):
            # status?
            status.append(CheckQueryStatus(running_reports[reportKey]["query_ids"][section_id]))
        # error ?
        if any(x == "Failed" for x in status):
            log.info(f"Report {reportKey} failed")
            del running_reports[reportKey]
            SetStatus(reportKey, "Failed")
            continue
        if not all(x == "finished" for x in status):
            log.info("Report {} not done yet.".format(reportKey))
            continue
        log.info("Report {} is ready for formatting".format(reportKey))
        try:
            FormatReport(reportKey)
            del running_reports[reportKey]
            SetStatus(reportKey, "finished")
        except Exception as e:
            log.error("Could not format Report for key {} : {}".format(reportKey, e))
            SetStatus(reportKey, "Failed")

    return running_reports


def FormatReport(reportKey, report_details=None):
    log.info("Formatting {}".format(reportKey))
    # details not provided when using ad-hoc formatting of a single report.
    if not report_details:
        try:
            details = GetReportDetails(reportKey)
        except Exception as e:
            raise e

        query_ids = {}
        with open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "api/query_results/",
                reportKey,
                "section_query_ids",
            ),
            "r",
        ) as fh:
            for line in fh:
                k, sectionKey = line.rstrip().split("=", 1)
                query_ids[k] = sectionKey

        report_details = {"details": details, "query_ids": query_ids}

    # get sample name.
    sample_name = dbh.runQuery(
        "SELECT name FROM `Samples` WHERE id = %s", report_details["details"]["form"]["sid"]
    ).pop()["name"]

    # prepare the document
    Report = InitializeLaTeX(sample_name)
    seen_variants = set()
    # process sections
    for sidx, section_id in enumerate(report_details["details"]["report_sections"].split(",")):
        sectionKey = report_details["query_ids"][str(sidx)]
        log.info(
            "Formatting section {}/{} : section_id={} : sectionKey={}".format(
                sidx + 1, len(report_details["query_ids"].keys()), section_id, sectionKey
            )
        )

        Report, seen_variants = FormatSection(
            Report,
            sectionKey,
            report_details["details"]["sections"][str(section_id)],
            seen_variants,
        )

    # generate pdf
    # with open("/tmp/dump.text", "w") as fh:
    #    fh.write(Report.dumps())
    Report.generate_pdf(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/", reportKey, "Report"),
        silent=True,
        clean_tex=False,
    )


def InitializeLaTeX(sample_name):
    # create
    geometry = {"top": "2cm", "left": "0.8cm", "right": "1.2cm", "bottom": "2cm"}
    Report = Document.GetDocument(
        document_options=["hidelinks", "10pt", "a4paper"], geometry_options=geometry
    )
    # custom enviromnments (must be first)
    Report = Environments.AddEnv(Report, "PackedItem")
    Report = Environments.AddEnv(Report, "FootnoteSize")
    Report = Environments.AddEnv(Report, "ScriptSize")
    Report = Environments.AddEnv(Report, "TinySize")
    Report = Environments.AddEnv(Report, "MultiCol")
    Report = Commands.AddCommand(Report, "AddToToc")
    Report = Commands.AddCommand(Report, "UrlRef")
    Report = Commands.AddCommand(Report, "MakeCell")

    # headers & footers (default BR : code versions)
    Report = Document.AddUZAHeader(Report, address=True)
    Report = Document.AddToFooter(Report, "L", "{}\n{}".format(sample_name, datetime.date.today()))

    # new commands/items
    Report.preamble.append(pu.NoEscape(r"\renewcommand{\contentsname}{}"))
    Report.preamble.append(
        pl.UnsafeCommand(
            "newcommand", r"\head", options=1, extra_arguments=r"\textcolor{white}{\textbf{#1}}"
        )
    )
    Report.preamble.append(pu.NoEscape(r"\renewcommand{\familydefault}{\sfdefault}"))
    # needed packages (eg used in tmp_doc, preventing auto-load)
    Report.packages.append(pp.Package("tabularx"))
    Report.packages.append(pp.Package("amssymb"))

    # add colors
    Report.packages.append(pp.Package("color"))
    Report.preamble.append(pl.Command("definecolor", arguments=["grey", "RGB", "160,160,160"]))
    Report.preamble.append(pl.Command("definecolor", arguments=["darkgrey", "RGB", "100,100,100"]))
    Report.preamble.append(pl.Command("definecolor", arguments=["lightgrey", "RGB", "200,200,200"]))
    Report.preamble.append(pl.Command("definecolor", arguments=["red", "RGB", "255,0,0"]))
    Report.preamble.append(pl.Command("definecolor", arguments=["white", "RGB", "255,255,255"]))
    Report.preamble.append(pl.Command("definecolor", arguments=["orange", "RGB", "238,118,0"]))

    # main title & TOC
    with Report.create(pl.Section("Report for {}".format(sample_name), numbering=False)):
        Report.append(pl.FootnoteText(pu.NoEscape(r"\tableofcontents")))

    # ready
    return Report


def GetFormattingDetails():
    # from python 3.6+, dicts are ordered by default.
    details = dict()
    # chromosome mapping:
    details["chr_map"] = {x: str(x) for x in range(1, 23)}
    details["chr_map"].update({23: "X", 24: "Y", 25: "MT"})

    # non-subtable annotations order:
    #   - classification
    #   - validation
    #   - quality
    #   - frequency
    #   - patho-predictions
    details["general"] = {
        "Classification": dict.fromkeys(
            [
                "^diagnostic_class",
                "^autoclassified",
                "^inheritance_mode",
                "^validation",
            ]
        ),
        "Quality": dict.fromkeys(
            [
                "^Tranches_Filter",
                "^AllelicRatio",
                "Quality",
                "Stretch",
            ]
        ),
        "Variant.Frequency": dict.fromkeys(["gAD", "Occurence", "snp1", "exac"]),
    }
    # items to skip
    details["skip"] = set(
        [
            "chr",
            "position",
            "ref_allele",
            "alt_allele",
            "Alt_Allele_Depth",
            "Ref_Allele_Depth",
            "Genotype",
            "estimated_inheritance",
            "set_inheritance",
            "validation_details",
            "variant_id",
            "location",
            "parents",
            "sid",
        ]
    )
    details["replace"] = {
        "diagnostic_class": "Diag.Class",
        "autoclassified": "autoclass'd",
        "inheritance_mode": "Inh.Mode",
        "validation": "validated",
        "Tranches_Filter": "Filter",
        "AllelicRatio": "All.Ratio",
        "Mapping_Quality": "Map.Qual",
        "Quality_By_Depth": "Qual.by.Depth",
    }

    details["replace_regex"] = {
        "_Rank_Sum": [r"(.).*_(.).*_Rank_Sum", r"\1\2_RSum"],
        "gAD": [r"_", r"."],
        "Occurence_": [r"^Occurence_", r"Occ."],
        "All_Samples": [r"All_Samples_", r"Inhouse."],
        "Alternate": [r"Alternate", r"Alt."],
        "RefSeq": [r"^RefSeq_", r""],
        "SNV_": [r"^SNV_", r""],
    }
    details["value_regex"] = {
        "VariantType": [r" SNV$", r""],
    }
    # skip if condition is met.
    details["conditional_skip"] = {
        # logic is : if in stretch, show the unit. else, show "strecht = no"
        "StretchUnit": "",
        "InStretch": "yes",
        "validation": "-",
    }
    # subtable column orders:
    details["subtable_order"] = {
        "RefSeq": [
            "RefSeq_Symbol",
            "RefSeq_Transcript",
            "RefSeq_GeneLocation",
            "RefSeq_Exon",
            "RefSeq_VariantType",
            "RefSeq_cPointNT",
            "RefSeq_cPointAA",
        ]
    }
    # specific columns in subtables that should be split at ~ provided nr of characters into newlines
    details["splitCells"] = {
        "ClinVar/Entry": {"SNV_Disease": 35, "SNV_Class_Comment": 25, "SNV_Class": 25},
        "GenePanels": {"Panel_Gene_Comment": 75},
    }
    return details


def FormatSection(report, sectionKey, section_details, seen_variants):
    try:
        with open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/", sectionKey, "results"
            ),
            "r",
        ) as fh:
            section_data = json.load(fh)
    except Exception as e:
        raise Exception("Could not load json data for section {}: {}".format(sectionKey, e))

    # title & TOC
    with report.create(pl.Subsection(section_details["Name"], numbering=False)):
        report.append(
            Commands.AddToToc(
                arguments=pb.Arguments(
                    "subsection",
                    section_details["Name"],
                    f"Applied Filter : {section_details['FilterName']} | Description: {section_details['Description']}",
                )
            )
        )
        # check variants if any were seen in previous sections.
        # reverse processing to not disturb positions.
        skipped_vars = 0
        for i in range(len(section_data["Variants"]) - 1, -1, -1):
            if section_data["Variants"][i]["location"] in seen_variants:
                skipped_vars += 1
            else:
                seen_variants.add(section_data["Variants"][i]["location"])
        # comments
        comments = [c for c in section_data["Comments"] if "Runtime" not in c]
        if skipped_vars:
            comments.append(
                "NOTICE: {} variant{} listed in a previous section".format(
                    skipped_vars, "s were" if skipped_vars > 1 else " was"
                )
            )
        if len(comments) > 0:
            report.append(pl.UnsafeCommand("vspace", "-0.2cm"))
            with report.create(Environments.TinySize()) as ts:
                with ts.create(Environments.PackedItem()) as pi:
                    for c in comments:
                        pi.add_item(c)
            report.append(pl.UnsafeCommand("vspace", "-0.4cm"))
        # no variants : end here
        if len(section_data["Variants"]) == 0:
            with report.create(Environments.ScriptSize()) as scriptsize:
                # todo : scriptsize
                scriptsize.append(pu.italic("No Variants retained"))
            return report, seen_variants
        #
        with report.create(Environments.FootnoteSize()) as fs:
            with fs.create(
                pl.LongTable(
                    r"p{0.12\textwidth} p{0.25\textwidth} p{0.17\textwidth} p{0.12\textwidth} p{0.2\textwidth}"
                )
            ) as data_table:
                # header
                data_table.add_hline()
                data_table.add_row(
                    [
                        pu.NoEscape(r"\hspace{-0.2cm}\head{Position}"),
                        pu.NoEscape(r"\head{Allele (Ref/Alt)}"),
                        pu.NoEscape(r"\head{Depth (Ref/Alt)}"),
                        pu.NoEscape(r"\head{Genotype}"),
                        pu.NoEscape(r"\head{Inheritance (Set/Est)}"),
                    ],
                    color="darkgrey",
                    mapper=[pu.bold],
                )
                data_table.add_hline()
                data_table.end_table_header()
                # footer if split
                data_table.add_hline()
                data_table.add_row(
                    (pl.MultiColumn(5, align="r", data=pu.italic("Continued on next page")),)
                )
                data_table.add_hline()
                data_table.end_table_footer()
                # last footer
                data_table.add_hline()
                data_table.end_table_last_footer()

                # contents
                for variant in section_data["Variants"]:
                    # re-initiate track handled annotations / variant.
                    global handled_fields
                    handled_fields = set()
                    row = [
                        pu.NoEscape(
                            r"\hspace{-0.2cm}"
                            + r"chr{}:{}".format(variant["chr"], f'{variant["position"]:,}')
                        ),
                        "{}/{}".format(variant["ref_allele"], variant["alt_allele"]),
                        "{}/{}".format(variant["Ref_Allele_Depth"], variant["Alt_Allele_Depth"]),
                        variant["Genotype"],
                        "{}/{}".format(
                            variant["set_inheritance"], variant["estimated_inheritance"]
                        ),
                    ]
                    # data_table.append(pu.NoEscape(r"\hspace{-0.2cm}"))
                    data_table.add_row(row, color="lightgrey", stick=True)
                    # MAIN
                    data_table.add_row(
                        (
                            pl.MultiColumn(
                                5,
                                align=pu.NoEscape(r"@{\hspace{0.01\textwidth}} p{0.95\textwidth}"),
                                # data must be tex objects, if multiple: a list is provided.
                                data=[
                                    pl.position.VerticalSpace(pu.NoEscape(r"-0.6cm")),
                                    ConstructMainAnnotations(variant),
                                ],
                            ),
                        ),
                        stick=True,
                    )
                    # SUBTABLES
                    for key, rows in variant.items():
                        # tables are lists of dicts
                        if not isinstance(variant[key], list):
                            continue
                        # double check
                        if key in handled_fields:
                            continue
                        handled_fields.add(key)
                        data_table.add_row(
                            (
                                pl.MultiColumn(
                                    5,
                                    align=pu.NoEscape(
                                        r"@{\hspace{0.01\textwidth}} p{0.95\textwidth}"
                                    ),
                                    # data must be tex objects, if multiple: a list is provided.
                                    data=[
                                        pl.position.VerticalSpace(pu.NoEscape(r"0.1cm")),
                                        ConstructSubtable(key, variant[key]),
                                    ],
                                ),
                            ),
                            stick=True,
                        )
                    # CHECKBOXES.
                    if section_details["checkboxes"]:
                        data_table.add_empty_row(stick=True)
                        data_table.add_row(
                            (
                                pl.MultiColumn(
                                    5,
                                    align=pu.NoEscape(
                                        r"@{\hspace{0.02\textwidth}} p{0.94\textwidth}"
                                    ),
                                    data=[
                                        ConstructCheckboxes(section_details),
                                    ],
                                ),
                            ),
                            stick=True,
                        )
                    # extra space ?
                    if section_details["vspace"]:
                        for i in range(0, section_details["vspace"]):
                            data_table.add_empty_row(stick=True)
                    # finaly empty, non sticking row.
                    data_table.add_empty_row()

    # return.
    return report, seen_variants


def ConstructCheckboxes(data):
    tmp_doc = pl.Document()
    # get lognest box
    longest_box = ""
    for box_key, boxes in data["cbx"].items():
        longest_box = max([box_key, longest_box, max(boxes, key=len)], key=len)
    # max 185 chars per line.
    #  the [] box is 2 chars; 0 chars space between items.
    #    => lower(120 / (6+maxlen) ) is # cols
    nr_cb_cols = int((185 / (2 + len(longest_box)))) - 1
    log.debug(f"Longest field : {longest_box}")
    log.debug(f"nr.cols : {nr_cb_cols}")
    # table with label in col 1, boxes in 4 cols.
    fmt = "r" + r"@{\extracolsep{\fill} }" + "l" * nr_cb_cols
    log.debug(f"fmt: {fmt}")
    with tmp_doc.create(Environments.ScriptSize(arguments=pb.Arguments())) as ss:
        with ss.create(pl.Tabularx(fmt, width_argument=pu.NoEscape(r"\textwidth"))) as t:
            # t.append(pu.NoEscape(r"\tiny"))
            # test box
            # row = [
            #    pu.bold(pu.italic(pu.NoEscape(
            #            r"\underline{\smash{" + pu.escape_latex('test') + r"}}")))
            # ] + ['0123456789'*12] + [""]*3
            # t.add_row(row)
            for box_key, boxes in data["cbx"].items():
                nr_boxes = len(boxes)
                # do not print empty lists
                if not nr_boxes:
                    continue
                boxes = [pu.NoEscape(r"$\square${}".format(pu.escape_latex(x))) for x in boxes]
                # make sure there are trailing empty fields
                if len(boxes) % nr_cb_cols:
                    boxes.extend([""] * nr_cb_cols)

                # first row has label
                row = [
                    pu.bold(
                        pu.italic(
                            pu.NoEscape(r"\underline{\smash{" + pu.escape_latex(box_key) + r"}}")
                        )
                    )
                ] + boxes[0:nr_cb_cols]
                t.add_row(row)
                # then continue.
                for i in range(nr_cb_cols, nr_boxes, nr_cb_cols):
                    row = [""] + boxes[i : (i + nr_cb_cols)]
                    t.add_row(row)

    return ss
    # return tmp_doc.create(Environments.ScriptSize(arguments=pb.Arguments(t)))


def ConstructMainAnnotations(variant):
    # use a tmp_doc to construct content, then copy to main doc.
    tmp_doc = pl.Document()
    # track handled annotations
    global handled_fields
    # get some details for formatting & column order
    fd = GetFormattingDetails()
    # general annotations
    with tmp_doc.create(Environments.MultiCol(arguments=pb.Arguments(4))) as mc:
        mc.append(pu.NoEscape(r"\raggedright\scriptsize"))
        # general annotations per theme.
        for key, subkeys in fd["general"].items():
            # mc.append(pu.NoEscape(r"\vspace{-0.01cm}"))
            # with mc.create(pl.MiniPage(width=r"\columnwidth", pos="t")) as mp:
            mc.append(pu.bold(key))
            mc.append(pl.LineBreak())
            for sk in subkeys:
                # match using regexes
                for ssk in [x for x in re.scanlist(sk, variant.keys())]:
                    handled_fields.add(ssk)
                    if ssk in fd["skip"]:
                        continue
                    if ssk in fd["conditional_skip"] and str(fd["conditional_skip"][ssk]) == str(
                        variant[ssk]
                    ):
                        continue
                    # shorter name?
                    ssk_txt = ssk
                    if ssk in fd["replace"]:
                        ssk_txt = fd["replace"][ssk]
                    # regex correct name
                    for rk, rv in fd["replace_regex"].items():
                        if re.search(rk, ssk):
                            ssk_txt = re.sub(rv[0], rv[1], ssk_txt)
                    try:
                        mc.append(
                            "{}: {}".format(
                                ssk_txt.replace("_", "."),
                                FormatValue(variant[ssk], ssk),
                            )
                        )
                    except Exception as e:
                        log.info(ssk)
                        log.info(variant[ssk])
                    mc.append(pl.LineBreak())
        # other non-table annotations:
        title = None
        for key, val in variant.items():
            if key in handled_fields:
                continue
            # subtable
            if isinstance(val, list):
                continue
            handled_fields.add(key)
            # skip ?
            if key in fd["skip"]:
                continue
            if key in fd["conditional_skip"] and str(fd["conditional_skip"][key]) == str(
                variant[key]
            ):
                continue
            if not title:
                title = "Various"
                mc.append(pu.bold(title))
                mc.append(pl.LineBreak())
            # shorter name?
            key_txt = key
            if key in fd["replace"]:
                key_txt = fd["replace"][key]
            # regex correct name
            for rk, rv in fd["replace_regex"].items():
                if re.search(rk, key):
                    key_txt = re.sub(rv[0], rv[1], key_txt)
            mc.append(
                "{}: {}".format(
                    key_txt.replace("_", "."),
                    FormatValue(variant[key], key),
                )
            )
            mc.append(pl.LineBreak())
        return mc


def ConstructSubtable(key, data):
    log.debug("Formatting subtable {}".format(key))
    # use a tmp_doc to construct content, then copy to main doc.
    tmp_doc = pl.Document()
    # track handled annotations
    global handled_fields
    # get some details for formatting & column order
    fd = GetFormattingDetails()
    nr_cols = len(data[0].keys())
    # get titles, optionally ordered by lab-asked order.
    titles = OrderTitles(key, list(data[0].keys()))

    full_titles = titles.copy()  # list(data[0].keys())
    # shorter name?
    for i in range(0, nr_cols):
        title = titles[i]
        if title in fd["replace"]:
            titles[i] = fd["replace"][title]
        # regex correct name
        for rk, rv in fd["replace_regex"].items():
            if re.search(rk, title):
                titles[i] = re.sub(rv[0], rv[1], titles[i])
    fmt = r"@{\extracolsep{\fill} }" + " l" * nr_cols
    with tmp_doc.create(pl.Tabularx(fmt, width_argument=pu.NoEscape(r"0.96\textwidth"))) as t:
        t.append(pu.NoEscape(r"\scriptsize"))
        t.add_row([key] + [" "] * (nr_cols - 1), mapper=[pu.bold])
        # t.add_hline()
        t.add_row(titles, mapper=[pu.italic])

        # get unique rows
        d = [dict(y) for y in set(tuple(x.items()) for x in data)]

        for row in d:
            # replace empty values
            row = {k: "." if not v else v for k, v in row.items()}
            # need to work on the values ?
            for i in range(0, nr_cols):
                # safety : remove tabs & newlines.
                row[full_titles[i]] = str(row[full_titles[i]]).replace("\n", " ").replace("\t", " ")

                # simple regexes
                if titles[i] in fd["value_regex"]:
                    row[full_titles[i]] = re.sub(
                        fd["value_regex"][titles[i]][0],
                        fd["value_regex"][titles[i]][1],
                        row[full_titles[i]],
                    )
                # more elaborate : decapitalize some items.
                if titles[i] in ["Effect"]:
                    row[full_titles[i]] = row[full_titles[i]].lower()
                # more elaborate : rewrite hyperrefs
                if titles[i] in ["Link"]:
                    # extract if needed ?
                    if re.search(r"href='(htt.*)'.*>(.*)</a>", row[full_titles[i]]):
                        row[full_titles[i]] = Commands.UrlRef(
                            arguments=pb.Arguments(re.last_match.group(1), re.last_match.group(2))
                        )
                if titles[i] in ["PubMed"]:
                    row[full_titles[i]] = Commands.UrlRef(
                        arguments=pb.Arguments(
                            "https://pubmed.ncbi.nlm.nih.gov/?term={}".format(row[full_titles[i]]),
                            "link",
                        )
                    )
                # more elaborate : need to split long text ?
                if key in fd["splitCells"] and full_titles[i] in fd["splitCells"][key]:
                    log.debug(f"Splitting cell : {key} : {full_titles[i]} !")
                    # reformat contents & explicitly escape
                    # this is needed to allow NoEscape at row level & keep \\ in makecell
                    words = row[full_titles[i]].split(" ")
                    new_contents = ""
                    current_length = 0
                    for word in words:
                        new_contents += pu.escape_latex(word) + " "
                        current_length += len(word)
                        if current_length > fd["splitCells"][key][full_titles[i]]:
                            new_contents += pu.NoEscape(r" \\\hspace{0.1cm} ")
                            current_length = 0

                    # it's an explicit escape/noescape combo to keep the \\ in the output.
                    row[full_titles[i]] = Commands.MakeCell(
                        arguments=pb.Arguments(
                            "tl", pu.NoEscape(re.sub(r" \\\\\\hspace\{0.1cm\} $", "", new_contents))
                        )
                    )

                # more elaborate : set mane annotation if present

            # sort according to titles list.
            row = {k: row[k] for k in full_titles}
            t.add_row(row.values())
        return t


def OrderTitles(key, titles):
    fd = GetFormattingDetails()
    # no order specified.
    if key not in fd["subtable_order"]:
        log.debug("not in order table")
        return titles
    # order list depending on what fields are present
    new_order = list()
    for t in fd["subtable_order"][key]:
        if t in titles:
            # add to new list
            new_order.append(t)
            # remove
            titles.remove(t)
    # add remainder of titles.
    new_order.extend(titles)
    return new_order


def WritePID(name, type="w"):
    pid = str(os.getpid())
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs", "{}.pid".format(name)), type
    ) as fh:
        fh.write(pid)


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # write out the pid.
    try:
        WritePID("VariantDB_Report_Runner")
    except Exception as e:
        print("Unable to set pid. Exiting : {}".format(e))
        sys.exit(1)

    # start logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_Report_Runner",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        quiet=True,
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)

    # connections
    vdb = Connection(
        url="https://{}/{}/api/".format(
            config["LOCATIONS"]["DOMAIN"], config["LOCATIONS"]["WEBPATH"]
        )
    )

    if "report_id" in args and args.report_id:
        # re-enable stdout in logger
        setup_logging(
            name="VariantDB_Report_Runner",
            level=config["LOGGING"]["LOG_LEVEL"],
            log_dir=config["LOGGING"]["LOG_PATH"],
            to_addrs=config["LOGGING"]["LOG_EMAIL"],
            quiet=False,
            permissions=777,
        )
        log = get_logger("main")
        log.info("Disabled Quiet mode")
        # format the report.
        log.info("Running PDF generation only for report {}".format(args.report_id))
        dbh = GetDBConnection()
        if not dbh:
            # this is interactive.
            sys.exit(1)

        # format.
        try:
            FormatReport(args.report_id)
        except Exception as e:
            log.error("Could not format Report for key {} : {}".format(args.report_id, e))
            sys.exit(1)

        sys.exit(0)

    # track running reports
    running_reports = dict()
    # endless loop.
    while True:
        # fresh db connection on every iteration.
        dbh = GetDBConnection()
        if not dbh:
            log.warning("DB not available. wait & retry.")
            time.sleep(60)
            continue
        log.debug("iterating")
        # check running reports.
        running_reports = CheckReportStatus(running_reports, config)

        # Get queue item.
        try:
            queryKey = GetQueueItem(config)
        except Exception as e:
            log.error("Failed to process Report Queue : {}".format(e))
            sys.exit(1)
        if not queryKey:
            log.debug("queue is empty.")
            dbh.cnx.close()
            time.sleep(15)
            continue
        log.info("Working on item {}".format(queryKey))

        # Process Item
        try:
            report_details, query_ids = ProcessItem(config, queryKey)
            running_reports[queryKey] = {"details": report_details, "query_ids": query_ids}
        except Exception as e:
            SetStatus(queryKey, "Failed")
            log.error("Could not process queue item {} : {}".format(queryKey, e))
        dbh.cnx.close()
