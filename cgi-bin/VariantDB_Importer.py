#!/usr/bin/env python
from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger, add_filestream
from CMG.Utils import Re

import configparser
import argparse
import os
import sys

import subprocess
import json
import time
import datetime
import urllib
import vcf as Vcf
from multiprocessing import Pool


# error classes
class DataError(OSError):
    pass


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    # set path :
    if "PATH" in config["PATH"]:
        config["PATH"]["PATH"] = "{}:{}".format(config["PATH"]["PATH"], os.environ["PATH"])
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    if "CONDA_ENV" in config["PATH"]:
        config["PATH"]["PATH"] = "{}/bin:{}".format(
            config["PATH"]["CONDA_ENV"], config["PATH"]["PATH"]
        )
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    # command line arguments
    help_text = """
        GOAL:
        #####
            - Import VCF files into Database
            
        """
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # parser.add_argument('-s', '--source', help='Calling origin. [API] or UI', default='API')
    parser.add_argument("-d", "--datadir", help="Path to Prepared input folder. Can be read-only")
    # parser.add_argument('-n', '--nrs', help="Number of samples to import")
    parser.add_argument("-u", "--userid", help="ID of the user to import samples for.")
    parser.add_argument("-q", "--queryKey", help="ID of import job")
    args = parser.parse_args()

    return config, args


def GetDBConnection(quiet=True):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)

    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)
    if not quiet:
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    return dbh


def Fail(msg):
    log.error(msg)
    print("Content-type: application/json\r\n")
    if "sudo" in msg:
        msg = "Error messsage masked"
    print(json.dumps({"ERROR": msg, "status": "error"}))
    sys.exit(1)


def SetStatus(status, msg=None):
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "api/import_jobs/{}".format(args.queryKey),
            "status",
        ),
        "w",
    ) as fh:
        log.info(f"Setting Status to : {status}")
        fh.write(status)
    if msg:
        with open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "api/import_jobs/{}".format(args.queryKey),
                "msg",
            ),
            "w",
        ) as fh:
            fh.write(str(msg))
        log.error(msg)


def CheckPlatform():
    vdb_status = dbh.runQuery("SELECT `status` FROM `NGS-Variants-Admin`.`SiteStatus`")[0]["status"]
    while not vdb_status == "Operative":
        log.warning(
            "VariantDB is in construction mode. Import will start when VariantDB is active again."
        )
        time.sleep(60)
        vdb_status = dbh.runQuery("SELECT `status` FROM `NGS-Variants-Admin`.`SiteStatus`")[0][
            "status"
        ]
    return True


def GetArguments():
    data = {}
    # sample names
    data["names"] = {}
    with open(os.path.join(args.datadir, "names.txt"), "r") as fh:
        for line in fh:
            k, v = line.rstrip().split("==")
            data["names"][k] = v
    # genders
    data["genders"] = {}
    with open(os.path.join(args.datadir, "genders.txt"), "r") as fh:
        for line in fh:
            k, v = line.rstrip().split("==")
            data["genders"][k] = v
    # formats
    data["formats"] = {}
    with open(os.path.join(args.datadir, "formats.txt"), "r") as fh:
        for line in fh:
            k, v = line.rstrip().split("==")
            data["formats"][k] = v
    # vcf files
    data["vcfs"] = {}
    with open(os.path.join(args.datadir, "vcfs.txt"), "r") as fh:
        for line in fh:
            k, v = line.rstrip().split("==")
            data["vcfs"][k] = v
    # vcf files
    data["bams"] = {}
    if os.path.isfile(os.path.join(args.datadir, "bams.txt")):
        with open(os.path.join(args.datadir, "bams.txt"), "r") as fh:
            for line in fh:
                k, v = line.rstrip().split("==")
                data["bams"][k] = v

    # custom fields
    cf = {}
    if os.path.isfile(os.path.join(args.datadir, "cf.txt")):
        with open(os.path.join(args.datadir, "cf.txt"), "r") as fh:
            for line in fh:
                k, v = line.rstrip().split("=")
                k1, k2 = re.findall(r"(.*)_(\d+)", k)[0]

                cf[k2][k1] = v
        # get codes.
        data["cf"] = {}
        cf_types = {}
        for k in cf.keys():
            if cf[k]["field"] not in data["cf"]:
                data["cf"][cf[k]["field"]] = {}
            data["cf"][cf[k]["field"]]["type"] = cf[k]["type"]
            cf_types[cf[k]["type"]] = 1
            # code
            codes = dbh.runQuery(
                "SELECT aid FROM `Custom_Annotations` WHERE `value_type` = %s AND `field_name` = %s",
                (cf[k]["type"], cf[k]["field"]),
            )
            if not codes:
                code = dbh.insertQuery(
                    "INSERT INTO `Custom_Annotations` (`value_type`,`field_name`) VALUES (%s, %s)",
                    (cf[k]["type"], cf[k]["field"]),
                )
            else:
                code = codes[0]["aid"]
            data["cf"][cf[k]["field"]]["id"] = code

    # lockdb ?
    if os.path.isfile(os.path.join(args.datadir, "lockdbs.txt")):
        data["lockdbs"] = True
    else:
        data["lockdbs"] = False

    # project name?
    if os.path.isfile(os.path.join(args.datadir, "project.name.txt")):
        with open(os.path.join(args.datadir, "project.name.txt"), "r") as fh:
            data["projectname"] = fh.readline().rstrip()
    else:
        data["projectname"] = time.strftime("%Y.%m.%d_%H:%M:%S")

    return data


def GetProjectID(projectname):
    rows = dbh.runQuery(
        "SELECT p.id FROM `Projects` p JOIN `Projects_x_Users` pu ON p.id = pu.pid WHERE pu.uid = %s AND pu.editsample = 1 AND p.Name = %s",
        (args.userid, projectname),
    )
    if not rows:
        log.info("Creating project : {}".format(projectname))
        pid = dbh.insertQuery(
            "INSERT INTO `Projects` (`Name`, `userID`, `collection`, `SummaryStatus`) VALUES (%s,%s,'none',1)",
            (projectname, args.userid),
        )
        dbh.doQuery(
            "INSERT INTO `Projects_x_Users` (pid, uid, editvariant, editclinic, editsample) VALUES (%s,%s,'1','1','1')",
            (pid, args.userid),
        )
        # novel project, set summary status just for owner
        dbh.doQuery("UPDATE `Users` SET `SummaryStatus` = 1 WHERE id = %s", args.userid)

    else:
        log.info("Adding data to existing project : {}".format(projectname))
        pid = rows[0]["id"]
        dbh.doQuery("UPDATE `Projects` SET `SummaryStatus` = 1 WHERE id = %s", pid)
        # set summary status for all users with access.
        dbh.doQuery(
            "UPDATE `Users` SET `SummaryStatus` = 1 WHERE id IN (SELECT uid FROM `Projects_x_Users` WHERE pid = %s)",
            pid,
        )
    return pid


def GetClassifiers():
    results = {}
    rows = dbh.runQuery(
        "SELECT cv.vid,v.chr, v.start, v.RefAllele, v.AltAllele, c.Name, c.id, cv.genotype, cv.class, cv.inheritance_mode, cv.comments FROM `Variants` v JOIN `Classifiers` c JOIN `Classifiers_x_Variants` cv JOIN `Users_x_Classifiers` uc ON v.id = cv.vid AND c.id = uc.cid AND c.id = cv.cid WHERE uc.uid = %s AND uc.apply_on_import = 1 AND cv.validate_reject = 1",
        args.userid,
    )

    for row in rows:
        k = "{}--{}--{}--{}".format(row["chr"], row["start"], row["RefAllele"], row["AltAllele"])
        for gt in row["genotype"].split(","):
            # already present in a more pathogenic class:
            if (
                k in results
                and gt in results[k]
                and results[k][gt]["dc"] <= row["class"]
                and results[k][gt]["dc"] != 0
                and results[k][gt]["dc"] != ""
            ):
                continue
            # store new.
            if k not in results:
                results[k] = {}
            log.debug("Adding AC variant {} : {}".format(k, gt))
            results[k][gt] = {
                "cn": row["Name"],
                "cid": row["id"],
                "dc": row["class"],
                "im": row["inheritance_mode"],
                "cm": row["comments"].rstrip().replace("\n", ";").replace("\t", " "),
            }
    return results


def GetFile(source, target):
    # target dir exists ?
    os.makedirs(os.path.dirname(target), exist_ok=True)
    # regular file / symlink
    if os.path.exists(target):
        log.warning("Target exists, skipping : {}".format(target))
    elif os.path.isfile(source):
        # mk link.
        os.symlink(os.path.abspath(source), target)
    elif source.startswith("http"):
        try:
            site = urllib.request.urlopen(source)
            header = site.info()
            dummy = header["content-type"]
        except Exception as e:
            msg = "File not downloadable: {} : {}".format(source, e)
            SetStatus("error", msg)
            sys.exit(1)
        urllib.request.urlretrieve(source, target)
    elif source.startswith("s3://"):
        log.info("not implemented yet...")


def CreateSample(idx, data):
    samplenames = list()
    # no name given:
    if not data["names"]["name{}".format(idx)]:
        # make "unnamed_"
        r = dbh.runQuery(
            "SELECT Name FROM `Samples` WHERE Name LIKE 'Unnamed\_Sample\_%' ORDER BY id DESC LIMIT 1"
        )
        if not r:
            samplename = "Unnamed_Sample_1"
        else:
            last = re.findall(r"Unnamed_Sample_(\d+)", r[0]["name"])[0]
            samplename = "Unnamed_Sample_{}".format(int(last) + 1)
        samplenames.append(samplename)
    # cohort
    elif data["names"]["name{}".format(idx)].lower() == "%cohort%":
        # take samplesnames from column headers.
        with vcf.Reader(open(os.path.join(args.tmpdir, "data{}.vcf".format(idx)), "r")) as vcf:
            samplenames.extend(vcf.samples)
    # plain sample.
    else:
        samplenames.append(data["names"]["name{}".format(idx)])

    # insert
    data["vdb_ids"][idx] = list()
    data["vdb_names"][idx] = list()
    id_map = open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "api/import_jobs/{}".format(args.queryKey),
            "id_map.txt",
        ),
        "a",
    )
    for sample in samplenames:
        # check for double imports on same date : append nr if so.
        full_sample = "{}_{}".format(sample, args.date)
        r = dbh.runQuery("SELECT id FROM `Samples` WHERE Name = %s", full_sample)
        i_idx = 1
        while r:
            i_idx += 1
            full_sample = "{}_{}_import.{}".format(sample, args.date, i_idx)
            r = dbh.runQuery("SELECT id FROM `Samples` WHERE Name = %s", full_sample)
        # gender
        if data["genders"]["gender{}".format(idx)].lower() in ["male", "female"]:
            gender = data["genders"]["gender{}".format(idx)].capitalize()
        else:
            gender = "Unknown"

        sid = dbh.insertQuery(
            "INSERT INTO `Samples` (Name, gender, AnnotationStatus) VALUES (%s, %s, 'importing')",
            (full_sample, gender),
        )
        id_map.write("{}={}\n".format(sid, full_sample))
        data["vdb_ids"][idx].append(sid)
        data["vdb_names"][idx].append(full_sample)
        log.info("Created sample {} : {}".format(sid, full_sample))
    # close sample map.
    id_map.close()
    # return.
    return data


def ProcessData():
    # Only work on active platform status.
    CheckPlatform()

    # Read data from WD:
    log.debug("Loading settings from datadir")
    data = GetArguments()

    # mk folders in scratch
    # for subdir in ["split/done", "db_import/done"]:
    #    os.makedirs(os.path.join(args.tmpdir, subdir), exist_ok=True)

    # get project id
    pid = GetProjectID(data["projectname"])
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "api/import_jobs/{}".format(args.queryKey), "pid.txt"
        ),
        "w",
    ) as fh:
        fh.write(str(pid))

    # get active classifier variants
    data["cv"] = GetClassifiers()

    # thread pool for parsing
    parsers = Pool(4)

    # loop over available files.
    data["vdb_ids"] = {}
    data["vdb_names"] = {}
    idx = 1
    parser_results = []
    while "vcf{}".format(idx) in data["vcfs"]:
        # SKIP IF NOT IN SUPPORTED FORMATS
        if data["formats"]["format{}".format(idx)] not in ["HC"]:
            log.warning("VCF {} is not supported. Skipping!".format(idx))
            idx += 1
            continue

        # get vcf file
        source = data["vcfs"]["vcf{}".format(idx)]
        target = os.path.join(args.tmpdir, "data{}.vcf".format(idx))
        if source.endswith(".gz"):
            target += ".gz"
        log.info("Downloading {} to {}".format(source, target))
        GetFile(source, target)
        # get BAM file
        if data["bams"]["bam{}".format(idx)]:
            source = data["bams"]["bam{}".format(idx)]
            target = os.path.join(args.tmpdir, "data{}.bam".format(idx))
            log.info("Downloading {} to {}".format(source, target))
            GetFile(source, target)
        else:
            log.info("BAM FILE {} not specified.".format(idx))

        # Create sample(s) in database.
        data = CreateSample(idx, data)

        # TODO : STORE VCF/BAM FILES ?

        # Parse
        # loop over samples in this vcf, add to queue
        for sidx, sid in enumerate(data["vdb_ids"][idx]):
            # add sample to project.
            dbh.doQuery("INSERT INTO `Projects_x_Samples` (pid, sid) VALUES (%s,%s)", (pid, sid))
            # single arguments : sid | vcf-path | 0-based idx in this file | format | BAM
            vcf_path = os.path.join(args.tmpdir, "data{}.vcf".format(idx))
            if os.path.exists("{}.gz".format(vcf_path)):
                vcf_path += ".gz"
            entry = "{}|{}|{}|{}|{}".format(
                sid,
                vcf_path,
                sidx,
                data["formats"]["format{}".format(idx)],
                os.path.join(args.tmpdir, "data{}.bam".format(idx)),
            )
            log.info("Parsing SID == {}".format(sid))
            # all vcfs are parsed in parallel. then final DB loading in serial through the callback.
            r = parsers.apply_async(Parser, args=(entry,), kwds=data, callback=Loader)
            parser_results.append(r)
            # Parser(*entry)

        # next vcf
        idx = idx + 1

    # end threads
    log.info("Waiting for parser to finish")
    for r in parser_results:
        # result passes from Parser to Loader => parser_results.
        result = r.get()
        log.info("parser result : {}".format(result))
        if str(result).startswith("error"):
            # quit process!
            msg = result.split("|", 1)[1]
            log.error(msg)
            SetStatus("error", msg)

    # set summarystatus
    try:
        dbh.doQuery("UPDATE `Projects` SET SummaryStatus = 0 WHERE id = %s", pid)
        dbh.doQuery(
            "UPDATE `Users` SET `SummaryStatus` = 0 WHERE id IN (SELECT uid FROM `Projects_x_Users` WHERE pid = %s)",
            pid,
        )
    except Exception as e:
        log.error("Failed to set summarystatus: {}".format(e))
        SetStatus("error", f"Failed to set summary status: {e}")

    log.info("All Done")
    return data


def SetSampleStatus(data):
    for idx in data["vdb_ids"]:
        for sidx, sid in enumerate(data["vdb_ids"][idx]):
            dbh.doQuery("UPDATE `Samples` SET `AnnotationStatus` = 'Pending' WHERE id = %s", sid)


# this runs sequential in main thread. if problem => exit whole script.


def Loader(sid):
    if str(sid).startswith("error"):
        log.info("Thread Failed. Passing along error message.")
        SetStatus("error", sid)
        return
    # make the tmp table.
    query = "DROP TABLE IF EXISTS `Import_tmp_{}`".format(sid)
    try:
        dbh.doQuery(query)
    except Exception as e:
        msg = "Failed to drop tmp-table for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )

        SetStatus("error", msg)
        return
    query = "CREATE TABLE `Import_tmp_{}` ENGINE=MEMORY SELECT * FROM `Import_tmp_template` WHERE 0".format(
        sid
    )
    try:
        dbh.doQuery(query)
    except Exception as e:
        msg = "Failed to create tmp-table for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )

        SetStatus("error", msg)
        return
    log.debug("Created tmp table : Import_tmp_{}".format(sid))

    # load into tmp table, ignoring duplicates.
    query = "LOAD DATA LOCAL INFILE '{}' IGNORE INTO TABLE `Import_tmp_{}`".format(
        os.path.join(args.tmpdir, "load.{}.txt".format(sid)), sid
    )
    log.debug("Loading data : {}".format(query))
    try:
        dbh.doQuery(query)
        # os.remove(os.path.join(args.tmpdir, "load.{}.txt".format(sid)))
    except Exception as e:
        msg = "Failed to load tmp-table for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )

        SetStatus("error", msg)
        return

    log.info("Updating Variants Table for sid == {}".format(sid))
    # existing
    log.info("Fetching variant-ids for existing variants")
    query = "UPDATE `Import_tmp_{}` i LEFT JOIN `Variants` v ON i.chr = v.chr AND i.start = v.start AND i.RefAllele = v.RefAllele AND i.AltAllele = v.AltAllele SET i.id = v.id WHERE v.id IS NOT NULL".format(
        sid
    )
    try:
        dbh.doQuery(query)
    except Exception as e:
        msg = "Failed to set variant-ids for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )

        SetStatus("error", msg)
        return
    # insert novel
    log.info("Inserting novel variants into DB (ignoring duplicates key in case of racing)")
    query = "INSERT IGNORE INTO `Variants` (`start`, `stop`, `chr`, `RefAllele`, `AltAllele`, `Stretch`, `StretchUnit`) SELECT i.`start`, i.`stop`, i.`chr`, i.`RefAllele`, i.`AltAllele`, i.`Stretch`, i.`StretchUnit` FROM `Import_tmp_{}` i WHERE i.id = 0".format(
        sid
    )
    try:
        dbh.doQuery(query)
    except Exception as e:
        msg = "Failed to load novel variants for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )

        SetStatus("error", msg)
        return
    # add new variant-ids to tmp table.
    log.info("Applying variant-ids for novel variants into tmp_table.")
    query = "UPDATE `Import_tmp_{}` i INNER JOIN `Variants` v ON i.chr = v.chr AND i.start = v.start AND i.RefAllele = v.RefAllele AND i.AltAllele = v.AltAllele SET i.id = v.id WHERE i.id = 0".format(
        sid
    )
    try:
        dbh.doQuery(query)
    except Exception as e:
        msg = "Failed to load novel variants for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )
        SetStatus("error", msg)
        return
    # stretch info
    log.info("Updating Stretch info in Variants table")
    query = "UPDATE `Variants` v JOIN `Import_tmp_{}` i ON v.id = i.id SET v.`Stretch` = i.`Stretch`, v.`StretchUnit` = i.`StretchUnit` WHERE v.`Stretch` = 0 AND i.`Stretch` = 1".format(
        sid
    )
    try:
        dbh.doQuery(query)
    except Exception as e:
        msg = "Failed to update stretch info in variants for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )
        SetStatus("error", msg)
        return
    # drop the unneeded columns.
    log.info("Dropping variant columns.")
    try:
        # drop columns
        dbh.doQuery(
            "ALTER TABLE `Import_tmp_{}` DROP COLUMN `chr`, DROP COLUMN `start`,DROP COLUMN  `stop`,DROP COLUMN  `RefAllele`,DROP COLUMN  `AltAllele`,DROP COLUMN  `Stretch`,DROP COLUMN  `StretchUnit`".format(
                sid
            )
        )
        # drop duplicate vids
        dbh.doQuery(
            "CREATE TABLE `Import_tmp_{}_uniq` ENGINE=MEMORY SELECT * FROM `Import_tmp_{}` WHERE 0".format(
                sid, sid
            )
        )
        dbh.doQuery(
            "ALTER TABLE `Import_tmp_{}_uniq` ADD UNIQUE INDEX (id) USING BTREE".format(sid)
        )
        dbh.doQuery(
            "INSERT IGNORE INTO `Import_tmp_{}_uniq` SELECT * FROM `Import_tmp_{}`".format(sid, sid)
        )
        dbh.doQuery("DROP TABLE `Import_tmp_{}`".format(sid))
    except Exception as e:
        msg = "Failed to drop columns in tmp table for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )
        SetStatus("error", msg)
        return
    # add to variants_x_samples
    log.info("Loading variants into Variants_x_Samples")
    # ALTERNATIVE : TABLE `tmp` INTO OUTFILE 'load.$sid.txt' ; LOAD DATA INFILE ; cron on sql-server to clean up files ?
    try:
        # work in batches to prevent extensive table locking.
        while dbh.runQuery("SELECT id FROM  `Import_tmp_{}_uniq` LIMIT 1".format(sid)):
            # the i.id > 0 is to force use of the index, otherwise filesorting was used
            dbh.doQuery(
                "INSERT LOW_PRIORITY INTO `Variants_x_Samples` SELECT * FROM `Import_tmp_{}_uniq` i WHERE i.id > 0 ORDER BY id LIMIT 10000".format(
                    sid
                )
            )
            dbh.doQuery(
                "DELETE FROM `Import_tmp_{}_uniq` WHERE id > 0 ORDER BY id LIMIT 10000".format(sid)
            )
        # clean up.
        dbh.doQuery("DROP TABLE `Import_tmp_{}_uniq`".format(sid))

    except Exception as e:
        msg = "Failed to transfer variants to V_x_S for import key {}, sid {} : {}".format(
            args.queryKey, sid, e
        )
        SetStatus("error", msg)
        return
    # done.


# Parser :
def Parser(entry, **data):
    # arguments
    sid, vcffile, sidx, format, bamfile = entry.split("|")
    sid = int(sid)
    sidx = int(sidx)
    # local DB connection.
    # dbh = GetDBConnection()
    # load VCF with pyvcf
    try:
        log.info("Loading VCF file: {}".format(vcffile))
        vcf = Vcf.Reader(filename=vcffile)
        # vcf = Vcf.Reader(open(vcffile, "r"))
        log.info("found samples: {}".format(", ".join(vcf.samples)))
        log.info("Open output stream")
        fh = open(os.path.join(args.tmpdir, "load.{}.txt".format(sid)), "w")
    except Exception as e:
        log.error("Could not open filestreams for sample {} : {}".format(sid, e))
        return f"error|Could not open filestreams for sample {sid} : {e}"

    # TODO : how are gVCFs handled ?
    # parse VCF to tab-sep file for loading. large try loop to catch errors
    try:
        for record in vcf:
            # if not called in this sample (./.): skip
            if not record.genotype(vcf.samples[sidx]).called:
                continue
            # variant info : chr, pos, ref, alt, stretch, stretchunit,
            chr = ConvertChr(record.CHROM)
            if len(record.ALT) > 1:
                ma = 1
            else:
                ma = 0
            # pyvcf normalize the "altcount" for multiallelic as well : 0 == homref, 1 == het, 2 == hom.alt
            try:
                alt_count = record.genotype(vcf.samples[sidx]).gt_type
                if alt_count is None:
                    raise Exception(
                        "Invalid alt_count : {} : {} : {}".format(alt_count, record, record.samples)
                    )
            except Exception as e:
                log.error(e)
                return "error|{}".format(e)

            # log.info("REcord: {}:{} {}/{}".format(chr, record.POS, record.REF, record.ALT[0]))
            for a_idx, alt in enumerate(record.ALT):
                pos, ref, alt = NormalizeVariant(record.POS, record.REF, alt)
                stop = pos + len(ref) - 1
                # VARIANT : vid=0, chr, start, stop, ref, alt, stretch, stretch-unit
                out_line = "0\t{}\t{}\t{}\t{}\t{}".format(chr, pos, stop, ref, alt)
                if "RU" in record.INFO:
                    stretch = 1
                    stretch_unit = record.INFO["RU"]
                    # unit lengths based on GT (for MA variants)
                    gt_alleles = [
                        int(x) for x in record.genotype(vcf.samples[sidx])["GT"].split("/")
                    ]
                    stretch_A = record.INFO["RPA"][gt_alleles[0]]
                    stretch_B = record.INFO["RPA"][gt_alleles[1]]
                    # stretch_A, stretch_B = record.INFO["RPA"][0:2]
                else:
                    stretch = 0
                    stretch_unit = ""
                    stretch_A, stretch_B = [0, 0]
                out_line += "\t{}\t{}".format(stretch, stretch_unit)
                # general/preset info: sid, multi-allelic, inheritance, inheritance_mode, class, auto_classified, validation, validation_details
                var_key = "{}--{}--{}--{}".format(chr, pos, ref, alt)
                alt_count = str(alt_count)
                if var_key in data["cv"] and alt_count in data["cv"][var_key]:
                    log.debug("var {} found in classifiers".format(var_key))
                    ac = 1
                    im = data["cv"][var_key][alt_count]["im"]
                    dc = data["cv"][var_key][alt_count]["dc"]
                    comment = "Auto-Classified: {} : {}".format(
                        data["cv"][var_key][alt_count]["cn"], data["cv"][var_key][alt_count]["cm"]
                    )
                    comment = re.sub(r'[\'"\\]', "", comment, count=0)
                else:
                    log.debug("var {} not in classifiers".format(var_key))
                    ac = 0
                    im = 0
                    dc = 0
                    comment = ""
                out_line += "\t{}\t{}\t0\t{}\t{}\t{}\t-\t{}".format(sid, ma, im, dc, ac, comment)
                # FILTER is handled oddly by pyVCF:
                if type(record.FILTER) is list and len(record.FILTER) == 0:
                    # PASS is translated to empty list.
                    record.FILTER = ["PASS"]
                elif record.FILTER is None:
                    # no filter ( . ) is translated to None
                    record.FILTER = ["."]
                elif type(record.FILTER) is str:
                    # some examples online showed that filter can also be just a string...
                    record.FILTER = [record.FILTER]
                # QC info : QUAL(phredpoly) , filter, alt_count, ref_depth, alt_depth, GQ (phred_qual), MQ
                out_line += "\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(
                    record.QUAL,
                    ",".join(record.FILTER),
                    alt_count,
                    record.genotype(vcf.samples[sidx])["AD"][0]
                    if hasattr(record.genotype(vcf.samples[sidx]).data, "AD")
                    else 0,
                    record.genotype(vcf.samples[sidx])["AD"][a_idx + 1]
                    if hasattr(record.genotype(vcf.samples[sidx]).data, "AD")
                    else 0,
                    record.genotype(vcf.samples[sidx])["GQ"]
                    if hasattr(record.genotype(vcf.samples[sidx]).data, "GQ")
                    else 0,
                    record.INFO["MQ"] if "MQ" in record.INFO else 0,
                )
                # conditional info (sometimes missing): BQRS, MQRS, RPRS, SB, QbD, FS, VQSLOD, GTRatio, Ploidy, SomaticState,
                out_line += "\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t2\t1".format(
                    record.INFO["BaseQRankSum"] if "BaseQRankSum" in record.INFO else 0,
                    record.INFO["MQRankSum"] if "MQRankSum" in record.INFO else 0,
                    record.INFO["ReadPosRankSum"] if "ReadPosRankSum" in record.INFO else 0,
                    record.INFO["StrandBias"] if "StrandBias" in record.INFO else 0,
                    record.INFO["QD"] if "QD" in record.INFO else 0,
                    record.INFO["FS"] if "FS" in record.INFO else 0,
                    record.INFO["VQSLOD"] if "VQSLOD" in record.INFO else 0,
                    # GTRatio ~ fraction of alleles being non-ref ~ Expected Allelic Ratio.
                    int(alt_count) / 2,
                )
                # stretch : if hom => B == A
                # if alt_count == '2':
                #    stretch_A = stretch_B
                # GT data : dPL, RPA, RPB, RBQual, AltBaseQual, PhredFisherPValue
                out_line += "\t{}\t{}\t{}\t0\t0\t0".format(
                    sorted(record.genotype(vcf.samples[sidx])["PL"])[1],
                    stretch_A,
                    stretch_B,
                )
                out_line += "\n"
                fh.write(out_line)
    except Exception as e:
        log.error("VCF Parsing Error for importKey {}, sid {}: {}".format(args.queryKey, sid, e))
        return f"error|VCF Parsing Error for importKey {args.queryKey}, sid {sid}: {e}"

    # close file.
    fh.close()
    log.info("sample {} ready for tmp-table loading".format(sid))
    return sid


def ConvertChr(v):
    v = v.lstrip("chr")
    chrom_dict = {str(x): x for x in range(1, 23)}
    chrom_dict.update({"X": 23, 23: "X", "Y": 24, 24: "Y", "M": 25, "MT": 25, 25: "M"})
    return chrom_dict[v]


def NormalizeVariant(start, ref, alt):
    newref = str(ref).upper()
    newalt = str(alt).upper()
    newstart = start
    newend = newstart + len(newref) - 1

    # trim 5'
    while newref[0:1] == newalt[0:1] and len(newref) > 1 and len(newalt) > 1:
        newref = newref[1:]
        newalt = newalt[1:]
        newstart += 1

    # trim 3'
    while newref[-1] == newalt[-1] and len(newref) > 1 and len(newalt) > 1:
        newref = newref[:-1]
        newalt = newalt[:-1]
        newend -= 1

    # return
    return [newstart, newref, newalt]


def PostProcess():
    # set annotator status
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "update.txt"), "w"
    ) as fh:
        fh.write("1")
    # concat stdout/stderr
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "api/import_jobs/{}".format(args.queryKey),
            "Process.output.txt",
        ),
        "w",
    ) as fh:
        fh.write("##########\n")
        fh.write("# STDOUT #\n")
        fh.write("##########\n")
        with open(os.path.join(args.tmpdir, "stdout"), "r") as fi:
            for line in fi:
                fh.write(line)
        fh.write("\n")
        fh.write("##########\n")
        fh.write("# STDERR #\n")
        fh.write("##########\n")
        with open(os.path.join(args.tmpdir, "stderr"), "r") as fi:
            for line in fi:
                fh.write(line)


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
        # store tmpdir location
        args.tmpdir = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "api",
            "import_jobs",
            str(args.queryKey),
            "scratch",
        )
        # add today's date
        args.date = datetime.datetime.now().strftime("%Y-%m-%d")
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # start logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_Importer",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        quiet=True,
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)
    # python version
    which = subprocess.getoutput("which python")
    log.info("Runing python from : {}".format(which))
    # add log streams to working dir with reduced output in stdout, but default format in stderr
    fmt = "%(asctime)s {app} %(levelname)-8s - %(message)s."
    add_filestream(
        name="Importer",
        fmt=fmt,
        file=os.path.join(args.tmpdir, "stdout"),
        min_level="INFO",
        max_level="WARN",
    )
    add_filestream(name="Importer", file=os.path.join(args.tmpdir, "stderr"), min_level="ERROR")

    # start the cmg-regex module.
    re = Re()

    # todo : add the build
    dbh = GetDBConnection()

    try:
        # set status.
        SetStatus("Running")
        # Process
        data = ProcessData()
        # set sample statuses
        SetSampleStatus(data)
        # cp output from wd to queryID folder
        PostProcess()

        # set finished if not error'd before.
        with open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "api/import_jobs/{}".format(args.queryKey),
                "status",
            ),
            "r",
        ) as fh:
            status = fh.readline()
            if status.rstrip() == "error":
                log.warning("Process failed. keeping current status.")
            else:
                SetStatus("Finished")
        # exit
        sys.exit(0)
    except Exception as e:
        # log.error("Could not run Process wd {} : {}".format(datadir, e))
        SetStatus("error", e)
        Fail("Could not run Process wd {} : {}".format(args.datadir, e))
