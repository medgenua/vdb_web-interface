#!/usr/bin/env bash
set -euo pipefail

## export path from .credentials (default location)
if [ -f "../../.Credentials/.credentials" ] ; then
	# grep the path
	E=$(grep -P '^PATH=' ../../.Credentials/.credentials)
	if [ ! -z $E ]; then
		## prepend to path
		export $E:$PATH
	#else
	#	echo "grep is empty" 
	fi
    # grep CONDA ENV
    E=$(grep -P '^CONDA_ENV=' ../../.Credentials/.credentials)
	if [ ! -z $E ]; then
		## prepend to path
        E="${E:10}/bin"
        export PATH=$E:$PATH
	fi	

else 
	echo "Credentials not found."
    exit 1
fi
# variables
DIR=$1
SCRIPT=$2
LOG=$3
cd $DIR
echo "PATH: $PATH" >> $LOG 2>&1
echo "Starting $SCRIPT in $DIR. Logging to $LOG" >> $LOG 2>&1
echo "  Logging to $LOG" >> $LOG 2>&1
echo "" >> $LOG 2>&1
# test to check for dependencies etc.
python3 $SCRIPT -h > /dev/null 
# actual run.
python3 $SCRIPT >> $LOG 2>&1 &
echo $!
