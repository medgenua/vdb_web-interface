#!/usr/bin/env python
from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger

import configparser
import argparse
import os
import sys
import fcntl
import hashlib
import subprocess
import json
import time

# libraries for cgi (form handler)
import cgi

# error handler
import cgitb


# error classes
class DataError(OSError):
    pass


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")
    # set path :
    if "PATH" in config["PATH"]:
        config["PATH"]["PATH"] = "{}:{}".format(config["PATH"]["PATH"], os.environ["PATH"])
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    if "CONDA_ENV" in config["PATH"]:
        config["PATH"]["PATH"] = "{}/bin:{}".format(
            config["PATH"]["CONDA_ENV"], config["PATH"]["PATH"]
        )
    else:
        config["PATH"]["PATH"] = os.environ["PATH"]
    # command line arguments
    help_text = """
        GOAL:
        #####
            - organize api query request
            - prepare files to run request
            - launch request.
        """
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    args = parser.parse_args()

    return config, args


def GetQueryKey(config):
    # initialize if not existing.
    if not os.path.exists(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter")
    ):
        try:
            with open(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter"),
                "w",
            ) as fh:
                fh.write("0")
        except Exception as e:
            log.error("Could not initialize api job_counter file: {}".format(e))
            sys.exit(1)
    # get counter value.
    lock = open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter.lck"), "r+"
    )
    fcntl.lockf(lock, fcntl.LOCK_EX)
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter"), "r"
    ) as fh:
        queryKey = int(fh.readline().rstrip()) + 1
        while os.path.exists(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/{}".format(queryKey))
        ):
            queryKey += 1
    os.makedirs(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/{}".format(queryKey))
    )
    # update counter
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_counter"), "w"
    ) as fh:
        fh.write(str(queryKey))
    # close lock.
    lock.close()
    # return key
    return queryKey


# write settings to file, add status etc.
def PrepareJob(form, queryKey, config):
    wd = os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/{}".format(queryKey))
    # put status file
    with open(os.path.join(wd, "status"), "w") as fh:
        fh.write("Queued")
    # print the form.
    with open(os.path.join(wd, "form"), "w") as fh:
        for k in form:
            fh.write("{}={}\n".format(k, form[k].value))

    # local call ? (so not from external /api/SubmitQuery)
    if "local_call" in form:
        open(os.path.join(wd, "local_call"), "w").close()

    # print apiKey to file
    with open(os.path.join(wd, "apiKey"), "w") as fh:

        fh.write(hashlib.md5(form["apiKey"].value.encode("utf-8")).hexdigest())
    # qsub file.
    with open(os.path.join(wd, "qsub.sh"), "w") as fh:
        fh.write(BuildQsub(form, queryKey, config))

    # open permissions to allow scriptuser access.
    subprocess.check_call(["chmod", "-R", "777", wd])


def BuildQsub(form, queryKey, config):
    wd = os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/{}".format(queryKey))
    # header
    qsub_contents = "#!/usr/bin/env bash\n"
    qsub_contents += "#PBS -N Query.{}\n".format(queryKey)
    qsub_contents += "#PBS -o {}/stdout\n".format(wd)
    qsub_contents += "#PBS -e {}/stderr\n".format(wd)
    qsub_contents += "#PBS -l nodes1:ppn={},mem=16G\n".format(config["DATABASE"]["MYSQLTHREADS"])
    qsub_contents += "#PBS -d {}/cgi-bin\n".format(config["LOCATIONS"]["SCRIPTDIR"])
    qsub_contents += "#PBS -V\n"
    try:
        qsub_contents += "#PBS -q {}\n".format(config["HPC"]["QUEUE"])
    except KeyError:
        pass
    try:
        qsub_contents += "#PBS -A {}\n".format(config["HPC"]["ACCOUNT"])
    except KeyError:
        pass
    qsub_contents += "set -euo pipefail\n"
    try:
        qsub_contents += "export PATH={}\n".format(config["PATH"]["PATH"])
    except KeyError:
        pass

    # cmd
    if "region" in form and form["region"].value != "false" and form["region"].value != "":
        cmd = "python Run.Region.Query.py -i {} -s API".format(queryKey)
    elif "project" in form and form["project"].value != "false" and form["project"].value != "":
        cmd = "python Run.Region.Query.py -i {} -s API".format(queryKey)
    else:
        cmd = "python Run.Sample.Query.py -i {} -s API".format(queryKey)

    # run in conda env.
    # if "CONDA_ENV" in config["PATH"]:
    #    qsub_contents += "conda run -p {} {}\n".format(config["PATH"]["CONDA_ENV"], cmd)
    # else:
    qsub_contents += "{}\n".format(cmd)

    # make results readable.
    qsub_contents += "chmod a+r '{}'/results* ".format(wd)
    # return.
    return qsub_contents


def SubmitJob(queryKey, config, form):
    # lcok queue file.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue.lck"), "w+"
    ) as qh:
        fcntl.lockf(qh, fcntl.LOCK_EX)
        # write to queue file:
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue"), "r"
        ) as q:
            queue = q.readlines()
            # strip empty lines if any
            queue = [x.rstrip() for x in queue if x.rstrip()]
        # add new item
        queue.append(str(queryKey))
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue"), "w"
        ) as q:
            q.write("{}\n".format("\n".join(queue)))

    # get queue position.
    queue_id = None
    queue_position = sum(
        1
        for line in open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results/.job_queue")
        )
    )
    log.info("queue pos : {}".format(queue_position))
    # on hpc
    if int(config["HPC"]["USEHPC"]) == 1:
        if "preload" in form and int(form["preload"].value) == 1:
            cmd = 'echo  {} | sudo -u {} -S bash -c "python VariantDB_HPC_Query_Launcher.py {} >> {}/Query_Logs/HPC_Query_Launcher.log 2>&1 " '.format(
                config["USERS"]["SCRIPTPASS"],
                config["USERS"]["SCRIPTUSER"],
                queryKey,
                config["LOCATIONS"]["SCRIPTDIR"],
            )
            # fork & start.
            try:
                pid = os.fork()
            except Exception as e:
                log.error("Could not fork : {}".format(e))
                raise e
            if pid == 0:
                # configure to keep running.
                os.chdir("/")
                os.setsid()
                os.umask(0)
                try:
                    subprocess.check_call(cmd, shell=True)
                    # exit child
                    sys.exit(0)
                except Exception as e:
                    log.error("Could not run Query_Launcher : {}".format(e))
                    sys.exit(1)
        else:
            wd = os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "api/query_results", queryKey)
            cmd = 'echo {} | sudo -u {} -S bash -c "qsub {}/qsub.sh"'.format(
                config["USERS"]["SCRIPTPASS"], config["USERS"]["SCRIPTUSER"], wd
            )
            # run.
            try:
                queue_id = subprocess.check_output(cmd, shell=True)
            except Exception as e:
                log.error("Could not submit job : {} : {}".format(queryKey, e))
                sys.exit(1)
    else:
        # handled by api monitor. launch if needed.
        log.debug("Check if query runner is active")
        pid_file = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs/VariantDB_Query_Runner.pid"
        )
        if not os.path.exists(pid_file):
            with open(pid_file, "w") as fh:
                fh.write("-1")
        with open(pid_file, "r") as fh:
            pid = fh.readline().rstrip()
            if not pid:
                pid = -1
        if not os.path.exists("/proc/{}".format(pid)):
            try:
                fork_pid = os.fork()
            except Exception as e:
                log.error("Could not fork : {}".format(e))
                raise e
            if fork_pid == 0:
                # configure to keep running.
                os.chdir("/")
                os.setsid()
                os.umask(0)
                log_file = os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs/VariantDB_Query_Runner.log"
                )

                cmd = "(echo {} | sudo -u {} -S bash -c \"cd {}/cgi-bin && ./Service_Launcher.sh '{}/cgi-bin' 'VariantDB_Query_Runner.py' '{}' \" ) 2>>{}".format(
                    config["USERS"]["SCRIPTPASS"],
                    config["USERS"]["SCRIPTUSER"],
                    config["LOCATIONS"]["SCRIPTDIR"],
                    config["LOCATIONS"]["SCRIPTDIR"],
                    log_file,
                    log_file,
                )
                try:
                    pid = subprocess.check_output(cmd, shell=True)
                    with open(pid_file, "w") as fh:
                        fh.write(pid.decode("utf-8"))
                    log.info("Query_Runner launched")
                    # exit child.
                    sys.exit(0)
                except Exception as e:
                    log.error("Failed to run Query_Runner : {}".format(e))
                    sys.exit(1)
    return queue_position, queue_id


def Fail(msg):
    log.error(msg)
    print("Content-type: application/json\r\n")
    print(json.dumps({"ERROR": msg, "status": "error"}))
    sys.exit(1)


if __name__ == "__main__":
    # PAYLOAD : handles both GET and POST options.
    form = cgi.FieldStorage()
    # header :
    print("Content-type: application/json\r\n")
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)
    # start logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_API_Wrapper",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        quiet=True,
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)
    conda = subprocess.check_output("echo $PATH", shell=True)
    log.debug("Running path : {}".format(conda))
    # error handler : cmg-module
    # cgitb.enable(display=0, logdir=config["LOGGING"]["LOG_PATH"])

    # get job key.
    try:
        queryKey = GetQueryKey(config)
    except Exception as e:
        Fail("Could not fetch queryKey: {}".format(e))

    # prepare job
    try:
        PrepareJob(form, queryKey, config)
    except Exception as e:
        Fail("Could no prepare query folder for {} : {}".format(queryKey, e))

    # queue job.
    try:
        queue_pos, queue_id = SubmitJob(queryKey, config, form)
    except Exception as e:
        Fail("Could not queue request {} : {}".format(queryKey, e))

    # format results.
    result = {"query_key": queryKey, "queue_position": queue_pos}
    if queue_id:
        result["hpc_job_id"] = queue_id
    print(json.dumps(result))
