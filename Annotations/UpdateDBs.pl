#!/usr/bin/perl

## AIM: Check if data files exists, download if absent. No check for updates

$|++;
use XML::Simple;
use Getopt::Std;
use DBI;
use Cwd 'abs_path';

# u : email to send finish notification
# b : genome build
# i : installation trigger. First run skips old/big annotations and notifies the user of this.
getopts('u:b:i', \%opts);  # option are in %opts

## connect to database.
$credpath = abs_path("../.LoadCredentials.pl");
require($credpath);

my $install = 0;
if (defined($opts{'i'})) {
	$install = 1;
}

## set build to current  build if not provided
my $db = "NGS-Variants-Admin";
$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	my $sth = $dbh->prepare("SELECT name FROM `CurrentBuild` ");
	$sth->execute();
	my ($currentdb) = $sth->fetchrow_array();
	$sth->finish();
	$db = "NGS-Variants$currentdb";
	$dbh->disconnect();
}
else {
	$dbh->disconnect();
	$db = "NGS-Variants".$opts{'b'};
}
$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 

## connect to the correct build database.
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}


## get replacement values from database. These values are used to make the script build independent
my $sth = $dbh->prepare("SELECT Annotation, Value FROM `Annotation_DataValues`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
my $dbvalues;
foreach(@$aref) {
	$dbvalues{$_->[0]} = $_->[1];
}
$sth->finish();

my $date = `date`;
my $output = "Starting time: $date\n";

my $xml = new XML::Simple;
# this xml file contains all the parameter/method settings.
my $settings = $xml->XMLin("Config.xml");
## processed items
my %queued;
## some datalocations
my %datalocations;
$datalocations{'ANNOVAR'} = 'humandb';
$datalocations{'snpEff'} = '.';
my %skipped = ();
## loop settings
foreach my $subdir (keys %{$settings}) {
	$subdir =~ s/^_//;
	$output .= "Checking: $subdir\n";
	
	foreach my $anno (keys(%{$settings->{$subdir}})) {
		# skip deprecated.
		if (exists($settings->{$subdir}->{$anno}->{deprecated})) {
			next;
		}	
		## get AnnotationSource
		if (!defined($settings->{$subdir}->{$anno}->{AnnotationSource})) {
			$output .= "No Annotation Source available for $subdir/$anno. Skipping\n";
			next;
		}
		$annosource = $settings->{$subdir}->{$anno}->{AnnotationSource};
		if (defined($queued{"$subdir-$annosource"})) {
			$output .= "$subdir/$annosource is already processed from ".$queued{"$subdir-$annosource"}.". Skipping for $subdir/$anno\n";
			next;
		}
		if ($annosource eq 'none') {
			$output .= "AnnotationSource for $subdir/$anno == 'none'. Skipping\n";
			$queued{"$subdir-$annosource"} = "$subdir/$anno";
			next;
		}
		## if installing && skip is set, skip.
		if ($install == 1 && defined($settings->{$subdir}->{$anno}->{skip_during_install})) {
			$skipped{"$subdir : $annosource"} = 1;
			$queued{"$subdir-$annosource"} = "$subdir/$anno";
			next;
		}
		## is there an "extra" db specified? check first before skipping. This is only for ANNOVAR (for now)
		if (defined($settings->{$subdir}->{$anno}->{ExtraDB})) {
			my $extradb = $settings->{$subdir}->{$anno}->{ExtraDB};
			if (!defined($queued{"$subdir-$extradb"})) {
				if (-e "ANNOVAR/humandb/$dbvalues{$subdir}_$extradb.txt") {
					print "extraDB already present. skipping ANNOVAR/humandb/$dbvalues{$subdir}_$extradb.txt\n";
				}
				else {
					my $command = "cd ANNOVAR && perl annotate_variation.pl -buildver $dbvalues{$subdir} -downdb -webfrom annovar $extradb humandb/";
					system($command)== 0 || DieWithGrace("Downloading of $extradb failed : $?\n");
				}
				$queued{"$subdir-$extradb"} = "$subdir/$anno/ExtraDB";
			}
		}

		my $renamed = $anno;
		## skip if no db-file is specified.
		if (!defined($settings->{$subdir}->{$anno}->{AnnotationFile})) {
			$output .= "No File to download for $subdir/$anno. Skipping\n";
			next;
		}
		my $dbfile = $settings->{$subdir}->{$anno}->{AnnotationFile};
		my $replace = $dbvalues{$subdir};
		$dbfile =~ s/\%build\%/$replace/g;	

		##  skip if the file exists.
		if (-e "$subdir/".$datalocations{$subdir}."/$dbfile") {
			$output .= "AnnotationSource is already present for $subdir/$anno ($dbfile). Skipping\n";
			$queued{"$subdir-$annosource"} = "$subdir/$anno";
			next;
		}
		$output .= "\t=> Fetching $subdir/$annosource\n";
		$queued{"$subdir-$annosource"} = "$subdir/$anno";
		# ANNOVAR
		if ($subdir eq 'ANNOVAR') {
			## exception: exac03* has custom table available from main VariantDB website.
			if ($annosource =~ m/exac03/) {
				$an_table = $annosource."_AN";
				# if here, then main table is missing. Also delete the AN tables.
				if (-e "ANNOVAR/humandb/$dbvalues{$subdir}_$an_table.txt") {
					system("rm -f 'ANNOVAR/humandb/$dbvalues{$subdir}_$an_table.txt' 'ANNOVAR/humandb/$dbvalues{$subdir}_$an_table.txt.idx'") == 0 || DieWithGrace("Could not remove $an_table files : $?\n");
				}
				#system("wget -q -O 'ANNOVAR/humandb/$dbvalues{$subdir}_$an_table.txt.gz'  'http://143.169.238.105/variantdb/Public_Files/$dbvalues{$subdir}_$an_table.txt.gz' ; cd ANNOVAR/humandb/ ; gunzip $dbvalues{$subdir}_$an_table.txt.gz ");
				#system("wget -q -O 'ANNOVAR/humandb/$dbvalues{$subdir}_$an_table.txt.idx'  'http://143.169.238.105/variantdb/Public_Files/$dbvalues{$subdir}_$an_table.txt.idx'");
				my @files = qw/hg19_exac03_AN.txt.gz hg19_exac03_AN.txt.idx.gz/;
				my @md5s = qw/hg19_exac03_AN.txt.md5/;
				my $url = "https://cloud.biomina.be/index.php/s/tPpFxmumhj4Loi2/download?path=%2F&files=";
				# download big files
				foreach my $file (@files) {
					print "Downloading $file\n";
					my $cmd = "cd ANNOVAR/humandb/ && wget -c -q -O $file '$url$file' ";
					system($cmd) == 0 || DieWithGrace("Could not download $file : $url$file : $?\n");
				}
				# do checksums 
				foreach my $md5file (@md5s) {
					# download
					print "Checking MD5sums for $md5file\n";
					my $cmd = "cd ANNOVAR/humandb/ && wget -c -q -O $md5file '$url$md5file'";
					system($cmd) == 0 || DieWithGrace("Could not download $md5file : $url$md5file : $?\n");
					# check
					$cmd = "cd ANNOVAR/humandb/ && md5sum -c $md5file";
					system($cmd) == 0 || DieWithGrace("MD5 checks failed in $md5file : $?\n");
				}
				# unpack
				foreach my $file (@files) {
					print "Unpacking $file\n";
					my $cmd = "cd ANNOVAR/humandb/ && gunzip $file";
					system($cmd) == 0 || DieWithGrace("Could not unpack $file : $?\n");
				}

			}
			# gnomad is provided by VDB.
			if ($annosource =~ m/gnomAD_g_2.1/) {
				my @files = qw/hg19_gnomAD_g_2.1.txt.gz hg19_gnomAD_g_2.1.txt.idx.gz/;
				my @md5s = qw/hg19_gnomAD_g_2.1.txt.md5/;
				my $url = "https://cloud.biomina.be/index.php/s/tPpFxmumhj4Loi2/download?path=%2F&files=";
				# download big files
				foreach my $file (@files) {
					print "Downloading $file\n";
					my $cmd = "cd ANNOVAR/humandb/ && wget -c -q -O $file '$url$file'";
					system($cmd) == 0 || DieWithGrace("Could not download $file : $url$file : $?\n");
				}
				# do checksums 
				foreach my $md5file (@md5s) {
					# download
					print "Checking MD5sums for $md5file\n";
					my $cmd = "cd ANNOVAR/humandb/ && wget -c -q -O $md5file '$url$md5file'";
					system($cmd) == 0 || DieWithGrace("Could not download $md5file : $url$md5file : $?\n");
					# check
					$cmd = "cd ANNOVAR/humandb/ && md5sum -c $md5file";
					system($cmd) == 0 || DieWithGrace("MD5 checks failed in $md5file : $?\n");
				}
				# unpack
				foreach my $file (@files) {
					print "Unpacking $file\n";
					my $cmd = "cd ANNOVAR/humandb/ && gunzip $file";
					system($cmd) == 0 || DieWithGrace("Could not unpack $file : $?\n");
				}


			}
			# gnomad is provided by VDB.
			if ($annosource =~ m/gnomAD_e_2.1/) {
				my @files = qw/hg19_gnomAD_e_2.1.txt.gz hg19_gnomAD_e_2.1.txt.idx.gz/;
				my @md5s = qw/hg19_gnomAD_e_2.1.txt.md5/;
				my $url = "https://cloud.biomina.be/index.php/s/tPpFxmumhj4Loi2/download?path=%2F&files=";
				# download big files
				foreach my $file (@files) {
					print "Downloading $file\n";
					my $cmd = "cd ANNOVAR/humandb/ && wget -c -q -O $file '$url$file'";
					system($cmd) == 0 || DieWithGrace("Could not download $file : $url$file : $?\n");
				}
				# do checksums 
				foreach my $md5file (@md5s) {
					# download
					print "Checking MD5sums for $md5file\n";
					my $cmd = "cd ANNOVAR/humandb/ && wget -c -q -O $md5file '$url$md5file'";
					system($cmd) == 0 || DieWithGrace("Could not download $md5file : $url$md5file : $?\n");
					# check
					$cmd = "cd ANNOVAR/humandb/ && md5sum -c $md5file";
					system($cmd) == 0 || DieWithGrace("MD5 checks failed in $md5file : $?\n");
				}
				# unpack
				foreach my $file (@files) {
					print "Unpacking $file\n";
					my $cmd = "cd ANNOVAR/humandb/ && gunzip $file";
					system($cmd) == 0 || DieWithGrace("Could not unpack $file : $?\n");
				}


			}
			# continue for main tables.	
			elsif ($annosource =~ m/1000g/) {
				$annosource =~ s/(.*)_(\S+)/$1/;
				my $command = "cd ANNOVAR && perl annotate_variation.pl -buildver $dbvalues{$subdir} -downdb -webfrom annovar $annosource humandb/";
				#print "Command: $command\n";
				system($command) == 0 || DieWithGrace("Could not download $annosource : $?\n");
			}
			elsif ($annosource =~ m/ljb\d*_/) {
				$annosource = lc($annosource);
				$annosource =~ s/polyphen2/pp2/;
				$annosource =~ s/muttast/mt/;
				$annosource =~ s/gerp/gerp\+\+/;
				my $command = "cd ANNOVAR && perl annotate_variation.pl -buildver $dbvalues{$subdir} -downdb -webfrom annovar '$annosource' humandb/";
				#print "Command: $command\n";
				system($command) == 0 || DieWithGrace("Could not download $annosource : $?\n");
			}	
			elsif ($annosource =~ m/avsift|mce46way|mirna|tfbf|esp5400_|snp1\d+|knownGene|ensGene|refGene|esp6500|exac|cosmic|kaviar|dbscsnv/i) {
				my $command = "cd ANNOVAR && perl annotate_variation.pl -buildver $dbvalues{$subdir} -downdb -webfrom annovar $annosource humandb/";
				system($command) == 0 || DieWithGrace("Could not download $annosource : $?\n");
				## extra : reflink.
				if ($annosource =~ m/refGene/i) {
					print "Downloading refLink.\n";
					system("cd ANNOVAR/humandb && wget -q 'http://hgdownload.cse.ucsc.edu/goldenPath/hgFixed/database/refLink.txt.gz' && gunzip refLink.txt.gz && mv refLink.txt $dbvalues{$subdir}"."_refLink.txt") == 0 || DieWithGrace("Could not download refLink : $?\n");
				}
			}
			elsif ($annosource =~ m/segdup/) {
				my $command = "cd ANNOVAR && perl annotate_variation.pl -buildver $dbvalues{$subdir} -downdb genomicSuperDups humandb/";
				#print "Command: $command\n";
				system($command) == 0 || DieWithGrace("Could not download $annosource : $?\n");
			}
			elsif ($annosource =~ m/cadd/i) {
				my $command = "cd ANNOVAR && perl annotate_variation.pl -buildver $dbvalues{$subdir} -downdb -webfrom annovar caddgt20 humandb/";
				#print "Command: $command\n";
				system($command) == 0 || DieWithGrace("Could not download $annosource : $?\n");
				system("cd ANNOVAR/humandb && ln -s $dbvalues{$subdir}"."_caddgt20.txt $dbvalues{$subdir}"."_cadd.txt && ln -s $dbvalues{$subdir}"."_caddgt20.txt.idx $dbvalues{$subdir}"."_cadd.txt.idx") == 0 || DieWithGrace("Could not download $annosource : $?\n");
				$output .= "###############\n";
				$output .= "## WARNING : ##\n";
				$output .= "###############\n";
				$output .= "  By default, VariantDB downloads the CADD_gt_20 annotation set from ANNOVAR and links it to the full dataset name (hg19_cadd.txt). \n";
				$output .= "  To use the full dataset, remove the links and manually downlad the data (330Gb !) using the following command: \n";
				$output .= "     perl annotate_variation.pl -buildver $dbvalues{$subdir} -downdb -webfrom annovar cadd humandb/\n";
				$output .= "####################\n";
				$output .= "## END OF WARNING ##\n";
				$output .= "####################\n";

			}

			else {
				$output .= "###############\n";
				$output .= "## WARNING : ##\n";
				$output .= "###############\n";
				$output .= "  $subdir/$anno not recognised by database-downloader. \n\n";
			}
		}
		elsif($subdir eq 'snpEff') {
			$annosource =~ s/\%build\%/$dbvalues{$subdir}/;
			my $command = "cd snpEff && ../../dependencies/sun-jre-8/bin/java -jar snpEff.jar download $annosource";
			system($command) == 0 || DieWithGrace("Could not download $annosource : $?\n");
		}
		elsif($subdir eq 'ClinVar') {
			my $command = "cd ClinVar && perl UpdateClinVar.pl";
			system($command) == 0 || DieWithGrace("Could not update ClinVar : $?\n");
		}
		elsif($subdir eq 'CADD') {
			$output .= "CADD data is missing, but this can not be automated !\n";
		}	
		else {
			$output .= "###############\n";
			$output .= "## WARNING : ##\n";
			$output .= "###############\n";
			$output .= "  $subdir/$anno not recognised by database-downloader. \n\n";
		}
		$output .= "Downloading of $subdir/$anno finished. \n";
		sleep 5;	
	}
}

## UPDATE HPO tables indepedently of Config.xml file. 
my $command = "cd HPO && perl Update.HPO.db.pl >> /tmp/dbupdate.mail.txt";
system($command) == 0 || DieWithGrace("Could not update HPO : $?\n");

## UPDATE GO 
my $command = "cd GO_Annotations && perl Update_GO.pl >> /tmp/dbupdate.mail.txt";
system($command) == 0 || DieWithGrace("Could not update GO: $?\n");

#######################
## send mail to user ##
#######################
# first compose message.
if ($install != 1 && exists($opts{'u'})) {
	$email = $opts{'u'};
	open OUT, ">/tmp/dbupdate.mail.txt";
	print OUT "to: $email\n";
	print OUT "subject: VariantDB Annotation Database Update Finished\n";
	print OUT "from: no-reply\@variantdb.ua.ac.be\n\n";
	print OUT "The Annotation databases were updated. This was the program output: \n\n";
	print OUT $output;	
	close OUT;
	system("sendmail $email < /tmp/dbupdate.mail.txt ");
}
## notify skipped if installing
if ($install == 1 && keys(%skipped) > 0) {
	print "The following annotation sources were not downloaded. They are either very large (e.g. CADD), or deprecated:\n";
	foreach(keys(%skipped)) {
		print "  - $_\n";
	}
	print "\n";
	print "You have now two options: \n";
	print "  - Re-run $scriptdir/Annotations/UpdateDBs.pl  : This second run will download all remaining databases\n";
	print "  - Edit $scriptdir/Annotations/Config.xml and comment out annotations you don't want/need. Then run the UpdateDB.pl script.\n";
}
# trigger validation.
open LOCK, ">$scriptdir/Query_Results/.validator.lck";
flock(LOCK,2);
open OUT, ">>$scriptdir/Query_Results/.validator.queue";
print OUT "Anno\n";
close OUT;
close LCK;
exit();

sub DieWithGrace {
	my $msg = shift;
	my $email = $adminemail ;
	open OUT, ">/tmp/dbupdate.error.mail.txt";
	print OUT "to: $email\n";
	print OUT "subject: VariantDB Annotation Database Update ERROR\n";
	print OUT "from: no-reply\@variantdb.uantwerpen.be\n\n";
	print OUT "The Annotation databases were updated but an error was encountered. Please investigate:\n\n";
	print OUT $msg;	
	close OUT;
	system("sendmail $email < /tmp/dbupdate.error.mail.txt ");
	die("$msg\n");
}
