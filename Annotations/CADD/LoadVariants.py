from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, AutoVivification, CheckVariableType
import configparser
import argparse
import os
import sys
import time
import subprocess
import shelve
import gzip

# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


## some variables/objects
re = Re()  # customized regex-handler


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Run CADD on the all NEW variants & store to DB
            - Run CADD on all Variants & store to dedicated validation table in DB
            - Run CADD on a VCF file & store to new VCF file.
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Run in Validation Mode. All variants are annotated, and stored in a separate table.",
    )
    # annotation (ignored)
    parser.add_argument(
        "-a",
        "--annotation",
        required=False,
        help="Ignored argument, present for compatibility with other sources",
    )
    # file mode
    parser.add_argument(
        "-f", "--infile", required=False, help="Input VCF file to use in file-based mode."
    )
    parser.add_argument(
        "-o", "--outfile", required=False, help="Output VCF file to use in file-based mode."
    )
    args = parser.parse_args()
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    return config, args


def StoreResults(cadd_outfile):

    # create shelve/pickle object to get variant id
    sf = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/CADD/Output_Files/",
        "{}.{}.shelf".format(args.table_name, time.time()),
    )
    for suffix in ["bak", "dat", "dir"]:
        annotator.files.append(f"{sf}.{suffix}")
    s = shelve.open(sf)
    # another shelf to hold annotations.
    if annotator.args.infile:
        sfa = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/CADD/Output_Files/",
            "{}.{}.anno.shelf".format(args.table_name, time.time()),
        )
        log.debug(f"Opening second shelf: {sfa}")
        s_anno = shelve.open(sfa)
        for suffix in ["bak", "dat", "dir"]:
            annotator.files.append(f"{sf}.{suffix}")

    # get over infile to get variantIDs per variant.
    with open(annotator.vcf, "r") as f_in:
        for line in f_in:
            if line.startswith("#"):
                continue
            cols = line.rstrip().split("\t")
            vkey = "{},{},{},{}".format(cols[0], cols[1], cols[3], cols[4])
            if vkey not in s:
                s[vkey] = [cols[2]]
            else:
                # see discussion on safe shelve-coding: https://docs.python.org/3/library/shelve.html
                tmp = s[vkey]
                tmp.append(cols[2])
                s[vkey] = tmp

    # parse cadd output
    cadd_load_file = cadd_outfile.replace("tsv.gz", "load.txt")
    with gzip.open(cadd_outfile, "rt") as f_in, open(cadd_load_file, "w") as f_out:
        for line in f_in:
            if line.startswith("#"):
                continue
            cols = line.rstrip().split("\t")
            vids = s["{},{},{},{}".format(cols[0], cols[1], cols[2], cols[3])]
            # why multiple ? : effect of normalization
            for vid in vids:
                if annotator.args.infile:
                    s_anno[vid] = cols
                else:
                    f_out.write("{}\t{}\t{}\n".format(vid, cols[4], cols[5]))

    # close the shelf.
    s.close()
    # if validation:
    if annotator.args.infile:
        # parse load file again : outfile might be slightly altered compared to provided infile...
        with open(annotator.args.infile, "r") as f_in, open(annotator.args.outfile, "w") as f_out:
            line_idx = 0
            for line in f_in:
                # headers
                if line.startswith("#"):
                    f_out.write(line)
                    continue
                line_idx += 1
                # variantID
                cols = line.rstrip().split("\t")
                if cols[2] == '.':
                    vid = f"line_{line_idx}"
                else:
                    vid = cols[2]
                if vid not in s_anno:
                    log.warning(f"Variant {vid} not found in shelf. Skipping")
                    continue
                cols[7] += ";CADDv1.4=Raw_Score:{},Phred_Score:{}".format(
                    s_anno[vid][4], s_anno[vid][5]
                )
                f_out.write("\t".join(cols) + "\n")

    else:
        annotator.LoadDataTable(cadd_load_file)


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # get logger
    log = StartLogger(config, "CADD")

    # get the annotator object
    annotator = Annotator(config=config, method="CADD", annotation="CADDv1.4", args=args)

    # set table name.
    annotator.args.table_name = "Variants_x_CADD_v1.4"
    annotator.GetTableInfo()

    ## Validate options
    try:
        annotator.ValidateTable()
    except (DataError, ValueError) as e:
        log.error(e)
        sys.exit(1)

    # Get Input Variants
    log.info("Retrieving variants to process")
    if not annotator.args.infile:
        try:
            log.info("Variant source == Database")
            annotator.GetVariants(normalize=True)
        except Exception as e:
            log.error("Could not fetch input variants: {}".format(e))
            sys.exit(1)
    else:
        # cp to the tmp folder, adding line_idx as id in INFO, strip non-needed fields
        log.info("Variant Source == VCF file")
        annotator.PrepareInputVCF(normalize=True, strip_chr=True)
        # convert to correct format (add the id field)
    annotator.files.append(annotator.vcf)

    # run the annotation procedures
    try:
        # get build name
        build = annotator.dbh.runQuery(
            "SELECT `Value` FROM `Annotation_DataValues` WHERE `Annotation` = 'CADD'"
        )[0]["Value"]
        # out file:
        cadd_outfile = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/CADD/Output_Files/",
            "{}.{}.tsv.gz".format(annotator.args.table_name, time.time()),
        )
        # build command
        cmd = "export PATH={}/bin:{}:$PATH && micromamba run -p '{}' ./CADD_v1.4/CADD.sh -g '{}' -o '{}' '{}'".format(
            config.get("PATH", "CONDA_ENV", fallback=""),
            config.get("PATH", "PATH", fallback=""),
            config.get("PATH", "CADD_ENV", fallback=""),
            build,
            cadd_outfile,
            annotator.vcf,
        )
        log.debug("Launching third-party CADD scripts")
        subprocess.check_call(cmd, shell=True)
        log.debug("CADD finished.")
        annotator.files.append(cadd_outfile)

    except Exception as e:
        log.error("CADD failed : {}".format(e))
        sys.exit(1)
    # Process & store results
    try:
        log.debug("Processing Results")
        StoreResults(cadd_outfile)
        annotator.files.append(cadd_outfile.replace(".tsv.gz", ".load.txt"))
    except Exception as e:
        log.error("Failed to process CADD results: {}".format(e))
        sys.exit(1)

    # cleanup
    #annotator.cleanup()
