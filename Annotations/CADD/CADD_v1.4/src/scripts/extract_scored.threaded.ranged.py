#!/usr/bin/env python
# -*- coding: ASCII -*-

import sys, os
import pysam
from optparse import OptionParser
from multiprocessing import Process, Pool
import pickle
import tempfile
import shutil


def ScoreChrom(filename, chrom, tmpdir, scores):
    regionTabix = pysam.Tabixfile(filename, "r")
    # with open(tmpdir+"/scores."+str(chrom)+'.dict','rb') as f:
    # scores = pickle.load(f)
    # sort the list.
    positions = scores.keys()
    positions.sort(key=int)
    sys.stderr.write(
        "\t - Scoring chrom " + str(chrom) + ": " + str(len(positions)) + " positions\n"
    )

    # process.
    done = set()
    for i in xrange(len(positions)):
        # position seen before.
        if positions[i] in done:
            continue
        done.add(positions[i])
        range_start = int(positions[i])
        inc = 1
        while (
            i + inc < len(positions) and int(positions[i + inc]) < int(positions[i + inc - 1]) + 50
        ):
            done.add(positions[i + inc])
            inc = inc + 1

        range_end = int(positions[i + inc - 1])

        try:
            # [ chrom,pos,ref,alt] = var_key.split('|')
            for regionHit in regionTabix.fetch(chrom, range_start - 1, range_end):
                vfields = regionHit.rstrip().split("\t")
                if not vfields[1] in scores:
                    continue

                lvar_key = vfields[2] + "|" + vfields[3]

                if not lvar_key in scores[vfields[1]]:
                    continue

                scores[vfields[1]][lvar_key] = vfields[4] + "\t" + vfields[5]
        except Exception as e:  # ValueError:
            sys.stderr.write(
                "Encountered problem on: "
                + chrom
                + ":"
                + str(range_start)
                + "-"
                + str(range_end)
                + ": "
                + str(e)
                + "\n"
            )

    with open(tmpdir + "/scores." + str(chrom) + ".dict", "wb") as f:
        scores = pickle.dump(scores, f)


## get options
parser = OptionParser()
parser.add_option("-p", "--path", dest="path", help="Path to scored variants.")
parser.add_option(
    "--found_out",
    dest="found_out",
    help="Write found variants to file (default 'output.tsv')",
    default=None,
)
parser.add_option(
    "--header",
    dest="header",
    help="Write full header to output (default none)",
    default=False,
    action="store_true",
)
parser.add_option("-t", "--threads", dest="threads", help="Threads to use (default = 1)", default=1)
# parser.add_option('-b','--binsize',dest='binsize',help='binsize',default=5000)
(options, args) = parser.parse_args()

if options.found_out:
    found_out = open(options.found_out, "w")
else:
    found_out = sys.stdout

fref, falt = 2, 3
if os.path.exists(options.path) and os.path.exists(options.path + ".tbi"):
    filename = options.path
    # sys.stderr.write("  - Opening %s...\n"%(filename))
    regionTabix = pysam.Tabixfile(filename, "r")
    header = list(regionTabix.header)
    for line in header:
        if options.header:
            found_out.write(line + "\n")
        try:
            fref = line.split("\t").index("Ref")
            falt = line.split("\t").index("Alt")
        except ValueError:
            pass
    regionTabix.close()
else:
    raise IOError("No valid file with pre-scored variants.\n")

## temp file
tmpdir = tempfile.mkdtemp()
# sys.stderr.write("tmp dir : "+tmpdir)

# keep stuff.
# manager = Manager()
scores = dict()  # manager.dict()
lines = []
sys.stderr.write("  - preparing score dict.\n")

# scan to list needed bins & construct score dict
lidx = 0
for line in sys.stdin:
    if line.startswith("#"):
        lines.append(line)
        continue

    try:
        lidx = lidx + 1
        fields = line.rstrip().split("\t")
        chrom = fields[0]
        if not chrom in scores:
            scores[chrom] = dict()
        if not fields[1] in scores[chrom]:
            scores[chrom][fields[1]] = dict()

        # var_key = chrom+"|"+str(fields[1])+"|"+fields[-2]+"|"+fields[-1]
        var_key = fields[-2] + "|" + fields[-1]
        scores[chrom][fields[1]][var_key] = ""
        lines.append(line)
    except:
        sys.stderr.write("problem encountered when parsing stream on line: " + str(lidx) + "\n")

sys.stderr.write("  - Scoring variants\n")
pool = Pool(processes=int(options.threads))
for chrom in scores:
    # sys.stderr.write("  - Saving chrom "+str(chrom)+"\n")
    ## write out the bins to do.
    # with open(tmpdir+'/scores.'+str(chrom)+'.dict', 'wb') as f:
    #  pickle.dump(scores[chrom], f)
    # sys.stderr.write("  - ok. now starting thread\n")
    ## launch thread
    pool.apply_async(ScoreChrom, args=(filename, chrom, tmpdir, scores[chrom]))


# wait for all to finish
pool.close()
pool.join()

# reload scores.
for chrom in scores:
    with open(tmpdir + "/scores." + str(chrom) + ".dict", "rb") as f:
        scores[chrom] = pickle.load(f)


sys.stderr.write("\n")
# finally, print.
sys.stderr.write("  - Printing results\n")
for line in lines:
    if line.startswith("#"):
        sys.stdout.write(line)
        continue

    fields = line.rstrip().split("\t")
    chrom = fields[0]
    # pos = int(fields[1])
    lref, allele = fields[-2], fields[-1]
    var_key = fields[-2] + "|" + fields[-1]
    if scores[chrom][fields[1]][var_key] == "":
        sys.stdout.write(line)
    else:
        found_out.write(
            chrom
            + "\t"
            + fields[1]
            + "\t"
            + lref
            + "\t"
            + allele
            + "\t"
            + scores[chrom][fields[1]][var_key]
            + "\n"
        )


if options.found_out:
    found_out.close()


# manager._process.terminate()
# manager.shutdown()
shutil.rmtree(tmpdir)
sys.exit()
