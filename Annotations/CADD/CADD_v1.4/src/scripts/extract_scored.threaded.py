#!/usr/bin/env python
# -*- coding: ASCII -*-

import sys, os
import pysam
from optparse import OptionParser
from multiprocessing import Process, Pool
import pickle 
import tempfile
import shutil


def ScoreChrom(filename,chrom,tmpdir):
   regionTabix = pysam.Tabixfile(filename,'r')
   #with open(tmpdir+'/scan.bins.'+str(chrom)+'.set','rb') as f:  # Python 3: open(..., 'wb')
   # bins = pickle.load(f)
   with open(tmpdir+"/scores."+str(chrom)+'.dict','rb') as f:
    scores = pickle.load(f)
   sys.stderr.write("\t - Scoring chrom "+str(chrom)+": "+str(len(scores))+" variants\n")
   #sys.stderr.write(repr(bins))
   for var_key in scores:
     # position seen before, no need to requery (indels, different alt in SNV, ...)
     if scores[var_key] != '' :
        #sys.stderr.write("\t  => Skipping double query for "+var_key)
        continue
     #sys.stderr.write(str(chrom)+":"+str(bin_start) + "\n" )
     try: 
            [ chrom,pos,ref,alt] = var_key.split('|')
            for regionHit in regionTabix.fetch(chrom, int(pos)-1, int(pos)):
              vfields = regionHit.rstrip().split('\t')
              lvar_key = chrom+"|"+str(vfields[1])+"|"+vfields[2]+"|"+vfields[3]
              
              if not lvar_key in scores:
                continue
              scores[lvar_key] = vfields[4]+"\t"+vfields[5]
     except ValueError:
                sys.stderr.write('Encountered uncovered chromosome: '+chrom+'\n')
   
   with open(tmpdir+"/scores."+str(chrom)+'.dict','wb') as f:
    scores = pickle.dump(scores,f) 


## get options
parser = OptionParser()
parser.add_option("-p","--path", dest="path", help="Path to scored variants.")
parser.add_option("--found_out", dest="found_out", help="Write found variants to file (default 'output.tsv')",default=None)
parser.add_option("--header", dest="header", help="Write full header to output (default none)",default=False, action="store_true")
parser.add_option("-t","--threads", dest="threads", help="Threads to use (default = 1)",default=1)
#parser.add_option('-b','--binsize',dest='binsize',help='binsize',default=5000)
(options, args) = parser.parse_args()

if options.found_out:
    found_out = open(options.found_out,'w')
else:
    found_out = sys.stdout

fref,falt = 2,3
if os.path.exists(options.path) and os.path.exists(options.path+".tbi"):
  filename = options.path
  #sys.stderr.write("  - Opening %s...\n"%(filename))
  regionTabix = pysam.Tabixfile(filename,'r')
  header = list(regionTabix.header)
  for line in header:
    if options.header:
      found_out.write(line+"\n")
    try:
      fref = line.split('\t').index('Ref')
      falt = line.split('\t').index('Alt')
    except ValueError:
      pass
  regionTabix.close()
else:
  raise IOError("No valid file with pre-scored variants.\n")

## temp file
tmpdir = tempfile.mkdtemp()
#sys.stderr.write("tmp dir : "+tmpdir)

# keep stuff.
#manager = Manager()
scores = dict() #manager.dict()
lines = []
#scan_bins = set() 
scan_dict = dict()
sys.stderr.write("  - preparing score dict.\n")

# scan to list needed bins & construct score dict
lidx = 0
for line in sys.stdin:
  if line.startswith('#'):
    lines.append(line);
    continue

  try:
    lidx = lidx + 1
    fields = line.rstrip().split('\t')
    #pos_bin = int(int(fields[1])/int(options.binsize))
    #found = False
    chrom = fields[0]
    #scan_bins.add(chrom+"|"+str(pos_bin))
    if not chrom in scores:
        #scan_dict[chrom] = set()
        scores[chrom] = dict()
    
    #pos = int(fields[1])
    #lref,allele = fields[-2],fields[-1]
    var_key = chrom+"|"+str(fields[1])+"|"+fields[-2]+"|"+fields[-1]
    scores[chrom][var_key] = ''
    lines.append(line)
  except: 
    sys.stderr.write("problem encountered when parsing stream on line: "+str(lidx)+'\n')

sys.stderr.write("  - Scoring variants\n")
pool = Pool(processes=int(options.threads))
for chrom in scores:
  ## write out the bins to do.
  with open(tmpdir+'/scores.'+str(chrom)+'.dict', 'wb') as f:  
    pickle.dump(scores[chrom], f)
  ## launch thread
  pool.apply_async(ScoreChrom,args=(filename,chrom,tmpdir))


# wait for all to finish
pool.close()
pool.join()

# reload scores.
for chrom in scores:
  with open(tmpdir+'/scores.'+str(chrom)+'.dict', 'rb') as f:
        scores[chrom] = pickle.load(f)


sys.stderr.write("\n")
# finally, print.
sys.stderr.write("  - Printing results\n")
for line in lines:
  if line.startswith('#'):
    sys.stdout.write(line)
    continue

  fields = line.rstrip().split('\t')
  chrom = fields[0]
  pos = int(fields[1])
  lref,allele = fields[-2],fields[-1]
  var_key = chrom+"|"+str(fields[1])+"|"+fields[-2]+"|"+fields[-1]
  if scores[chrom][var_key] == '':
      sys.stdout.write(line)
  else:
      found_out.write(chrom+"\t"+str(pos)+"\t"+lref+"\t"+allele+"\t"+scores[chrom][var_key]+"\n")


if options.found_out:
    found_out.close()


#manager._process.terminate()
#manager.shutdown() 
shutil.rmtree(tmpdir)
sys.exit() 


