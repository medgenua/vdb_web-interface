#!/usr/bin/perl

## only start if the Mechanize is available.
eval
{
  my $toload = "WWW//Mechanize.pm";
  require $toload;
  $toload->import();
};
if($@)
{
	dieWithGrace("Module WWW::Mechanize is not available. MutationTaster Scores will not be fetched.");
}

# other modules
use DBI;
use Getopt::Std;
use Cwd 'abs_path';

# load credentials
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);
if ($usemc == 1) {
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	my $mcpath = abs_path("../../cgi-bin/inc_memcache.pl");
	require($mcpath);	
	#use JSON;
	#our $memd = new Cache::Memcached { 'servers' => \@mcs, 'debug' => 0, 'compress_threshold' => 1_000_000, };
}


# disable buffer
$|++;

####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_; 
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";

###############################
## Do not run double process ##
###############################
if (!-e 'MT.Status.txt') {
	$status = 0;
	system("echo $status > MT.Status.txt");
}
else {
	my $status = `cat MT.Status.txt`;
	chomp($status);
	while ($status == 1) {
		#print "status == 1\n";
		## is it really running?
		my $ps = `ps auxwwwe | grep 'perl MutationTaster.pl' | grep -v grep | wc -l`;
		chomp($ps);
		if ($ps == 1) {
			
			## not running. update status.
			system("echo 0 > MT.Status.txt");
		}
		else {
			## sleep.
			sleep 60;
		}
		$status = `cat MT.Status.txt`;
		chomp($status);
	}
}

## ok, ready to start : set status.
$status = 1;
system("echo $status > MT.Status.txt");
###########################
# CONNECT TO THE DATABASE #
###########################
# opts
# n : (n)ew variants (missing mutation taster scores)
# u : (u)pdate scores older than 6 months (future work)

getopts('b:nu', \%opts) ;
## GET current/provided build
our $db = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
}

$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}


if (!defined($opts{'n'}) && !defined($opts{'u'})) {
	$opts{'n'} = '';
}
our $anno = 'MutationTaster';
our $file = "Input_Files/$anno.list.txt";

## label : comes here after data from current query are stored, untill nrrows == 0
our %vids;
NEXTBATCH:
%vids = ();

## GET VARIANTS
if (defined($opts{'n'})) {
	## all variants
	$query = "SELECT id FROM `Variants`";
	my $allvariants = $dbh->selectcol_arrayref($query);
	## wait for the number of result to stabilize (in case of multiple inserts, prevents running many small queries.
	my $prevnr = scalar(@$allvariants);
	sleep 60;
	$allvariants = $dbh->selectcol_arrayref($query);
	my $nr = scalar(@$allvariants);
	while ($nr > $prevnr) {
		sleep 60;
		$prevnr = $nr;
		print "Check if #variants incremented\n";
		
		$allvariants = $dbh->selectcol_arrayref($query);
		$nr = scalar(@$allvariants)
	}
	## variants in annotation table
	$query = "SELECT vid FROM `Variants_x_MutationTaster`";
	my $annovariants = $dbh->selectcol_arrayref($query);
	## array difference
	my %second = map {$_=>1} @$annovariants;
	my @toannotate = grep {!$second{$_}} @$allvariants;
	# limit to 15000 variants per web-batch
	@toannotate = splice(@toannotate,0,15000);
	if (scalar(@toannotate) == 0) {
		print "No variants to update.  Exiting\n";
		close OUT;
	}
	# clear up variables
	$annovariants = [];
	$allvariants = [];
	%second = ();
	open OUT, ">$file";
	print OUT "##fileformat=VCFv4.1\n";
	print OUT "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t1\n";

	if (scalar(@toannotate) == 0) {
		print "No variants to update. Exiting\n";
		## first clean 
		close OUT;
		system("rm -f $file");
		system("rm -f Output_Files/MutationTaster.Results.txt");
		## clear status.
		$status = 0;
		system("echo $status > MT.Status.txt");
		exit();
	}

	#while (my $var = join(",",splice(@toannotate,0,500))) {
		#$query = "SELECT v.chr, v.start, v.RefAllele, v.AltAllele, v.id FROM `Variants` v WHERE v.id IN ($var)";
		$query = "SELECT v.chr, v.start, v.stop, v.RefAllele, v.AltAllele, v.id AS VariantID FROM `Variants` v WHERE v.id IN (?)";
		my $rowcache = runQuery($query,"Variants",\@toannotate);

		#$sth = $dbh->prepare($query);
		#my $nrrows = $sth->execute();
		#$max = $nrrows;
		#my $rowcache;
		foreach my $result (@$rowcache)  {
			$result->{'chr'} = $chromhash{$result->{'chr'}};
			my $string = "chr".$result->{'chr'}."\t".$result->{'start'}."\t.\t".$result->{'RefAllele'}."\t".$result->{'AltAllele'}."\t1000\t.\tAC=1;AF=0.50;AN=2\tGT:AD:DP:GQ:PL\t0/1:50,50:100:1000:1000,0,1000\n";
			## muttast rewrites indels!
			if (length($result->{'RefAllele'}) > length($result->{'AltAllele'})) {
				# deletion.
				# position
				$result->{'start'} = $result->{'start'} + 1; #( length($result->[2]) - length($result->[3]));
				# ref : 
				$result->{'RefAllele'} = substr($result->{'RefAllele'},1); # from second pos to end.
				# alt:
				if (length($result->{'AltAllele'}) == 1) {
					$result->{'AltAllele'} = ''; 
				}
				else {
					$result->{'AltAllele'} = substr($result->{'AltAllele'},1);
				}
			}
			if (length($result->{'RefAllele'}) <  length($result->{'AltAllele'})) {	
				# pos++
				$result->{'start'} = $result->{'start'} + 1;
				# ref:
				if (length($result->{'RefAllele'}) == 1) {
					$result->{'RefAllele'} = ''; 
				}
				else {
					$result->{'RefAllele'} = substr($result->{'RefAllele'},1);
				}
				# alt: 
				$result->{'AltAllele'} = substr($result->{'AltAllele'},1);
			}	
			## store vid based on pos & alleles (chr is numeric in MTQE output. 1-25)
			$vids{$chromhash{$result->{'chr'}}."-".$result->{'start'}."-".$result->{'RefAllele'}."-".$result->{'AltAllele'}}{'vid'} = $result->{'VariantID'};
			$vids{$chromhash{$result->{'chr'}}."-".$result->{'start'}."-".$result->{'RefAllele'}."-".$result->{'AltAllele'}}{'ok'} = 0;
			print OUT "$string";

		}
		#$sth->finish();
	#}

}
else {
	exit;
}
close OUT;

## disconnect from db. 
$dbh->disconnect();
print "Connecting to MutationTaster QE\n";
# come here if fetching results takes abnormally long.
MTQE:
############################
## Connect To QueryEngine ##
############################
# create object
my $mech = WWW::Mechanize->new();

my $url = "http://www.mutationtaster.org/StartQueryEngine.html";

# fetch url
$mech->get( $url) 
	or dieWithGrace("Could not fetch MutationTaster QueryEngine Page",$!);

# select the correct field
$mech->form_name('form') 
	or dieWithGrace("Form 'form' not found on MutationTaster QueryEngine Page",$!);

# set the field values.
my %fields = ('name' => 'VariantDB_Updater', 
		'filename'  => $file, 
		'email' => $adminemail, 
		'no_html' => 1, 
		'homo' => undef,
		'regions_filter' => 0,
		'only_exons' => undef,
		'tgp_filter_homo' => undef,
		'tgp_count_hetero' => undef,
		'min_cov' => 1
);
# fill in form and check if fields exist.
my $form = $mech->form_name('form');
foreach (keys(%fields)) {
	$field = $_;
	$value = $fields{$field};
	if ($form->find_input($field)) {
		eval {
			$mech->field($field,$value)
		};
		if($@){
			dieWithGrace("Could not set field : '$field' with '$value'",$@);
		
		}
	}
	else {
		dieWithGrace("Could not set field : '$field'. Field is missing.",$@);
	}
}	

# submit.
$mech->click_button('name' => 'Submit');

# base url of new page (other server!)
$base = $mech->uri();
if ($base =~ m/(http:\/\/[^\/]*\/).*/) {
	$base = $1;
}
else {
	dieWithGrace("Could not extract base url from $base");
}
# extract the refresh url from the temp page
if ($mech->content() =~ m/<meta http-equiv="refresh" content="(\d+);\sURL=([^"]*)"/) {
	$sleep = $1;
	$url = $base.$2;
	print "Sleeping $sleep seconds before going to '$url'\n";
	sleep($1);
	eval {
		$mech->get($url);
	};
	$try = 0;
	while (($@ || !$mech->success()) && $try <= 10) {
		sleep 2;
		$try++;
		eval {
			$mech->get($url);
		};nTaster.pl line 196, near
	}
	if ($@ || !$mech->success()) {
		dieWithGrace("Could not fetch '$url'",$!);
	}
}
else {
	dieWithGrace("Could not extract meta-refresh url from submitted form",$mech->content());
}

## wait for the results page to finish. This can take a very long time !
my $waitidx = 1;
while ($mech->content() =~ m/<meta http-equiv="refresh" content="(\d+);\sURL=([^"]*)"/) {
	$url = $2;
	$sleep = 60; # in url = 3, but I don't want to overload the server.
	$base = $mech->uri();
	if ($base =~ m/(http:\/\/[^\/]*\/).*/) {
		$base = $1;
	}
	else {
		dieWithGrace("Could not extract base url from $base");
	}
	$url = $base.$url;
	print "$waitidx/ : Sleeping $sleep seconds before going to '$url'\n";
	$waitidx++;
	if ($waitidx > 60*6) {
		## waited 6 hours no normal. quit.
		dieWithGrace("MutationTaster query did not finish in 6 hours. Please Check","");
		#goto MTQE;
	}
	sleep($sleep);
	eval {
		$mech->get($url);
	};
	
	$try = 0;
	while (($@ || !$mech->success() )&& $try <= 10) {
		sleep 2;
		$try++;
		eval {
			$mech->get($url);
		};
	}
	if ($@ || !$mech->success()) {
		dieWithGrace("Could not fetch '$url'",$!);
	}

}

## click export as tsv link.
$mech->form_name("main");
$form = $mech->form_name("main");
$i = 1;

# set the tuples field to '' to fetch all variants at once.
$field = "tuples";
$value = '';
if ($form->find_input($field)) {
	eval {
		$mech->field($field,$value)
	};
	if($@){
		
		dieWithGrace("Could not set field : '$field' with '$value'",$@);
		
	}
}
else {
	dieWithGrace("Could not set field : '$field'. Field is missing.",$@);
}
	
# submit (might take a few seconds for complete answer).
$mech->click_button('value' => 'export as TSV');

# check for the link.
if (!$mech->find_link( text => 'retrieve TSV file')) {
	dieWithGrace("Could not download results. Link is missing.",$@);
}
## download!
$base = $mech->uri();
if ($base =~ m/(http:\/\/[^\/]*\/).*/) {
	$base = $1;
}
else {
	dieWithGrace("Could not extract base url from $base");
}

my $link = $mech->find_link( text => 'retrieve TSV file');
my $url = $base.$link->url();
print "Downloading results from '$url'\n";
system("wget '$url' -O 'Output_Files/MutationTaster.Results.txt.zip'");
system("cd Output_Files && unzip MutationTaster.Results.txt.zip && mv export.tsv MutationTaster.Results.txt");

###########
## STORE ##
###########
$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
#open infile
open IN, 'Output_Files/MutationTaster.Results.txt';
my $head = <IN>;
## prepare query
$sth = $dbh->prepare("INSERT INTO `Variants_x_MutationTaster` (vid, score, Effect) VALUES (?,?,?)");
%short = ('disease_causing' => 'D', 'disease_causing_automatic' => 'A', 'polymorphism' => 'N', 'polymorphism_automatic' => 'P');
my %stored;
while (<IN>) {
	chomp($_);
	if ($_ eq '') {
		next;
	}
	elsif ($_ =~ m/MutationTaster predictions/) {
		next;
	}
	my @p = split(/\t/,$_);
	my $vid = $vids{$p[0]."-".$p[1]."-".$p[9]."-".$p[10]}{'vid'};
	if ($vid != '') {
		if ($p[3] =~ m/polymorphism/i) {
			# invert score (similar to annovar).
			$p[5] = 1 - $p[5] ;
		}
		if ($p[3] =~ m/n\/a/i) {
			$p[3] = '.';
			$p[5] = '-1';
		}
		else {
			$p[3] = $short{$p[3]};
		}
		if (!defined($stored{"$vid-".$p[5]."-".$p[3]})) {
			$sth->execute($vid,$p[5],$p[3]);
			$stored{"$vid-".$p[5]."-".$p[3]} = 1;
			$vids{$p[0]."-".$p[1]."-".$p[9]."-".$p[10]}{'ok'} = 1;
		}
		
	}
}
## scan for failed variants and store as such.( not mapped to transcripts, give no result back.)
foreach (keys(%vids)) {
	if ($vids{$_}{'ok'} == 0) {
		$sth->execute($vids{$_}{'vid'},'-1','.');
	}
}
$sth->finish();
## process next batch. 
if ($usemc == 1) {
	clearMemcache("Variants_x_MutationTaster");
}
goto NEXTBATCH;
#exit;

##########
## SUBS ##
##########
sub dieWithGrace {
	my ($message,$error) = @_; 
	
	open OUT, ">dwg.mail.txt";
	print OUT "To: $admin\n";
	print OUT "subject: Mutation Taster Batch Query died\n";
	print OUT "from: $admin\n\n";
	print OUT "Issue:\n";
	print OUT " $message\n";
	print OUT " \n\n $error\n";
	print OUT "\n";
	close OUT;
	## send mail, 
	#system("sendmail -t < 'dwg.mail.txt'");
	print  "####################\n";
	print  "# CRITICAL PROBLEM #\n";
	print  "####################\n";
	print  "\n";
	print  "MTQE died\n";
	print  "Message was:\n";
	print  "$message\n\n";
	print  "$error\n\n";
	print  "\tMail sent to notify admin of crash.\n";
	print  "Monitor will exit now\n";
	exit;
}
sub runQuery {
	if ($usemc == 1) {
		$memd = new Cache::Memcached::Fast { 'servers' => \@mcs, 'compress_threshold' => 1_000_000, };
	}
	my ($query,$table,$var,$type) = @_;
	my %slices;
	if (!defined($var)) {
		$slices{1} = '';
	}
	else {
		## created slices based on VariantID for ranges per 10,000
		foreach(@$var) {
			my $s = int($_/10000);
			if (!defined($slices{$s})) {
				$slices{$s} = ();
			}
			push(@{$slices{$s}},$_);
		}
	}
	my $mcidx = "";
	if ($usemc == 1) {
		$mcidx = getMCidx($table);
	}
	#print "fetching ".keys(%slices)." slices for ".scalar(@$var). " variants<br/>";
	my $result = [];
	if ($query =~ m/ IN \(\s*\?\s*\)/i) {
		$query =~ s/ IN \(\s*\?\s*\)/ BETWEEN ? AND ? /i;
		$exec = 'between';
	}
	elsif ($query =~ m/\?/) {
		$exec = 'in';
	}
	#my $db = "NGS-Variants-hg19";
	$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) ;#or die('Could not connect to database'); ## our : make available to subs
	$dbh->{mysql_auto_reconnect} = 1; 
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 1;
		$i++;
		#print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) ;
	}
	if (!defined($dbh)) {
		 die('Could not connect to database');
	}

	my $sth = $dbh->prepare($query);

	foreach(keys(%slices)) {
		my @vids = @{$slices{$_}};
		my $sstart = $_*10000;
		my $sstop = ($_+1)*10000 - 1;

		#my $qpart = join(",",(($_*10000)..((($_+1)*10000)-1)));
		my $lq = $query;
		my $qpart = '';
		if ($lq =~ m/BETWEEN \? AND \?/i) {
			$lq =~ s/BETWEEN \? AND \?/BETWEEN $sstart AND $sstop/i;
		}
		else {
			$qpart = join(",",($sstart..$sstop)); 
			$lq =~ s/\?/$qpart/;
		}

		#$lq =~ s/\?/$qpart/;
		my $href;
		if ($usemc == 1) {
			$href = $memd->get("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq));
		}
		if (!defined($href) || $usemc == 0) {
			if ($exec eq 'between') {
				my $nr = $sth->execute($sstart,$sstop);
			}
			elsif($exec eq 'in') {
				my $nr = $sth->execute( $qpart );
			}
			else {
				my $nr = $sth->execute();
			}

			#$href = $sth->fetchall_hashref('VariantID');
			$href = $sth->fetchall_arrayref({});
			if ($usemc == 1) {
				## store in memcached
				my $r = $memd->replace("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq),$href);
				if (!$r) {
					$r = $memd->set("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq),$href);
				}
				#if ($r) {
				#	my $ksth = $dbh->prepare("REPLACE INTO `NGS-Variants-Admin`.`Memcache_Keys` (mctable,mckey) VALUES (?,?)");
				#	## success. store key for invalidation.
				#	my @t = split(/:/,$table);
				#	foreach(@t) {
				#		my $ltable = $_;
				#		$ksth->execute($ltable,"ngs:$db:$table:cgi:".md5_hex($lq));
				#		#my $tkeys = $memd->get("keys:ngs:$_");
				#		#if (!defined($tkeys)) {
				#	#		$tkeys = {"ngs:$table:cgi:".md5_hex($lq) => 1};	
				#		#}
				#		#else {
				#		#	$tkeys = decode_json($tkeys); 
				#		#	$tkeys->{"ngs:$table:cgi:".md5_hex($lq)} = 1;
				#		#}
				#		#my $r = $memd->replace("keys:ngs:$_",encode_json($tkeys));
				#		#if (!$r) {
				#		#	$memd->set("keys:ngs:$_",encode_json($tkeys));
				#		#}
				#	}
				#	$ksth->finish();
				#}
			}

		}
		push(@$result,@$href);
		## add slice results to full results aref.
		#if (!defined($var)) {
		#	push(@$result,$href->{$_}) for keys (%$href);
		#}
		#else {
		#	my %needed;
		#	## intersect href with items in var (if any)
		#	@needed{@vids} = @{$href}{@vids};
		#	push(@$result,$needed{$_}) for keys(%needed);
		#	#print "keys needed: ".keys(%needed)."<br/>"; 
		#}
	}
	$sth->finish();
	$dbh->disconnect();
	## select the actually needed items.
        if (defined($var)) {
                my $t = time;
                my %needed = map {$_ => 1} @$var;
                my @r;
                my $to = scalar(@$var);
                my $from = scalar(@$result);
                foreach(@$result) {
                        if (defined($needed{$_->{'VariantID'}})) {
                                push(@r,$_);
                        }
                }
                $result = \@r;
                #if ($uid == 1) {print "$table from $from to $to variants in ".(time - $t)."s<br/>";}
        }

	return($result);
}

=cut
sub clearMemcache {
	
	## READ CREDENTIALS  
	$credpath = abs_path("../../.LoadCredentials.pl");
	require($credpath) ;
	if ($usemc != 1) {
		return;
	}
	###########################
	# CONNECT TO THE DATABASE #
	###########################
	my $db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;

	# memcached : Make sure module is available if this is set in .credentials
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	#use JSON;
	my $memd = new Cache::Memcached::Fast { 'servers' => \@mcs,  'compress_threshold' => 1_000_000, };
	my ($tables) = @_;
	my $sth = $dbh->prepare("SELECT mckey FROM `Memcache_Keys` WHERE mctable = ?");
	my @t = split(/:/,$tables);
	foreach(@t) {
		my $tname = $_;
		$sth->execute($tname);
		my $rowcache = $sth->fetchall_arrayref();
		$dbh->do("DELETE  FROM `Memcache_Keys` WHERE mctable = '$tname'");
		#$tkeys = $memd->get("keys:ngs:$tname");
		#if (defined($tkeys)) {
		foreach my $row (@$rowcache) {
			my $tk = $row->[0];	

			## allow tries 
			my $t = 0;
			my $r = $memd->delete($tk);
			while (!defined($r) && $t < 100 ) {
				$t++;
				$r = $memd->delete($tk);
			}
				
		}
		#else {
		#	print "key 'keys:ngs:$tname' not found\n";
	#	}
	}
	$sth->finish();
	$dbh->disconnect();

	$memd->disconnect_all;
}
=cut
