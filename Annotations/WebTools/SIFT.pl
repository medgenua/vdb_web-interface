#!/usr/bin/perl

## only start if the Mechanize is available.
eval
{
  my $toload = "WWW//Mechanize.pm";
  require $toload;
  $toload->import();
};
if($@)
{
	dieWithGrace("Module WWW::Mechanize is not available. MutationTaster Scores will not be fetched.");
}

# other modules
use DBI;
use Getopt::Std;
use Cwd 'abs_path';

# load credentials
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);
if ($usemc == 1) {
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	my $mcpath = abs_path("../../cgi-bin/inc_memcache.pl");
	require($mcpath);	
}


# disable buffer
$|++;

####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_; 
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";

###############################
## Do not run double process ##
###############################
if (!-e 'SIFT.Status.txt') {
	$status = 0;
	system("echo $status > SIFT.Status.txt");
}
else {
	my $status = `cat SIFT.Status.txt`;
	chomp($status);
	while ($status == 1) {
		## is it really running?
		my $ps = `ps auxwwwe | grep 'perl SIFT.pl' | grep -v grep | wc -l`;
		chomp($ps);
		if ($ps == 1) {
			## not running. update status. (one hit because the current process is present.)
			system("echo 0 > SIFT.Status.txt");
		}
		else {
			## sleep.
			sleep 60;
		}
		$status = `cat SIFT.Status.txt`;
		chomp($status);
	}
}

## ok, ready to start : set status.
$status = 1;
system("echo $status > SIFT.Status.txt");
###########################
# CONNECT TO THE DATABASE #
###########################
## GET current/provided build
# opts
# n : (n)ew variants (missing mutation taster scores)
# u : (u)pdate scores older than 6 months (future work)
# d : debug
getopts('nudb:', \%opts) ;

our $db = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
}

$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

## load grantham scores.
open IN, "Grantham.txt";
my %grantham = ();
while (<IN>) {
	chomp($_);
	my ($from,$to,$score) = split(/\t/,$_);
	$grantham{$from}{$to} = $score;
}
close IN;

if (!defined($opts{'n'}) && !defined($opts{'u'})) {
	$opts{'n'} = '';
}
our $anno = 'SIFT';
our $file = "Input_Files/$anno.list.txt";


## label : comes here after data from current query are stored, untill nrrows == 0
our %vids;
NEXTBATCH:
%vids = ();


## GET VARIANTS
if (defined($opts{'n'})) {
	#$query = "SELECT v.chr, v.start, v.RefAllele, v.AltAllele, v.id FROM `Variants` v LEFT JOIN `Variants_x_SIFT` va ON v.id = va.vid WHERE va.vid IS NULL LIMIT 150000";
	## all variants
	$query = "SELECT id FROM `Variants`";
	my $allvariants = $dbh->selectcol_arrayref($query);
	## wait for the number of result to stabilize (in case of multiple inserts, prevents running to many small queries.
	my $prevnr = scalar(@$allvariants);
	sleep 60;
	$allvariants = $dbh->selectcol_arrayref($query);
	my $nr = scalar(@$allvariants);
	while ($nr > $prevnr) {
		sleep 60;
		$prevnr = $nr;
		$allvariants = $dbh->selectcol_arrayref($query);
		$nr = scalar(@$allvariants)
	}
	## variants in annotation table
	$query = "SELECT vid FROM `Variants_x_SIFT`";
	my $annovariants = $dbh->selectcol_arrayref($query);
	## array difference
	my %second = map {$_=>1} @$annovariants;
	my @toannotate = grep {!$second{$_}} @$allvariants;
	# limit to 15000 variants per web-batch
	@toannotate = splice(@toannotate,0,15000);
	if (scalar(@toannotate) == 0) {
		print "No variants to update.  Exiting\n";
		#close OUT;
	}
	# clear up variables
	$annovariants = [];
	$allvariants = [];
	%second = ();
	open OUT, ">$file";
	if (scalar(@toannotate) == 0) {
		print "No variants to update. Exiting\n";
		## first clean 
		close OUT;
		system("rm -f $file");
		system("rm -f Output_Files/SIFT.Results.txt");
		## clear status.
		$status = 0;
		system("echo $status > SIFT.Status.txt");
		exit();
	}
	#while (my $var = join(",",splice(@toannotate,0,500))) {
		$query = "SELECT v.chr, v.start, v.RefAllele, v.AltAllele, v.id FROM `Variants` v WHERE v.id IN ($var)";
		$query = "SELECT v.chr, v.start, v.stop, v.RefAllele, v.AltAllele, v.id AS VariantID FROM `Variants` v WHERE v.id IN (?)";
		$rowcache = runQuery($query,"Variants",\@toannotate);

		#$sth = $dbh->prepare($query);
		#my $nrrows = $sth->execute();
		#$max = $nrrows;
		#my $rowcache;
		foreach my $result (@$rowcache)  {
			$result->{'chr'} = $chromhash{$result->{'chr'}};
			## SIFT INPUT DEL = AACC,. 
			if (length($result->{'RefAllele'}) > length($result->{'AltAllele'})) {
				# deletion.
				# position
				$result->{'start'} = $result->{'start'} + 1; 
				# ref : 
				$result->{'RefAllele'} = substr($result->{'RefAllele'},1); # from second pos to end.
				# alt:
				$result->{'AltAllele'} = '.';		
			}
			## SIFT INPUT INS = DEFAULT VCF: G,GCCA
			my $string = $result->{'chr'}.",".$result->{'start'}.",".$result->{'RefAllele'}.",".$result->{'AltAllele'}.",".$result->{'VariantID'}."\n";
			## store vid based on pos & alleles (chr is numeric in MTQE output. 1-25)
			#$vids{$chromhash{$result->[0]}."-".$result->[1]."-".$result->[2]."-".$result->[3]}{'vid'} = $result->[4];
			#$vids{$chromhash{$result->[0]}."-".$result->[1]."-".$result->[2]."-".$result->[3]}{'ok'} = 0;
			print OUT "$string";
		}
		#$sth->finish();
	#}
	
	
}
else {
	exit;
}
close OUT;
## check for empty input
my $wc = `cat $file | wc -l`;
chomp($wc);
if ($wc == 0) {
	exit;
}

## label : comes here after data from current query are stored, untill nrrows == 0
#our %vids;
#NEXTBATCH:
#open OUT, ">$file";
#print OUT "##fileformat=VCFv4.1\n";
#print OUT "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t1\n";

#$sth = $dbh->prepare($query);
#my $nrrows = $sth->execute();

## wait for the number of result to stabilize (in case of multiple inserts, prevents running to many small queries.
#my $prevnr = $nrrows;
#sleep 60;
#$nrrows = $sth->execute();
#while ($nrrows > $prevnr) {
#	sleep 60;
#	$prevnr = $nrrows;
#	$nrrows = $sth->execute();
#}
##
#if ($nrrows < 10000) {
#	$max = $nrrows;
#}
#else {
#	$max = 10000;
#}#

#my $rowcache;
#if ($nrrows == 0) {
#	print "No variants to update. Exiting\n";
##	## first clean 
#	close OUT;
#	system("rm -f $file");
#	system("rm -f Output_Files/SIFT.Results.txt");
#	## clear status.
#	$status = 0;
#	system("echo $status > SIFT.Status.txt");
#	exit();
#}
#%vids = ();
#print "Fetching Variants to update from database\n";
#while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
#	$result->[0] = $chromhash{$result->[0]};
#	## SIFT INPUT DEL = AACC,. 
#	if (length($result->[2]) > length($result->[3])) {
#		# deletion.
#		# position
#		$result->[1] = $result->[1] + 1; 
#		# ref : 
#		$result->[2] = substr($result->[2],1); # from second pos to end.
#		# alt:
#		$result->[3] = '.';		
#	}
#	## SIFT INPUT INS = DEFAULT VCF: G,GCCA
#	my $string = $result->[0].",".$result->[1].",".$result->[2].",".$result->[3].",".$result->[4]."\n";
#	## store vid based on pos & alleles (chr is numeric in MTQE output. 1-25)
#	#$vids{$chromhash{$result->[0]}."-".$result->[1]."-".$result->[2]."-".$result->[3]}{'vid'} = $result->[4];
#	#$vids{$chromhash{$result->[0]}."-".$result->[1]."-".$result->[2]."-".$result->[3]}{'ok'} = 0;
#	print OUT "$string";
#
#}#
#$sth->finish();
#close OUT;
## disconnect from db. 
$dbh->disconnect();
print "Connecting to SIFT Website\n";
# skip
#goto STORE;
# come here if fetching results takes abnormally long.
SIFT:
############################
## Connect To QueryEngine ##
############################
# create object
my $mech = WWW::Mechanize->new();

my $url = "http://provean.jcvi.org/genome_submit.php";

# fetch url
$mech->get( $url) 
	or dieWithGrace("Could not fetch SIFT/PROVEAN Page",$!);
if (defined($opts{'d'})) {
	print $mech->content();
}
# select the correct field
$mech->form_name('chrForm') 
	or dieWithGrace("Form 'chrForm' not found on SIFT Page",$!);

# set the field values.
my %fields = ('CHR_file'  => $file,
		'email' => $adminemail);
# fill in form and check if fields exist.
my $form = $mech->form_name('chrForm');
foreach (keys(%fields)) {
	$field = $_;
	$value = $fields{$field};
	if ($form->find_input($field)) {
		eval {
			$mech->field($field,$value)
		};
		if($@){
			#if($@ =~ /No such field/i){
			#	dieWithGrace("Could not set field : 'name'. Field is missing.",$@);
			#}
			#else {
				dieWithGrace("Could not set field : '$field' with '$value'",$@);
			#}
		}
	}
	else {
		dieWithGrace("Could not set field : '$field'. Field is missing.",$@);
	}
}	
if (defined($opts{'d'})) {
	use Data::Dumper;
	print Dumper($mech->form_name('chrForm'));
}
my @submits = $mech->find_all_inputs(type => 'submit');
if (scalar(@submits) != 1) {
	dieWithGrace("Submission form changed. Zero or multiple submit buttons present.",'');
}
# submit.
my $submit = $mech->current_form()->find_input( undef,'submit');
$mech->click_button(input => $submit);
if (!$mech->success()) {
	dieWithGrace("Submission failed (clicking the actual button)","");
}
# base url of new page (other server!)
$base = $mech->uri();
$url = $mech->uri();
$url =~ m/.*genome_report_2\.php\?jobid=(\d+)$/;
our $jobid = $1;
if ($base =~ m/(http:\/\/[^\/]*\/).*/) {
	$base = $1;
}
else {
	dieWithGrace("Could not extract base url from $base");
}
sleep 3;
# Page refreshes until a table is present  with the output link (javascript based, no meta refresh).
eval {
	$mech->get($url);
};
$try = 0;
while (($@ || !$mech->success()) && $try <= 10) {
	sleep 2;
	$try++;
	eval {
		$mech->get($url);
	};
}
if ($@ || !$mech->success()) {
	dieWithGrace("Could not fetch '$url'",$!);
}

## wait for the results page to finish. This can take a very long time !
my $waitidx = 1;
while ($mech->content() !~ m/serve_file\.php\?VAR=g$jobid\/$jobid\.result\.tsv/) {
	if ($mech->content() =~ m/You have submitted more than 100 jobs in this 24 hour period/) {
		print "Job limit reached. Suspending for 24h!\n";
		sleep 60*60*24;
		goto SIFT;
	}
	$sleep = 60; # in url = 10, but I don't want to overload the server.
	if ($mech->content() =~ m/genome_prg\.php/){
		dieWithGrace('Issue with SIFT Service. Reason unknown...','');
	}
	print "$waitidx/ : Sleeping $sleep seconds before going to '$url'\n";
	$waitidx++;
	 ## give it 24 hours.
	if ($waitidx > 60*24) {
		# still not done, die..
		dieWithGrace("SIFT job not done after 24 hours...","");
	}
	sleep($sleep);
	eval {
		$mech->get($url);
	};
	
	$try = 0;
	while (($@ || !$mech->success() )&& $try <= 10) {
		sleep 2;
		$try++;
		eval {
			$mech->get($url);
		};
	}
	if ($@ || !$mech->success()) {
		dieWithGrace("Could not fetch '$url'",$!);
	}

}

## download!
$base = $mech->uri();
if ($base =~ m/(http:\/\/[^\/]*\/).*/) {
	$base = $1;
}
else {
	dieWithGrace("Could not extract base url from $base");
}

#my $link = $mech->find_link( url => "serve_file.php?VAR=g$jobid/$jobid.result.tsv");
#my $url = $base.$link->url();
my $url = $base."serve_file.php?VAR=g$jobid/$jobid.result.tsv";
if ($mech->content() =~ m/serve_file\.php\?VAR=g$jobid\/$jobid\.result\.tsv\.gz/) {
	$url .= ".gz";
	print "Downloading results from '$url'\n";
	system("wget '$url' -O 'Output_Files/SIFT.Results.txt.gz' && cd Output_Files/ && gunzip -f SIFT.Results.txt.gz");

}
else {
	print "Downloading results from '$url'\n";
	system("wget '$url' -O 'Output_Files/SIFT.Results.txt'");
}
###########
## STORE ##
###########
STORE:
$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
#open infile
open IN, 'Output_Files/SIFT.Results.txt';
my $head = <IN>;
## prepare query
$sth = $dbh->prepare("INSERT INTO `Variants_x_SIFT` (vid, SIFTScore, SIFTEffect,PROVEANScore,PROVEANEffect,ensp,plen,ppos,resref,resalt,type,grantham) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
%short = ('.' => '.','Deleterious' => 'D', 'Damaging' => 'D', 'Tolerated' => 'T', 'Neutral' => 'N');
#my %stored;
while (<IN>) {
	chomp($_);
	if ($_ eq '') {
		next;
	}
	my @p = split(/\t/,$_);
	my @s = split(/,/,$p[1]);
	my $vid = $s[-1];
	if ($p[2] eq 'record not found' || $p[2] eq 'overlap with intron region' || $p[2] eq 'input format error' ) {
		$sth->execute($vid,'-1','.','0','.','.','.','.','.','.','.','.');
		next;
	}
	if ($p[2] eq 'reference nucleotide not matched') {
		print "WARNING, Reference nucleotide did not match !\n";
		$sth->execute($vid,'-1','.','0','.','.','.','.','.','.','.','.');
		next;
	}
	
	if ($p[9] eq 'Unknown') {
		$sth->execute($vid,'-1','.','0','.','.','.','.','.','.',$p[9],'.');
		next;
	}
	if (defined($short{$p[15]})) {
		$p[15] = $short{$p[15]};
	}
	if (defined($short{$p[11]})) {
		$p[11] = $short{$p[11]};
	}
	## get grantham score (todo).
	if (defined($grantham{$p[7]}{$p[8]})) {
		$gt = $grantham{$p[7]}{$p[8]};
	}
	else {
		$gt = '.';
	}
	## set frameshift and nonsense to custom effect and score.
	if ($p[9] eq 'Nonsense' || $p[9] eq 'Frameshift') {
		$p[14] = 0;
		$p[10] = -99;
		$p[15] = '*';
		$p[11] = '*';
	}
	## NA SIFT scores => predication failed, set to missing
	if ($p[14] eq 'NA' || $p[14] eq '') {
		$p[14] = -1;
		$p[15] = '.';
	}
	else {
		## SIFT invert ala ANNOVAR
		$p[14] = 1 - $p[14] ;
	}
	## NA PROVEAN scores => predication failed, set to missing
	if ($p[11] eq 'NA' || $p[11] eq '') {
		$p[11] = '.';
		$p[10] = '0';
	}
	#(vid, SIFTScore, SIFTEffect,PROVEANScore,PROVEANEffect,ensp,plen,ppos,resref,resalt,type,grantham)
	for ($i = 6; $i <= 8; $i++) {
		if ($p[$i] eq '') {
			$p[$i] = '.';
		}
	}
	$sth->execute($vid,$p[14],$p[15],$p[10],$p[11],$p[2],$p[3],$p[6],$p[7],$p[8],$p[9],$gt);
}
close IN;
$sth->finish();
## process next batch. 
if ($usemc == 1) {
	clearMemcache("Variants_x_SIFT");
}
goto NEXTBATCH;
#exit;

##########
## SUBS ##
##########
sub runQuery {
	if ($usemc == 1) {
		$memd = new Cache::Memcached::Fast { 'servers' => \@mcs,  'compress_threshold' => 1_000_000, };
	}
	my ($query,$table,$var,$type) = @_;
	my %slices;
	if (!defined($var)) {
		$slices{1} = '';
	}
	else {
		## created slices based on VariantID for ranges per 10,000
		foreach(@$var) {
			my $s = int($_/10000);
			if (!defined($slices{$s})) {
				$slices{$s} = ();
			}
			push(@{$slices{$s}},$_);
		}
	}
	my $mcidx = '';
	if ($usemc == 1) {
		$mcidx = getMCidx($table);
	}
	#print "fetching ".keys(%slices)." slices for ".scalar(@$var). " variants<br/>";
	my $result = [];
	if ($query =~ m/ IN \(\s*\?\s*\)/i) {
		$query =~ s/ IN \(\s*\?\s*\)/ BETWEEN ? AND ? /i;
		$exec = 'between';
	}
	elsif ($query =~ m/\?/) {
		$exec = 'in';
	}
	$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) ;#or die('Could not connect to database'); ## our : make available to subs
	$dbh->{mysql_auto_reconnect} = 1; 
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 1;
		$i++;
		#print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) ;
	}
	if (!defined($dbh)) {
		 die('Could not connect to database');
	}

	my $sth = $dbh->prepare($query);
	foreach(keys(%slices)) {
		my @vids = @{$slices{$_}};
		my $sstart = $_*10000;
		my $sstop = ($_+1)*10000 - 1;

		#my $qpart = join(",",(($_*10000)..((($_+1)*10000)-1)));
		my $lq = $query;
		my $qpart = '';
		if ($lq =~ m/BETWEEN \? AND \?/i) {
			$lq =~ s/BETWEEN \? AND \?/BETWEEN $sstart AND $sstop/i;
		}
		else {
			$qpart = join(",",($sstart..$sstop)); 
			$lq =~ s/\?/$qpart/;
		}
		my $href;
		if ($usemc == 1) {
			$href = $memd->get("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq));
		}
		if (!defined($href) || $usemc == 0) {
			if ($exec eq 'between') {
				my $nr = $sth->execute($sstart,$sstop);
			}
			elsif($exec eq 'in') {
				my $nr = $sth->execute( $qpart );
			}
			else {
				my $nr = $sth->execute();
			}


			$href = $sth->fetchall_arrayref({});
			if ($usemc == 1) {
				## store in memcached
				my $r = $memd->replace("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq),$href);
				if (!$r) {
					$r = $memd->set("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq),$href);
				}
				#if ($r) {
				#	## success. store key for invalidation.
				#	my @t = split(/:/,$table);
				#	my $ksth = $dbh->prepare("REPLACE INTO `NGS-Variants-Admin`.`Memcache_Keys` (mctable,mckey) VALUES (?,?)");
				#	foreach(@t) {
				#		my $ltable = $_;
				#		$ksth->execute($ltable,"ngs:$db:$table:cgi:".md5_hex($lq));
				#	}
				#}
			}
		}
		push(@$result,@$href);
	}
	$sth->finish();
	$dbh->disconnect();
	## select the actually needed items.
        if (defined($var)) {
                my %needed = map {$_ => 1} @$var;
                my @r;
                foreach(@$result) {
                        if (defined($needed{$_->{'VariantID'}})) {
                                push(@r,$_);
                        }
                }
                $result = \@r;
        }

	return($result);
}

sub dieWithGrace {
	my ($message,$error) = @_; 
	
	open OUT, ">dwg.mail.txt";
	print OUT "To: $admin\n";
	print OUT "subject: Mutation Taster Batch Query died\n";
	print OUT "from: $admin\n\n";
	print OUT "Issue:\n";
	print OUT " $message\n";
	print OUT " \n\n $error\n";
	print OUT "\n";
	close OUT;
	## send mail, 
	#system("sendmail -t < 'dwg.mail.txt'");
	print  "####################\n";
	print  "# CRITICAL PROBLEM #\n";
	print  "####################\n";
	print  "\n";
	print  "MTQE died\n";
	print  "Message was:\n";
	print  "$message\n\n";
	print  "$error\n\n";
	print  "\tMail sent to notify admin of crash.\n";
	print  "Monitor will exit now\n";
	exit;
}
=cut
sub clearMemcache {
	
	## READ CREDENTIALS  
	$credpath = abs_path("../../.LoadCredentials.pl");
	require($credpath) ;
	if ($usemc != 1) {
		return;
	}
	###########################
	# CONNECT TO THE DATABASE #
	###########################
	my $db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;

	# memcached : Make sure module is available if this is set in .credentials
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	#use JSON;
	my $memd = new Cache::Memcached::Fast { 'servers' => \@mcs,  'compress_threshold' => 1_000_000, };
	my ($tables) = @_;
	my $sth = $dbh->prepare("SELECT mckey FROM `Memcache_Keys` WHERE mctable = ?");
	my @t = split(/:/,$tables);
	foreach(@t) {
		my $tname = $_;
		$sth->execute($tname);
		my $rowcache = $sth->fetchall_arrayref();
		$dbh->do("DELETE FROM `Memcache_Keys` WHERE mctable = '$tname'");

		#if (defined($tkeys)) {
		foreach my $row (@$rowcache) {
			my $tk = $row->[0];	

				my $t = 0;
				my $r = $memd->delete($tk);
				while (!defined($r) && $t < 100 ) {
					$t++;
					$r = $memd->delete($tk);
				}
		}
	}
	$sth->finish();
	$dbh->disconnect();

	$memd->disconnect_all;
}
=cut

