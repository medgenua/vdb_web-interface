#!/usr/bin/perl


$|++;
use DBI;
#use Getopt::Std;
use XML::Simple;
use Cwd 'abs_path';
$credpath = abs_path("../.LoadCredentials.pl");
require($credpath);
use Cwd;
# for mailing
use Net::Domain qw/hostfqdn/;
use MIME::Lite;


my $dir = getcwd;

my $date = `date`;
print "Starting time: $date";

###########################
# CONNECT TO THE DATABASE #
###########################
## GET current/provided build
my $db = "NGS-Variants-Admin";
$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
$gsth->execute();
my ($cb) = $gsth->fetchrow_array();
$gsth->finish();
$db = "NGS-Variants$cb";
$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;

## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
## get build replacement values
my %builds = ();
my $sth = $dbh->prepare("SELECT Annotation, Value FROM `Annotation_DataValues`");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$builds{$r[0]} = $r[1];
}
$sth->finish();

if ($usehpc == 1) {
	if ($queue eq '' || !defined($queue)) {
		$queue = 'batch';
	}
	if (defined($account) && $account ne '') {
		$account = "-A $account";
	}
}

## GET FILES THAT NEED ANNOTATION
$date = `date`;
chomp($date);
$sth = $dbh->prepare("SELECT id FROM `Samples` WHERE AnnotationStatus = 'Pending'");
$sth->execute();
my @samplerow = ();
while (my @row = $sth->fetchrow_array()) {
	push(@samplerow, $row[0]);
	$dbh->do("UPDATE `Samples` SET AnnotationStatus = 'Started on $date' WHERE id = '$row[0]'");
}
$sth->finish();
$dbh->disconnect(); 

####################################
## READ THE ANNOTATION CONFIG XML ##
####################################
my $xml = new XML::Simple;
# this xml file contains all the parameter/method settings.
my $settings = $xml->XMLin("Config.xml");
my %queued;
## DEPENDENCIES
my %delayed = ();
my %depends = ();
my %jobids = ();
$depends{"GO_Annotations"} = $builds{'snpEff'}; #"GRCh37.66"  ; # from snpEff
$depends{"GenePanels"} = "refgene"; # from annovar.

@subdirs = keys(%{$settings});
$firstloop = 1;
# process
#my $previd = 0;
our %jobstring = ();
our %stat_files = ();

PROCESS:
if ($usehpc != 1) {
	system("echo 1 > global.update.running.txt");
}
foreach my $subdir (@subdirs) {
	$subdir =~ s/^_//;
	## depends and not yet run => delay
	my $dependid = '';
	if (defined($depends{$subdir}) ){
		if (!defined($delayed{$subdir}) ) { 
			$delayed{$subdir} = 1;
			next;
		}
		else {
			$dependid = $jobids{$depends{$subdir}};
			print "Delayed submission of $subdir. Depends on job $dependid\n";
			delete($delayed{$subdir});
		}
	}
	## webtools are run locally and only in first loop (do not rerun if processing dependencies).
	if ($subdir eq 'WebTools' && $firstloop == 1) {
		print "Launching WebTool Updaters locally.\n";
		foreach my $anno (keys(%{$settings->{$subdir}})) {
			if (!defined($settings->{$subdir}->{$anno}->{'Script'})) {
				print "No 'Script' , skipping $anno\n";
				# skip
				next;
			}
			elsif ($settings->{$subdir}->{$anno}->{'Script'} eq ''){
				print "'Script' empty , Running perl $anno.pl\n";
				## check if already running.
				my $pid_line = `ps aux  | grep '$anno.pl' | grep -v 'grep'`;
				chomp($pid_line);
				if ($pid_line ne '') {
					print "  => $anno.pl is running. skipped\n";
					next;
				}
				system("cd WebTools && nohup perl $anno.pl > ../job_output/$anno.o.txt 2>&1 &");
				sleep 15;
				
			}
			else {
				my $script = $settings->{$subdir}->{$anno}->{'Script'};
				print "'Script' specified , Running perl $script.pl\n";
				my $pid_line = `ps aux  | grep '$script.pl' | grep -v 'grep'`;
				chomp($pid_line);
				if ($pid_line ne '') {
					print "  => $script.pl is running. skipped\n";
					next;
				}
				system("cd WebTools && nohup perl $script.pl > ../job_output/$script.o.txt 2>&1 &");
				sleep 15;
			}
		}
	}
	else {
		print "Processing : $subdir\n";
	}
	NEXTDIR: foreach my $anno (keys(%{$settings->{$subdir}})) {
		## skip if deprecated
		if (exists($settings->{$subdir}->{$anno}->{deprecated})) {
			next;
		}
		## get AnnotationSource
		if (!defined($settings->{$subdir}->{$anno}->{AnnotationSource})) {
			print "No Annotation Source available for $subdir/$anno. Skipping\n";
			next;
		}
		$annosource = $settings->{$subdir}->{$anno}->{AnnotationSource};
		if ($annosource eq '%build%') {
			$annosource = $builds{$subdir};
		}
		if (defined($queued{"$subdir-$annosource"})) {
			#print "$subdir/$annosource is already queued from ".$queued{"$subdir-$annosource"}.". Skipping for $subdir/$anno\n";
			next;
		}
		if ($annosource eq 'none') {
			print "AnnotationSource for $subdir/$anno == 'none'. Skipping\n";
			$queued{"$subdir-$annosource"} = "$subdir/$anno";
			next;
		}
		my $afterok = '';
		## if in the queue AND NOT running yet => skip
		if ($usehpc == 1) {
			# TODO : this has clashes between the dev/main instances
			my $grep = "Ann.".substr($annosource,0,12);
=cut
			my $out = `qstat -a | grep '$grep'`;
			my @lines = split(/\n/,$out);
			#if ($previd != 0) {
			#	$afterok =  " -W depend=afterok:$previd";
			#}
			foreach(@lines) {
				chomp;
				my @p = split(/\s+/,$_);
				#if (($grep eq 'Ann.1000g2014oct' || $grep eq 'Ann.1000g2012apr' || $grep eq 'Ann.esp6500siv2_') && ($p[9] eq 'Q' || $p[9] eq 'H' || $p[9] eq 'R' )) {
				if ($grep =~ m/1000g||esp6500/ && ($p[9] eq 'Q' || $p[9] eq 'H' || $p[9] eq 'R' )) {
				
					## 1000g / esp6500 grep string is not specific, just make them dependent on eachother. 
					$p[0] =~ m/(\d+)(\..*)/;
					if ($afterok eq '') {
						$afterok = " -W depend=afterok:$1";
					}
					else {
						$afterok .= ":$1";
					}
				}
 
				elsif ($p[9] eq 'Q' || $p[9] eq 'H') {
					print "$subdir/$annosource is still in the job queue. Skipping for $subdir/$anno\n";
					$p[0] =~ m/(\d+)\..*/;
					my $dpid = $1;
					if ($dpid ne '') {
						#print "ADDED $dpid to jobstring from queue\n";
						$jobstring{$dpid} = 1;
					}
					$queued{"$subdir-$annosource"} = "$subdir/$anno";
					next NEXTDIR;
				}
				elsif ($p[9] eq 'R') {
					## similar annotation job is running => set this one to execute only after the other is finished.
					$p[0] =~ m/(\d+)(\..*)/;
					if ($afterok eq '') {
						$afterok = " -W depend=afterok:$1";
					}
					else {
						$afterok .= ":$1";
					}
				}
			}
			## queue. 
			if ($dependid ne '') {
				if ($afterok eq '') {
					$afterok = " -W depend=afterok:$dependid";
				}
				else {
					$afterok .= ":$dependid";
				}
			}
=cut
		}
		## create the run script.
		$dir =~ s/\/$//;
		print "\t=> Updating $subdir/$annosource\n";
		# this can be simplified by putting all to a high enough amount.. evaluate how WGS will perform.
		my $mem = '24gb';
		my $ppn = 1;
		if ($annosource =~ m/GRCh/) {
			$mem = '24gb'; # snpEff needs lots of memory for annotation.
			$ppn = '4'; # runs multithreaded by default under java-7 (full node?). Request enough to make sure.
		}
		elsif ($annosource =~ m/ClinVar/) {
			$mem = '24g';
		}
		elsif ($annosource =~ m/CADD|spidex/i) {
			$mem = '24gb';
		}
		my ($stat_file,$job_file) = &WriteJobFile($dir,$subdir,$annosource,$ppn,$mem,$afterok);
		if ($usehpc == 1) {	
			my $jobid = 0;
			$jobid = `qsub $job_file`;
			#print "JOBID: $jobid";
			chomp($jobid);
			$jobid =~ m/(\d+)\..*/;
			$jobid = $1;
			#print "  => '$jobid'\n";
			if ($jobid eq '') {
				Critical("Job not submitted correctly",$dir,$subdir,$annosource);
			}
			$jobstring{$jobid} = 1;
			$jobids{$annosource} = $jobid;
			$stat_files{$stat_file} = 0;
		}
		else {
			
			my $command = "cd $dir/$subdir && bash '$job_file'";
			system("$command > $dir/job_output/$annosource.o.txt 2>$dir/job_output/$annosource.e.txt");
			$stat_files{$stat_file} = 0
		}
		$queued{"$subdir-$annosource"} = "$subdir/$anno";	
	}
}

if (keys(%delayed) > 0) {
	@subdirs = keys(%delayed);		
	print scalar(@subdirs) . " annotations were delayed.\n";
	$firstloop = 0;
	goto PROCESS;
}
## Wait for jobs to finish.
my $time = time();
my $statusfile = "$dir/qsub_scripts/Status.$time.txt";

## Process status files.
my $all_done = 0;
while ($all_done == 0) {
	$all_done = 1;
	foreach my $stat_file (keys(%stat_files)) {
		# already finished.
		next if ($stat_files{$stat_file} == 1);
		# not existing yet.
		if (!-f $stat_file) {
			$all_done = 0;
			next;
		}
		# contents
		open IN, $stat_file or Critical("Could not open status file for reading",$stat_file);
		my $line = <IN>;
		close IN;
		chomp($line);
		if ($line eq 'OK') {
			$stat_files{$stat_file} = 1;
			next;
		}
		else {
			Critical("Error Listed in status file : $line",$stat_file);
		}

	}
	sleep 30;

}
if ($usehpc != 1) {
	system("echo 0 > global.update.running.txt");
}
print "Annotations are ready.\n";
=cut
if(keys(%jobstring) > 0) {
	$jobstr = join(":",sort{ $a <=> $b } keys(%jobstring));
	#$jobstring = substr($jobstring,0,-1);
	my $time = time();
	my $jobfile = "$dir/qsub_scripts/WaitForAnnotations.$time.sh";
	open OUT, ">$jobfile";
	print OUT "#!/usr/bin/env bash\n";
	print OUT "#PBS -d $dir/\n";
	print OUT "#PBS -W depend=afterok:$jobstr\n";
	print OUT "#PBS -N Ann.Waiting\n";
	print OUT "#PBS -o $dir/job_output/WaitForAnnotations.$time.o.txt\n";
	print OUT "#PBS -e $dir/job_output/WaitForAnnotations.$time.e.txt\n";
	print OUT "#PBS -l nodes=1:ppn=1,mem=500m\n";
	print OUT "#PBS -m a\n";
	print OUT "echo 1 > $statusfile\n";
	#print OUT "rm $jobfile\n";
	close OUT;
	system("qsub $jobfile");
	while (!-e "$statusfile") {
		sleep 60;
	}
	system("rm $statusfile");
}
=cut
## ok, now update the database.
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
$date = `date`;
chomp($date);
foreach(@samplerow) {
	#print "UPDATE `Samples` SET AnnotationStatus = 'Finished on $date' WHERE id = '$_'\n";
	$dbh->do("UPDATE `Samples` SET AnnotationStatus = 'Finished on $date' WHERE id = '$_'");
}
# invalidate memcache for samples table.
$dbh->do("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` = 'Samples'");
# invalidate VIEWS.
my $query = "SELECT `name` FROM `VIEWS` WHERE 1";
$sth = $dbh->prepare($query);
my $nr = $sth->execute();
while (my @row = $sth->fetchrow_array()) {
	$dbh->do("DELETE FROM `VIEWS` WHERE `name` = '$row[0]'");
	$dbh->do("DROP TABLE IF EXISTS `$row[0]`");
}
$sth->finish();
$dbh->disconnect();
# exit;
exit;



sub WriteJobFile() {
	my ($dir,$subdir,$annosource, $ppn,$mem,$afterok) = @_;
	my $time = time();
	# open job file
	my $job_file = "$dir/qsub_scripts/$annosource.$time.sh";
	my $stat_file = "$dir/qsub_scripts/$annosource.$time.status";
	open OUT, ">$job_file";
	# PBS headers
	print OUT "#!/usr/bin/env bash\n";
	print OUT "#PBS -d $dir/$subdir\n";
	if ($afterok ne '') {
		print OUT "#PBS $afterok\n";
	}
	print OUT "#PBS -N Ann.$annosource\n";
	print OUT "#PBS -o $dir/job_output/$annosource.o.\$PBS_JOBID\n";
	print OUT "#PBS -e $dir/job_output/$annosource.e.\$PBS_JOBID\n";
	print OUT "#PBS -l nodes=1:ppn=$ppn,mem=$mem\n";
	print OUT "#PBS -m a\n";
	print OUT "#PBS -M $adminemail\n"; 
	print OUT "\n\n";
	## add error handling.
	print OUT <<ERROR;
	# retry variable, set to 1 on resubmission
	retry=0
	function SubmitJob {
	  jobScript=\${1:-}
	  if [ ! -f \$jobScript  ]; then
	       echo "\$jobScript : qsub script not found" >> $stat_file
	       echo "\$jobScript " >> $stat_file
	       exit 1
	  fi
	  JID=`qsub \$jobScript`
	  while  [[ ! \$JID =~ ^[[:digit:]] ]] ; do
	    sleep 60
	    tmpFile=\$(mktemp)
	    TRASH=`qstat -x 2>\$tmpFile | grep \$jobScript`
	    # can be non-zero on grep-not-found and qstat-failure. only resumbit on valid qstat check.
	    if  [[ "\$?" -ne "0" ]] ; then
	     if [ ! -s \$tmpFile ]; then
		    echo "job \$jobScript not found in qstat. resubmitting."
	         JID=`qsub \$jobScript`
	     else
		    echo 'qstat failure. retry in 30s.'
	     fi
	    else
	      echo "job \$jobScript existed afterall, not retrying"
	      rm \$tmpFile
	      break
	    fi
	    rm \$tmpFile
	  done
	  echo \$JID >> $dir/qsub_scripts/.job_list
	}
	
	function CheckError {
	  exit_var=\$1
	  comment=\${2:-}
	  del_file=\${3:-}
	  if [[ "\$comment" != "" ]]; then
	    comment="(Reason: \$comment)"
	  fi
	  if [[ "\$exit_var" -ne "0" ]] ; then
	    if [ \$retry -eq 1 ]; then
	       echo "$job_file \$comment" >> $stat_file
	       if [[ "\$del_file" != "" ]]; then
	           rm -f "\$del_file"
	       fi
	       exit \$exit_var
	    else
	       if [[ "\$del_file" != "" ]]; then
	           rm -f "\$del_file"
	       fi
	       sed -i 's/^retry=0/retry=1/' $job_file
	       SubmitJob $job_file
	       exit
	    fi
	  fi
	}


ERROR

	print OUT "echo 'Running on : ' `hostname`\n";
	print OUT "echo 'Start Time : ' `date`\n";

	# the actual command
	print OUT "perl LoadVariants.pl -a $annosource\n";
	print OUT "CheckError \"\$?\" \"Main Command Failed\"\n";
	
	# status file on success
	print OUT "echo OK > $stat_file\n";

	#print OUT "rm $job_file\n";
	close OUT;
	return($stat_file,$job_file);
}

sub Critical {
	my ($msg, $item1,$item2,$item3) = @_; #("Job not submitted correctly",$dir,$subdir,$annosource);
	# send out an email.
	my $from = 'no-reply@'.hostfqdn();
	my $subject = "VariantDB Annotation Failure";
	my $content =  "Dear Administrator,\r\n\r\n";
	   $content .= "A problem was encountered with the annotation of new variants in VariantDB. The following message was provided: \r\n\r\n";
	   $content .= "$msg\r\n";
	if ($item1) {
	   $content .= "Location : $item1\r\n";
	}
	if ($item2) {
	   $content .= "Location : $item2\r\n";
	}
	if ($item3) {
	   $content .= "Location : $item3\r\n";
	}
	SendMail($from,$adminemail,$subject,'','application/gzip',$content);
		
	
}


sub SendMail
{
   use MIME::Lite;
   use File::Basename;

   my( $from, $to, $subject, $filename, $type, $data ) = @_;
   # headers
   $msg = MIME::Lite->new(
	From	=> $from,
	To	=> $to,
	Subject	=> $subject,
	Type	=> 'multipart/mixed'
	);
   # text content
   $msg->attach(
	Type	=> 'TEXT',
	Data	=> $data
	);
   if ($filename ne '') {
	   # attachment
	   $msg->attach(
		Type	=> $type,
		Path	=> $filename,
		Filename => basename($filename),
		Disposition	=> 'attachment'
		);
   }
   # send.
   $msg->send; # I believe this depends on correctly set smtp in sendmail configuration (DS field)
   return;
}

