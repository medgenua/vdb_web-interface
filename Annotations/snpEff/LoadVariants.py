from CMG.Utils import Re
import configparser
import argparse
import os
import sys
import time
import subprocess
import shelve
import gzip

# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


## some variables/objects
re = Re()  # customized regex-handler


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Run snpEff on the all NEW variants & store to DB
            - Run snpEff on all Variants & store to dedicated validation table in DB
            - Run snpEff on a VCF file & store to new VCF file.
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # annotation version
    parser.add_argument("-a", "--anno", required=True, help="Annotation version to use")
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Run in Validation Mode. All variants are annotated, and stored in a separate table.",
    )
    # file mode
    parser.add_argument(
        "-f", "--infile", required=False, help="Input VCF file to use in file-based mode."
    )
    parser.add_argument(
        "-o", "--outfile", required=False, help="Output VCF file to use in file-based mode."
    )
    args = parser.parse_args()
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    return config, args


def StoreResults(snpEff_outfile):
    # create shelve/pickle object to hold annotations if file-based work.
    if annotator.args.infile:
        sf = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations",
            annotator.method,
            "Output_Files/",
            "{}.{}.shelf".format(args.table_name, time.time()),
        )
        s = shelve.open(sf)
        for suffix in ["bak", "dat", "dir"]:
            annotator.files.append(f"{sf}.{suffix}")
    # load codes
    for c in ["Effect", "EffectImpact", "FunctionalClass", "GeneCoding", "TranscriptBioType"]:
        annotator.GetCode(c)

    # parse output
    snpEff_load_file = snpEff_outfile.replace("vcf", "load.txt")
    with open(snpEff_outfile, "r") as f_in, open(snpEff_load_file, "w") as f_out:
        for line in f_in:
            if line.startswith("#"):
                continue
            cols = line.rstrip().split("\t")
            effects = cols[7].split(";").pop().replace("EFF=", "").split(",")
            for e in effects:
                parts = re.isplit(r"\(|\||\)", e)
                parts = ["." if x == "" else x for x in parts]
                while len(parts) <= 10:
                    log.debug("not enough parts found, extending array")
                    parts.append(".")
                parts[0] = annotator.GetCode("Effect", parts[0])
                parts[1] = annotator.GetCode("EffectImpact", parts[1])
                parts[2] = annotator.GetCode("FunctionalClass", parts[2])
                parts[7] = annotator.GetCode("TranscriptBioType", parts[7])
                parts[8] = annotator.GetCode("GeneCoding", parts[8])
                # db vs output has different order.
                fields_out = [
                    str(x)
                    for x in [
                        cols[2],  # vid
                        parts[6],  # gene symbol
                        parts[9],  # transcript
                        parts[10],  # exon
                        parts[0],  # effect
                        parts[1],  # effect impact
                        parts[2],  # functional class
                        parts[3],  # codon change
                        parts[4],  # aa change
                        parts[8],  # gene coding
                        parts[7],  # transcript biotype
                    ]
                ]
                if annotator.args.infile:
                    # add to shelf by virtual vid
                    vid = "{},{},{},{}".format(cols[0], cols[1], cols[2], cols[3])
                    if vid not in s:
                        s[vid] = ""
                    # in result : add the 'vid' stored in vcf column 2
                    s[vid] += (
                        ",".join([f"{k}:{v}" for k, v in zip(annotator.fieldnames, fields_out)])
                        + "|"
                    )
                else:
                    f_out.write("\t".join(fields_out) + "\n")

    # if file-based:
    if annotator.args.infile:
        # parse load file again
        line_idx = 0
        with open(annotator.args.infile, "r") as f_in, open(annotator.args.outfile, "w") as f_out:
            for line in f_in:
                # headers
                if line.startswith("#"):
                    f_out.write(line)
                    continue
                line_idx += 1
                # variantID
                cols = line.rstrip().split("\t")
                if cols[2] == ".":
                    vid_tmp = f"line_{line_idx}"
                else:
                    vid_tmp = cols[2]
                vid = "{},{},{},{}".format(cols[0], cols[1], vid_tmp, cols[3])
                if vid not in s:
                    log.warning(f"Variant {vid} not found in shelf. Skipping")
                    continue
                cols[7] += ";snpEff.{}={}".format(annotator.annotation, s[vid].rstrip("|"))
                f_out.write("\t".join(cols) + "\n")
        s.close()
    # if regular db-based
    else:
        field_string = "`" + "`,`".join(annotator.fieldnames) + "`"
        annotator.dbh.doQuery(
            "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}{}` ({})".format(
                snpEff_load_file, annotator.args.prefix, annotator.args.table_name, field_string
            )
        )
    annotator.files.append(snpEff_load_file)


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # get logger
    log = StartLogger(config, "snpEff")

    # get the annotator object
    annotator = Annotator(config=config, method="snpEff", annotation="snpEff", args=args)

    # set table name.
    annotator.args.table_name = "Variants_x_snpEff"
    annotator.annotation = annotator.dbh.runQuery(
        "SELECT `Value` FROM `Annotation_DataValues` WHERE `Annotation` = 'snpEff'"
    )[0]["Value"]

    ## Validate options
    try:
        # table exists ?
        annotator.ValidateTable()
        # get field/column names:
        annotator.fieldnames = annotator.dbh.getColumns(annotator.args.table_name)["names"]
        # strip aid
        d = annotator.fieldnames.pop(0)
        # annotation folder present?
        os.path.isdir(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations",
                annotator.method,
                "data",
                annotator.annotation,
            )
        )
    except (DataError, ValueError) as e:
        log.error(e)
        sys.exit(1)

    # Get Input Variants
    log.info("Retrieving variants to process")
    if not annotator.args.infile:
        try:
            annotator.GetVariants()
        except Exception as e:
            log.error("Could not fetch input variants: {}".format(e))
            sys.exit(1)
    else:
        # cp to the tmp folder, adding line_idx as id in INFO, strip non-needed fields
        log.info("Parsing input VCF file...")
        annotator.PrepareInputVCF()
        # convert to correct format (add the id field)
    annotator.files.append(annotator.vcf)

    # run the annotation procedures
    try:

        # out file:
        snpEff_outfile = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations",
            annotator.method,
            "Output_Files/",
            "{}.{}.vcf".format(annotator.args.table_name, time.time()),
        )
        working_dir = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "Annotations", annotator.method
        )
        # build command
        cmd = "cd {} && ./sun-jre-7/bin/java -Xmx6G -jar snpEff.jar eff -noStats -noLog '{}' '{}' > '{}'".format(
            working_dir,
            annotator.annotation,
            annotator.vcf,
            snpEff_outfile,
        )
        subprocess.run(cmd, shell=True, check=True)
        annotator.files.append(snpEff_outfile)

    except Exception as e:
        log.error("snpEff failed : {}".format(e))
        sys.exit(1)
    # Process & store results
    try:
        StoreResults(snpEff_outfile)
        # annotator.files.append(snpEff_outfile.replace(".tsv.gz", ".load.txt"))
    except Exception as e:
        log.error("Failed to process snpEff results: {}".format(e))
        sys.exit(1)

    # cleanup
    annotator.cleanup()
