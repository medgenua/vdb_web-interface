from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, AutoVivification, CheckVariableType
from CMG.DataBase import MySqlError
from CMG.HPO.OBO import Parser
import configparser
import argparse
import os
import subprocess
import sys

# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger

import time
from datetime import date
import subprocess
import glob
import tempfile
import urllib.request
import hashlib
import shutil
import ftplib
import shelve


class DataError(Exception):
    pass



## some variables/objects
re = Re()  # customized regex-handler


def ConvertChr(v):
    chrom_dict = {x: x for x in range(1, 23)}
    chrom_dict.update({"X": 23, 23: "X", "Y": 24, 24: "Y", "M": 25, "MT": 25, 25: "M"})
    return chrom_dict[v]


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Evaluate if a new annotatation release is present
            - Run annotator on all Variants if new release & store to dedicated validation table in DB
            
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # annotation type
    # parser.add_argument("-a", "--anno", required=True, help="Annotation Type")
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    parser.add_argument('-a','--annotation',required=False,help='dummy argument for compatibility')
    # download only mode
    parser.add_argument(
        "-d",
        "--download",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Download new release only into Validation_ tables.",
    )
    # force mode (normally snpEff is considered static)
    parser.add_argument(
        "-f",
        "--force",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Force reannotate and log.",
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Validation Mode: No data download, just reannotation and results are saved to files instead of emailed",
    )

    args = parser.parse_args()
    # set prefix to validation during update.
    args.prefix = "Validation_"

    return config, args

def CompareResults():
    # get codings
    #codes = LoadDecodingInfo()
    # open all output files.
    time_stamp = date.today().strftime("%Y-%m-%d")
    out_files = dict()
    for vtype in ["SNV", "INS", "DEL", "SUB", "NEW", "LOST", "LOG"]:
        out_files[vtype] = open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/snpEff/Output_Files/",
                "Validation.{}.{}.{}".format(annotator.args.anno, time_stamp, vtype),
            ),
            "w",
        )
    # some trackers
    nr_match = 0
    nr_new = 0
    nr_lost = 0
    # work in slices of 50K variants :
    start_vid = 0
    db_names = [
        "vid",
        "GeneSymbol", 
        "Transcript",
        "Exon",
        "Effect",
        "EffectImpact",
        "FunctionalClass",
        "CodonChange",
        "AAchange",
        "GeneCoding",
        "TranscriptBioType"
        ]
    while True:
        # get last vid.
        vids = annotator.dbh.runQuery(
            "SELECT id FROM `Variants` WHERE id >= %s ORDER BY `id` LIMIT 50000", start_vid
        )
        if not vids:
            # no data left.
            break
        last_vid = vids[-1]["id"]
        log.info("Working on 50K variants with VIDS from {} to {}".format(start_vid, last_vid))
        vids = []
        # get old annotations
        log.info("Fetching old annotations")
        rows = annotator.dbh.runQuery(
            "SELECT va.vid,va.aid, va.GeneSymbol, va.Transcript, va.Exon, va.Effect, va.EffectImpact, va.FunctionalClass, va.CodonChange, va.AAchange, va.GeneCoding, va.TranscriptBioType FROM `{}` va WHERE va.vid BETWEEN %s AND %s".format(
                annotator.args.table_name
            ),
            [start_vid, last_vid],
            as_dict=False,
        )
        old_annotations = AutoVivification()
        for row in rows:
            row = list(row)
            vid = row.pop(0)
            aid = row.pop(0)
            row = [(x if x else ".") for x in row]
            old_annotations[vid][aid] = row
        # get new annotations & compare
        log.info("Fetching and comparing new annotations.")
        rows = annotator.dbh.runQuery(
            "SELECT va.vid,va.aid, va.GeneSymbol, va.Transcript, va.Exon, va.Effect, va.EffectImpact, va.FunctionalClass, va.CodonChange, va.AAchange, va.GeneCoding, va.TranscriptBioType FROM `Validation_{}` va WHERE va.vid BETWEEN %s AND %s".format(
                annotator.args.table_name
            ),
            [start_vid, last_vid],
            as_dict=False,
        )
        for row in rows:
            row = list(row)
            vid = row.pop(0)
            aid = row.pop(0)
            row = [(x if x else ".") for x in row]
            # raw values for Variants_x_snpEff
            db_fields = ListOfStrings([vid] + row)
            
        
            # annotation not seen in old table (new gene?)
            # vid not in old annotations:
            if not vid in old_annotations:
                nr_new += 1
                if annotator.args.validate:
                    out_files["NEW"].write("{}\n".format("; ".join(ListOfStrings(row))))
                else:
                    # insert into main table
                    new_aid = annotator.dbh.insertQuery(
                        "INSERT INTO `{}` (`{}`) VALUES ('{}')".format(
                            annotator.args.table_name, "`,`".join(db_names), "','".join(db_fields)
                        )
                    )

                    # add to log.
                    out_files["LOG"].write(
                        "{}\t{}\t{}:{}\t2\n".format(
                            vid, annotator.args.userid, annotator.args.table_name, new_aid
                        )
                    )
                continue
            # vid exists in old data : loop all aids for match.
            match = False
            for o_aid in list(old_annotations[vid]):
                if row == old_annotations[vid][o_aid]:
                    nr_match += 1
                    match = True
                    del old_annotations[vid][o_aid]
                    break
            if not match:
                # mismatch : add new
                nr_new += 1
                if annotator.args.validate:
                    out_files["NEW"].write("{}\n".format("; ".join(ListOfStrings(row))))
                else:

                    # add new:
                    new_aid = annotator.dbh.insertQuery(
                        "INSERT INTO `{}` (`{}`) VALUES ('{}')".format(
                            annotator.args.table_name, "`,`".join(db_names), "','".join(db_fields)
                        )
                    )

                    # add to log.
                    out_files["LOG"].write(
                        "{}\t{}\t{}:{}\t2\n".format(
                            vid, annotator.args.userid, annotator.args.table_name, new_aid
                        )
                    )
        # any left on the old_annotations stack : these are lost.
        for vid in old_annotations:
            for aid in old_annotations[vid]:
                nr_lost += 1
                if annotator.args.validate:
                    out_files["LOST"].write(
                        "{}\n".format("; ".join(ListOfStrings(old_annotations[vid][aid])))
                    )
                else:
                    # archive old annotation. (without vid field)
                    db_fields = [vid] + old_annotations[vid][aid]
                    d = list(zip(db_names, db_fields))
                    # del d[0]
                    contents = "@@@".join(["{}|||{}".format(i[0], i[1]) for i in d])
                    # archive
                    archive_aid = annotator.dbh.insertQuery(
                        "INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (%s,%s,%s,%s,%s)",
                        [
                            vid,
                            annotator.args.table_name,
                            f"-{args.build}",
                            time_stamp,
                            contents,
                        ],
                    )
                    # log
                    out_files["LOG"].write(
                        "{}\t{}\t{}:{}\t1\n".format(
                            vid, annotator.args.userid, annotator.args.table_name, archive_aid
                        )
                    )
                    # delete the annotation entry.
                    annotator.dbh.doQuery(
                        f"DELETE FROM `{args.table_name}` WHERE vid = %s AND aid = %s",
                        [vid, aid],
                    )
         # clear
        old_annotations.clear()
        # next iteration.
        start_vid = last_vid + 1
    # overview.
    log.info("STATISTICS:")
    log.info(f"  MATCH : {nr_match}")
    log.info(f"  NOVEL : {nr_new}")
    log.info(f"  LOST  : {nr_lost}")

    # close file handles
    for fh in out_files:
        out_files[fh].close()
    # load to log-table.
    if not annotator.args.validate:
        annotator.dbh.doQuery(
            "LOAD DATA LOCAL INFILE '{}' INTO TABLE `Variants_x_Log` (`vid`, `uid`, `entry`, `arguments`)".format(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/snpEff/Output_Files/",
                    "Validation.{}.{}.{}".format(annotator.args.anno, time_stamp, "LOG"),
                )
            )
        )

def ListOfStrings(data):
    return [str(x) for x in data]



if __name__ == "__main__":
    try:
        config, args = LoadConfig()
        # hardcoded
        args.anno = "snpEff"
        args.table_name = "Variants_x_snpEff"
        
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)
    log = StartLogger(config, "snpEff")

    # get the annotator object
    annotator = Annotator(config=config, method="snpEff", annotation="snpEff", args=args)

    # validate the tables & files.
    try:
        # general validation
        annotator.ValidateTable()
    except DataError as e:
        log.error("Could not locate necessary resources: {}".format(e))
        sys.exit(1)

    # get userid of admin user (for archive logs)
    try:
        annotator.args.userid = annotator.dbh.runQuery(
            "SELECT id FROM `Users` WHERE `email` = %s", [config["USERS"]["ADMIN"]]
        )[0]["id"]
    except Exception as e:
        log.error("Admin email not recognized by VariantDB : {}".format(e))
        sys.exit(1)

    ## DOWNLOAD IS NOT SUPPORTED (static release)
    if not annotator.args.force:
        log.warning("snpEff is a static repository. Download of novel annotation data is not supported.")
        log.info("Process ends here")
        sys.exit(0)

    if not annotator.args.download:
        # run loadvariants
        #try:
        #    build = annotator.dbh.runQuery("SELECT `Value` FROM `Annotation_DataValues` WHERE `Annotation` = 'snpEff'")[0]["Value"]
        #    cmd = ["python", os.path.join(
        #            config["LOCATIONS"]["SCRIPTDIR"], "Annotations/snpEff/LoadVariants.py"
        #        ), 
        #        "-a",
        #        build, 
        #        "-v"
        #        ]
        #    subprocess.check_call(cmd)
        #except Exception as e:
        #    log.error("snpEff failed : {}".format(e))
        #    sys.exit(1)
        # compare results
        try:
            CompareResults()
        except Exception as e:
            log.error("Failure in result comparison : {}".format(e))
            sys.exit(1)


# cleanup
