#!/usr/bin/perl
$|++;
use DBI;
use Getopt::Std;

use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);
# opts
# a : (a)nnotation table to use.
# v : validation mode.
# f : provide a VCF file to annotate into the VALIDATION_ tables.
getopts('a:b:vf:o:', \%opts) ;

if (!exists($opts{'a'})) {
	die('Annotation table not specified.');
}
$anno = $opts{'a'};

print "Annotation : $anno\n";

## IN VALIDATION MODE ?
our $validation = 0;
our $table_prefix = '';
our $var_file = '';
our $out_file = '';
if (defined($opts{'f'})) {
	if (!-f $opts{'f'}) {
		print "ERROR: provided file not found: $opts{'f'}\n";
		die();
	}
	$var_file = abs_path($opts{'f'});
	#$out_file = substr($var_file,0,-3)."out.snpEff.$anno.vcf";
	$out_file = abs_path($opts{'o'});
	$validation = 2;
	print "RUNNING ANNOTATIONS IN FILE MODE:\n";
	print "  - variants in file '$var_file' will be annotated\n";
	print "  - Annotations are not stored in the database\n";
	print "  - Annotations are written to $out_file\n"; 
	print "  - original annotations are not modified\n";
	print "\n";
}
elsif (defined($opts{'v'})) {
	print "\n";
	print "RUNNING ANNOTATIONS IN VALIDATION MODE:\n";
	print "  - All variants will be annotated\n";
	print "  - Annotations are stored in 'Validation_<tablename>' table\n";
	print "  - original annotations are not modified\n";
	print "\n";
	print "press <ctrl>-c to abort\n";
	sleep 5;
	print "\n\n";
	$validation = 1;
	$table_prefix = 'Validation_';
}


####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";

###########################
# CONNECT TO THE DATABASE #
###########################
## GET current/provided build
our $db = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
}

$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
if ($usemc == 1) {
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	my $mcpath = abs_path("../../cgi-bin/inc_memcache.pl");
	require($mcpath);	
	#use JSON;
	#our $memd = new Cache::Memcached { 'servers' => \@mcs, 'debug' => 0, 'compress_threshold' => 1_000_000, };
}
require("../../cgi-bin/inc_views.pl");

# file mode has a VCF file ready.
our $timestamp = time();
if (-e "Input_Files/$anno.$timestamp.vcf") {
	unlink("Input_Files/$anno.$timestamp.vcf");
}
if ($validation != 2) {
	## fork : better clears memory after fetching variants
	my $pid = fork;
	if (!defined($pid)) {
		die "cannot fork: $!\n";
	}
	elsif ($pid == 0) {
		my %second = ();
		## GET VARIANTS
		## all variants
		$query = "SELECT id AS VariantID FROM `Variants`";
		my $allvariants = runQuery($query,"Variants");
		#my @allvids; # push backwards, skip filling this array for memory.
		#foreach(@$allvariants) {
		#	push(@allvids,$_->{'VariantID'});
		#}
		if ($validation == 1) {
			# VALIDATION : annotate all into new table.
			$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."Variants_x_snpEff`");
			$dbh->do("CREATE TABLE `$table_prefix"."Variants_x_snpEff` LIKE `Variants_x_snpEff`");
		}
		else {
			## variants in annotation table
			my $query = "SELECT vid as VariantID FROM `Variants_x_snpEff`";
			## too many variants , so not using the aref of hashes approach of memcache.
			my $cv = $dbh->prepare($query);
			my $nr = $cv->execute();
			if ($nr	 < 50000) {
				$max = $nr;
			}
			else {
				$max = 50000;
			}
			my $lcache;
			while (my $result = shift(@$lcache) || shift( @{$lcache = $cv->fetchall_arrayref(undef,$max)|| []})) {
				$second{$result->[0]} = 1;
			}
			$cv->finish();
		}
		my @toannotate = ();# = grep {!$second{$_}} @allvids;
		foreach(@$allvariants) {
			if (!defined($second{$_->{'VariantID'}})) {
				push(@toannotate,$_->{'VariantID'});
			}
		}	
		# clear up variables
		$annovariants = [];
		$allvariants = [];
		@allvids = ();
		%second = ();
		
		if (scalar(@toannotate == 0)) {
			print "No variants to update.  Exiting\n";
			close OUT;
			exit;
		}
	
		$query = "SELECT v.chr, v.start, v.stop, v.RefAllele, v.AltAllele, v.id AS VariantID FROM `Variants` v WHERE v.id IN (?)";
		$rowcache = runQuery($query,"Variants",\@toannotate);
		if (scalar(@$rowcache) == 0) {
			print "No variants to update. Did you specify the correct annotation table ($anno)? Exiting\n";
			close OUT;
			exit;
		}
		open OUT, ">Input_Files/$anno.$timestamp.vcf";
		## print headers
		print OUT "##fileformat=VCFv4.1\n";
		print OUT "##source=VariantDB_snpeff_annoation\n";
		print OUT "##reference=CMG_Biomina-galaxy-hg19.fa\n";
		print OUT "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n";
	

		## print 
		$ok = 0;
		foreach my $result (@$rowcache)  {
			$result->{'chr'} = $chromhash{$result->{'chr'}};
			$result->{'chr'} =~ s/23/X/;
			$result->{'chr'} =~ s/24/Y/;
			if ($result->{'chr'} eq '' || $result->{'start'} eq '' || $result->{'VariantID'} eq '' ) {
				next;
			}
			$ok++;
			if ($result->{AltAllele} =~ m/non_ref/i) {
				print "Skipping non-regular variant : ".$result->{'chr'}."\t".$result->{'start'}."\t".$result->{'VariantID'}."\t".$result->{'RefAllele'}."\t".$result->{'AltAllele'}."\n";
				next;
			}
			if ($result->{AltAllele} =~ m/N/) {
				print "Replacing -N- in AltAllele by -A- for annotation in : ".$result->{'chr'}."\t".$result->{'start'}."\t".$result->{'VariantID'}."\t".$result->{'RefAllele'}."\t".$result->{'AltAllele'}."\n";
				$result->{AltAllele} =~ s/N/A/g;
			}
			print OUT $result->{'chr'}."\t".$result->{'start'}."\t".$result->{'VariantID'}."\t".$result->{'RefAllele'}."\t".$result->{'AltAllele'}."\n";
		}
		if ($ok == 0) {
			print "No variants printed. Quitting\n";
			exit;
		}
		close OUT;
		## disconnect from db. 
		$dbh->disconnect();
		exit; #child
	}
	else {
		print "Waiting for variant fetching and input file generation\n";
		waitpid $pid, 0;
	}
}
if ($validation == 2) {
	## run snpeff eff
	`./sun-jre-7/bin/java -Xmx6g -jar snpEff.jar eff -v $anno $var_file > $out_file`;
	system("cp '$out_file' '$out_file.orig'");
	## rewrite VCF tags.
	ReWriteVCF("snpEff.$anno",$out_file);

}
else {
	# no data : exit.
	if (!-f "Input_Files/$anno.$timestamp.vcf" || !-s "Input_Files/$anno.$timestamp.vcf") {
		print "No variants to annotate.\n";
		exit;
	}
	## run snpeff eff 
	`./sun-jre-7/bin/java -Xmx6g -jar snpEff.jar eff -v $anno Input_Files/$anno.$timestamp.vcf > Output_Files/$anno.$timestamp.vcf`;

	## store results
	Store_Gene($anno,$table_prefix,$timestamp);	
}

#################
## SUBROUTINES ##
#################
sub ReWriteVCF {
	my ($anno,$vcf_file) = @_;
	## local db connection
	my $connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	my $i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	## load codings
	my %codes = ();
	my @cols = qw/Effect EffectImpact FunctionalClass GeneCoding TranscriptBioType/;
	foreach(@cols) {
		$col = $_;
		my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_snpEff_$col'");
		$sth->execute();
		while (my @r = $sth->fetchrow_array()) {
			$codes{$col."_".$r[1]} = $r[0];
		}
		$sth->finish();
	}
	#my %hash = ();
	#my %lines = ();						   
	open IN, $vcf_file;
	# skip headers
	#$ok = 0;
	open OUT, ">$vcf_file.tmp";
	while (my $line = <IN>) {
		chomp($line);
		if ($line =~ m/^#/) {
			print OUT $line."\n";
			next;
		}
		my @p = split(/\t/,$line);
		my $vid = '';
		if ($p[2] =~ m/^\d+$/) {
			$vid = $p[2];
		}
		elsif ($p[7] =~ m/id=(\d+)[;\t]/) {
			$vid = $1;
		}
		else {
			print "Missing VID: $line\n";
			next;
		}
		my $info = "$anno=";
		my @info_fields = split(/;/,$p[7]);
		my $cidx = -1;
		my $ok = 0;
		foreach (@info_fields) {	
			$cidx++;
			# need the EFF=... field
			if ($_ =~ m/^EFF/) {
				$ok = 1;
				last;
			}
		}
		if ($ok == 0) { # some variants cause errors && have no info => add blank entries
			#print OUT join("\t",@p)."\n";
			$cidx++;
			$info_fields[$cidx] = "EFF=.(.|.|.|.|.|.|.|.|.|.)";
		}
		#remove the front tag.
		$info_fields[$cidx] =~ s/EFF=//;
		# transcript variants separated by comma 
		# same transcript variant present multiple times IF ALT == N (translated to all options by snpEff)
		my @ia = split(/,/,$info_fields[$cidx]);
		# process each transcript.
		foreach(@ia) {
			# 1 : GeneSymbol 		i 6
			# 2 : Transcript		i 9
			# 3 : Exon			i 10
			# 4 : Effect			i 0
			# 5 : EffectImpact 		i 1
			# 6 : FunctionalClass		i 2
			# 7 : CodonChange		i 3 
			# 8 : AAchange			i 4
			# 9 : GeneCoding		i 8
			# 10 : TranscriptBiotype	i 7
			# structure : Effect(EffectImpact|FunctionalClass|CodonChange|AAchange|AAlength|GeneSymbol|TranscriptBiotype|GeneCoding|Transcript|Exon[|ignored fields])
			my @i = split(/\(|\||\)/,$_);
			my $idx = 0;
			foreach(@i) {
				if ($i[$idx] eq '') {
					$i[$idx] = '.';
				}
				$idx++;
			}
			while ($idx <= 10){ 
				$i[$idx] = '.';
				$idx++;
			}
			# encode 
			if (!defined($codes{"Effect_".$i[0]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_Effect','$i[0]')");
				$codes{"Effect_".$i[0]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[0] = $codes{"Effect_".$i[0]};	
			if (!defined($codes{"EffectImpact_".$i[1]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_EffectImpact','$i[1]')");
				$codes{"EffectImpact_".$i[1]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[1] = $codes{"EffectImpact_".$i[1]};
			if (!defined($codes{"FunctionalClass_".$i[2]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_FunctionalClass','$i[2]')");
				$codes{"FunctionalClass_".$i[2]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[2] = $codes{"FunctionalClass_".$i[2]};
			if (!defined($codes{"GeneCoding_".$i[8]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_GeneCoding','$i[8]')");
				$codes{"GeneCoding_".$i[8]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[8] = $codes{"GeneCoding_".$i[8]};
			if (!defined($codes{"TranscriptBioType_".$i[7]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_TranscriptBioType','$i[7]')");
				$codes{"TranscriptBioType_".$i[7]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[7] = $codes{"TranscriptBioType_".$i[7]};
			$info .= "GeneSymbol:$i[6],Transcript:$i[9],Exon:$i[10],Effect:$i[0],EffectImpact:$i[1],FunctionalClass:$i[2],CodonChange:$i[3],AAchange:$i[4],GeneCoding:$i[8],TranscriptBiotype:$i[7]|";
		}
		$info_fields[$cidx] = substr($info,0,-1);
		$p[7] = join(";",@info_fields);
		print OUT join("\t",@p)."\n";
	}
	close IN;
	close OUT;
	#system("cp '$vcf_file' '$vcf_file.tmp2'");
	system("mv '$vcf_file.tmp' '$vcf_file'");
	$dbh->disconnect();
}

sub Store_Gene {
	## local db connection
	my $connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	my $i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	## load codings
	my %codes = ();
	my @cols = qw/Effect EffectImpact FunctionalClass GeneCoding TranscriptBioType/;
	foreach(@cols) {
		$col = $_;
		my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_snpEff_$col'");
		$sth->execute();
		while (my @r = $sth->fetchrow_array()) {
			$codes{$col."_".$r[1]} = $r[0];
		}
		$sth->finish();
	}
	my ($anno,$prefix,$timestamp) = @_;
	
	my %hash = ();
	my %lines = ();						   
	open IN, "Output_Files/$anno.$timestamp.vcf";
	# skip headers
	$ok = 0;
	while (my $line = <IN>) {
		if ($line =~ m/^#CHR/){
			$ok = 1;
			last;
		}
	}
	if ($ok == 0) {
		print "No data in output file. exiting\n";
		close IN;
		exit;
	}
	open OUT, ">Output_Files/load.$anno.$timestamp.txt";
	while (<IN>) {
		chomp($_);
		my @p = split(/\t/,$_);
		my $vid = $p[2];
		$p[7] =~ s/EFF=//;
		my @ia = split(/,/,$p[7]); # this causes multiple variants / transcript IF ALT == N (translated to all options by snpEff in output)
		foreach(@ia) {
			my @i = split(/\(|\||\)/,$_);
			my $idx = 0;
			foreach(@i) {
				if ($i[$idx] eq '') {
					$i[$idx] = '.';
				}
				$idx++;
			}
			while ($idx <= 10){ 
				$i[$idx] = '.';
				$idx++;
			}
			# encode 
			if (!defined($codes{"Effect_".$i[0]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_Effect','$i[0]')");
				$codes{"Effect_".$i[0]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[0] = $codes{"Effect_".$i[0]};	
			if (!defined($codes{"EffectImpact_".$i[1]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_EffectImpact','$i[1]')");
				$codes{"EffectImpact_".$i[1]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[1] = $codes{"EffectImpact_".$i[1]};
			if (!defined($codes{"FunctionalClass_".$i[2]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_FunctionalClass','$i[2]')");
				$codes{"FunctionalClass_".$i[2]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[2] = $codes{"FunctionalClass_".$i[2]};
			if (!defined($codes{"GeneCoding_".$i[8]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_GeneCoding','$i[8]')");
				$codes{"GeneCoding_".$i[8]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[8] = $codes{"GeneCoding_".$i[8]};
			if (!defined($codes{"TranscriptBioType_".$i[7]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_TranscriptBioType','$i[7]')");
				$codes{"TranscriptBioType_".$i[7]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[7] = $codes{"TranscriptBioType_".$i[7]};

			#$sth->execute($vid,$i[6],$i[9],$i[10],$i[0],$i[1],$i[2],$i[3],$i[4],$i[8],$i[7]);
			print OUT "$vid\t$i[6]\t$i[9]\t$i[10]\t$i[0]\t$i[1]\t$i[2]\t$i[3]\t$i[4]\t$i[8]\t$i[7]\n";
		}
	}
	close IN;
	close OUT;
	#$sth->finish();
	$dbh->do("LOAD DATA LOCAL INFILE 'Output_Files/load.$anno.$timestamp.txt' INTO TABLE `$prefix"."Variants_x_snpEff` (vid, GeneSymbol, Transcript, Exon, Effect, EffectImpact, FunctionalClass,CodonChange,AAchange,GeneCoding,TranscriptBiotype)");
	## clean up.
	#system("rm -f Input_Files/$anno.list.txt");
	#system("rm -f Output_Files/$anno.*");
	#system("rm -f Output_Files/load.$anno.txt");
	#system("rm -f snpEff_genes.txt");
	#system("rm -f snpEff_summary.html");
	$dbh->disconnect();
	if ($usemc == 1) {
		clearMemcache($prefix."Variants_x_snpEff");
	}
	clearViews($prefix."Variants_x_snpEff");
}




sub runQuery {
	if ($usemc == 1) {
		$memd = new Cache::Memcached::Fast { 'servers' => \@mcs, 'compress_threshold' => 1_000_000, };
	}
	my ($query,$table,$var,$type) = @_;
	my %slices;
	if (!defined($var)) {
		$slices{1} = '';
	}
	else {
		## created slices based on VariantID for ranges per 10,000
		foreach(@$var) {
			my $s = int($_/10000);
			if (!defined($slices{$s})) {
				$slices{$s} = ();
			}
			push(@{$slices{$s}},$_);
		}
	}
	my $mcidx = '';
	if ($usemc == 1) {
		$mcidx = getMCidx($table);
	}
	#print "fetching ".keys(%slices)." slices for ".scalar(@$var). " variants<br/>";
	my $result = [];
	if ($query =~ m/ IN \(\s*\?\s*\)/i) {
		$query =~ s/ IN \(\s*\?\s*\)/ BETWEEN ? AND ? /i;
		$exec = 'between';
	}
	elsif ($query =~ m/\?/) {
		$exec = 'in';
	}
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) ;#or die('Could not connect to database'); ## our : make available to subs
	$dbh->{mysql_auto_reconnect} = 1; 
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 1;
		$i++;
		#print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) ;
	}
	if (!defined($dbh)) {
		 die('Could not connect to database');
	}

	my $sth = $dbh->prepare($query);

	foreach(keys(%slices)) {
		my @vids = @{$slices{$_}};
		my $sstart = $_*10000;
		my $sstop = ($_+1)*10000 - 1;
		#my $qpart = join(",",(($_*10000)..((($_+1)*10000)-1)));
		my $lq = $query;
		my $qpart = '';
		if ($lq =~ m/BETWEEN \? AND \?/i) {
			$lq =~ s/BETWEEN \? AND \?/BETWEEN $sstart AND $sstop/i;
		}
		else {
			$qpart = join(",",($sstart..$sstop)); 
			$lq =~ s/\?/$qpart/;
		}

		#$lq =~ s/\?/$qpart/;
		my $href;
		if ($usemc == 1) {
			$href = $memd->get("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq));
		}
		if (!defined($href) || $usemc == 0) {
			if ($exec eq 'between') {
				my $nr = $sth->execute($sstart,$sstop);
			}
			elsif($exec eq 'in') {
				my $nr = $sth->execute( $qpart );
			}
			else {
				my $nr = $sth->execute();
			}

			#$href = $sth->fetchall_hashref('VariantID');
			$href = $sth->fetchall_arrayref({});
			if ($usemc == 1) {
				## store in memcached
				my $r = $memd->replace("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq),$href);
				if (!$r) {
					$r = $memd->set("ngs:$db:$table:$mcidx:cgi:".md5_hex($lq),$href);
				}
			}

		}
		push(@$result,@$href);
	}
	$sth->finish();
	$dbh->disconnect();
	## select the actually needed items.
        if (defined($var)) {
                my %needed = map {$_ => 1} @$var;
                my @r;
                foreach(@$result) {
                        if (defined($needed{$_->{'VariantID'}})) {
                                push(@r,$_);
                        }
                }
                $result = \@r;
        }

	return($result);
}

