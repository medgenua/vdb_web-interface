#!/usr/bin/perl
$|++;
use DBI;
use Getopt::Std;
use Scalar::Util qw(looks_like_number);
use Math::BigFloat;
# opts
# a : (a)nnotation table to use.
# b : genome build (if provided)
getopts('a:b:', \%opts) ;

use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
our $datestring = ($year+1900)."-".$mon."-".$mday;

####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";
$annovariants = [];
$allvariants = [];
@allvids = ();
%second = ();

###########################
# CONNECT TO THE DATABASE #
###########################
## GET current/provided build
our $db = '';
our $dbbuild = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$dbbuild = $cb;
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
	$dbbuild = $opts{'b'};
}

$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
###################
## load memcache ##
###################
if ($usemc == 1) {
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	my $mcpath = abs_path("../../cgi-bin/inc_memcache.pl");
	require($mcpath);	

}

## get userid from admin user
$sth = $dbh->prepare("SELECT id FROM `Users` WHERE email = '$adminemail'");
my $nr = $sth->execute();
if ($nr == 0 ) {
	$sth->finish();
	$sth = $dbh->prepare("SELECT id FROM `Users` WHERE level = 3 ORDER BY id LIMIT 1");
	$nr = $sth->execute();
	if ($nr == 0) {
		die("No Admin user found\n");
	}
}
my ($uid) = $sth->fetchrow_array();
$sth->finish();

#####################################
# CHECK IF ANNOTATION WAS SPECIFIED #
#####################################
if (!exists($opts{'a'})) {
	die('Annotation table not specified.');
}
our $anno = $opts{'a'};
#$annotable = lc($anno);

#############################
## load annotation codings ##
#############################
our %codes = ();
my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_snpEff"."_Effect'");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$codes{"Effect_".$r[1]} = $r[0];
}
$sth->finish();
my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_snpEff"."_EffectImpact'");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$codes{"EffectImpact_".$r[1]} = $r[0];
}
$sth->finish();
my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_snpEff"."_FunctionalClass'");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$codes{"FunctionalClass_".$r[1]} = $r[0];
}
$sth->finish();
my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_snpEff"."_GeneCoding'");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$codes{"GeneCoding_".$r[1]} = $r[0];
}
$sth->finish();
my $sth = $dbh->prepare("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_snpEff"."_TranscriptBioType'");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$codes{"TranscriptBioType_".$r[1]} = $r[0];
}
$sth->finish();

###################################################
## get replacement value for build from database ##
###################################################
$sth = $dbh->prepare("SELECT Value FROM `Annotation_DataValues` WHERE Annotation = 'snpEff'");
$sth->execute();
my @r = $sth->fetchrow_array();
$sth->finish();
our $build = $r[0];

############################
## DOWNLOAD NEW DATATABLE ##
############################
system("cd $scriptdir/Annotations/snpEff/ && $scriptdir/Annotations/snpEff/sun-jre-7/bin/java -jar $scriptdir/Annotations/snpEff/snpEff.jar download $anno >/dev/null 2>&1");

########################################
## LOOP THE VARIANT FILES AND PROCESS ##
########################################

my $fileidx = 1;
my @discard;
my $nrok = $nrfail = $nrnew = $missingfield = 0;
$log = $dbh->prepare("INSERT DELAYED INTO `Variants_x_Log` (vid, uid, entry, arguments) VALUES (?,'$uid',?,?)");
$archive = $dbh->prepare("INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (?,'Variants_x_snpEff','$dbbuild','$datestring',?)");
#$sids = $dbh->prepare("SELECT sid FROM `Variants_x_Samples` WHERE vid = ?");
$del = $dbh->prepare("DELETE LOW_PRIORITY FROM `Variants_x_snpEff` WHERE aid = ? AND vid = ?");
$update = $dbh->prepare("UPDATE `Variants_x_snpEff` SET CodonChange = ?, Exon = ? WHERE aid = ? AND vid = ?");
our %sidlist = ();
while (-e "$scriptdir/Annotations/Update.Files/Variants.$fileidx.txt") {
	my $l = `head -n 1 $scriptdir/Annotations/Update.Files/Variants.$fileidx.txt`;
	chomp($l);
	my @p = split(/\t/,$l);
	my $fidx = $p[-1];
	$l = `tail -n 1 $scriptdir/Annotations/Update.Files/Variants.$fileidx.txt`;
	chomp($l);
	@p = split(/\t/,$l);
	my $lidx = $p[-1];
	print "FILE: $fileidx (variants $fidx to $lidx)\n";
	# get current annotations from DB.
	$current = GetAnno($fidx,$lidx);
	# get new annotations from snpEff
	my $new = AnnoSnpEff($fileidx);
	# Compare annotations.
	foreach(keys(%$current)) {
		## check
		my $vid = $_;
		## current annotations for this vid
		foreach(keys(%{$current->{$vid}})) {
			my $aok = 0;
			my $caid = $_;
			my $skiplog = 0;
			## compare against all new annotations
			foreach(keys(%{$new->{$vid}})) {
				my $naid = $_;
				my $match = 1;
				## loop the fields
				foreach(keys(%{$current->{$vid}{$caid}})) {
					next if ($_ eq 'aid' || $_ eq 'vid');
					if (!defined($new->{$vid}{$naid}{$_})) {
						$match = 0;
						last;
					}
					if (looks_like_number($current->{$vid}{$caid}{$_})) {
						## round to 3 digits and then compare. Floating point errors: allow mismatch < 0.2*0.1precision. 
						my $old = sprintf("%.4f",$current->{$vid}{$caid}{$_});
						my $new = sprintf("%.4f",$new->{$vid}{$naid}{$_}) ;
						if (abs($old-$new) > 0.0002 ) {
							$match = 0;
							last;
						}
					}
					elsif ($current->{$vid}{$caid}{$_} ne $new->{$vid}{$naid}{$_}) {
						## hack 1 : old intronic notation : no CodonChange, Now : distance to protein. : update in db, don't log.
						if ($_ eq 'CodonChange' && $current->{$vid}{$caid}{$_} eq '.') {
							$skiplog = 1;
							next;
						}
						## hack 2 : old exonic notation : 2_start_pos ; new : 2 : update in db, don't log.
						elsif ($_ eq 'Exon' && $current->{$vid}{$caid}{$_} =~ m/_/) {
							$skiplog = 1;
							next;
						}
						## hack 3 : snpeff now numbers introns also. skip log if numbered intron vs . in current
						elsif ($_ eq 'Exon' && $new->{$vid}{$naid}{$_} ne '.' && $codes{"Effect_INTRON"} eq $new->{$vid}{$naid}{'Effect'}) {
							$skiplog = 1;
							next;
						}
						else {
							#print "$_ mismatch : '$current->{$vid}{$caid}{$_}' vs  '$new->{$vid}{$naid}{$_}'\n";
							$match = 0;
							last;
						}
					}
				}
				## annotation matches
				if ($match == 1) {
					## if skiplog == 1 => update db without logging for novel exon/codonchange notations.
					$update->execute($new->{$vid}{$naid}{'CodonChange'},$new->{$vid}{$naid}{'Exon'},$caid,$vid);
					## increase counters
					$nrok++;
					$aok = 1;
					delete($new->{$vid}{$naid});
					last; # from new annotations for this caid.
				}
			}
			if ($aok == 0) {
				## annotation not found.
				$nrfail++;
				# move variant to archived table
				my $contents = '';
				foreach(keys(%{$current->{$vid}{$caid}})) {
					next if ($_ eq 'aid' || $_ eq 'vid');
					$contents .= $_ . '|||'.$current->{$vid}{$caid}{$_}.'@@@';
				}
				$contents = substr($contents,0,-4);
				$archive->execute($vid,$contents);
				my $archid = $dbh->last_insert_id(undef,undef,"Archived_Annotations",undef);
				# add to log file for samples with this variant
				#if (!defined($sidlist{$vid})) {
				#	$sids->execute($vid);
				#	$sidlist{$vid} = $sids->fetchall_arrayref();
				#}
				my $argument = "1";
				# pass archived-id as entry to fetch details.
				#foreach(@{$sidlist{$vid}}) {
					$log->execute($vid,"Variants_x_snpEff:$archid",$argument);
				#}
				# delete the annotation
				$del->execute($caid,$vid);
			}
		}
		## store new annotations for this variant (ones not deleted from hash yet)
		foreach(keys(%{$new->{$vid}})) {
			my $naid = $_;
			$nrnew++;
			my $f = $v = '';
			foreach(keys(%{$new->{$vid}{$naid}})) {
				next if ( $_ eq 'aid');
				$f .= $_.',';
				$v .= "'".$new->{$vid}{$naid}{$_}."',";
			}
			$f = substr($f,0,-1);
			$v = substr($v,0,-1);
			my $query = "INSERT INTO `Variants_x_snpEff` ($f) VALUES ($v)";
			$dbh->do($query);
			$aid = $dbh->last_insert_id(undef,undef,"Variants_x_snpEff",undef);
			
			# add to log file for samples with this variant
			#if (!defined($sidlist{$vid})) {
			#	$sids->execute($vid);
			#	$sidlist{$vid} = $sids->fetchall_arrayref();
			#}
			my $argument = "2";
			# pass archived-id as entry to fetch details.
			#foreach(@{$sidlist{$vid}}) {
				$log->execute($vid,"Variants_x_snpEff:$aid",$argument);
			#}

					
		}
	}
	$fileidx++;
}

$log->finish();
$archive->finish();
#$sids->finish();
$del->finish();
if ($usemc == 1) {
	clearMemcache("Variants_x_snpEff");
}
print "Nr of matching annotations: $nrok\n";
print "Nr of discarded annotations: $nrfail\n";
print "Nr of new annotations: $nrnew\n";

exit;


#################
## SUBROUTINES ##
#################
sub GetAnno {
	my ($fidx,$lidx) = @_;
	## local db connection
	my $connectionInfocnv="dbi:mysql:$db:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	my $i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my %return = ();
	my $sth = $dbh->prepare("SELECT * FROM `Variants_x_snpEff` WHERE vid BETWEEN $fidx AND $lidx");
	$sth->execute();
	$href = $sth->fetchall_arrayref({});
	## convert to hash{vid}{aid}
	foreach(@$href) {
		
		$return{$_->{'vid'}}{$_->{'aid'}} = $_;	
	}
	$sth->finish();
	$dbh->disconnect();
	return \%return;
}


sub AnnoSnpEff {
	my ($fidx) = @_;
	my %return;
	# convert to vcf.
	open IN, "$scriptdir/Annotations/Update.Files/Variants.$fidx.txt";
	open OUT, ">$scriptdir/Annotations/snpEff/Input_Files/Update.$fidx.vcf";
	print OUT "##fileformat=VCFv4.1\n";
	print OUT "##source=VariantDB_snpeff_annoation\n";
	print OUT "##reference=CMG_Biomina-galaxy-hg19.fa\n";
	print OUT "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n";
	while (<IN>) {
		chomp;
		my ($chr, $start, $stop, $ref, $alt, $vid) = split(/\t/,$_);
		print OUT $chromhash{$chr}."\t".$start."\t".$vid."\t".$ref."\t".$alt."\n";
	}
	close IN;
	close OUT;
	system("cd $scriptdir/Annotations/snpEff && $scriptdir/Annotations/snpEff/sun-jre-7/bin/java -Xmx4g -jar $scriptdir/Annotations/snpEff/snpEff.jar eff -v $anno $scriptdir/Annotations/snpEff/Input_Files/Update.$fidx.vcf > $scriptdir/Annotations/snpEff/Output_Files/Update.$fidx.vcf");
	my %hash = ();
	my %lines = ();
	open IN, "$scriptdir/Annotations/snpEff/Output_Files/Update.$fidx.vcf";
	# skip headers
	$ok = 0;
	while (my $line = <IN>) {
		if ($line =~ m/^#CHR/){
			$ok = 1;
			last;
		}
	}
	if ($ok == 0) {
		print "No data in output file. exiting\n";
		close IN;
		exit;
	}
	while (<IN>) {
		chomp($_);
		my @p = split(/\t/,$_);
		my $vid = $p[2];
		$p[7] =~ s/EFF=//;
		my @ia = split(/,/,$p[7]);
		my $aid = 0;
		foreach(@ia) {
			$aid++;
			my @i = split(/\(|\||\)/,$_);
			my $idx = 0;
			foreach(@i) {
				if ($i[$idx] eq '') {
					$i[$idx] = '.';
				}
				$idx++;
			}
			while ($idx <= 10){ 
				$i[$idx] = '.';
				$idx++;
			}
			## if effect == intron => set exon to . (introns are counted also)
			#if ($i[0] =~ m/INTRON/i) {
			#	$i[10] = '.';
			#}
			# encode 
			if (!defined($codes{"Effect_".$i[0]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_Effect','$i[0]')");
				$codes{"Effect_".$i[0]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[0] = $codes{"Effect_".$i[0]};	
			if (!defined($codes{"EffectImpact_".$i[1]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_EffectImpact','$i[1]')");
				$codes{"EffectImpact_".$i[1]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[1] = $codes{"EffectImpact_".$i[1]};
			if (!defined($codes{"FunctionalClass_".$i[2]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_FunctionalClass','$i[2]')");
				$codes{"FunctionalClass_".$i[2]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[2] = $codes{"FunctionalClass_".$i[2]};
			if (!defined($codes{"GeneCoding_".$i[8]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_GeneCoding','$i[8]')");
				$codes{"GeneCoding_".$i[8]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[8] = $codes{"GeneCoding_".$i[8]};
			if (!defined($codes{"TranscriptBioType_".$i[7]})) {
				$dbh->do("INSERT INTO `Value_Codes` (Table_x_Column,Item_Value) VALUES ('Variants_x_snpEff_TranscriptBioType','$i[7]')");
				$codes{"TranscriptBioType_".$i[7]} = $dbh->last_insert_id(undef,undef,undef,undef);
			}
			$i[7] = $codes{"TranscriptBioType_".$i[7]};
			
			if (!defined($return{$vid})) {
				$return{$vid} = ();
			}
			#(vid, GeneSymbol, Transcript, Exon, Effect, EffectImpact, FunctionalClass,CodonChange,AAchange,GeneCoding,TranscriptBiotype)
			$return{$vid}{$aid}{'vid'} = $vid;
			$return{$vid}{$aid}{'GeneSymbol'} = $i[6];
			$return{$vid}{$aid}{'Transcript'} = $i[9];
			$return{$vid}{$aid}{'Exon'} = $i[10];
			$return{$vid}{$aid}{'Effect'} = $i[0];
			$return{$vid}{$aid}{'EffectImpact'} = $i[1];
			$return{$vid}{$aid}{'FunctionalClass'} = $i[2];
			$return{$vid}{$aid}{'CodonChange'} = $i[3];
			$return{$vid}{$aid}{'AAchange'} = $i[4];
			$return{$vid}{$aid}{'GeneCoding'} = $i[8];
			$return{$vid}{$aid}{'TranscriptBiotype'} = $i[7];
		}
	}
	close IN;
	close OUT;
	return \%return ;
}

=cut
sub clearMemcache {
	
	## READ CREDENTIALS  
	$credpath = abs_path("../../.LoadCredentials.pl");
	require($credpath) ;
	if ($usemc != 1) {
		return;
	}
	###########################
	# CONNECT TO THE DATABASE #
	###########################
	my $db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;

	# memcached : Make sure module is available if this is set in .credentials
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	#use JSON;
	my $memd = new Cache::Memcached::Fast { 'servers' => \@mcs, 'compress_threshold' => 1_000_000, };
	my ($tables) = @_;
	my $sth = $dbh->prepare("SELECT mckey FROM `Memcache_Keys` WHERE mctable = ?");
	my @t = split(/:/,$tables);
	foreach(@t) {
		my $tname = $_;
		$sth->execute($tname);
		my $rowcache = $sth->fetchall_arrayref();
		#$tkeys = $memd->get("keys:ngs:$tname");
		#if (defined($tkeys)) {
		foreach my $row (@$rowcache) {
			my $tk = $row->[0];	

				my $t = 0;
				my $r = $memd->delete($tk);
				while (!defined($r) && $t < 100 ) {
					$t++;
					$r = $memd->delete($tk);
				}
				if (defined($r)) {
					print "key '$tk' cleared.\n";

				}
				else {
					print "unable to clear '$tk'\n";
				}

			if (defined($r)) {
				#print "key 'keys:ngs:$tname' cleared.\n";
				$dbh->do("DELETE FROM `Memcache_Keys` WHERE mckey = '$tk'");
			}

			
			
				
		}
	}	
	$sth->finish();
	$dbh->disconnect();

	$memd->disconnect_all;
}
=cut
