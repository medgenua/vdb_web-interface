#!/usr/bin/perl

use DBI;
use XML::Simple;
use Cwd 'abs_path';
use Getopt::Std;

# opts
# b : genome build (if provided)
getopts('b:', \%opts) ;

$|++;
## load credentials
$credpath = abs_path("../.LoadCredentials.pl");
require($credpath);
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
$datestring = ($year+1900)."-".($mon + 1)."-".$mday;
print "Update date: $datestring\n";
## set validation prefix & variables
our $validation = 1;
our $table_prefix = 'Validation_';

###########################
# CONNECT TO THE DATABASE #
###########################
## GET current/provided build
our $db = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
}
$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

## set VariantDB to offline.
print "Shutdown Web-Interface\n";
$dbh->do("UPDATE `NGS-Variants-Admin`.`SiteStatus` SET status = 'Construction' WHERE 1");
open OUT, ">/tmp/VDB_DownTime_Reason.inc";
print OUT "<br/><span color=red'>Reason:</span> Annotation sources are being updated. This includes downloading fresh data tables, annotating all variants in the database and comparing new and old annotations. <br/><br/>You will receive an email upon completion if there are possibly novel interesting annotations found. This process takes a while, but is only run once a month.";
close OUT;

##################
## HPC settings ##
##################
if ($usehpc == 1) {
	if ($queue eq '' || !defined($queue)) {
		$queue = 'batch';
	}
	if (defined($account) && $account ne '') {
		$account = "#PBS -A $account\n";
	}
}

####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";

## get build replacement values
my %builds = ();
my $sth = $dbh->prepare("SELECT Annotation, Value FROM `Annotation_DataValues`");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$builds{$r[0]} = $r[1];
}
$sth->finish();

my %depends = ();
my %jobids = ();
$depends{"GO_Annotations"} = $builds{'snpEff'}; #"GRCh37.66"  ; # from snpEff
$depends{"GenePanels"} = "refgene"; # from annovar.
my %downloaded1kg = ();

system("mkdir -p '$scriptdir/Annotations/Update.Files/$datestring'");

print "Dump variants in batches of 100,000 items\n";
# first clean up old files.
system("rm -f $scriptdir/Annotations/Update.Files/Variants.*.txt");
## DUMP ALL Variants, in batches of 100K Variants.
my $sth = $dbh->prepare("SELECT id, chr, start, stop, RefAllele, AltAllele FROM `Variants` ORDER BY id");
my $nrrows = $sth->execute();
if ($nrrows < 100000) {
	$max = $nrrows;
}
else {
	$max = 100000;
}
$i = 0;
while (my $rowcache = $sth->fetchall_arrayref(undef,$max)) {
	$i++;
	print "  - Batch $i\n";
	open OUT, ">$scriptdir/Annotations/Update.Files/Variants.$i.txt";
	$out = '';
	foreach my $result (@$rowcache)  {
		$result->[1] = $chromhash{$result->[1]};
		if ($result->[5] =~ m/N/) {
			#print "WARNING: $result->{'AltAllele'} has -N-. This is replaced by -A- for annotation\n";
			$result ->[5] =~ s/N/A/g;
		}
		$out .= $result->[1]."\t".$result->[2]."\t".$result->[3]."\t".$result->[4]."\t".$result->[5]."\t".$result->[0]."\n";
	}
	print OUT $out;
	close OUT;
}
$sth->finish();
my $nrfiles = $i;
## fetch latest version of annovar file sizes.
print "Fetch annovar file sizes\n";
my $adb = $db;
$adb =~ s/NGS-Variants-//;
if (-e "$scriptdir/Annotations/Update.Files/$adb"."_avdblist.txt") {
	unlink("$scriptdir/Annotations/Update.Files/$adb"."_avdblist.txt");
}
#my $result = system("wget -q -O '$scriptdir/Annotations/Update.Files/Annovar.sizes.txt' --timeout=10s -t 2 'http://www.openbioinformatics.org/annovar/download/filesize.txt'");
system("cd $scriptdir/Annotations/ANNOVAR && perl annotate_variation.pl -buildver $adb -downdb -webfrom annovar avdblist '$scriptdir/Annotations/Update.Files/' >/dev/null 2>&1");
if (!-e "$scriptdir/Annotations/Update.Files/$adb"."_avdblist.txt" || !-s  "$scriptdir/Annotations/Update.Files/$adb"."_avdblist.txt") { 
#if ($result != 0) {
	print "Annovar website not available. Update will exit here and now. \n";
	print "download exit code was $return\n";
	print "restore access\n";
	print "Restore Platform Status to Active\n";
	$dbh->do("UPDATE `NGS-Variants-Admin`.`SiteStatus` SET status = 'Operative' WHERE 1");
	unlink('/tmp/VDB_DownTime_Reason.inc');
	# notify system admin
	open OUT, ">/tmp/annovar.error.mail.txt";
	print OUT "to: $adminemail\n";
	print OUT "subject: VariantDB Annotation Update Failed\n";
	print OUT "from: $adminemail\n\n";
	
	print OUT "Dear system administrator,\r\n\r\n";
	print OUT "VariantDB annotations were not updated due to a ANNOVAR being not accessible.\r\n";
	
	print OUT "\r\nBest Regards,\r\nThe VariantDB update script.\r\n";
	close OUT;
	system("sendmail -t < '/tmp/annovar.error.mail.txt'");
	system("rm /tmp/annovar.error.mail.txt");
	exit;
}
my %annovarsizes = ();
#open IN, "$scriptdir/Annotations/Update.Files/Annovar.sizes.txt";
open IN, "$scriptdir/Annotations/Update.Files/$adb"."_avdblist.txt";
while (<IN>) {
	chomp;
	my ($f,$d,$s) = split(/\t/,$_);
	$annovarsizes{$f} = $s;
}
#unlink("$scriptdir/Annotations/Update.Files/Annovar.sizes.txt");

print "Get some replacement values from the DB\n";
## get replacement values from database. These values are used to make the script build independent
my $sth = $dbh->prepare("SELECT Annotation, Value FROM `Annotation_DataValues`");
$sth->execute();
my $aref = $sth->fetchall_arrayref();
my $dbvalues;
foreach(@$aref) {
	$dbvalues{$_->[0]} = $_->[1];
}
$sth->finish();


my $xml = new XML::Simple;
# this xml file contains all the parameter/method settings.
my $settings = $xml->XMLin("Config.xml");
## processed items
my %queued;
## some datalocations
my %datalocations;
$datalocations{'ANNOVAR'} = 'humandb';
$datalocations{'snpEff'} = '.';
my %delayed;
my %jobs;
my %skipped;
my %statusfiles = ();
## loop settings
PROCESS:
foreach my $subdir (keys %{$settings}) {
	$subdir =~ s/^_//;
	## depends and not yet run => delay
	my $dependid = '';
	if (defined($depends{$subdir}) ){
		if (!defined($delayed{$subdir}) ) { 
			if (defined($skipped{$depends{$subdir}})) {
				# dependency was up to date.
				# proceed as normal.
			}
			else {
				$delayed{$subdir} = 1;
				next;
			}
		}
		else {
			if (defined($skipped{$depends{$subdir}})) {
				print "Delayed submission of $subdir. Dependency was skipped.\n";
			}
			else {
				$dependid = $jobs{$depends{$subdir}};
				print "Delayed submission of $subdir. Depends on job $dependid\n";
			}
			delete($delayed{$subdir});
		}
	}
	
	$output .= "Checking: $subdir\n";
	
	foreach my $anno (keys(%{$settings->{$subdir}})) {
		print "Checking $subdir -- $anno\n";
		# deprecated : no longer updated.
		if (exists($settings->{$subdir}->{$anno}->{'deprecated'})) {
			print "  => deprecated\n";
			next;
		}

		## get AnnotationSource
		if (!defined($settings->{$subdir}->{$anno}->{AnnotationSource})) {
			print "  => no annotation source\n";
			next;
		}
		$annosource = $settings->{$subdir}->{$anno}->{AnnotationSource};
		# multiple annotation entries can be loaded from a single source. Process once.
		if (defined($queued{"$subdir-$annosource"})) {
			print "  => handled before\n";
			next;
		}
		if ($annosource eq 'none') {
			$queued{"$subdir-$annosource"} = "$subdir/$anno";
			print "  => no annotation source\n";
			next;
		}
		my $renamed = $anno;
		## skip if no db-file is specified (apart from the depended jobs)
		my $dbfile = '';
		if (!defined($depends{$subdir}) && !defined($settings->{$subdir}->{$anno}->{AnnotationFile})) {
			print "  => no source for dependency.\n";
			next;
		}
		else {
			$dbfile = $settings->{$subdir}->{$anno}->{AnnotationFile};
		}
		my $replace = $dbvalues{$subdir};
		$dbfile =~ s/\%build\%/$replace/g;	
		$queued{"$subdir-$annosource"} = "$subdir/$anno";
		my $afterok = '';
		if ($dependid ne '') {
				$afterok = "#PBS -W depend=afterok:$dependid\n";
		}

		# ANNOVAR
		if ($subdir eq 'ANNOVAR') {
			my $skip = 0;
			## 1kg data comes in zip files for multiple tables. unable to check. download zip, unpack to tmp, compare then.
			if ($annosource =~ m/(1000g.*)_(.*)/) {
				my $dl = $1;
				if (!defined($downloaded1kg{$dl})) {
					if (-d "/tmp/1kg.update/$dl") {
						system("rm /tmp/1kg.update/$dl/*");
					}
					else {
						system("mkdir -p /tmp/1kg.update/$dl/*");
					}
					system("cd $scriptdir/Annotations/ANNOVAR && perl annotate_variation.pl -buildver $replace -downdb -webfrom annovar '$dl' /tmp/1kg.update/$dl/");
					$downloaded1kg{$dl} = 1;
				}
				my $fs = -s "$scriptdir/Annotations/ANNOVAR/humandb/$dbfile";
				my $nfs = -s "/tmp/1kg.update/$dl/$dbfile";
				if ($fs == $nfs) {
					print "  => ANNOVAR $dbfile is up to date, skipping\n";
					$skip = 1;
					$skipped{$annosource} = 1;
					system("rm /tmp/1kg.update/$dl/$dbfile*");
				}
				else {
					system("mv /tmp/1kg.update/$dl/$dbfile* $scriptdir/Annotations/ANNOVAR/humandb/");
				}
			}
			elsif (defined($annovarsizes{"$dbfile.idx.gz"})) {
				if (-e "$scriptdir/Annotations/ANNOVAR/humandb/$dbfile.idx") {
					system("gzip -c '$scriptdir/Annotations/ANNOVAR/humandb/$dbfile.idx' > /tmp/$dbfile.idx.gz");
					my $fs = -s "/tmp/$dbfile.idx.gz";
					unlink("/tmp/$dbfile.idx.gz");
					if ($fs == $annovarsizes{"$dbfile.idx.gz"}) {
						print "  => ANNOVAR $dbfile index is up to date (used as proxy for the full DB). skipping.\n";
						$skip = 1;
						$skipped{$annosource} = 1;
					}
					else {
						print "  => ANNOVAR $dbfile.idx changed ($fs vs ".$annovarsizes{"$dbfile.idx.gz"}."). Updating\n";
					}

				}
			}
			elsif (defined( $annovarsizes{"$dbfile.gz"} )) {
				system("gzip -c '$scriptdir/Annotations/ANNOVAR/humandb/$dbfile' > /tmp/$dbfile.gz");
				my $fs = -s "/tmp/$dbfile.gz";
				unlink("/tmp/$dbfile.gz");
				if ($fs == $annovarsizes{"$dbfile.gz"}) {
					print "  => ANNOVAR $dbfile is up to date. skipping.\n";
					$skip = 1;
					$skipped{$annosource} = 1;
				}
				else {
					print "  => ANNOVAR $dbfile changed ($fs vs ".$annovarsizes{"$dbfile.gz"}."). Updating\n";
				}

			}
			else {
				print "  => specified DB file is not available from ANNOVAR. Skipping.\n";
				$skip = 1;
				$skipped{$annosource} = 1;
			}
	
			if ($skip == 0) {
				
				## download 1000g files once per release.
				if ($annosource =~ m/(1000g.*)_(.*)/) {
					my $dl = $1;
					if (!defined($downloaded1kg{$dl})) {
						#open OUT, ">$scriptdir/Annotations/Update.Files/ANNOVAR.download.$dl.$datestring.sh";
						#print OUT "#!/bin/bash\n";
						#print OUT "#PBS -N Download.$dl\n";
						#print OUT "#PBS -l mem=2g\n";
						#print OUT "#PBS -q batch\n";
						#print OUT "#PBS -o $scriptdir/Annotations/job_output/Download.$dl.o.txt\n";	
						#print OUT "#PBS -e $scriptdir/Annotations/job_output/Update.$dl.e.txt\n";
						#print OUT "#PBS -d $scriptdir/Annotations/$subdir\n";
						#print OUT "#PBS -m a\n";
						#print OUT "#PBS -M $adminemail\n";
						#print OUT "$afterok";
						#print OUT "\n";
						system("cd $scriptdir/Annotations/ANNOVAR && perl annotate_variation.pl -buildver $replace -downdb -webfrom annovar '$dl' '$table_prefix"."humandb/'");
						#close OUT;
						$downloaded1kg{$dl} = 1;
						
					}
				}
				# mk qsub script
				open OUT, ">$scriptdir/Annotations/Update.Files/ANNOVAR.$annosource.$datestring.sh";
				print OUT "#!/bin/bash\n";
				print OUT "#PBS -N Update.$annosource\n";
				print OUT "#PBS -l mem=24g\n";
				print OUT "#PBS -q $queue\n";
				print OUT "#PBS -o $scriptdir/Annotations/job_output/Update.$annosource.o.txt\n";	
				print OUT "#PBS -e $scriptdir/Annotations/job_output/Update.$annosource.e.txt\n";
				print OUT "#PBS -d $scriptdir/Annotations/$subdir\n";
				print OUT "#PBS -m a\n";
				print OUT "#PBS -M $adminemail\n";
				print OUT $account;
				print OUT $afterok;
				print OUT "\n";
				print OUT "perl UpdateAnnotations.pl -a $annosource\n";
				print OUT "echo 1 > $scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt\n";
				close OUT;
				if ($usehpc == 1) {
					my $jid = `qsub '$scriptdir/Annotations/Update.Files/ANNOVAR.$annosource.$datestring.sh'`;
					chomp($jid);
					$jobs{$annosource} = $jid;
					$statusfiles{$annosource} = "$scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt";
				}
				else {
					system("chmod u+x '$scriptdir/Annotations/Update.Files/ANNOVAR.$annosource.$datestring.sh' && cd '$scriptdir/Annotations/$subdir' && $scriptdir/Annotations/Update.Files/ANNOVAR.$annosource.$datestring.sh >$scriptdir/Annotations/job_output/Update.$annosource.o.txt 2>$scriptdir/Annotations/job_output/Update.$annosource.e.txt");
					$jobs{$annosource} = 1;
					$statusfiles{$annosource} = "$scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt";
				}
			}
		}	
					
		elsif($subdir eq 'snpEff') {
			# get checksum of latest (if any)
			my $md5 = $fn = '';
			if (-e "$scriptdir/Annotations/Update.Files/snpEff.latest.md5") {
				my $line = `cat '$scriptdir/Annotations/Update.Files/snpEff.latest.md5'`;
				chomp($line);
				($md5,$fn) = split(/\s+/,$line);
			}	
			$annosource =~ s/\%build\%/$dbvalues{$subdir}/;
			my $new_annosource = $annosource;
=cut TODO : snpEff JAR is outdated, cannot connect to download server anymore. 
	    => snpEff is currently NOT updated during monthly rotations.
	    => Update to new release && validate changes.

			# check new ensembl release in snpEff databases
			$annosource =~ m/GRCh(\d+)\.(\d+)$/;
			my $main_release = $1;
			my $sub_release = $2;
			my @releases = `$scriptdir/Annotations/snpEff/sun-jre-7/bin/java -jar $scriptdir/Annotations/snpEff/snpEff.jar databases | cut -f 1 | grep 'GRCh$main_release'`;
			chomp(@releases);
			my $highest = $sub_release;
			foreach(@releases) {
				if ($_ =~ m/^GRCh$main_release\.(\d+)$)/) {
					if ($1 > $sub_release) {
						$sub_release = $1;
					}
				}
			}
			my $new_annosource = "GRCh$main_release.$sub_release";
			# always download (maybe new version of same release?)
=cut
			# download to temporary location, then compare filesizes. It immediately overwrites data of the same release!
			## this is not important in case of identical version, but might be important on updates. Therefore : take backup.
			if (-d "$scriptdir/Annotations/snpEff/data/$annosource") {
				system("cd $scriptdir/Annotations/snpEff/data && cp -Rf '$annosource' '$annosource.$datestring'");
			}
			## download the latest version.
			if (!-d "$scriptdir/Annotations/Update.Files/snpeff.tmp") {
				system("mkdir -p '$scriptdir/Annotations/Update.Files/snpeff.tmp'");
			}
			else {
				system("rm $scriptdir/Annotations/Update.Files/snpeff.tmp/*zip");
			}
			my $command = "cd $scriptdir/Annotations/Update.Files/snpeff.tmp && $scriptdir/Annotations/snpEff/sun-jre-7/bin/java -jar $scriptdir/Annotations/snpEff/snpEff.jar download $new_annosource >/dev/null 2>&1";
			system($command);
			$line = `md5sum $scriptdir/Annotations/Update.Files/snpeff.tmp/*zip | head -n 1`; # should only contain 1 zip file. Just to make sure 
			chomp($line);
			my ($nmd5,$nfn) = split(/\s+/,$line);
			if ($md5 eq $nmd5) {
				print "snpEff $annosource is up to date. Skipping\n";
				$skipped{$annosource} = 1;
			}
			else {
				print "snpEff $annosource changed. Updating\n";
				open OUT, ">$scriptdir/Annotations/Update.Files/snpEff.latest.md5";
				print OUT "$line\n";
				close OUT;
				# mk qsub script
				open OUT, ">$scriptdir/Annotations/Update.Files/snpEff.$annosource.$datestring.sh";
				print OUT "#!/bin/bash\n";
				print OUT "#PBS -N Update.$annosource\n";
				print OUT "#PBS -l nodes=1:ppn=4,mem=24g\n";
				print OUT "#PBS -q $queue\n";
				print OUT "#PBS -o $scriptdir/Annotations/job_output/Update.$annosource.o.txt\n";	
				print OUT "#PBS -e $scriptdir/Annotations/job_output/Update.$annosource.e.txt\n";
				print OUT "#PBS -d $scriptdir/Annotations/$subdir\n";
				print OUT "#PBS -m a\n";
				print OUT "#PBS -M $adminemail\n";
				print OUT $account;
				print OUT $afterok;
				print OUT "\n";
				print OUT "perl UpdateAnnotations.pl -a $new_annosource\n";
				print OUT "echo 1 > $scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt\n";
				close OUT;
				$statusfiles{$annosource} = "$scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt";
				if ($usehpc == 1) {
					my $jid = `qsub '$scriptdir/Annotations/Update.Files/snpEff.$annosource.$datestring.sh'`;
					chomp($jid);
					$jobs{$annosource} = $jid;
				}
				else {
					system("chmod u+x '$scriptdir/Annotations/Update.Files/snpeff.$annosource.$datestring.sh' && cd '$scriptdir/Annotations/$subdir' && $scriptdir/Annotations/Update.Files/snpEff.$annosource.$datestring.sh >$scriptdir/Annotations/job_output/Update.$annosource.o.txt 2>$scriptdir/Annotations/job_output/Update.$annosource.e.txt");
					$jobs{$annosource} = 1;
					
				}
			}	
		}
		elsif($subdir eq 'ClinVar') {
			system("wget -q 'ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/xml/ClinVarFullRelease_00-latest.xml.gz' -O '$scriptdir/Annotations/Update.Files/ClinVarLatest.xml.gz' 2>/dev/null");
			if (-e "$scriptdir/Annotations/Update.Files/ClinVarLatest.xml.gz") {
				my $vstring = `zcat $scriptdir/Annotations/Update.Files/ClinVarLatest.xml.gz | head -n 2 | tail -n 1`;
				$vstring =~ m/Dated="(\d{4}-\d{2}-\d{2})"/;
				$online = $1;
			}
			else {
				$online = '';
			}
			if (!-e "$scriptdir/Annotations/ClinVar/ClinVarFullRelease_Loaded.xml") {
				$local = '';
			}
			else {
				$lstring = `head -n 2 $scriptdir/Annotations/ClinVar/ClinVarFullRelease_Loaded.xml | tail -n 1`;
				$lstring =~ m/Dated="(\d{4}-\d{2}-\d{2})"/;
				$local = $1;
			}
			if ("$online" eq "$local") {
				print "ClinVar up to date. Skipping\n";
				$skipped{$annosource} = 1;
			}
			else {
				print "ClinVar changed. Updating\n";
				# mk qsub script
				open OUT, ">$scriptdir/Annotations/Update.Files/ClinVar.$datestring.sh";
				print OUT "#!/bin/bash\n";
				print OUT "#PBS -N Update.ClinVar\n";
				print OUT "#PBS -l nodes=1:ppn=1,mem=24g\n";
				print OUT "#PBS -q $queue\n";
				print OUT "#PBS -o $scriptdir/Annotations/job_output/Update.ClinVar.o.txt\n";	
				print OUT "#PBS -e $scriptdir/Annotations/job_output/Update.ClinVar.e.txt\n";
				print OUT "#PBS -d $scriptdir/Annotations/$subdir\n";
				print OUT "#PBS -m a\n";
				print OUT "#PBS -M $adminemail\n";
				print OUT $afterok;
				print OUT $account;
				print OUT "\n";
				print OUT "perl UpdateAnnotations.pl -a ClinVar\n";
				print OUT "echo 1 > $scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt\n";
				close OUT;
				$statusfiles{$annosource} = "$scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt";
				if ($usehpc == 1) {
					my $jid = `qsub '$scriptdir/Annotations/Update.Files/ClinVar.$datestring.sh'`;	
					chomp($jid);
					$jobs{'ClinVar'} = $jid;
					
				}
				else {
					system("chmod u+x '$scriptdir/Annotations/Update.Files/ClinVar.$datestring.sh' && cd '$scriptdir/Annotations/$subdir' && $scriptdir/Annotations/Update.Files/ClinVar.$datestring.sh >$scriptdir/Annotations/job_output/Update.ClinVar.o.txt 2>$scriptdir/Annotations/job_output/Update.ClinVar.e.txt");
					$jobs{$annosource} = 1;
				}
			}
		}
		elsif($subdir eq 'GATK_Annotations') {
			# GATK is static (once read on import of VCF file)
			next;
		}
		elsif ($subdir eq 'GO_Annotations') {
			my $url = 'http://archive.geneontology.org/latest-termdb/go_daily-termdb-tables.tar.gz';
			my $md5 = $fn = '';
			if (-e "$scriptdir/Annotations/Update.Files/GO.latest.md5") {
				my $line = `cat '$scriptdir/Annotations/Update.Files/GO.latest.md5'`;
				chomp($line);
				($md5,$fn) = split(/\s+/,$line);
			}	
			my $url = 'http://archive.geneontology.org/latest-termdb/go_daily-termdb-tables.tar.gz';
			$url =~ m/http:\/\/.*\/(\S+)\.tar\.gz/;
			my $tzfile = "$scriptdir/Annotations/Update.Files/$1.tar.gz";
			system("rm -f '$tzfile'");
			## get 
			system("cd $scriptdir/Annotations/Update.Files/ && wget '$url' && tar xzf $tzfile");
			$line = `md5sum $tzfile`;
			chomp($line);
			my ($nmd5,$nfn) = split(/\s+/,$line);
			if ($md5 eq $nmd5) {
				print "Gene_Ontology is up to date. Skipping\n";
				$skipped{$annosource} = 1;
			}
			else {
				print "Gene Ontology changed. Updating\n";
				open OUT, ">$scriptdir/Annotations/Update.Files/GO.latest.md5";
				print OUT "$line\n";
				close OUT;
				# mk qsub script
				open OUT, ">$scriptdir/Annotations/Update.Files/GeneOntology.$datestring.sh";
				print OUT "#!/bin/bash\n";
				print OUT "#PBS -N Update.GO\n";
				print OUT "#PBS -l nodes=1:ppn=1,mem=24g\n";
				print OUT "#PBS -q $queue\n";
				print OUT "#PBS -o $scriptdir/Annotations/job_output/Update.GeneOntology.o.txt\n";	
				print OUT "#PBS -e $scriptdir/Annotations/job_output/Update.GeneOntology.e.txt\n";
				print OUT "#PBS -d $scriptdir/Annotations/GO_Annotations\n";
				print OUT "#PBS -m a\n";
				print OUT "#PBS -M $adminemail\n";
				print OUT $afterok;
				print OUT $account;
				print OUT "\n";
				print OUT "perl UpdateAnnotations.pl\n";
				print OUT "echo 1 > $scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt\n";
				close OUT;
				$statusfiles{$annosource} = "$scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt";
				if ($usehpc == 1) {
					my $jid = `qsub '$scriptdir/Annotations/Update.Files/GeneOntology.$datestring.sh'`;
					chomp($jid);
					$jobs{$annosource} = $jid;
					
				}
				else {
					system("chmod u+x '$scriptdir/Annotations/Update.Files/GeneOntology.$datestring.sh' && cd '$scriptdir/Annotations/$subdir' && $scriptdir/Annotations/Update.Files/GeneOntology.$datestring.sh >$scriptdir/Annotations/job_output/Update.$annosource.o.txt 2>$scriptdir/Annotations/job_output/Update.$annosource.e.txt");
					$jobs{$annosource} = 1;
				}
			}
		}
		elsif ($subdir eq 'GenePanels') {
			if (defined($skipped{$depends{$subdir}})) {
				print "RefGene was not changed. No need to update GenePanels.\n";
				$skipped{'GenePanels'} = 1;
			}
			else {	
				print "Updating Gene Panels\n";
				# mk qsub script
				open OUT, ">$scriptdir/Annotations/Update.Files/GenePanels.$datestring.sh";
				print OUT "#!/bin/bash\n";
				print OUT "#PBS -N Update.GP\n";
				print OUT "#PBS -l nodes=1:ppn=1,mem=24g\n";
				print OUT "#PBS -q $queue\n";
				print OUT "#PBS -o $scriptdir/Annotations/job_output/Update.GenePanels.o.txt\n";	
				print OUT "#PBS -e $scriptdir/Annotations/job_output/Update.GenePanels.e.txt\n";
				print OUT "#PBS -d $scriptdir/Annotations/GenePanels\n";
				print OUT "#PBS -m a\n";
				print OUT "#PBS -M $adminemail\n";
				print OUT $afterok;
				print OUT $account;
				print OUT "\n";
				print OUT "perl UpdateAnnotations.pl\n";
				print OUT "echo 1 > $scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt\n";
				close OUT;
				$statusfiles{$annosource} = "$scriptdir/Annotations/Update.Files/$datestring/status.$annosource.txt";
				if ($usehpc == 1) {
					my $jid = `qsub '$scriptdir/Annotations/Update.Files/GenePanels.$datestring.sh'`;
					chomp($jid);
					$jobs{$annosource} = $jid;
					
				}
				else {
					system("chmod u+x '$scriptdir/Annotations/Update.Files/GenePanels.$datestring.sh' && cd '$scriptdir/Annotations/$subdir' && $scriptdir/Annotations/Update.Files/GenePanels.$datestring.sh >$scriptdir/Annotations/job_output/Update.$annosource.o.txt 2>$scriptdir/Annotations/job_output/Update.$annosource.e.txt");
					$jobs{$annosource} = 1;
				}
			}

		}	
		elsif($subdir eq 'WebTools' ) {
			# todo.
			next;
		}
		else {
			next;
			#$output .= "###############\n";
			#$output .= "## WARNING : ##\n";
			#$output .= "###############\n";
			#$output .= "  $subdir/$anno not recognised by database-downloader. \n\n";
		}
		#$output .= "Downloading of $subdir/$anno finished. \n";
	}
}
if (keys(%delayed) > 0) {
	@subdirs = keys(%delayed);		
	print scalar(@subdirs) . " annotations were delayed.\n";
	#%delayed = ();
	$firstloop = 0;
	goto PROCESS;
}
###########################################
## IF DISTRIBUTED JOBS, wait for finish. ##
###########################################
if (keys(%jobs) == 0) {
	print "No Tables updated. Not processing Emailing\n";
	exit;
}
my $finished = 0;
while ($finished == 0) {
	$finished = 1;
	foreach(keys(%statusfiles)) {
		# status file for annosource present?
		if (!-f $statusfiles{$_}){
			$finished = 0;
			sleep 10;
			last;
		}
	}
}
=cut
if (-e "$scriptdir/Annotations/Update.Files/Finished.txt") {
	system("rm '$scriptdir/Annotations/Update.Files/Finished.txt'");
}
if ($usehpc == 1) {
	open OUT, ">$scriptdir/Annotations/Update.Files/Status.sh";
	print OUT "#!/usr/bin/env bash\n";
	print OUT "#PBS -d $scriptdir\n";
	print OUT "#PBS -m ae\n";
	print OUT "#PBS -M $adminemail\n";
	print OUT "#PBS -l nodes=1:ppn=1,mem=1g\n";
	print OUT "#PBS -N VariantDB.Wait.Update.Finish\n";
	print OUT "#PBS -o $scriptdir/Annotations/Update.Files/Status.o.txt\n";
	print OUT "#PBS -e $scriptdir/Annotations/Update.Files/Status.e.txt\n";
	print OUT "#PBS -q $queue\n";
	print OUT $account;
	my $after = '';
	foreach(keys(%jobs)) {
		my $job = $jobs{$_};
		my $out = `qstat $job`;
		chomp($out);
		if ($out ne '' && $out !~ m/Unknown Job/i) {
			$after .= ":$jobs{$_}";
		}
	}
	print OUT "#PBS -W depend=afterok$after\n";
	print OUT "echo 1 > $scriptdir/Annotations/Update.Files/Finished.txt\n";
	close OUT; 
	my $sID = `qsub $scriptdir/Annotations/Update.Files/Status.sh`;
	while ($sID !~ m/^\d+\..*/){
		sleep 1;
		$sID = `qsub $scriptdir/Annotations/Update.Files/Status.sh`;
	}
	my $finished = 0;
	while ($finished == 0) {
		if (-e "$scriptdir/Annotations/Update.Files/Finished.txt") {
			$finished = 1;
		}
		else {
			sleep 60;
		}
	}
}
=cut
FINALIZE:
## CLEAR ALL CACHES.
$dbh->do("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE 1");
## Clear all VIEWS
my $query = "SELECT `name` FROM `VIEWS` WHERE `date` < DATE_SUB(CURDATE(),INTERVAL -2 DAY)";
my $sth = $dbh->prepare($query);
my $nr = $sth->execute();
while (my @row = $sth->fetchrow_array()) {
	$dbh->do("DELETE FROM `VIEWS` WHERE `name` = '$row[0]'");
	$dbh->do("DROP TABLE IF EXISTS `$row[0]`");
}
$sth->finish();

# move ANNOVAR tables to final destination.
system("cd $scriptdir/Annotations/ANNOVAR && mv $table_prefix"."humandb/* humandb/");

## set VariantDB to online.
print "Restore Platform Status to Active\n";
$dbh->do("UPDATE `NGS-Variants-Admin`.`SiteStatus` SET status = 'Operative' WHERE 1");
unlink('/tmp/VDB_DownTime_Reason.inc');



MAILS:
## Compose user emails.
my $users = $dbh->prepare("SELECT id, email, LastName, FirstName FROM `Users` WHERE level > 0"); 
$users->execute();
my $ucache = $users->fetchall_arrayref();
$users->finish();

# types to report (build first-pass regex against 'entry')
$regex = 'ANNOVAR_refgene|ClinVar|GenePanels|snpEff';	

## get value codes for relevant items.
my $csth = $dbh->prepare("SELECT * FROM `Value_Codes`"); 
$csth->execute();
my $ccache = $csth->fetchall_arrayref();
my %rcodes;
my %codes;
foreach(@$ccache){
	$codes{$_->[0]} = $_->[2];
	# relevant? 
	next if ($_->[1] !~ m/$regex/) ;
	# refgene
	if ($_->[1] eq "Variants_x_ANNOVAR_refgene_VariantType" && $_->[2] =~ m/frameshift|stop|nonsynonymous|splicing/) {
		$rcodes{'Variants_x_ANNOVAR_refgene_VariantType'}{$_->[0]} = $_->[2];
	}
	# clinvar exact match on Amino acid.
	elsif ($_->[1] eq "Variants_x_ClinVar_AA_MatchType" && $_->[2] eq 'match') {
		$rcodes{'Variants_x_ClinVar_AA_MatchType'}{$_->[0]} = $_->[2];
	}
	# clinvar class pathogenic (exclude conflicting entries)
	elsif($_->[1] eq "Variants_x_ClinVar_Class" && $_->[2] =~ m/pathogenic/i && $_->[2] !~ m/Conflicting interpretations/i) {
		$rcodes{'Variants_x_ClinVar_Class'}{$_->[0]} = $_->[2];
	}
	# snpEff
	elsif($_->[1] eq 'Variants_x_snpEff_Effect' && $_->[2] =~ m/DELETION|INSERTION|FRAME_SHIFT|NON_SYNONYMOUS|LOST|GAINED|ACCEPTOR|DONOR/) {
		$rcodes{'Variants_x_snpEff_Effect'}{$_->[0]} = $_->[2];
	}
}
## prepare queries
$ssth = $dbh->prepare("SELECT s.Name AS sname,s.id,p.Name AS pname FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects_x_Users` pu JOIN `Projects` p ON s.id = ps.sid AND ps.pid = p.id  AND ps.pid = pu.pid WHERE pu.uid = ?");
$asth = $dbh->prepare("SELECT Contents FROM `Archived_Annotations` WHERE aid = ? ");
$vsth = $dbh->prepare("SELECT chr,start,RefAllele,AltAllele FROM `Variants` WHERE id = ?"); 
$cvsth = $dbh->prepare("SELECT vc.AA_MatchType, cvdb.Class, vc.cvid,cvdb.VarType,cvdb.Gene,cvdb.Disease,cvdb.Class FROM `Variants_x_ClinVar` vc JOIN `ClinVar_db` cvdb ON cvdb.id = vc.cvid WHERE vc.aid = ? AND vid = ?");
my $refgenesth = $dbh->prepare("SELECT * FROM `Variants_x_ANNOVAR_refgene` WHERE aid = ? AND vid = ?");
my $snpeffsth = $dbh->prepare("SELECT * FROM `Variants_x_snpEff` WHERE aid = ? AND vid = ?");
## hashes to store previously fetched data.
my %aanno;
my %done;
my %variants;
my %var_gene;
# build log cache
print "Loading Log entries.\n";
$lsth = $dbh->prepare("SELECT l.vid, l.entry, l.arguments FROM `Variants_x_Log` l WHERE l.time > CURDATE() - INTERVAL 2 DAY AND l.arguments IN (1,2)");
my $nrr = $lsth->execute();
if ($nrr < 50000) {
	$max = $nrr;
}
else {
	$max = 50000;
}
if ($nrr == 0) {
	print "nothing to do, exit.\n";
	exit;
}
my $lcache; 
my %log_hash = ();
while (my $result = shift(@$lcache) || shift( @{$lcache = $lsth->fetchall_arrayref(undef,$max)|| []})) {
	# per variant, per type (del/new) => entry(s)
	next if ($result->[1] !~ m/$regex/) ;
	$log_hash{$result->[0]}{$result->[2]}{$result->[1]} = 1;
}
$lsth->finish();
print "  => ".keys(%log_hash). " variants added with possibly relevant changes.\n";
## process
foreach(@$ucache) {
	## don't process if no log entries.
	next if (keys(%log_hash) == 0);
	# get entries
	my ($uid, $umail,$lname,$fname) = @$_;
	print "Checking data for $fname $lname\n";
	## samples user has access to.
	$ssth->execute($uid);
	my $scache = $ssth->fetchall_arrayref();
	my %samples = ();
	my %project = ();
	my %vxs = ();
	my $sidstring = '';
	foreach(@$scache) {
		$sidstring .= $_->[1].",";
		$samples{$_->[1]} = $_->[0];
		$projects{$_->[1]} = $_->[2];
		#$projects{$_->[2]}{$_->[1]} = 1;
	}
	# user has no samples
	if ($sidstring eq '') {
		print "\tUser $uid has no samples\n";
		next;
	}
	$sidstring = substr($sidstring,0,-1);
	my %emailcontents = ();
	my %vidsid = ();
	## for speed: get all vid/sid combinations, compare to pre-fetchs log_hash.	
	$lsth = $dbh->prepare("SELECT vs.sid,vs.vid  FROM `Variants_x_Samples` vs WHERE vs.sid IN ($sidstring)");
	my $nrr = $lsth->execute();
	print "  $nrr variant-sample entries in total\n";
	if ($nrr < 50000) {
		$max = $nrr;
	}
	else {
		$max = 50000;
	}
	if ($nrr == 0) {
		next;
	}
	my $lcache; 
	my $uidx = 0;
	while (my $result = shift(@$lcache) || shift( @{$lcache = $lsth->fetchall_arrayref(undef,$max)|| []})) {
		## skip if not in log 
		next if (!defined($log_hash{$result->[1]}));
		## assign
		my ($sid,$vid) = @$result;
		for (my $arguments = 2; $arguments >= 1; $arguments--) {  # these are deleted/added events.
		   # skip if no entry of this type for this variant.
		   next if (!defined($log_hash{$vid}{$arguments}));
		   $uidx++;
		   foreach(keys(%{$log_hash{$vid}{$arguments}})) {  # go over all entries for this type/variant
		      my $entry = $_;
		      # first pass for variant, check if relevant
		      if (!defined($done{$vid."-".$entry."-".$arguments}) ) {
			$entry =~ m/(.*):(\d+)/;
			my $table = $1;
			my $aid = $2;
			## ARGUMENTS : 1 == deleted ; 2 == added ; 3 == updated
			## if deleted : get relevant previous items
			if ($arguments == 1) {
				if ($table eq 'Variants_x_ANNOVAR_refgene') {
					# get variant types of archived annotations for this variant.
					$asth->execute($aid);
					my ($contents) = $asth->fetchrow_array();
					my @p = split(/@@@/,$contents);
					my %avtypes;
					foreach(@p) {
						my ($c,$v) = split(/\|\|\|/,$_);
						if ($c eq 'VariantType') {
							$aanno{$vid}{'rg'}{$v} = 1;
							last;
						}
					}
				}
				elsif($table eq 'Variants_x_snpEff') {
					# get variant types of archived annotations for this variant.
					$asth->execute($aid);
					my ($contents) = $asth->fetchrow_array();
					my @p = split(/@@@/,$contents);
					my %avtypes;
					foreach(@p) {
						my ($c,$v) = split(/\|\|\|/,$_);
						if ($c eq 'Effect') {
							$aanno{$vid}{'se'}{$v} = 1;
							last;
						}
					}
				}
				else {
					#print "Deletion from $table not monitored\n";
				}
				$done{$vid."-".$entry."-".$arguments} = '';
				#next;
			}
			## if added : compare to previous ones.
			elsif ($arguments == 2) {
			  ## if refgene, check if nonsyn,stopgain,frameshift,etc AND if there previously was no such variant. 
			  if ($table eq 'Variants_x_ANNOVAR_refgene') {
				# get variant types of archived annotations for this variant.
				#my $nsth = $dbh->prepare("SELECT * FROM `$table` WHERE aid = '$aid' AND vid = '$vid'");
				$refgenesth->execute($aid,$vid);
				my @r = $refgenesth->fetchrow_array();
				#$nsth->finish();
				# skip if non-relevant variant type
				if (!defined($rcodes{$table.'_VariantType'}{$r[6]})) {
					$done{$vid."-".$entry."-".$arguments} = '';
					#print "new refgene. Type $r[6] is not interesting\n";	
					#next;
				}
				# skip if there was a similar annotation (same variant type) discarded during the update due to other changes
				elsif (defined($aanno{$vid}{'rg'}{$r[6]})) {
					$done{$vid."-".$entry."-".$arguments} = '';	
					#print "new refGene, variant of type ".$rcodes{$table.'_VariantType'}{$r[6]}." was deleted\n";
					#next;
				}
				else {
					# new, relevant annotation. record. 
					$done{$vid."-".$entry."-".$arguments} = "  - $r[2] ($r[3]), $codes{$r[5]} : $codes{$r[6]} : $r[8]";
					#print "New RefGene annotation\n";
				}
			  }
		  	  ## if snpEff, check if nonsyn,stopgain,frameshift,etc AND if there previously was no such variant. 
			  elsif ($table eq 'Variants_x_snpEff') {
				# get variant types of archived annotations for this variant.
				#my $nsth = $dbh->prepare("SELECT * FROM `$table` WHERE aid = '$aid' AND vid = '$vid'");
				$snpeffsth->execute($aid,$vid);
				my @r = $snpeffsth->fetchrow_array();
				#$nsth->finish();
				# skip if non-relevant variant type
				if (!defined($rcodes{$table.'_Effect'}{$r[5]})) {
					$done{$vid."-".$entry."-".$arguments} = '';	
					#next;
				}
				# skip if there was a similar annotation (same variant type) discarded during the update due to other changes
				elsif (defined($aanno{$vid}{'se'}{$r[5]})) {
					$done{$vid."-".$entry."-".$arguments} = '';
					#print "new snpEff, variant of type ".$rcodes{$table.'_Effect'}{$r[5]}." was deleted\n";	
					#next;
				}
				else {
					# new, relevant annotion. record. 
					$done{$vid."-".$entry."-".$arguments} = "  - $r[2] ($r[3]), $codes{$r[5]} : $codes{$r[6]} Impact : p.$r[9]";
					#print "New snpEff annotation\n";
				}
			  }

			  ## if clinvar, add if exact AA match of ~pathogenic class..
			  elsif ($table eq 'Variants_x_ClinVar') {
				# get variant types of archived annotations for this variant.
				#my $nsth = $dbh->prepare("SELECT vc.MatchType, cvdb.Class, vc.cvid,cvdb.VarType,cvdb.Gene,cvdb.Disease FROM `Variants_x_ClinVar` vc JOIN `ClinVar_db` cvdb ON cvdb.id = vc.cvid WHERE vc.aid = '$aid' AND vid = '$vid'");
				$cvsth->execute($aid,$vid);
				my @r = $cvsth->fetchrow_array();
				## skip if not exact, or non-relevant class
				if (!defined($rcodes{'Variants_x_ClinVar_AA_MatchType'}{$r[0]}) || !defined($rcodes{'Variants_x_ClinVar_Class'}{$r[1]})) {
					 $done{$vid."-".$entry."-".$arguments} = '';
				}
				else {
					# new, relevant annotion. record. 
					$done{$vid."-".$entry."-".$arguments} = "  - $r[2], $r[3]: AA $codes{$r[0]} ($r[4]): $codes{$r[1]}, Disease: $r[5]";
					#print "  - added : ".$done{$vid."-".$entry."-".$arguments}."\n";
				}

			  }
			  else {
				#print "$table not interesting\n";
			  }
			  ## if genePanel, report always
					# TODO
			  
	
			}
			## UPDATEs are not active yet.
		      }	
		      if ($done{$vid."-".$entry."-".$arguments} ne '') {
			
				$emailcontents{$vid}{$done{$vid."-".$entry."-".$arguments}} = 1;
				#$vxs{$vid}{$sid} = 1;
				if (!defined($vidsid{$vid})) {
					if (!defined($var_gene{$vid})){
						$var_gene{$vid} = GetGeneAnno($vid,$dbh);
					}
					my $gt = GetVidSid($sid,$vid,$db);
					if ($gt ne '') {
						$vidsid{$vid} = "  - ".$samples{$sid} . " : $gt\r\n";
						$vxs{$vid}{$sid} = $samples{$sid} . " ($gt)";
					}
				}
				else {
					my $gt = GetVidSid($sid,$vid,$db);
					if ($gt ne '') {
						$vidsid{$vid} .= "  - ".$samples{$sid} . " : $gt\r\n";
						$vxs{$vid}{$sid} = $samples{$sid} . " ($gt)";;
					}
				}
		      }
		   }
		 }
	}
	$lsth->finish();
	## all log entries done. Compose email
	if (keys(%emailcontents) == 0) {
		print "   => nothing to report for user $uid\n";
		# nothing to report. next user
		next;
	}
	else {
		print "   => $uidx relevant entries found\n";
	}
	open OUT, ">/tmp/$uid.mail.txt";
	print OUT "to: $umail\n";
	print OUT "bcc: $adminemail\n";
	print OUT "subject: VariantDB Important Annotation Updates\n";
	print OUT "from: $adminemail\n\n";
	
	print OUT "Dear $fname $lname,\r\n\r\n";
	print OUT "VariantDB annotations were updated and some possibly novel interesting annotations came out. Only the following entries are reported:\r\n";
	print OUT " - Exonic, NonSynonymous RefSeq, where no such annotation was available previously\r\n";
	print OUT " - Exonic, NonSynonymous snpEff, where no such annotation was available previously\r\n";
	print OUT " - Exact, possibly pathogenic  variants in ClinVar\r\n\r\n";
	print OUT "Below is the list of variants, together with samples containing them:\r\n\r\n";
	
	foreach(keys(%emailcontents)) {
		my $vid = $_;
		if (!defined($variants{$vid})) {
			$vsth->execute($vid);
			my ($chr,$start,$ref,$alt) = $vsth->fetchrow_array();
			$variants{$vid} = "chr".$chromhash{$chr}.":$start : $ref/$alt";
		}
		print OUT "$variants{$vid} : \r\n";
		print OUT '=' x length("$variants{$vid} :")."\r\n";
		print OUT $var_gene{$vid};
		
		foreach(sort {$a cmp $b } keys(%{$emailcontents{$vid}})) {
			print OUT "$_\r\n";
		}
		
		print OUT " Variant is present in the following samples:\r\n";
		my %tmp = ();
		foreach(keys(%{$vxs{$vid}})) {
			$tmp{$projects{$_}}{$samples{$_}} = $vxs{$vid}{$_};
		}
		foreach(sort {$a cmp $b} keys(%tmp)) {
			my $p = "  - Project $_ : ";
			my $pk = $_;
			foreach(sort {$a cmp $b} keys(%{$tmp{$pk}})) {
				$p .= "$tmp{$pk}{$_}, ";
			}
			print OUT substr($p,0,-2)."\r\n";
		}
		print OUT "\r\n";
	}
	print OUT "\r\nBest Regards,\r\nThe VariantDB admin\r\n";
	close OUT;
	#system("sendmail -t < '/tmp/$uid.mail.txt'");
	#system("rm '/tmp/$uid.mail.txt'");

}
$vsth->finish();
$ssth->finish();
$asth->finish();

sub GetGeneAnno {
	my ($vid,$dbh)  = @_;
	my $sth = $dbh->prepare("SELECT vg.GeneSymbol, vg.Transcript, vg.Exon, vc2.Item_Value AS VariantType, vc1.Item_Value AS GeneLocation FROM `Variants_x_ANNOVAR_refgene` vg JOIN `Value_Codes` vc1 JOIN `Value_Codes` vc2 ON vg.GeneLocation = vc1.id AND vg.VariantType = vc2.id WHERE vg.vid = $vid");
	$sth->execute();
	my $rc = $sth->fetchall_arrayref();
	my $entry = '';
	my %effects = ();
	foreach my $r (@$rc) {
		if (!defined($effects{$r->[3]})) {
			$entry .= " $r->[0] ($r->[1]) : Exon $r->[2] ($r->[4]) : $r->[3]\r\n";
			$effects{$r->[3]} = 1;
		}
	}
	$sth->finish();
	return($entry);
	
}

sub GetVidSid {
	my ($sid,$vid,$db) = @_;
	my $sth = $dbh->prepare("SELECT AltCount,RefDepth,AltDepth FROM `Variants_x_Samples` WHERE vid = '$vid' AND sid = '$sid'");
	$sth->execute();
	my $r = $sth->fetchall_arrayref();
	$sth->finish();
	# skip hom.ref calls
	if ($r->[0]->[0] == 0) {
		return('');
	}
	my %gts = ("0" => "Hom.Ref", "1" => "Het.", "2" => "Hom.Alt");
	my $gt = $gts{$r->[0]->[0]}.":$r->[0]->[1]/ $r->[0]->[2]";
	return($gt);
}
