from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, AutoVivification, CheckVariableType
import configparser
import argparse
import os
import sys
import time
import subprocess
import glob
import xml.etree.ElementTree as ET
import psutil

class DataError(Exception):
    pass


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")

    # command line arguments
    help_text = """
        GOAL:
        #####
            - Monitor requests to run annotation of novel variants. 
            
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )

    args = parser.parse_args()

    return config, args


def WritePID(name, type="w"):
    pid = str(os.getpid())
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs", "{}.pid".format(name)), type
    ) as fh:
        fh.write(pid)

def CheckImports(name="VariantDB_Importer.py"):
    for process in psutil.process_iter(['pid', 'name', 'cmdline']):
        if process.info['name'] == name or (process.info['cmdline'] and name in process.info['cmdline']):
            log.info("Found running import : {}".format(process.info['cmdline']))
            return True
    return False


def ProcessPanels():
    # get todo list.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "update.panel.txt"), "r"
    ) as fh:
        panels = [x.rstrip() for x in fh.readlines()]
    # clear file
    open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "update.panel.txt"), "w"
    ).close()
    # set status.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "panel.update.running.txt"),
        "w",
    ) as fh:
        fh.write("1")
    # process.
    for panel in panels:
        if not panel or str(panel) == "0":
            continue
        # set cmd
        cmd = "cd {} && python3 LoadVariants.py -g {}".format(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "GenePanels"), panel
        )
        subprocess.check_call(cmd, shell=True, stdin=None, stdout=None)

    # set status.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "panel.update.running.txt"),
        "w",
    ) as fh:
        fh.write("0")

def CopySamples():
    timestamp = int(time.time())
    # touch if not existing.
    open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", ".CopySample.queue"), "a"
    ).close()
    # get todo list.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", ".CopySample.queue"), "r"
    ) as fh:
        # remove duplicates
        todo = list(set([x.rstrip() for x in fh.readlines()]))
    # clear file
    open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", ".CopySample.queue"), "w"
    ).close()
    # nothing to do
    if not todo:
        return
    # get Database handle
    dbh = GetDBH()
    # check permissions / strip failing samples
    todo_copy = todo.copy()
    for row in todo_copy:
        log.info(row)
        # target_pid, sid, userid, mail;
        pid, sid, uid, _, _ = row.split(",")
        if not dbh.runQuery(f"SELECT 1 FROM `Projects_x_Users` WHERE pid = %s AND uid = %s", [pid, uid]):
            todo.remove(row)
    if not todo:
        return
    # add relatives if set; track relations.
    todo_copy = todo.copy()
    for row in todo_copy:

        pid, sid, uid, mail, add_relatives = row.split(",")
        if not add_relatives:
            continue
        relatives = set([row])
        try:
            relatives.update(get_relatives(sid,dbh,relatives, pid,uid,mail))
        except ValueError as e:
            # TODO : notify the user.
            log.error(f"Failed to get relatives for sample {sid} : {e}")
            todo.remove(row)
            continue
        # add the relatives to the todo list.
        todo.extend(list(relatives))

    # remove duplicates.
    todo = list(set(todo))
    del todo_copy
    if not todo:
        return
    log.info(f"Copying {len(todo)} samples")
    log.info(f"Samples : {todo}")
    sids = [str(x.split(",")[1]) for x in todo ]
    log.debug(f"Sids : {sids}")
    
    # create samples
    sid_map = {}
    log.info("creating new samples")
    for sid in sids:
        row = dbh.runQuery(f"SELECT * FROM `Samples` WHERE id = {sid}")[0]
        del row["id"]
        row['Name'] += " (Duplicate)"
        new_sid = dbh.insertQuery("INSERT INTO `Samples` (`{}`) VALUES ({})".format(
            "`,`".join(row.keys()), ",".join(["%s"]*len(row))), list(row.values()))
        sid_map[sid] = new_sid
    log.info(f"sid_map : {sid_map}")
    # add to projects
    log.info("Adding new samples to target projects")
    for row in todo:
        pid, sid, _ , _, _ = row.split(",")
        dbh.doQuery(f"INSERT INTO `Projects_x_Samples` (`pid`, `sid`) VALUES ({pid}, {sid_map[sid]})")
    
    tables = [f"Samples_x_Reference_Blocks_chr{x}" for x in range(1,26)] + [ "Samples_x_HPO_Terms", "Variants_x_Samples", "Projects_x_Samples", "Custom_Annotations_x_Samples", "Variants_x_Custom_Annotations_decimal", "Variants_x_Custom_Annotations_integer", "Variants_x_Custom_Annotations_list" ]
    for table in tables:
        log.info("Copying data from table : {}".format(table))
        # copy table structure
        dbh.doQuery(f"CREATE TABLE IF NOT EXISTS `{table}_tmp_{timestamp}` LIKE `{table}`")
        # copy data
        dbh.doQuery(f"INSERT INTO `{table}_tmp_{timestamp}` SELECT * FROM `{table}` WHERE sid IN ({','.join(sids)})")
        for sid in sids:
            dbh.doQuery(f"UPDATE `{table}_tmp_{timestamp}` SET sid = {sid_map[sid]} WHERE sid = {sid}")
        # then copy back to main table
        dbh.doQuery(f"INSERT INTO `{table}` SELECT * FROM `{table}_tmp_{timestamp}`")
        # drop tmp table
        dbh.doQuery(f"DROP TABLE `{table}_tmp_{timestamp}`")

    # copy saved results pages
    log.info("Copying saved results")
    saved_sets = dbh.runQuery(f"SELECT * FROM `Samples_x_Saved_Results` WHERE sid IN ({','.join(sids)})")
    set_map = {}
    for row in saved_sets:
        log.info(" Copying set : {} for sample {}".format(row['set_name'], row['sid']))
        set_id = row['set_id']
        del row['set_id']
        row['sid'] = sid_map[str(row['sid'])]
        row['set_name'] += " (Duplicate)"
        new_set_id = dbh.insertQuery("INSERT INTO `Samples_x_Saved_Results` (`{}`) VALUES ({})".format(
            "`,`".join(row.keys()), ",".join(["%s"]*len(row))), list(row.values()))
        set_map[set_id] = new_set_id
    # copy saved results pages
    log.info("Copying saved results pages")
    for set_id, new_set_id in set_map.items():
        pages = dbh.runQuery(f"SELECT * FROM `Samples_x_Saved_Results_Pages` WHERE set_id = {set_id}")
        for page in pages:
            page['set_id'] = new_set_id
            dbh.insertQuery("INSERT INTO `Samples_x_Saved_Results_Pages` (`{}`) VALUES ({})".format(
                "`,`".join(page.keys()), ",".join(["%s"]*len(page))), list(page.values()))

    # create new relations.
    log.info("Creating new relations")
    # copy table structure
    table = "Samples_x_Samples"
    dbh.doQuery(f"CREATE TABLE IF NOT EXISTS `{table}_tmp_{timestamp}` LIKE `{table}`")
    # copy data
    dbh.doQuery(f"INSERT INTO `{table}_tmp_{timestamp}` SELECT * FROM `{table}` WHERE sid1 IN ({','.join(sids)}) OR sid2 IN ({','.join(sids)})")
    for sid in sids:
        dbh.doQuery(f"UPDATE `{table}_tmp_{timestamp}` SET sid1 = {sid_map[sid]} WHERE sid1 = {sid}")
        dbh.doQuery(f"UPDATE `{table}_tmp_{timestamp}` SET sid2 = {sid_map[sid]} WHERE sid2 = {sid}")
    # then copy back to main table
    dbh.doQuery(f"INSERT INTO `{table}` SELECT * FROM `{table}_tmp_{timestamp}`")
    # drop tmp table
    dbh.doQuery(f"DROP TABLE `{table}_tmp_{timestamp}`")

    # notify
    # TODO

    # reset summary status
    # TODO
    
    log.info("Done")


def get_relatives(sid,dbh,relatives,pid, uid,mail):
    # recursive function to obtain all relatives.
    rows = dbh.runQuery(f"SELECT ss.sid1, p1.pid AS pid1 ,ss.sid2, p2.pid AS pid2 FROM `Samples_x_Samples` ss JOIN `Projects_x_Samples` p1 JOIN `Projects_x_Samples` p2 ON ss.sid1 = p1.sid AND ss.sid2 = p2.sid WHERE ss.sid1 = {sid} OR ss.sid2 = {sid}")
    for row in rows:
        entry = f"{pid},{row['sid1']},{uid},{mail},1"
        if entry not in relatives:
            # access ? 
            r = dbh.runQuery(f"SELECT 1 FROM `Projects_x_Users` WHERE pid = {row['pid1']} AND uid = {uid}")
            if not r:
                raise ValueError(f"User does not have access to project {row['pid1']} containing relative {row['sid1']}")
            relatives.add(entry)
            relatives = get_relatives(row['sid1'],dbh,relatives,pid,uid,mail)
        entry = f"{pid},{row['sid2']},{uid},{mail},1"
        if entry not in relatives:
            # access ?
            r = dbh.runQuery(f"SELECT 1 FROM `Projects_x_Users` WHERE pid = {row['pid2']} AND uid = {uid}")
            relatives.add(entry)
            relatives = get_relatives(row['sid2'],dbh,relatives,pid,uid,mail)
        
    return relatives

def ProcessDelete():
    # touch if not existing.
    open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", ".delete_queue"), "a"
    ).close()
    # get todo list.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", ".delete_queue"), "r"
    ) as fh:
        todo = [x.rstrip() for x in fh.readlines()]
    # clear file
    open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Results", ".delete_queue"), "w"
    ).close()
    # nothing to do
    if not todo:
        return

    dbh = GetDBH()

    # get users & projects to resummarize:
    sid_string = ",".join([str(x) for x in todo])
    query = f"SELECT pid FROM `Projects_x_Samples` WHERE sid IN ({sid_string})"
    pids = dbh.runQuery(query)
    pid_string = ",".join([str(x["pid"]) for x in pids])
    query = f"SELECT pu.uid FROM `Projects_x_Users` pu WHERE pu.pid IN ({pid_string})"
    uids = dbh.runQuery(query)
    uid_string = ",".join([str(x["uid"]) for x in uids])

    # process.
    for sid in todo:
        # empty.
        if not sid:
            continue
        # delete in 100K batches from V_x_S.
        r = dbh.runQuery("SELECT 1 FROM `Variants_x_Samples` WHERE sid in ({}) LIMIT 1".format(sid))
        while r:
            dbh.doQuery(
                "DELETE FROM `Variants_x_Samples` WHERE sid in ({}) LIMIT 100000".format(sid)
            )
            r = dbh.runQuery(
                "SELECT 1 FROM `Variants_x_Samples` WHERE sid in ({}) LIMIT 1".format(sid)
            )
        # RefBlock tables
        for chr in list(range(1, 26)):
            table = "Samples_x_Reference_Blocks_chr{}".format(chr)
            r = dbh.runQuery("SELECT 1 FROM `{}` WHERE sid IN ({}) LIMIT 1".format(table, sid))
            while r:
                dbh.doQuery("DELETE FROM `{}` WHERE sid IN ({}) LIMIT 100000".format(table, sid))
                r = dbh.runQuery("SELECT 1 FROM `{}` WHERE sid IN ({}) LIMIT 1".format(table, sid))
        # Log
        dbh.doQuery("DELETE FROM `Log` WHERE sid in ({})".format(sid))
        # reset memcache.
        dbh.doQuery(
            "UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` IN ('Log','Variants_x_Samples')"
        )
        dbh.doQuery(
            "UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` LIKE 'Samples_x_Reference_Blocks_%'"
        )
    # reset summary status
    log.info(f"Reset SummaryStatus for projects : {pid_string}")
    dbh.doQuery(f"UPDATE Projects SET SummaryStatus = 0 WHERE id IN ({pid_string})")
    log.info(f"Reset SummaryStatus for users : {uid_string}")
    dbh.doQuery(f"UPDATE Users SET SummaryStatus = 0 WHERE id in ({uid_string})")


def ProcessAnnotations(force=False):
    # check if update needed.
    if not force:
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "update.txt"), "r"
        ) as fh:
            needed = fh.readline().rstrip()
        if int(needed) == 0:
            return

    # reset files
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "update.txt"), "w"
    ) as fh:
        fh.write("0")
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "update.force.txt"), "w"
    ) as fh:
        fh.write("0")

    # set status.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "global.update.running.txt"),
        "w",
    ) as fh:
        fh.write("1")

    # run updater.
    cmd = "cd {} && python3 Annotate.py".format(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations")
    )
    try:
        subprocess.check_call(cmd, shell=True, stdin=None, stdout=None)
    except Exception as e:
        raise e

    # set status.
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "global.update.running.txt"),
        "w",
    ) as fh:
        fh.write("0")


def GetDBH(quiet=False):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)

    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        # set status
        with open(os.path.join(args.directory, "status"), "w") as fh:
            fh.write("error")
        sys.exit(1)

    if not quiet:
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    return dbh


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # write out the pid.
    try:
        WritePID("monitor")
    except Exception as e:
        print("Unable to set pid. Exiting : {}".format(e))
        sys.exit(1)

    # setup logging.
    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    setup_logging(
        name="VariantDB_Master_Annotator",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
    )
    log = get_logger("main")
    log.info(msg)

    
    # counter to make it run 1x/hour
    counter = 59
    while True:
        # global annotations
        counter += 1

        # panel update needed?
        try:
            ProcessPanels()
        except Exception as e:
            log.error("Failed to process panels: {}".format(e))
            sys.exit(1)

        # admin-set forced annotation needed ?
        with open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", "update.force.txt"), "r"
        ) as fh:
            needed = fh.readline().rstrip()
        log.debug(f"needed : {needed}")
        if int(needed) > 0:
            log.info(f"Starting forced annotation run: {needed}")
            ProcessAnnotations(force=True)

        # delete/copy queue : at half hours
        if counter == 30:
            try:
                ProcessDelete()
            except Exception as e:
                log.error("Failed to process Deletion queue: {}".format(e))
                sys.exit(1)
            try:
                CopySamples()
            except Exception as e:
                log.error("Failed to process Copy queue: {}".format(e))
                sys.exit(1)

        # global annotations
        if counter == 60:
            # reset.
            counter = 0
            try:
                # check for running imports : the script is VariantDB_Importer.py
                if not CheckImports():
                    ProcessAnnotations()
                else:
                    log.info("Skip annotations to allow imports to finish")
                    
            except Exception as e:
                log.error("Could not process Regular annotations : {}".format(e))
                sys.exit(1)
        # sleep.
        time.sleep(60)
