#!/usr/bin/perl
$|++;
use DBI;
use Getopt::Std;
use Scalar::Util qw(looks_like_number);
use Math::BigFloat;
# opts
# a : (a)nnotation table to use.
# b : genome build (if provided)
getopts('a:b:', \%opts) ;

use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
our $datestring = ($year+1900)."-".$mon."-".$mday;

####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";
$annovariants = [];
$allvariants = [];
@allvids = ();
%second = ();

###########################
# CONNECT TO THE DATABASE #
###########################
## GET current/provided build
our $db = '';
our $dbbuild = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$dbbuild = $cb;
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
	$dbbuild = $opts{'b'};
}

$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
###################
## load memcache ##
###################
if ($usemc == 1) {
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	my $mcpath = abs_path("../../cgi-bin/inc_memcache.pl");
	require($mcpath);	

}


## get userid from admin user
$sth = $dbh->prepare("SELECT id FROM `Users` WHERE email = '$adminemail'");
my $nr = $sth->execute();
if ($nr == 0 ) {
	$sth->finish();
	$sth = $dbh->prepare("SELECT id FROM `Users` WHERE level = 3 ORDER BY id LIMIT 1");
	$nr = $sth->execute();
	if ($nr == 0) {
		die("No Admin user found\n");
	}
}
my ($uid) = $sth->fetchrow_array();
$sth->finish();



# clear up variables
$annovariants = [];
$allvariants = [];
@allvids = ();
%second = ();

# set validation prefix
my $validation = 1;
my $table_prefix = "Validation_";

## load the annotations. 
system("cd $scriptdir/Annotations/GenePanels && perl LoadVariants.pl -v");


## process
my $fileidx = 1;
my $nrok = $nrfail = $nrnew = $missingfield = 0;
my @discard;

open LOG, ">$scriptdir/Annotations/Update.Files/Load.Log.GP.txt";
#$log = $dbh->prepare("INSERT DELAYED INTO `Variants_x_Log` (sid, vid, uid, entry, arguments) VALUES (?,?,'$uid',?,?)");
$archive = $dbh->prepare("INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (?,'Variants_x_GenePanels','$dbbuild','$datestring',?)");
#$sids = $dbh->prepare("SELECT sid FROM `Variants_x_Samples` WHERE vid = ?");

my $delq = "DELETE FROM `Variants_x_GenePanels` WHERE ";
my $empty_delq = $delq;
my $del_idx = 0;
#$del = $dbh->prepare("DELETE LOW_PRIORITY FROM `Variants_x_GenePanels` WHERE aid = ? AND vid = ?");

#our %sidlist = ();
while (-e "$scriptdir/Annotations/Update.Files/Variants.$fileidx.txt") {
	my $l = `head -n 1 $scriptdir/Annotations/Update.Files/Variants.$fileidx.txt`;
	chomp($l);
	my @p = split(/\t/,$l);
	my $fidx = $p[-1];
	$l = `tail -n 1 $scriptdir/Annotations/Update.Files/Variants.$fileidx.txt`;
	chomp($l);
	@p = split(/\t/,$l);
	my $lidx = $p[-1];
	print "FILE: $fileidx (variants $fidx to $lidx)\n";
	# get current annotations from DB.
	$current = GetAnno($fidx,$lidx,'');
	# get updated annotations from DB (this script should depend on ANNOVAR_refgene)
	$new = GetAnno($fidx,$lidx,$table_prefix);
	# Compare annotations.
	foreach(keys(%$current)) {
		## check
		my $vid = $_;
		## current annotations for this vid
		foreach(keys(%{$current->{$vid}})) {
			my $aok = 0;
			my $caid = $_;
			## compare against all new annotations
			foreach(keys(%{$new->{$vid}})) {
				my $naid = $_;
				my $match = 1;
				## loop the fields
				foreach(keys(%{$current->{$vid}{$caid}})) {
					next if ($_ eq 'aid' || $_ eq 'vid' || $_ eq 'LastEdit');
					if (!defined($new->{$vid}{$naid}{$_})) {
						print "Fail : new $vid ; $naid ; $_\n";
						$match = 0;
						last;
					}
					if ($current->{$vid}{$caid}{$_} ne $new->{$vid}{$naid}{$_}) {
						## hack : some gene-based annotations have genesymbol value of more than 255 chars. if substr ==, then ok.
						if ($_ ne 'gene' || substr($new->{$vid}{$naid}{$_},0,250) ne substr($current->{$vid}{$caid}{$_},0,250)) {
							$match = 0;
							last;
						}
					}
				}
				## annotation matches
				if ($match == 1) {
					$nrok++;
					$aok = 1;
					delete($new->{$vid}{$naid});
					last; # from new annotations for this caid.
				}
			}
			if ($aok == 0) {
				## annotation not found.
				$nrfail++;
				# move variant to archived table
				my $contents = '';
				foreach(keys(%{$current->{$vid}{$caid}})) {
					next if ($_ eq 'aid' || $_ eq 'vid');
					$contents .= $_ . '|||'.$current->{$vid}{$caid}{$_}.'@@@';
				}
				$contents = substr($contents,0,-4);
				$archive->execute($vid,$contents);
				my $archid = $dbh->last_insert_id(undef,undef,"Archived_Annotations",undef);
				# add to log file for samples with this variant
				#if (!defined($sidlist{$vid})) {
				#	$sids->execute($vid);
				#	$sidlist{$vid} = $sids->fetchall_arrayref();
				#}
				my $argument = 1; #"Annotation Deleted";
				# pass archived-id as entry to fetch details.
				#foreach(@{$sidlist{$vid}}) {
				#	$log->execute($_->[0],$vid,"Variants_x_GenePanels:$archid",$argument);
				#}
				print LOG "$vid\t$uid\tVariants_x_GP:$archid\t$argument\n";
				# delete the annotation
				#$del->execute($caid,$vid);
				$delq .= " (`aid` = $caid AND `vid` = $vid) OR";
				$del_idx++;
				if ($del_idx == 500) {
					$delq = substr($delq,0,-3);
					$dbh->do($delq);
					$del_idx = 0;
					$delq = $empty_delq;
				}

			}
		}
		
	}
	# store new annotations for this range (ones not deleted from hash yet)
	foreach my $vid (keys(%$new)) {
		foreach(keys(%{$new->{$vid}})) {
			my $naid = $_;
			$nrnew++;
			my $f = $v = '';
			foreach(keys(%{$new->{$vid}{$naid}})) {
				next if ( $_ eq 'aid');
				$f .= $_.',';
				$v .= "'".$new->{$vid}{$naid}{$_}."',";
			}
			$f = substr($f,0,-1);
			$v = substr($v,0,-1);
			my $query = "INSERT INTO `Variants_x_GenePanels` ($f) VALUES ($v)";
			$dbh->do($query);
			$aid = $dbh->last_insert_id(undef,undef,"Variants_x_GenePanels",undef);
			
			# add to log file for samples with this variant
			#if (!defined($sidlist{$vid})) {
			#	$sids->execute($vid);
			#	$sidlist{$vid} = $sids->fetchall_arrayref();
			#}
			my $argument = 2;#"Annotation Added";
			# pass archived-id as entry to fetch details.
			#foreach(@{$sidlist{$vid}}) {
			#	$log->execute($_->[0],$vid,"Variants_x_GenePanels:$aid",$argument);
			#}
			print LOG "$vid\t$uid\tVariants_x_GP:$archid\t$argument\n";

					
		}
	}
	$fileidx++;
}
if ($del_idx > 0) {
	$delq = substr($delq,0,-3);
	$dbh->do($delq);
}
$dbh->do("LOAD DATA LOCAL INFILE '$scriptdir/Annotations/Update.Files/Load.Log.GP.txt' INTO TABLE `Variants_x_Log` (`vid`, `uid`, `entry`, `arguments`)");
system("rm '$scriptdir/Annotations/Update.Files/Load.Log.GP.txt'");

#$log->finish();
$archive->finish();
#$sids->finish();
#$del->finish();

if ($usemc==1) {
	clearMemcache("Variants_x_GenePanels");
}
print "Nr of matching annotations: $nrok\n";
print "Nr of discarded annotations: $nrfail\n";
print "Nr of new annotations: $nrnew\n";
exit;
$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."Variants_x_GenePanels`");
exit;




sub GetAnno {
	my ($fidx,$lidx,$prefix) = @_;
	## local db connection
	my $connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	my $i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my %return = ();
	my $sth = $dbh->prepare("SELECT * FROM `$prefix"."Variants_x_GenePanels` WHERE vid BETWEEN $fidx AND $lidx");
	$sth->execute();
	$href = $sth->fetchall_arrayref({});
	## convert to hash{vid}{aid}
	foreach(@$href) {
		
		$return{$_->{'vid'}}{$_->{'aid'}} = $_;	
	}
	$sth->finish();
	$dbh->disconnect();
	return \%return;
}


