from CMG.Utils import Re, AutoVivification, CheckVariableType
from CMG.DataBase import MySqlError
import configparser
import argparse
import os
import sys
import time
from datetime import date
import subprocess
import glob
import tempfile
import urllib.request
import hashlib
import shutil
import ftplib


# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


## some variables/objects
re = Re()  # customized regex-handler


def ConvertChr(v):
    chrom_dict = {x: x for x in range(1, 23)}
    chrom_dict.update({"X": 23, 23: "X", "Y": 24, 24: "Y", "M": 25, "MT": 25, 25: "M"})
    return chrom_dict[v]


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Run update on all Variants if new release & store to dedicated validation table in DB
            - Compare current and new annations, notify users if relevant items were found. 
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # annotation type
    parser.add_argument("-a", "--anno", required=False, help="Annotation Type")
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # download only mode
    parser.add_argument(
        "-d",
        "--download",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Download new release only into Validation_humandb folder.",
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Validation Mode: No data download, just reannotation and results are saved to files instead of emailed",
    )

    args = parser.parse_args()
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    return config, args


def CompareResults():
    
    # open all output files.
    time_stamp = date.today().strftime("%Y-%m-%d")
    # open log loading file handle:
    out_files = dict()
    out_files["LOG"] = open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GenePanels/tmp_files",
            "Load.Log.GP.{}.txt".format(time_stamp),
        ),
        "w",
    )

    for vtype in ["NOVEL", "LOST"]:
        out_files[vtype] = open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/GenePanels/tmp_files/",
                "Validation.GP.{}.{}".format(time_stamp, vtype),
            ),
            "w",
        )

    
    # work in slices of 50K variants :
    start_vid = 0
    nr_ok = 0
    nr_lost = 0
    nr_new = 0
    nr_altered = 0
    while True:
        # get last vid.
        vids = annotator.dbh.runQuery(
            "SELECT id FROM `Variants` WHERE id >= %s ORDER BY `id` LIMIT 50000",
            start_vid,
        )
        if not vids:
            # no data left.
            break
        last_vid = vids[-1]["id"]
        log.info("Working on 50K variants with VIDS from {} to {}".format(start_vid, last_vid))
        vids = []

        # get old annotations
        log.info("Fetching old annotations")
        #old_annotations = AutoVivification()
        old_annotations = dict()
        query = f"SELECT vid, gpid, gene FROM `{annotator.args.table_name}` WHERE vid BETWEEN %s AND %s"
        log.debug(query)
        rows = annotator.dbh.runQuery(
            query,
            [start_vid, last_vid],
            as_dict=True,
        )
        for row in rows:
            # hash[vid] = set() of 'gpid|symbol
                        
            # create
            if row["vid"] not in old_annotations:
                old_annotations[row["vid"]] = set()
            # add
            old_annotations[row["vid"]].add(f"{row['gpid']}|{row['gene']}")


        # get new annotations & compare
        log.info("Fetching and comparing new annotations.")
        query = f"SELECT vid, gpid, gene FROM `Validation_{annotator.args.table_name}` WHERE vid BETWEEN %s AND %s"
        log.debug(query)
        rows = annotator.dbh.runQuery(query,
                [start_vid, last_vid],
                as_dict=True,
            )
        new_annotations = dict()
        for row in rows:
            vid = row["vid"]
            # create
            if row["vid"] not in new_annotations:
                new_annotations[row["vid"]] = set()
            # add
            new_annotations[row["vid"]].add(f"{row['gpid']}|{row['gene']}")

        # compare. : iterate keys cast to a list : this allows deleting keys on the way
        for vid in list(new_annotations):
            # exact hit
            if vid in old_annotations and new_annotations[vid] == old_annotations[vid]:
                nr_ok += 1
                del old_annotations[vid]
                continue
            
            # present in old_annotations?
            if vid not in old_annotations:
                nr_new += 1
                if annotator.args.validate:
                    out_files["NOVEL"].write(
                        "{}\t{}\n".format(
                            vid,
                            "; ".join(new_annotations[vid])),
                        )
                    
                else:
                    # copy to main table.
                    for i in new_annotations[vid]:
                        gpid,gene = i.split('|')
                        new_aid = annotator.dbh.insertQuery(
                            f"INSERT INTO `{annotator.args.table_name}` (`vid`,`gpid`,`gene`) VALUES (%s,%s,%s)",
                            [vid,gpid,gene]
                        )
                        # add to log table.
                        out_files["LOG"].write(
                            "{}\t{}\tVariants_x_GenePanels_ncbigene:{}\t2\n".format(
                                vid, annotator.args.userid, new_aid
                            )
                        )
                # next entry.
                del new_annotations[vid]
                continue
            
            # altered : process items individually.
            nr_altered += 1
            for i in list(new_annotations[vid]):
                
                if i in old_annotations[vid]:
                    old_annotations[vid].remove(i)
                    continue
                # add as novel
                if annotator.args.validate:
                    out_files["NOVEL"].write("{}\t{}\n".format(vid,i))
                               
                else:
                    # copy to main table.
                
                    gpid,gene = i.split('|')
                    new_aid = annotator.dbh.insertQuery(
                        f"INSERT INTO `{annotator.args.table_name}` (`vid`,`gpid`,`gene`) VALUES (%s,%s,%s)",
                        [vid,gpid,gene]
                    )
                    # add to log table.
                    out_files["LOG"].write(
                        "{}\t{}\tVariants_x_GenePanels_ncbigene:{}\t2\n".format(
                            vid, annotator.args.userid, new_aid
                        )
                    )
                new_annotations[vid].remove(i)
            
        # any left on the old_annotations stack : these are lost.
        log.info("Archiving discarded annotations")
        for vid in old_annotations:
            nr_lost += 1
            for i in old_annotations[vid]:
                gpid,gene = i.split('|')
                # validate : to files
                if annotator.args.validate:
                    out_files["LOST"].write("{}\t{}\n".format(vid,i))
                # else : to log table
                else:
                    
                    contents = f"gpid|||{gpid}@@@gene|||{gene}"
                    # archive
                    aaid = annotator.dbh.insertQuery(
                        "INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (%s,'Variants_x_GenePanels_ncbigene',%s,%s,%s)",
                        [vid, f"-{annotator.args.build}", time_stamp, contents],
                    )
                    # log
                    out_files["LOG"].write(
                        "{}\t{}\tVariants_x_GenePanels_ncbigene:{}\t1\n".format(
                            vid, annotator.args.userid, aaid
                        )
                    )
                    # delete
                    annotator.dbh.doQuery(
                        f"DELETE FROM `{annotator.args.table_name}` WHERE `vid` = %s AND `gpid` = %s AND `gene` = %s",
                        [vid, gpid,gene],
                    )

        # clear
        old_annotations.clear()
        # next iteration.
        start_vid = last_vid + 1

    # overview.
    log.info("STATISTICS:")
    log.info("  IDENTICAL   : {}".format(nr_ok))
    log.info("  NOVEL ENTRY : {}".format(nr_new))
    log.info("  DISCARDED   : {}".format(nr_lost))
    log.info("  ALTERED     : {}".format(nr_altered))

    # close file handles
    for fh in out_files:
        out_files[fh].close()

    # load to log-table.
    if not annotator.args.validate:
        annotator.dbh.doQuery(
            "LOAD DATA LOCAL INFILE '{}' INTO TABLE `Variants_x_Log` (`vid`, `uid`, `entry`, `arguments`)".format(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/GenePanels/tmp_files",
                    "Load.Log.GP.{}.txt".format(time_stamp),
                )
            )
        )


def ValidateTables():
    for table in ["Variants_x_ANNOVAR_ncbigene", "GenePanels", "GenePanels_x_Genes_ncbigene"]:
        if not annotator.dbh.tableExists(f"{annotator.args.prefix}{table}"):
            raise Exception(f"Table missing : {annotator.args.prefix}{table}")



if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    log = StartLogger(config, "GenePanels")

    # get the annotator object
    annotator = Annotator(config=config, method="GenePanels", annotation="GenePanels", args=args)

    # set table name.
    annotator.args.table_name = "Variants_x_GenePanels_ncbigene"

    ## Validate options
    try:
        # table exists ?
        annotator.ValidateTable()
        # other tables exist ?
        ValidateTables()
        # get field/column names:
        annotator.fieldnames = annotator.dbh.getColumns(annotator.args.table_name)["names"]
        # strip aid
        d = annotator.fieldnames.pop(0)
    except (DataError, ValueError) as e:
        log.error(e)
        sys.exit(1)

    # get userid of admin user (for archive logs)
    try:
        annotator.args.userid = annotator.dbh.runQuery(
            "SELECT id FROM `Users` WHERE `email` = %s", [config["USERS"]["ADMIN"]]
        )[0]["id"]
    except Exception as e:
        log.error("Admin email not recognized by VariantDB : {}".format(e))
        sys.exit(1)

    
    
    
    # run the annotation procedures
    if not annotator.args.download:
        try:
            # the command :
            log.info("Running update for ALL variants.")
            cmd = [
                "python",
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"], "Annotations/GenePanels/LoadVariants.py"
                ),
                "-v"
            ]
            subprocess.check_call(cmd)
        except Exception as e:
            log.error("GenePanel Update failed : {}".format(e))
            sys.exit(1)

        # compare results.
        try:
            CompareResults()
        except Exception as e:
            log.error("Failure in result comparison : {}".format(e))
            sys.exit(1)


# cleanup
