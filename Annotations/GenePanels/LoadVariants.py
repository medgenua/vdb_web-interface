import configparser
import argparse
import os
import sys
import time

# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Annotate all NEW variants with GenePanels data & store to DB
            - Annotate all Variants & store to dedicated validation table in DB
            - Annotate a VCF file & store to new VCF file.
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Run in Validation Mode. All variants are annotated, and stored in a separate table.",
    )
    # file mode
    parser.add_argument(
        "-f", "--infile", required=False, help="Input VCF file to use in file-based mode."
    )
    parser.add_argument(
        "-o", "--outfile", required=False, help="Output VCF file to use in file-based mode."
    )
    # only a single gene panel
    parser.add_argument(
        "-g",
        "--genepanel",
        required=False,
        help="GenePanel ID, only redo this panel, but for ALL variants",
    )
    parser.add_argument(
        "-a", "--annotation", required=False, help="Dummy Variable for compatibility reasons"
    )
    # overrule log level
    parser.add_argument(
        "-l", "--loglevel", required=False, help="Set the log level : ERROR, WARN, [INFO], DEBUG"
    )
    args = parser.parse_args()
    # if loglevel set : overrule value in config.
    if args.loglevel:
        config["LOGGING"]["LOG_LEVEL"] = args.loglevel
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    return config, args


## routine to update ALL variants in ONE panel
def ProcessPanel(gpid, tmp_table=None):
    # panel must exist.
    panel_info = annotator.dbh.runQuery(
        "SELECT `Name` FROM `GenePanels` WHERE id = %s", gpid
    )
    if not panel_info:
        raise DataError("Specified Gene Panel ID does not exist: {}".format(gpid))

    log.info("Updating panel : {} : {} ".format(gpid, panel_info[0]["Name"]))
    # get genes.
    p_r = annotator.dbh.runQuery(
        "SELECT Symbol FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = %s",
        gpid,
    )
    if not p_r:
        log.warning("No Genes in panel {}".format(gpid))
        return True
    # dictionary to track data.
    genes = {x["Symbol"].upper() for x in p_r}

    # get variants+symbol
    if tmp_table:
        v_r = annotator.dbh.runQuery(
            statement="SELECT va.vid, va.GeneSymbol FROM `Variants_x_ANNOVAR_ncbigene` va INNER JOIN `{}` t ON t.vid = va.vid WHERE GeneSymbol IN ('{}') GROUP BY va.vid, va.GeneSymbol".format(
                 tmp_table, "','".join(list(genes))
            ),
            size=50000,
        )
    else:
        # all variants, no joining needed
        v_r = annotator.dbh.runQuery(
            statement="SELECT vid, GeneSymbol FROM `Variants_x_ANNOVAR_ncbigene` WHERE GeneSymbol IN ('{}') GROUP BY vid, GeneSymbol".format(
                "','".join(list(genes))
            ),
            size=50000,
        )
    # fetch & write to loading file.
    load_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations",
        annotator.method,
        "tmp_files",
        "{}.list.txt".format(time.time()),
    )
    log.debug("load_file : {}".format(load_file))
    with open(load_file, "w") as fh:
        while len(v_r) > 0:
            for row in v_r:
                # should be ok
                if row["GeneSymbol"].upper() in genes:
                    fh.write("{},{},{}\n".format(row["vid"], gpid, row["GeneSymbol"]))
            v_r = annotator.dbh.GetNextBatch()

    log.info("Loading Database")
    # clear current db entries if annotation all variants (no tmp_table)
    if not tmp_table:
        annotator.dbh.doQuery(
            "DELETE FROM `{}{}` WHERE `gpid` = %s".format(
                annotator.args.prefix, annotator.args.table_name
            ),
            gpid,
        )
    # load into DB.
    annotator.dbh.doQuery(
        "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}{}` FIELDS TERMINATED BY ',' (`vid`,`gpid`,`gene`)".format(
            load_file, annotator.args.prefix, annotator.args.table_name
        )
    )
    log.info("GenePanel Done")


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def ValidateTables():
    for table in ["Variants_x_ANNOVAR_ncbigene", "GenePanels", "GenePanels_x_Genes_ncbigene"]:
        if not annotator.dbh.tableExists(f"{table}"):
            raise Exception(f"Table missing : {table}")


def LoadVariants(all_variants=False):
    # all variants, don't make a tmp table
    if annotator.args.validate or all_variants:
        return None

    tmp_table = "Variants_x_GenePanels_tmp_{}".format(time.time())
    log.debug("tmp_table: {}".format(tmp_table))
    try:
        annotator.dbh.doQuery(
            "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                tmp_table
            )
        )
    except Exception as e:
        log.error("Could not create tmp table {} : {}".format(tmp_table, e))
        sys.exit(1)
    try:
        # this extracts all variants not in any panel.
        log.info("Extracting variants NOT in any panel")
        annotator.dbh.doQuery(
            "INSERT INTO `{}` (vid) SELECT v.id FROM `Variants` v  WHERE v.id NOT IN (SELECT va.vid FROM `{}` va) ".format(
                tmp_table, annotator.args.table_name
            )
        )
    except Exception as e:
        log.error("Could not fill tmp table : {}".format(e))
        sys.exit(1)
    return tmp_table


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # get logger
    log = StartLogger(config, "GenePanels")

    # get the annotator object
    annotator = Annotator(config=config, method="GenePanels", annotation="GenePanels", args=args)

    # set table name.
    annotator.args.table_name = "Variants_x_GenePanels_ncbigene"

    ## Validate options
    try:
        # table exists ?
        annotator.ValidateTable()
        # other tables exist ?
        ValidateTables()
        # get field/column names:
        #annotator.fieldnames = annotator.dbh.getColumns(annotator.args.table_name)["names"]
        # strip aid
        #d = annotator.fieldnames.pop(0)

    except (DataError, ValueError) as e:
        log.error(e)
        sys.exit(1)

    ## single panel:
    if annotator.args.genepanel:
        gpids = [annotator.args.genepanel]
    # all panels
    else:
        gp_r = annotator.dbh.runQuery("SELECT id FROM `GenePanels`")
        gpids = [x["id"] for x in gp_r]
        log.debug(gpids)

    # fill tmp_table if processing new variants
    if not annotator.args.genepanel:
        tmp_table = LoadVariants()
    else:
        # one genepanel => all variants
        tmp_table = None # LoadVariants(all_variants=True)


    # process.
    for gpid in gpids:
        try:
            #if not annotator.args.genepanel:
            ProcessPanel(gpid, tmp_table)
            #else:
            #    ProcessPanel(gpid)
        except Exception as e:
            log.error("Failed to Process Panel {} : {}".format(gpid, e))
            sys.exit(1)

    # clean up.
    try:
        annotator.dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(tmp_table))
    except Exception as e:
        log.error("Could not drop table {} : {}".format(tmp_table, e))
        sys.exit(1)
    annotator.cleanup()
