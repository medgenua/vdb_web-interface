from CMG.Utils import Re, AutoVivification, CheckVariableType
from CMG.DataBase import MySqlError
import configparser
import argparse
import os
import sys
import time
from datetime import date
import subprocess
import glob
import tempfile
import urllib.request
import hashlib
import shutil
import ftplib


# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


## some variables/objects
re = Re()  # customized regex-handler


def ConvertChr(v):
    chrom_dict = {x: x for x in range(1, 23)}
    chrom_dict.update({"X": 23, 23: "X", "Y": 24, 24: "Y", "M": 25, "MT": 25, 25: "M"})
    return chrom_dict[v]


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Evaluate if a new annotatation release is present
            - Run Annovar on all Variants if new release & store to dedicated validation table in DB
            - Compare current and new annations, notify users if relevant items were found. 
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # annotation type
    parser.add_argument("-a", "--anno", required=True, help="Annotation Type")
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # download only mode
    parser.add_argument(
        "-d",
        "--download",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Download new release only into Validation_humandb folder.",
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Validation Mode: No data download, just reannotation and results are saved to files instead of emailed",
    )

    args = parser.parse_args()
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    return config, args


def GetNames():
    anno = annotator.args.anno
    table = anno
    re = Re()
    # returns annotation and corresponding table.
    if re.isearch(r"refgene|refseq", anno):
        anno = "refGene"
        table = "refgene"
    elif re.isearch(r"ncbi", anno):
        anno = "ncbiGene"
        table = "ncbigene"
    elif re.isearch(r"ucsc", anno):
        anno = "knownGene"
        table = "knowngene"
    elif re.isearch(r"ensgene|Ensembl", anno):
        anno = "ensGene"
        table = "ensgene"
    elif re.isearch(r"cadd", anno):
        anno = anno.lower()
        table = anno
    elif re.isearch(r"snp(\d+)", anno):
        # annotation has avsnp\d ; table does not have 'av' in the name
        table = "snp{}".format(re.last_match.group(1))
    elif re.isearch(r"gnomad|mitimpact", anno):
        # gnomad and mitimpact : remove dots in version
        table = anno.lower().replace(".", "")
    elif re.isearch(r"ljb_", anno):
        anno = anno.lower()
    elif re.isearch(r"revel",anno):
        anno = anno.lower()
        table = anno

    # no longer supported:
    #  polyphen2 ; muttast ; exac ; siv2

    # full table :
    annotator.args.table_name = "Variants_x_ANNOVAR_{}".format(table)
    annotator.args.anno_table = table
    # annotation file
    annotator.args.annotation_file = "{}_{}.txt".format(annotator.args.build, anno)
    annotator.args.anno = anno


def GetSource(anno):
    # custom requires functions with the same name.
    custom = ["refgene", "ncbigene", "gnomad_e_2.1", "gnomad_g_2.1","revel"]
    # download with webfrom Annovar switch
    annovar = [""]
    if anno.lower() in custom:
        return "custom"
    elif anno in annovar:
        return "annovar"
    else:
        return "ucsc"


def GetFileMD5(file):
    # md5 file exists : read content.
    if os.path.exists("{}.md5".format(file)):
        with open("{}.md5".format(file), "r") as fh:
            md5 = fh.readline().rstrip().split("\t")[0]
    # else : create the contents.
    else:
        with open(file, "rb") as f:
            file_hash = hashlib.md5()
            while chunk := f.read(8192):
                file_hash.update(chunk)
        md5 = file_hash.hexdigest()

        with open("{}.md5".format(file), "w") as fh:
            fh.write(file_hash.hexdigest())

    return md5


# returns true if identical, false in any other case
def CheckMD5(file, tmpdir):
    # file missing
    if not os.path.exists(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/humandb")
    ):
        return False
    # get md5s
    try:
        old_md5 = GetFileMD5(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/ANNOVAR/humandb",
                "{}_{}".format(annotator.args.build, file),
            )
        )
    except FileNotFoundError as e:
        log.warning("annotation file missing : {} : {}".format(file, e))
        old_md5 = ""

    new_md5 = GetFileMD5(os.path.join(tmpdir, file))

    # compare
    if old_md5 == new_md5:
        log.info(f"File '{file}': no changes : {old_md5} vs {new_md5}")
        return True
    else:
        log.info("File '{}': changes detected".format(file))
        return False


def Download_Annovar(source, args):
    if source == "annovar":
        webfrom = "-webfrom annovar"
    else:
        webfrom = ""
    os.makedirs(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations",
            "ANNOVAR",
            "{}humandb".format(annotator.args.prefix),
        ),
        exist_ok=True,
    )
    cmd = "cd {} && perl annotate_variation.pl -downdb {} -buildver {} {} {}humandb/".format(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/"),
        webfrom,
        annotator.args.build,
        annotator.args.anno,
        annotator.args.prefix,
    )
    print(cmd)
    subprocess.check_call(cmd, shell=True)


## CUSTOM DOWNLOAD FUNCTIONS
def Download_revel():
    # prepare
    tmpdir = tempfile.mkdtemp(
        dir=os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/humandb")
    )
    identical = list()
    # source file:
    url="https://rothsj06.dmz.hpc.mssm.edu/revel-v1.3_all_chromosomes.zip"
    try:
        log.info("Downloading Revel data file")
        # download
        outfile = os.path.join(tmpdir, "revel-raw.zip")
        with urllib.request.urlopen(url) as response, open(outfile, "wb") as out_file:
            shutil.copyfileobj(response, out_file)
        # unzip
        subprocess.check_call(["unzip",outfile, "-d",tmpdir])
        #subprocess.check_call(["rm", outfile])
        rawfile = os.path.join(tmpdir, "revel_with_transcript_ids")
    except Exception as e:
        log.error(f"Failed to download and extract raw revel data: {e}")
        sys.exit(1)
    # reformat
    try:
        log.info("Formatting raw revel scores into max-ranked annovar format")
        # trackers
        pp = ""
        max_v = {'score' : 0, 'aa' : '', 'transcript' : '', 'others' : ''}

        # outfiles
        outfile = os.path.join(tmpdir,"hg19_revel.txt")
        fh_out = open(outfile,"w")
        fh_out.write("#Chr\tStart\tEnd\tRef\tAlt\tMaxScore\tMaxAA\tMaxENST\tOtherTranscripts\n")
        
        with open(rawfile,"r") as fh:
            head = fh.readline()
            first_line = fh.readline()
            c = first_line.rstrip().split(',')
            max_v = {'score' : float(c[7]), 'aa' : c[6], 'transcript' : c[8], 'others' : '.|'}
            pp = f"{c[0]}-{c[1]}-{c[3]}-{c[4]}"
            for line in fh:
                c = line.rstrip().split(",")
                pat = f"{c[0]}-{c[1]}-{c[3]}-{c[4]}"
                if pp == pat: 
                    # new high score        
                    if float(c[7]) > max_v['score']:
                        # move prev to "others"
                        max_v["others"] += f"{max_v['score']}:{max_v['aa']}:{max_v['transcript']}|"
                        max_v['score'] = float(c[7])
                        max_v['aa'] = c[6]
                        max_v['transcript'] = c[8]
                    else:
                        max_v["others"] += f"{c[7]}:{c[6]}:{c[8]}|"
                else:
                    # new item : print
                    i = pp.split('-')
                    if max_v['others'] == '.|':
                        others = '.'
                    else: 
                        others = max_v['others'].rstrip('|').lstrip('.|')
                    fh_out.write(f"{i[0]}\t{i[1]}\t{i[1]}\t{i[2]}\t{i[3]}\t{max_v['score']}\t{max_v['aa']}\t{max_v['transcript']}\t{others}\n")
                    # reset trackers
                    max_v = {'score' : float(c[7]), 'aa' : c[6], 'transcript' : c[8], 'others' : '.|'}
                # track the pattern
                pp = pat
        # print last entry
        i = pp.split('-')
        others = max_v['others'].rstrip('|').lstrip('.|')
        fh_out.write(f"{i[0]}\t{i[1]}\t{i[1]}\t{i[2]}\t{i[3]}\t{max_v['score']}\t{max_v['aa']}\t{max_v['transcript']}\t{others}\n")
        fh_out.close()
        # mk index.
        log.info("Building Annovar index")
        cmd = "perl '{}' '{}' 1000 > '{}.idx'".format( 
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/in_house_scripts/","compileAnnnovarIndex.pl"), 
                outfile,
                outfile
        )
        subprocess.check_call(cmd,shell=True)

        # finally, get MD5
        identical.append(CheckMD5(os.path.basename("hg19_revel.txt"), tmpdir))
        identical.append(CheckMD5(os.path.basename("hg19_revel.txt.idx"), tmpdir))

    except Exception as e:
        log.error(f"Failed to parse Revel for max scores : {e}")
        sys.exit(1)
        

    

    # no diff : return
    if all(identical):
        log.info("No changes detected. No need to update.")
        return ["identical"]

    # diff : move to validation folder & launch annotator.
    os.makedirs(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/Validation_humandb"),
        exist_ok=True,
    ) 
    # mv
    try:
        target_file = os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/Validation_humandb", os.path.basename(outfile))
        shutil.move(outfile,target_file)
        shutil.move(f"{outfile}.idx",f"{target_file}.idx")
    except Exception as e:
        log.error(f"Failed to mv revel files into validation_humandb: {e}")
        sys.exit(1)

    # clean up
    shutil.rmtree(tmpdir)
    return [outfile]

def Download_ncbiGene():
    # prepare
    tmpdir = tempfile.mkdtemp(
        dir=os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/humandb")
    )
    log.info("using tmpdir : '{}'".format(tmpdir))
    base_url = "https://hgdownload.soe.ucsc.edu/goldenPath"
    # download latest & mk md5
    identical = list()
    # needed files, RefSeq is replaced by Gene in output files.
    needed = ["ncbiRefSeq.txt.gz", "ncbiRefSeqSelect.txt.gz", "ncbiRefSeqLink.txt.gz"]
    for f in needed:
        try:
            outfile = os.path.join(tmpdir, f.replace('ncbiRefSeq','ncbiGene'))
            url = "{}/{}/database/{}".format(base_url, annotator.args.build, f)
            with urllib.request.urlopen(url) as response, open(outfile, "wb") as out_file:
                shutil.copyfileobj(response, out_file)
            # urllib.request.urlretrieve("{}/{}/database/{}".format(base_url, annotator.args.build, f), outfile)
            log.info("Downloaded {}/{}/database/{} ".format(base_url, annotator.args.build, f))
            # unpack
            subprocess.check_call(["gunzip", outfile])
            outfile = outfile.rstrip(".gz")
            # reformat & rename the reflink file.
            if f == 'ncbiRefSeqLink.txt.gz':
                with open(outfile,'r') as fh_in, open(os.path.join(tmpdir, 'refLink_ncbigene.txt'),'w') as fh_out:
                    lidx = 0
                    for line in fh_in:
                        lidx += 1
                        c = line.rstrip().split("\t")
                        # IN
                        # id \t status \t symbol \t full_name \t NM \t NP \t ncbiGeneID \t omimID \t HGNC \t genbank \t <others>
                        # OUT 
                        # symbol \t full_name \t NM \t NP \t <other> \t <other> ncbiGeneID \t omimID
                        #  => <other> is unknown identifier. 
                        out_line = f"{c[2]}\t{c[3]}\t{c[4]}\t{c[5]}\t{lidx}\t{lidx}\t{c[6]}\t{c[7]}\n"
                        fh_out.write(out_line)
                # remove full file from needed.
                needed.remove(f)
                outfile = os.path.join(tmpdir, 'refLink_ncbigene.txt')
                f = os.path.basename(outfile)
                needed.append(f)

            # get md5
            md5 = GetFileMD5(outfile)
            log.info("md5 : {}".format(md5))
        except Exception as e:
            log.error(
                "Failed to download {}/{}/database/{} : {}".format(
                    base_url, annotator.args.build, f, e
                )
            )
            sys.exit(1)
        # compare to existing
        identical.append(CheckMD5(f.replace('ncbiRefSeq','ncbiGene').rstrip(".gz"), tmpdir))
    # these files come from NCBI
    ncbi = ftplib.FTP("ftp.ncbi.nlm.nih.gov")
    ncbi.login("anonymous", "")
    ncbi_files = ncbi.nlst("refseq/MANE/MANE_human/current/")
    for file in ncbi_files:
        # transcripts NOT in mane (but in UCSC) + reason
        if re.search(r"genes_not_in_mane.txt.gz", file):
            try:
                log.info("Downloading not-in-mane file.")
                outfile = os.path.join(tmpdir, "genes_not_in_mane.txt.gz")
                ncbi.retrbinary("RETR {}".format(file), open(outfile, "wb").write)
                log.info("Unzip not-in-mane file")
                subprocess.check_call(["gunzip", outfile])

            except Exception as e:
                log.error("Failed to download {} : {}".format(file, e))
                sys.exit(1)

        # mane select + clinical
        elif re.search(r"summary.txt.gz", file):
            try:
                log.info("Downloading mane summary file.")
                outfile = os.path.join(tmpdir, "ncbi.mane.txt.gz")
                ncbi.retrbinary("RETR {}".format(file), open(outfile, "wb").write)
                log.info("Unzip & cut mane summary file.")
                subprocess.check_call(
                    "zcat {} | cut -f 6,10 > {}".format(outfile, outfile.rstrip(".gz")),
                    shell=True,
                )

            except Exception as e:
                log.error("Failed to download {} : {}".format(file, e))
                sys.exit(1)
    # combine the files into one MANE list : NM_...\tMane_Type : Mane_Type = [Mane, Clinical, Problematic]
    try:
        # read gene symbols + reason from "not_in_mane"
        not_in_mane = dict()
        log.info("Read not-in-mane file")
        with open(os.path.join(tmpdir, "genes_not_in_mane.txt"), "r") as fh:
            for line in fh:
                c = line.rstrip().split("\t")
                not_in_mane[c[2]] = c[3]
        # then scan the UCSC list . matching symbols are added to ncbi.mane.txt
        log.info("Appending to mane file")
        with open(os.path.join(tmpdir, "ncbi.mane.txt"), "a") as f_out, open(
            os.path.join(tmpdir, "ncbiGeneSelect.txt"), "r"
        ) as f_in:
            for line in f_in:
                c = line.rstrip().split("\t")
                # not in mane for a reason : add
                if c[12] in not_in_mane:
                    log.debug("appending {}".format(c[1]))
                    f_out.write("{}\t{}\n".format(c[1], not_in_mane[c[12]]))

        # finally, get MD5
        identical.append(CheckMD5(os.path.basename("ncbi.mane.txt"), tmpdir))

    except Exception as e:
        log.error("Failed to extract transcripts for not-in-mane genes: {}".format(e))
        sys.exit(1)

    # no diff : return
    if all(identical):
        log.info("No changes detected. No need to update.")
        return ["identical"]

    # diff : move to validation folder & launch annotator.
    os.makedirs(
        os.path.join(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/Validation_humandb")
        ),
        exist_ok=True,
    )
    # mv
    needed.append("ncbi.mane.txt")
    new_files = list()
    for f in needed:
        for suffix in ["", ".md5"]:
            src = os.path.join(tmpdir, "{}{}".format(f.replace('ncbiRefSeq','ncbiGene').rstrip(".gz"), suffix))
            target = os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/ANNOVAR/Validation_humandb",
                "{}_{}{}".format(annotator.args.build, f.replace('ncbiRefSeq','ncbiGene').rstrip(".gz"), suffix),
            )

            shutil.move(src, target)
            new_files.append(target)
    # extra files to create annovar-version of mRNA:
    try:
        seqfile = GetGenomeSeq()
    except Exception as e:
        log.error("Genomic FASTA not found : {}".format(e))
        sys.exit(1)
    mRNA_file = target = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/ANNOVAR/Validation_humandb/{}_ncbiGeneMrna.fa".format(annotator.args.build),
    )
    new_files.append(target)
    region_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/ANNOVAR/Validation_humandb/{}_ncbiGene.txt".format(annotator.args.build),
    )
    new_files.append(region_file)
    log.info("Construct custom mRNA File using ANNOVAR")
    cmd = [
        "perl",
        "retrieve_seq_from_fasta.pl",
        "--altchr",
        "--outfile",
        mRNA_file,
        "--seqfile",
        seqfile,
        "--format",
        "refGene",
        region_file,
    ]
    try:
        subprocess.check_call(cmd)
    except Exception as e:
        log.error("Failed to generate mRNA file : {}".format(e))
        sys.exit(1)

    # ncbi mRNA sequences :
    build_map = {"hg19": "GCF_000001405.25_GRCh37.p13"}
    url = "genomes/refseq/vertebrate_mammalian/Homo_sapiens/all_assembly_versions/{}/{}_rna.fna.gz".format(
        build_map[args.build], build_map[args.build]
    )
    outfile = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/ANNOVAR/Validation_humandb/{}_ncbiGene.fromNCBI.fa.gz".format(
            annotator.args.build
        ),
    )
    try:
        ncbi = ftplib.FTP("ftp.ncbi.nlm.nih.gov")
        ncbi.login("anonymous", "")
        ncbi.retrbinary("RETR {}".format(url), open(outfile, "wb").write)
        # urllib.request.urlretrieve(url, outfile)
        subprocess.check_call(["gunzip", "--force", outfile])
        new_files.append(outfile.rstrip(".gz"))
    except Exception as e:
        log.error("Failed to download/unpack ncbi-mrna sequences : {}".format(e))
        sys.exit(1)

    # clean up
    shutil.rmtree(tmpdir)
    # return
    return new_files


def GetGenomeSeq():
    seqfile = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/ANNOVAR/humandb/{}_allChr.fa".format(annotator.args.build),
    )
    # present : ok.
    if os.path.exists(seqfile):
        return seqfile
    # download.
    urllib.request.urlretrieve(
        "https://hgdownload.soe.ucsc.edu/goldenPath/{}/bigZips/{}.fa.gz".format(
            annotator.args.build, annotator.args.build
        ),
        "{}.gz".format(seqfile),
    )
    # unpack
    cmd = ["gunzip", "{}.gz".format(seqfile)]
    subprocess.check_call(cmd)
    if os.path.exists(seqfile):
        return seqfile
    else:
        raise DataError("Unable to download reference fasta. Please investigate")


def CompareResults():

    if str(annotator.args.anno).endswith("Gene"):
        try:
            CompareGene()
        except Exception as e:
            log.error("Failed to compare gene annotations : {}".format(e))
            sys.exit(1)
    else:
        log.warning("Non-Gene Comparison is not implemented yet.")
        log.warning("Procedure ends here")
        sys.exit(0)


def CompareGene():
    # get codings
    codes = LoadDecodingInfo()
    # open all output files.
    time_stamp = date.today().strftime("%Y-%m-%d")
    out_files = dict()
    for vtype in ["SNV", "INS", "DEL", "SUB", "NEW", "LOST", "LOG"]:
        out_files[vtype] = open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/ANNOVAR/Output_Files/",
                "Validation.{}.{}.{}".format(annotator.args.anno, time_stamp, vtype),
            ),
            "w",
        )
    # some trackers
    nr_match = 0
    nr_new = 0
    nr_lost = 0
    # work in slices of 50K variants :
    start_vid = 0

    while True:
        # get last vid.
        vids = annotator.dbh.runQuery(
            "SELECT id FROM `Variants` WHERE id >= %s ORDER BY `id` LIMIT 50000", start_vid
        )
        if not vids:
            # no data left.
            break
        last_vid = vids[-1]["id"]
        log.info("Working on 50K variants with VIDS from {} to {}".format(start_vid, last_vid))
        vids = []

        # get old annotations
        log.info("Fetching old annotations")

        rows = annotator.dbh.runQuery(
            "SELECT va.vid,va.aid, va.GeneSymbol, va.Transcript, va.Exon, va.GeneLocation, va.VariantType, va.CpointNT, va.CpointAA, va.ProteinLengthDifference FROM `{}` va WHERE va.vid BETWEEN %s AND %s".format(
                annotator.args.table_name
            ),
            [start_vid, last_vid],
            as_dict=False,
        )
        old_annotations = AutoVivification()
        for row in rows:
            row = list(row)
            # strip transcript version
            vid = row.pop(0)
            aid = row.pop(0)
            row = [(x if x else ".") for x in row]
            nm = row[1]
            row[1] = re.sub(r"\.\d+$", "", row[1])
            # then decde & replace empty by .
            # row[8] = codes["{}_{}_{}".format(annotator.args.table_name, "GeneLocation", row[8])]
            # row[9] = codes["{}_{}_{}".format(annotator.args.table_name, "VariantType", row[9])]
            # vid : Gene|Transcript : version_NM + full row without version.
            old_annotations[vid][aid] = [nm] + row

        # get new annotations & compare
        log.info("Fetching and comparing new annotations.")
        rows = annotator.dbh.runQuery(
            "SELECT va.vid,va.aid, va.GeneSymbol, va.Transcript, va.Exon, va.GeneLocation, va.VariantType, va.CpointNT, va.CpointAA, va.ProteinLengthDifference FROM `Validation_{}` va WHERE va.vid BETWEEN %s AND %s".format(
                annotator.args.table_name
            ),
            [start_vid, last_vid],
            as_dict=False,
        )
        for row in rows:
            row = list(row)
            vid = row.pop(0)
            aid = row.pop(0)
            row = [(x if x else ".") for x in row]
            nm = row[1]
            # raw values for Variants_x_*Gene
            db_fields = ListOfStrings([vid] + row)
            row[1] = re.sub(r"\.\d+$", "", row[1])

            db_names = [
                "vid",
                "GeneSymbol",
                "Transcript",
                "Exon",
                "GeneLocation",
                "VariantType",
                "CPointNT",
                "CPointAA",
                "ProteinLengthDifference",
            ]

            
            # annotation not seen in old table (new gene?)
            # vid not in old annotations:
            if not vid in old_annotations:
                nr_new += 1
                if annotator.args.validate:
                    out_files["NEW"].write("{}\n".format("; ".join(ListOfStrings(row))))
                else:
                    # insert into main table
                    new_aid = annotator.dbh.insertQuery(
                        "INSERT INTO `{}` (`{}`) VALUES ('{}')".format(
                            annotator.args.table_name, "`,`".join(db_names), "','".join(db_fields)
                        )
                    )

                    # add to log.
                    out_files["LOG"].write(
                        "{}\t{}\t{}:{}\t2\n".format(
                            vid, annotator.args.userid, annotator.args.table_name, new_aid
                        )
                    )
                continue
            
            # vid exists in old data : loop all aids for match.
            match = False
            for o_aid in list(old_annotations[vid]):
                # skip first entry (nm) in old_annotation entry
                if row == old_annotations[vid][o_aid][1:]:
                    nr_match += 1
                    match = True
                    del old_annotations[vid][o_aid]
                    break

            if not match:
                # mismatch : add new
                nr_new += 1
                if annotator.args.validate:
                    out_files["NEW"].write("{}\n".format("; ".join(ListOfStrings(row))))
                else:

                    # add new:
                    new_aid = annotator.dbh.insertQuery(
                        "INSERT INTO `{}` (`{}`) VALUES ('{}')".format(
                            annotator.args.table_name, "`,`".join(db_names), "','".join(db_fields)
                        )
                    )

                    # add to log.
                    out_files["LOG"].write(
                        "{}\t{}\t{}:{}\t2\n".format(
                            vid, annotator.args.userid, annotator.args.table_name, new_aid
                        )
                    )
                    

        # any left on the old_annotations stack : these are lost.
        for vid in old_annotations:
            for aid in old_annotations[vid]:
                nr_lost += 1
                # get full nm.vers back in place
                full_nm = old_annotations[vid][aid].pop(0)
                old_annotations[vid][aid][1] = full_nm
                if annotator.args.validate:
                    out_files["LOST"].write(
                        "{}\n".format("; ".join(ListOfStrings(old_annotations[vid][aid])))
                    )
                else:
                    # archive old annotation. (without vid field)
                    db_fields = [vid] + old_annotations[vid][aid]
                    d = list(zip(db_names, db_fields))
                    # del d[0]
                    contents = "@@@".join(["{}|||{}".format(i[0], i[1]) for i in d])
                    # archive
                    archive_aid = annotator.dbh.insertQuery(
                        "INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (%s,%s,%s,%s,%s)",
                        [
                            vid,
                            annotator.args.table_name,
                            f"-{args.build}",
                            time_stamp,
                            contents,
                        ],
                    )
                    # log
                    out_files["LOG"].write(
                        "{}\t{}\t{}:{}\t1\n".format(
                            vid, annotator.args.userid, annotator.args.table_name, archive_aid
                        )
                    )
                    # delete the annotation entry.
                    annotator.dbh.doQuery(
                        f"DELETE FROM `{args.table_name}` WHERE vid = %s AND aid = %s",
                        [vid, aid],
                    )
        # clear
        old_annotations.clear()
        # next iteration.
        start_vid = last_vid + 1
        # DEV : break
    # overview.
    log.info("STATISTICS:")
    log.info(f"  MATCH : {nr_match}")
    log.info(f"  NOVEL : {nr_new}")
    log.info(f"  LOST  : {nr_lost}")

    # close file handles
    for fh in out_files:
        out_files[fh].close()
    # load to log-table.
    if not annotator.args.validate:
        annotator.dbh.doQuery(
            "LOAD DATA LOCAL INFILE '{}' INTO TABLE `Variants_x_Log` (`vid`, `uid`, `entry`, `arguments`)".format(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/ANNOVAR/Output_Files/",
                    "Validation.{}.{}.{}".format(annotator.args.anno, time_stamp, "LOG"),
                )
            )
        )


def ListOfStrings(data):
    return [str(x) for x in data]


def LoadDecodingInfo():
    # value codings:
    rows = annotator.dbh.runQuery(
        "SELECT  HIGH_PRIORITY  id, `Table_x_Column`, `Item_Value` FROM `Value_Codes`"
    )
    codes = dict()
    for row in rows:
        # map column + ID to text : rewrite "table" to "subtable"
        codes["{}_{}".format(row["Table_x_Column"], row["id"])] = row["Item_Value"]

    return codes


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    log = StartLogger(config, f"ANNOVAR.{args.anno}")

    # get the annotator object
    annotator = Annotator(config=config, method="ANNOVAR", annotation=args.anno, args=args)

    # get userid of admin user (for archive logs)
    try:
        annotator.args.userid = annotator.dbh.runQuery(
            "SELECT id FROM `Users` WHERE `email` = %s", [config["USERS"]["ADMIN"]]
        )[0]["id"]
    except Exception as e:
        log.error("Admin email not recognized by VariantDB : {}".format(e))
        sys.exit(1)

    # get corresponding table / file names.
    try:
        GetNames()
        log.info(
            "Annotation : {} => Table: {} ; annotation (edit): {}".format(
                annotator.args.anno, annotator.args.table_name, annotator.args.annotation_file
            )
        )
    except DataError as e:
        log.error("Could not initiate analysis: {}".format(e))
        sys.exit(1)

    # validate the tables & files.
    try:
        annotator.ValidateTable()
    except DataError as e:
        log.error("Could not locate necessary resources: {}".format(e))
        sys.exit(1)

    if not annotator.args.validate:
        # where is the data ?
        source = GetSource(annotator.args.anno)

        if source == "custom":
            # locate function
            try:
                f = globals()["Download_{}".format(annotator.args.anno)]
            except KeyError as e:
                log.warning(
                    "Custom download function 'Download_{}' not found".format(annotator.args.anno)
                )
                log.warning("Procedure ends here.")
                sys.exit(0)

            # run function
            try:
                log.debug("Downloading data.")
                new_files = f()
            except Exception as e:
                log.error("Failed to run Download_{}() : {}".format(annotator.args.anno, e))
                sys.exit(1)
        else:
            log.info("get from {}".format(source))
            try:
                log.warning("Non_Gene updates are not implemented yet.")
                log.warning("Procedure ends here.")
                sys.exit(0)
                # Download_Annovar(source, args)
            except Exception as e:
                log.error(
                    "Failed to download annotation files for {}: {}".format(annotator.args.anno, e)
                )

    # no changes detected.
    if not annotator.args.download and not annotator.args.validate and new_files[0] == "identical":
        log.info("No new release found. Update is not needed. Procedure ends here")
        sys.exit(0)

    # run the annotation procedures
    if not annotator.args.download:
        try:
            # the command :
            log.info("Running ANNOVAR for ALL variants.")
            cmd = [
                "python",
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/LoadVariants.py"
                ),
                "-a",
                annotator.args.anno,
                "-v",
            ]
            subprocess.check_call(cmd)
        except Exception as e:
            log.error("ANNOVAR failed : {}".format(e))
            sys.exit(1)

        # compare results.
        try:
            CompareResults()
        except Exception as e:
            log.error("Failure in result comparison : {}".format(e))
            sys.exit(1)

        # put new data in production folder.
        if not annotator.args.validate:
            target = os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/humandb")
            for source in new_files:
                try:
                    shutil.copy(source, target)
                except Exception as e:
                    log.error(f"Failed to copy {source} to production location: {e}")
                    sys.exit(1)

# cleanup
