#!/usr/bin/perl
$|++;
open IN, "zcat $ARGV[0] |";
# skip headers.
while (<IN>) {
	if ($_ =~ m/^#CHROM/) {
		last;
	}
}
open OUT, ">AN.txt";
my $out = "#Chr\tStart\tEnd\tRef\tAlt\tAN\tAN_AFR\tAN_AMR\tAN_EAS\tAN_FIN\tAN_NFE\tAN_OTH\tAN_SAS\n";
my $chr = '';
while (<IN>) {
	chomp;
	my @c = split(/\t/,$_);
	if ($chr ne $c[0]) {
		print "Processing Chromosome $c[0]\n";
		print OUT $out;
		$out = '';
		$chr = $c[0];
	}
	my %h = ();
	foreach(split(/;/,$c[7])) {
		my ($k,$v) = split(/=/,);
		$h{$k} = $v;
	}
	#my $rl = length($c[3]);
	#my $end = $c[1] + $rl -1;
	foreach(split(/,/,$c[4])) {
		my $ref = $c[3];
		my $alt = $_;
		my $raw_alt = $_;
		my $start = $c[1];
		my $stop = $start;
		# raw entry : 
		my $end = $c[1] + length($c[3]) - 1;
		$out .= "$c[0]\t$c[1]\t$end\t$c[3]\t$_\t$h{'AN_Adj'}\t$h{'AN_AFR'}\t$h{'AN_AMR'}\t$h{'AN_EAS'}\t$h{'AN_FIN'}\t$h{'AN_NFE'}\t$h{'AN_OTH'}\t$h{'AN_SAS'}\n";

		# trim alleles from 3' : AC/TC => A/T  ; GTTT/GT => GTT/G
		my $max_i = 0;
		if (length($ref) < length($alt)) { # maximal 3'->5' shift == length of shortest fragment
			$max_i = length($ref) ;
		}
		else {
			$max_i = length($alt) ;
		}
		my $i = 0;
		while  ($i < $max_i && substr($ref,(length($ref)-$i-1),1) eq substr($alt,(length($alt)-$i-1),1)) {
			$i++;
		}
		$ref = substr($ref,0,(length($ref)-$i));
		$alt = substr($alt,0,(length($alt)-$i));
		# insertion: position is previous base.
		if ($ref eq '' ) {
			$start = $stop = $start ;
		}
		else {
			$start = $start;
			$stop = $start + length($ref) - 1 ;
		}
=cut
		## empty => - 
		if ($ref eq '') {
			$ref = '-';
		}
		if ($alt eq '') {
			$alt = '-';
		}
=cut

		# trim allele from 5' : GTT/G => TT/-
		
		$i = 0;
		while  (substr($ref,$i,1) eq substr($alt,$i,1)) {
			$i++;
			if ($i == length($ref) || $i == length($alt)) {
				last;
			}
		}
		$ref = substr($ref,$i);
		$alt = substr($alt,$i);
		# insertion: position is previous base. (only if something is trimmed)
		if ($ref eq '' && $i > 0) {
			$start = $stop = $start + $i - 1;
		}
		elsif ($i > 0) {
			$start = $start + $i;
			$stop = $start + length($ref) -1;
		}
		## empty => - 
		if ($ref eq '') {
			$ref = '-';
		}
		if ($alt eq '') {
			$alt = '-';
		}
=cut
		# trim alleles from 3' : AC/TC => A/T
		my $max_i = 0;
		if (length($ref) < length($alt)) { # maximal 3'->5' shift == length of shortest fragment
			$max_i = length($ref) ;
		}
		else {
			$max_i = length($alt) ;
		}
		$i = 0;
		while  ($i < $max_i && substr($ref,(length($ref)-$i-1),1) eq substr($alt,(length($alt)-$i-1),1)) {
			$i++;
		}
		$ref = substr($ref,0,(length($ref)-$i));
		$alt = substr($alt,0,(length($alt)-$i));
		# insertion: position is previous base.
		if ($ref eq '' ) {
			$start = $stop = $start ;
		}
		else {
			$start = $start;
			$stop = $start + length($ref) - 1 ;
		}
		## empty => - 
		if ($ref eq '') {
			$ref = '-';
		}
		if ($alt eq '') {
			$alt = '-';
		}
=cut
		if ($ref ne $c[3] || $alt ne $raw_alt) {
			#if (substr($start,0,8) eq '10015955') {
			#	print "$c[0]\t$start\t$stop\t$ref\t$alt\n";
			#}
			$out .= "$c[0]\t$start\t$stop\t$ref\t$alt\t$h{'AN_Adj'}\t$h{'AN_AFR'}\t$h{'AN_AMR'}\t$h{'AN_EAS'}\t$h{'AN_FIN'}\t$h{'AN_NFE'}\t$h{'AN_OTH'}\t$h{'AN_SAS'}\n";
		}
	}
}
close IN;
print OUT $out;
close OUT;
