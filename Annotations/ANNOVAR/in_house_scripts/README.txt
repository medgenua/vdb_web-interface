compileAnnnovarIndex.pl
=======================
- compile an annovar index from a generic annotation table. 
- Table should have a header line
- table should have columns:
 - chr
 - start 
 - stop 
 - ref-allele
 - alt-allele 
 - annotation data.
	=> No bin column in front  ! (ucsc style).
- Usage : perl compileAnnnovarIndex.pl hg19_myTable.txt 1000 > hg19.myTable.txt.idx


Parse.exac.for.AN.pl
====================
- Extract the allele numbers from the Exac VCF release files. default annovar table only provides AF values. 
- extracts all populations
- ALL population is based on the AN_Adj, similar to annovar AF_ALL, which is AC_Adj/AN_Adj.
- usage: perl Parse.exac.for.AN.pl exac.vcf.gz 
- output : AN.txt
