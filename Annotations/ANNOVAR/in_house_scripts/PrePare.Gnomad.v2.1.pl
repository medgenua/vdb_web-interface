#!/usr/bin/perl
use Parallel::ForkManager; 
use strict;
use warnings;

## goal : download gnomad ; parse ; build annovar table


my $pm = new Parallel::ForkManager(12);

# chromosomes to process
our @chrs = (1..22,'X');
#our @chrs = ('X');
# populations present
# with genders
our @populations = qw/fin afr eas nfe amr asj oth/;

# no genders available
our @nogenders = qw/nfe_nwe/;
our @controls = qw/controls_<tag> controls_<tag>_male controls_<tag>_female non_topmed_<tag> non_topmed_<tag>_male non_topmed_<tag>_female non_neuro_<tag> non_neuro_<tag>_male non_neuro_<tag>_female controls_<tag>_nfe controls_<tag>_nfe_male controls_<tag>_nfe_female non_topmed_<tag>_nfe non_topmed_<tag>_nfe_male non_topmed_<tag>_nfe_female controls_<tag>_nfe_nwe non_topmed_<tag>_nfe_nwe/;


# axel installed?
my $axel = `which axel`;
chomp($axel);
if ($axel ne '') {
	$axel = 1;
}
else {
	$axel = 0;
}
# base urls
my $genomes = "https://storage.googleapis.com/gnomad-public/release/2.1/vcf/genomes/gnomad.genomes.r2.1.sites.<chr>.vcf.bgz";
my $exomes = "https://storage.googleapis.com/gnomad-public/release/2.1/vcf/exomes/gnomad.exomes.r2.1.sites.<chr>.vcf.bgz";

#############
## GENOMES ##
#############
system("mkdir -p 'genomes'");
my $file = "genomes/hg19_gnomAD_g_2.1.txt";

if (-f "$file") {
	print "Genomes file '$file' is already present. Skipping parser\n";
	goto EXOMES;
}
foreach my $chr (@chrs) {
	#download genome data.
	my $lfile =  "genomes/hg19_gnomAD_g_$chr.2.1.txt.gz";
	if (-f "$lfile") {
		print "Genomes.chr$chr already processed: $lfile : Skipping\n";
		#$pm->finish();
		next;
	}
	my $vcf_in = "genomes/gnomad.chr$chr.vcf.bgz";
	if (-f $vcf_in) {
		print "VCF exists : skipping download : $vcf_in\n";
	}
	else {
		print "Downloading genome data : $chr.\n";

		my $url = $genomes;
		$url =~ s/<chr>/chr$chr/;
		if ($axel == 1) {
			system("axel -a '$url' -o '$vcf_in'");
		} else {
			system("wget '$url' -O '$vcf_in'");
		}
	}
	# start parsing thread.
        my $pid = $pm->start and next;
	# parse genome data.
	&parseVCF("genomes/gnomad.chr$chr.vcf.bgz","$lfile.tmp");
	system("mv '$lfile.tmp' '$lfile'");
	$pm->finish();
}

#############
## EXOMES ##
#############
EXOMES:
system("mkdir -p 'exomes'");
$file = "exomes/hg19_gnomAD_e_2.1.txt.gz";
if (-f "$file") {
	print "Exome file '$file' is already present. Skipping parser\n";
	goto WAIT;
}
foreach my $chr (@chrs) {
	#download genome data.
	my $lfile =  "exomes/hg19_gnomAD_e_$chr.2.1.txt.gz";
	if (-f "$lfile") {
		print "Exomes.chr$chr already processed: $lfile : Skipping\n";
		#$pm->finish();
		next;
	}
	my $vcf_in = "exomes/gnomad.chr$chr.vcf.bgz";
	if (-f $vcf_in) {
		print "VCF exists : skipping download : $vcf_in\n";
	}
	else {
		print "Downloading exome data : $chr.\n";

		my $url = $exomes;
		$url =~ s/<chr>/chr$chr/;
		if ($axel == 1) {
			system("axel -a '$url' -o '$vcf_in'");
		} else {
			system("wget '$url' -O '$vcf_in'");
		}
	}
	# start parsing thread.
        my $pid = $pm->start and next;
	# parse genome data.
	&parseVCF("exomes/gnomad.chr$chr.vcf.bgz","$lfile.tmp");
	system("mv '$lfile.tmp' '$lfile'");
	$pm->finish();
}

WAIT:
print "Waiting for parsers to finish\n";
$pm->wait_all_children;
# concat genome data.
$file = "genomes/hg19_gnomAD_g_2.1.txt";
if (!-f "$file") {
	print "Combine Genome files\n";;

	open OUT, ">$file";
	my $header = "#Chr\tStart\tEnd\tRef\tAlt\tgnomAD_Gen_AF_ALL\tgnomAD_Gen_AN_ALL\tgnomAD_Gen_Hom_ALL";
	  $header .= "\tgnomAD_Gen_AF_Female\tgnomAD_Gen_AN_Female\tgnomAD_Gen_Hom_Female";
	  $header .= "\tgnomAD_Gen_AF_Male\tgnomAD_Gen_AN_Male\tgnomAD_Gen_Hom_Male";


	foreach my $pop (@populations) {
		$header .= "\tgnomAD_Gen_AF_$pop\tgnomAD_Gen_AN_$pop\tgnomAD_Gen_Hom_$pop";
		$header .= "\tgnomAD_Gen_AF_${pop}_Female\tgnomAD_Gen_AN_${pop}_Female\tgnomAD_Gen_Hom_${pop}_Female";
		$header .= "\tgnomAD_Gen_AF_${pop}_Male\tgnomAD_Gen_AN_${pop}_Male\tgnomAD_Gen_Hom_${pop}_Male";
	}
	foreach my $pop (@nogenders) {
		$header .= "\tgnomAD_Gen_AF_$pop\tgnomAD_Gen_AN_$pop\tgnomAD_Gen_Hom_$pop";
	}
	foreach my $controle (@controls) {
		# autosomes and generated values for Y
		my $af = $controle;
		$af =~ s/<tag>/AF/;
		my $an = $controle;
		$an =~ s/<tag>/AN/;
		my $nhomalt = $controle;
		$nhomalt =~ s/<tag>/nhomalt/;
		$header .= "\tgnomAD_Gen_$af\tgnomAD_Gen_$an\tgnomAD_Gen_$nhomalt";
	}
	print OUT "$header\n";
	close OUT;
	foreach my $chr (@chrs) {
		my $lfile =  "genomes/hg19_gnomAD_g_$chr.2.1.txt.gz";
		system("zcat '$lfile' >> '$file'");
	}
}
# concat exome data.
$file = "exomes/hg19_gnomAD_e_2.1.txt";
if (!-f "$file") {
	print "Combine exome files\n";;

	open OUT, ">$file";
	my $header = "#Chr\tStart\tEnd\tRef\tAlt\tgnomAD_Ex_AF_ALL\tgnomAD_Ex_AN_ALL\tgnomAD_Ex_Hom_ALL";
	  $header .= "\tgnomAD_Ex_AF_Female\tgnomAD_Ex_AN_Female\tgnomAD_Ex_Hom_Female";
	  $header .= "\tgnomAD_Ex_AF_Male\tgnomAD_Ex_AN_Male\tgnomAD_Ex_Hom_Male";

	foreach my $pop (@populations) {
		$header .= "\tgnomAD_Ex_AF_$pop\tgnomAD_Ex_AN_$pop\tgnomAD_Ex_Hom_$pop";
		$header .= "\tgnomAD_Ex_AF_${pop}_Female\tgnomAD_Ex_AN_${pop}_Female\tgnomAD_Ex_Hom_${pop}_Female";
		$header .= "\tgnomAD_Ex_AF_${pop}_Male\tgnomAD_Ex_AN_${pop}_Male\tgnomAD_Ex_Hom_${pop}_Male";

	}
	foreach my $pop (@nogenders) {
		$header .= "\tgnomAD_Ex_AF_$pop\tgnomAD_Ex_AN_$pop\tgnomAD_Ex_Hom_$pop";
	}

	foreach my $controle (@controls) {
		# autosomes and generated values for Y
		my $af = $controle;
		$af =~ s/<tag>/AF/;
		my $an = $controle;
		$an =~ s/<tag>/AN/;
		my $nhomalt = $controle;
		$nhomalt =~ s/<tag>/nhomalt/;
		$header .= "\tgnomAD_Ex_$af\tgnomAD_Ex_$an\tgnomAD_Ex_$nhomalt";
	}

	print OUT "$header\n";
	close OUT;
	foreach my $chr (@chrs) {
		my $lfile =  "exomes/hg19_gnomAD_e_$chr.2.1.txt.gz";
		system("zcat '$lfile' >> '$file'");
	}
}

print "All Done\n";

sub parseVCF {
	my $total = 0;
	my ($in,$out) = @_;
	open IN, "zcat $in | " or die("Could not open '$in' for reading\n");
	my $idx = 0;
	my $buffer = '';
	while (my $line = <IN>) {
		next if ($line =~ m/^#/);
		$idx++;
		chomp($line);
		my ($chr,$orig_start,$dbsnp,$orig_ref,$altlist,$qual,$filter,$info) = split(/\t/,$line);
		#print "$chr:$orig_start : $orig_ref/$altlist\n";
		# get alternate alleles
		my @alts = split(/,/,$altlist);
		# parse info fields to hash
		my %info = ();
		foreach (split(/;/,$info)) {
			my ($k,$v) = split(/=/,$_);
			if (defined($v)) {
				my @values = split(/,/,$v);	
				$info{$k} = \@values;
			}
		}
		# go over alleles (v2.1 multiallelics are split, but kept this way.
		for (my $aidx = 0; $aidx < scalar(@alts); $aidx++) {
			my $ref = $orig_ref;
			my $start = $orig_start;
			my $alt = $alts[$aidx];
			my $raw_ref = $ref;
			my $raw_alt = $alt;
			my $stop = $start;

			# trim alleles from 3' : AC/TC => A/T  ; GTTT/GT => GTT/G
			my $max_i = 0;
			if (length($ref) < length($alt)) { # maximal 3'->5' shift == length of shortest fragment
				$max_i = length($ref) ;
			}
			else {
				$max_i = length($alt) ;
			}
			my $i = 0;
			while  ($i < $max_i && substr($ref,(length($ref)-$i-1),1) eq substr($alt,(length($alt)-$i-1),1)) {
				$i++;
			}
			$ref = substr($ref,0,(length($ref)-$i));
			$alt = substr($alt,0,(length($alt)-$i));
			# insertion: position is previous base.
			if ($ref eq '' ) {
				$start = $stop = $start ;
			}
			else {
				$start = $start;
				$stop = $start + length($ref) - 1 ;
			}

			# trim allele from 5' : GTT/G => TT/-
			$i = 0;
			while  (substr($ref,$i,1) eq substr($alt,$i,1)) {
				$i++;
				if ($i == length($ref) || $i == length($alt)) {
					last;
				}
			}
			$ref = substr($ref,$i);
			$alt = substr($alt,$i);
			# insertion: position is previous base. (only if something is trimmed)
			if ($ref eq '' && $i > 0) {
				$start = $stop = $start + $i - 1;
			}
			elsif ($i > 0) {
				$start = $start + $i;
				$stop = $start + length($ref) -1;
			}
			## empty => - 
			if ($ref eq '') {
				$ref = '-';
			}
			if ($alt eq '') {
				$alt = '-';
			}

						
			# raw entry (untrimmed)
			my $end = $orig_start + length($raw_ref) - 1;
			if (!defined($info{AF}->[$aidx])) {
				$info{AF}->[$aidx] = 0;
			}
			$buffer .= "$chr\t$orig_start\t$end\t$raw_ref\t$raw_alt\t$info{AF}->[$aidx]\t$info{AN}->[0]\t$info{'nhomalt'}->[$aidx]";
			if (!defined($info{AF_female}->[$aidx])) {
				$info{AF_female}->[$aidx] = 0;
			}
			$buffer .= "\t$info{AF_female}->[$aidx]\t$info{AN_female}->[0]\t$info{'nhomalt_female'}->[$aidx]";
			if (!defined($info{AF_male}->[$aidx])) {
				$info{AF_male}->[$aidx] = 0;
			}
			$buffer .= "\t$info{AF_male}->[$aidx]\t$info{AN_male}->[0]\t$info{'nhomalt_male'}->[$aidx]";
			foreach my $pop (@populations) {
				# both sexes
				if (!defined($info{"AF_$pop"}->[$aidx])) {
					$info{"AF_$pop"}->[$aidx] = 0;
				}
				$buffer .= "\t".$info{"AF_$pop"}->[$aidx]."\t".$info{"AN_$pop"}->[0]."\t".$info{"nhomalt_$pop"}->[$aidx] or die();
				# female
				if (!defined($info{"AF_${pop}_female"}->[$aidx])) {
					$info{"AF_${pop}_female"}->[$aidx] = 0;
				}
				$buffer .= "\t".$info{"AF_${pop}_female"}->[$aidx]."\t".$info{"AN_${pop}_female"}->[0]."\t".$info{"nhomalt_${pop}_female"}->[$aidx] or die();
				# male
				if (!defined($info{"AF_${pop}_male"}->[$aidx])) {
					$info{"AF_${pop}_male"}->[$aidx] = 0;
				}
				if (!defined($info{"AN_${pop}_male"})) {
					print "ND : ${pop}_male\n";
					print "$line\n\n";
				}
				$buffer .= "\t".$info{"AF_${pop}_male"}->[$aidx]."\t".$info{"AN_${pop}_male"}->[0]."\t".$info{"nhomalt_${pop}_male"}->[$aidx] or die();

			}
			foreach my $pop (@nogenders) {
				# both sexes
				if (!defined($info{"AF_$pop"}->[$aidx])) {
					$info{"AF_$pop"}->[$aidx] = 0;
				}
				$buffer .= "\t".$info{"AF_$pop"}->[$aidx]."\t".$info{"AN_$pop"}->[0]."\t".$info{"nhomalt_$pop"}->[$aidx] or die();
			}
			foreach my $controle (@controls) {
				# autosomes and generated values for Y
				my $af = $controle;
				$af =~ s/<tag>/AF/;
				my $an = $controle;
				$an =~ s/<tag>/AN/;
				my $nhomalt = $controle;
				$nhomalt =~ s/<tag>/nhomalt/;
				if (!defined($info{$af}->[$aidx])) {
					$info{$af}->[$aidx] = 0;
				}
				if (!defined($info{$an})) {
					print "not defined AN: $an\n";
				}
				$buffer .= "\t".$info{$af}->[$aidx]."\t".$info{$an}->[0]."\t".$info{$nhomalt}->[$aidx];
			}
			$buffer .= "\n";
			# if the alleles were trimmed : add normalized version.
			if ($ref ne $raw_ref || $alt ne $raw_alt) {
				$buffer .= "$chr\t$start\t$stop\t$ref\t$alt\t$info{AF}->[$aidx]\t$info{AN}->[0]\t$info{nhomalt}->[$aidx]";
				$buffer .= "\t$info{AF_female}->[$aidx]\t$info{AN_female}->[$aidx]\t$info{nhomalt_female}->[$aidx]";
				$buffer .= "\t$info{AF_male}->[$aidx]\t$info{AN_male}->[$aidx]\t$info{nhomalt_male}->[$aidx]";

				foreach my $pop (@populations) {
					# both sexes
					$buffer .= "\t".$info{"AF_$pop"}->[$aidx]."\t".$info{"AN_$pop"}->[0]."\t".$info{"nhomalt_$pop"}->[$aidx];
					# female
					$buffer .= "\t".$info{"AF_${pop}_female"}->[$aidx]."\t".$info{"AN_${pop}_female"}->[0]."\t".$info{"nhomalt_${pop}_female"}->[$aidx];	
					# male
					$buffer .= "\t".$info{"AF_${pop}_male"}->[$aidx]."\t".$info{"AN_${pop}_male"}->[0]."\t".$info{"nhomalt_${pop}_male"}->[$aidx];
				}
				foreach my $pop (@nogenders) {
					# both sexes
					$buffer .= "\t".$info{"AF_$pop"}->[$aidx]."\t".$info{"AN_$pop"}->[0]."\t".$info{"nhomalt_$pop"}->[$aidx];
				}
				foreach my $controle (@controls) {
					# autosomes and generated values for Y
					my $af = $controle;
					$af =~ s/<tag>/AF/;
					my $an = $controle;
					$an =~ s/<tag>/AN/;
					my $nhomalt = $controle;
					$nhomalt =~ s/<tag>/nhomalt/;
					$buffer .= "\t".$info{$af}->[$aidx]."\t".$info{$an}->[0]."\t".$info{$nhomalt}->[$aidx];

				}

				$buffer .= "\n";
			}
		}
		if ($idx == 1000000) {
			$total += ($idx/1000000);
			print "Printed $total"."M variants for $in\n";
			open OUT, '|-',"gzip >> $out" or die("Could not open '|gzip >> $out' for writing/appending\n");
			print OUT $buffer;
			close OUT;
			$buffer = '';
			$idx = 0;
		}

	}
	# final set of variants.
	open OUT, '|-',"gzip >> $out" or die("Could not open '$out' for writing/appending\n");
	print OUT $buffer;
	close OUT;
	$buffer = '';
	$idx = 0;

}
