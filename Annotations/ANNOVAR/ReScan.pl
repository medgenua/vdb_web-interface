#!/usr/bin/perl
$|++;
#use warnings;
#use strict;
use Pod::Usage;
use Getopt::Long;
use File::Spec;
use Cwd;

## load credentials and db modules.
use Cwd 'abs_path';



our $VERSION = 			'$Revision: 506 $';
our $LAST_CHANGED_DATE =	'$LastChangedDate: 2012-05-25 01:57:38 -0700 (Fri, 25 May 2012) $';

our ($verbose, $help, $man,$validation,$timestamp);
our ($queryfile, $dbloc);
our ($outfile, $separate, $batchsize, $dbtype, $neargene, $genomebinsize, $geneanno, $regionanno, $filter, $downdb, $buildver, $score_threshold, $normscore_threshold, $minqueryfrac, $expandbin, $splicing_threshold,
	$maf_threshold, $chromosome, $zerostart, $rawscore, $memfree, $memtotal, $sift_threshold, $gff3dbfile, $genericdbfile, $vcfdbfile, $time, $wget, $precedence,
	$webfrom, $colsWanted, $comment, $scorecolumn, $transfun, $exonsort, $avcolumn, $bedfile, $hgvs, $reverse, $indexfilter_threshold, $otherinfo, $infoasscore);
our $seq_padding;   # If set, create a new file with cDNA, aa sequence padded by this much on either side.
our $indel_splicing_threshold; # If set, use this value for allowed indel size for splicing variants, else, use splicing_threshold;
our (%valichr, $dbtype1);
our (@precedence, @colsWanted, @avcolumn);
sub printerr;			#declare a subroutine
processArguments ();			#process program arguments, set up default values, check for errors, check for existence of db files




open (DROPPED, ">$outfile") or die "Error: cannot write to output file $outfile: $!\n";
open (QUERY, $queryfile) or die "Error: cannot read from query file $queryfile: $!\n";
my (%variant, $filedone, $batchdone, %rsids);
my ($linecount, $batchlinecount, $invalid, $invalidcount) = (0, 0);
my ($chr, $start, $end, $ref, $obs, $info,$vid);
while (1) {
	$_ = <QUERY>;
	if (not defined $_) {
		$filedone++;
	} else {
		s/[\r\n]+$//;

		if (m/^#/) {				#comment line start with #, do not include this is $linecount
			#print FIL "$_\n";
			#print DROPPED "#comment\t#comment\t$_\n";
			next;
		}
		
		$linecount++;
		$batchlinecount++;
		if ($batchlinecount == $batchsize) {
			$batchdone++;
		}
		$invalid = 0;	
		my @nextline = split (/\s+/, $_);
		($rsid,$chr, $start, $end, $ref, $obs,$vid) = @nextline[@avcolumn];
		($ref, $obs) = (uc $ref, uc $obs);
		$chr =~ s/^chr//;
		if ($start == $end and $ref eq '-') {	#insertion
			$obs = "0$obs";
		} elsif ($obs eq '-') {			#deletion
			$obs = $end-$start+1;
		} elsif ($end>$start or $start==$end and length($obs)>1) {	#block substitution	#fixed the bug here 2011feb19
			$obs = ($end-$start+1) . $obs;
		}
		
		## update, store variants by chr, start and variantID
		if (exists $variant{$chr, $start, $rsid} && $variant{$chr, $start, $rsid} != $vid) {
			$variant{$chr, $start, $rsid} .= ",$vid";
			$rsids{$rsid} .=",$vid|$chr|$start";
		} else {
			$variant{$chr, $start, $rsid} = "$vid";
			$rsids{$rsid} ="$vid|$chr|$start";
		}
		# below == storing above? effect of double entries? TODO
		if (exists $rsids{$rsid} && $rsids{$rsid} ne "$vid|$chr|$start") {
			$rsids{$rsid} .=",$vid|$chr|$start";
		} else {
			$rsids{$rsid} ="$vid|$chr|$start";
		}


	}
	if ($filedone or $batchdone) {
		printerr "NOTICE: Processing next batch with ${\(scalar keys %variant)} unique variants in $batchlinecount input lines\n";
		ProcessNextBatch (\%variant,\%rsids);
		%variant = ();
		%rsids = ();
		$batchlinecount = 0;				#reset the line count for this batch
		$batchdone = 0;
	}
	if ($filedone) {
		last;
	}

}

close(DROPPED);


sub ProcessNextBatch {
	my ($variant,$rsids) = @_;
	my $dbfile;
	my %seen;
	if ($dbtype1 eq 'generic') {
		$dbfile = File::Spec->catfile ($dbloc, $genericdbfile);
	} elsif ($dbtype1 eq 'vcf') {
		$dbfile = File::Spec->catfile ($dbloc, $vcfdbfile);
	} else {
		$dbfile = File::Spec->catfile ($dbloc, "${buildver}_$dbtype1.txt");
	}
	my (@record, $chr, $start, $end,$vid, $ref, $obs, $score, @otherinfo, $qual, $fil, $info);		#@otherinfo: other information about the variant in addition to score
	my ($rsid, $strand, $ucscallele, $twoallele, $class, $af, $attribute);
	my $count_invalid_dbline;

	my ($BIN, $DBSIZE) = (0, 0);
	my %index = ();
	my $bb = {};			#a subset of %index, which corresponds to the input variants
	my $flag_idx_search = 0;	#indicate if index-based search algorithm is used (faster speed for a small number of input variants
	if ( -f "$dbfile.idx" ) {
		open(IDX, "$dbfile.idx") or die "Error: cannot read from input database index $dbfile.idx: $!\n";
		my $line = <IDX>;
		if (not $line =~ m/BIN\t(\d+)\t(\d+)/) {
			printerr "WARNING: Malformed database index file $dbfile.idx.\n";
		} elsif ($2 != -s $dbfile) {		#file version is different, do not use index file in this case
			printerr "WARNING: Your index file $dbfile.idx is out of date and will not be used. ANNOVAR can still generate correct results without index file.\n";
		} else {
			($BIN, $DBSIZE) = ($1, $2);
			while ( $line = <IDX> ) {
				$line =~ s/[\r\n]+$//;
				my ( $chrom, $pos, $offset0, $offset1 ) = split (/\t/, $line);
				$chrom =~ s/^chr//;		#delete the chr in snp135, etc 
				defined $offset1 or next;				#invalid input line in the index file
				#$index{"$chrom\t$pos"} = [$offset0, $offset1];
				$index{"$chrom|$pos"} = "$offset0|$offset1";
				
			}
		}
		close (IDX);
		if (not %index) {
			printerr "WARNING: Unable to load database index successfully from file $dbfile.idx.\n";
		} else {
			#foreach my $k ( keys %$variant ) {
			foreach my $rsid (keys(%$rsids)) {
				## rsid can be multiple ,-seperated.
				foreach my $rs (split(/,/,$rsids->{$rsid})) {
					#my ($vid,$chrom,$pos) = split(/\|/,$rsids->{$rsid});
					my ($vid,$chrom,$pos) = split(/\|/,$rs);
					#my ($chrom, $pos) = split ($;, $k);
					my $bin = $pos - ( $pos % $BIN );
					defined $index{"$chrom|$bin"} or next;
					$bb->{ "$chrom|$bin" }{$rsid} = $rs; 
					# for borderline cases A) bin border: missed due to zero/one based index : decrease pos by one 
					my $abin = $pos - 1 - ( ($pos-1) % $BIN );
					defined $index{"$chrom|$abin"} or next;
					if ($abin != $bin) {
						$bb->{ "$chrom|$abin" }{$rsid} = $rs; 
					}
					# for borderline cases B) near upper bin border: missed due to left/right alignment : increase pos by 50 (arbitrary) 
					my $bbin = $pos + 50 - ( ($pos+50) % $BIN );
					defined $index{"$chrom|$bbin"} or next;
					if ($bbin != $bin) {
						$bb->{ "$chrom|$bbin" }{$rsid} = $rs; 
					}

				}
			}
			if (scalar (keys %$bb) / scalar (keys %index) < $indexfilter_threshold) {
				$flag_idx_search++;
				printerr "NOTICE: Database index loaded ($dbfile.idx). Total number of bins is ".  scalar (keys %index) . " and " . "the number of bins to be scanned is " . scalar (keys %$bb) . "\n";
			}
		}
	}
	if (not $flag_idx_search) {
		$bb = {1, [0, -s "$dbfile"]};
		%index = (1, [0, -s "$dbfile"]);
	}
	open (DB, $dbfile) or die "Error: cannot read from input database file $dbfile: $!\n";
	printerr "NOTICE: Scanning filter database $dbfile...";
	## set updatequery:
	my $sth;
	# scan database.
	foreach my $be ( sort keys %$bb ) {
		my ( $chunk_min, $chunk_max ) = split(/\|/,$index{$be} );
		seek( DB, $chunk_min, 0 );
		my $chunk_here = $chunk_min;
		while (<DB>) {
			my $line_length = length($_);
			s/[\r\n]+$//;
			m/\S/ or next;
			m/^#/ and next;
			@record = split (/\t/, $_, -1);
			@record == 18 or @record == 26 or die "Error: invalid record found in dbSNP database file $dbfile (18 or 26 fields expected but found ${\(scalar @record)}): <$_>\n" . join("\n",@record);
			$record[1] =~ s/^chr// or die "Error: invalid record found in DB file (2nd field should start with 'chr'): <$_>\n";
			#($chr, $start, $end, $rsid, $strand, $ucscallele, $twoallele, $class) = @record[1,2,3,4,6,8,9,11];
			($chr,$start,$rsid) = @record[1,2,4];
			
			$start++; # zero indexed
			## variant in hash?
			#if ($variant->{$chr,$start,$rsid}) {
			if ($rsids->{$rsid}) {
				my @vids = split(/,/,$rsids->{$rsid});
				foreach(@vids) {
				   my ($vid,$chr,$start) = split(/\|/,$_);
				   if (@record == 26 ) { # only continue if needed cols are present 
					## values to annotate.
					my $nchr = 0;
					my $clin = 0;
					my $maf = 0;
					## frequencies: 
					my @fr = split(/,/,$record[23]);
					@fr = sort { $a <=> $b } @fr;
					## get sum
					$nchr += $_ foreach @fr;
					$nchr = int($nchr);
					## get maf
					if ($nchr > 0) {
						$rmaf = $fr[0] / $nchr; 
						$maf = sprintf("%.5f",(($fr[0] / $nchr) + 0.0000001));
					}
					## check clinical
					if ($record[-1] =~ m/clinically/) {
						$clin = 1;
					}	
					# format taken by main LoadVariants.py script : 
                    # dummy annoA,annoB,annoC   <stuff> vid
					print  DROPPED ".\t$rsid,$maf,$nchr,$clin\t.\t$vid\n";	
					$seen{$rsid} = 1;

				   }
				}
				
					
			}
			
			$chunk_here += $line_length;
			if ( $chunk_here > $chunk_max ) {
				last;
			}

		}# end while <db>
	} # end $bb
	#$sth->finish();
	## missing variants? 
	foreach my $rsid (keys(%$rsids)) {
		if (!defined($seen{$rsid})) {
			my @vids = split(/,/,$rsids->{$rsid});
			foreach(@vids) {
				my ($vid,$chr,$start) = split(/\|/,$_);
				print  DROPPED ".\t$rsid,0,0,0\t.\t$vid\n";
			}
		}
	}
}
print "UPDATE Complete\n";

## 
exit();


sub processArguments {
	my @command_line = @ARGV;		#command line argument
	GetOptions('verbose|v'=>\$verbose, 'help|h'=>\$help, 'man|m'=>\$man, 'outfile=s'=>\$outfile, 'separate'=>\$separate,
	'batchsize=s'=>\$batchsize, 'dbtype=s'=>\$dbtype, 'neargene=i'=>\$neargene, 'genomebinsize=s'=>\$genomebinsize,
	'geneanno'=>\$geneanno, 'regionanno'=>\$regionanno, , 'filter'=>\$filter, 'downdb'=>\$downdb, 'buildver=s'=>\$buildver, 'score_threshold=f'=>\$score_threshold, 
	'normscore_threshold=i'=>\$normscore_threshold,	'minqueryfrac=f'=>\$minqueryfrac, 'expandbin=i'=>\$expandbin, 'splicing_threshold=i'=>\$splicing_threshold,
	'maf_threshold=f'=>\$maf_threshold, 'chromosome=s'=>\$chromosome, 'zerostart'=>\$zerostart, 'rawscore'=>\$rawscore, 'memfree=i'=>\$memfree, 
	'memtotal=i'=>\$memtotal, 'sift_threshold=f'=>\$sift_threshold, 'gff3dbfile=s'=>\$gff3dbfile, 'genericdbfile=s'=>\$genericdbfile, 'vcfdbfile=s'=>\$vcfdbfile,
	'time'=>\$time, 'wget!'=>\$wget, 'precedence=s'=>\$precedence, 'webfrom=s'=>\$webfrom, 'colsWanted=s'=>\$colsWanted, 'comment'=>\$comment,
	'scorecolumn=i'=>\$scorecolumn, 'transcript_function'=>\$transfun, 'exonsort'=>\$exonsort, 'avcolumn=s'=>\$avcolumn, 'bedfile=s'=>\$bedfile,
	'hgvs'=>\$hgvs, 'reverse'=>\$reverse, 'indexfilter_threshold=f'=>\$indexfilter_threshold, 'otherinfo'=>\$otherinfo, 
	'seq_padding=i'=>\$seq_padding, 'indel_splicing_threshold=i'=>\$indel_splicing_threshold, 'infoasscore'=>\$infoasscore, 'validation|V=i'=>\$validation,'timestamp|t=s'=>\$timestamp) or pod2usage ();
	
	## I run filter by default
	$filter = 1;
	
	#$help and pod2usage (-verbose=>1, -exitval=>1, -output=>\*STDOUT);
	#$man and pod2usage (-verbose=>2, -exitval=>1, -output=>\*STDOUT);
	@ARGV or pod2usage (-verbose=>0, -exitval=>1, -output=>\*STDOUT);
	@ARGV == 2 or pod2usage ("Syntax error");
	# $queryfile = "Output_Files/$dbtype.hg19_$dbtype"."_dropped";
	($queryfile, $dbloc) = @ARGV;
	$dbloc =~ s/[\\\/]$//;			#delete the trailing / or \ sign as part of the directory name
	if (defined $batchsize) {
		$batchsize =~ s/k$/000/;
		$batchsize =~ s/m$/000000/;
		$batchsize =~ m/^\d+$/ or pod2usage ("Error: the --batchsize argument must be a positive integer (suffix of k or m is okay)");
	} else {
		$batchsize = 5_000_000;
	}
	if (defined $genomebinsize) {
		$genomebinsize =~ s/k$/000/;
		$genomebinsize =~ s/m$/000000/;
		$genomebinsize =~ m/^\d+$/ or pod2usage ("Error: the --genomebinsize argument must be a positive integer (suffix of k or m is okay)");
		$genomebinsize > 1000 or pod2suage ("Error: the --genomebinsize argument must be larger than 1000");
	} else {
		if ($geneanno) {
			$genomebinsize = 100_000;		#gene usually span large genomic regions
		} else {
			$genomebinsize = 10_000;		#MCE, TFBS, miRNA, etc are small genomic regions
		}
	}

	$verbose ||= 0;			#when it is not specified, it is zero
	$neargene ||= 1_000;		#for upstream/downstream annotation of variants, specify the distance threshold between variants and genes
	$expandbin ||= int(2_000_000/$genomebinsize);		#for gene-based annotations, when intergenic variants are found, expand to specified number of nearby bins to find closest genes
	$outfile ||= $queryfile;	#specify the prefix of output file names
	#set up log file
	open (LOG, ">$outfile.extra.log") or die "Error: cannot write LOG information to log file $outfile.extra.log: $!\n";
	#print LOG "ANNOVAR Version:\n\t", q/$LastChangedDate: 2012-05-25 01:57:38 -0700 (Fri, 25 May 2012) $/, "\n";
	#print LOG "ANNOVAR Information:\n\tFor questions, comments, documentation, bug reports and program update, please visit http://www.openbioinformatics.org/annovar/\n";
	#print LOG "ANNOVAR Command:\n\t$0 @command_line\n";
	print LOG "Extra Annotation Started:\n\t", scalar (localtime), "\n";
	
	my $num = 0;
	#$geneanno and $num++;
	#$downdb and $num++;
	$filter and $num++;
	#$regionanno and $num++;
	$num <= 1 or pod2usage ("Error in argument: please specify only one of --geneanno, -regionanno, --downdb, --filter");
		
	my %dbtype1 = ('gene'=>'refGene', 'refgene'=>'refGene', 'knowngene'=>'knownGene', 'ensgene'=>'ensGene', 'band'=>'cytoBand', 'cytoband'=>'cytoBand', 'tfbs'=>'tfbsConsSites', 'mirna'=>'wgRna',
			'mirnatarget'=>'targetScanS', 'segdup'=>'genomicSuperDups', 'omimgene'=>'omimGene', 'gwascatalog'=>'gwasCatalog', 
			'1000g_ceu'=>'CEU.sites.2009_04', '1000g_yri'=>'YRI.sites.2009_04', '1000g_jptchb'=>'JPTCHB.sites.2009_04', 
			'1000g2010_ceu'=>'CEU.sites.2010_03', '1000g2010_yri'=>'YRI.sites.2010_03', '1000g2010_jptchb'=>'JPTCHB.sites.2010_03',
			'1000g2010jul_ceu'=>'CEU.sites.2010_07', '1000g2010jul_yri'=>'YRI.sites.2010_07', '1000g2010jul_jptchb'=>'JPTCHB.sites.2010_07',
			'1000g2010nov_all'=>'ALL.sites.2010_11', '1000g2011may_all'=>'ALL.sites.2011_05'
			);
	
	defined $dbtype or pod2usage ("Error in argument: please specify --dbtype (required for the --filter operation)");
	#as of Feb 2012, I no longer check the validity of the database name for -filter operation, to give users the maximum amount of flexibility in designing and using their own favorite databases
	#$dbtype =~ m/^avsift|generic|1000g_(ceu|yri|jptchb)|1000g2010_(ceu|yri|jptchb)|1000g20\d\d[a-z]{3}_[a-z]+|snp\d+|vcf|(ljb_[\w\+]+)$/ or pod2usage ("Error in argument: the specified --dbtype $dbtype is not valid for --filter operation (valid ones are '1000g_ceu', '1000g2010_yri', 'snp129', 'avsift', 'vcf', 'generic', etc)");
	$dbtype =~ m/^avsift|generic|1000g_(ceu|yri|jptchb)|1000g2010_(ceu|yri|jptchb)|1000g20\d\d[a-z]{3}_[a-z]+|snp\d+|vcf|(ljb_[\w\+]+)|esp5400_[\w]+$/ or print STDERR "NOTICE: the --dbtype $dbtype is assumed to be in generic ANNOVAR database format\n";
	
	$dbtype1 = $dbtype1{$dbtype} || $dbtype;
	
	if ($dbtype1 =~ m/^1000g(20\d\d)([a-z]{3})_([a-z]+)$/) {
		my %monthhash = ('jan'=>'01', 'feb'=>'02', 'mar'=>'03', 'apr'=>'04', 'may'=>'05', 'jun'=>'06', 'jul'=>'07', 'aug'=>'08', 'sep'=>'09', 'oct'=>'10', 'nov'=>'11', 'dec'=>'12');
		$dbtype1 = uc ($3) . '.sites.' . $1 . '_' . $monthhash{$2};
	}
	
	if ($dbtype1 eq 'generic') {
		defined $genericdbfile or pod2usage ("Error in argument: please specify --genericdbfile for the --dbtype of 'generic'");
	}
	if ($dbtype eq 'vcf') {
		defined $vcfdbfile or pod2usage ("Error in argument: please specify --vcfdbfile for the --dbtype of 'vcf'");
	}
	 	
	if (not $buildver) {
		$buildver = 'hg19';
		printerr "NOTICE: The --buildver is set as 'hg19' by default\n";
	}
	
	
	if (defined $indexfilter_threshold) {
		$filter or pod2usage ("Error in argument: the --indexfilter_threshold is supported only for the --filter operation");
		$indexfilter_threshold >= 0 and $indexfilter_threshold <= 1 or pod2usage ("Error in argument: the --indexfilter_threshold must be between 0 and 1 inclusive");
	} else {
		$indexfilter_threshold = 0.9;
	}
	
    	#if (defined $maf_threshold) {
	#	$filter or pod2usage ("Error in argument: the --maf_threshold is supported only for the --filter operation");
	#} else {
	#	$maf_threshold = 0;		#for filter-based annotations on 1000 Genomes Project data, specify the MAF threshold to be used in filtering
	#}
	#if (defined $minqueryfrac) {
	#	$regionanno or pod2usage ("Error in argument: the --minqueryfrac is supported only for the --regionanno operation");
	#} else {
	#	$minqueryfrac = 0;		#minimum query overlap to declare a "match" with database records
	#}
	if (defined $genericdbfile) {
		$filter or pod2usage ("Error in argument: the --genericdbfile argument is supported only for the --filter operation");
	}
		
		
	#if (defined $avcolumn) {
	#	$avcolumn =~ m/^\d+,\d+,\d+,\d+,\d+$/ or pod2usage ("Error in argument: the --avcolumn argument must be five integer numbers separated by comma");
	#	@avcolumn = split (/,/, $avcolumn);
	#	@avcolumn = map {$_-1} @avcolumn;
	#} else {
		@avcolumn = (1..6);		#by default, the first five columns are the required AVINPUT information, two columns are added in first ANNOVAR ROUND, so increased from (0..2) to (2..6) and 1 (equals the rsID)
		push(@avcolumn,"-1");  # variantID
	#}
	
	
	$maf_threshold >= 0 and $maf_threshold <= 0.5 or pod2usage ("Error in argument: the --maf_threshold must be between 0 and 0.5 (you specified $maf_threshold)");
	$minqueryfrac >= 0 and $minqueryfrac <= 1 or pod2usage ("Error in argument: the --minqueryfrac must be between 0 and 1 (you specified $minqueryfrac)");
	$memfree and $memfree >= 100_000 || pod2usage ("Error in argument: the --memfree argument must be at least 100000 (in the order of kilobytes)");
	$memtotal and $memtotal >= 100_000 || pod2usage ("Error in argument: the --memtotal argument must be at least 100000 (in the order of kilobytes)");
	
	#if ($chromosome) {
	#	my @chr = split (/,/, $chromosome);
	#	for my $i (0 .. @chr-1) {
	#		if ($chr[$i] =~ m/^(\d+)-(\d+)$/) {
	#			for my $j ($1 .. $2) {
	#				$valichr{$j}++;
	#			}
	#		} else {
	#			$valichr{$chr[$i]}++;
	#		}
	#	}
	#	printerr "NOTICE: These chromosomes in database will be examined: ", join (",", sort keys %valichr), "\n";
	#}
}


sub printerr {
	print STDERR @_;
	print LOG @_;
}


