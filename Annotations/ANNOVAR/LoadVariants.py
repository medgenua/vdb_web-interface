from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, AutoVivification, CheckVariableType
import configparser
import argparse
import os
import subprocess
import sys
from itertools import chain
import shelve

# add the cgi-bin/Modules to path.
import pathlib

file_path = pathlib.Path(__file__).parent.resolve()
sys.path.append(os.path.join(file_path, "../../cgi-bin/Modules"))
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


# some variables/objects
re = Re()  # customized regex-handler


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile(os.path.join(file_path, "../../../.Credentials/.credentials")):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read(os.path.join(file_path, "../../../.Credentials/.credentials"))

    # command line arguments
    help_text = """
        GOAL:
        #####
            - Run Annovar on the all NEW variants & store to DB
            - Run Annovar on all Variants & store to dedicated validation table in DB
            - Run Annovar on a VCF file & store to new VCF file.
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # annotation type
    parser.add_argument("-a", "--anno", required=True, help="Annotation Type")
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # overrule log level
    parser.add_argument(
        "-l", "--loglevel", required=False, help="Set the log level : ERROR, WARN, [INFO], DEBUG"
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Run in Validation Mode. All variants are annotated, and stored in a separate table.",
    )
    # file mode
    parser.add_argument(
        "-f", "--infile", required=False, help="Input VCF file to use in file-based mode."
    )
    parser.add_argument(
        "-o", "--outfile", required=False, help="Output VCF file to use in file-based mode."
    )
    # custom data folder
    parser.add_argument(
        "-p",
        "--prefix",
        required=False,
        default="",
        help="Use a custom Prefix for the humandb/ folder",
    )
    args = parser.parse_args()
    # if loglevel set : overrule value in config.
    if args.loglevel:
        config["LOGGING"]["LOG_LEVEL"] = args.loglevel
    # if validation, add the prefix.
    if args.validate and not args.prefix:
        args.prefix = "Validation_"
    # else:
    #    args.prefix = ""
    return config, args


def GetNames(annotator):
    anno = annotator.args.anno
    annotator.args.anno_provided = anno
    table = anno
    fields = list()
    re = Re()
    # returns annotation and corresponding table.
    if re.isearch(r"refgene|refseq", anno):
        anno = "refGene"
        table = "refgene"
    elif re.isearch(r"ncbi", anno):
        anno = "ncbiGene"
        table = "ncbigene"
    elif re.isearch(r"ucsc", anno) or re.isearch(r"knowngene", anno):
        anno = "knownGene"
        table = "knowngene"
    elif re.isearch(r"ensgene|Ensembl", anno):
        anno = "ensGene"
        table = "ensgene"
    elif re.isearch(r"cadd", anno):
        anno = anno.lower()
        table = anno
    elif re.isearch(r"snp(\d+)", anno):
        # annotation has avsnp\d ; table does not have 'av' in the name
        table = "snp{}".format(re.last_match.group(1))
    elif re.isearch(r"gnomad|mitimpact", anno):
        # gnomad and mitimpact : remove dots in version
        table = anno.lower().replace(".", "")
    elif re.isearch(r"ljb_", anno):
        anno = anno.lower()
    elif re.isearch(r"segdup", anno):
        anno = "genomicSuperDups"
    elif re.isearch(r"revel", anno):
        anno = anno.lower()
        table = anno
    # no longer supported:
    #  polyphen2 ; muttast ;  siv2
    # assign to annotator:
    # full table :
    annotator.args.table_name = "Variants_x_ANNOVAR_{}".format(table)
    # annotation file
    annotator.args.annotation_file = "{}_{}.txt".format(args.build, anno)

    annotator.args.anno = anno

    # get field/column names:
    annotator.fieldnames = annotator.dbh.getColumns(annotator.args.table_name)["names"]
    # strip aid
    d = annotator.fieldnames.pop(0)
    # for genes, strip the new protein sequence.
    if "gene" in table.lower():
        d = annotator.fieldnames.pop()


# Validate the provided options.
def ValidateTables(config):
    # the table if not file based

    # annotation file:
    if not os.path.isfile(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/ANNOVAR/{}humandb".format(annotator.args.prefix),
            annotator.args.annotation_file,
        )
    ):
        raise DataError(
            "ANNOVAR Data table missing : {}humandb/{}".format(
                annotator.args.prefix, annotator.args.annotation_file
            )
        )
    # special annotation tables.
    if re.isearch(r"(ncbi|ref|ucsc|ens|known)Gene", annotator.args.annotation_file):
        # fasta:
        fasta = annotator.args.annotation_file.replace("Gene.txt", "GeneMrna.fa")
        if not os.path.isfile(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/ANNOVAR/{}humandb".format(annotator.args.prefix),
                fasta,
            )
        ):
            raise DataError("Fasta File missing : {}".fasta)
        annotator.args.fasta = fasta
        # link ?
        if re.isearch(r"ncbiGene", annotator.args.annotation_file):
            reflink = annotator.args.annotation_file.replace("ncbiGene", "refLink_ncbigene")
            if not os.path.isfile(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/ANNOVAR/{}humandb".format(annotator.args.prefix),
                    reflink,
                )
            ):
                raise DataError("reflink File missing : {}".format(reflink))
            annotator.args.reflink = reflink
        if re.isearch(r"refGene", annotator.args.annotation_file):
            reflink = annotator.args.annotation_file.replace("refGene", "refLink")
            if not os.path.isfile(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/ANNOVAR/{}humandb".format(annotator.args.prefix),
                    reflink,
                )
            ):
                raise DataError("reflink File missing : {}".reflink)
            annotator.args.reflink = reflink
        if re.isearch(r"knownGene", annotator.args.annotation_file):
            # crossref =
            log.debug("Checking presence of hg19_kgXref.txt")

    return True


def VcfToAnnovar(vcf):
    # convert to annovar format.
    log.info("Converting input variants to annovar TXT format")
    annovar_infile = vcf.replace(".vcf", ".txt")
    # ugly as hell, but open to suggestions to detect failures in pipes....
    cmd = "bash -o pipefail -c \"perl {}convert2annovar.pl --format vcf4 --includeinfo {} | cut -f 1-5,13 | sed 's/\\tid=/\\t/' \"".format(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/"), vcf
    )
    try:
        subprocess.run(cmd, shell=True, check=True, stdout=open(annovar_infile, "w"))
    except Exception as e:
        raise e

    # return file location
    return annovar_infile


def RunAnnovar(annovar_infile):
    # variables:
    annovar_out_base = annovar_infile.replace("Input_Files", "Output_Files").replace(
        ".list.txt", ""
    )
    #########################
    ## MAIN ANNOTATION RUN ##
    #########################
    # gene based tables
    if re.isearch(r"gene$", annotator.args.anno):
        cmd = "perl {}annotate_variation.pl --separate --nofirstcodondel --memtotal 4000000 --buildver '{}' --dbtype {} --outfile {} {} '{}humandb/'".format(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/"),
            annotator.args.build,
            annotator.args.anno,
            annovar_out_base,
            annovar_infile,
            annotator.args.prefix,
        )
    # region based
    elif re.isearch("genomicSuperDups", annotator.args.anno):
        cmd = "perl {}annotate_variation.pl --regionanno --memtotal 4000000 --buildver '{}' --dbtype {}  --outfile {} {} '{}humandb/'".format(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/"),
            annotator.args.build,
            annotator.args.anno_provided,
            annovar_out_base,
            annovar_infile,
            annotator.args.prefix,
        )
    # all others : filter-based
    else:
        cmd = "perl {}annotate_variation.pl --filter --memtotal 4000000 --buildver '{}' --dbtype {} --otherinfo --outfile {} {} '{}humandb/'".format(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/"),
            annotator.args.build,
            annotator.args.anno,
            annovar_out_base,
            annovar_infile,
            annotator.args.prefix,
        )
    # run.
    try:
        log.debug(cmd)
        subprocess.run(cmd, shell=True, check=True)
    except Exception as e:
        raise e
    ############################
    ## some follow-up commands.#
    ############################
    if re.isearch(r"gene$", annotator.args.anno):
        with open("{}.exonic_variant_function".format(annovar_out_base), "r") as f_in, open(
            "{}.exonic_variant_function.filtered".format(annovar_out_base), "w"
        ) as f_out:
            log.info("Preparing coding variants for coding_change analysis")
            for line in f_in:
                if re.search(r"unknown|c\.-", line):
                    continue
                p = line.rstrip().split("\t")
                isoforms = p[2].split(",")
                for iso in isoforms:
                    if iso == "":
                        continue
                    p[2] = iso
                    f_out.write("\t".join(str(x) for x in p) + "\n")
        log.info("Running coding change analysis")
        stdout = "{}.Coding.change.txt".format(annovar_out_base)
        stderr = "{}.Coding.change.errors.txt".format(annovar_out_base)
        cmd = "perl {}coding_change.pl {}.exonic_variant_function.filtered '{}humandb/{}' '{}humandb/{}'".format(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/"),
            annovar_out_base,
            annotator.args.prefix,
            annotator.args.annotation_file,
            annotator.args.prefix,
            annotator.args.fasta,
        )
        try:
            subprocess.run(
                cmd, shell=True, check=True, stdout=open(stdout, "w"), stderr=open(stderr, "w")
            )
        except Exception as e:
            raise e
    elif "exac03" in args.anno:
        log.info("Running exac 03 AN annotations.")
        cmd = [
            "perl",
            "{}annotate_variation.pl".format(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/")
            ),
            "--filter",
            "--memtotal",
            "4000000",
            "--buildver",
            annotator.args.build,
            "--dbtype",
            f"{annotator.args.anno}_AN",
            "--otherinfo",
            "--outfile",
            f"{annovar_out_base}",
            annovar_infile,
            f"{annotator.args.prefix}humandb/",
        ]
        try:
            stdout = "{}.AN.txt".format(annovar_out_base)
            stderr = "{}.AN.errors.txt".format(annovar_out_base)
            subprocess.run(cmd, check=True, stdout=open(stdout, "w"), stderr=open(stderr, "w"))
        except Exception as e:
            raise e
    elif re.isearch(r"snp\d+", args.anno):
        log.info("Running dbSNP MAF annotations.")
        cmd = [
            "perl",
            "{}ReScan.pl".format(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/")
            ),
            "--buildver",
            annotator.args.build,
            "--dbtype",
            annotator.args.anno.replace("av", ""),
            "--outfile",
            "{}.{}_{}_frequencies".format(
                annovar_out_base, annotator.args.build, annotator.args.anno
            ),
            "{}.{}_{}_dropped".format(annovar_out_base, annotator.args.build, annotator.args.anno),
            f"{annotator.args.prefix}humandb/",
        ]
        try:
            stdout = "{}.frequencies.stdout".format(annovar_out_base)
            stderr = "{}.frequencies.stderr".format(annovar_out_base)
            subprocess.run(cmd, check=True, stdout=open(stdout, "w"), stderr=open(stderr, "w"))
        except Exception as e:
            raise e
    return True


def StoreResults(infile):
    # gene based tables
    if re.isearch(r"gene$", annotator.args.anno):
        try:
            inserts = StoreGeneAnnotations(infile)
        except Exception as e:
            raise e

    # region based tables
    elif re.isearch(r"genomicSuperDups", annotator.args.anno):
        try:
            inserts = StoreRegionAnnotations(infile)
        except Exception as e:
            raise e
    # plain filter based tables.
    else:
        # gnomad, dbscsnv11, cosmic, ...
        try:
            inserts = StoreFilterAnnotations(infile)
        except Exception as e:
            raise e
    # add to VCF ?
    if annotator.args.infile:
        l_idx = 0
        with open(annotator.args.infile, "r") as f_in, open(annotator.args.outfile, "w") as f_out:
            for line in f_in:
                # headers
                if line.startswith("#"):
                    f_out.write(line)
                    continue
                l_idx += 1
                # get columns.
                columns = line.rstrip().split("\t")
                if columns[2] == ".":
                    vid = f"line_{l_idx}"
                else:
                    vid = str(columns[2])
                # vid = str(l_idx)  # columns[2]
                columns[7] += ";ANNOVAR.{}=".format(args.anno)
                if vid not in inserts:
                    log.info("vid not found: {}".format(vid))
                # can be multiple entries per vid.
                for e in inserts[vid]:
                    # construct the entry by combining field names and values.
                    entry = ",".join(
                        [f"{k}:{v}" for k, v in zip(annotator.fieldnames, inserts[vid][e])]
                    )

                    if "{}" in entry:
                        log.warning("invalid entry: ")
                        log.warning(entry)
                    columns[7] += "{}|".format(entry)
                # others ?

                # strip end & write out.
                columns[7] = columns[7].rstrip("|")
                f_out.write("\t".join(columns) + "\n")
    return


# Region based : only matching are reported.
def StoreRegionAnnotations(infile):
    inserts = AutoVivification()
    annovar_out_base = infile.replace("Input_Files", "Output_Files").replace(".list.txt", "")
    f_out = open("{}.load.txt".format(annovar_out_base), "w")
    # tracker used in inserts
    t = 0
    # found annotations:
    vids = set()
    with open(
        "{}.{}".format(annovar_out_base, annotator.args.annotation_file.replace(".txt", "")), "r"
    ) as f_in:
        for line in f_in:
            t += 1
            # anno_name, value, <variant info>
            c = line.rstrip().split("\t")
            if "genomicSuperDups" in annotator.args.anno:
                score, position = re.ifindall(r"Score=([\d\.]+);Name=(.*)", c[1])[0]
                v = [float(score), position]
            # if needed : put different parsers here.

            # check types.
            v = annotator.CheckTypes(v)
            # write vid, followed by all freq cols.
            if not annotator.args.infile:
                f_out.write("{}\t{}\n".format(c[-1], "\t".join(str(x) for x in v)))
            else:
                # reflect structure in StoreGenes : [vid][transcript] = info
                inserts[c[-1]][t] = [c[-1]] + v
            # track vid
            vids.add(c[-1])
    # load to database.
    f_out.close()
    if not annotator.args.infile:
        annotator.LoadDataTable(f"{annovar_out_base}.load.txt")

    # then go over infile to detect non-annotated variants.
    f_out = open("{}.load.missing.txt".format(annovar_out_base), "w")
    with open(infile, "r") as f_in:
        for line in f_in:
            t += 1
            c = line.rstrip().split("\t")
            if c[1] not in vids:
                # write vid, others are set to DB-default values.
                if not annotator.args.infile:
                    f_out.write("{}\n".format(c[-1]))
                else:
                    # vid + defaults for all other fields.
                    inserts[c[-1]][t] = [c[-1]] + annotator.table_info["defaults"][1:]
    f_out.close()
    if not annotator.args.infile:
        annotator.LoadDataTable("{}.load.missing.txt".format(annovar_out_base), field_names=["vid"])
    # add output files to list for cleanup
    for suffix in [
        "log",
        f"{annotator.args.annotation_file}",
        "load.txt",
        "load.missing.txt",
    ]:
        annotator.files.append(f"{annovar_out_base}.{suffix}")
    # return inserts structure
    return inserts


# composite annotations : split & store
def StoreFilterAnnotations(infile):
    # prepare
    annovar_suffix = "dropped"
    inserts = AutoVivification()
    annovar_out_base = infile.replace("Input_Files", "Output_Files").replace(".list.txt", "")
    f_out = open("{}.load.txt".format(annovar_out_base), "w")
    sfh = None
    # tracker used in inserts
    t = 0

    # anno specific requirements.
    if "cosmic" in annotator.args.anno:
        annotator.GetCode("tissue")
    if re.isearch(r"snp\d+", annotator.args.anno):
        annovar_suffix = "frequencies"
        log.info("Using frequencies suffix!")
    if "exac03" in annotator.args.anno:
        sf = "{}.{}_AN_shelf".format(
            annovar_out_base, annotator.args.annotation_file.replace(".txt", "")
        )
        for suffix in ["bak", "dat", "dir"]:
            annotator.files.append(f"{sf}.{suffix}")
        sfh = shelve.open(sf)
        with open(
            "{}.{}_AN_dropped".format(
                annovar_out_base, annotator.args.annotation_file.replace(".txt", "")
            ),
            "r",
        ) as fh:
            for line in fh:
                c = line.rstrip().split("\t")
                sfh[c[-1]] = c[1]

    # dropped (present)
    log.info("Loading dropped")
    with open(
        "{}.{}_{}".format(
            annovar_out_base, annotator.args.annotation_file.replace(".txt", ""), annovar_suffix
        ),
        "r",
    ) as f_in:
        for line in f_in:
            t += 1
            c = line.rstrip().split("\t")
            # allow for multiple entries on a single output line.
            entries = list()
            # anno specific parsing.
            if "cosmic" in annotator.args.anno:
                id, occs = c[1].split(";")
                id = id[3:]
                for occ in occs[10:].split(","):
                    # nr and tissue
                    nr, tissue = re.ifindall(r"(\d+)\((.*)\)", occ)[0]
                    tissue = annotator.GetCode("tissue", tissue)
                    # correct/reset invalid column types
                    v = [id, tissue, int(nr)]
                    entries.append(annotator.CheckTypes(v))
            # general filter based annotations:
            else:
                v = c[1].split(",")
                # interleave shelved data?
                if "exac03" in annotator.args.anno:
                    an_v = sfh[c[-1]].split(",")
                    v = list(chain(*zip(v, an_v)))
                    # replace missing by the default value.
                    v = [x if not x == "." else -1 for x in v]
                # add & replace . by numeric defaults for missing values.
                entries.append(annotator.CheckTypes(v, silent=False))

            for v in entries:
                # write vid, followed by all freq cols.
                if not annotator.args.infile:
                    f_out.write("{}\t{}\n".format(c[-1], "\t".join(str(x) for x in v)))
                else:
                    # reflect structure in StoreGenes : [vid][transcript] = info
                    inserts[c[-1]][t] = [c[-1]] + v
    f_out.close()
    if sfh:
        sfh.close()

    # load
    if not annotator.args.infile:
        annotator.LoadDataTable(f"{annovar_out_base}.load.txt")

    # filtered (not present)
    f_out = open("{}.load.missing.txt".format(annovar_out_base), "w")
    with open(
        "{}.{}_filtered".format(
            annovar_out_base, annotator.args.annotation_file.replace(".txt", "")
        ),
        "r",
    ) as f_in:
        for line in f_in:
            t += 1
            c = line.rstrip().split("\t")
            # write vid, others are set to DB-default values.
            if not annotator.args.infile:
                f_out.write("{}\n".format(c[-1]))
            else:
                # vid + defaults for all other fields.
                inserts[c[-1]][t] = [c[-1]] + annotator.table_info["defaults"][1:]
    f_out.close()
    # load
    if not annotator.args.infile:
        annotator.LoadDataTable(f"{annovar_out_base}.load.missing.txt", field_names=["vid"])

    # add output files to list for cleanup
    for suffix in [
        "log",
        f"{annotator.args.annotation_file}_filtered",
        f"{annotator.args.annotation_file}_dropped",
        "load.txt",
        "load.missing.txt",
    ]:
        annotator.files.append(f"{annovar_out_base}.{suffix}")
    # return inserts structure
    return inserts


# MOST COMPLEX ROUTINE : process & refine protein effect annotations
# STEP 1 : <base>.variant_function :
#               => extract info for non_genic , UTR and splice variants
#               => add to DB
# STEP 2 : <base>.exonic_variant_function :
#               => extract info for coding variants, add to DB & track db-id for step 3
#               => store non-coding variants (intronic)
# STEP 3 : <base>.Coding.change.txt :
#               => refine Exonic effects for indels


def StoreGeneAnnotations(infile):
    # are transcript versions available ?
    nm_version_file = "{}humandb/{}_{}Version.txt".format(
        annotator.args.prefix, annotator.args.build, annotator.args.anno
    )
    log.info("looking for version file: {}".format(nm_version_file))
    versions = dict()
    if os.path.isfile(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "Annotations", annotator.method, nm_version_file
        )
    ):
        with open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"], "Annotations", annotator.method, nm_version_file
            ),
            "r",
        ) as f_in:
            for line in f_in:
                tr, v = line.rstrip().split("\t")
                versions[tr] = v
    else:
        log.info("Version file not found.")

    # load codings from DB.
    for column in ["GeneLocation", "VariantType"]:
        annotator.GetCode(column)

    # basename
    annovar_out_base = infile.replace("Input_Files", "Output_Files").replace(".list.txt", "")

    # tracking variables
    lines = AutoVivification()
    var_hash = dict()  # dictionary of lists.
    inserts = AutoVivification()
    to_delete = dict()
    to_pop = dict()
    # STEP 1 : all variants, basic info : extract for noncoding + splicing + UTR
    with open("{}.variant_function".format(annovar_out_base), "r") as f_in, open(
        "{}.variant_function.load.txt".format(annovar_out_base), "w"
    ) as f_out:
        for line in f_in:
            p = line.strip().split("\t")
            # if two locations ; column 1 AND 2 are separated by ;  eg : upstream;downstread \t geneA(dist=4);geneB(dist=500)
            log.debug("GLOCS: {}".format(p[0]))
            log.debug("SYMBOLS: {}".format(p[1]))
            glocs = p[0].split(";")
            symbols = p[1].split(";")
            vid = p[-1]
            # reason of this dict : if all entries of a var are intronic/splicing :
            #    => fully handled here, can be discarded.
            if vid not in to_delete:
                to_delete[vid] = list()
                to_pop[vid] = list()

            # go over different genomic locations : eg exonic for gene 1, upstream for gene 2
            for idx in range(len(glocs)):
                gloc = glocs[idx]
                symbol = symbols[idx]
                loc_code = annotator.GetCode("GeneLocation", gloc)
                # initialize to gene_location (intronic, exonic, splicing, UTR, ...)
                type_code = annotator.GetCode("VariantType", gloc)
                # add to tracking hash.
                if not var_hash.get(vid):
                    var_hash[vid] = list()
                var_hash[vid].append({"loc": loc_code, "symbol": symbol, "line": p[0]})
                i = len(var_hash[vid]) - 1
                if gloc == "intergenic":
                    log.debug(f"Intergenic variant : stripping symbols '{symbol}' to ''")
                    var_hash[vid][i]["symbol"] = ""
                # up/down stream variants
                if gloc in [
                    "upstream",
                    "downstream",
                    "intergenic",
                    "intronic",
                    "ncRNA_intronic",
                ]:
                    for gs in symbol.split(","):
                        log.debug(f"investigating {vid} : {gs} in {gloc}")
                        # main case : only one symbol present, this one has 'symbol(dist=\d+)' format
                        if re.findall(r"(.*)\((dist=.*)\)", gs):
                            gs = re.last_match[0][0]
                            dist = re.last_match[0][1]
                        # remaining genes don't have distance info : PTGES3L,PTGES3L-AARSD1,RUNDC1(dist=1)
                        else:
                            dist = "."

                        # track in case of vcf handling <transcript>.<type>.<gene|cpoint>
                        if annotator.args.infile:
                            inserts[vid]["none.{}.{}".format(type_code, gs)] = [
                                vid,
                                gs,
                                "",
                                0,
                                loc_code,
                                type_code,
                                dist,
                                ".",
                                0,
                                "",
                            ]

                        else:
                            values = [vid, gs, "", 0, loc_code, type_code, dist, "."]
                            f_out.write("\t".join(str(x) for x in values) + "\n")
                    # delete item from var_hash
                    to_delete[vid].append(True)
                    # only remove this entry from var_hash (if both intronic and exonic variants are present)
                    to_pop[vid].append(i)
                    continue
                # splice variants and UTR variants are coded here, not in exonic.
                # structure : GENE(NM:exon:cNT,NM:exon:cNT)
                # or intronic/invalid splicing : GENE without the (info)
                elif gloc in [
                    "splicing",
                    "ncRNA_splicing",
                    "ncRNA_exonic",
                ] or gloc.startswith("UTR"):
                    # the look behind operator : split on  '),'   but keep the ')'
                    genes = re.split(r"(?<=\)),", symbol)
                    for gs in genes:
                        # extract info
                        log.debug("Looking for info in symbol : {}".format(gs))
                        if re.search(r"(.*)\((.*)\)", gs):
                            log.debug("found transcript info: {}".format(gs))
                            var_hash[vid][i]["symbol"] = re.last_match.group(1)
                            # multiple transcript per gene
                            for transcript in re.last_match.group(2).split(","):
                                # replace empty values by '.'
                                items = [x if x != "" else "." for x in transcript.split(":")]
                                # add version
                                if items[0] in versions:
                                    tr = "{}.{}".format(items[0], versions[items[0]])
                                else:
                                    tr = items[0]

                                # UTR : 2 entries : NM:cPointNT
                                if len(items) == 2:
                                    exon = 0
                                    cp = items[1]
                                # splicing : 3 items : NM:exon_NR:cPointNT
                                else:
                                    exon = items[1].replace("exon", "")
                                    # TODO : optimize cPoint_NT notations?
                                    cp = items[2]

                                # track in case of vcf handling
                                if annotator.args.infile:
                                    # hack to have double effects on a single transcript : annotation problem? varaints affects 5' and 3' splice site.
                                    if len(inserts[vid]["{}.{}.{}".format(tr, type_code, cp)]) > 0:
                                        log.warning(
                                            "Double effect on single transcript. Please investigate vid : {}".format(
                                                vid
                                            )
                                        )
                                        log.warning(
                                            inserts[vid]["{}.{}.{}".format(tr, type_code, cp)]
                                        )
                                        log.warning("overwriting first effect...")
                                        inserts[vid]["{}.{}.{}".format(tr, type_code, cp)] = [
                                            vid,
                                            var_hash[vid][i]["symbol"],
                                            tr,
                                            exon,
                                            loc_code,
                                            type_code,
                                            cp,
                                            ".",
                                            0,
                                            "",
                                        ]

                                    else:
                                        inserts[vid]["{}.{}.{}".format(tr, type_code, cp)] = [
                                            vid,
                                            var_hash[vid][i]["symbol"],
                                            tr,
                                            exon,
                                            loc_code,
                                            type_code,
                                            cp,
                                            ".",
                                            0,
                                            "",
                                        ]
                                else:
                                    values = [
                                        vid,
                                        var_hash[vid][i]["symbol"],
                                        tr,
                                        exon,
                                        loc_code,
                                        type_code,
                                        cp,
                                        ".",
                                    ]
                                    f_out.write("\t".join(str(x) for x in values) + "\n")
                        else:
                            # intronic        GHSR    chr3    172165396       172165396       G       T       2
                            # invalid splice entry (looks like default for all dels > 1bp):
                            # splicing        ZG16B   chr16   2880411 2880433 CACAGAGCCCTGGGATGCACCGG -       1873
                            # ncRNA_exonic    PRR34   chr22   46449963        46449963        G       -       8448
                            log.debug("no transcript info for {}".format(gs))
                            if annotator.args.infile:
                                # keep one entry per type:
                                inserts[vid]["none.{}.{}".format(type_code, gs)] = [
                                    vid,
                                    gs,
                                    "",
                                    0,
                                    loc_code,
                                    type_code,
                                    ".",
                                    ".",
                                    0,
                                    "",
                                ]

                            else:
                                values = [vid, gs, "", 0, loc_code, type_code, ".", "."]
                                f_out.write("\t".join(str(x) for x in values) + "\n")
                    # delete item from dict.
                    to_delete[vid].append(True)
                    to_pop[vid].append(i)
                    # continue
                else:
                    # variants with effect on CDS.
                    to_delete[vid].append(False)

    # clean var_hash if ALL are set to "ok to delete"
    for vid in to_delete:
        if all(to_delete[vid]):
            var_hash.pop(vid, None)
        elif len(to_pop[vid]):
            var_hash[vid] = PopList(var_hash[vid], to_pop[vid])

    to_delete = dict()
    to_pop = dict()

    # basic info done : load nongenic + splicing + UTR to DB.
    try:
        if not annotator.args.infile:
            log.info("Loading base info to database")

            annotator.LoadDataTable(
                f"{annovar_out_base}.variant_function.load.txt",
                field_names=[
                    "vid",
                    "GeneSymbol",
                    "Transcript",
                    "Exon",
                    "GeneLocation",
                    "VariantType",
                    "CPointNT",
                    "CPointAA",
                ],
            )
    except Exception as e:
        raise e

    # STEP 2 : continue with exonic variants :
    #    structure : Gene:NM:exon:cPointNT:cPointAA,Gene:NM:....
    log.info("Refining exonic variants")
    with open("{}.exonic_variant_function".format(annovar_out_base), "r") as f_in:
        for line in f_in:
            p = line.rstrip().split("\t")
            type_code = annotator.GetCode("VariantType", p[1])
            log.debug("Variant Type for vid {} : {}".format(p[1], p[-1]))
            # checks for variant info.
            vid = p[-1]
            if vid not in to_delete:
                to_delete[vid] = list()

            if vid not in var_hash:
                # notify to admin, but continue analysis.
                log.error("Variant {} not found in var_hash.".format(vid))
                continue
            if len(var_hash[vid]) > 1:
                # notify to admin, but continue to analysis: theoritically possible, length refers to different effects in different genes.
                #  but all non-codings are already removed fromt he hash.
                log.error("multiple entries in hash for {}. Using first".format(vid))
            # take the first entry in var_hash.
            vkidx = 0

            # tr
            transcripts = p[2].rstrip(",").split(",")
            for transcript in transcripts:
                # symbol, NM_ID, exon, CpointNT, CpointAA (force existence)
                items = transcript.split(":")
                # add missing values
                for i in range(5):
                    if len(items) == i:
                        items.append(".")
                    elif items[i] == "":
                        items[i] = "."
                # affected exon idx
                items[2] = "0" if items[2] == "." else items[2].replace("exon", "")
                # if transcript version info taken from a separate file, add it here.
                #   => ncbiGene has it in the base data.
                if items[1] in versions:
                    tr = "{}.{}".format(items[1], versions[items[1]])
                else:
                    tr = items[1]
                # missannotation of insertions : not relevant anymore
                # if ($items[3] =~ m/c\.(\d+)_(\d+)([ACTG]+)/) {
                #    $items[3] = "c.$1"."_$2"."ins$3";
                # }

                # symbol issues
                if var_hash[vid][vkidx]["symbol"] == "":
                    log.error("No symbol for vid {}".format(vid))
                # overrule symbol in general with exonic annotations.
                if var_hash[vid][vkidx]["symbol"] != items[0] and items[0] != "UNKNOWN":
                    var_hash[vid][vkidx]["symbol"] = items[0]

                # track variant by line number+transcript.
                lines[p[0]]["v"] = vid
                lines[p[0]][items[1]] = {
                    "e": p[1],
                    "p": items[4],
                    "cp": items[3],
                    "type": type_code,
                }
                # track in memory (vcf input)
                if annotator.args.infile:
                    inserts[vid]["{}.{}.{}".format(items[1], type_code, items[3])] = [
                        vid,
                        var_hash[vid][vkidx]["symbol"],
                        tr,
                        items[2],
                        var_hash[vid][vkidx]["loc"],
                        type_code,
                        items[3],
                        items[4],
                        0,
                        "",
                    ]
                # or add to DB & track db-id.
                else:
                    values = [
                        vid,
                        var_hash[vid][vkidx]["symbol"],
                        tr,
                        items[2],
                        var_hash[vid][vkidx]["loc"],
                        type_code,
                        items[3],
                        items[4],
                    ]
                    query = "INSERT INTO `{}{}` (vid, GeneSymbol, Transcript, Exon, GeneLocation, VariantType, CPointNT,CPointAA) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)".format(
                        annotator.args.prefix, annotator.args.table_name
                    )
                    # insert
                    aid = annotator.dbh.insertQuery(
                        query,
                        values,
                    )
                    lines[p[0]][items[1]]["a"] = aid

            # delete variant
            to_delete[vid].append(True)
            # var_hash.pop(vid, None)

    # clean var_hash if ALL are set to "ok to delete"
    for vid in to_delete:
        # currently, no false conditions defined.
        if all(to_delete[vid]):
            var_hash.pop(vid, None)
    to_delete = dict()

    # then store remaining variants : these are non-coding
    log.info("Finalize non-coding variants")
    type_code = annotator.GetCode("VariantType", "")
    with open("{}.non_coding".format(annovar_out_base), "w") as f_out:
        for vid in var_hash:
            if vid == "":
                continue
            if len(var_hash[vid]) > 1:
                # notify to admin, but continue to analysis.
                log.error("multiple entries in hash for {}. Using first".format(vid))
            vkidx = 0  # list(var_hash[vid].keys())[0]
            for gs in var_hash[vid][vkidx]["symbol"].split(","):
                if not annotator.args.infile:
                    f_out.write(
                        "\t".join(
                            str(x)
                            for x in [
                                vid,
                                gs,
                                "",
                                0,
                                var_hash[vid][vkidx]["loc"],
                                type_code,
                                "",
                                "",
                            ]
                        )
                        + "\n"
                    )
                else:
                    # i = len(inserts[vid]["none"])
                    inserts[vid]["none.{}.{}".format(type_code, gs)] = [
                        vid,
                        gs,
                        "",
                        0,
                        var_hash[vid][vkidx]["loc"],
                        type_code,
                        "",
                        "",
                        0,
                        0,
                        "",
                    ]
    if not annotator.args.infile:
        try:
            annotator.LoadDataTable(
                f"{annovar_out_base}.non_coding",
                field_names=[
                    "vid",
                    "GeneSymbol",
                    "Transcript",
                    "Exon",
                    "GeneLocation",
                    "VariantType",
                    "CPointNT",
                    "CPointAA",
                ],
            )
        except Exception as e:
            log.error("Failed to load non coding variants: {}".format(e))
            sys.exit(1)
    var_hash = None

    # STEP 3 : finally, analyse effect on protein (mainly indels)
    log.info("Refining indels")
    with open("{}.Coding.change.txt".format(annovar_out_base), "r") as f_in:
        # tracking variables
        prevwthead = ""
        prevwtseq = ""
        prevmutseq = ""
        prevmuthead = ""
        storing = ""
        for line in f_in:
            line = line.rstrip()
            # it is a fasta file, scan for new entries by '^>'
            if line.startswith(">"):
                # process / update tracked data.
                (prevwthead, prevmuthead, prevwtseq, prevmutseq, storing, inserts,) = ProcessInDel(
                    prevwthead,
                    prevmuthead,
                    prevwtseq,
                    prevmutseq,
                    storing,
                    line,
                    lines,
                    inserts,
                )
                continue
            elif storing == "wt":
                prevwtseq += line
            elif storing == "mut":
                prevmutseq += line
            else:
                log.error(
                    "Coding Change Fasta is invalid: {}.Coding.change.txt".format(annovar_out_base)
                )
                raise
    # store last entry:
    prevwthead, prevmuthead, prevwtseq, prevmutseq, storing, inserts = ProcessInDel(
        prevwthead, prevmuthead, prevwtseq, prevmutseq, storing, None, lines, inserts
    )

    # process errors to check for large indels spanning last exon.
    if os.path.isfile("{}.Coding.change.errors.txt".format(annovar_out_base)):
        log.info("There were coding errors: parsing them now.")
        with open("{}.Coding.change.errors.txt".format(annovar_out_base), "r") as f_in:
            for line in f_in:
                if re.search(
                    r"ERROR in (line\d+): end position of variant \(\d+\) in (\S+) is longer",
                    line.rstrip(),
                ):
                    line_nr = re.last_match.group(1)
                    nm = re.last_match.group(2)
                    aid = lines[line_nr][nm]["a"]
                    vid = lines[line_nr]["v"]
                    eff = lines[line_nr][nm]["e"]
                    cpoint = lines[line_nr][nm]["cp"]
                    o_type_code = lines[line_nr][nm]["type"]
                    if eff != "frameshift substitution":
                        continue
                    new_effect = "stoploss"
                    type_code = annotator.GetCode("VariantType", new_effect)
                    if annotator.args.infile:
                        inserts[vid]["{}.{}.{}".format(nm, o_type_code, cpoint)][5] = type_code
                    else:
                        annotator.dbh.doQuery(
                            "UPDATE `{}{}` SET VariantType = %s WHERE vid = %s AND aid = %s".format(
                                annotator.args.prefix, annotator.args.table_name
                            ),
                            [type_code, vid, aid],
                        )
    # track created files:
    for suffix in [
        "variant_function",
        "exonic_variant_function",
        "log",
        "exonic_variant_function.filtered",
        "Coding.change.errors.txt",
        "Coding.change.txt",
        "variant_function.load.txt",
        "non_coding",
    ]:
        annotator.files.append(f"{annovar_out_base}.{suffix}")

    # done : return the inserts (used if annotator.args.infile)
    return inserts


# annovar doesn't completely follow HGVS guidelines. we optimize some here.
def ProcessInDel(prevwthead, prevmuthead, prevwtseq, prevmutseq, storing, line, lines, inserts):
    # error in reference genome?
    if prevwtseq == "*" and prevmutseq == "*":
        log.error("Abnormal sequence detected : {}".format(prevwthead))
        # reset
        prevwthead = ""
        prevmuthead = ""
        prevwtseq = ""
        prevmutseq = ""

    # if both wt & mut are available => process & save.
    elif prevwthead != "" and prevmuthead != "":
        # difference in length :
        delta = len(prevmutseq) - len(prevwtseq)
        # variables taken from the fasta header.
        f_nm = ""
        f_cpoint = ""
        f_peffect = ""
        f_descr = ""
        f_ppoint = ""
        # annovar v 2015 version : no ppoint in fasta header.
        # >line180 NM_001290208.3 c.623dupC protein-altering  (position 210 changed from Q to S)
        if re.search(r">line\d+\s(\S+)\s(\S+)\s(\S+)\s\s(.*)", prevmuthead):
            f_nm = re.last_match.group(1)
            f_cpoint = re.last_match.group(2)
            f_peffect = re.last_match.group(3)
            f_descr = re.last_match.group(4)
        # regular frameshift : >line1 NM_00.1 c.1467_168insCTCC p.G57fs*125 protein-altering  (....)
        elif re.search(r">line\d+\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s\s(.*)", prevmuthead):
            f_nm = re.last_match.group(1)
            f_cpoint = re.last_match.group(2)
            f_ppoint = re.last_match.group(3)
            f_peffect = re.last_match.group(4)
            f_descr = re.last_match.group(5)
        # start loss (no description)
        elif re.search(r">line\d+\s(\S+)\s(\S+)\s(\S+)\s(\S+)", prevmuthead):
            f_nm = re.last_match.group(1)
            f_cpoint = re.last_match.group(2)
            f_ppoint = re.last_match.group(3)
            f_peffect = re.last_match.group(4)
        # immediate stop-gain: >line1496 NM_001354636.2 c.704_1555del p.M99* immediate-stopgain
        elif re.search(r">line\d+\s(\S+)\s(\S+)\s(\S+)\s(\S+)", prevmuthead):
            f_nm = re.last_match.group(1)
            f_cpoint = re.last_match.group(2)
            f_ppoint = re.last_match.group(3)
            f_peffect = re.last_match.group(4)
        # no ppoint : >line1496 NM_001354636.2 c.704_1555del  effect
        #  => not found in 1.4M entries : legacy
        # elif re.search(r">line\d+\s(\S+)\s(\S+)\s(\S+)", prevmuthead):
        #    f_nm = re.last_match.group(1)
        #    f_cpoint = re.last_match.group(2)
        #    f_ppoint = re.last_match.group(3)
        #    f_peffect = re.last_match.group(4)

        # if anything else : report
        else:
            log.error("Unexpected protein effect : {}".format(prevmuthead))

        # correct indel notation (legacy format, probably not needed anymore)
        if re.search(r"c\.(\d+)_(\d+)([ACTG]+)", f_cpoint):
            # notify admin.
            log.error("Incorrect indel notation : {}".format(prevmuthead))
            f_cpoint = "c.{}_{}ins{}".format(
                re.last_match.group(1), re.last_match.group(2), re.last_match.group(3)
            )

        # extract info from descr.
        f_to_AA = ""
        # f_pos = ""
        # f_from = ""
        # description empty
        if f_descr == "":
            if prevwtseq == prevmutseq:
                f_to_AA = "*"
            elif f_peffect == "startloss":
                f_to_AA = "?"
            else:
                log.error("Unknown protein situation: {}".format(prevmuthead))

        # description says : single AA change.
        elif re.search(r"\(position ([\d-]+) changed from ([A-Z\*]+) to ([A-Z\*]*)\)", f_descr):
            # descr_pos = re.last_match.group(1)
            # descr_from = re.last_match.group(2)
            f_to_AA = re.last_match.group(3)
        # description says : insertion on position
        elif re.search(r"\(position ([\d-]+) has insertion ([A-Z\*]+)\)", f_descr):
            f_to_AA = re.last_match.group(2)
        # line1496 NM_001354635.2 c.815_817del p.*272* silent  (no amino acid change)
        elif f_peffect == "silent" and re.search(r"p\.[X\*]\d[X\*]", f_ppoint):
            f_to_AA = "*"

        # get variantID & others.
        re.search(r"^>(line\d+)\s.*", prevwthead)
        line_nr = re.last_match.group(1)
        if f_nm not in lines[line_nr]:
            log.error("NM not found for line {} : {} : {}".format(line_nr, f_nm, prevwthead))
            log.error(lines[line_nr])
        try:
            aid = lines[line_nr][f_nm]["a"]
        # in file-based operation, there is no annotation-id.
        except Exception as e:
            pass
        vid = lines[line_nr]["v"]

        # variables taken from variant hash
        v_eff = lines[line_nr][f_nm]["e"]
        v_ppoint = lines[line_nr][f_nm]["p"]

        ###############################################
        # optimize some stopgain/frameshift notations #
        ###############################################
        new_effect = f_peffect
        new_ppoint = f_ppoint

        # SNV
        #####
        # only (wrongly annotated?) SNVS in the start codon get here.
        # p.L1L silent
        # p.R1M protein-altering
        if re.search(r"p.([A-Z])1([A-Z])", f_ppoint):
            # p.L1L
            if re.last_match.group(1) == re.last_match.group(2):
                # c.1dupA / c.1_2insA
                if "dup" in f_cpoint or "del" in f_cpoint or "ins" in f_cpoint:
                    new_effect = "synonymous substitution"
                else:
                    new_effect = "synonymous SNV"
            # p.M1L
            elif re.last_match.group(1) == "M":
                new_effect = "startloss"
            # p.R1M
            else:
                new_effect = "nonsynonymous SNV"

        # DELETIONS
        ###########
        # p.Y436_T949del => to p.Y436*
        elif f_peffect == "immediate-stopgain" and re.search(
            r"p.([A-Z]\d+)_[A-Z]\d+del$", f_ppoint
        ):
            new_ppoint = "p.{}*".format(re.last_match.group(1))
            new_effect = "stopgain"
        # fs-del with immediate stop
        # >line195 NM_001278298.2 c.7770delT protein-altering  (position 2591 changed from L to *)
        # p.P161_R210delinsRK to p.P161_R210delinsRK*
        elif f_peffect == "immediate-stopgain" and re.search(
            r"p.[A-Z]\d+_[A-Z]\d+delins", f_ppoint
        ):
            new_ppoint = f_ppoint + "*"
            new_effect = "stopgain"
        # single AA deletion (second to last position):
        #   => hote: some cases are also handled in ScanSeq. These are annotated as protein-altering. reason unknown.
        elif f_peffect == "immediate-stopgain" and re.search(r"p.([A-Z]\d+)del", f_ppoint):
            new_ppoint = "p.{}*".format(re.last_match.group(1))
            new_effect = "stopgain"
        # p.T35delinsTAAAA*  : the T is double and should be removed (legacy?)
        elif f_peffect == "immediate-stopgain" and re.search(
            r"p.([A-Z])(\d+)delins([A-Z])([A-Z]*)", f_ppoint
        ):
            log.error("Incorrect delins detected : {}".format(prevmuthead))
            next_aa = prevwtseq[re.last_match.group(2)]
            next_pos = re.last_match.group(2) + 1
            new_ppoint = "p.{}{}_{}{}ins{}".format(
                re.last_match.group(1),
                re.last_match.group(2),
                next_aa,
                next_pos,
                re.last_match.group(4),
            )
            new_effect = "stopgain"
        # non-fs deletion
        elif f_peffect == "protein-altering" and v_eff == "nonframeshift deletion":
            new_effect = v_eff
            try:
                new_ppoint = ScanSeq("nfs", prevwtseq, prevmutseq)
            except ValueError as e:
                log.warning(f"{e} : {prevmuthead}")
            except Exception as e:
                log.error(f"{e} : {prevmuthead}")
            # one of the scanseq outcomes is stopgain.
            if new_ppoint.endswith("*"):
                new_effect = "stopgain"
            # special case 3' end fully replaced by delins, ScanSeq can give wrong result for partial hits.
            elif (
                "delins" in new_ppoint
                and re.search(r"p.[A-Z]\d+_[A-Z](\d+)delins[A-Z]*", f_ppoint)
                and re.last_match.group(1) == str(len(prevwtseq.rstrip("*")))
            ):
                # then keep the f_ppoint notations.
                new_ppoint = f_ppoint

        # frameshift deletion :
        # >line7 NM_001304388.1 c.2080delA p.R694Efs*284 protein-altering (position ... )
        elif f_peffect == "protein-altering" and v_eff == "frameshift deletion":
            new_effect = v_eff

        # INSERTIONS
        ############
        # frameshift insertion with inserted stop codon.
        elif (
            f_peffect == "immediate-stopgain"
            and re.search(r"[A-Z]+\*", f_to_AA)
            and re.search(r"^c\.(\d+)_(\d+)ins([ACGT]+)", f_cpoint)
        ):
            inserted = re.last_match.group(3)
            if len(inserted) % 3:
                new_effect = "frameshift insertion"
            else:
                new_effect = "nonframeshift insertion"
        # non-fs insertion : duplication should be noted correctely.
        elif f_peffect == "protein-altering" and v_eff == "nonframeshift insertion":
            new_effect = v_eff
            new_ppoint = ScanSeq("nfs", prevwtseq, prevmutseq)

            # might also be a stopgain : inserted sequence ends with *
            if re.search(r"p.([A-Z])(\d+)_([A-Z])(\d+)delins([A-Z]+\*)", new_ppoint):
                log.info("nfs ins stopgain")
                new_effect = "stopgain"
                # ppoint is ok.

        # frameshift insertion
        #  ppoint is ok, just change effect : p.F362Vfs*91
        elif f_peffect == "protein-altering" and v_eff == "frameshift insertion":
            new_effect = v_eff

        # SILENT
        # ######
        #   silent : * => *
        elif f_peffect == "silent":
            # most of the cases : annotated as fs: p.X127delinsX
            if re.search(r"delins|fs", v_ppoint):
                new_effect = "synonymous substitution"
                # update X => *
                if re.search(r"p\.X(\d+)delinsX", v_ppoint):
                    new_ppoint = "p.*{}*".format(re.last_match.group(1))

                # Some p.*500fs are p.500* : fs without effect on seq
                elif re.search(r"p\.[A-Z\*](\d+)fs", v_ppoint) and int(re.last_match.group(1)) > 1:
                    # log.error("Unexpected silent variant : {}".format(prevmutseq))
                    new_ppoint = "p.*{}*".format(len(prevwtseq))
                # duplication of first A in start codon
                if v_ppoint == "p.M1fs" and f_cpoint == "c.1dupA":
                    new_ppoint = "p.M1M"
            # rare cases : immediately seen as silent, annotated correctly at p level
            elif re.search(r"frameshift", v_eff):
                new_effect = "synonymous substitution"
                # but then annotated as p.X100X instead of p.*100*
                new_ppoint = f_ppoint.replace("X", "*")
        elif f_peffect == "startloss":
            new_effect = f_peffect
            new_ppoint = "p.M1?"
        else:
            log.debug("no special case detected")

        # update DB / inserts
        type_code = annotator.GetCode("VariantType", new_effect)

        if annotator.args.infile:
            cp = lines[line_nr][f_nm]["cp"]
            o_type_code = lines[line_nr][f_nm]["type"]
            inserts[vid]["{}.{}.{}".format(f_nm, o_type_code, cp)][8] = delta
            inserts[vid]["{}.{}.{}".format(f_nm, o_type_code, cp)][9] = prevmutseq
            inserts[vid]["{}.{}.{}".format(f_nm, o_type_code, cp)][7] = new_ppoint
            inserts[vid]["{}.{}.{}".format(f_nm, o_type_code, cp)][5] = type_code
        else:
            log.debug("Update {} : {} => {}".format(vid, v_ppoint, new_ppoint))
            update_query = "UPDATE `{}{}` SET `ProteinLengthDifference` = %s , `NewProtein` = %s ,  CPointAA = %s, VariantType = %s WHERE vid = %s AND aid = %s".format(
                annotator.args.prefix, annotator.args.table_name
            )
            values = [delta, prevmutseq, new_ppoint, type_code, vid, aid]
            annotator.dbh.doQuery(update_query, values)

        # reset
        prevwthead = ""
        prevmuthead = ""
        prevwtseq = ""
        prevmutseq = ""
    # then update trackers
    if line:
        if re.search(r"WILDTYPE", line):
            prevwthead = line.rstrip()
            storing = "wt"
        else:
            prevmuthead = line
            storing = "mut"
    return prevwthead, prevmuthead, prevwtseq, prevmutseq, storing, inserts


def ScanSeq(type, wtseq, mutseq):

    # sporadically wtseq/mutseq does not have *
    if not wtseq.endswith("*"):
        wtseq = wtseq + "*"
    mutseq = mutseq.rstrip("*") + "*"
    p = "p."
    min_length = min(len(wtseq), len(mutseq))
    # non-frameshift
    if type == "nfs":
        first_pos = None
        # non-fs, so adjust mut-pos by diff in length
        # this is valid for del, ins and exact subs.
        mut_pos_offset = len(mutseq) - len(wtseq)
        # find first altered position
        for i in range(min_length):
            # identical (5' end, or after an initial mismatch)
            if wtseq[i] == mutseq[i]:
                continue
            # first mismatch, 1-based
            first_pos = i + 1
            # p += "{}{}".format(wtseq[i], pos)
            break

        # then continue checking sequences for first new match, using the offset
        log.debug(f"first_pos {first_pos} and offset {mut_pos_offset}:")
        # if deletion : scan wt untill it matches mut.
        if mut_pos_offset < 0:
            # first different position zero based on wt : first matching base after del on mut.
            mut_pos = first_pos - 1
            last_pos = False
            last_mut_pos = False
            max_match = 0
            # interstitial deletion
            while mut_pos < len(mutseq):
                # find a matching position in wt with next item in mut.
                for i in range(first_pos - 1, len(wtseq)):
                    log.debug(f"  - investigate {i} : {wtseq[i]} vs {mutseq[mut_pos]}")
                    if wtseq[i] == mutseq[mut_pos]:
                        # if end of del/sub : remainder of seq should be equal
                        # but : some annotation problems lead to mutseq > wtseq (missing stop codon in wt)
                        # compare wtseq to mutseq for same length, without (added) stop
                        remaining = wtseq[i:]
                        # only keep * if it's the only remaining character.
                        if not remaining == "*":
                            remaining = remaining.rstrip("*")
                        log.debug(f"remaining : {remaining}")
                        if mutseq[mut_pos:].startswith(remaining) and len(remaining) > max_match:
                            log.debug(f"  => match on position {i} with mutpos {mut_pos}")
                            # i is first matching zero-based == last mismatch 1 based
                            last_pos = i
                            last_mut_pos = mut_pos
                            max_match = len(remaining)
                            break

                # increment mut-pos
                # if not last_pos:
                mut_pos += 1
                # stop if remaining length < max_match
                if max_match > len(mutseq[mut_pos:]):
                    break
                # if mut_pos == len(mutseq):
                # break
            if not last_pos:
                raise ValueError("Failed to detect AA-change from SEQ. Please investigate.")
            log.debug(f"first pos {first_pos} ; last_pos {last_pos}")
            # single pos pure del
            if first_pos == last_pos:
                # special case : one-to-last deleted.
                if wtseq[first_pos] == "*":
                    p += "{}{}*".format(wtseq[first_pos - 1], first_pos)
                # general : one AA del
                else:
                    p += "{}{}del".format(wtseq[first_pos - 1], first_pos)
            # pure multi-AA deletion : first mismatch position on WT is first base after event on MUT
            elif first_pos - 1 == last_mut_pos:
                # special case : AA's before stop are deleted.
                if wtseq[last_pos:] == "*":
                    p += "{}{}*".format(wtseq[first_pos - 1], first_pos)
                else:
                    p += "{}{}_{}{}del".format(
                        wtseq[first_pos - 1], first_pos, wtseq[last_pos - 1], last_pos
                    )
            # delins : some values were inserted (less than deleted amount)
            else:
                p += "{}{}_{}{}delins{}".format(
                    # from AA
                    wtseq[first_pos - 1],
                    # from POS
                    first_pos,
                    # to AA
                    wtseq[last_pos - 1],
                    # to POS
                    last_pos,
                    # inserted MUT SEQ
                    mutseq[(first_pos - 1) : last_mut_pos],
                )
        # if insertion: scan mutseq untill match.
        elif mut_pos_offset > 0:
            # first different position zero based on mut : first matching base after ins on wt.
            wt_pos = first_pos - 1
            last_pos = False
            log.debug(f"starting with offset {mut_pos_offset} and first pos == {first_pos}")
            while not last_pos:
                # find a matching position in mut with next item in wt.
                for i in range(first_pos - 1, len(mutseq)):
                    log.debug(f"  - investigate {i} : {mutseq[i]} vs {wtseq[wt_pos]}")
                    if mutseq[i] == wtseq[wt_pos]:
                        # if end of ins/sub : remainder of seq should be equal
                        remaining = wtseq[wt_pos:].rstrip("*")
                        log.debug(f"remaining : {remaining}")
                        if mutseq[i:].startswith(remaining):
                            # i is first matching zero-based == last mismatch 1 based
                            log.debug(f"Found match at {i} with {remaining}")
                            last_pos = i
                            break
                # no matching substring found : increment mut-pos
                if not last_pos:
                    wt_pos += 1
                    if wt_pos == len(wtseq):
                        break
            if not last_pos:
                raise ValueError("Failed to detect AA-change from SEQ. Please investigate.")

            # pure insertion : first mismatch position on mut is first base after event on wt
            #    wt:HCRGEEEG => mut:HCRGEEEEEEG  with wt_pos == 8, first_pos == 9
            elif first_pos - 1 == wt_pos:

                log.debug(f"pure insertion between: {first_pos} and  {last_pos}")
                # general insertion : one before the altered base.
                p += "{}{}_{}{}ins{}".format(
                    # wtseq[first_pos - 1],
                    wtseq[wt_pos - 1],
                    # first_pos,
                    wt_pos,
                    # wtseq[last_pos - 1],
                    wtseq[wt_pos],
                    wt_pos + 1,
                    mutseq[(first_pos - 1) : last_pos],
                )
                # check for a duplication event:
                # ins is 3' aligned
                # wt   MTLLL ---KRS : (5-1)-(insLen - 1) to 5-1 : 5 - insLen to 5 -1 : 2-to-4 * 2 = LLL LLL
                # mut  MTLLL LLLKRS : (5-1)-(insLen - 1) to 5 - 1 + insLen : 2 to 7 : LLL LLL
                #  p.L5_K6insLLL
                #   => look before instead of after
                try:
                    re.search(r"p.([A-Z\*])(\d+)_([A-Z\*])(\d+)ins([A-Z\*]+)", p)
                    m = re.last_match
                    inserted_length = len(m.group(5))
                    log.debug(f"inserted length: {inserted_length}")
                    log.debug(
                        "on wt: "
                        + wtseq[max(0, (int(m.group(2)) - inserted_length)) : int(m.group(2))] * 2
                    )
                    log.debug(
                        "on mt: "
                        + mutseq[
                            max(0, (int(m.group(2)) - inserted_length)) : (
                                int(m.group(2)) + inserted_length
                            )
                        ]
                    )

                    # simplification : make a dup notation if insXXX is actually a tandem duplication
                    if (
                        wtseq[max(0, (int(m.group(2)) - inserted_length)) : int(m.group(2))] * 2
                        == mutseq[
                            max(0, (int(m.group(2)) - inserted_length)) : (
                                int(m.group(2)) + inserted_length
                            )
                        ]
                    ):
                        # single duplicated item : simplify notation.
                        if inserted_length == 1:
                            p = "p.{}{}dup".format(m.group(1), m.group(2))
                        # duplicated stretch.
                        else:
                            p = "p.{}{}_{}{}dup".format(
                                wtseq[max(0, (int(m.group(2)) - inserted_length))],
                                max(0, (int(m.group(2)) - inserted_length + 1)),
                                wtseq[int(m.group(2)) - 1],
                                m.group(2),
                            )
                except Exception as e:
                    log.warning(f"RegEx failed on {p} : {e}")
                    log.warning(type)
                    log.warning(wtseq)
                    log.warning(mutseq)
                    log.warning(f"Setting p to p.?")
                    p = "p.?"
            # rare case where WT is misannotated (too short), mut is longer : looks like ins, but is actually a del.
            # this was correct in exonic_variant_function, but is wrong in Coding_change.
            elif first_pos - 1 == last_pos:
                log.info(f"misannotated deletion from {first_pos} to {wt_pos}")
                # single AA del
                if first_pos == wt_pos:
                    p += "{}{}del".format(
                        # from AA
                        wtseq[first_pos - 1],
                        # from POS
                        first_pos,
                    )
                # multi AA del
                else:
                    p += "{}{}_{}{}del".format(
                        # from AA
                        wtseq[first_pos - 1],
                        # from POS
                        first_pos,
                        # to AA
                        wtseq[wt_pos - 1],
                        # to POS
                        wt_pos,
                    )
            # delins : some values were inserted (more than deleted amount)
            else:
                log.debug(f"first pos:{first_pos} ; last_pos: {last_pos} ; wt_pos : {wt_pos}")
                log.debug("inserted seq : " + mutseq[(first_pos - 1) : last_pos])
                # one AA replaced. C25delinsXYZ
                if first_pos == wt_pos:
                    p += "{}{}delins{}".format(
                        # from AA
                        wtseq[first_pos - 1],
                        # from POS
                        first_pos,
                        # inserted MUT SEQ
                        mutseq[(first_pos - 1) : last_pos],
                    )
                # stretch replaced. C25_C26delinsXYZ
                else:
                    p += "{}{}_{}{}delins{}".format(
                        # from AA
                        wtseq[first_pos - 1],
                        # from POS
                        first_pos,
                        # to AA
                        wtseq[wt_pos - 1],
                        # to POS
                        wt_pos,
                        # inserted MUT SEQ
                        mutseq[(first_pos - 1) : last_pos],
                    )

    return p


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    log = StartLogger(config, f"ANNOVAR--{args.anno}")

    # get the annotator object (annotatioin gets corrected later)
    annotator = Annotator(config=config, method="ANNOVAR", annotation="ncbiGene", args=args)

    # get table & file names based on annotation , updates the args in annotator.
    try:
        GetNames(annotator)
        # load the table structure (column names & types)
        annotator.GetTableInfo()

        log.debug(
            "Annotation : {} => Table: {}{} ; annotation (edit): {}".format(
                annotator.args.anno,
                annotator.args.prefix,
                annotator.args.table_name,
                annotator.args.annotation_file,
            )
        )
    except DataError as e:
        log.error("Could not initiate analysis: {}".format(e))
        sys.exit(1)

    # validate the tables & files.
    try:
        # general validation
        annotator.ValidateTable()
        # specific requirements
        ValidateTables(config)
    except DataError as e:
        log.error("Could not locate necessary resources: {}".format(e))
        sys.exit(1)

    # Get Input Variants
    log.info("Retrieving variants to process")

    if not annotator.args.infile:
        try:
            annotator.GetVariants()
        except Exception as e:
            log.error("Could not fetch input variants: {}".format(e))
            sys.exit(1)
    else:
        # cp to the tmp folder, adding line_idx as id in INFO, strip non-needed fields
        annotator.PrepareInputVCF()
        # convert to correct format (add the id field)

    annotator.files.append(annotator.vcf)

    # convert vcf to annovar input file.
    annovar_infile = VcfToAnnovar(annotator.vcf)
    annotator.files.append(annovar_infile)

    # no variants to process.
    if not os.stat(annovar_infile).st_size:
        log.info("No Variants to process after vcf2annovar conversion. Exiting.")
        annotator.cleanup()
        sys.exit(0)

    # run the annotation procedures
    try:
        RunAnnovar(annovar_infile)
    except Exception as e:
        log.error("ANNOVAR failed : {}".format(e))
        sys.exit(1)

    # Process & store results
    try:
        StoreResults(annovar_infile)
    except Exception as e:
        log.error("Failed to process ANNOVAR results: {}".format(e))
        sys.exit(1)

    # cleanup if not debug/not set loglevel
    if (
        not hasattr(args, "loglevel")
        or args.loglevel is None
        or not args.loglevel.lower() == "debug"
    ):
        annotator.cleanup()
