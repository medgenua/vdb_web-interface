#!/usr/bin/perl

##########
## GOAL ##
##########
##
## For each non-deprecated annotation type: 
##   - select at least 50 variants
##   - for categorical items (e.g. exonic/intronic, disease class,) make sure each category has five variants. 
##   - combine all variants into a VCF
##   - Annotate all variants for all annotations using VCF annotation functionality.
##   - Compare results 
## 	=> Missing == Critical
##	=> Difference == warning
##
##    => Run this script after avery ApplyUpdates to GUI, and after monthly updates. 


$|++;
use DBI;
use Getopt::Std;
use Data::Dumper;
use XML::Simple;
use Cwd qw/getcwd abs_path/;
use Scalar::Util qw(looks_like_number);
use File::Basename;
# for mailing
use MIME::Lite;
use Net::Domain qw/hostfqdn/;
use List::Util 'shuffle';

# opts
# b : genome build (if provided)
# a : annotate and compare only (do not refetch variants) ; needs -f
# c : compare only (do not fetch & annotate variants) ; needs -f
# f : use a prefetched variant file.

getopts('b:acf:', \%opts) ;

my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";





our @errors; # logging variable.

my $date = `date`;
print "Starting time: $date";
my $dir = getcwd;
my $log_date = $date;
$log_date =~ s/\s|:/_/g;
$log_date =~ s/_$//;
our $logfile = "job_output/Validate_Updates.$log_date.log";
$logfile = abs_path($logfile);
print " - Logging to : $logfile\n";
open LOG, ">$logfile" or die("Could not open logfile for writing: $logfile\n");
print LOG "Starting time: $date\n";
close LOG;

## the file with validation variants
my $var_file = abs_path("Update.Files/Variants.update.$log_date.in.vcf");
if ($opts{'a'} || $opts{'c'}) {
	if (!$opts{'f'} || ! -f $opts{'f'}) {
		print "No, or invalid, variant file provided. Use -f 'file' in combination with -a or -c\n";
		die();
	}
	$var_file = abs_path($opts{'f'});
}


$credpath = abs_path("../.LoadCredentials.pl");
require($credpath);
# include views routine
require("../cgi-bin/inc_views.pl");

## connect to DB
my $dbh = Get_DB_Connection();


## get build replacement values
my %builds = ();
my $rc = $dbh->selectall_arrayref("SELECT Annotation, Value FROM `Annotation_DataValues`");
foreach my $r (@$rc) {
	$builds{$r->[0]} = $r->[1];
}

####################################
## READ THE ANNOTATION CONFIG XML ##
####################################
my $xml = new XML::Simple;
# this xml file contains all the parameter/method settings.
my $settings = $xml->XMLin("Config.xml");
my @subdirs = keys(%{$settings});

if ($opts{'a'} || $opts{'c'}) {
	print "Skipping variant fetching\n";
	goto ANNOTATE;
}
###################################################
## PASS 1: get needed  variants from the tables. ##
###################################################
Logger("Obtaining testing variants\n");
my %allvars = ();
my %tables_done = ();
## get all vids to extract random sets.
my $all_vids = $dbh->selectcol_arrayref("SELECT id FROM `Variants`");
$dbh->disconnect();
#=cut
foreach my $subdir (@subdirs) {
	$subdir =~ s/^_//;
	# skip webtools
	if ($subdir eq 'WebTools') {
		next;
	}
	# second level (eg all under ANNOVAR)
	foreach my $anno (keys(%{$settings->{$subdir}})) {
			
		if (exists($settings->{$subdir}->{$anno}->{deprecated})) {
			next;
		}
		if (!defined($settings->{$subdir}->{$anno}->{AnnotationSource})) {
			#print "No Annotation Source available for $subdir/$anno. Skipping\n";
			next;
		}
		# anntation source (e.g. RefGene)
		my $annosource = $settings->{$subdir}->{$anno}->{AnnotationSource};
		if ($annosource eq '%build%') {
			$annosource = $builds{$subdir};
		}
		if ($annosource eq 'none') {
			next;
		}
		## table & column
		my $table;
		my $abbr;
		my @tables = split(/,/,$settings->{$subdir}->{$anno}->{from});
		foreach my $t (@tables) {
			my ($tt, $a) = split(/ /,$t);
			if ($tt =~ m/^Variants_x_/) {
				$table = $tt;
				$abbr = $a;
				last;
			}
		}
		my ($column,$as) = split(/ AS /,$settings->{$subdir}->{$anno}->{select});
		$column =~ s/^$abbr\.//;
		$column =~ s/`$table`\.//;
		# complex query, table not replaced?
		if ($column =~ m/`\./) {
			Logger("  - skipping $anno : $column\n");
			next;
		}
		my %lvars = ();
		# use local db-connection to prevent timeout/disconnections
		my $dbh = Get_DB_Connection();
		my @shuffled = shuffle(@$all_vids);
		## categorical (in Coded_Values) => min 5/category
		if (defined($settings->{$subdir}->{$anno}->{coded}) && $settings->{$subdir}->{$anno}->{coded} == 1) {
			# get values.
			my %codes = ();
			my $vc = $dbh->selectall_arrayref("SELECT id,`Item_Value` FROM `Value_Codes` WHERE `Table_x_Column` = '$table"."_$column'");
			foreach my $r (@$vc) {
				$codes{$r->[0]} = ();
				Logger("  - $table : $column : $r->[0]\n");
				# first use already fetched variants (other tables)
				if (keys(%allvars) > 0) {
					my $ic = $dbh->selectcol_arrayref("SELECT vid FROM `$table` WHERE vid IN (".join(',',keys(%allvars)).") AND `$column` = '$r->[0]'");
					foreach my $vid (@$ic) {
						$lvars{$vid} = 1;
						$allvars{$vid} = 1;
						$codes{$r->[0]}{$vid} = 1;
						# end this category.
						if (keys(%{$codes{$r->[0]}}) >= 5) {
							last;
						}
					}
				}
				if (keys(%{$codes{$r->[0]}}) < 5) {
				# fill with new random variables : see : https://stackoverflow.com/a/41581041/1633756
					# get all
					my $l_vids = $dbh->selectcol_arrayref("SELECT vid FROM `$table` WHERE `$column` = '$r->[0]'");
					my @l_shuffled = shuffle(@$l_vids);
					#while ($iter < 25 && scalar(@l_shuffled) > 0) {
					for (my $li = 0; $li < scalar(@l_shuffled); $li++) {
						my $vid = $l_shuffled[$li];
						$lvars{$vid} = 1;
						$allvars{$vid} = 1;
						$codes{$r->[0]}{$vid} = 1;
						# end this category.
						if (keys(%{$codes{$r->[0]}}) >= 5) {
							$iter = 10;
							last;
						}
					}
				}
			}			
		}
		# fill to 50 variants from the table (once per table)
		if (!defined($tables_done{$table})) {
			Logger("  - $table : general\n");
			my $iter = 1;
			while ($iter < 10 && scalar(@shuffled) > 0) {	
				my $vid_list = join(",",splice(@shuffled,0,100));
				my $ic = $dbh->selectcol_arrayref("SELECT vid FROM `$table` WHERE vid IN ($vid_list)");

				#my $ic = $dbh->selectcol_arrayref("SELECT vid FROM `$table` WHERE vid IN (SELECT vid FROM (SELECT vid FROM `$table` ORDER BY RAND() LIMIT 100) t)");
				foreach my $vid (@$ic) {
					$lvars{$vid} = 1;
					$allvars{$vid} = 1;
					if (keys(%lvars) >= 50) {
						$iter = 10;
						last;
					}
				}
				$iter++;
			}
			$tables_done{$table} = 1;
		}
		$dbh->disconnect();
		
	}
}
Logger("All Annotations Done. \n");
#=cut
Logger("  => Nr.Variants Fetched : ".keys(%allvars)."\n");
## get variant positions
open OUT, ">$var_file" or die("could not open : $var_file\n");
print OUT "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tSAMPLE\n";
$dbh = Get_DB_Connection();
my $vc = $dbh->selectall_arrayref("SELECT chr,start,id,RefAllele,AltAllele FROM `Variants` WHERE id IN (".join(",",keys(%allvars)).") ORDER BY chr ASC, start ASC");
foreach my $v (@$vc) {
	# replace X/Y/M
	$v->[0] = $chromhash{$v->[0]};

	if ($v->[4] =~ m/non_ref/i) {
		print LOG "Skipping non-regular variant : ".join(":",@$v)."\n";
		next;
	}
	if ($v->[4] =~ m/N/) {
		print LOG "Replacing -N- in AltAllele by -A- for annotation in : ".join(":",@$v)."\n";
		$v->[4] =~ s/N/A/g;
	}
	print OUT "chr".join("\t",@$v)."\t99\tPASS\tid=$v->[2]\tGT\t0/1\n";
}
close OUT;
Logger("Running annotations\n");
$dbh->disconnect();
#=cut
ANNOTATE:
if ($opts{'c'}) {
	print "Skipping annotating of variants\n";
	goto COMPARE;
}
########################################
## PASS 2 : ANNOTATE FOR ALL sources. ##
########################################
my %sources_done = ();
foreach my $subdir (@subdirs) {
	$subdir =~ s/^_//;
	# skip webtools
	if ($subdir eq 'WebTools') {
		next;
	}
	# second level (eg all under ANNOVAR)
	foreach my $anno (sort {$a cmp $b} keys(%{$settings->{$subdir}})) {
		if (exists($settings->{$subdir}->{$anno}->{deprecated})) {
			next;
		}
		if (!defined($settings->{$subdir}->{$anno}->{AnnotationSource})) {
			#print "No Annotation Source available for $subdir/$anno. Skipping\n";
			next;
		}
		# anntation source (e.g. RefGene)
		my $annosource = $settings->{$subdir}->{$anno}->{AnnotationSource};
		if ($annosource eq '%build%') {
			$annosource = $builds{$subdir};
		}
		if ($annosource eq 'none') {
			next;
		}
		# processed before.	
		if (defined($sources_done{"$subdir-$annosource"})) {
			next;
		}
		## is annotation file present?
		if (defined($settings->{$subdir}->{$anno}->{AnnotationFile})) {
			my $anno_file = $settings->{$subdir}->{$anno}->{AnnotationFile};
			$anno_file =~ s/%build%/$builds{$subdir}/;
			my $path = GetAnnoFilePath($subdir,$anno_file,$builds{$subdir});
			if (!-f $path) {
				$not_found{"$subdir|$anno"} = $path;
				print "Source File not found for $subdir|$anno: $path\n";
				next;
			}
			else {
				#print "Source File found for $subdir|$anno: $path\n";
			}
		}
	
		$sources_done{"$subdir-$annosource"} = 1;
		## Command
		my $out_file = $var_file;
		$out_file =~ s/\.in\.vcf/.out.$annosource.vcf/;
		my $anno_log = $logfile;
		$anno_log =~ s/log$/${subdir}_$annosource.txt/;
		Logger("  - Annotating $subdir : $annosource  : output in : $anno_log\n");
		my $command = "cd $dir/$subdir && perl LoadVariants.pl -a $annosource -f '$var_file' -o '$out_file' >> $anno_log 2>&1";
		#print "$command\n\n";
		system($command) == 0 || push(@errors, " - Command failed: $command");
		
	}
}
COMPARE:
$dbh = Get_DB_Connection();
##############################
## PASS 3 : COMPARE RESULTS ##
##############################
Logger("\n\nComparing results:\n");
my $vidstr = '';
open IN, $var_file;
while (<IN>) {
	chomp;
	next if ($_ =~ m/^#/);
	my @c = split(/\t/,$_);
	$vidstr .= "$c[2],";
}
$vidstr = substr($vidstr,0,-1);
close IN;

open MM, ">Mismatches.$log_date.txt";
my $nr_mm = 0;
print MM "vid\tsubdir\tsource\tanno\tDB\tVCF\n";
my %missing = (); # track missing outfiles (multiple anno's per file : print once)
foreach my $subdir (@subdirs) {
	$subdir =~ s/^_//;
	# skip webtools
	if ($subdir eq 'WebTools') {
		next;
	}
	# second level (eg all under ANNOVAR)
	foreach my $anno (sort {$a cmp $b} keys(%{$settings->{$subdir}})) {
		# do not handle deprecated sources.	
		if (exists($settings->{$subdir}->{$anno}->{deprecated})) {
			next;
		}
		if (!defined($settings->{$subdir}->{$anno}->{AnnotationSource})) {
			next;
		}
		# anntation source (e.g. RefGene)
		my $annosource = $settings->{$subdir}->{$anno}->{AnnotationSource};
		if ($annosource eq '%build%') {
			$annosource = $builds{$subdir};
		}
		if ($annosource eq 'none') {
			next;
		}
		## is annotation file present?
		if (defined($not_found{"$subdir|$anno"})) {
			if (!defined($missing{"$subdir|$annosource"})) {
				push(@errors," - Source file not found for $subdir/$annosource");
				$missing{"$subdir|$annosource"} = 1;
			}
			#print "not found  $subdir - $anno\n";
			next;
		}
		## table & column
		my $table;
		my $abbr;
		my @tables = split(/,/,$settings->{$subdir}->{$anno}->{from});
		# skip compound queries (use derived, non  directly annotated data.
		next if (scalar(@tables) != 1);	
		foreach my $t (@tables) {
			my ($tt, $a) = split(/ /,$t);
			if ($tt =~ m/^Variants_x_/) {
				$table = $tt;
				$abbr = $a;
				last;
			}
		}
		my ($cs,$as) = split(/ AS /,$settings->{$subdir}->{$anno}->{select});
		$cs =~ s/`$table`\.//;
		$cs =~ s/^$abbr\.//;
		my @columns = split(/,/,$cs);
		foreach my $column (@columns) {
			#print "evaluating $column from ".lc("$subdir.$annosource")."\n";
			if ($column =~ m/.*`(.*)`.*/) {
				$column = $1;
			}
			# complex query, table not replaced?
			if ($column =~ m/`|\)|\(|=/) {
				next;
			}
			# get data from DB
			my $query = "SELECT `vid`,`$column` FROM `$table` $abbr WHERE vid IN ($vidstr)";
			#print "query : $query\n";
			my $rc = $dbh->selectall_arrayref($query);
			my %current;
			foreach(@$rc) {
				if ($_->[1] =~ m/,/) {
					$_->[1] = join(",",sort {$a cmp $b} split(/,/,$_->[1]));
				}
				$current{$_->[0]}{$_->[1]} = 1;
			}
			# get data from VCF
			my %vcf;
			my $out_file = $var_file;
			$out_file =~ s/\.in\.vcf/.out.$annosource.vcf/;
			if (!-f $out_file && !defined($missing{$out_file})) {
				push(@errors," - Missing file: $out_file");
				$missing{$out_file} = 1;
				next;
			}
			open IN, "$out_file" or next;# or die("$out_file not found\n");
			#print "reading : $out_file\n";		
			
			while (<IN>) {
				chomp;
				next if ($_ =~ m/^#/);
				my @cols = split(/\t/,$_);
				my $vid = $cols[2];
				my @info = split(/;/,$cols[7]);
				foreach my $field (@info) {
					my ($lab,$data) = split(/=/,$field,2);
					# for readability : 	use snpEff.GRCh37.66 instead of just GRCh37.66
					#			use ANNOVAR.refgene instead of just refgene			
					if (lc($lab) eq lc($annosource) || lc($lab) eq lc("$subdir.$annosource")) {
						my @entries = split(/\|/,$data);
						foreach my $entry (@entries) {
							my @values = split(/,/,$entry);
							foreach $item (@values) {
								my ($c,$v) = split(/:/,$item,2);
									
								if($c eq $column) {
									# possibility of having a,b vs b,a (annovar hash => list conversion variability?)
									if ($v =~ s/\\x2c/,/g) {
										$v = join(",",sort {$a cmp $b} split(/,/,$v));
									}
									$vcf{$vid}{$v} = 1;
								}
							}
						}
					}
				}	
			}
			
			# compare.
			foreach my $vid (split(/,/,$vidstr)) {
				# exact match.
				if (join(",",sort {$a cmp $b} keys(%{$current{$vid}})) eq join(",",sort {$a cmp $b} keys(%{$vcf{$vid}}))) {
					next;
				}
				# single item.
				if (keys(%{$current{$vid}}) == 1 && keys(%{$vcf{$vid}}) == 1) {
					my $a = (keys(%{$current{$vid}}))[0];
					my $b = (keys(%{$vcf{$vid}}))[0];
					# numeric
					if (looks_like_number($a) && looks_like_number($b)) {
						# match.
						if ($a == $b) {
							next;
						}
						# rounding ?
						if (abs(sprintf("%.5f",$a) - sprintf("%.5f",$b)) < 0.0001)  {
							next;
						}
					}
				}
				$nr_mm++;
				print MM "$vid\t$subdir\t$annosource\t$anno\t".join(",",sort {$a cmp $b} keys(%{$current{$vid}}))."\t".join(",",sort {$a cmp $b} keys(%{$vcf{$vid}}))."\n";
				
			}
		}
	}
}
close MM;
if ($nr_mm > 0) {
	# message to stdout.
	Logger("\n\n##########\n\n");
	Logger("# ERROR: #\n");
	Logger("##########\n\n");
	Logger("  We found $nr_mm mismatches in annotations between the database and live annotations.\n");
	Logger("  An overview will be sent to the system administration and can be found in : \n");
	Logger("     ".abs_path("Mismatches.$log_date.txt").".gz\n");
	## compose mail
	system("gzip 'Mismatches.$log_date.txt'")== 0 || push(@errors, " - Result file compression failed: '$dir/Mismatches.$log_date.txt' ");
	my $from = 'no-reply@'.hostfqdn();
	my $subject = 'VariantDB ERROR : Annotation discrepancy';
	my $content =  "Dear Administrator,\r\n\r\n";
	   $content .= "An update was applied to the VariantDB web-interface at ".hostfqdn()."\r\n";
	   $content .= "Some annotation discrepancies were observed during a post-update check. Please revise the file in attachment for details.\r\n\r\n";
	   if (scalar(@errors) > 0) {
		$content .= "Some errors were encountered:\r\n";
		foreach(@errors) {
			$content .= "$_\r\n";
		}
	   }
	   $content .= "This is an automated message from VariantDB.";
	SendMail($from,$adminemail,$subject,"Mismatches.$log_date.txt.gz",'application/gzip',$content);
}
else {
	## send mail for success. 
	my $from = 'no-reply@'.hostfqdn();
	my $subject = 'VariantDB Update Report';
	my $content =  "Dear Administrator,\r\n\r\n";
	if (scalar(@errors) > 0) {
	   $content .= "An update was applied to the VariantDB web-interface at ".hostfqdn()."\r\n";
	   $content .= "No annotation discrepancies were observed, but some possible problems were encountered. Please review:\r\n\r\n";
	   foreach(@errors) {
		$content .= "$_\r\n";
	   }
        }
	else {
	   $content .= "An update was applied to the VariantDB web-interface at ".hostfqdn()."\r\n";
	   $content .= "No annotation discrepancies were observed.\r\n\r\n";
	}
	$content .= "This is an automated message from VariantDB.";
	SendMail($from,$adminemail,$subject,'','application/gzip',$content);

	unlink("Mismatches.$log_date.txt");
	print LOG "\nFinished : no mismatches found\n";
	# clean up ? 
	system("rm Update.Files/Variants.update.$log_date.*.vcf");
	system("rm job_output/Validate_Updates.$log_date.*.txt");
	
}
print "\n\t=> RESULT : $nr_mm mismatches found\n\n";	
Logger("\n\t=> RESULT : $nr_mm mismatches found\n\n");

sub Logger {
	# open/close forces flushing
	my $msg = shift;
	open LOG, ">>$logfile" or die("Could not open logfile for appending msg: \n file: $logfile\n msg: $msg\n");
	print LOG $msg;
	close LOG;
}

sub SendMail
{
   use MIME::Lite;
   use File::Basename;

   my( $from, $to, $subject, $filename, $type, $data ) = @_;
   # headers
   $msg = MIME::Lite->new(
	From	=> $from,
	To	=> $to,
	Subject	=> $subject,
	Type	=> 'multipart/mixed'
	);
   # text content
   $msg->attach(
	Type	=> 'TEXT',
	Data	=> $data
	);
   if ($filename ne '') {
	   # attachment
	   $msg->attach(
		Type	=> $type,
		Path	=> $filename,
		Filename => basename($filename),
		Disposition	=> 'attachment'
		);
   }
   # send.
   $msg->send; # I believe this depends on correctly set smtp in sendmail configuration (DS field)
   return;
}

sub GetAnnoFilePath {
	my ($subdir,$anno_file,$build) = @_;
	if ($subdir eq "ANNOVAR") {
		return("ANNOVAR/humandb/$anno_file");
	}
	if ($subdir eq "snpEff") {
		return("snpEff/data/$build/snpEffectPredictor.bin");
	}
	if ($subdir eq "ClinVar") {
		return("ClinVar/ClinVarFullRelease_Loaded_head.xml");
	}
	if ($subdir eq "CADD") {
		return("CADD/$anno_file");
	}
	# not special, return original
	return($anno_file);
}



sub Get_DB_Connection {
	## GET current/provided build
	my $db = '';
	my $dbh;
	if (!defined($opts{'b'}) || $opts{'b'} eq '') {
		$db = "NGS-Variants-Admin";
		my $connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
		$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
		$dbh->{mysql_auto_reconnect} = 1;
		## retry on failed connection (server overload?)
		my $i = 0;
		while ($i < 10 && ! defined($dbh)) {
			sleep 7;
			$i++;
			print "Connection to $dbhost failed, retry nr $i/10\n";
			$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) ;
		}
		my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
		$gsth->execute();
		my ($cb) = $gsth->fetchrow_array();
		$gsth->finish();
		$db = "NGS-Variants$cb";
		$dbh->disconnect();
	}
	else {
		$db = "NGS-Variants".$opts{'b'};
	}
	my $connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	my $i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $dbhost failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) ;
	}
	return($dbh);
}


