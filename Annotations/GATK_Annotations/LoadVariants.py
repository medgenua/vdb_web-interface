from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, KillProcessTree
import configparser
import argparse
import os
import sys
import time
from datetime import datetime
import subprocess
import fcntl
from multiprocessing import Pool, Value, Lock
import statistics
import psutil

class DataError(Exception):
    pass


## cross-thread counter : not fully safe from race conditions...
class Counter(object):
    def __init__(self):
        self.val = Value("i", 0)

    def increment(self, n=1):
        with self.val.get_lock():
            self.val.value += n

    def set(self, v):
        with self.val.get_lock():
            self.val.value = v

    @property
    def value(self):
        return self.val.value


## some variables/objects
re = Re()  # customized regex-handler


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Summarize variant frequencies by user/project
            
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Run in Validation Mode. All variants are annotated, and stored in a separate table.",
    )
    # all users/projects
    parser.add_argument(
        "-a",
        "--all",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="(Re)Process all users and projects.",
    )
    parser.add_argument(
        "-q",
        "--queued",
        required=False,
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        help="Process queued users/projects (requested through UI).",
    )
    parser.add_argument("-l", "--loglevel", required=False, help="Override the log level")
    args = parser.parse_args()
    # if loglevel provided : set it.
    if args.loglevel:
        config["LOGGING"]["LOG_LEVEL"] = args.loglevel.upper()
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    return config, args


# Validate the provided options.
def ValidateOptions(args):
    # suffix provided ?
    if config["DATABASE"].get("DBSUFFIX",None):
        db_suffix = f"_{config['DATABASE']['DBSUFFIX']}"
    else:
        db_suffix = ""
    # build provided ?
    if args.build:
        # should exist
        try:
            dbh.select_db("NGS-Variants-{}{}".format(args.build, db_suffix))
        except MySqlError as e:
            raise ValueError("Invalid GenomeBuild '{}{}' : {}".format(args.build, db_suffix, repr(e)))
    else:
        # use/get current.
        row = dbh.runQuery(f"SELECT `name`, `StringName` FROM `NGS-Variants-Admin{db_suffix}`.`CurrentBuild`")
        args.build = row[0]["name"][1:]

    return True


# Validate the provided options.
def ValidateTables(args, table_name):

    # validation:
    if args.validate:
        try:
            dbh.doQuery("DROP TABLE IF EXISTS `{}{}`".format(args.prefix, table_name))
            dbh.doQuery("CREATE TABLE `{}{}` LIKE `{}`".format(args.prefix, table_name, table_name))
            return True
        except Exception as e:
            raise MySqlError(
                "Failed to create validation table : {}{} : {}".format(args.prefix, table_name, e)
            )
    # no validation
    if not dbh.tableExists(table_name):
        raise MySqlError("Required Table does not exists: {}".format(table_name))

    return True


def ProjectSummarizer(args, config):
    global counter
    counter = Counter()

    log.debug("Fetching Projects to summarize")
    scriptdir = config["LOCATIONS"]["SCRIPTDIR"]
    global table_name
    table_name = "{}Variants_x_Projects_Summary".format(args.prefix)
    # get projects.
    projects = set()
    if args.all or args.validate:
        projects = {x["id"] for x in dbh.runQuery("SELECT id FROM `Projects`")}
    elif args.queued:
        # projects associated to queued users
        locked_file_descriptor = open(
            os.path.join(scriptdir, "Query_Results/.ProjectSummary.lck"), "w+"
        )
        fcntl.lockf(locked_file_descriptor, fcntl.LOCK_EX)
        if not os.path.exists(os.path.join(scriptdir, "Query_Results/.ProjectSummary.queue")):
            open(os.path.join(scriptdir, "Query_Results/.ProjectSummary.queue"), "w").close()
        with open(os.path.join(scriptdir, "Query_Results/.ProjectSummary.queue"), "r") as fh:
            for line in fh:
                p_rows = dbh.runQuery(
                    "SELECT p.id FROM `Projects` p JOIN `Projects_x_Users` pu ON p.id = pu.pid WHERE pu.uid = ? AND `SummaryStatus` = 0",
                    line.rstrip(),
                )
                projects.update([x["id"] for x in p_rows])
        open(os.path.join(scriptdir, "Query_Results/.ProjectSummary.queue"), "w").close()
        locked_file_descriptor.close()
    else:
        projects = {
            x["id"] for x in dbh.runQuery("SELECT id FROM `Projects` WHERE `SummaryStatus` = 0")
        }
    # nothing to do. return to __main__
    if not projects:
        log.debug("No projects to do.")
        return 0
    global nr_projects
    nr_projects = len(projects)
    p_string = ",".join([str(x) for x in projects])
    log.info("Projects to do : {}".format(nr_projects))
    ## QUEUE USERS WITH ACCESS TO THESE PROJECTS.
    log.info(f"requeueing users with access to {p_string}")
    dbh.doQuery(
        f"UPDATE Users SET SummaryStatus = 0 WHERE id IN (SELECT uid FROM `Projects_x_Users` WHERE pid IN ({p_string}))"
    )
    # start workers.
    if "MYSQLTHREADS" in config["DATABASE"] and int(config["DATABASE"]["MYSQLTHREADS"]) > 2:
        log.info(f"Starting {config['DATABASE']['MYSQLTHREADS']} worker threads")
        pool = Pool(processes=int(config["DATABASE"]["MYSQLTHREADS"]))
    else:
        log.info("Using a single worker thread")
        pool = Pool(processes=1)
    try:
        _ = pool.map_async(
            SummarizeProject, projects, chunksize=1, error_callback=ErrorCallback
        ).get()
    except Exception as e:
        log.error("Failed to Summarize Projects : {}".format(e))
        KillProcessTree(include_parent=True)
    try:
        pool.close()
        pool.join()
    except Exception as e:
        raise e
    log.info("All projects done.")
    if not args.all and not args.validate and not args.queued:
        projects = {
            x["id"] for x in dbh.runQuery("SELECT id FROM `Projects` WHERE `SummaryStatus` = 0")
        }
        return len(projects)
    else:
        return 0


def SummarizeProject(pid):
    global table_name
    global nr_projects
    log = get_logger(f"SummarizeProject.{pid}")

    # thread specific DB connection.
    dbh = GetDBConnection(quiet=True)
    global counter
    dbh.doQuery("UPDATE `Projects` SET `SummaryStatus` = 1 WHERE id = %s", pid)

    # samples
    s_r = dbh.runQuery(
        "SELECT s.id,s.gender,s.IsControl FROM `Samples` s JOIN `Projects_x_Samples` ps ON s.id = ps.sid  WHERE ps.pid = %s",
        pid,
    )

    # print log
    counter.increment()
    if not s_r:
        log.info(
            "Skipping work on poject {}/{} : pid {} : no samples".format(
                counter.value, nr_projects, pid
            )
        )
        dbh.doQuery("UPDATE `Projects` SET `SummaryStatus` = 2 WHERE id = %s", pid)
        return True

    log.info(
        "Starting work on poject {}/{} : pid {} : {} samples".format(
            counter.value, nr_projects, pid, len(s_r)
        )
    )
    # start work
    samples = dict()
    nr_controles = nr_males = nr_females = 0

    for r in s_r:
        samples[r["id"]] = {"g": r["gender"], "c": r["IsControl"]}
        nr_controles += r["IsControl"]
        if r["gender"] == "Male":
            nr_males += 1
        elif r["gender"] == "Female":
            nr_females += 1
    sids = ",".join([str(x) for x in list(samples.keys())])
    # highest variant
    maxvid = dbh.runQuery("SELECT id FROM `Variants` ORDER BY id DESC LIMIT 1")[0]["id"]

    ## loop in batches of 10,000 UNIQUE variants (can be present in many samples.)
    from_idx = 1
    while from_idx < maxvid:
        to_idx = from_idx + 9999
        if to_idx > maxvid:
            to_idx = maxvid
        # fetch by 50K rows
        rows = dbh.runQuery(
            "SELECT vid,sid, inheritance,InheritanceMode,class,AltCount,RefDepth,AltDepth FROM `Variants_x_Samples` WHERE (vid BETWEEN %s AND %s) AND sid in ({}) ".format(
                sids
            ),
            (from_idx, to_idx),
            size=50000,
        )
        summary = dict()
        depths = dict()

        while len(rows) > 0:
            # process current set
            for row in rows:
                if row["vid"] not in summary:
                    summary[row["vid"]] = [0] * 24
                    depths[row["vid"]] = list()
                # GT index = AltCount	+ 0 for all samples
                # 			+ 3 for controls
                # 			+ 6 for males
                # 			+ 9 for females

                # all samples
                summary[row["vid"]][row["AltCount"]] += 1
                # Controls
                if samples[row["sid"]]["c"] == 1:
                    summary[row["vid"]][3 + row["AltCount"]] += 1
                # male
                if samples[row["sid"]]["g"] == "Male":
                    summary[row["vid"]][6 + row["AltCount"]] += 1
                # female
                if samples[row["sid"]]["g"] == "Female":
                    summary[row["vid"]][9 + row["AltCount"]] += 1
                # diagnostic class == 11+class
                if row["class"] > 0:
                    summary[row["vid"]][11 + row["class"]] += 1
                # inheritance position : (19+inh)
                if row["inheritance"] > 0:
                    summary[row["vid"]][19 + row["inheritance"]] += 1
                # inheritance mode position : (17+inhM)
                if row["InheritanceMode"] > 0:
                    summary[row["vid"]][17 + row["InheritanceMode"]] += 1
                # depths
                depths[row["vid"]].append(row["RefDepth"] + row["AltDepth"])

            # get next set
            rows = dbh.GetNextBatch()

        # print depths & means
        if not len(summary) == len(depths):
            log.error(
                "Writing {} summaries and {} depth entries for pid {}. Please investigate".format(
                    len(summary), len(depths), pid
                )
            )

        with open(
            os.path.join(args.wd, "load_depth_{}_{}.txt".format(pid, from_idx)), "w"
        ) as dh, open(os.path.join(wd, "load_project_{}_{}.txt".format(pid, from_idx)), "w") as ph:
            for vid in depths.keys():
                # depths
                dh.write("{},{},{}\n".format(vid, pid, "-".join([str(x) for x in depths[vid]])))
                # project
                mean, median = ArrayStats(depths[vid])
                ph.write(
                    "{},{},{},{},{}\n".format(
                        vid, pid, ",".join([str(x) for x in summary[vid]]), mean, median
                    )
                )
        # get variants to delete.
        vids = [
            x["vid"]
            for x in dbh.runQuery(
                "SELECT vid FROM `{}` WHERE pid = %s AND vid BETWEEN %s AND %s".format(table_name),
                (pid, from_idx, to_idx),
            )
        ]
        in_del = ""
        for vid in vids:
            if vid not in summary:
                in_del = "{}{},".format(in_del, vid)

        # get variants to delete from depths.
        vids = [
            x["vid"]
            for x in dbh.runQuery(
                "SELECT vid FROM `{}` WHERE pid = %s AND vid BETWEEN %s AND %s".format(
                    table_name.replace("_Summary", "_Depths")
                ),
                (pid, from_idx, to_idx),
            )
        ]
        in_depth_del = ""
        for vid in vids:
            if vid not in summary:
                in_depth_del = "{}{},".format(in_depth_del, vid)

        # load into DB.
        with open(os.path.join(args.wd, "db.lock"), "w+") as lock:
            fcntl.lockf(lock, fcntl.LOCK_EX)
            # variant counts
            dbh.doQuery(
                "LOAD DATA CONCURRENT LOCAL INFILE '{}' REPLACE INTO TABLE `{}` FIELDS TERMINATED BY ','  (vid,pid,AllHomRef,AllHet,AllHomAlt,ConHomRef,ConHet,ConHomAlt,MaleHomRef,MaleHet,MaleHomAlt,FemaleHomRef,FemaleHet,FemaleHomAlt,nrPatho,nrUVKL4,nrUVKL3,nrUVKL2,nrBenign,nrFalsePositive,nrDominant,nrRecessive,nrPaternal,nrMaternal,nrDeNovo,nrBiParental,MeanCov,MedianCov)".format(
                    os.path.join(wd, "load_project_{}_{}.txt".format(pid, from_idx)),
                    table_name,
                )
            )
            os.remove(os.path.join(wd, "load_project_{}_{}.txt".format(pid, from_idx)))
            # variant depths
            dbh.doQuery(
                "LOAD DATA CONCURRENT LOCAL INFILE '{}' REPLACE INTO TABLE `{}` FIELDS TERMINATED BY ',' (vid,pid,depth)".format(
                    os.path.join(wd, "load_depth_{}_{}.txt".format(pid, from_idx)),
                    table_name.replace("_Summary", "_Depths"),
                )
            )
            os.remove(os.path.join(wd, "load_depth_{}_{}.txt".format(pid, from_idx)))
            # delete deprecated variants
            if in_del:
                dbh.doQuery(
                    "DELETE FROM `{}` WHERE pid = '{}' AND vid in ({})".format(
                        table_name, pid, in_del[:-1:]
                    )
                )
            if in_depth_del:
                dbh.doQuery(
                    "DELETE FROM `{}` WHERE pid = '{}' AND vid in ({})".format(
                        table_name.replace("_Summary", "_Depths"), pid, in_depth_del[:-1:]
                    )
                )

        # slice done. prepare for next
        from_idx = to_idx + 1
    # all slices done. update project.
    dbh.doQuery(
        "UPDATE `Projects` SET `SummaryStatus` = 2 WHERE id = %s AND `SummaryStatus` = 1", pid
    )


def UserSummarizer(args, config):
    # reset counter
    global counter
    counter = Counter()
    # new dbh connection. (might/is) lost after forking projects.
    dbh = GetDBConnection(quiet=True)

    log.debug("Fetching users to summarize")
    scriptdir = config["LOCATIONS"]["SCRIPTDIR"]
    global table_name
    table_name = "{}Variants_x_Users_Summary".format(args.prefix)
    # get users
    users = set()
    if args.all or args.validate:
        log.debug("Recalculating all users")
        users = {x["id"] for x in dbh.runQuery("SELECT id FROM `Users`")}
    elif args.queued:
        log.debug("Fetching users from queue file")
        locked_file_descriptor = open(
            os.path.join(scriptdir, "Query_Results/.UserSummary.lck"), "w+"
        )
        fcntl.lockf(locked_file_descriptor, fcntl.LOCK_EX)
        if not os.path.exists(os.path.join(scriptdir, "Query_Results/.UserSummary.queue")):
            open(os.path.join(scriptdir, "Query_Results/.UserSummary.queue"), "w").close()
        with open(os.path.join(scriptdir, "Query_Results/.UserSummary.queue"), "r") as fh:
            for line in fh:
                u_status = dbh.runQuery(
                    "SELECT `SummaryStatus` FROM `Users` WHERE id = %s AND level > 0",
                    line.rstrip(),
                )[0]["SummaryStatus"]
                if u_status == 0:
                    users.add(line.rstrip())
        # clear queue
        open(os.path.join(scriptdir, "Query_Results/.UserSummary.queue"), "w").close()
        locked_file_descriptor.close()
    else:
        log.debug("Fetching users with out-of-date summaries")
        users = {x["id"] for x in dbh.runQuery("SELECT id FROM `Users` WHERE `SummaryStatus` = 0 ")}

    # nothing to do. return to __main__
    if not users:
        log.debug("No users to do.")
        return 0

    # group users by samples to reduce calculation cost.
    grouped_users = dict()
    project_map = dict()
    for user in users:
        # all projects for user
        projects = dbh.runQuery(
            "SELECT pu.pid FROM `Projects_x_Users` pu JOIN `Projects` p ON p.id = pu.pid WHERE uid = %s AND p.summarize = 1",
            user,
        )
        projects = set([x["pid"] for x in projects])
        log.debug(f"User {user} has these projects summarized: {projects}")
        # this is the first user with access to this set of projects.
        first_user = next((key for key, value in project_map.items() if value == projects), None)
        if first_user is not None:
            # identical project set : list as dependent user.
            grouped_users[first_user] += f",{user}"
        else:
            # novel dependent user set.
            grouped_users[user] = f"{user}"
            project_map[user] = projects
    # re-list the (groups of) users
    users = list(grouped_users.values())
    global nr_users
    nr_users = len(users)
    # finally, order by desc max priority of users in the groups.
    max_priorities = []
    for x in users:
        max_priority = dbh.runQuery("SELECT MAX(priority) as max_priority FROM `Users` WHERE id IN ({}) GROUP BY id".format(x))[0]['max_priority']
        max_priorities.append(max_priority)
    users = [x for _, x in sorted(zip(max_priorities, users), reverse=True)]    

    log.info("Users(groups) to do : {}".format(nr_users))
    # start workers.
    if "MYSQLTHREADS" in config["DATABASE"] and int(config["DATABASE"]["MYSQLTHREADS"]) > 2:
        log.info(f"Starting {config['DATABASE']['MYSQLTHREADS']} worker threads")
        pool = Pool(processes=int(config["DATABASE"]["MYSQLTHREADS"]))
    else:
        log.info("Using a single worker thread")
        pool = Pool(processes=1)

    # launch processes
    try:
        output = pool.map_async(
            SummarizeUser, users, chunksize=1, error_callback=ErrorCallback
        ).get()
    except Exception as e:
        log.error("Failed to Summarize Projects : {}".format(e))
        KillProcessTree(include_parent=True)
    try:
        pool.close()
        pool.join()
    except Exception as e:
        raise e
    log.info("All users done.")
    if not args.all and not args.validate and not args.queued:
        users = {x["id"] for x in dbh.runQuery("SELECT id FROM `Users` WHERE `SummaryStatus` = 0")}
        return len(users)
    else:
        return 0


def ErrorCallback(result):
    log.error(result)
    KillProcessTree(timeout=15, include_parent=True)


def SummarizeUser(uid_string):
    global table_name
    global nr_users
    global counter
    uids = uid_string.split(",")
    uid = uids.pop(0)
    log.info(f"Launching SummarizeUser.{uid} with extra users : {uids}")
    # thread specific db connection.
    dbh = GetDBConnection(quiet=True)
    # set all to processing
    dbh.doQuery(f"UPDATE `Users` SET `SummaryStatus` = 1 WHERE id IN ({uid_string})")
    # highest variant
    maxvid = dbh.runQuery("SELECT id FROM `Variants` ORDER BY id DESC LIMIT 1")[0]["id"]
    # projects & samples with access
    u_r = dbh.runQuery(
        "SELECT s.id,s.gender,s.IsControl,ps.pid FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON s.id = ps.sid AND ps.pid = pu.pid WHERE pu.uid = %s",
        uid,
    )
    # print log msg.

    counter.increment()
    if not u_r:
        log.info(
            "Skipping work on user {}/{} : uid {} : no samples".format(counter.value, nr_users, uid)
        )
        last_update = datetime.now().strftime("%Y-%m-%d")
        dbh.doQuery(
            f"UPDATE `Users` SET nrSamples = '0',nrControls = '0', nrMales = '0', nrFemales = '0', SummaryStatus = 2, LastSummaryUpdate = %s WHERE id IN ({uid_string})",
            last_update,
        )
        dbh.doQuery(f"DELETE FROM `Variants_x_Users_Summary` WHERE uid IN ({uid_string})")
        return True

    log.info(
        "Starting work on user {}/{} : uid {} : {} samples".format(
            counter.value, nr_users, uid, len(u_r)
        )
    )
    # start work.
    samples = dict()
    projects = set()
    nr_controles = nr_males = nr_females = 0
    for r in u_r:
        projects.add(r["pid"])
        samples[r["id"]] = {"g": r["gender"], "c": r["IsControl"]}
        nr_controles += r["IsControl"]
        if r["gender"] == "Male":
            nr_males += 1
        elif r["gender"] == "Female":
            nr_females += 1

    # check project status
    pids = ",".join([str(x) for x in list(projects)])
    # runnings imports set status to 1 , so they are picked up here.
    # finished imports or any othr action sets status to 0.
    pending_projects = dbh.runQuery(
        f"SELECT id FROM Projects WHERE id IN ({pids}) AND SummaryStatus < 2"
    )
    if pending_projects:
        log.warning(f"User {uid} has {len(pending_projects)} Pending projects. ReQueuing user(s)")
        dbh.doQuery(f"UPDATE `Users` SET SummaryStatus = 0 WHERE id IN ({uid_string})")
        return True

    # update user info
    dbh.doQuery(
        f"UPDATE `Users` SET nrSamples = %s ,nrControls = %s, nrMales = %s, nrFemales = %s WHERE id IN ({uid_string})",
        (len(u_r), nr_controles, nr_males, nr_females),
    )

    # for logging : user-related variant_id in samples :
    log.info(f"uid:{uid} : Requesting highest vid values")
    try:
        highest_sample_vid, highest_summary_vid = GetMaxVids(pids, uid)
        log.info(
            f" User {uid} PRE LOAD: highest v_x_s vid: {highest_sample_vid} -- highest summ.vid : {highest_summary_vid}"
        )
    except Exception as e:
        log.error(f"{e}")
        # not fatal, it's purely stats.

    ## loop in batches of 10,000 UNIQUE variants (can be present in multiple projects, but this is limited.)
    from_idx = 1
    while from_idx < maxvid:
        to_idx = from_idx + 9999
        if to_idx > maxvid:
            to_idx = maxvid
        log.debug(f"Processing user {uid} variant {from_idx} to {to_idx}")
        # build user summary by summing the project summaries. get data as array, easier to sum.
        summary = dict()
        depths = dict()

        # first frequencies
        rows = dbh.runQuery(
            statement="SELECT * FROM `{}Variants_x_Projects_Summary` WHERE pid IN ({}) AND vid BETWEEN %s AND %s".format(
                args.prefix, pids
            ),
            params=(from_idx, to_idx),
            as_dict=False,
        )
        for row in rows:
            # new variant : initialize
            if row[0] not in summary:
                summary[row[0]] = [0] * 24
                depths[row[0]] = list()
            # add values : skip first & last two entries (vid / pid / mean_depth / median_depth)
            summary[row[0]] = [x + y for (x, y) in zip(summary[row[0]], row[2:-2])]

        # then depth
        rows = dbh.runQuery(
            statement="SELECT `vid`, `pid`, `depth` FROM `{}Variants_x_Projects_Depths` WHERE pid IN ({}) AND vid BETWEEN %s AND %s".format(
                args.prefix, pids
            ),
            params=(from_idx, to_idx),
        )
        for row in rows:
            if row["vid"] not in depths:
                log.error(
                    "VID {} not in frequency table, and present in depths. ReQueue".format(
                        row["vid"]
                    )
                )
                # requeue the project
                dbh.doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id = %s", row["pid"])
                # and the user(s)
                dbh.doQuery(f"UPDATE `Users` SET `SummaryStatus` = 0 WHERE id IN {uid_string}")
                continue
            depths[row["vid"]].extend([int(x) for x in row["depth"].split("-")])

        # write loading file.
        with open(os.path.join(wd, "load_user_{}_{}.txt".format(uid, from_idx)), "w") as uh:
            for vid in summary:
                if len(depths[vid]) == 0:
                    log.error(
                        "VID {} not in depth table, and present in frequencies. ReQueue Projects? ".format(
                            row["vid"]
                        )
                    )
                    # requeue the variant for all samples having it
                    vps = [
                        x["pid"]
                        for x in dbh.runQuery(
                            "SELECT ps.pid FROM `Variants_x_Samples` vs JOIN `Projects_x_Samples` ps ON ps.sid = vs.sid WHERE ps.pid IN (pids) AND vs.vid = vid"
                        )
                    ]
                    dbh.doQuery(
                        "UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id IN  ({})".format(
                            ",".join(vps)
                        )
                    )
                    dbh.doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = %s", uid)

                mean, median = ArrayStats(depths[vid])
                uh.write(
                    "{},{},{},{},{}\n".format(
                        vid, uid, ",".join([str(x) for x in summary[vid]]), mean, median
                    )
                )
        # get variants to delete.
        vids = [
            x["vid"]
            for x in dbh.runQuery(
                "SELECT vid FROM `{}` WHERE uid = %s AND vid BETWEEN %s AND %s".format(table_name),
                (uid, from_idx, to_idx),
            )
        ]
        in_del = ""
        for vid in vids:
            if vid not in summary:
                in_del = "{}{},".format(in_del, vid)

        # load into DB
        with open(os.path.join(args.wd, "db.lock"), "w+") as lock:
            fcntl.lockf(lock, fcntl.LOCK_EX)
            # variant counts
            dbh.doQuery(
                "LOAD DATA CONCURRENT LOCAL INFILE '{}' REPLACE INTO TABLE `{}` FIELDS TERMINATED BY ','  (vid,uid,AllHomRef,AllHet,AllHomAlt,ConHomRef,ConHet,ConHomAlt,MaleHomRef,MaleHet,MaleHomAlt,FemaleHomRef,FemaleHet,FemaleHomAlt,nrPatho,nrUVKL4,nrUVKL3,nrUVKL2,nrBenign,nrFalsePositive,nrDominant,nrRecessive,nrPaternal,nrMaternal,nrDeNovo,nrBiParental,MeanCov,MedianCov)".format(
                    os.path.join(wd, "load_user_{}_{}.txt".format(uid, from_idx)),
                    table_name,
                )
            )
            # additional users:
            for extra_uid in uids:
                log.debug(f"Loading user {extra_uid} based on user {uid}")
                log.debug(
                    f"using datafile : "
                    + os.path.join(wd, "load_user_{}_{}.txt".format(uid, from_idx))
                )
                with open(
                    os.path.join(wd, "load_user_{}_{}.txt".format(uid, from_idx)), "r"
                ) as fh_in, open(
                    os.path.join(wd, "load_user_{}_{}.txt".format(extra_uid, from_idx)), "w"
                ) as fh_out:
                    for line in fh_in:
                        c = line.rstrip().split(",")
                        c[1] = extra_uid
                        fh_out.write(",".join([str(x) for x in c]) + "\n")
                dbh.doQuery(
                    "LOAD DATA CONCURRENT LOCAL INFILE '{}' REPLACE INTO TABLE `{}` FIELDS TERMINATED BY ','  (vid,uid,AllHomRef,AllHet,AllHomAlt,ConHomRef,ConHet,ConHomAlt,MaleHomRef,MaleHet,MaleHomAlt,FemaleHomRef,FemaleHet,FemaleHomAlt,nrPatho,nrUVKL4,nrUVKL3,nrUVKL2,nrBenign,nrFalsePositive,nrDominant,nrRecessive,nrPaternal,nrMaternal,nrDeNovo,nrBiParental,MeanCov,MedianCov)".format(
                        os.path.join(wd, "load_user_{}_{}.txt".format(extra_uid, from_idx)),
                        table_name,
                    )
                )
                os.remove(os.path.join(wd, "load_user_{}_{}.txt".format(extra_uid, from_idx)))

            # clean up
            os.remove(os.path.join(wd, "load_user_{}_{}.txt".format(uid, from_idx)))
            # delete deprecated variants
            if in_del:
                dbh.doQuery(
                    "DELETE FROM `{}` WHERE uid = '{}' AND vid in ({})".format(
                        table_name, uid, in_del[:-1:]
                    )
                )
                for extra_uid in uids:
                    dbh.doQuery(
                        "DELETE FROM `{}` WHERE uid = '{}' AND vid in ({})".format(
                            table_name, extra_uid, in_del[:-1:]
                        )
                    )
        # slice done. prepare for next
        from_idx = to_idx + 1
    # all slices done. update user if not requeued by now.
    last_update = datetime.now().strftime("%Y-%m-%d")
    dbh.doQuery(
        f"UPDATE `Users` SET `SummaryStatus` = 2, `LastSummaryUpdate` = %s WHERE id IN ({uid_string}) AND `SummaryStatus` = 1",
        last_update,
    )
    # for logging : user-related variant_id in samples :
    log.info(f"uid:{uid} : Requesting highest vid values")
    try:
        highest_sample_vid, highest_summary_vid = GetMaxVids(pids, uid)
        log.info(
            f" User {uid} POST LOAD: highest v_x_s vid: {highest_sample_vid} -- highest summ.vid : {highest_summary_vid}"
        )
    except Exception as e:
        log.error(f"{e}")
        # not fatal, it's purely stats.
    # if status == 2 ; these vids should be equal.
    s_status = dbh.runQuery("SELECT `SummaryStatus` FROM `Users` WHERE id = %s", uid)[0][
        "SummaryStatus"
    ]
    if s_status == 2 and not highest_sample_vid == highest_summary_vid:
        log.error(
            f"User summary failed for uid  {uid}. Highest summary VID != highest sample vid : {highest_summary_vid} != {highest_sample_vid}"
        )
    log.info(f"User {uid} : done")
    return


def GetMaxVids(pids, uid):
    try:
        # V_x_S : just fetch all, untill a matching sample is found.
        query = f"SELECT vid FROM `Variants_x_Samples` vs FORCE INDEX (`vid`) WHERE vs.sid IN (SELECT sid FROM Projects_x_Samples WHERE pid IN ({pids})) ORDER BY vid DESC LIMIT 1"
        highest_sample_vid = dbh.runQuery(query)[0]["vid"]

        # summary
        highest_summary_vid = dbh.runQuery(
            "SELECT vid FROM `Variants_x_Users_Summary` WHERE uid = %s ORDER BY vid DESC LIMIT 1",
            uid,
        )
        if highest_summary_vid:
            highest_summary_vid = highest_summary_vid[0]["vid"]
        else:
            highest_summary_vid = 0

        return highest_sample_vid, highest_summary_vid

    except Exception as e:
        raise e


def ArrayStats(d):
    if not d:
        return 0, 0
    try:
        mean = statistics.mean(d)
        median = statistics.median(d)
        return mean, median
    except Exception as e:
        log.error(
            "Could not compute mean/median. Returning 0,0. Input: {} -- error : {}".format(
                "-".join(d), e
            )
        )
        return 0, 0


def GetDBConnection(quiet=True):
    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)
        if not quiet:
            log.info(
                "Connected to Database {} using Genome Build {}".format(db, row[0]["StringName"])
            )
    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        sys.exit(1)
    # switch build?
    if args.build:
        # should exist
        try:
            dbh.select_db("NGS-Variants-{}{}".format(args.build, db_suffix))
        except MySqlError as e:
            raise ValueError("Invalid GenomeBuild '{}' : {}".format(args.build, e))
    return dbh


def WritePID(name, type="w"):
    pid = str(os.getpid())
    with open(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Logs", "{}.pid".format(name)), type
    ) as fh:
        fh.write(pid)

def CheckImports(name="VariantDB_Importer.py"):
    for process in psutil.process_iter(['pid', 'name', 'cmdline']):
        if process.info['name'] == name or (process.info['cmdline'] and name in process.info['cmdline']):
            log.info("Found running import : {}".format(process.info['cmdline']))
            return True
    return False

## array consisting of :
# 	0 : #hom.ref.samples
# 	1 : #het.samples
# 	2 : #hom.alt.samples
# 	3 : #hom.ref.controls
# 	4 : #het.controls
# 	5 : #hom.alt.controls
# 	6 : #hom.ref.males
# 	7 : #het.males
# 	8 : #hom.alt.males
# 	9 : #hom.ref.females
# 	10: #het.females
# 	11: #hom.alt.females
# 	12: #pathogenic
# 	13: #UVKL4
# 	14: #UVKL3 : unkown signficance
# 	15: #UVKL2
# 	16: #benign
# 	17: #false.positive
# 	18: #dominant
# 	19: #recessive
# 	20: #paternal
# 	21: #maternal
# 	22: #de novo
# 	23: #biparental


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # write out the pid.
    try:
        WritePID("LoadVariants")
    except Exception as e:
        print("Unable to set pid. Exiting : {}".format(e))
        sys.exit(1)

    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except Exception as e:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        print(f"ERROR: Could not log to config path : {e}")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )

    # setup logging.
    setup_logging(
        name="VariantDB_Summarizer",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
    )
    log = get_logger("main")
    log.info(msg)

    dbh = GetDBConnection(quiet=False)
    ## Validate options
    try:
        ValidateOptions(args)
    except (DataError, ValueError) as e:
        log.error(e)
        sys.exit(1)
    try:
        ValidateTables(args, "Variants_x_Projects_Summary")
        ValidateTables(args, "Variants_x_Users_Summary")
    except Exception as e:
        log.error("Problem with required tables: {}".format(e))
        sys.exit(1)

    # create working dir.
    try:
        wd = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GATK_Annotations/Output_Files/",
            "PU_{}_{}".format(os.getpid(), time.time()),
        )
        os.makedirs(wd, exist_ok=False)
        args.wd = wd
    except Exception as e:
        log.error("Could not create working dir: {}".format(e))
        sys.exit(1)

    # this annotator runs continuously.
    iterator = -1  # first round launch all subtasks.
    while True:
        iterator += 1
        # every round : single variant queue.
        try:
            cmd = ["python", "LoadSingleVariants.py", "--loglevel", config["LOGGING"]["LOG_LEVEL"]]
            log.debug(cmd)
            subprocess.check_call(cmd)
        except Exception as e:
            log.error(f"Failed to run LoadSingleVariants.py : {e}")
            sys.exit(1)

        # Postpone work if imports are running
        if CheckImports():
            time.sleep(30)
            continue
        # approximately once every 5 minutes : full update.
        if iterator % (2 * 5) == 0:
            log.debug("Starting Project/User summarizer")
        else:
            time.sleep(30)
            continue

        ######################
        ## PROCESS PROJECTS ##
        ######################
        try:
            log.debug("Start Projects")
            pending_projects = ProjectSummarizer(args, config)
        except Exception as e:
            log.error("Failed in ProjectSummarizer: {}".format(e))
            sys.exit(1)

        ###################
        ## PROCESS USERS ##
        ###################

        try:
            log.debug("Start Users")
            pending_users = UserSummarizer(args, config)
        except Exception as e:
            log.error("Failed in UserSummarizer: {}".format(e))
            sys.exit(1)

        #############################
        ## manual runs : quit here ##
        #############################
        if args.all or args.validate or args.queued:
            break

        ## if there are pending tasks, reduce iterator so next round will immediately start
        if pending_projects + pending_users > 0:
            log.info(
                f"Pending tasks: {pending_projects} projects and {pending_users} users. reset iterator."
            )
            iterator -= 1
