from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, KillProcessTree
import configparser
import argparse
import os
import sys
import time
import fcntl
import shutil
from multiprocessing import Pool
import statistics


class DataError(Exception):
    pass


class ValueError(Exception):
    pass


## some variables/objects
re = Re()  # customized regex-handler


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Summarize variant frequencies starting from as singel variant+sample.
            
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Run in Validation Mode. All variants are annotated, and stored in a separate table.",
    )
    parser.add_argument("-l", "--loglevel", help="override log level", required=False)
    args = parser.parse_args()
    if args.loglevel:
        config["LOGGING"]["LOG_LEVEL"] = args.loglevel.upper()
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    return config, args


# Validate the provided options.
def ValidateOptions(args):

    # build provided ?
    if args.build:
        # should exist
        try:
            dbh.select_db("NGS-Variants-{}".format(args.build))
        except MySqlError as e:
            raise ValueError("Invalid GenomeBuild '{}' : {}".format(args.build, e))
    else:
        # use/get current.
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `NGS-Variants-Admin`.`CurrentBuild`")
        args.build = row[0]["name"][1:]

    return True


# Validate the provided options.
def ValidateTables(args, table_name):

    # validation:
    if args.validate:
        try:
            dbh.doQuery("DROP TABLE IF EXISTS `{}{}`".format(args.prefix, table_name))
            dbh.doQuery("CREATE TABLE `{}{}` LIKE `{}`".format(args.prefix, table_name, table_name))
            return True
        except Exception as e:
            raise MySqlError(
                "Failed to create validation table : {}{} : {}".format(args.prefix, table_name, e)
            )
    # no validation
    if not dbh.tableExists(table_name):
        raise MySqlError("Required Table does not exists: {}".format(table_name))

    return True


def SummarizeVariant(task):
    # task details
    vid, sid = task.split(",")

    # thread specific db connection.
    dbh = GetDBConnection()

    log.info("Starting work on Variant {} from sample {}".format(vid, sid))

    ##################
    ## USER SUMMARY ##
    ##################
    # all users with access to this sample:
    u_r = dbh.runQuery(
        "SELECT pu.`uid` FROM `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON ps.`pid` = pu.`pid` WHERE ps.`sid` = %s",
        sid,
    )
    if not u_r:
        log.warning("No users found with access to sample {}".format(sid))
        return True
    log.debug(f"Nr.Users with access to sample {sid} : {len(u_r)}")

    # for each user, get samples details & update metrics in user-summary.
    for uid in [u["uid"] for u in u_r]:
        s_r = dbh.runQuery(
            "SELECT s.id,s.gender,s.IsControl FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects_x_Users` pu JOIN `Projects` p ON p.id = ps.pid AND s.id = ps.sid AND ps.pid = pu.pid WHERE p.summarize = 1 AND pu.uid = %s",
            uid,
        )
        samples = dict()
        nr_controles = nr_males = nr_females = 0
        for r in s_r:
            samples[r["id"]] = {"g": r["gender"], "c": r["IsControl"]}
            nr_controles += r["IsControl"]
            if r["gender"] == "Male":
                nr_males += 1
            elif r["gender"] == "Female":
                nr_females += 1
        sids = ",".join([str(x) for x in list(samples.keys())])
        # then get all metrics for vid in sids
        rows = dbh.runQuery(
            "SELECT vid,sid, inheritance,InheritanceMode,class,AltCount,RefDepth,AltDepth FROM `Variants_x_Samples` WHERE vid = %s AND sid in ({})".format(
                sids
            ),
            vid,
        )
        summary = [0] * 24
        depths = list()

        log.debug(f"Processing {len(rows)} entries from V_x_S for vid {vid} and user {uid}")
        for row in rows:
            # GT index = AltCount	+ 0 for all samples
            # 			+ 3 for controls
            # 			+ 6 for males
            # 			+ 9 for females
            # all samples
            summary[row["AltCount"]] += 1
            # Controls
            if samples[row["sid"]]["c"] == 1:
                summary[3 + row["AltCount"]] += 1
            # male
            if samples[row["sid"]]["g"] == "Male":
                summary[6 + row["AltCount"]] += 1
            # female
            if samples[row["sid"]]["g"] == "Female":
                summary[9 + row["AltCount"]] += 1
            # diagnostic class == 11+class
            if row["class"] > 0:
                summary[11 + row["class"]] += 1
            # inheritance position : (19+inh)
            if row["inheritance"] > 0:
                summary[19 + row["inheritance"]] += 1
            # inheritance mode position : (17+inhM)
            if row["InheritanceMode"] > 0:
                summary[17 + row["InheritanceMode"]] += 1
            # depths
            depths.append(row["RefDepth"] + row["AltDepth"])
        # coverage stats
        mean, median = ArrayStats(depths)
        # update in DB.
        replace_query = "REPLACE INTO `{}Variants_x_Users_Summary` (vid,uid,AllHomRef,AllHet,AllHomAlt,ConHomRef,ConHet,ConHomAlt,MaleHomRef,MaleHet,MaleHomAlt,FemaleHomRef,FemaleHet,FemaleHomAlt,nrPatho,nrUVKL4,nrUVKL3,nrUVKL2,nrBenign,nrFalsePositive,nrDominant,nrRecessive,nrPaternal,nrMaternal,nrDeNovo,nrBiParental,MeanCov,MedianCov) VALUES ({},{},{},{},{})".format(
            args.prefix, vid, uid, ",".join([str(x) for x in summary]), mean, median
        )
        log.debug(replace_query)
        dbh.doQuery(replace_query)

    #####################
    ## PROJECT SUMMARY ##
    #####################
    # sample <=> project is unique
    pid = dbh.runQuery("SELECT pid FROM `Projects_x_Samples` WHERE sid = %s", sid)[0]["pid"]
    # all samples in this project.
    p_r = dbh.runQuery(
        "SELECT s.id,s.gender,s.IsControl FROM `Samples` s JOIN `Projects_x_Samples` ps ON s.id = ps.sid WHERE ps.pid =  %s",
        pid,
    )
    samples = dict()
    nr_controles = nr_males = nr_females = 0
    for r in p_r:
        samples[r["id"]] = {"g": r["gender"], "c": r["IsControl"]}
        nr_controles += r["IsControl"]
        if r["gender"] == "Male":
            nr_males += 1
        elif r["gender"] == "Female":
            nr_females += 1
    sids = ",".join([str(x) for x in list(samples.keys())])
    # variant details
    rows = dbh.runQuery(
        "SELECT sid, inheritance,InheritanceMode,class,AltCount,RefDepth,AltDepth FROM `Variants_x_Samples` WHERE vid = %s AND sid in ({})".format(
            sids
        ),
        vid,
    )
    summary = [0] * 24
    depths = list()
    for row in rows:
        # GT index = AltCount	+ 0 for all samples
        # 			+ 3 for controls
        # 			+ 6 for males
        # 			+ 9 for females
        # all samples
        summary[row["AltCount"]] += 1
        # Controls
        if samples[row["sid"]]["c"] == 1:
            summary[3 + row["AltCount"]] += 1
        # male
        if samples[row["sid"]]["g"] == "Male":
            summary[6 + row["AltCount"]] += 1
        # female
        if samples[row["sid"]]["g"] == "Female":
            summary[9 + row["AltCount"]] += 1
        # diagnostic class == 11+class
        if row["class"] > 0:
            summary[11 + row["class"]] += 1
        # inheritance position : (19+inh)
        if row["inheritance"] > 0:
            summary[19 + row["inheritance"]] += 1
        # inheritance mode position : (17+inhM)
        if row["InheritanceMode"] > 0:
            summary[17 + row["InheritanceMode"]] += 1
        # depths
        depths.append(row["RefDepth"] + row["AltDepth"])
    # coverage stats
    mean, median = ArrayStats(depths)
    # update in DB.
    dbh.doQuery(
        "REPLACE INTO `{}Variants_x_Projects_Summary` (vid,pid,AllHomRef,AllHet,AllHomAlt,ConHomRef,ConHet,ConHomAlt,MaleHomRef,MaleHet,MaleHomAlt,FemaleHomRef,FemaleHet,FemaleHomAlt,nrPatho,nrUVKL4,nrUVKL3,nrUVKL2,nrBenign,nrFalsePositive,nrDominant,nrRecessive,nrPaternal,nrMaternal,nrDeNovo,nrBiParental,MeanCov,MedianCov) VALUES ({},{},{},{},{})".format(
            args.prefix, vid, pid, ",".join([str(x) for x in summary]), mean, median
        )
    )
    # depths
    dbh.doQuery(
        "REPLACE INTO `{}Variants_x_Projects_Depths` (vid,pid,depth) VALUES (%s,%s,%s)".format(
            args.prefix
        ),
        (vid, pid, "-".join([str(x) for x in depths])),
    )
    log.info("Variant updated : {}".format(task))


def ArrayStats(d):
    if not d:
        return 0, 0
    try:
        mean = statistics.mean(d)
        median = statistics.median(d)
        return mean, median
    except Exception as e:
        log.error(
            "Could not compute mean/median. Returning 0,0. Input: {} -- error : {}".format(
                "-".join(d), e
            )
        )
        return 0, 0


def GetDBConnection(quiet=True):
    try:
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database="NGS-Variants-Admin",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}".format(row[0]["name"])
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)
        if not quiet:
            log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))

    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        sys.exit(1)
    # switch build?
    if args.build:
        # should exist
        try:
            dbh.select_db("NGS-Variants-{}".format(args.build))
        except MySqlError as e:
            raise ValueError("Invalid GenomeBuild '{}' : {}".format(args.build, e))
    return dbh


def ErrorCallback(result):
    log.error(result)
    KillProcessTree(timeout=15, include_parent=True)


## array consisting of :
# 	0 : #hom.ref.samples
# 	1 : #het.samples
# 	2 : #hom.alt.samples
# 	3 : #hom.ref.controls
# 	4 : #het.controls
# 	5 : #hom.alt.controls
# 	6 : #hom.ref.males
# 	7 : #het.males
# 	8 : #hom.alt.males
# 	9 : #hom.ref.females
# 	10: #het.females
# 	11: #hom.alt.females
# 	12: #pathogenic
# 	13: #UVKL4
# 	14: #UVKL3 : unkown signficance
# 	15: #UVKL2
# 	16: #benign
# 	17: #false.positive
# 	18: #dominant
# 	19: #recessive
# 	20: #paternal
# 	21: #maternal
# 	22: #de novo
# 	23: #biparental


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_Summarizer_SingleVariants",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
        permissions=777,
    )
    log = get_logger("main")
    log.info(msg)

    dbh = GetDBConnection(quiet=False)

    ## Validate options
    try:
        ValidateOptions(args)
    except (DataError, ValueError) as e:
        log.error(e)
        sys.exit(1)
    try:
        ValidateTables(args, "Variants_x_Projects_Summary")
        ValidateTables(args, "Variants_x_Users_Summary")
    except Exception as e:
        log.error("Problem with required tables: {}".format(e))
        sys.exit(1)

    ## get todo list.
    log.info("Fetching work list")
    try:
        queue_file = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "Query_Results/.VariantSummary.queue"
        )
        if not os.path.exists(queue_file):
            log.warning("Queue not found. Creating : {}".format(queue_file))
            open(queue_file, "w").close()
        todo = set()
        lock = open(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Query_Results/.VariantSummary.lock"),
            "w+",
        )
        fcntl.lockf(lock, fcntl.LOCK_EX)
        with open(queue_file, "r") as fh:
            for line in fh:
                line = line.rstrip()
                # only lines with contents.
                if line:
                    todo.add(line)
        # clear queue:
        open(queue_file, "w").close()
    except Exception as e:
        log.error("Could not read in variant queue: {}".format(e))
        sys.exit(1)

    if not todo:
        log.info("Nothing to do.")
        sys.exit(0)

    # start processing.
    log.info("Processing {} items".format(len(todo)))
    # start workers.
    if "MYSQLTHREADS" in config["DATABASE"] and int(config["DATABASE"]["MYSQLTHREADS"]) > 2:
        log.info("Staring 4 worker threads")
        pool = Pool(processes=4)
    else:
        log.info("Using a single worker thread")
        pool = Pool(processes=1)

    # create unique working dir.
    try:
        wd = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GATK_Annotations/Output_Files/",
            "SV_{}_{}".format(os.getpid(), time.time()),
        )
        os.makedirs(wd, exist_ok=False)
        args.wd = wd
    except Exception as e:
        log.error("Could not create working dir: {}".format(e))
        sys.exit(1)

    # process variants
    # launch processes
    # output = [pool.apply_async(SummarizeVariant, args=(x,)) for x in todo]

    # pool.close()
    try:
        # Monitor(output)
        output = pool.map_async(
            SummarizeVariant, todo, chunksize=1, error_callback=ErrorCallback
        ).get()
    except Exception as e:
        log.error("Failed to Summarize Variants : {}".format(e))
        log.warning("Killing children.")
        KillProcessTree(include_parent=True)
    pool.close()
    pool.join()
    log.info("All Done")
    # clean up.
    shutil.rmtree(args.wd)
