#!/bin/bash
 
## qsubwrapper.sh v1.0 
## Script to submit jobs to Torque/pbs without 
##   the need to write your own scripts. 
 
## Two Step process: 
##   1. Take command and arguments, submit to this script preceeded with qsub
##   2. use this script as the actual batch script running provided command
 
## written by Geert Vandeweyer 
## see : http://geertvandeweyer.zymichost.com/index.php?page=read&id=28

 
 
##################################################
## PRINT SHORT HELP IF NO ARGUMENTS ARE PRESENT ##
##################################################
if [[ $# == 0 && $run == "" ]] ; then
    echo ""
    echo "  Provide a program and parameters to run on the cluster."
    echo "    eg :  QueueJob -o output.file.txt -- uptime"
    echo "       => this will get the uptime of a single (random) node and write it to output.file.txt"
    echo " "
    echo " NOTE: You need to escape all ' and \" by backslash: \\' or \\\""
    echo "  Try 'qsubwrapper -h' for the all options"
    echo ""
    echo ""
    exit;
fi     
 
if [[ "$1" == '-h' ]] ; then
    echo ""
    echo " ###############################################"
    echo " ## HOW-TO : Queue Jobs on the Torque Cluster ##"
    echo " ###############################################"
    echo ""
    echo "  NAME: "
    echo "      qsubwrapper.sh - Send Programs to the cluster queue in a non-interactive way"
    echo ""
    echo "  Synopsis:"
    echo "      qsubwrapper.sh [torque options --] Executable [additional options]"
    echo ""
    echo "  Main available Torque options:"
    echo "      -o file      : redirect stdoutput to the specified file. Default is %homedir%/jobname.o%jobid% "
    echo "      -e file      : redirect stderror to the specified file. Default is %homedir%/jobname.o%jobid% "
    echo "      -d path      : set working directory before execution. Default: home."
    echo "      -l resources : specify needed resources. See manual for all options"
    echo "                      eg: -l nodes=1:ppn=2,mem=5Gb"
    echo "      -m be        : send mail on job (b)egin and/or (e)nd to mail under -M"
    echo "      -M mail      : Set your email to recieve notifications"
    echo "      -N jobname   : set job name (max 15 chars, no white)"
    echo "      -q queue     : specify a queue (default : batch)"
    echo ""
    echo "      See 'man qsub' for a full overview of options"
    echo ""
    echo "          Seperate Torque options from main command by double hyphen '--'"
    echo ""
    echo " NOTE: You need to escape all ' and \" by backslash: \\' or \\\""
    echo ""
    echo " Example:"
    echo "      qsubwrapper.sh -o output.file.txt -- df -h ";
    echo "          => Get information on the free disk space of the node this job is sent to,";
    echo "             write output to output.file.txt in the current directory.";
    echo ""
    echo ""
    echo "      qsubwrapper.sh -o output.file.txt -d /path/to/directory -- find . -type f -name \\'*\\'"
    echo "          => Get all files in the specified directory. Note the escaped ' !"
    echo ""
    echo ""
    exit;
fi
########################################
## FIRST CASE : RESUBMIT USING qsub ##
########################################
#pathtoscript=`which qsubwrapper.sh`
pathtoscript=`pwd`"/qsubwrapper.sh"
echo $0
echo "pathtoscript: '"$pathtoscript"'"
if [ "$0" == "$pathtoscript" ] ; then
    args=("$@")
    command=""
    pbsopts=""
    index=0
    
pbsfound=0
    ## torque/pbs options
    for (( f=0; f<$#; f++ )); do
        if [ ${args[$f]} == '--' ] ; then
            pbsfound=1;
            index=$f+1;
            break;
        fi;
        ## escape single quotes
        #case ${args[$f]} in
        #   *\\\'* )
        #        ## already escaped. skip
        #        ;;
        #    *\'* )
        #        args[$f]=${args[$f]//\'/\\\'}
        #        ;;
        #esac
        ## surround arguments with quotes if containing spaces
        case ${args[$f]} in
                *\ * )
                pbsopts="$pbsopts \"${args[$f]}\""
                    ;;
                *)
                pbsopts="$pbsopts ${args[$f]}"
                    ;;
        esac
 
    done;
    if [ $pbsfound == 0 ] ; then
        pbsopts=""
    else
        pbsopts=${pbsopts:1}    
    fi
    ## program to run and parameters    
    for (( f=$index; f<$#; f++ )) do
        ## escape single quotes
        #case ${args[$f]} in
        #    *\\\'* )
	#	echo "escaped"
        #       ## already escaped. skip
        #        ;;
        #    *\'* )
	#	echo "escaping"
        #       args[$f]=${args[$f]//\'/\\\'}
        #        ;;
        #esac
        ## surround arguments with quotes if containing spaces
	
	#arg=${args[$f]//\ /\\\ }
	#command="$command $arg"
        case ${args[$f]} in
                *\ * )
                command="$command \"${args[$f]}\""
                   ;;
                *)
                command="$command ${args[$f]}"
                    ;;
        esac
	
    done;
    command=${command:1}  
    PROGRAM=${command}
    export PROGRAM
    ## pass the command to the script as the PROGRAM variable.
    #fullcommand="qsub $pbsopts -V PROGRAM='$command' $pathtoscript"   # add preceeding (pbs) options
    fullcommand="qsub $pbsopts -V -v run=1 $pathtoscript"
    echo "Submitting Job to Cluster Queue."
    echo "  => qsub command : $fullcommand"
    echo "  => job command : $PROGRAM"
    $fullcommand
	exit
 
#############################################
## SECOND CASE : ACTUAL RUNNING OF PROGRAM ##
############################################# 
else
    # command should be in environment $PROGRAM variable
    eval $PROGRAM

fi


