from CMG.DataBase import MySQL
from CMG.Utils import Mailer, Re, KillProcessTree
import configparser
import argparse
import os
import sys
import time
import subprocess
import glob
import xml.etree.ElementTree as ET
from multiprocessing import Pool
from functools import partial

from datetime import date

# add the cgi-bin/Modules to path.
sys.path.append("../cgi-bin/Modules")
from Utils import StartLogger, Annotator

re = Re()

#

""" The following struct holds the info of changes to report to users."""
report_settings = {
    # table
    "Variants_x_ANNOVAR_ncbigene": {
        # relevant fields to query:
        "relevant_fields": ["VariantType"],
        # annotation values relevant to report
        "VariantType": r"frameshift|stop|nonsynonymous|splicing",
        "CADD": {"for": r"nonsynonymous", "threshold": 30},
        # fields that need decoding : number => string in emails
        "decode": ["VariantType", "GeneLocation"],
    },
    "Variants_x_ClinVar_ncbigene": {
        "relevant_fields": ["AA_MatchType", "Class"],
        "AA_MatchType": r"^match$",
        "Class": [r"pathogenic", r"NOTconflicting interpretations"],
        "decode": ["AA_MatchType"],
        # if data comes from multiple tables
        "join": " cv JOIN `ClinVar_db` cvdb ON cvdb.id = cv.cvid",
    },
    # "Variants_x_GenePanels_ncbigene": r"NotUsed",
    "Variants_x_snpEff": {
        "relevant_fields": ["Effect"],
        "Effect": r"DELETION|INSERTION|FRAME_SHIFT|NON_SYNONYMOUS|LOST|GAINED|ACCEPTOR|DONOR",
        "CADD": {"for": r"NON_SYNONYMOUS", "threshold": 30},
        "decode": ["Effect", "EffectImpact"],
    },
    # CADD
    "Variants_x_CADD_v1.4": {
        "relevant_fields": ["Phred_Score"],
        "comparison": {"Phred_Score": ">="},
        "Phred_Score": 30,
    },
}


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL: EVALUATE NEW ANNOTATION RELEASES
        ######################################
            - check new release for all annotation sources 
            - if present : run "UpdateAnnotations.py" in annotation subdirs
            - when finished : scan log for relevant new changes
            - notify users of these changes.
            
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "-e",
        "--email",
        required=False,
        help="For Validation, send all emails to this address instead of users",
    )
    parser.add_argument(
        "-i",
        "--last_log_id",
        required=False,
        help="Process log entries starting at this value, instead of current highest",
    )
    parser.add_argument(
        "-t", "--time_stamp", required=False, help="Restart from a specified timestamp"
    )
    args = parser.parse_args()
    return config, args


class DataError(Exception):
    pass


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def LoadAnnotations():
    # track with sets to remove duplicates (multiple items from one source/table)
    normal = set()
    delayed = set()
    # load config.
    annotations_config = ET.parse(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/Config.xml")
    )
    a_root = annotations_config.getroot()
    # first level == subdirectories in Annotations folder.
    for subdir in a_root:
        log.info(subdir.tag)
        # not all are relevant
        if (
            not os.path.isdir(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", subdir.tag)
            )
            or subdir.tag == "GATK_Annotations"
        ):
            log.info(" => skipped")
            continue

        # second level: annotation to work on (-a xxx)
        for anno in subdir:
            log.info("   => {}".format(anno.tag))
            # skip if deprecated
            if ET.iselement(anno.find("deprecated")):
                log.info("      => skipped/depr")
                continue
            # skip if no annotation source specified
            if not ET.iselement(anno.find("AnnotationSource")):
                log.info("      => skipped/noSource")
                continue
            # delayed ?
            if ET.iselement(anno.find("depend")):
                delayed.add(
                    "{}|{}|{}".format(
                        subdir.tag, anno.find("AnnotationSource").text, anno.find("depend").text
                    )
                )
            else:
                # normal
                normal.add("{}|{}".format(subdir.tag, anno.find("AnnotationSource").text))

    return normal, delayed


def RunAnnotationUpdates(job):
    subfolder, source, *dep = job.split("|")
    log.info("Running anntations {} : {} ".format(subfolder, source))
    try:
        cmd = "cd {} ; python UpdateAnnotations.py -a {}".format(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", subfolder), source
        )
        log.info(cmd)
        # run silently, each script logs individually.
        log.info(f"Starting {job}")
        subprocess.check_call(cmd, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        log.info(" {} : done".format(job))
        # return

    except Exception as e:
        log.warning(f"{job} : {e}")
        raise e
        # return ("{} : {}".format(job, e), None)


def Monitor(async_output):
    while True:
        AllDone = True
        for r in async_output:
            log.info(f"Monitor : {r}")
            if not r.ready():
                AllDone = False
                continue
            if not r.successful():
                log.warning("Detected failure ! ")
                raise Exception(r.get())
        if not AllDone:
            time.sleep(1)
        else:
            log.info("Annotations done.")
            return True


""" CHECK log since time-stamp, detect relevant changes, then notify users."""


def ComposeEmails(time_stamp, last_log_id):
    log.info("composing emails.")
    # get codes.
    codes = dict()
    # relevant_codes = dict()
    # rel_regex = r"|".join(list(report_settings.keys()))
    rows = annotator.dbh.runQuery("SELECT * FROM `Value_Codes`", as_dict=False)
    for row in rows:
        # numeric => textual
        codes[str(row[0])] = row[2]
    # add missing
    codes["."] = "."

    tmp_vxs_table = "Tmp_Update_vxs_{}".format(time_stamp)
    tmp_log_table = "Tmp_Update_LogParsing_{}".format(time_stamp)
    tmp_var_table = "Tmp_Update_var_{}".format(time_stamp)
    tmp_clinvar_table = "Tmp_Update_clinvar_{}".format(time_stamp)

    ## slice log to tmp table.
    try:
        log.info("Parsing logfile")
        tmp_log_table = ParseLog(time_stamp, last_log_id, codes)
    except Exception as e:
        log.warning("Failed to Parse the log table : {}".format(e))
        raise (e)

    ## get samples linked to log events
    try:
        tmp_vxs_table = "Tmp_Update_vxs_{}".format(time_stamp)
        log.info("extract samples with affected variants")
        annotator.dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(tmp_vxs_table))
        annotator.dbh.doQuery(
            "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(9) , `sid` INT(6), `AltCount` TINYINT(1), `Class` TINYINT(1), PRIMARY KEY (sid, vid))  ENGINE=MYISAM CHARSET=latin1".format(
                tmp_vxs_table
            )
        )

        annotator.dbh.doQuery(
            "INSERT IGNORE INTO `{}` SELECT vs.vid, vs.sid , vs.AltCount, vs.class FROM `Variants_x_Samples` vs INNER JOIN `{}` tl ON vs.vid = tl.vid".format(
                tmp_vxs_table, tmp_log_table
            )
        )
    except Exception as e:
        log.error("Failed to fill tmp_vxs table : {} : {} ".format(tmp_vxs_table, e))
        raise (e)

    # some variant meta info
    try:
        log.info("Collect variant meta info")
        tmp_var_table = "Tmp_Update_var_{}".format(time_stamp)
        annotator.dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(tmp_var_table))
        annotator.dbh.doQuery(
            "CREATE TABLE `{}` (`vid` INT(9) PRIMARY KEY, `varloc` VARCHAR(1000), `vargene` VARCHAR(5000)) ENGINE=MYISAM CHARSET=latin1".format(
                tmp_var_table
            )
        )
        tmp_clinvar_table = "Tmp_Update_clinvar_{}".format(time_stamp)
        annotator.dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(tmp_clinvar_table))
        annotator.dbh.doQuery(
            "CREATE TABLE `{}` (`vid` INT(9) PRIMARY KEY, `clinvar` VARCHAR(5000)) ENGINE=MYISAM CHARSET=latin1".format(
                tmp_clinvar_table
            )
        )
        # gene (transcript) : Exon nr (GeneLoc) : VariantType
        var_concat = "CONCAT('chr',v.chr,':',v.start, ' ',v.RefAllele,'/',v.AltAllele)"
        gene_concat = "CONCAT('   - ',vg.GeneSymbol, ' (',vg.Transcript,') : Exon ',vg.Exon, ' (', vc1.Item_Value, ') : ', vc2.Item_Value)"  #  FROM `Variants_x_ANNOVAR_refgene` vg JOIN `Value_Codes` vc1 JOIN `Value_Codes` vc2 ON vg.GeneLocation = vc1.id AND vg.VariantType = vc2.id WHERE vg.vid = $vid");
        from_table = "`Variants` v INNER JOIN `{}` tv ON v.id = tv.vid INNER JOIN `Variants_x_ANNOVAR_ncbigene` vg ON vg.vid = tv.vid JOIN `Value_Codes` vc1 ON vg.GeneLocation = vc1.id JOIN `Value_Codes` vc2 ON vg.VariantType = vc2.id ".format(
            tmp_log_table
        )
        q = "INSERT INTO `{}` SELECT v.id, {}, GROUP_CONCAT(DISTINCT {} SEPARATOR '\\n' ) FROM {} GROUP BY v.id".format(
            tmp_var_table, var_concat, gene_concat, from_table
        )
        log.debug(q)
        annotator.dbh.doQuery(q)
        # clinvar
        cv_concat = (
            "CONCAT('   -',cv.cvid, ' -- Class: ', vc.Item_Value, ' --  Disease:', cvdb.Disease)"
        )
        from_table = "`{}` tv JOIN `Variants_x_ClinVar_ncbigene` cv JOIN `ClinVar_db` cvdb JOIN `Value_Codes` vc ".format(
            tmp_log_table
        )
        on_table = "tv.vid = cv.vid AND cvdb.id = cv.cvid AND vc.id = cvdb.class"
        q = "INSERT INTO `{}` SELECT cv.vid, GROUP_CONCAT(DISTINCT {} SEPARATOR '\\n') FROM {}  ON {} GROUP BY cv.vid".format(
            tmp_clinvar_table, cv_concat, from_table, on_table
        )
        log.info(q)
        annotator.dbh.doQuery(q)
    except Exception as e:
        log.error("Failed to get variant meta info : {}".format(e))
        raise (e)

    # process all users (only project owners, no shared data users.)
    users = annotator.dbh.runQuery("SELECT * FROM Users")
    mailer = Mailer(
        sender=config["EMAIL"]["ADMINMAIL"],
        # port=config["EMAIL"]["SMTP_PORT"],
        # host=config["EMAIL"]["SMTP_HOST"],
        # login=False,
        showname=config["EMAIL"]["NAME"],
    )

    for user in users:
        log.info("Processing data for user {}".format(user["email"]))
        mail = BuildMail(user, tmp_log_table, tmp_vxs_table, tmp_var_table, tmp_clinvar_table)
        if not mail:
            log.info("No data for this user.")
            continue
        if os.path.getsize(mail):

            # build full body.
            log.info("sending mail to {}".format(user["email"]))
            body = "Dear {} {},\r\n\r\n".format(user["FirstName"], user["LastName"])
            body += "VariantDB annotations were updated and some possibly novel interesting annotations came out. Only the following entries are reported:\r\n"
            body += " - Exonic, NonSynonymous RefSeq, where no such annotation was available previously\r\n"
            body += " - Exonic, NonSynonymous snpEff, where no such annotation was available previously\r\n"
            body += " - Exact, possibly pathogenic  variants in ClinVar\r\n\r\n"
            body += "Below is the list of variants, together with samples containing them:\r\n\r\n"
            mh = open(mail, "r")
            body += mh.read()
            mh.close()
            body += "\r\nBest Regards,\r\nThe VariantDB admin\r\n"
            # send
            try:
                if args.email:
                    to = args.email
                else:
                    to = user["email"]
                mailer.send(to=to, subject="VariantDB: critical updates", message=body)

            except Exception as e:
                mail_path = os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/Update.Files",
                    "mail.{}.{}.txt".format(time_stamp, user["id"]),
                )
                with open(mail_path, "w") as fh:
                    fh.write(body)
                log.error(
                    "Failed to send email to {}. Stored at {}. Error was : {}".format(
                        user["email"], mail_path, e
                    )
                )


def BuildMail(user, tmp_log_table, tmp_vxs_table, tmp_var_table, tmp_clinvar_table):
    # genotype array:
    gts = ["HomRef", "Heterozygous", "HomAlt"]
    classes = {0: "-", 1: "Path.", 2: "UVKL2", 3: "UVKL3", 4: "UVKL4", 5: "Benign", 6: "FP"}
    # get samples
    projects = annotator.dbh.runQuery("SELECT `id` FROM `Projects` WHERE `userID` = %s", user["id"])
    projects = [str(x["id"]) for x in projects]
    if not projects:
        log.debug("no projects")
        return ""
    samples = annotator.dbh.runQuery(
        "SELECT s.id, s.Name, s.Gender FROM `Samples` s JOIN `Projects_x_Samples` ps ON ps.sid = s.id WHERE ps.pid IN ('{}')".format(
            "','".join(projects)
        )
    )
    samples = {x["id"]: x for x in samples}
    sids = list(samples.keys())
    if not sids:
        log.debug("No samples")
        return ""
    log.debug(f"sids: {sids}")
    # loop samples & construct contents
    rows = annotator.dbh.runQuery(
        "SELECT l.`comment`, vs.sid,vs.vid, vs.AltCount, vs.Class, v.varloc, v.vargene, cv.clinvar FROM `{}` l JOIN `{}` vs ON l.vid = vs.vid JOIN `{}` v ON v.vid = l.vid LEFT JOIN `{}` cv ON v.vid = cv.vid WHERE vs.sid IN ('{}') ORDER BY vs.vid,vs.sid".format(
            tmp_log_table,
            tmp_vxs_table,
            tmp_var_table,
            tmp_clinvar_table,
            "','".join([str(x) for x in sids]),
        ),
        size=50000,
    )
    log.debug(
        "SELECT l.`comment`, vs.sid,vs.vid, vs.AltCount, vs.Class, v.varloc, v.vargene, cv.clinvar FROM `{}` l JOIN `{}` vs ON l.vid = vs.vid JOIN `{}` v ON v.vid = l.vid LEFT JOIN `{}` cv ON v.vid = cv.vid WHERE vs.sid IN ('{}') ORDER BY vs.vid,vs.sid".format(
            tmp_log_table,
            tmp_vxs_table,
            tmp_var_table,
            tmp_clinvar_table,
            "','".join([str(x) for x in sids]),
        )
    )
    last_sid = ""
    last_vid = ""
    log.info(f"entries to do : {len(rows)}")
    mail_body = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/Update.Files",
        "body.{}.{}.txt".format(time_stamp, user["id"]),
    )
    fh = open(mail_body, "w")
    nr_samples = 0
    nr_benign = 0
    var_entry = ""
    while len(rows):
        for row in rows:
            # new vid:
            if row["vid"] != last_vid:
                # > 15 samples : print (... n more ...)
                if nr_samples > 10:
                    var_entry += "  (... {} more samples not listed ... )\r\n".format(
                        nr_samples - 10
                    )
                # only print in email if nr_samples < 50
                if nr_benign > 2:
                    log.info(f"Encountered {nr_benign} classifications of variant {last_loc}.")
                if nr_samples < 50 and nr_benign <= 2:
                    fh.write(var_entry)
                # reset sample tracker
                nr_samples = 0
                last_sid = ""
                nr_benign = 0
                # new variant
                last_vid = row["vid"]
                last_loc = row["varloc"]
                # general variant info
                var_entry = "\r\n{}:\n".format(row["varloc"])
                var_entry += "RefSeq:\n{}\n".format(row["vargene"])
                var_entry += "ClinVar:\n{}\n".format(row["clinvar"])
                var_entry += "Samples:\n"

            # new sample
            if last_sid != row["sid"]:
                nr_samples += 1
                if row["Class"] > 3:
                    nr_benign += 1
                if nr_samples > 10:
                    continue
                last_sid = row["sid"]
                var_entry += " - Sample {} ({}): {} : DC:{}\r\n".format(
                    samples[last_sid]["Name"],
                    samples[last_sid]["Gender"],
                    gts[row["AltCount"]],
                    classes[row["Class"]],
                )

            # the observed change
            # fh.write("{}\r\n".format(row["comment"]))
        rows = annotator.dbh.GetNextBatch()
    # last entry:
    # > 15 samples : print (... n more ...)
    if nr_samples > 10:
        var_entry += "  (... {} more samples not listed ... )\r\n".format(nr_samples - 10)
    # only print in email if nr_samples < 50
    if nr_samples < 50:
        fh.write(var_entry)
    fh.close()
    return mail_body


""" 
    Parsing the log for relevant new entries consists of:
    - get added annotations (argument == 2) : check if relevant fields => add to tmp table (vid : table : type : comment)
    - get removed annotations (argument == 1) : 
          - delete from temp table because same table+type+vid adding and deleting => same effect on results.
    - get a slice of Variants_x_Samples with only vid/sid entry for relevant changes from tmp table
    - return sliced log and anno-tmp table.
"""


def ParseLog(time_stamp, last_log_id, codes):
    tmp_table = "Tmp_Update_LogParsing_{}".format(time_stamp)
    ## load the mane data
    mane_transcripts = {"mane": set(), "userKB": set()}
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ANNOVAR/humandb", "hg19_ncbi.mane.txt"
        ),
        "r",
    ) as fh:
        head = fh.readline()
        for line in fh:
            nm = line.rstrip().split("\t")[0].split(".")[0]
            mane_transcripts["mane"].add(nm)
    # load userkb
    rows = annotator.dbh.runQuery(
        "select nm_id from TranscriptSets_x_Users tu JOIN TranscriptSets_Data td ON td.set_id = tu.set_id WHERE tu.uid = 165 AND favourite = 1"
    )
    for row in rows:
        nm = row["nm_id"].split(".")[0]
        mane_transcripts["userKB"].add(nm)

    ################
    # new entries. #
    ################
    log.info("Parsing log for novel annotation entries")

    rows = annotator.dbh.runQuery(
        "SELECT vid, entry, arguments FROM `Variants_x_Log` WHERE id > %s AND arguments = 2 ",
        params=last_log_id,
        size=80000,
    )

    load_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/Update.Files/",
        "{}_log.load".format(time_stamp),
    )
    # parallel
    pool = Pool(8)

    with open(load_file, "w") as fh:
        while len(rows) > 0:
            ## reformat rows into blocks of rows
            blocks = list()
            f_idx = 0
            t_idx = 10000
            while f_idx < len(rows):
                blocks.append(rows[f_idx:t_idx])

                f_idx = t_idx
                t_idx += 10000
                if t_idx > len(rows):
                    t_idx = len(rows)
            # process.

            for resultBatch in pool.map(
                partial(ParseLogRowBatch, codes=codes, mane_transcripts=mane_transcripts), blocks
            ):
                for result in resultBatch:
                    # result == [key , vid, info]
                    # with key = "{}|{}|{}".format(vid, table, relevant_key)
                    fh.write("{}\n".format("\t".join([str(x) for x in result])))

            # next batch
            rows = annotator.dbh.GetNextBatch()
    pool.close()
    pool.join()
    try:
        # two steps because LOAD DATA does not support on duplicate/update
        # first: no keys
        annotator.dbh.doQuery("DROP TABLE IF EXISTS `{}_1`".format(tmp_table))
        annotator.dbh.doQuery(
            "CREATE TABLE IF NOT EXISTS `{}_1` (`pkey` VARCHAR(255), `vid` INT(9), `comment` VARCHAR(10000))  ENGINE=MYISAM CHARSET=latin1".format(
                tmp_table
            )
        )

        # second : add keys & count by pkey = "{}|{}|{}".format(vid, table, relevant_key)
        annotator.dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(tmp_table))
        annotator.dbh.doQuery(
            "CREATE TABLE IF NOT EXISTS `{}` (`pkey` VARCHAR(255) PRIMARY KEY, `vid` INT(9), `comment` VARCHAR(10000), `counts` INT(3) DEFAULT 1, INDEX (vid))  ENGINE=MYISAM CHARSET=latin1".format(
                tmp_table
            )
        )
        # load first
        log.info(f"Load raw entries in {annotator.dbh.database}")
        annotator.dbh.doQuery(
            "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}_1`".format(load_file, tmp_table)
        )
        # load second
        log.info(f"Deduplicate entries in {annotator.dbh.database}")
        annotator.dbh.doQuery(
            "INSERT INTO `{}` (`pkey`, `vid`, `comment`) SELECT * FROM `{}_1` ON DUPLICATE KEY UPDATE `counts` = `counts` + 1".format(
                tmp_table, tmp_table
            )
        )
        # drop first
        annotator.dbh.doQuery("DROP TABLE `{}_1`".format(tmp_table))
    except Exception as e:
        log.error("Failed to load parsed logfile into tmptable {} : {}".format(tmp_table, e))
        raise (e)
    ###################
    # deleted entries #
    # #################
    log.info("Parsing log for discarded annotation entries")

    #   don't report novel entries of same type : remove from tmp table
    rows = annotator.dbh.runQuery(
        "SELECT vid, entry, arguments FROM `Variants_x_Log` WHERE id > %s AND arguments = 1",
        params=last_log_id,
        size=80000,
    )
    pool = Pool(8)
    while len(rows) > 0:
        # reformat
        blocks = list()
        f_idx = 0
        t_idx = 10000
        while f_idx < len(rows):
            blocks.append(rows[f_idx:t_idx])

            f_idx = t_idx
            t_idx += 10000
            if t_idx > len(rows):
                t_idx = len(rows)
        # process.
        delete_batch = list()
        for resultBatch in pool.map(
            partial(ParseLogRowBatch, codes=codes, mane_transcripts=mane_transcripts), blocks
        ):
            # if result:
            for result in resultBatch:
                delete_batch.append(result)

        # delete.
        f_idx = 0
        t_idx = 1000
        while f_idx < len(delete_batch):
            in_str = "','".join(delete_batch[f_idx:t_idx])
            annotator.dbh2.doQuery(
                "DELETE FROM `{}` WHERE `pkey` IN ('{}')".format(tmp_table, in_str)
            )
            f_idx = t_idx
            t_idx += 1000
            if t_idx > len(delete_batch):
                t_idx = len(delete_batch)

        # next batch
        rows = annotator.dbh.GetNextBatch()
    # we only report NOVEL :
    #  => all entries in annotation table are new
    #  => counts in tmp and main are equal
    #   => if count in tmp < count_main : there were existing annotations before the update => do not report.
    log.info("cross-checking new with existing annotations")
    delete_batch = CountEntries(tmp_table)
    f_idx = 0
    t_idx = 1000
    while f_idx < len(delete_batch):
        in_str = "','".join(delete_batch[f_idx:t_idx])
        annotator.dbh.doQuery("DELETE FROM `{}` WHERE `pkey` IN ('{}')".format(tmp_table, in_str))
        f_idx = t_idx
        t_idx += 1000
        if t_idx > len(delete_batch):
            t_idx = len(delete_batch)
    return tmp_table


def CountEntries(tmp_table):
    del_batch = list()
    rows = annotator.dbh.runQuery("SELECT `pkey`,`counts` FROM `{}`".format(tmp_table))
    for row in rows:
        c = row["pkey"].split("|")
        # base query
        q = "SELECT COUNT(1) as c FROM `{}` {} WHERE vid = %s".format(
            c[1], report_settings[c[1]].get("join", "")
        )

        for field in report_settings[c[1]].get("relevant_fields", []):
            try:
                comparison = report_settings[c[1]]["comparison"][field]
            except KeyError:
                comparison = "="
            q = "{} AND `{}` {} %s".format(q, field, comparison)
        # parameters are encoded in pkey : vid|table|param_value1|param_value2|...
        params = [c[0]] + c[-len(report_settings[c[1]].get("relevant_fields", [])) :]
        count = annotator.dbh.runQuery(q, params)[0]["c"]
        if row["counts"] < count:
            log.debug(f"Similar entries existed for vid : {row['pkey']}")
            del_batch.append(row["pkey"])
    return del_batch


# parse a batch of log rows in pool
def ParseLogRowBatch(rows, codes, mane_transcripts):
    ldbh = annotator.GetDatabaseConnection(return_handle=True, silent=True)
    results = list()
    for row in rows:
        result = ParseLogRow(row, codes, ldbh, mane_transcripts)
        if result:
            results.append(result)

    return results


# parse single row
def ParseLogRow(row, codes, ldbh, mane_transcripts):
    # get private db connection.
    # ldbh = annotator.GetDatabaseConnection(return_handle=True,silent=True)
    vid = row["vid"]
    event_type = row["arguments"]
    table, aid = re.findall(r"(.*):(\d+)", row["entry"])[0]
    # only process the specified 'relevant' entries
    if table not in report_settings:
        return False

    # a deleted entry :
    if event_type == 1:
        archived_data = ldbh.runQuery(
            "SELECT Contents FROM `Archived_Annotations` WHERE aid = %s ", aid
        )[0]
        # parse for relevant info
        contents = archived_data["Contents"].split("@@@")
        relevant_key = False
        d = {}
        for item in contents:
            c, v = item.split("|||")
            d[c] = v
        # ncbigene
        if table == "Variants_x_ANNOVAR_ncbigene":
            # non relevant type
            if not re.isearch(report_settings[table]["VariantType"], codes[str(d["VariantType"])]):
                return False
            # cadd too low
            if re.isearch(report_settings[table]["CADD"]["for"], codes[d["VariantType"]]):
                try:
                    vcadd = ldbh.runQuery(
                        "SELECT `Phred_Score` FROM `Variants_x_CADD_v1.4` WHERE vid = %s", vid
                    )[0]["Phred_Score"]
                except IndexError:
                    log.warning(f"No cadd score in table for variant {vid}")
                    vcadd = 0
                if float(vcadd) < float(report_settings[table]["CADD"]["threshold"]):
                    log.debug(f"nonsynonymous variant has low cadd : {vcadd}")
                    return False
            # transcript should be MANE+CLINICAL+UserKb.
            if not IsMane(d["Transcript"], mane_transcripts):
                return False

            # relevant ! set key
            relevant_key = d["VariantType"]  # codes["{}_{}_{}".format(table, "VariantType", v)]

        elif table == "Variants_x_snpEff":
            # field not relevant
            if not re.isearch(report_settings[table]["Effect"], codes[str(d["Effect"])]):
                return False
            # for some vtypes, add CADD As secondary requirement:
            if re.isearch(report_settings[table]["CADD"]["for"], codes[str(d["Effect"])]):
                # fetch CADD
                vcadd = ldbh.runQuery(
                    "SELECT `Phred_Score` FROM `Variants_x_CADD_v1.4` WHERE vid = %s", vid
                )[0]["Phred_Score"]
                # compare.
                if float(vcadd) < float(report_settings[table]["CADD"]["threshold"]):
                    log.debug(f"nonsynonymous variant has low cadd : {vcadd}")
                    return False

            relevant_key = d["Effect"]

        elif table == "Variants_x_CADD_v1.4":  # and c == 'Phred_Score':
            # cadd too low.
            if not float(d["Phred_Score"]) >= float(report_settings[table]["Phred_Score"]):
                return False
            # dummy key : it means 'there was a variant with high cadd score'.
            # this allows matching eg 31 vs 32.3 on score changes
            relevant_key = 1
        # // cannot be checked for discared annotations => clinvarDB old version is gone...
        elif table == "Variants_x_ClinVar_ncbigene":
            return False

        key = "{}|{}|{}".format(vid, table, relevant_key)
        return key

    # a new entry :
    elif event_type == 2:
        # get annotations from the specified table.
        try:
            info = ldbh.runQuery(
                "SELECT * FROM `{}` WHERE aid = %s AND vid = %s".format(table),
                [aid, vid],
            )[0]
        except Exception as e:
            log.warning(f"Problem for {table} aid {aid} and vid {vid}")
            raise e
        raw_info = dict()
        # decode requested fields, if any.
        for item in report_settings[table].get("decode", []):
            raw_info[item] = info[item]
            # info[item] = codes["{}_{}_{}".format(table, item, info[item])]
            info[item] = codes[str(info[item])]
        # these blocks are partly redundant, but kept for ease of reading.
        comment = False
        relevant_key = ""
        if re.isearch(r"Variants_x_ANNOVAR_.*gene", table):
            # annotation value is not relevant for this category.
            if not re.isearch(
                report_settings[table]["VariantType"],
                info["VariantType"],
            ):
                return False
            # not mane transcript
            if not IsMane(info["Transcript"], mane_transcripts):
                return False
            # for some vtypes, add CADD As secondary requirement:
            if re.isearch(report_settings[table]["CADD"]["for"], info["VariantType"]):
                # fetch CADD
                vcadd = ldbh.runQuery(
                    "SELECT `Phred_Score` FROM `Variants_x_CADD_v1.4` WHERE vid = %s", vid
                )[0]["Phred_Score"]
                # compare.
                if float(vcadd) < float(report_settings[table]["CADD"]["threshold"]):
                    log.debug(f"nonsynonymous variant has low cadd : {vcadd}")
                    return False
            # relevant
            relevant_key = raw_info["VariantType"]
            comment = " - {} ({}), {} : {} : {}".format(
                info["GeneSymbol"],
                info["Transcript"],
                info["GeneLocation"],
                info["VariantType"],
                info["CPointAA"],
            )
        elif table == "Variants_x_CADD_v1.4":
            if not float(info["Phred_Score"]) >= float(report_settings[table]["Phred_Score"]):
                return False
            # dummy key : it means 'there was a variant with high cadd score'.
            # this allows matching eg 31 vs 32.3 on score changes
            relevant_key = 1
        elif table == "Variants_x_snpEff":
            # annotation value is not relevant for this category.
            if not re.isearch(
                report_settings[table]["Effect"],
                info["Effect"],
            ):
                return False
            # relevant
            relevant_key = raw_info["Effect"]
            comment = " - {} ({}): {} : {} : {}".format(
                info["GeneSymbol"],
                info["Transcript"],
                info["Effect"],
                info["EffectImpact"],
                info["AAchange"],
            )
        elif table == "Variants_x_ClinVar_ncbigene":
            # variant level clinvar info.
            if not re.isearch(
                report_settings[table]["AA_MatchType"],
                info["AA_MatchType"],
            ):
                return False
            # extra info from main ClinVar_DB
            cv_info = ldbh.runQuery(
                "SELECT Class, VarType, Gene, Disease, Class FROM `ClinVar_db` WHERE id = %s",
                info["cvid"],
            )[0]
            raw_info["Class"] = cv_info["Class"]
            # cv_info["Class"] = codes["{}_{}_{}".format(table, "Class", cv_info["Class"])]
            cv_info["Class"] = codes[str(cv_info["Class"])]
            # relevant ?
            keep = True
            # class has two entries
            for rg in report_settings[table]["Class"]:
                if rg.startswith("NOT") and re.isearch(rg[3:], cv_info["Class"]):
                    keep = False
                elif not re.isearch(rg, cv_info["Class"]):
                    keep = False
            if not keep:
                log.debug(f"{cv_info['Class']} does not match {report_settings[table]['Class']}")
                return False
            # relevant
            relevant_key = "{}|{}".format(raw_info["AA_MatchType"], raw_info["Class"])
            log.debug(f"clinvar match 2 : {relevant_key}")
            comment = " - {}, {} : AA {} : ({}) : {}, Disease: {}".format(
                info["cvid"],
                cv_info["VarType"],
                info["AA_MatchType"],
                cv_info["Gene"],
                cv_info["Class"],
                cv_info["Disease"],
            )
        # if there was a match, return the info
        key = "{}|{}|{}".format(vid, table, relevant_key)
        return [key, vid, comment]
    else:
        log.warning("Argument type {} is not supported yet :".format(event_type))
        return False


def IsMane(transcript, mane_transcripts):
    nm = transcript.split(".")[0]
    if nm in mane_transcripts["mane"] or nm in mane_transcripts["userKB"]:
        return True
    else:
        return False


def ErrorCallback(result):
    log.error(result)
    KillProcessTree(timeout=15, include_parent=True)


if __name__ == "__main__":
    # read config
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # get logger
    log = StartLogger(config, "UpdateMaster")

    # get the annotator object
    annotator = Annotator(config=config, method="Updater", annotation="all")
    # get Annotation Config XML
    normal, delayed = LoadAnnotations()
    # run in parallel at max(4,_mysqlthreads_) threads if local.
    # TODO reimplement HPC processing if needed for performance.
    # track start date.
    if not args.time_stamp:
        time_stamp = date.today().strftime("%Y-%m-%d")
    else:
        log.info(f"using provided time stamp : {args.time_stamp}")
        time_stamp = args.time_stamp
    # track current highest entry in log.
    if not args.last_log_id:
        try:
            last_log_id = annotator.dbh.runQuery(
                "SELECT id FROM `Variants_x_Log` ORDER BY id DESC LIMIT 1"
            )[0]["id"]
        except IndexError:
            last_log_id = 0
        except Exception as e:
            log.error("Failed to get highest log entry : {}".format(e))
            last_log_id = 0
    else:
        last_log_id = args.last_log_id

    log.info(
        "Starting update on date  {} ; latest log entry has ID : {}".format(time_stamp, last_log_id)
    )
    # set Interface to offline.
    annotator.dbh.doQuery(
        "UPDATE `NGS-Variants-Admin{}`.`SiteStatus` SET status = 'Construction' WHERE 1".format(
            annotator.db_suffix
        )
    )

    log.info("Launching normal annotations")
    ## RUN ANNOTATIONS
    p = Pool(processes=8)
    try:
        # map_async with .get() to immediately catch errors as they come
        # eg : item 3 fails , p.map() would wait for 1&2 to finish before detecting it.
        result = p.map_async(
            RunAnnotationUpdates, normal, chunksize=1, error_callback=ErrorCallback
        ).get()

    # this is double check, as the error_callback terminates the process...
    except Exception as e:
        log.error("Failed to run annotations : {}".format(e))
        # kill all children, with a timeout of 15s to wait for them to actually terminate.
        log.warning("Shutting Down Process.")
        # kill process & children.
        KillProcessTree(timeout=15, include_parent=True)

    p.close()
    p.join()

    # delayed. : TODO : implement double dependency if needed (depend on delayed item)
    log.info("Launching delayed annotations")
    p = Pool(processes=8)
    try:

        # map_async with .get() to immediately catch errors as they come
        # eg : item 3 fails , p.map() would wait for 1&2 to finish before detecting it.
        result = p.map_async(
            RunAnnotationUpdates, delayed, chunksize=1, error_callback=ErrorCallback
        ).get()

    except Exception as e:
        log.error("Failed to run annotations : {}".format(e))
        log.warning("Shutting Down Process")
        # kill all children, with a timeout of 15s to wait for them to actually terminate.
        KillProcessTree(timeout=15, include_parent=True)

    p.close()
    p.join()

    ## Clear caches
    annotator.dbh.doQuery(
        "UPDATE `NGS-Variants-Admin{}`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE 1".format(
            annotator.db_suffix
        )
    )

    # set Interface to online.
    annotator.dbh.doQuery(
        "UPDATE `NGS-Variants-Admin{}`.`SiteStatus` SET status = 'Operative' WHERE 1".format(
            annotator.db_suffix
        )
    )
    ## compose emails.
    try:
        summary = ComposeEmails(time_stamp, last_log_id)
    except Exception as e:
        log.error("Failed to compose emails with annotation updates: {}".format(e))
        sys.exit(1)

    # done.
    log.info("Updates finished. Notifying site admin.")
