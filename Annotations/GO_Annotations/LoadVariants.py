from CMG.DataBase import MySqlError
from CMG.Utils import Re
import configparser
import argparse
import os
import sys
import time

# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


re = Re()

# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Annotate all NEW variants with Gene Ontology data & store to DB
            - Annotate all Variants & store to dedicated validation table in DB
            - Annotate a VCF file & store to new VCF file.
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Run in Validation Mode. All variants are annotated, and stored in a separate table.",
    )
    # file mode
    parser.add_argument(
        "-f", "--infile", required=False, help="Input VCF file to use in file-based mode."
    )
    parser.add_argument(
        "-o", "--outfile", required=False, help="Output VCF file to use in file-based mode."
    )
    # overrule log level
    parser.add_argument(
        "-l", "--loglevel", required=False, help="Set the log level : ERROR, WARN, [INFO], DEBUG"
    )
    args = parser.parse_args()
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    # if loglevel set : overrule value in config.
    if args.loglevel:
        config["LOGGING"]["LOG_LEVEL"] = args.loglevel
    return config, args


# Validate the provided options.
def ValidateTables(args, config):
    # data files:
    if not os.path.exists(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GO_Annotations/{}datafiles".format(args.prefix),
            "gene2ensembl",
        )
    ):
        raise DataError("Data File not found: {}datafiles/gene2ensembl".format(args.prefix))
    if not os.path.exists(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GO_Annotations/{}datafiles".format(args.prefix),
            "gene2go",
        )
    ):
        raise DataError("Data File not found: {}datafiles/gene2go".format(args.prefix))

    # the table if not file based
    if args.infile:
        return True
    # validation:
    if args.validate:
        try:
            annotator.dbh.doQuery(
                "DROP TABLE IF EXISTS `{}{}`".format(args.prefix, args.table_name)
            )
            annotator.dbh.doQuery(
                "CREATE TABLE `{}{}` LIKE `{}`".format(
                    args.prefix, args.table_name, args.table_name
                )
            )
            return True
        except Exception as e:
            raise MySqlError(
                "Failed to create validation table : {}{} : {}".format(
                    args.prefix, args.table_name, e
                )
            )
    # no validation
    if not annotator.dbh.tableExists(args.table_name):
        raise MySqlError("Required Table does not exists: {}".format(args.table_name))

    return True


## routine to update ALL variants in ONE panel
def ProcessGO(tmp_table, config):
    log.info("Load datatables")
    # read in data
    e2g = dict()
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GO_Annotations/",
            "{}gene2ensembl".format(annotator.args.prefix),
        ),
        "r",
    ) as fh:
        for line in fh:
            if line.rstrip() == "":
                continue
            cols = line.rstrip().split("\t")
            if int(cols[0]) != 9606:
                continue
            cols[4] = re.sub(r"(ENST\d+)\.*\d*", "\1", cols[4])
            e2g[cols[4]] = cols[1]

    g2go = dict()
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GO_Annotations/",
            "{}gene2go".format(annotator.args.prefix),
        ),
        "r",
    ) as fh:
        for line in fh:
            if line.rstrip() == "":
                continue
            cols = line.rstrip().split("\t")
            if int(cols[0]) != 9606:
                continue
            if cols[1] not in g2go:
                g2go[cols[1]] = ""
            g2go[cols[1]] = "{}{},".format(g2go[cols[1]], cols[2])

    # coded non relevant effects for snpEff:
    e_r = annotator.dbh.runQuery(
        "SELECT id FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_snpEff_Effect' AND Item_Value IN ('DOWNSTREAM','INTERGENIC','UPSTREAM','NONE','.')"
    )
    effects = ",".join([x["id"] for x in e_r])

    log.info("Load snpEff Transcripts for selected variants")
    v_r = annotator.dbh.runQuery(
        "SELECT vid, Transcript FROM `Variants_x_snpEff` vs INNER JOIN `{}` t ON vs.vid = t.vid WHERE Effect NOT IN ({}) GROUP BY vid, Transcript".format(
            tmp_table, effects
        ),
        size=50000,
    )
    load_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/GO_Annotations/tmp_files/",
        "{}.list.txt".format(time.time()),
    )
    log.debug("load_file : {}".format(load_file))
    seen = set()
    with open(load_file, "w") as fh:
        while len(v_r) > 0:
            for row in v_r:
                # skip empty transcripts
                if row["Transcript"] == ".":
                    continue
                if row["Transcript"] not in e2g:
                    continue
                gid = row["Transcript"]
                if gid not in g2go:
                    continue
                for go in g2go[gid][0:-1].split(","):
                    if "{}--{}".format(row["vid"], go) not in seen:
                        seen.add("{}--{}".format(row["vid"], go))
                        fh.write("{},{}\n".format(row["vid"], go))
            v_r = annotator.dbh.GetNextBatch()

    log.info("Loading Database")

    # load into DB.
    annotator.dbh.doQuery(
        "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}{}` FIELDS TERMINATED BY ',' (`vid`,`gos`)".format(
            load_file, args.prefix, args.table_name
        )
    )


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    log = StartLogger(config, "GO")

    # get the annotator object
    annotator = Annotator(config=config, method="GO", annotation="GO", args=args)
    annotator.args.table_name = "Variants_x_GO"

    ## Validate options
    try:
        annotator.ValidateTable()
    except (DataError, ValueError) as e:
        log.error(e)
        sys.exit(1)

    # validate the tables & files.
    try:
        ValidateTables(annotator.args, config)
    except DataError as e:
        log.error("Could not locate necessary resources: {}".format(e))
        sys.exit(1)

    # fill tmp_table
    tmp_table = "Variants_x_GO_tmp_{}".format(time.time())
    log.debug("tmp_table: {}".format(tmp_table))
    try:
        annotator.dbh.doQuery(
            "CREATE TABLE IF NOT EXISTS `{}` (`vid` INT(11) PRIMARY KEY) ENGINE=MEMORY CHARSET=latin1".format(
                tmp_table
            )
        )
    except Exception as e:
        log.error("Could not create tmp table {} : {}".format(tmp_table, e))
        sys.exit(1)
    try:
        if annotator.args.validate:
            # all variants
            annotator.dbh.doQuery(
                "INSERT INTO `{}` (vid) SELECT id FROM `Variants`".format(tmp_table)
            )
        else:
            # new variants
            annotator.dbh.doQuery(
                "INSERT INTO `{}` (vid) SELECT v.id FROM `Variants` v LEFT OUTER JOIN `{}` go ON v.id = gp.vid WHERE gp.aid IS NULL".format(
                    tmp_table, annotator.args.table_name
                )
            )
    except Exception as e:
        log.error("Could not fill tmp table : {}".format(e))
        sys.exit(1)

    # process.
    try:
        ProcessGO(tmp_table, config)
    except Exception as e:
        log.error("Failed to Process GO entries {} : {}".format(e))
        sys.exit(1)

    # clean up.
    try:
        annotator.dbh.doQuery("DROP TABLE `{}`".format(tmp_table))
    except Exception as e:
        log.error("Could not drop table {} : {}".format(tmp_table, e))
        sys.exit(1)
