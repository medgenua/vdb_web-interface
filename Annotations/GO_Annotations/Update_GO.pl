#!/usr/bin/perl

$|++;
use DBI;
use Getopt::Std;

use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);

# opts
# b : genome build (if provided)
# v : validation mode.
getopts('b:v', \%opts) ;

## IN VALIDATION MODE ?
our $validation = 0;
our $table_prefix = '';
if (defined($opts{'v'})) {
	print "\n";
	print "RUNNING ANNOTATIONS IN VALIDATION MODE:\n";
	print "  - All variants will be annotated\n";
	print "  - Annotations are stored in 'Validation_<tablename>' table\n";
	print "  - original annotations are not modified\n";
	print "\n";
	print "press <ctrl>-c to abort\n";
	sleep 5;
	print "\n\n";
	$validation = 1;
	$table_prefix = 'Validation_';
}
## first the ncbi tables
if (-e $table_prefix.'gene2go') {
	system("rm $table_prefix"."gene2go");
}
if (-e $table_prefix.'gene2ensembl') {
	system("rm $table_prefix"."gene2ensembl");
}
my $command = "wget 'ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2go.gz' -O '$table_prefix"."gene2go.gz' && gunzip '$table_prefix"."gene2go.gz' && wget 'ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2ensembl.gz' -O '$table_prefix"."gene2ensembl.gz' && gunzip '$table_prefix"."gene2ensembl.gz'";
system($command);

my $url = 'http://archive.geneontology.org/latest-termdb/go_daily-termdb-tables.tar.gz';

# extract filenames
$url =~ m/http:\/\/.*\/(\S+)\.tar\.gz/;
my $tzfile = "$1.tar.gz";
my $dir = $1;
## get (no prefix, as untar goes to stored dirname.)
system("wget '$url' -O '/tmp/$tzfile' && cd /tmp && tar xzf '$tzfile'");
###########################
# CONNECT TO THE DATABASE #
###########################
# GET current/provided build
our $db = '';
our $dbbuild = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$dbbuild = $cb;
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
	$dbbuild = $opts{'b'};
}

$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

if ($validation == 1) {
	# VALIDATION : annotate all into new table.
	$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."GO_term`");
	$dbh->do("CREATE TABLE `$table_prefix"."GO_term` LIKE `GO_term`");
	
	$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."GO_term2term`");
	$dbh->do("CREATE TABLE `$table_prefix"."GO_term2term` LIKE `GO_term2term`");

	$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."GO_db`");
	$dbh->do("CREATE TABLE `$table_prefix"."GO_db` LIKE `GO_db`");

	$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."GO_dbxref`");
	$dbh->do("CREATE TABLE `$table_prefix"."GO_dbxref` LIKE `GO_dbxref`");

	$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."GO_term_dbxref`");
	$dbh->do("CREATE TABLE `$table_prefix"."GO_term_dbxref` LIKE `GO_term_dbxref`");

	$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."GO_Summary`");
	$dbh->do("CREATE TABLE `$table_prefix"."GO_Summary` LIKE `GO_Summary`");


}

## load tables.
## term
$dbh->do("TRUNCATE TABLE `$table_prefix"."GO_term`");
$dbh->do("LOAD DATA LOCAL INFILE '/tmp/$dir/term.txt' INTO TABLE `$table_prefix"."GO_term`");

## term2term
$dbh->do("TRUNCATE TABLE `$table_prefix"."GO_term2term`");
$dbh->do("LOAD DATA LOCAL INFILE '/tmp/$dir/term2term.txt' INTO TABLE `$table_prefix"."GO_term2term`");

## db
$dbh->do("TRUNCATE TABLE `$table_prefix"."GO_db`");
$dbh->do("LOAD DATA LOCAL INFILE '/tmp/$dir/db.txt' INTO TABLE `$table_prefix"."GO_db`");

## dbxref
$dbh->do("TRUNCATE TABLE `$table_prefix"."GO_dbxref`");
$dbh->do("LOAD DATA LOCAL INFILE '/tmp/$dir/dbxref.txt' INTO TABLE `$table_prefix"."GO_dbxref`");

## term_dbxref
$dbh->do("TRUNCATE TABLE `$table_prefix"."GO_term_dbxref`");
$dbh->do("LOAD DATA LOCAL INFILE '/tmp/$dir/term_dbxref.txt' INTO TABLE `$table_prefix"."GO_term_dbxref`");

## CREATE SUMMARY TABLE 
## brute force :
#	- load all terms into hash
#	- loop hash : foreach term, fetch parental terms and concatenate
my %terms = ();
open IN, "/tmp/$dir/term.txt";
while (<IN>) {
	chomp($_);
	my @p = split(/\t/,$_) ;
	$terms{$p[0]}{'pGO'} = '';
	$terms{$p[0]}{'pTer'} = '';
	$terms{$p[0]}{'GO'} = $p[3];
	$terms{$p[0]}{'Ter'} = $p[1];
	$terms{$p[0]}{'type'} = $p[2];
	$terms{$p[0]}{'obs'} = $p[4];
}
close IN;
open IN,  "/tmp/$dir/term2term.txt";
while (<IN>) {
	chomp($_);
	my @p = split(/\t/,$_) ;
	## only is_a is relevant
	if ($p[1] != 1) {
		next;
	}
	# p[3] is child; $p[2] is parent.
	$terms{$p[3]}{'pGO'} .= $terms{$p[2]}{'GO'}.";";
	$terms{$p[3]}{'pTer'} .= $terms{$p[2]}{'Ter'}.";";
}	
close IN;

open OUT, ">/tmp/$dir/parental.txt";
foreach(keys(%terms)) {
	if ($terms{$_}{'pGO'} ne '') {
		$terms{$_}{'pGO'} = substr($terms{$_}{'pGO'},0,-1);
		$terms{$_}{'pTer'} = substr($terms{$_}{'pTer'},0,-1);
	}
	print OUT $terms{$_}{'GO'} . "\t" .$terms{$_}{'Ter'} . "\t" .$terms{$_}{'type'} . "\t" .$terms{$_}{'obs'}."\t".$terms{$_}{'pGO'} . "\t" .$terms{$_}{'pTer'} . "\n";
}
close OUT;
$dbh->do("TRUNCATE TABLE `$table_prefix"."GO_Summary`");
$dbh->do("LOAD DATA LOCAL INFILE '/tmp/$dir/parental.txt' INTO TABLE `$table_prefix"."GO_Summary`");

## clean
if ($dir ne '') {
	system("cd /tmp && rm -Rf $dir*");
}
exit();
