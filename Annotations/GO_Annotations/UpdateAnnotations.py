from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, AutoVivification, CheckVariableType
from CMG.DataBase import MySqlError
from CMG.HPO.OBO import Parser
import configparser
import argparse
import os
import subprocess
import sys

# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger

import time
from datetime import date
import subprocess
import glob
import tempfile
import urllib.request
import hashlib
import shutil
import ftplib
import shelve


class DataError(Exception):
    pass


## UNFINISHED :
##   => INPUT FORMAT NO LONGER ACCESSIBLE. Need to revise to build

## some variables/objects
re = Re()  # customized regex-handler


def ConvertChr(v):
    chrom_dict = {x: x for x in range(1, 23)}
    chrom_dict.update({"X": 23, 23: "X", "Y": 24, 24: "Y", "M": 25, "MT": 25, 25: "M"})
    return chrom_dict[v]


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Evaluate if a new annotatation release is present
            - Run annotator on all Variants if new release & store to dedicated validation table in DB
            - Compare current and new annations, notify users if relevant items were found. 
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # annotation type
    # parser.add_argument("-a", "--anno", required=True, help="Annotation Type")
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    # download only mode
    parser.add_argument(
        "-d",
        "--download",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Download new release only into Validation_ tables.",
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Validation Mode: No data download, just reannotation and results are saved to files instead of emailed",
    )

    args = parser.parse_args()
    # set prefix to validation during update.
    args.prefix = "Validation_"

    return config, args


# Validate the provided options.
def ValidateTables(args, config):
    for table in ["GO_term", "GO_term2term", "GO_db", "GO_dbxref", "GO_term_dbxref", "GO_Summary"]:
        if not annotator.dbh.tableExists(table):
            raise MySqlError("Required Table does not exists: {}".format(table))
        # if validation :  create new table.
        if args.validate:
            annotator.dbh.doQuery(
                "DROP TABLE IF EXISTS `{}{}`".format(annotator.args.prefix, table)
            )
            annotator.dbh.doQuery(
                "CREATE TABLE `{}{}` LIKE `{}`".format(annotator.args.prefix, table, table)
            )

    return True


def GetFileMD5(file):
    # md5 file exists : read content.
    if os.path.exists("{}.md5".format(file)):
        with open("{}.md5".format(file), "r") as fh:
            md5 = fh.readline().rstrip().split("\t")[0]
    # else : create the contents.
    else:
        with open(file, "rb") as f:
            file_hash = hashlib.md5()
            while chunk := f.read(8192):
                file_hash.update(chunk)
        md5 = file_hash.hexdigest()

        with open("{}.md5".format(file), "w") as fh:
            fh.write(file_hash.hexdigest())

    return md5


def CompareResults():

    # open all output files.
    time_stamp = date.today().strftime("%Y-%m-%d")
    # open log loading file handle:
    out_files = dict()
    out_files["LOG"] = open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GO_Annotations/Output_Files",
            "Load.Log.GO.{}.txt".format(time_stamp),
        ),
        "w",
    )

    for vtype in ["NOVEL", "LOST"]:
        out_files[vtype] = open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/GO_Annotations/Output_Files/",
                "Validation.{}.{}.{}".format(annotator.args.anno, time_stamp, vtype),
            ),
            "w",
        )

    # work in slices of 50K variants :
    start_vid = 0
    nr_ok = 0
    nr_lost = 0
    nr_new = 0
    while True:
        # get last vid.
        vids = annotator.dbh.runQuery(
            "SELECT id FROM `Variants` WHERE id >= %s ORDER BY `id` LIMIT 50000", start_vid
        )
        if not vids:
            # no data left.
            break
        last_vid = vids[-1]["id"]
        log.info("Working on 50K variants with VIDS from {} to {}".format(start_vid, last_vid))
        vids = []

        # get old annotations
        log.info("Fetching old annotations")
        rows = annotator.dbh.runQuery(
            "SELECT `aid`, `vid`, `gos` FROM `{}` WHERE vid BETWEEN %s AND %s".format(
                annotator.args.table_name
            ),
            [start_vid, last_vid],
            as_dict=True,
        )
        old_annotations = AutoVivification()
        for row in rows:
            # hash[vid][aid]
            old_annotations[row["vid"]][row["aid"]] = row["gos"]

        # get new annotations & compare
        log.info("Fetching and comparing new annotations.")
        rows = annotator.dbh.runQuery(
            "SELECT `aid`, `vid`, `gos` FROM `{}{}` WHERE vid BETWEEN %s AND %s".format(
                annotator.args.prefix, annotator.args.table_name
            ),
            [start_vid, last_vid],
            as_dict=True,
        )
        for row in rows:
            vid = row["vid"]
            # present in old_annotations?
            annotation_found = False
            for old_aid in old_annotations[vid]:
                if old_annotations[vid][old_aid] == row["gos"]:
                    annotation_found = True
                    # remove matched old annotation.
                    del old_annotations[vid][old_aid]
                    break

            # annotation found :
            if annotation_found:
                nr_ok += 1
                # process next row.
                continue

            # annotation not found : novel entry
            nr_new += 1
            if annotator.args.validate:
                out_files["NOVEL"].write("{}\n".format("; ".join(ListOfStrings(row))))
            else:
                # copy to main table.
                new_aid = annotator.dbh.insertQuery(
                    "INSERT INTO `{}` (`vid`,`gos`) VALUES (%s,%s)".format(
                        annotator.args.table_name
                    ),
                    [row["vid"], row["gos"]],
                )
                # add to log table.
                out_files["LOG"].write(
                    "{}\t{}\tVariants_x_GO:{}\t2\n".format(vid, annotator.args.userid, new_aid)
                )

        # any left on the old_annotations stack : these are lost.
        for vid in old_annotations:
            for aid in old_annotations[vid]:
                if old_annotations[vid][aid]:
                    nr_lost += 1
                    # validate : to files
                    if annotator.args.validate:
                        out_files["LOST"].write(
                            "{}\t{}\t{}\n".format(vid, aid, old_annotations[vid][aid])
                        )
                        nr_lost += 1
                    # else : to log table
                    else:
                        contents = "gos|||{}".format(old_annotations[vid][aid])
                        aaid = annotator.dbh.insertQuery(
                            "INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (%s,'Variants_x_GO',%s,%s,%s)",
                            [vid, f"-{annotator.args.build}", time_stamp, contents],
                        )
                        out_files["LOG"].write(
                            "{}\t{}\tVariants_x_GO:{}\t1\n".format(vid, annotator.args.userid, aaid)
                        )

        # clear
        old_annotations.clear()
        # next iteration.
        start_vid = last_vid + 1
        # DEV : break
        break
    # overview.
    log.info("STATISTICS:")
    log.info("  NOVEL ENTRY : {}".format(nr_new))
    log.info("  DISCARDED   : {}".format(nr_lost))

    # close file handles
    for fh in out_files:
        out_files[fh].close()

    # load to log-table.
    if not annotator.args.validate:
        annotator.dbh.doQuery(
            "LOAD DATA LOCAL INFILE '{}' INTO TABLE `Variants_x_Log` (`vid`, `uid`, `entry`, `arguments`)".format(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/GO_Annotations/Output_Files",
                    "Load.Log.GO.{}.txt".format(time_stamp),
                )
            )
        )


def ListOfStrings(data):
    return [str(x) for x in data]


def DownloadGO():
    # create output folder:
    if not os.path.isdir("{}datafiles".format(annotator.args.prefix)):
        os.makedirs("{}datafiles".format(annotator.args.prefix))
    # from ncbi
    dir = "gene/DATA"
    ncbi = ftplib.FTP("ftp.ncbi.nlm.nih.gov")
    ncbi.login("anonymous", "")
    for f in ["gene2go.gz", "gene2ensembl.gz"]:
        try:
            log.info("Retrieving : {}".format(f))
            outfile = os.path.join("{}datafiles".format(annotator.args.prefix), f)
            ncbi.retrbinary("RETR {}".format(os.path.join(dir, f)), open(outfile, "wb").write)
            subprocess.check_call(["gunzip", outfile])
        except Exception as e:
            raise e
    # from GO
    url = "http://purl.obolibrary.org/obo/go.obo"
    # plain download (for md5sums)
    try:
        log.info("Retrieving GO.obo")
        subprocess.check_call(
            [
                "wget",
                "-O",
                os.path.join("{}datafiles".format(annotator.args.prefix), "go.obo"),
                url,
            ]
        )
    except Exception as e:
        raise e


# returns true if identical, false in any other case
def CheckMD5():
    # file missing
    if not os.path.exists(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"], "Annotations/GO_Annotations/datafiles/md5.txt"
        )
    ):
        return False
    # copy from production to validation:
    infile = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"], "Annotations/GO_Annotations/datafiles/md5.txt"
    )
    outfile = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/GO_Annotations/{}datafiles/md5.txt".format(annotator.args.prefix),
    )
    try:
        subprocess.check_call(["cp", "-f", infile, outfile])
    except Exception as e:
        log.error("Could not cp md5sum file: {}".format(e))
        sys.exit(1)

    # compare
    try:
        subprocess.check_output(
            [
                "md5sum",
                "-c",
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/GO_Annotations/{}datafiles/md5.txt".format(annotator.args.prefix),
                ),
            ]
        )
        return True
    except Exception as e:
        log.info("Checksums did not match, new release available.")
        return False


def SummarizeGO():
    # use a shelve to reduce memory footprint.
    sf = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/GO_Annotations/{}datafiles".format(annotator.args.prefix),
        "{}.{}.shelf".format(annotator.args.table_name, time.time()),
    )
    s = shelve.open("{}datafiles/shelve".format(annotator.arg.prefix))
    # read all terms.
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GO_Annotations/{}datafiles".format(annotator.arg.prefix),
            "go_daily-termdb-tables",
            "term.txt",
        ),
        "r",
    ) as fh:
        for line in fh:
            cols = line.rstrip().split("\t")
            s[cols[0]] = {
                "pGO": "",
                "pTer": "",
                "GO": cols[3],
                "Ter": cols[1],
                "type": cols[2],
                "obs": cols[4],
            }
    # read links between terms to find 'is_a' for parental links.
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GO_Annotations/{}datafiles".format(annotator.arg.prefix),
            "go_daily-termdb-tables",
            "term2term.txt",
        ),
        "r",
    ) as fh:
        for line in fh:
            cols = line.rstrip().split("\t")
            if not cols[1] == "1":
                continue
            s[cols[3]]["pGO"] += s[cols[2]]["GO"] + ";"
            s[cols[3]]["pTer"] += s[cols[2]]["Ter"] + ";"
    # write out for DB loading.
    with open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/GO_Annotations/{}datafiles".format(annotator.arg.prefix),
            "go_daily-termdb-tables",
            "parental.txt",
        ),
        "w",
    ) as fh:
        for k in s:
            if s[k]["pGO"]:
                fh.write(
                    "{}\t{}\t{}\t{}\t{}\t{}\n".format(
                        s[k]["GO"],
                        s[k]["Ter"],
                        s[k]["type"],
                        s[k]["obs"],
                        s[k]["pGO"].rstrip(";"),
                        s[k]["pTer"].rstrip(";"),
                    )
                )
    # load.
    annotator.dbh.doQuery(
        "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}GO_Summary`".format(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/GO_Annotations/{}datafiles".format(annotator.arg.prefix),
                "go_daily-termdb-tables",
                "parental.txt",
            ),
            annotator.args.prefix,
        )
    )
    # close & cleanup shelve
    s.close()
    os.remove(sf)


# parse GO
def ParseGO():
    files = dict()
    for n in ["term", "term2term", "db", "dbxref", "term_dbxref"]:
        files[n] = open(os.path.join("{}datafiles".format(annotator.args.prefix), f"{n}.txt"), "w")

    url = "http://purl.obolibrary.org/obo/go.obo"
    go_obo = Parser(url)
    import pprint

    for go_id, data in go_obo.terms.items():
        pprint.pprint(go_id)
        pprint.pprint(data)
        print("")
        *_, id = go_id.split(":")
        # short name
        if "name" in data:
            name = data["name"]
        else:
            log.warning("Term {} doesn't seem to have a name, skipping...".format(go_id))
            continue
        if "definition" in data:
            definition = data["definition"]
        else:
            definition = None
        if "comment" in data:
            comment = ";".join(data["comment"])
        else:
            comment = None
        if "xref" in data:
            xref = ",".join(data["xref"])
        else:
            xref = None
        if "synonyms" in data:
            synonyms = ",".join(data["synonyms"])
        else:
            synonyms = None
        if "isa" in data:
            isa = data["isa"]
        else:
            isa = None
    ## CONTINUE HERE : PICK RELEVANT FIELDS & STORE.
    sys.exit(0)


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
        # hardcoded
        args.anno = "GO"
        args.table_name = "Variants_x_GO"
        import pprint

        pprint.pprint(args)
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)
    log = StartLogger(config, "ANNOVAR")

    # get the annotator object
    annotator = Annotator(config=config, method="GO", annotation="GO", args=args)

    # validate the tables & files.
    try:
        # general validation
        annotator.ValidateTable()
        # specific requirements
        ValidateTables(annotator.args, config)
    except DataError as e:
        log.error("Could not locate necessary resources: {}".format(e))
        sys.exit(1)

    # get userid of admin user (for archive logs)
    try:
        annotator.args.userid = annotator.dbh.runQuery(
            "SELECT id FROM `Users` WHERE `email` = %s", [config["USERS"]["ADMIN"]]
        )[0]["id"]
    except Exception as e:
        log.error("Admin email not recognized by VariantDB : {}".format(e))
        sys.exit(1)

    ## END HERE AS PARSER AND UPDATER IS NOT FINISHED
    log.warning("GO updating is not supported yet.")
    log.warning("Process ends here !")
    sys.exit(0)

    # check / download new release
    if not args.validate:
        # get latest release
        try:
            DownloadGO()

        except Exception as e:
            log.error("Failed to download GO release: {}".format(e))
            sys.exit(1)
        annotate = CheckMD5()
    else:
        annotate = True

    annotate = True
    # run the annotation procedures if md5s didn't match
    if annotate and not args.download:
        ParseGO()

        # create summary table.
        try:
            log.info("Summarizing GO tables")
            SummarizeGO()
        except Exception as e:
            log.error("Failed to summarize GO database:  {}".format(e))
            sys.exit(1)

        # run the annotator.
        try:
            log.info("Running annotations.")
            subprocess.check_output(["python", "LoadVariants.py", "-v"])
        except Exception as e:
            log.error("Failed to run annotator for GO: {}".format(e))
            sys.exit(1)

        # compare results.
        try:
            CompareResults(args)
        except Exception as e:
            log.error("Failed to process GO differences after update: {}".format(e))
            sys.exit(1)

        # put the datafiles in place.

        # copy results to production tables.

        # put new md5 in place (TODO)
        try:
            subprocess.check_output(
                [
                    "md5sum",
                    "-c",
                    os.path.join(
                        config["LOCATIONS"]["SCRIPTDIR"],
                        "Annotations/GO_Annotations/datafiles/md5.txt",
                    ),
                ]
            )
        except Exception as e:
            log.error("Failed to create final md5sum (for new release) : {}".format(e))

# cleanup
