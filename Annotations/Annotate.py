from CMG.DataBase import MySQL, MySqlError
from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, AutoVivification, CheckVariableType, KillProcessTree
import configparser
import argparse
import os
import sys
import time
import subprocess
import glob
import xml.etree.ElementTree as ET
from multiprocessing import Pool

from pprint import pprint


class DataError(Exception):
    pass


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Run Annotation updates on new variants
            
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )

    args = parser.parse_args()

    return config, args


def GetSamples():
    rows = dbh.runQuery("SELECT id FROM `Samples` WHERE AnnotationStatus = 'Pending'")
    global sids
    sids = [r["id"] for r in rows]
    if not sids:
        return
    date_str = time.strftime("%Y.%m.%d_%H:%M:%S")
    SetSampleStatus("Started")
    return


def LoadAnnotations():
    # track with sets to remove duplicates (multiple items from one source/table)
    normal = set()
    delayed = set()
    # load config.
    annotations_config = ET.parse(
        os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations/Config.xml")
    )
    a_root = annotations_config.getroot()
    # first level == subdirectories in Annotations folder.
    for subdir in a_root:
        log.info(subdir.tag)
        # not all are relevant
        if (
            not os.path.isdir(
                os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", subdir.tag)
            )
            or subdir.tag == "GATK_Annotations"
        ):
            log.info(" => skipped")
            continue

        # second level: annotation to work on (-a xxx)
        for anno in subdir:
            log.info("   => {}".format(anno.tag))
            # skip if deprecated
            if ET.iselement(anno.find("deprecated")):
                log.info("      => skipped/depr")
                continue
            # skip if no annotation source specified
            if not ET.iselement(anno.find("AnnotationSource")):
                log.info("      => skipped/noSource")
                continue
            # delayed ?
            if ET.iselement(anno.find("depend")):
                delayed.add(
                    "{}|{}|{}".format(
                        subdir.tag, anno.find("AnnotationSource").text, anno.find("depend").text
                    )
                )
            else:
                # normal
                normal.add("{}|{}".format(subdir.tag, anno.find("AnnotationSource").text))

    return normal, delayed


def RunAnnotations(job):
    subfolder, source, *dep = job.split("|")
    log.info("Running anntations {} : {} ".format(subfolder, source))
    try:
        cmd = "cd {} ; python LoadVariants.py -a {}".format(
            os.path.join(config["LOCATIONS"]["SCRIPTDIR"], "Annotations", subfolder), source
        )
        # run silently
        subprocess.check_call(cmd, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        log.info(" {} : done".format(job))
        # return
        return (None, job)
    except Exception as e:
        raise e
        # return ("{} : {}".format(job, e), None)


def ErrorCallback(result):
    log.error(result)
    # set samples to failed
    global sids
    SetSampleStatus("Failed")
    KillProcessTree(timeout=15, include_parent=True)


# SET SAMPLE STATUS
def SetSampleStatus(status):
    global sids
    try:
        date_str = time.strftime("%Y.%m.%d_%H:%M:%S")
        dbh.doQuery(
            "UPDATE `Samples` SET AnnotationStatus = '{} on {}' WHERE id IN ('{}')".format(
                status, date_str, "','".join([str(x) for x in sids])
            )
        )

    except Exception as e:
        log.error(
            "Failed to update Sample Status to {} for sids {}".format(
                status, ",".join([str(x) for x in sids])
            )
        )
        sys.exit(1)


## MAIN ROUTINE
if __name__ == "__main__":

    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    try:
        if os.path.isfile(config["LOGGING"]["LOG_PATH"]):
            config["LOGGING"]["LOG_PATH"] = os.path.dirname(config["LOGGING"]["LOG_PATH"])
        os.makedirs(config["LOGGING"]["LOG_PATH"], exist_ok=True)
        # start logger
        msg = "Logging to : %s" % config["LOGGING"]["LOG_PATH"]
    except:
        config["LOGGING"]["LOG_PATH"] = os.path.expanduser("~/python_logs")
        msg = (
            "Could not create specified logging path: Logging to : %s"
            % config["LOGGING"]["LOG_PATH"]
        )
    # setup logging.
    setup_logging(
        name="VariantDB_Master_Annotator",
        level=config["LOGGING"]["LOG_LEVEL"],
        log_dir=config["LOGGING"]["LOG_PATH"],
        to_addrs=config["LOGGING"]["LOG_EMAIL"],
    )
    log = get_logger("main")
    log.info(msg)

    try:
        db_suffix = config["DATABASE"].get("DBSUFFIX", "")
        if db_suffix:
            db_suffix = f"_{db_suffix}"
        dbh = MySQL(
            user=config["DATABASE"]["DBUSER"],
            password=config["DATABASE"]["DBPASS"],
            host=config["DATABASE"]["DBHOST"],
            database=f"NGS-Variants-Admin{db_suffix}",
            allow_local_infile=True,
        )
        row = dbh.runQuery("SELECT `name`, `StringName` FROM `CurrentBuild`")
        db = "NGS-Variants{}{}".format(row[0]["name"], db_suffix)
        # use statement doesn't work with placeholder. reason unknown
        dbh.select_db(db)
        log.info("Connected to Database using Genome Build {}".format(row[0]["StringName"]))
    except Exception as e:
        log.error("Could not connect to VariantDB Database : {}".format(e))
        sys.exit(1)

    # Get samples to process.
    # a global variable is used to be accessible in failure routines
    global sids
    GetSamples()
    if not sids:
        log.info("No samples to update. Shutting down")
        sys.exit(0)

    # get Annotation Config XML
    normal, delayed = LoadAnnotations()
    # run in parallel at max(4,_mysqlthreads_) threads if local.
    # TODO reimplement HPC processing if needed for performance.

    ## RUN ANNOTATIONS
    p = Pool(processes=4)
    # output = [p.apply_async(RunAnnotations, args=(x,)) for x in normal]
    # p.close()
    try:
        output = p.map_async(
            RunAnnotations, normal, chunksize=1, error_callback=ErrorCallback
        ).get()
        # Monitor(output)
    except Exception as e:
        log.error("Failed to run annotations : {}".format(e))
        SetSampleStatus("Failed")
        log.warning("Killing children.")
        KillProcessTree(include_parent=True)
    p.close()
    p.join()

    # delayed. : TODO : implement double dependency if needed (depend on delayed item)
    p = Pool(processes=4)
    # output = [p.apply_async(RunAnnotations, args=(x,)) for x in delayed]
    # p.close()
    try:
        output = p.map_async(
            RunAnnotations, delayed, chunksize=1, error_callback=ErrorCallback
        ).get()
    except Exception as e:
        log.error("Failed to run annotations : {}".format(e))
        SetSampleStatus("Failed")
        KillProcessTree(timeout=15, include_parent=True)
    p.close()
    p.join()

    # SET SAMPLE STATUS
    SetSampleStatus("Finished")
