#!/usr/bin/perl
$|++;
use DBI;
use Getopt::Std;

use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);

###########################
# CONNECT TO THE DATABASE #
###########################
my $db = "NGS-Variants-hg19";
$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

## DOWNLOAD LATEST RELEASE
#my $url = "http://compbio.charite.de/hudson/job/hpo.annotations.monthly/ws/ontology/human-phenotype-ontology.obo";
my $url = "http://purl.obolibrary.org/obo/hp.obo";
`wget -O latest.obo "$url"`;
#print "wget -O latest.obo \"$url\"\n";
## compare checksums
my $prevmd5 = '';
if (-e "previous.obo") {
	$prevmd5 = `md5sum previous.obo`;
	$prevmd5 =~ s/(.*)\sprevious\.obo.*/$1/;
}
$latestmd5 = `md5sum latest.obo`;
$latestmd5 =~ s/(.*)\slatest\.obo.*/$1/; 
if ("$prevmd5" eq "$latestmd5") {
	print "No changes since last update. program will exit.\n";
	exit();
}

## prepare queries
$termsth = $dbh->prepare("INSERT DELAYED INTO HPO_Terms (id, Name, Definition, Comment, xref) VALUES (?,?,?,?,?)");
$synsth = $dbh->prepare("INSERT DELAYED INTO HPO_Synonyms (tid, Synonym) VALUES (?,?)");
$relsth = $dbh->prepare("INSERT DELAYED INTO HPO_Term_x_Term (tid1, tid2) VALUES (?,?)");

## scan the file for first entry
open IN, "latest.obo";
while (<IN>) {
	if ($_ =~ m/\[Term\]/) {
		last;
	}
}

## clear tables
$dbh->do("TRUNCATE TABLE `HPO_Synonyms`");
$dbh->do("TRUNCATE TABLE `HPO_Terms`");
$dbh->do("TRUNCATE TABLE `HPO_Term_x_Term`");

## store entries
my $tid = 0;
my $tname = '';
my $tdef = '';
my $tcomment = '';
my $xrefs = '';
my @synonyms = ();
my @isa = ();
while (<IN>) {
	chomp;
	next if ($_ eq '');
	# new entry, store previous
	if ($_ =~ m/\[Term\]/) {
		# HPO_Terms
		$termsth->execute($tid,$tname,$tdef,$tcomment,$xrefs);
		# HPO_Synonyms
		foreach (@synonyms) {
			$synsth->execute($tid,$_);
		}
		# HPO_Term_x_Term
		foreach (@isa) {
			$relsth->execute($tid,$_);
		}
		# clear
		$tid = 0;
		$tname = '';
		$tdef = '';
		$tcomment = '';
		$xrefs = '';
		@synonyms = ();
		@isa = ();
		next;
	}
	# id
	if ($_ =~ m/^id:\sHP:0*(\d+)$/) {
		$tid = $1;
	}
	# name
	elsif ($_ =~ m/^name:\s(.*)$/) {
		$tname = $1;
		push(@synonyms,$tname);
	}
	# definition
	elsif ($_ =~ m/^def:\s\"(.*)\".*$/) {
		$tdef = $1;
	}
	# comment 
	elsif ($_ =~ m/^comment:\s(.*)$/) {
		$tcomment = $1;
	}

	# synonyms
	elsif ($_ =~ m/^synonym:\s\"(.*)\"\s.*$/) {
		push(@synonyms,$1);
	}
	# xref
	elsif ($_ =~ m/^xref:\s(.*)$/) {
		$xrefs .= "$1,";
	}
	# is_a
	elsif ($_ =~ m/^is_a:\sHP:0*(\d+)\s.*$/) {
		push(@isa,$1);
	}
}	
## insert last term.	
# HPO_Terms
$termsth->execute($tid,$tname,$tdef,$tcomment,$xrefs);
# HPO_Synonyms
foreach (@synonyms) {
	$synsth->execute($tid,$_);
}
# HPO_Term_x_Term
foreach (@isa) {
	$relsth->execute($tid,$_);
}

## close queries
$termsth->finish();
$synsth->finish();
$relsth->finish();

## move obo file to previous obo for next run.
system("mv latest.obo previous.obo");	
