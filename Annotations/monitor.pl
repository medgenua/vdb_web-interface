#!/usr/bin/perl
use DBI;
use Cwd 'abs_path';
$credpath = abs_path("../.LoadCredentials.pl");
require($credpath);

## print pid.
open OUT, ">$scriptdir/Query_Logs/monitor.pid";
print OUT $$;
close OUT;
system("chmod 755 $scriptdir/Query_Logs/monitor.pid");

## variables
my $sleeptime = 60 ; # run once every minute (GenePanels), once per hour for global new variants.
my $counter = 59; 
$| = 1;
## infinite loop
while ( 1 == 1 ) {
	# read Panel.Update.txt
	my $p = `cat update.panel.txt`;
	system("rm update.panel.txt && touch update.panel.txt");	
	my @panels = split(/\n/,$p);
	my %done = ();
	system("echo 1 > panel.update.running.txt");
	foreach(@panels) {
		chomp($_);
		next if ($_ eq '');
		if (defined($done{$_})) {
			next;
		}
		print "Updating $_\n";
		my $command = "cd GenePanels && perl LoadVariants.pl -g $_";
		system("$command");
		$done{$_} = 1;
		print " => $_ done.\n";
	}
	system("echo 0 > panel.update.running.txt");
	$counter++;
	## also once per hour, but at the 30m interval.
	if ($counter == 30) {
		my @d = `cat ../Query_Results/.delete_queue`;
		system("echo '' > ../Query_Results/.delete_queue");
		my %h = ();
		chomp(@d);
		foreach(@d) {
			next if ($_ eq '') ;
			my @sids = split(/,/,$_);
			foreach(@sids) {
				$h{$_} = 1;
			}
		}
		if (keys(%h) > 0) {
			# delete in 50K slices. if result-nr < 50K, it was the last slice.
			my $nr = 50000;
			$sidstring = join(",",keys(%h));
			while ($nr == 50000) {
				my $dbh = &GetDB;
				if ($dbh) {
					print "Deleting from Variants_x_Sample\n";
					$nr = $dbh->do("DELETE FROM `Variants_x_Samples` WHERE sid IN ($sidstring) LIMIT 50000 ");
				}
				# fully disconnect to give other queries chance to lock the tables.
				$dbh->disconnect();
			}
			# delete from remaining tables		
			for (my $chr = 1; $chr <= 25 ; $chr++) {
				print "Deleting from Variants_x_Reference_Blocks_chr$chr\n";
				my $nr = 50000;
				while ($nr == 50000) {
					my $dbh = &GetDB;
					$nr = $dbh->do("DELETE FROM `Samples_x_Reference_Blocks_chr$chr` WHERE sid IN ($sidstring) LIMIT 50000 ");
					$dbh->disconnect();
				}
			}
			my $dbh = &GetDB;
				
			$dbh->do("DELETE FROM `Log` WHERE sid IN ($sidstring)");
			$dbh->do("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` IN ('Log','Variants_x_Samples')");
			$dbh->do("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` LIKE 'Samples_x_Reference_Blocks_%'");

			
			$dbh->disconnect();	
					
		}
	}
	## once per hour.
	if ($counter == 60) {
		# read update.txt
		my $update = `cat update.txt`;
		chomp($update) ;
		## run annotate.pl if galaxy has put a '1' in the file.
		if ($update == 1) {
			## first reset (if galaxy updates during the annotation run, it will be seen in the next round)
			system("echo 0 > update.txt");
			## run annotate.pl
			system("nohup perl Annotate.pl >> Annotate.log 2>&1");
		
		}
		$counter = 0;
	}
	
	## sleep 
	print "Going to sleep\n";
	sleep $sleeptime;
	
}


sub GetDB {
	## set build to current  build if not provided
	my $db = "NGS-Variants-Admin";
	$connectionInfo="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfo,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
	}
	if (!defined($opts{'b'}) || $opts{'b'} eq '') {
		my $sth = $dbh->prepare("SELECT name FROM `CurrentBuild` ");
		$sth->execute();
		my ($currentdb) = $sth->fetchrow_array();
		$sth->finish();
		$db = "NGS-Variants$currentdb";
		$dbh->disconnect();
	}
	else {
		$dbh->disconnect();
		$db = "NGS-Variants".$opts{'b'};
	}
	$connectionInfo="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	
	## connect to the correct build database.
	$dbh = DBI->connect($connectionInfo,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfo,$userid,$userpass) ;
	}
	if (!defined($dbh)) {
		print "Could not connect to $host....\n";
	}
	return($dbh);



}
