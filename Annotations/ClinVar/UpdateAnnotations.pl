#!/usr/bin/perl
###########################################
## UPDATES CLINVAR IN THE MONTHLY UPDATE ##
###########################################

$|++;
use DBI;
use Getopt::Std;
use Scalar::Util qw(looks_like_number);
use Math::BigFloat;
use Storable; 
use Data::Dumper;
# opts
# b : genome build (if provided)
getopts('b:', \%opts) ;

use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);
if ($usemc == 1) {
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	my $mcpath = abs_path("../../cgi-bin/inc_memcache.pl");
	require($mcpath);	
	#use JSON;
	#our $memd = new Cache::Memcached { 'servers' => \@mcs, 'debug' => 0, 'compress_threshold' => 1_000_000, };
}


my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
our $datestring = ($year+1900)."-".$mon."-".$mday;

## USE VALIDATION MODE TO UPDATE ALL VARIANTS.
our $validation = 1;
our $table_prefix = 'Validation_';

##############
## OVERVIEW ##
##############
# 1. Get current annotations => store to file (store/retrieve)
# 2. Update ClinVar to latest release : use 'UpdateClinVar.pl' script for this.
# 3. Repopulate ClinVar_DB
# 4. Get New annotations + compare 


####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{'MT'} = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";
$annovariants = [];
$allvariants = [];
@allvids = ();
%second = ();

###########################
# CONNECT TO THE DATABASE #
###########################
## GET current/provided build
our $db = '';
our $dbbuild = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$dbbuild = $cb;
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
	$dbbuild = $opts{'b'};
}

$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

## get userid from admin user
$sth = $dbh->prepare("SELECT id FROM `Users` WHERE email = '$adminemail'");
my $nr = $sth->execute();
if ($nr == 0 ) {
	$sth->finish();
	$sth = $dbh->prepare("SELECT id FROM `Users` WHERE level = 3 ORDER BY id LIMIT 1");
	$nr = $sth->execute();
	if ($nr == 0) {
		die("No Admin user found\n");
	}
}
my ($uid) = $sth->fetchrow_array();
$sth->finish();


#####################################
## 1. Load & Store old annotations ##
#####################################
print "Loading current annotations\n";
my $fileidx = 1;
our %sidlist = ();
while (-e "$scriptdir/Annotations/Update.Files/Variants.$fileidx.txt") {
	$fileidx++;
	
}
#=cut
############################
## DOWNLOAD NEW DATATABLE ##
############################

system("cd '$scriptdir/Annotations/ClinVar/' && perl UpdateClinVar.pl -v");

#=cut
###########################
## ANNOTATE ALL VARIANTS ##
###########################
system("cd '$scriptdir/Annotations/ClinVar/' && perl LoadVariants.pl -v");

#############################
## load annotation codings ##
#############################

print "Loading accessory hashes\n";
#our %codes = ();
our %ccodes = ();
my $sth = $dbh->prepare("SELECT id, Table_x_Column,Item_Value FROM `Value_Codes` WHERE Table_x_Column LIKE 'Variants_x_ClinVar%'");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	#if ($r[1] =~ m/Variants_x_ClinVar_(.*)/);
		
	#$codes{$r[1]."_".$r[2]} = $r[0];
	$ccodes{$r[1]}{$r[2]} = $r[0];
}
$sth->finish();
## get build replacement values
my %builds = ();
my $sth = $dbh->prepare("SELECT Annotation, Value FROM `Annotation_DataValues`");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$builds{$r[0]} = $r[1];
}
$sth->finish();
my $cvdb = $builds{'ClinVar'};
# Load XRef links
$xquery = "SELECT id, `XRef_Key`,`HRef_Name`, `XRef_url`,`ValueRegEx` FROM `ClinVar_XRefs`";
my $sth = $dbh->prepare($xquery);
$sth->execute();
my $rowcache = $sth->fetchall_arrayref();
$sth->finish();
our %RefHash = ();
foreach(@$rowcache) {
	$RefHash{$_->[1]}{'HREF'} = $_->[2];
	$RefHash{$_->[1]}{'URL'} = $_->[3];
	$RefHash{$_->[1]}{'RegEx'} = $_->[4];
}
our @replace = ("a".."z");
unshift(@replace,'');


########################################
## LOOP THE VARIANT FILES AND PROCESS ##
########################################
######################
## 1. SNV matching. ##
######################
print "PROCESS SNV MATCHES\n";
$fileidx = 1;
my @discard;
my $nrok = $nrfail = $nrnew = $missingfield = 0;
$log = $dbh->prepare("INSERT DELAYED INTO `Variants_x_Log` (vid, uid, entry, arguments) VALUES (?,'$uid',?,?)");
$archive = $dbh->prepare("INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (?,'Variants_x_ClinVar','$dbbuild','$datestring',?)");
#$sids = $dbh->prepare("SELECT sid FROM `Variants_x_Samples` WHERE vid = ?");
$del = $dbh->prepare("DELETE LOW_PRIORITY FROM `Variants_x_ClinVar` WHERE aid = ? AND vid = ?");
%sidlist = ();
while (-e "$scriptdir/Annotations/Update.Files/Variants.$fileidx.txt") {
	my $l = `head -n 1 $scriptdir/Annotations/Update.Files/Variants.$fileidx.txt`;
	chomp($l);
	my @p = split(/\t/,$l);
	my $fidx = $p[-1];
	$l = `tail -n 1 $scriptdir/Annotations/Update.Files/Variants.$fileidx.txt`;
	chomp($l);
	@p = split(/\t/,$l);
	my $lidx = $p[-1];
	print "  FILE: $fileidx (variants $fidx to $lidx)\n";
	
	# get current annotations from DB.
	#print "Loading Current Annotations\n";
	for ($i = 1; $i<=25; $i++) {
	  print "   Chromosome $i\n";
	  my $current = GetAnno($fidx,$lidx,$i,'');
	  #my $current = retrieve("$scriptdir/Annotations/Update.Files/ClinVarSNV.$fileidx.$i");
	  #system("rm '$scriptdir/Annotations/Update.Files/ClinVarSNV.$fileidx.$i'");
	  # get new annotations from CLINVAR 
	  #print "Loading New Annotations\n";
	  my $new = GetAnno($fidx,$lidx,$i,$table_prefix);
	  # Compare annotations.
	  print "    current keys: ".keys(%$current)."\n";
	  print "    new keys: ".keys(%$new)."\n";
	  my $t = keys(%$current);
	  $kidx = 0;
	  foreach(keys(%$current)) {
		$kidx++;
		## check
		my $vid = $_;
		## current annotations for this vid
		foreach(keys(%{$current->{$vid}})) {
			my $aok = 0;
			my $caid = $_;
			## compare against all new annotations
			foreach(keys(%{$new->{$vid}})) {
				my $naid = $_; ## simple increment value.
				my $match = 1;
				## loop the fields
				foreach(keys(%{$current->{$vid}{$caid}})) {
					next if ($_ eq 'aid' || $_ eq 'vid');
					## skip XREF entries
					next if ($_ =~ m/XRef/);
					## skip disease (too variable in content)
					next if ($_ =~ m/Disease/);
					## skip LastUpdate entries
					next if ($_ eq 'LastUpdate');
					## skip pubmed
					next if ($_ eq 'PubMed');
					if (!defined($new->{$vid}{$naid}{$_})) {
						#print "Field not found : $_\n"; 
						$match = 0;
						last;
					}
					if (looks_like_number($current->{$vid}{$caid}{$_})) {
						## round to 3 digits and then compare. Floating point errors: allow mismatch < 0.2*0.1precision. 
						my $oldv = sprintf("%.4f",$current->{$vid}{$caid}{$_});
						my $newv = sprintf("%.4f",$new->{$vid}{$naid}{$_}) ;
						if (abs($oldv-$newv) > 0.0002 ) {
							#print "$current->{$vid}{$caid}{'cvid'} : Field mismatch  '$_' : current: '$current->{$vid}{$caid}{$_}' vs new: '".$new->{$vid}{$naid}{$_}."'\n";
							$match = 0;
							last;
						}
					}
					elsif ($current->{$vid}{$caid}{$_} ne $new->{$vid}{$naid}{$_}) {
						#print "mismatch : $_ : $current->{$vid}{$caid}{$_} vs $new->{$vid}{$naid}{$_}\n";
						$match = 0;
						last;
					}
				}
				## annotation matches
				if ($match == 1) {
					$nrok++;
					$aok = 1;
					delete($new->{$vid}{$naid});
					last; # from new annotations for this caid.
				
				}
				else {
					next;
				}
			}
			if ($aok == 0) {
				## annotation not found.
				
				$nrfail++;
				# move variant to archived table
				my $contents = '';
				foreach(keys(%{$current->{$vid}{$caid}})) {
					next if ($_ eq 'aid' || $_ eq 'vid');
					$contents .= $_ . '|||'.$current->{$vid}{$caid}{$_}.'@@@';
				}
				$contents = substr($contents,0,-4);
				$archive->execute($vid,$contents);
				my $archid = $dbh->last_insert_id(undef,undef,"Archived_Annotations",undef);
				# add to log file for samples with this variant only if old  class =~ pathog
				#if ($current->{$vid}{$caid}{'Class'} =~ m/pathogenic/i) {
				my $argument = "1";
				# pass archived-id as entry to fetch details.
				
				$log->execute($vid,"Variants_x_ClinVar:$archid",$argument);
				
				# delete the annotation
				$del->execute($caid,$vid);
				
			}
		}
	  }
	
	  ## store new annotations for this variant (ones not deleted from hash yet, or variants not yet in the database.)
	  foreach(keys(%$new)) {
	    my $vid = $_;
	    foreach(keys(%{$new->{$vid}})) {
		my $naid = $_;
		$nrnew++;
		my $f = $v = '';
		foreach(keys(%{$new->{$vid}{$naid}})) {
			#next if ( $_ eq 'aid');
			next if ( $_ ne 'vid' && $_ ne 'cvid' && $_ ne 'MatchType' && $_ ne 'AA_MatchType' && $_ ne 'NM_ID' && $_ ne 'NP_ID' && $_ ne 'Effect');
			$f .= $_.',';
			$new->{$vid}{$naid}{$_} =~ s/'/"/g;
			$v .= "'".$new->{$vid}{$naid}{$_}."',";
		}
		$f = substr($f,0,-1);
		$v = substr($v,0,-1);
		my $query = "INSERT INTO `Variants_x_ClinVar` ($f) VALUES ($v)";
		$dbh->do($query);
		$aid = $dbh->last_insert_id(undef,undef,"Variants_x_ClinVar",undef);
		my $argument = "2";
		$log->execute($vid,"Variants_x_ClinVar:$aid",$argument);
	   }				
	  }
	  print "    status : $nrnew new , $nrok stable and $nrfail discarded annotations\n";
	}
	$fileidx++;
}

$log->finish();
$archive->finish();
#$sids->finish();
$del->finish();
if ($usemc == 1) {
	clearMemcache("Variants_x_ClinVar");
}

## mv validation to final.
system("cd $scriptdir/Annotations/ClinVar/ ; mv '$table_prefix"."refseq_links.txt' 'refseq_links.txt'");
system("cd $scriptdir/Annotations/ClinVar/ ; mv '$table_prefix"."ClinVar.parsed.txt' 'ClinVar.parsed.txt'");
system("cd $scriptdir/Annotations/ClinVar/ ; mv '$table_prefix"."refseq_replaced.txt' 'refseq_replaced.txt'");
system("cd $scriptdir/Annotations/ClinVar/ ; mv '$table_prefix"."ClinVarFullRelease_Loaded_head.xml' 'ClinVarFullRelease_Loaded_head.xml'");
$dbh->do("TRUNCATE TABLE `ClinVar_db`");
$dbh->do("INSERT INTO `ClinVar_db` SELECT * FROM `$table_prefix"."ClinVar_db` WHERE 1");
$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."ClinVar_db`");
$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."Variants_x_ClinVar`");

####################
## 2. CNV matching #
####################
## CNV is skipped untill size issues are resolved.
exit;

print "PROCESS CNV MATCHING\n";
$fileidx = 1;
my @discard;
my $nrok = $nrfail = $nrnew = $missingfield = 0;
$log = $dbh->prepare("INSERT DELAYED INTO `Variants_x_Log` (vid, uid, entry, arguments) VALUES (?,'$uid',?,?)");
$archive = $dbh->prepare("INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (?,'Variants_x_ClinVarCNV','$dbbuild','$datestring',?)");
#$sids = $dbh->prepare("SELECT sid FROM `Variants_x_Samples` WHERE vid = ?");
$del = $dbh->prepare("DELETE LOW_PRIORITY FROM `Variants_x_ClinVarCNV` WHERE aid = ? AND vid = ?");
%sidlist = ();
while (-e "$scriptdir/Annotations/Update.Files/Variants.$fileidx.txt") {
	my $l = `head -n 1 $scriptdir/Annotations/Update.Files/Variants.$fileidx.txt`;
	chomp($l);
	my @p = split(/\t/,$l);
	my $fidx = $p[-1];
	$l = `tail -n 1 $scriptdir/Annotations/Update.Files/Variants.$fileidx.txt`;
	chomp($l);
	@p = split(/\t/,$l);
	my $lidx = $p[-1];
	print "  FILE: $fileidx (variants $fidx to $lidx)\n";
	
	# get current annotations from DB.
	for ($i = 1; $i<=25; $i++) {
	  my $current = retrieve("$scriptdir/Annotations/Update.Files/ClinVarCNV.$fileidx.$i");
	  system("rm '$scriptdir/Annotations/Update.Files/ClinVarCNV.$fileidx.$i'");
	  # get new annotations from CLINVAR 
	  my $new = AnnoNewCNV($fileidx,$i);
	  # Compare annotations.
	  print "   Chromosome $i\n";
	  print "    current keys: ".keys(%$current)."\n";
	  print "    new keys: ".keys(%$new)."\n";
	  my $t = keys(%$current);
	  $kidx = 0;
	  foreach(keys(%$current)) {
		$kidx++;
		## check
		my $vid = $_;
		## current annotations for this vid
		foreach(keys(%{$current->{$vid}})) {
			my $aok = 0;
			my $caid = $_;
			## compare against all new annotations
			foreach(keys(%{$new->{$vid}})) {
				my $naid = $_;
				my $match = 1;
				## loop the fields
				foreach(keys(%{$current->{$vid}{$caid}})) {
					next if ($_ eq 'aid' || $_ eq 'vid');
					## skip XREF entries
					next if ($_ =~ m/XRef/);
					## skip disease (too variable in content)
					next if ($_ =~ m/Disease/);
					## skip LastUpdate entries
					next if ($_ eq 'LastUpdate');
					## skip pubmed
					next if ($_ eq 'PubMed');
					if (!defined($new->{$vid}{$naid}{$_})) {
						#print "Field not found : $_\n"; 
						$match = 0;
						last;
					}
					if (looks_like_number($current->{$vid}{$caid}{$_})) {
						## round to 3 digits and then compare. Floating point errors: allow mismatch < 0.2*0.1precision. 
						my $oldv = sprintf("%.4f",$current->{$vid}{$caid}{$_});
						my $newv = sprintf("%.4f",$new->{$vid}{$naid}{$_}) ;
						if (abs($oldv-$newv) > 0.0002 ) {
							$match = 0;
							last;
						}
					}
					elsif ($current->{$vid}{$caid}{$_} ne $new->{$vid}{$naid}{$_}) {
						$match = 0;
						last;
					}
				}
				## annotation matches
				if ($match == 1) {
					$nrok++;
					$aok = 1;
					delete($new->{$vid}{$naid});
					last; # from new annotations for this caid.
				}
				else {
					next;
				}
			}
			if ($aok == 0) {

				## annotation not found.
				$nrfail++;
				# move variant to archived table
				my $contents = '';
				foreach(keys(%{$current->{$vid}{$caid}})) {
					next if ($_ eq 'aid' || $_ eq 'vid');
					$contents .= $_ . '|||'.$current->{$vid}{$caid}{$_}.'@@@';
				}
				$contents = substr($contents,0,-4);
				$archive->execute($vid,$contents);
				my $archid = $dbh->last_insert_id(undef,undef,"Archived_Annotations",undef);
				# add to log file for samples with this variant
				if ($current->{$vid}{$caid}{'Class'} =~ m/pathogenic/i) {
					#if (!defined($sidlist{$vid})) {
					#	$sids->execute($vid);
					#	$sidlist{$vid} = $sids->fetchall_arrayref();
					#}
					my $argument = "1";
					# pass archived-id as entry to fetch details.
					#foreach(@{$sidlist{$vid}}) {
						$log->execute($vid,"Variants_x_ClinVarCNV:$archid",$argument);
					#}
					# delete the annotation
					$del->execute($caid,$vid);
				}
			}
		}
	  }
	
	  ## store new annotations for this variant (ones not deleted from hash yet, or variants not yet in the database.)
	  foreach(keys(%$new)) {
	    my $vid = $_;
	    foreach(keys(%{$new->{$vid}})) {
		my $naid = $_;
		$nrnew++;
		my $f = $v = '';
		foreach(keys(%{$new->{$vid}{$naid}})) {
			#next if ( $_ eq 'aid');
			next if ( $_ ne 'vid' && $_ ne 'cvid' && $_ ne 'MatchType');
			$f .= $_.',';
			$new->{$vid}{$naid}{$_} =~ s/'/"/g;
			$v .= "'".$new->{$vid}{$naid}{$_}."',";
		}
		$f = substr($f,0,-1);
		$v = substr($v,0,-1);
		my $query = "INSERT INTO `Variants_x_ClinVarCNV` ($f) VALUES ($v)";
		$dbh->do($query);
		$aid = $dbh->last_insert_id(undef,undef,"Variants_x_ClinVarCNV",undef);
		
		# add to log file for samples with this variant
		if ($current->{$new}{$naid}{'Class'} =~ m/pathogenic/i) {
			#if (!defined($sidlist{$vid})) {
			#	$sids->execute($vid);
			#	$sidlist{$vid} = $sids->fetchall_arrayref();
			#}
			my $argument = "2";
			# pass archived-id as entry to fetch details.
			#foreach(@{$sidlist{$vid}}) {
				$log->execute($vid,"Variants_x_ClinVarCNV:$aid",$argument);
			#}
		}
	   }				
	  }
	  print "    status : $nrnew new , $nrok stable and $nrfail discarded annotations\n";
	}
	$fileidx++;
}

$log->finish();
$archive->finish();
$sids->finish();
$del->finish();

if ($usemc == 1) {
	clearMemcache("Variants_x_ClinVarCNV");
}
print "\nDone\n\n";
print "Nr of matching annotations: $nrok\n";
print "Nr of discarded annotations: $nrfail\n";
print "Nr of new annotations: $nrnew\n";

exit;


#################
## SUBROUTINES ##
#################
### GET ANNO 
sub GetAnno {
	my ($fidx,$lidx,$cchr,$prefix) = @_;
	if ($cchr !=~ m/^\d+$/) {
		$cchr = $chromhash{$cchr};
	}
	## local db connection
	my $connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	my $i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my %return = ();
	my $sth = $dbh->prepare("SELECT cvdb.VarType, cvdb.RefAllele, cvdb.AltAllele, cvdb.Gene, cvdb.Class, cvdb.ClassComment,cvdb.PubMed, vc.aid, vc.vid, vc.cvid, vc.MatchType, vc.NM_ID, vc.NP_ID, vc.Effect,vc.AA_MatchType FROM `$prefix"."Variants_x_ClinVar` vc JOIN `Variants` v JOIN `$prefix"."ClinVar_db` cvdb ON  vc.vid = v.id AND cvdb.id = vc.cvid WHERE v.chr = '$cchr' AND v.id BETWEEN $fidx AND $lidx");
	$sth->execute();
	$href = $sth->fetchall_arrayref({});
	## convert to hash{vid}{aid}
	foreach(@$href) {
		
		$return{$_->{'vid'}}{$_->{'aid'}} = $_;	
	}
	$sth->finish();
	$dbh->disconnect();
	return \%return;
}

### GET ANNO CNV
sub GetAnnoCNV {
	my ($fidx,$lidx,$cchr) = @_;
	if ($cchr !=~ m/^\d+$/) {
		$cchr = $chromhash{$cchr};
	}
	## local db connection
	my $connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	my $i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my %return = ();
	my $sth = $dbh->prepare("SELECT cvdb.VarType, cvdb.RefAllele, cvdb.AltAllele, cvdb.Class, cvdb.ClassComment,cvdb.PubMed, vc.aid, vc.vid, vc.cvid, vc.MatchType FROM `Variants_x_ClinVarCNV` vc JOIN `Variants` v JOIN `ClinVar_db` cvdb ON  vc.vid = v.id AND cvdb.id = vc.cvid WHERE v.chr = '$cchr' AND v.id BETWEEN $fidx AND $lidx");
	$sth->execute();
	$href = $sth->fetchall_arrayref({});
	## convert to hash{vid}{aid}
	foreach(@$href) {
		
		$return{$_->{'vid'}}{$_->{'aid'}} = $_;	
	}
	$sth->finish();
	$dbh->disconnect();
	return \%return;
}
=cut
sub clearMemcache {
	
	## READ CREDENTIALS  
	$credpath = abs_path("../../.LoadCredentials.pl");
	require($credpath) ;
	if ($usemc != 1) {
		return;
	}
	###########################
	# CONNECT TO THE DATABASE #
	###########################
	my $db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	my $dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;

	# memcached : Make sure module is available if this is set in .credentials
	use Cache::Memcached::Fast;
	use Digest::MD5 qw(md5_hex);
	#use JSON;
	my $memd = new Cache::Memcached::Fast { 'servers' => \@mcs, 'compress_threshold' => 1_000_000, };
	my ($tables) = @_;
	my $sth = $dbh->prepare("SELECT mckey FROM `Memcache_Keys` WHERE mctable = ?");
	my @t = split(/:/,$tables);
	foreach(@t) {
		my $tname = $_;
		$sth->execute($tname);
		my $rowcache = $sth->fetchall_arrayref();
		#$tkeys = $memd->get("keys:ngs:$tname");
		#if (defined($tkeys)) {
		foreach my $row (@$rowcache) {
			my $tk = $row->[0];	

				my $t = 0;
				my $r = $memd->delete($tk);
				while (!defined($r) && $t < 100 ) {
					$t++;
					$r = $memd->delete($tk);
				}
				if (defined($r)) {
					print "key '$tk' cleared.\n";

				}
				else {
					print "unable to clear '$tk'\n";
				}

			if (defined($r)) {
				#print "key 'keys:ngs:$tname' cleared.\n";
				$dbh->do("DELETE FROM `Memcache_Keys` WHERE mckey = '$tk'");
			}

			
			
				
		}
	}	
	$sth->finish();
	$dbh->disconnect();

	$memd->disconnect_all;
}
=cut
