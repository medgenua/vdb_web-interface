# from distutils.command.build import build
# from tkinter import W
# from CMG.UZALogger import setup_logging, get_logger
from CMG.Utils import Re, AutoVivification, CheckVariableType
from CMG.DataBase import MySqlError
from dataclasses import dataclass, field
from typing import Dict, List, Type
import configparser
import argparse
import os
import subprocess
import sys
import time
from datetime import date
import subprocess
import hashlib
import shutil
import ftplib
import gzip
import string
import xml.etree.cElementTree as ET

# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


## Tracker class for XML parsing
@dataclass
class Trackers:
    active: int = 0
    GetName: str = ""
    XRefType: str = ""
    RS_NM: Dict = field(default_factory=dict)
    RS_NP: Dict = field(default_factory=dict)
    Title: str = ""
    ID: str = ""
    LastUpdate: str = ""
    ClinSign: str = ""
    ClinSignStat: str = ""
    msType: str = ""
    RS_NC: str = ""
    RS_EX: str = ""
    MolCons: str = ""
    PubMed: str = ""
    XRefMolCons: str = ""
    XRefGene: str = ""
    XRefDisease: str = ""
    GeneSymbol: str = ""
    refallele: str = ""
    altallele: str = ""
    DiseaseName: str = ""
    chrom: str = ""
    start: str = ""
    stop: str = ""
    strand: str = ""
    haplotype: int = 0
    GeneInfo: int = 0
    variants: List = field(default_factory=list)

    def ResetVariant(self):
        log.debug("Reset variant entry.")
        self.chrom = ""
        self.start = ""
        self.stop = ""
        self.strand = ""
        self.msType = ""
        self.RS_NMs = ""
        self.RS_NC = ""
        self.RS_NPs = ""
        self.RS_EX = ""
        self.MolCons = ""
        self.XRefMolCons = ""
        self.XRefGene = ""
        self.GeneSymbol = ""
        self.refallele = ""
        self.altallele = ""
        self.RS_NM = {}
        self.RS_NP = {}


## some variables/objects
re = Re()  # customized regex-handler


def ConvertChr(v):
    chrom_dict = {str(x): str(x) for x in range(1, 23)}
    chrom_dict.update(
        {"X": "23", "23": "X", "Y": "24", "24": "Y", "M": "25", "MT": "25", "25": "M"}
    )
    return chrom_dict.get(str(v), "Un")


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Evaluate if a new annotatation release is present
            - Run annotator on all Variants if new release & store to dedicated validation table in DB
            - Compare current and new annations, notify users if relevant items were found. 
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # annotation type
    # parser.add_argument("-a", "--anno", required=True, help="Annotation Type")
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    parser.add_argument('-a','--annotation',required=False,help='dummy argument for compatibility')
    # download only mode
    parser.add_argument(
        "-d",
        "--download",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Download new release only into Validation_ tables.",
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Validation Mode: No data download, just reannotation and results are saved to files instead of emailed",
    )

    args = parser.parse_args()
    # set prefix to validation during update.
    args.prefix = "Validation_"

    return config, args


# Validate the provided options.
def ValidateTables(args, config):
    for table in ["ClinVar_db", "ClinVar_XRefs"]:
        if not annotator.dbh.tableExists(table):
            raise MySqlError("Required Table does not exists: {}".format(table))
        # create new table to validate / store tmp results in update.
        annotator.dbh.doQuery("DROP TABLE IF EXISTS `{}{}`".format(annotator.args.prefix, table))
        annotator.dbh.doQuery(
            "CREATE TABLE `{}{}` LIKE `{}`".format(annotator.args.prefix, table, table)
        )
    return True


def GetFileMD5(file):
    # md5 file exists : read content.
    if os.path.exists("{}.md5".format(file)):
        with open("{}.md5".format(file), "r") as fh:
            md5 = fh.readline().rstrip().split("\t")[0]
    # else : create the contents.
    else:
        with open(file, "rb") as f:
            file_hash = hashlib.md5()
            while chunk := f.read(8192):
                file_hash.update(chunk)
        md5 = file_hash.hexdigest()

        with open("{}.md5".format(file), "w") as fh:
            fh.write(file_hash.hexdigest())

    return md5


def ListOfStrings(data):
    return [str(x) for x in data]


def DownloadClinVar():
    # create output folder:
    if not os.path.isdir("{}DataTables".format(annotator.args.prefix)):
        os.makedirs("{}DataTables".format(annotator.args.prefix))

    # from ncbi

    for f in ["pub/clinvar/xml/ClinVarFullRelease_00-latest.xml.gz", "gene/DATA/gene2refseq.gz"]:
        try:
            # download (rapid timeouts, so restart connections for each file.)
            log.info("Retrieving : {}".format(f))
            ncbi = ftplib.FTP("ftp.ncbi.nlm.nih.gov")
            ncbi.login("anonymous", "")
            outfile = os.path.join(
                "{}DataTables".format(annotator.args.prefix), os.path.basename(f)
            )
            annotator.files.append(outfile)
            ncbi.retrbinary("RETR {}".format(f), open(outfile, "wb").write)

        except Exception as e:
            raise e


# returns true if identical, false in any other case
def CheckMD5():
    # file missing
    current_md5 = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"], "Annotations/ClinVar/DataTables", "data.md5"
    )
    if not os.path.exists(current_md5):
        return False

    # create output folder:
    os.makedirs("{}DataTables".format(annotator.args.prefix), exist_ok=True)

    # Compare MD5 sums
    try:
        shutil.copy(current_md5, "{}DataTables/".format(annotator.args.prefix))
    except Exception as e:
        log.error(f"Failed to copy md5 file to validation folder: {e}")
        sys.exit(1)
    try:
        # use plain md5sum program, as we don't need actual checksums, just yes/no answer
        subprocess.check_call(
            "cd {} ; md5sum -c data.md5".format("{}DataTables".format(annotator.args.prefix)),
            shell=True,
        )
        # pass : return True
        return True
    except Exception as e:
        log.info("Checksums did not match. Updating !")
        return False


# parse ClinVar
def ClinVarParser():
    # some settings.
    dbbuild = annotator.dbh.runQuery(
        "SELECT `Annotation`, `Value` FROM `Annotation_DataValues` WHERE `Annotation` = 'ClinVar'"
    )[0]["Value"]

    RefHash = dict()
    mail = set()
    for r in annotator.dbh.runQuery(
        "SELECT `id`, `XRef_Key`,`HRef_Name`, `XRef_url`,`ValueRegEx` FROM `ClinVar_XRefs`"
    ):
        RefHash[r["XRef_Key"]] = {
            "HREF": r["HRef_Name"],
            "URL": r["XRef_url"],
            "RegEx": r["ValueRegEx"],
        }

    log.info(f"Looking for genome build : {dbbuild}")
    # read in NM/NP translations.
    log.info("Parsing gene2refseq map")
    
    refseq_map = {}
    with gzip.open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/ClinVar/{}DataTables".format(annotator.args.prefix),
            "gene2refseq.gz",
        ),
        "rt",
    ) as fh:
        for line in fh:
            cols = line.rstrip().split("\t")
            # only human links.
            if cols[0] != "9606":
                continue
            # only NM (coding)
            if not cols[3].startswith("NM_"):
                continue
            # map without version id
            # cols[3] = re.sub(r"^(.*)\.\d+$", r"\1", cols[3])
            cols[3] = cols[3].split(".")[0]
            # cols[5] = re.sub(r"^(.*)\.\d+$", r"\1", cols[5])
            cols[5] = cols[5].split(".")[0]
            log.debug(f"Mapping {cols[3]} => {cols[5]}")
            refseq_map[cols[3]] = cols[5]
    
    # prepare variables
    trackers = Trackers()
    # open outfile
    outfile = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations/ClinVar/{}DataTables".format(annotator.args.prefix),
        "ClinVar.parsed.txt",
    )
    annotator.files.append(outfile)
    fh_out = open(outfile, "w")

    fh_in = gzip.open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/ClinVar/{}DataTables".format(annotator.args.prefix),
            "ClinVarFullRelease_00-latest.xml.gz",
        ),
        "rb",
    )

    log.info("Parsing ClinVar XML.")
    # process
    counter = 0
    printed = 0
    skipped = 0
    # path = []
    for event, node in ET.iterparse(fh_in, events=("start", "end")):
        # start events can handle tag attributes
        if event == "start":
            ## ATTRIBUTES AND TRACKERS
            log.debug("{} : {}".format(event, node.tag))
            # main entry start
            if node.tag == "ClinVarSet":
                counter += 1
                if counter % 10000 == 0:
                    log.info(f"Progress : {counter} entries parsed...")
                trackers.active = 1

            # haplotype block : contains multiple variants.
            elif node.tag == "MeasureSet" and node.get("Type") == "Haplotype":
                trackers.haplotype = 1
            # active is set to 1 for each ClinVarSet, and set to zero at the end of ReferenceClinVarAssertion tag.
            elif trackers.active == 1:
                # this is the main block holding all important info.
                if node.tag == "ReferenceClinVarAssertion":
                    # Last update on reference entry
                    trackers.LastUpdate = node.get("DateLastUpdated")
                elif node.tag == "ClinVarAccession":
                    trackers.ID = node.get("Acc")
                ## variant entries
                elif node.tag == "Measure":
                    trackers.msType = node.get("Type")
                # sequence location is stored in tag attributes
                elif node.tag == "SequenceLocation":
                    # gene info also has a sequencelocation.
                    if trackers.GeneInfo != 1:
                        # only take correct genome bould
                        if node.get("Assembly") == dbbuild:
                            trackers.chrom = annotator.ConvertChr(node.get("Chr"))
                            # snv's / small indels
                            if node.get("start"):
                                trackers.start = node.get("start")
                            # complex/larger cnvs/svs
                            elif node.get("innerStart"):
                                trackers.start = node.get("innerStart")
                            if node.get("stop"):
                                trackers.stop = node.get("stop")
                            elif node.get("innerStop"):
                                trackers.stop = node.get("innerStop")
                            # not clear, but it seems *VCF is the canonical field
                            if node.get("referenceAlleleVCF"):
                                trackers.refallele = node.get("referenceAlleleVCF")
                            elif node.get("referenceAllele"):
                                trackers.refallele = node.get("referenceAllele")
                            if node.get("alternateAlleleVCF"):
                                trackers.altallele = node.get("alternateAlleleVCF")
                            elif node.get("alternateAllele"):
                                trackers.altallele = node.get("alternateAllele")
                            trackers.strand = node.get("Strand")
                # start of gene info.
                elif node.tag == "MeasureRelationship":
                    trackers.GeneInfo = 1
                    trackers.XRefType = "Gene"
                # only coding variants have this attribute
                elif node.tag == "Attribute" and node.get("Type") == "MolecularConsequence":
                    # set xreftype for selecting action below.
                    trackers.XRefType = "MolCons"
                # start of disease info
                elif node.tag == "Trait":
                    trackers.XRefType = "Disease"
                # XRef are links to external databases.
                elif node.tag == "XRef":
                    # set to true at beginning of each entry, and when MolecularConsequence tag is found.
                    if trackers.XRefType == "MolCons":
                        if node.get("DB") != "RefSeq":
                            # variant external references.
                            trackers.XRefMolCons += "||{}|{}".format(
                                node.get("DB"),
                                node.get("ID"),
                            )
                    # these are in the measureRelationship block.
                    elif trackers.XRefType == "Gene":
                        trackers.XRefGene += "||{}|{}".format(node.get("DB"), node.get("ID"))
                    #
                    elif trackers.XRefType == "Disease":
                        trackers.XRefDisease += "||{}|{}".format(node.get("DB"), node.get("ID"))

        # end events have the full node info (text, childs, ...)
        elif event == "end":
            ## GET CONTENTS
            log.debug(f"End of {node.tag} : active: {trackers.active}")
            # pubmed identifiers.
            if node.tag == "ID" and node.get("Source") == "PubMed":
                trackers.PubMed += "||{}".format(node.text)
            # active is set to 1 for each ClinVarSet, and set to zero at the end of ReferenceClinVarAssertion tag.
            elif trackers.active == 1:
                # title : holds nm_:c.point, and disease name
                if node.tag == "Title":
                    # title
                    trackers.Title = node.text  # getInnerText(node)
                    log.debug(f"Title : {node.text}")

                # Clinical Significance and its status
                elif node.tag == "ClinicalSignificance":
                    # from first entry (if more are present)
                    try:
                        trackers.ClinSign = node.find("Description").text
                    except Exception:
                        log.debug("No entry for clinical significance")
                        trackers.ClinSign = "."
                    try:
                        trackers.ClinSignStat = node.find("ReviewStatus").text
                    except Exception:
                        log.debug("No entry for ClinSign review status")
                        trackers.ClinSignStat = "."

                ## name tag: present for multiple items.
                elif node.tag == "Attribute":
                    atype = node.get("Type")
                    if atype == "HGVS, protein, RefSeq":
                        nodeContents = node.text.split(":")
                        acc = nodeContents[0].split(".")
                        trackers.RS_NP[acc[0]] = {}
                        if not acc[1]:
                            acc[1] = 1
                        trackers.RS_NP[acc[0]]["version"] = acc[1]
                        trackers.RS_NP[acc[0]]["change"] = nodeContents[1]
                    elif atype == "HGVS, coding, RefSeq":
                        nodeContents = node.text.split(":")
                        acc = nodeContents[0].split(".")
                        trackers.RS_NM[acc[0]] = {}
                        # no/invalid version number:
                        if len(acc) == 1 :
                            acc.append(1)
                        if not acc[1]:
                            acc[1] = 1
                        # no/invalid change notation:
                        if len(nodeContents) == 1:
                            nodeContents.append('')
                        trackers.RS_NM[acc[0]]["version"] = acc[1]
                        trackers.RS_NM[acc[0]]["change"] = nodeContents[1]
                    # only coding variants have this attribute
                    elif atype == "MolecularConsequence":
                        trackers.MolCons += f"||{node.text}"

                # gene symbol
                elif node.tag == "Symbol" and trackers.GeneInfo == 1:
                    # store gene symbol
                    for cnode in node.findall("ElementValue"):
                        if cnode.get("Type") == "Preferred":
                            trackers.GeneSymbol = cnode.text
                            break

                # the name of the trait/disease if not specified already.
                elif (
                    node.tag == "ElementValue"
                    and trackers.XRefType == "Disease"
                    and node.get("Type") == "Preferred"
                    and trackers.DiseaseName == ""
                ):
                    # events.expandNode(node)
                    trackers.DiseaseName = node.text  # getInnerText(node)
                # end of a variant entry.
                elif node.tag == "Measure":
                    # build RS_NM string.
                    RS_NMs = ""
                    RS_NPs = ""
                    for nm in trackers.RS_NM:
                        RS_NMs += "||{}.{}:{}".format(
                            nm, trackers.RS_NM[nm]["version"], trackers.RS_NM[nm]["change"]
                        )
                        # not in map, so NP is lost.
                        if not nm in refseq_map or len(trackers.RS_NP) == 0:
                            RS_NPs += "||."
                            continue
                        # get the mapped NP.
                        np = refseq_map[nm]
                        ## np known?
                        if np in trackers.RS_NP:
                            RS_NPs += "||{}.{}:{}".format(
                                np, trackers.RS_NP[np]["version"], trackers.RS_NP[np]["change"]
                            )
                        else:
                            RS_NPs += "||."
                        # add the effect to variant list.
                        log.debug(
                            f"Extracted Variant {trackers.chrom}:{trackers.start} {trackers.refallele}/{trackers.altallele}"
                        )
                    # add variant to processing queue
                    trackers.variants.append(
                        [
                            trackers.chrom,
                            trackers.start,
                            trackers.stop,
                            trackers.strand,
                            trackers.msType,
                            RS_NMs,
                            trackers.RS_NC,
                            RS_NPs,
                            trackers.RS_EX,
                            trackers.MolCons,
                            trackers.XRefMolCons,
                            trackers.XRefGene,
                            trackers.GeneSymbol,
                            trackers.refallele,
                            trackers.altallele,
                        ]
                    )
                    # reset variant within clinvar entry.
                    trackers.ResetVariant()
            ## TRACKER STATUS
            # end of the 'reference' block : stop tracking.
            if node.tag == "ReferenceClinVarAssertion":
                trackers.active = 0
                if trackers.ID == "":
                    log.info("Missing RCV entry")
                    log.info(
                        f"RefClin {counter} Ended: {trackers.ID} {trackers.Title} {trackers.LastUpdate} {trackers.ClinSign} {trackers.ClinSignStat}"
                    )

            # end of molcons block.
            elif node.tag == "MolecularConsequence" and trackers.XRefType == "MolCons":
                trackers.XRefType = ""
            # end of gene info : reset tracker.
            elif node.tag == "MeasureRelationship":
                trackers.XRefType = ""
                trackers.GeneInfo = 0

            # end of a clinvar entry (set of variants related to same disease?)
            elif node.tag == "ClinVarSet":
                for variant in trackers.variants:
                    ## end of the ClinVar entry. print out results. Taken out of the 'active' block, since some pubmeds were relocated.
                    raw_entry = [
                        trackers.ID,  # 1 clinvar ID
                        variant[0],  # 2 chrom
                        variant[1],  # 3 start
                        variant[2],  # 4 end
                        variant[4],  # 5 variant type
                        variant[13],  # 6 Ref Allele
                        variant[14],  # 7 Alt Allele
                        trackers.LastUpdate,  # 8 last update
                        variant[12],  # 9 Gene Symbol
                        trackers.DiseaseName,  # 10 Disease Name
                        trackers.ClinSign,  # 11 coded clinical signifance
                        trackers.ClinSignStat,  # 12 coded clinical signf. comment
                        variant[5],  # 13 refseq NMs
                        variant[7],  # 14 refseq NPs
                        variant[9],  # 15 MolCons
                        variant[10],  # 16 XREF MolCons
                        variant[11],  # 17 XRef Gene
                        trackers.XRefDisease,  # 18 XRef Disease
                        trackers.PubMed,  # 19 pubmed ids
                        trackers.haplotype,  # 24 Haplotype (1 or 0)
                    ]
                    # format for tabular db-load
                    prepped_entry, mail = PrepareVariant(raw_entry, RefHash, mail)
                    if not prepped_entry:
                        # invalid entries: do not print.
                        skipped += 1
                        continue
                    # write line
                    printed += 1
                    fh_out.write("\t".join([str(x) for x in prepped_entry]) + "\n")

                # reset the trackers.
                trackers = Trackers()
                # clear memory
                node.clear()

    # done. close outfile.
    fh_out.close()
    # stats
    log.info("Parsing done:")
    log.info(f"  - Printed : {printed}")
    log.info(f"  - Skipped : {skipped}")
    # print mail entries for evaluation.
    with open("/tmp/missing.data.txt", "w") as fh:
        for i in mail:
            fh.write(f"{i}\n")

    return outfile


def PrepareVariant(variant, RefHash, mail):
    # replacement_array
    replace = [""] + list(string.ascii_lowercase)
    # skip incomplete values.
    if variant[0] == "" or variant[1] == "" or variant[2] == "":
        return False, mail
    # lstrip('||') .
    variant = [str(x).lstrip("||") for x in variant]
    # title field has HGVS AND disease name
    if variant[8]:
        geneSymbol = variant[8]
    else:
        geneSymbol = ""

    # replace || by , in pubmed
    if variant[18]:
        variant[18] = variant[18].replace("||", ",")
    else:
        variant[18] = "."
    # rephrase REFs to full links for ease of display in application
    for item in ["Gene:16", "Disease:17", "Variant:15"]:
        refs = set()
        field_name, field_idx = item.split(":")
        for ref in variant[int(field_idx)].split("||"):
            if not ref:
                continue
            else:
                log.debug("Splitting ref : {}".format(ref))
            try:
                k, v = ref.split("|")
            except ValueError as e:
                log.warning(f"Could not split : {ref}")
                continue
            if f"{field_name}|{k}" not in RefHash:
                log.debug(f"{field_name}|{k} not in RefHash")
                mail.add(f"{field_name}|{k}")
                continue
            url = RefHash[f"{field_name}|{k}"]["URL"]

            if url == "-1":
                continue
            url = url.replace("$chr", ConvertChr(variant[1]))
            url = url.replace("$start", variant[2])
            url = url.replace("$end", variant[3])
            url = url.replace("$val", v)
            url = url.replace("$gene", geneSymbol)
            if RefHash[f"{field_name}|{k}"]["RegEx"]:
                r, rv = RefHash[f"{field_name}|{k}"]["RegEx"].split("||")
                rvals = rv.split(",")
                log.debug(f"running regex : {r} : {rv}")
                # eg : https://oi.gene.le.ac.uk/variants.php?select_db=$a&action=view&view=$b
                #   r: (.*)_(\d+)
                #     => v =~ m/r/   => then replace $a by $1 and $b by $2
                try:
                    m = re.findall(r, v)[0]
                except IndexError:
                    log.error(variant[int(field_idx)])
                    import pprint

                    pprint.pprint(variant)
                    log.error("regex didn't match : {}|{} : {} : {}".format(field_name, k, r, v))
                    sys.exit(1)
                for rval in rvals:
                    url = url.replace(f"${replace[int(rval)]}", m[int(rval) - 1])
                log.debug(url)
            # add url
            refs.add(
                f"<a href='{url}' target='_blank'>" + RefHash[f"{field_name}|{k}"]["HREF"] + "</a>"
            )
        # collapse urls
        variant[int(field_idx)] = ", ".join(refs)
    # cnvs :
    if "copy number" in variant[4].lower():
        geneSymbol = "-cnv-"
    # code
    variant[10] = annotator.GetCode("Class", variant[10])
    variant[11] = annotator.GetCode("ClassComment", variant[11])
    # return
    return variant, mail


def CompareResults():

    # open all output files.
    time_stamp = date.today().strftime("%Y-%m-%d")
    # open log loading file handle:
    out_files = dict()
    out_files["LOG"] = open(
        os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/ClinVar/Output_Files",
            "Load.Log.ClinVar.{}.txt".format(time_stamp),
        ),
        "w",
    )

    for vtype in ["NOVEL", "LOST"]:
        out_files[vtype] = open(
            os.path.join(
                config["LOCATIONS"]["SCRIPTDIR"],
                "Annotations/ClinVar/Output_Files/",
                "Validation.{}.{}.{}".format(annotator.args.anno, time_stamp, vtype),
            ),
            "w",
        )

    # fields to store in Variants_x_ClinVar
    field_names = [
        "vid",
        "cvid",
        "MatchType",
        "NM_ID",
        "NP_ID",
        "Effect",
        "AA_MatchType",
    ]
    # work in slices of 50K variants :
    start_vid = 0
    nr_ok = 0
    nr_lost = 0
    nr_new = 0
    while True:
        # get last vid.
        vids = annotator.dbh.runQuery(
            "SELECT id FROM `Variants` WHERE id >= %s ORDER BY `id` LIMIT 50000",
            start_vid,
        )
        if not vids:
            # no data left.
            break
        last_vid = vids[-1]["id"]
        log.info("Working on 50K variants with VIDS from {} to {}".format(start_vid, last_vid))
        vids = []

        # get old annotations
        log.info("Fetching old annotations")
        old_annotations = AutoVivification()
        # two queries : cvid defined (joined), cvid null (.) : defaults.
        queries = [
            f"SELECT cvdb.VarType, cvdb.RefAllele, cvdb.AltAllele, cvdb.Gene, cvdb.Class, cvdb.ClassComment,cvdb.PubMed, vc.aid, vc.vid, vc.cvid, vc.MatchType, vc.NM_ID, vc.NP_ID, vc.Effect,vc.AA_MatchType FROM `Variants` v JOIN `{annotator.args.table_name}` vc JOIN `ClinVar_db` cvdb ON  vc.vid = v.id AND cvdb.id = vc.cvid WHERE v.id BETWEEN %s AND %s",
            f"SELECT '.' AS VarType, '.' AS RefAllele, '.' AS AltAllele, '.' AS Gene, '.' AS Class, '.' AS ClassComment, '.' AS PubMed, vc.aid, vc.vid, vc.cvid, vc.MatchType, vc.NM_ID, vc.NP_ID, vc.Effect,vc.AA_MatchType FROM `Variants` v JOIN `{annotator.args.table_name}` vc ON  vc.vid = v.id WHERE v.id BETWEEN %s AND %s AND vc.cvid = '.'",
        ]
        for query in queries:
            rows = annotator.dbh.runQuery(
                query,
                [start_vid, last_vid],
                as_dict=True,
            )
            for row in rows:
                # hash[vid][aid]
                aid = row["aid"]
                del row["aid"]
                old_annotations[row["vid"]][aid] = row
                # if row["vid"] == 389:
                #    log.info("Fetched old variant 389")

        # get new annotations & compare
        log.info("Fetching and comparing new annotations.")
        # two queries : with / without defined cvid.
        queries = [
            f"SELECT cvdb.VarType, cvdb.RefAllele, cvdb.AltAllele, cvdb.Gene, cvdb.Class, cvdb.ClassComment,cvdb.PubMed, vc.aid, vc.vid, vc.cvid, vc.MatchType, vc.NM_ID, vc.NP_ID, vc.Effect,vc.AA_MatchType FROM `Variants` v JOIN `{annotator.args.prefix}{annotator.args.table_name}` vc JOIN `{annotator.args.prefix}ClinVar_db` cvdb ON  vc.vid = v.id AND cvdb.id = vc.cvid WHERE v.id BETWEEN %s AND %s",
            f"SELECT '.' AS VarType, '.' AS RefAllele, '.' AS AltAllele, '.' AS Gene, '.' AS Class, '.' AS ClassComment, '.' AS PubMed, vc.aid, vc.vid, vc.cvid, vc.MatchType, vc.NM_ID, vc.NP_ID, vc.Effect,vc.AA_MatchType FROM `Variants` v JOIN `{annotator.args.prefix}{annotator.args.table_name}` vc ON  vc.vid = v.id WHERE v.id BETWEEN %s AND %s AND vc.cvid = '.'",
        ]
        rows = list()
        for query in queries:
            sub_rows = annotator.dbh.runQuery(
                query,
                [start_vid, last_vid],
                as_dict=True,
            )
            rows.extend(sub_rows)
        # compare.
        for row in rows:
            vid = row["vid"]
            new_aid = row["aid"]
            del row["aid"]
            # present in old_annotations?
            annotation_found = False
            for old_aid in old_annotations[vid]:
                # clinvar entry exists.
                if old_annotations[vid][old_aid]["cvid"] == row["cvid"]:
                    # entry equal ?
                    if old_annotations[vid][old_aid] == row:
                        annotation_found = True
                        # remove matched old annotation.
                        del old_annotations[vid][old_aid]
                        break

            # annotation found :
            if annotation_found:
                nr_ok += 1
                # process next row.
                continue

            # annotation not found : novel entry
            nr_new += 1
            if annotator.args.validate:
                out_files["NOVEL"].write(
                    "{}\t{}\t{}\n".format(
                        vid,
                        new_aid,
                        "; ".join(ListOfStrings(["{}:{}".format(x, row[x]) for x in row])),
                    )
                )
            else:
                # copy to main table.
                new_aid = annotator.dbh.insertQuery(
                    "INSERT INTO `{}` (`{}`) VALUES ('{}')".format(
                        annotator.args.table_name,
                        "`,`".join(field_names),
                        "','".join([str(row[x]) for x in field_names]),
                    )
                )
                # add to log table.
                out_files["LOG"].write(
                    "{}\t{}\tVariants_x_ClinVar_ncbigene:{}\t2\n".format(
                        vid, annotator.args.userid, new_aid
                    )
                )

        # any left on the old_annotations stack : these are lost.
        log.info("Archiving discarded annotations")
        for vid in old_annotations:

            for aid in old_annotations[vid]:

                if old_annotations[vid][aid]:

                    # validate : to files
                    if annotator.args.validate:
                        out_files["LOST"].write(
                            "{}\t{}\t{}\n".format(
                                vid,
                                aid,
                                "; ".join(
                                    ListOfStrings(
                                        [
                                            "{}:{}".format(x, old_annotations[vid][aid][x])
                                            for x in old_annotations[vid][aid]
                                        ]
                                    )
                                ),
                            )
                        )
                        nr_lost += 1
                    # else : to log table
                    else:
                        
                        d = list(
                            zip(field_names, [old_annotations[vid][aid][x] for x in field_names])
                        )
                        del d[0]
                        contents = "@@@".join(["{}|||{}".format(i[0], i[1]) for i in d])
                        # archive
                        aaid = annotator.dbh.insertQuery(
                            "INSERT INTO `Archived_Annotations` (vid, SourceTable, SourceBuild, ArchiveDate, Contents) VALUES (%s,'Variants_x_ClinVar_ncbigene',%s,%s,%s)",
                            [vid, f"-{annotator.args.build}", time_stamp, contents],
                        )
                        # log
                        out_files["LOG"].write(
                            "{}\t{}\tVariants_x_ClinVar_ncbigene:{}\t1\n".format(
                                vid, annotator.args.userid, aaid
                            )
                        )
                        # delete
                        annotator.dbh.doQuery(
                            f"DELETE FROM `{annotator.args.table_name}` WHERE `vid` = %s AND `aid` = %s",
                            [vid, aid],
                        )

        # clear
        old_annotations.clear()
        # next iteration.
        start_vid = last_vid + 1

    # overview.
    log.info("STATISTICS:")
    log.info("  IDENTICAL   : {}".format(nr_ok))
    log.info("  NOVEL ENTRY : {}".format(nr_new))
    log.info("  DISCARDED   : {}".format(nr_lost))

    # close file handles
    for fh in out_files:
        out_files[fh].close()

    # load to log-table.
    if not annotator.args.validate:
        annotator.dbh.doQuery(
            "LOAD DATA LOCAL INFILE '{}' INTO TABLE `Variants_x_Log` (`vid`, `uid`, `entry`, `arguments`)".format(
                os.path.join(
                    config["LOCATIONS"]["SCRIPTDIR"],
                    "Annotations/ClinVar/Output_Files",
                    "Load.Log.ClinVar.{}.txt".format(time_stamp),
                )
            )
        )


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
        # hardcoded
        args.anno = "GO"
        args.table_name = "Variants_x_ClinVar_ncbigene"

    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    log = StartLogger(config, "ClinVar")

    # get the annotator object
    annotator = Annotator(config=config, method="ClinVar", annotation="ClinVar", args=args)
    annotator.args.table_name = "Variants_x_ClinVar_ncbigene"
    
    # validate the tables & files.
    try:
        log.debug("Validating table presence")
        # general validation
        annotator.ValidateTable()
        # specific requirements
        ValidateTables(annotator.args, config)
    except DataError as e:
        log.error("Could not locate necessary resources: {}".format(e))
        sys.exit(1)
    
    # get coding values
    for c in ["Class", "ClassComment"]:
        annotator.GetCode(c)

    # get userid of admin user (for archive logs)
    try:
        annotator.args.userid = annotator.dbh.runQuery(
            "SELECT id FROM `Users` WHERE `email` = %s", [config["USERS"]["ADMIN"]]
        )[0]["id"]
    except Exception as e:
        log.error("Admin email not recognized by VariantDB : {}".format(e))
        sys.exit(1)

    # check / download new release
    
    if not args.validate:
        # get latest release
        try:
            log.info("Downloading ClinVar datafiles")
            DownloadClinVar()
        except Exception as e:
            log.error("Failed to download ClinVar release: {}".format(e))
            sys.exit(1)
        identical = CheckMD5()
        
    else:
        identical = False
    # run the annotation procedures if md5s didn't match
    if not identical and not args.download:
        
        # parse xml => txt
        try:
            log.debug("Starting Parsing routine")
            parsed_file = ClinVarParser()

        except Exception as e:
            log.error(f"Failed to parse ClinVar XML : {e}")
            sys.exit(1)

        # load.
        parsed_file = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations/ClinVar/{}DataTables".format(annotator.args.prefix),
            "ClinVar.parsed.txt",
        )
        try:
            log.info("Loading entries into database.")
            annotator.dbh.doQuery(
                "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}ClinVar_db`".format(
                    parsed_file, annotator.args.prefix
                )
            )
        except Exception as e:
            log.error("Failed to load {} into database : {}".format(parsed_file, e))
            sys.exit(1)
        
        # run the annotator.
        try:
            log.info("Running annotations.")
            subprocess.check_output(["python", "LoadVariants.py", "-v"])
        except Exception as e:
            log.error("Failed to run annotator for ClinVar: {}".format(e))
            sys.exit(1)
        
        # compare results.
        try:
            log.info("Comparing Results")
            CompareResults()
        except Exception as e:
            log.error("Failed to process ClinVar differences after update: {}".format(e))
            sys.exit(1)
        
        # update production versions if not in validation mode.
        if not args.validate:
            # put new md5 in place 
            try:
                cmd = "cd {} ; md5sum ClinVarFullRelease_00-latest.xml.gz gene2refseq.gz > data.md5".format(
                    os.path.join(
                        config["LOCATIONS"]["SCRIPTDIR"],
                        "Annotations/ClinVar/",
                        f"{annotator.args.prefix}DataTables",
                    )
                )
                subprocess.check_output(cmd, shell=True)

                shutil.copy(
                    os.path.join(
                        config["LOCATIONS"]["SCRIPTDIR"],
                        "Annotations/ClinVar/",
                        f"{annotator.args.prefix}DataTables/data.md5",
                    ),
                    os.path.join(
                        config["LOCATIONS"]["SCRIPTDIR"],
                        "Annotations/ClinVar/",
                        "DataTables/data.md5",
                    ),
                )
            except Exception as e:
                log.error("Could not generate fresh md5 file: {}".format(e))
            # copy Validation_ClinVar_db to production.
            try:
                # first backup.
                time_stamp = date.today().strftime("%Y-%m-%d")
                with gzip.open(
                    os.path.join(
                        config["LOCATIONS"]["SCRIPTDIR"],
                        "Annotations/ClinVar/DataTables/",
                        f"ClinVar_db.dump.{time_stamp}.txt.gz",
                    ),
                    "wt",
                ) as fh:
                    rows = annotator.dbh.runQuery(
                        "SELECT * FROM `ClinVar_db`", size=50000, as_dict=False
                    )
                    while rows:
                        for row in rows:
                            fh.write("\t".join([str(x) for x in row]) + "\n")
                        rows = annotator.dbh.GetNextBatch()
                # then truncate & copy.
                annotator.dbh.doQuery("DROP TABLE `ClinVar_db`")
                annotator.dbh.doQuery(
                    f"ALTER TABLE `{annotator.args.prefix}ClinVar_db` RENAME TO `ClinVar_db`"
                )
            except Exception as e:
                log.error("Failed to move Validation data of ClinVar_db to production table.")
        
# annotator.cleanup()
annotator.cleanup()
