#!/usr/bin/perl
################################################################
## DOWNLOADS A NEW VERSION WITHOUT UPDATING EXISTING VARIANTS ##
################################################################

$|++;

use DBI;
use Getopt::Std;

use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);

# opts
# v : run in validation mode.
# b : genome build.
getopts('b:v', \%opts) ;

## IN VALIDATION MODE ?
our $validation = 0;
our $table_prefix = '';
if (defined($opts{'v'})) {
	print "\n";
	print "RUNNING ANNOTATIONS IN VALIDATION MODE:\n";
	print "  - All variants will be annotated\n";
	print "  - Annotations are stored in 'Validation_<tablename>' table\n";
	print "  - original annotations are not modified\n";
	print "\n";
	print "press <ctrl>-c to abort\n";
	sleep 5;
	print "\n\n";
	$validation = 1;
	$table_prefix = 'Validation_';
}




####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "MT" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "MT";


## CHECK VERSION : for validation : always update.
print "Downloading Latest ClinVar release\n";

system("wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/xml/ClinVarFullRelease_00-latest.xml.gz -O '/tmp/ClinVarFullRelease.xml.gz' 2>/dev/null");
if (-e '/tmp/ClinVarFullRelease.xml.gz') {
	my $vstring = `zcat /tmp/ClinVarFullRelease.xml.gz | head -n 2 | tail -n 1`;
	$vstring =~ m/Dated="(\d{4}-\d{2}-\d{2})"/;
	$online = $1;
}
else {
	$online = '';
}
if (!-e 'ClinVarFullRelease_Loaded_head.xml') {
	$local = '';
}
else {
	$lstring = `head -n 2 ClinVarFullRelease_Loaded_head.xml | tail -n 1`;
	$lstring =~ m/Dated="(\d{4}-\d{2}-\d{2})"/;
	$local = $1;
}
	
## Process IF LATEST >> CURRENT
if ($validation == 1 || "$online" ne "$local") {
	print "New Version Available, Unpacking!\n";
	system("gzip -dc /tmp/ClinVarFullRelease.xml.gz > /tmp/$table_prefix"."ClinVarFullRelease_Loaded.xml && rm /tmp/ClinVarFullRelease.xml.gz");
	# store head.
	system("head -n 50 /tmp/$table_prefix"."ClinVarFullRelease_Loaded.xml > $table_prefix"."ClinVarFullRelease_Loaded_head.xml");
}
else {
	exit;
}


## GET THE LATEST NM_ -> NP_ LINKS
##################################
print "Downloading NM/NP links from refgene\n";
system("rm -f '/tmp/gene2refseq.gz'");
system("wget ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2refseq.gz -O '/tmp/gene2refseq.gz' 2>/dev/null");
if (-e '/tmp/gene2refseq.gz') {
	my $command = "zcat /tmp/gene2refseq.gz | grep -P '^9606\\t' | cut -f 4,6 | grep 'NM_' > $table_prefix"."refseq_links.txt";
	system($command);
	system("rm '/tmp/gene2refseq.gz'");
}

## store to DB.
## GET current/provided build
our $db = '';
our $dbbuild = '';
if (!defined($opts{'b'}) || $opts{'b'} eq '') {
	$db = "NGS-Variants-Admin";
	$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
	$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
	$dbh->{mysql_auto_reconnect} = 1;
	## retry on failed connection (server overload?)
	$i = 0;
	while ($i < 10 && ! defined($dbh)) {
		sleep 7;
		$i++;
		print "Connection to $host failed, retry nr $i/10\n";
		$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
	}
	my $gsth = $dbh->prepare("SELECT name FROM `CurrentBuild`");
	$gsth->execute();
	my ($cb) = $gsth->fetchrow_array();
	$dbbuild = $cb;
	$gsth->finish();
	$db = "NGS-Variants$cb";
	$dbh->disconnect();
}
else {
	$db = "NGS-Variants".$opts{'b'};
	$dbbuild = $opts{'b'};
}

$connectionInfocnv="dbi:mysql:$db:mysql_local_infile=1:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

## get build replacement values
my %builds = ();
my $sth = $dbh->prepare("SELECT Annotation, Value FROM `Annotation_DataValues`");
$sth->execute();
while (my @r = $sth->fetchrow_array()) {
	$builds{$r[0]} = $r[1];
}
$sth->finish();
my $cvdb = $builds{'ClinVar'};

if ($validation == 1) {
	$dbh->do("DROP TABLE IF EXISTS `$table_prefix"."ClinVar_db`");
	$dbh->do("CREATE TABLE `$table_prefix"."ClinVar_db` LIKE `ClinVar_db`");
}
## PARSE the XML
print "Parse the XML to Database Loadable format (using build $cvdb)\n";
print "Command: python Parser.py '$cvdb' '$table_prefix'\n";
system("python Parser.py '$cvdb' '$table_prefix'");
# clear out xml.
system("rm /tmp/$table_prefix"."ClinVarFullRelease_Loaded.xml");
#system("cp ClinVar.old.txt ClinVar.parsed.txt");
print "ClinVar parsed. Proceeding with storing to DB\n";
$dbh->do("TRUNCATE TABLE `$table_prefix"."ClinVar_db`");

# Load XRef links
$xquery = "SELECT id, `XRef_Key`,`HRef_Name`, `XRef_url`,`ValueRegEx` FROM `ClinVar_XRefs`";
my $sth = $dbh->prepare($xquery);
$sth->execute();
my $rowcache = $sth->fetchall_arrayref();
$sth->finish();
our %RefHash = ();
our @replace = ("a".."z");
unshift(@replace,'');
foreach(@$rowcache) {
	$RefHash{$_->[1]}{'HREF'} = $_->[2];
	$RefHash{$_->[1]}{'URL'} = $_->[3];
	$RefHash{$_->[1]}{'RegEx'} = $_->[4];
}

## mail new XRef keys.
my %mail = ();

## load Value_Codes
my %codes = ();
$csth = $dbh->prepare("SELECT id, `Table_x_Column`, `Item_Value` FROM `Value_Codes` WHERE `Table_x_Column` LIKE 'Variants_x_ClinVar%'");
$csth->execute();
while (my @r = $csth->fetchrow_array()) {
        $codes{$r[1]}{$r[2]} = $r[0];
}
$csth->finish();

# prepare insert query.
$sth = $dbh->prepare("INSERT DELAYED INTO `$table_prefix"."ClinVar_db` (`id`,`Chr`, `Start`,`Stop`,`VarType`,`RefAllele`,`AltAllele`,`LastUpdate`,`Gene`,`Disease`,`Class`,`ClassComment`,`NM_ids`,`NP_ids`,`Effects`,`VarXRefs`,`GeneXRefs`,`DiseaseXRefs`,`PubMed`,IsHaplotype) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
open IN, $table_prefix."ClinVar.parsed.txt";
my $skipped = 0;
my $total = 0;
while (<IN>) {
	chomp;
	next if ($_ eq "");
	$total++;
	my @p = split(/\t/,$_);
	## skip empty positions
	if ($p[1] eq "" || $p[2] eq "" || $p[3] eq "") {
		$skipped++;
		next;
	}
	# strip leading ||
	for ($i = 0; $i<scalar(@p);$i++) {
		$p[$i] =~ s/^\|\|//;
	}
	## get details
	my $title = $p[5];
	my $disease = $geneSymbol = $cp = "";
	if ($title =~ m/\sAND\s/) {
		($geneSymbol,$disease) = split(/ AND /,$title);
		if ($geneSymbol =~ m/(.*):c\..*/) {
			$geneSymbol = $1;
		}
		else {
			$geneSymbol = "";
		}
	}
	my $haplotype = $p[23];
	## preference over specified preferred name.
	if ($p[20] ne '') {
		$geneSymbol = $p[20];
	}
	# observed alleles
	$ref = $alt = '.';
	if ($p[21] ne '' && $p[22] ne '') { # specified in columns
		$ref = $p[21];
		$alt = $p[22];
	}
	elsif ($p[11] =~ m/N._\d+\.\d+:g\.\d+([ACGT]+)>([ACGT]+)(,|$)/) { # extract from cDNA description (SNV)
		$ref = $1;
		$alt = $2;
	}
	elsif($p[11] =~ m/N._\d+\.\d+:g\.\d+_*\d*(ins|del)([ACGT]+)(,|$)/) { # extract from cDNA description (indel)
		my $indel = $1;
		if ($indel eq 'ins') {
			$ref = '.';
			$alt = $2;
		}
		elsif ($indel eq 'del') {
			$ref = $2;
			$alt = '.';
		}
		else {
			print $p[11]."\n";
		}
	}
	elsif($p[11] =~ m/N._\d+\.\d+:g\.\d+_*\d*del([ACGT]+)ins([ACGT]+)(,|$)/) { # extract from cDNA description (sub)
			$ref = $1;
			$alt = $2;
	}

	## replace pubmed || by ,
	if ($p[19] eq '') {
		$p[19] = '.';
	}
	else {
		$p[19] =~ s/\|\|/,/g;
	}
	## Rephrase GeneXRef to full links. This is more compact as it removes the need to parse at runtime or store for each Variants_x_ClinVar.
	my %gr = ();
	my @grefs = split(/\|\|/,$p[16]);
	foreach(@grefs) {
		my ($key,$value) = split(/\|/,$_);
		if (!defined($RefHash{"Gene|$key"})) {
			$mail{"Gene|$key"} = 1;
			next;
		}
		# get url
		my $url = $RefHash{"Gene|$key"}{'URL'};
		next if ($url eq '-1');
		# replace val, chr, genesymbol, start, stop
		$url =~ s/\$chr/$chromhash{$p[1]}/g;
		$url =~ s/\$start/$p[2]/g;
		$url =~ s/\$end/$p[3]/g;
		$url =~ s/\$val/$value/g;
		$url =~ s/\$gene/$geneSymbol/g;
		# process regex if defined.
		if ($RefHash{"Gene|$key"}{'RegEx'} ne '') {
			my ($reg,$val) = split(/\|\|/,$RefHash{"Gene|$key"}{'RegEx'});
			my @vals = split(/,/,$val);
			$value =~ m/$reg/;
			foreach(@vals) {
				my $torep = '\$'.$replace[$_];
				my $with = $$_;
				$url =~ s/$torep/$with/g;
			}
		}
		$gr{"<a href=\"$url\" target=\"_blank\">".$RefHash{"Gene|$key"}{'HREF'}."</a>"} = 1;
	}
	my $grs = join(", ",sort {$a cmp $b } keys(%gr));
	## Rephrase DiseaseXRef to full links. This is more compact as it removes the need to parse at runtime or store for each Variants_x_ClinVar.
	my %dr = ();
	my @drefs = split(/\|\|/,$p[17]);
	foreach(@drefs) {
		my ($key,$value) = split(/\|/,$_);
		if (!defined($RefHash{"Disease|$key"})) {
			$mail{"Disease|$key"} = 1;
			next;
		}
		# get url
		my $url = $RefHash{"Disease|$key"}{'URL'};
		next if ($url eq '-1');
		# replace val, chr, genesymbol, start, stop
		$url =~ s/\$chr/$chromhash{$p[1]}/g;
		$url =~ s/\$start/$p[2]/g;
		$url =~ s/\$end/$p[3]/g;
		$url =~ s/\$val/$value/g;
		$url =~ s/\$gene/$geneSymbol/g;
		# process regex if defined.
		if ($RefHash{"Disease|$key"}{'RegEx'} ne '') {
			my ($reg,$val) = split(/\|\|/,$RefHash{"Disease|$key"}{'RegEx'});
			my @vals = split(/,/,$val);
			$value =~ m/$reg/;
			foreach(@vals) {
				my $torep = '\$'.$replace[$_];
				my $with = $$_;
				$url =~ s/$torep/$with/g;
			}
		}
		$dr{"<a href=\"$url\" target=\"_blank\">".$RefHash{"Disease|$key"}{'HREF'}."</a>"} = 1;
	}
	my $drs = join(", ",sort {$a cmp $b } keys(%gr));
	## Rephrase VariantXRef to full links. This is more compact as it removes the need to parse at runtime or store for each Variants_x_ClinVar.
	my %vr = ();
	my @vrefs = split(/\|\|/,$p[15]);
	foreach(@vrefs) {
		my ($key,$value) = split(/\|/,$_);
		if (!defined($RefHash{"Variant|$key"})) {
			$mail{"Variant|$key"} = 1;
			next;
		}
		# get url
		my $url = $RefHash{"Variant|$key"}{'URL'};
		next if ($url eq '-1');
		# replace val, chr, genesymbol, start, stop
		$url =~ s/\$chr/$chromhash{$p[1]}/g;
		$url =~ s/\$start/$p[2]/g;
		$url =~ s/\$end/$p[3]/g;
		$url =~ s/\$val/$value/g;
		$url =~ s/\$gene/$geneSymbol/g;
		# process regex if defined.
		if ($RefHash{"Variant|$key"}{'RegEx'} ne '') {
			my ($reg,$val) = split(/\|\|/,$RefHash{"Variant|$key"}{'RegEx'});
			my @vals = split(/,/,$val);
			$value =~ m/$reg/;
			foreach(@vals) {
				my $torep = '\$'.$replace[$_];
				my $with = $$_;
				$url =~ s/$torep/$with/g;
			}
		}
		$vr{"<a href=\"$url\" target=\"_blank\">".$RefHash{"Variant|$key"}{'HREF'}."</a>"} = 1;
	}
	my $vrs = join(", ",sort {$a cmp $b } keys(%gr));

	# store
	if ($p[9] =~ m/copy number/i) {
		$geneSymbol = '-cnv-';
	}
	if (!defined($p[12]))  {
		#print "empty p[14]\n";
		$p[12] = '';
	}
	if (!defined($p[14])) {
		$p[14] = '';
	}
	## code class
	if (!defined($codes{"Variants_x_ClinVar_Class"}{$p[7]})) {
                  my $ncsth = $dbh->prepare("INSERT INTO `Value_Codes` (`Table_x_Column`,`Item_Value`) VALUES ('Variants_x_ClinVar_Class',?)");
                  $ncsth->execute($p[7]);
                  my $id = $dbh->{'mysql_insertid'};
                  $ncsth->finish();
                  $codes{'Variants_x_ClinVar_Class'}{$p[7]} = $id;
         }
         my $Class = $codes{'Variants_x_ClinVar_Class'}{$p[7]};

	## code classcomment
	if (!defined($codes{"Variants_x_ClinVar_ClassComment"}{$p[8]})) {
                  my $ncsth = $dbh->prepare("INSERT INTO `Value_Codes` (`Table_x_Column`,`Item_Value`) VALUES ('Variants_x_ClinVar_ClassComment',?)");
                  $ncsth->execute($p[8]);
                  my $id = $dbh->{'mysql_insertid'};
                  $ncsth->finish();
                  $codes{'Variants_x_ClinVar_ClassComment'}{$p[8]} = $id;
         }
         my $ClassComment = $codes{'Variants_x_ClinVar_ClassComment'}{$p[8]};



	$sth->execute($p[0],$chromhash{$p[1]},$p[2],$p[3],$p[9],$ref, $alt, $p[6], $geneSymbol, $disease, $Class,$ClassComment,$p[10],$p[12],$p[14],$vrs,$grs,$drs,$p[19],$haplotype) ;
	
}
close IN;
$sth->finish();

open OUT, ">ClinVar.stats.txt";
print OUT "version=$online\n";
print OUT "entries=$total\n";
print OUT "skipped=$skipped\n";
close OUT;
print "Skipped $skipped entries due to insufficient location information\n";
if ($validation == 0) {
   if (keys(%mail) > 0) {
	open OUT, ">/tmp/mail.new.keys.txt";
	print OUT "to: geert.vandeweyer\@uantwerpen.be\n";
	print OUT "subject: New clinvar keys\n";
	print OUT "from: geert.vandeweyer\@uantwerpen.be\n";
	print OUT "\n";
	foreach(keys(%mail)) {
		print OUT " - $_\n";
	}
	close OUT;
	system("sendmail -t < /tmp/mail.new.keys.txt");
	system("rm  /tmp/mail.new.keys.txt");
  }
}
