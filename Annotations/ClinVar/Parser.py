#!/usr/bin/env python
import time
start_time = time.time()
import xml.dom.pulldom as pulldom
import xml.etree.ElementTree as ET
import csv
import re
import urllib
import urllib2
import os.path
import sys    # sys.setdefaultencoding is cancelled by site.py
reload(sys)    # to re-enable sys.setdefaultencoding()
sys.setdefaultencoding('utf-8')

def getInnerText(oNode):
    rc = ""
    nodelist = oNode.childNodes
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc = rc + node.data
        elif node.nodeType==node.ELEMENT_NODE:
            rc = rc + getInnerText(node)   # recursive !!!
        elif node.nodeType==node.CDATA_SECTION_NODE:
            rc = rc + node.data
        else:
            # node.nodeType: PROCESSING_INSTRUCTION_NODE, COMMENT_NODE, DOCUMENT_NODE, NOTATION_NODE and so on
           pass
    return rc



# prepare
if (len(sys.argv) <= 1 ):
	print "no genome build specified"
	exit()	
else: 
	dbbuild = sys.argv[1]

if (len(sys.argv) == 3) :
	prefix = sys.argv[2]
else:
	prefix = '';

print "loading: "+'/tmp/'+prefix+'ClinVarFullRelease_Loaded.xml'
events = pulldom.parse('/tmp/'+prefix+'ClinVarFullRelease_Loaded.xml')

## read in nm_/np_ translations.
refseq_map = {}
regex = re.compile(r"^(.*)\.\d+$", re.IGNORECASE)
with open(prefix+'refseq_links.txt','rb') as csvfile:
	reader = csv.reader(csvfile,delimiter="\t")
	for line in reader:
		line[0] = re.sub(r"^(.*)\.\d+$",r'\1',line[0])
		line[1] = re.sub(r"^(.*)\.\d+$",r'\1',line[1])
		refseq_map[line[0]] = line[1]

# read in history id replacements.
replacements = {}
if not os.path.exists(prefix+'refseq_replaced.txt'):
	with open(prefix+'refseq_replaced.txt','a') as repfile:
		print "Created empty refseq_replaced.txt file"

with open(prefix+'refseq_replaced.txt','rb') as csvfile:
	reader = csv.reader(csvfile,delimiter="\t")
	for line in reader:
		replacements[line[0]] = line[1]

outfile = open(prefix+'ClinVar.parsed.txt','w')
# initialize variables.
counter = 0
active =  0
GetName = ''
XRefType = ''
RS_NM = {} 
RS_NP = {}
Title = ''
ID = ''
LastUpdate = ''
ClinSign = ''
ClinSignStat = ''
msType = ''
RS_NC = ''
RS_EX = ''
MolCons = ''
PubMed = ''
XRefMolCons = ''
XRefGene = ''
XRefDisease = ''
GeneSymbol = ''
refallele = ''
altallele =  ''
DiseaseName = ''
chrom = ''
start = ''
stop = ''
strand = ''
haplotype = 0
GeneInfo = 0
variants = []
for event,node in events:
	if event == 'START_ELEMENT' :
		# haplotype block : contains multiple variants.
		if node.tagName == 'MeasureSet' and node.getAttribute('Type') == 'Haplotype':
			haplotype = 1

		elif node.tagName == 'ClinVarSet':
			counter += 1
			active = 1
			#ID = node.getAttribute('ID')
			XRefType = 'MolCons'
		# pubmed identifiers.
		elif node.tagName == 'ID' and node.getAttribute('Source') == 'PubMed':
			events.expandNode(node)
			PubMed = "%s||%s" %(PubMed,getInnerText(node))	
		# active is set to 1 for each ClinVarSet, and set to zero at the end of ReferenceClinVarAssertion tag.
		elif active == 1: 
			# title : holds nm_:c.point, and disease name
			if node.tagName == 'Title':
				# title
				events.expandNode(node)
				Title = getInnerText(node)
			# this is the main block holding all important info.
			elif  node.tagName == 'ReferenceClinVarAssertion':
				# Last update on reference entry
				LastUpdate = node.getAttribute('DateLastUpdated')
			elif node.tagName == 'ClinVarAccession': 
				ID = node.getAttribute('Acc')
			# Clinical Significance and its status
       			elif node.tagName == 'ClinicalSignificance':
				events.expandNode(node)
  				ClinSign = getInnerText(node.getElementsByTagName('Description')[0])
				ClinSignStat = getInnerText(node.getElementsByTagName('ReviewStatus')[0])
			## variant entries
			elif node.tagName == 'Measure':
				msType = node.getAttribute('Type')
				#GetName = 'Measure'
			## name tag: present for multiple items.
			elif node.tagName == 'Attribute' : 
				atype = node.getAttribute('Type')
				if atype == 'HGVS, protein, RefSeq' :
					events.expandNode(node)
					nodeContents = getInnerText(node).split(':')
					acc = nodeContents[0].split('.')
					RS_NP[acc[0]] = {}
					if not acc[1]:
						acc[1] = 1
					RS_NP[acc[0]]['version'] = acc[1]
					RS_NP[acc[0]]['change'] = nodeContents[1]
				elif atype == 'HGVS, coding, RefSeq' :
					events.expandNode(node)
					nodeContents = getInnerText(node).split(':')
					acc = nodeContents[0].split('.')
					RS_NM[acc[0]] = {}
					if not acc[1]:
						acc[1] = 1
					RS_NM[acc[0]]['version'] = acc[1]
					RS_NM[acc[0]]['change'] = nodeContents[1]

				elif atype == 'MolecularConsequence':
					events.expandNode(node)
					MolCons = "%s||%s" %(MolCons,getInnerText(node))
					# set xreftype for selecting action below.
					XRefType = 'MolCons'

			elif node.tagName == 'XRef':
				if XRefType == 'MolCons':
					if node.getAttribute('DB') != 'RefSeq':
						# variant external references.
						XRefMolCons = "%s||%s|%s" %(XRefMolCons,node.getAttribute('DB'),node.getAttribute('ID'))
				elif XRefType == 'Gene':
					XRefGene = "%s||%s|%s" %(XRefGene,node.getAttribute('DB'),node.getAttribute('ID'))
				elif XRefType == 'Disease':
					XRefDisease = "%s||%s|%s" %(XRefDisease,node.getAttribute('DB'),node.getAttribute('ID'))				

			elif node.tagName == 'SequenceLocation':
				if GeneInfo != 1:
					if node.getAttribute('Assembly') == dbbuild:
						chrom = node.getAttribute('Chr')
						if node.getAttribute('start'):
							start = node.getAttribute('start')
						elif node.getAttribute('innerStart'):
							start = node.getAttribute('innerStart')
						if node.getAttribute('stop'):
							stop = node.getAttribute('stop')
						elif node.getAttribute('innerStop'):
							stop = node.getAttribute('innerStop')
                        if node.getAttribute('referenceAlleleVCF'):
    						refallele = node.getAttribute('referenceAlleleVCF')
						elif node.getAttribute('referenceAllele'):
							refallele = node.getAttribute('referenceAllele') 
                        if node.getAttribute('alternateAlleleVCF'):
    						altallele = node.getAttribute('alternateAlleleVCF')
						elif node.getAttribute('alternateAllele'):
							altallele = node.getAttribute('alternateAllele')
	
						strand = node.getAttribute('Strand')
	
			elif node.tagName == 'MeasureRelationship':
				GeneInfo = 1	
				XRefType = 'Gene'
			
			elif node.tagName == 'Symbol' and GeneInfo == 1:
				events.expandNode(node)
				cnodes = node.getElementsByTagName('ElementValue')
				for cnode in cnodes:
					if cnode.getAttribute('Type') == 'Preferred':
						GeneSymbol = getInnerText(cnode)
				
			elif node.tagName == 'Trait':
				XRefType = 'Disease'

			elif node.tagName == 'ElementValue' and XRefType == 'Disease' and node.getAttribute('Type') == 'Preferred' and DiseaseName == '':
				events.expandNode(node)
				DiseaseName = getInnerText(node) 
					

   	elif event == 'END_ELEMENT': 
		if node.tagName == 'ReferenceClinVarAssertion':
			active = 0
			if ID == '':
				print "Missing RCV entry"
				print "RefClin ",counter,"Ended: ",ID,Title,LastUpdate,ClinSign,ClinSignStat
				
		elif node.tagName == 'MolecularConsequence' and XRefType == 'MolCons':
				XRefType = ''
		elif node.tagName == 'MeasureRelationship':
				if XRefType == 'Gene':
					XRefType = 'MolCons'
				GeneInfo = 0
		elif node.tagName == 'Measure' and active == 1:
			# build RS_NM string.
			RS_NMs = '';
			RS_NPs = '';
			for nm in RS_NM:
				RS_NMs = "%s||%s.%s:%s" %(RS_NMs, nm,RS_NM[nm]['version'],RS_NM[nm]['change'])
				## nm not in the map 
				while not nm in refseq_map:
					# known replacement
					if nm in replacements:
						nm = replacements[nm]
						continue
					# don't use this, it takes too long.
					
					break
					# not seen, try to get replacement value.	
					data = urllib2.urlopen("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=nucleotide&term="+nm)
					root = ET.parse(data)
					idlist = root.find('IdList')
					nmid = idlist.find('Id').text
					data = urllib2.urlopen("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=nucleotide&id="+nmid)
					root = ET.parse(data)
					new_nm = ''
					for item in root.iter('Item'):
						if item.get('Name') == 'ReplacedBy':
							new_nm = item.text
							break
					if not new_nm or new_nm == 'None':
						print "No replacement value found."
						break
					replacements[nm] = new_nm
					with open(prefix+"refseq_links.txt", "a") as repfile:
						
    						repfile.write(nm+"\t"+new_nm)
					nm = new_nm
				# not in map, so NP is lost.
				if not nm in refseq_map or len(RS_NP) == 0:
					RS_NPs = "%s||." %(RS_NPs)
					continue
				# get the mapped NP.
				np = refseq_map[nm]
				## np not in the dict
				while not np in RS_NP:
					if np in replacements:
						np = replacements[np]
						continue
					# don't use this.
					break
					# try to get replacement value.	
					data = urllib2.urlopen("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=protein&term="+np)
					root = ET.parse(data)
					idlist = root.find('IdList')
					npid = idlist.find('Id').text
					data = urllib2.urlopen("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=protein&id="+npid)
					root = ET.parse(data)
					new_np = ''
					for item in root.iter('Item'):
						if item.get('Name') == 'ReplacedBy':
							new_np = item.text
							break
					if not new_np or new_np == 'None':
						print "no replacement value found for",np
						break
					replacements[np] = new_np
					with open(prefix+"refseq_links.txt", "a") as repfile:
    						repfile.write(np+"\t"+new_np)
					np = new_np

				if np in RS_NP:
					RS_NPs = "%s||%s.%s:%s" %(RS_NPs, np,RS_NP[np]['version'],RS_NP[np]['change'])
				else:
					RS_NPs = "%s||." %(RS_NPs)
					

			variants.append([chrom, start, stop, strand, msType, RS_NMs, RS_NC, RS_NPs,RS_EX,MolCons,XRefMolCons,XRefGene,GeneSymbol,refallele,altallele])
			chrom = ''
			start = ''
			stop = ''
			strand = ''
			msType = ''
			RS_NMs = ''
			RS_NC = ''
			RS_NPs = ''
			RS_EX = ''
			MolCons= ''
			XRefMolCons = ''
			XRefGene = ''
			GeneSymbol = ''
			refallele = ''
			altallele = ''
			RS_NM = {} 
			RS_NP = {}

		elif node.tagName == 'ClinVarSet':
			for variant in variants:
				## end of the ClinVar entry. print out results. Taken out of the 'active' block, since some pubmeds were relocated.
				outfile.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (ID, variant[0],variant[1],variant[2],variant[3],Title, LastUpdate, ClinSign, ClinSignStat, variant[4], variant[5], variant[6], variant[7],variant[8],variant[9],variant[10],variant[11],XRefDisease,DiseaseName,PubMed,variant[12],variant[13],variant[14],haplotype))

			Title = ''
			ID = ''
			LastUpdate = ''
			ClinSign = ''
			ClinSignStat = ''
			msType = ''
			RS_NMs = ''
			RS_NC = ''
			RS_NPs = ''
			RS_EX = ''
			MolCons =  ''
			PubMed = ''
			GeneSymbol = ''
			refallele = ''
			altallele = ''
			RS_NM = {}
			RS_NP = {}
			XRefMolCons = ''
			XRefGene = ''
			XRefDisease = ''
			chrom = ''
			start = ''
			stop = ''
			strand = ''
			DiseaseName = ''
			variants  = []
			haplotype = 0	
outfile.close()


print time.time() - start_time, "seconds"


exit()


 
