from CMG.Utils import Re
import configparser
import argparse
import os
import sys
import time
import shelve

# add the cgi-bin/Modules to path.
sys.path.append("../../cgi-bin/Modules")
from Utils import Annotator, PopList, StartLogger


class DataError(Exception):
    pass


## some variables/objects
re = Re()  # customized regex-handler

# load ini and CLI arguments
def LoadConfig():
    if not os.path.isfile("../../../.Credentials/.credentials"):
        raise DataError("Failure : Could not locate credentials file.")
    config = configparser.ConfigParser()
    config.read("../../../.Credentials/.credentials")

    ## command line arguments
    help_text = """
        GOAL:
        #####
            - Annotate all NEW variants with ClinVar data & store to DB
            - Annotate all Variants & store to dedicated validation table in DB
            - Annotate a VCF file & store to new VCF file.
        """
    # arguments : config file, simulate , help
    parser = argparse.ArgumentParser(
        description=help_text, formatter_class=argparse.RawTextHelpFormatter
    )
    # genome build
    parser.add_argument(
        "-b", "--build", required=False, help="Genome Build, defaults to current VDB version"
    )
    parser.add_argument(
        "-a",
        "--annotation",
        required=False,
        help="Dummy argument to preserve compatibility with Annovar version",
    )
    # validation mode
    parser.add_argument(
        "-v",
        "--validate",
        type=str2bool,
        nargs="?",
        const=True,
        default=False,
        required=False,
        help="Run in Validation Mode. All variants are annotated, and stored in a separate table.",
    )
    # file mode
    parser.add_argument(
        "-f", "--infile", required=False, help="Input VCF file to use in file-based mode."
    )
    parser.add_argument(
        "-o", "--outfile", required=False, help="Output VCF file to use in file-based mode."
    )
    parser.add_argument("-l","--loglevel",required=False,help="Override loglevel")
    args = parser.parse_args()
    if args.loglevel:
        config["LOGGING"]["LOG_LEVEL"] = args.loglevel
    # if validation, add the prefix.
    if args.validate:
        args.prefix = "Validation_"
    else:
        args.prefix = ""
    return config, args


def LoadVariants():
    tmp_table = "{}_annotations_{}".format(annotator.args.table_name, time.time())
    try:
        log.info("Creating table : {}".format(tmp_table))

        annotator.dbh.doQuery(
            "CREATE TABLE `{}` (`id` VARCHAR(255), `chr` INT(2), `start` INT(11), `stop` INT(11), `Ref` VARCHAR(255), `Alt` VARCHAR(255)) ENGINE=MEMORY CHARSET=latin1".format(
                tmp_table
            )
        )
        annotator.dbh.doQuery(
            f"ALTER TABLE `{tmp_table}` ADD INDEX (`chr`), ADD INDEX (`start`), ADD INDEX (`stop`)"
        )

    except Exception as e:
        log.error("Could not create tmp table for ClinVar: {}".format(e))
        sys.exit(1)

    # from VCF
    if annotator.args.infile:
        load_file = os.path.join(
            config["LOCATIONS"]["SCRIPTDIR"],
            "Annotations",
            annotator.method,
            "tmp_files",
            "{}.list.txt".format(tmp_table),
        )
        log.info("Prepare input VCF with Variants to annotate")
        annotator.files.append(load_file)
        with open(annotator.args.infile, "r") as fh, open(load_file, "w") as fo:
            for line in fh:
                if line.startswith("#"):
                    continue
                cols = line.rstrip().split("\t")
                # use vid if provided
                if re.search(r"^\d+$", cols[2]):
                    vid = cols[2]
                elif re.findall(r"id=(\d+)[;\t]", cols[7]):
                    vid = re.last_match[0]
                # else : make generic
                else:
                    vid = "{}:{}:{}:{}".format(cols[0], cols[1], cols[3], cols[4])
                cols[0] = cols[0].replace("chr", "")
                # expected 1:22-XYM
                
                numeric_chr = annotator.ConvertChr(cols[0])
                log.debug(f"Converted {cols[0]} to {numeric_chr}")
                # vid, chr, start, end, ref, alt
                stop = int(cols[1]) + len(cols[3]) - 1
                fo.write(
                    "{}\t{}\t{}\t{}\t{}\t{}\n".format(
                        vid, numeric_chr, cols[1], stop, cols[3], cols[4]
                    )
                )

        # load to DB.
        try:
            log.debug("LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}`".format(load_file, tmp_table))
            annotator.dbh.doQuery(
                "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}`".format(load_file, tmp_table)
            )
        except Exception as e:
            log.error("Could not load VCF data into tmp table: {} : {}".format(tmp_table, e))
            sys.exit(1)
    else:
        log.info("Loading Variants to annotate")
        # validation => all variants.
        if annotator.args.validate:
            try:
                
                annotator.dbh.doQuery(
                    "INSERT INTO `{}` SELECT `id`, `chr`, `start`, `stop`, `RefAllele`, `AltAllele` FROM `Variants`".format(
                        tmp_table
                    )
                )
            except Exception as e:
                log.error(
                    "Could not transfer all variants into ClinVar tmp table (table is kept): {}: {}".format(
                        tmp_table, e
                    )
                )
                sys.exit(1)
        # regular usage : all non-existing variants.
        else:
            try:
                annotator.dbh.doQuery(
                    "INSERT INTO `{}` SELECT v.`id`, v.`chr`, v.`start`, v.`stop`, v.`RefAllele`, v.`AltAllele` FROM `Variants` v WHERE v.id NOT IN (SELECT va.vid FROM  `{}` va )".format(
                        tmp_table, annotator.args.table_name
                    )
                )
            except Exception as e:
                log.error(
                    "Could not extract novel variants into ClinVar tmp table: {} : {}".format(
                        tmp_table, e
                    )
                )
                sys.exit(1)
    return tmp_table


def AnnotateVariants(tmp_table):
    # tmp file write results before loading to DB
    tmp_file = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations",
        annotator.method,
        "tmp_files",
        "{}.load.txt".format(tmp_table),
    )
    annotator.files.append(tmp_file)
    fh = open(tmp_file, "w")
    # load codings.
    for column in ["AA_MatchType", "Effect", "MatchType", "Class", "ClassComment"]:
        annotator.GetCode(column)

    # queries :
    cv_query = "SELECT id AS VariantID, Start, Stop, RefAllele, AltAllele,  `NM_ids`, `NP_ids`, Effects  FROM `{}ClinVar_db` WHERE VarType IN ('single nucleotide variant','indel') AND  `chr` = %s ORDER BY Start ASC, Stop ASC".format(
        annotator.args.prefix
    )

    for chromosome in list(range(1, 25)):
        # all from clinvar
        log.info("processing chromosome {}".format(chromosome))
        cv_variants = annotator.dbh.runQuery(cv_query, chromosome)
        # shortcut for missing variants.
        # cv_id \t matchtype \t nm \t np \t eff \t aamatch \t equalAlleles
        missing_variant = ".\t{}\t.\t.\t{}\t{}\t0".format(
            annotator.GetCode("MatchType", "."),
            annotator.GetCode("Effect", "."),
            annotator.GetCode("AA_MatchType", "."),
        )
        log.debug(missing_variant)
        # slices for variants; 5K / slice. if too high, mysql times out
        log.info(f"Fetching variants for chromosome {chromosome}")
        variants = annotator.dbh.runQuery(
            "SELECT * FROM `{}` WHERE chr = %s ORDER BY start ASC, stop ASC".format(tmp_table),
            chromosome,
            size=5000,
        )

        while len(variants) > 0:
            # process
            for variant in variants:
                log.debug("Actual Variant:")
                log.debug(variant)
                # skip gvcf entries
                if "non_ref" in variant["Alt"]:
                    # print as missing to outfile.
                    fh.write("{}\t{}\n".format(variant["id"], missing_variant))
                    continue
                # 'correct' N alleles
                if "N" in variant["Alt"]:
                    log.info(
                        "Variant contains invalid alt argument, replacing N by A: {}".format(
                            variant["Alt"]
                        )
                    )
                    variant["Alt"] = variant["Alt"].replace("N", "A")
                # track slice to remove before going to next variant
                pop_cnt = 0
                hit = False
                for i, cv_variant in enumerate(cv_variants):
                    # upstream of variant. take 50kb margin to allow large clinvar variants.
                    if variant["start"] > cv_variant["Stop"] + 50000:
                        pop_cnt = i
                        continue
                    # passed the variant.
                    if variant["stop"] < cv_variant["Start"]:
                        break

                    ## GET MATCH TYPE
                    overlap_type = ""
                    # same start => can be exact match
                    if variant["start"] == cv_variant["Start"]:
                        # exact match
                        if (
                            variant["stop"] == cv_variant["Stop"]
                            and variant["Ref"] == cv_variant["RefAllele"]
                            and variant["Alt"] == cv_variant["AltAllele"]
                        ):
                            overlap_type = "exact"
                        else:
                            overlap_type = "overlap"

                    # else: check overlap of any kind:
                    elif int(variant["start"]) <= int(cv_variant["Stop"]) and int(
                        variant["stop"]
                    ) >= int(cv_variant["Start"]):
                        overlap_type = "overlap"
                    else:
                        # due to the extension of 50K , this is now possible.
                        #log.warning(
                        #    "No overlap found, this is not expected : {} vs {}".format(
                        #        variant, cv_variant
                        #    )
                        #)
                        # move to next CV variant
                        continue
                    
                    log.debug("Overlapping ClinVar entry")
                    log.debug(cv_variant)
                    log.debug("")
                    matchtype = annotator.GetCode("MatchType", overlap_type)
                    hit = True
                    ## equal alleles:
                    if cv_variant['RefAllele'] == cv_variant['AltAllele'] and not cv_variant['RefAllele'] in ['','.']:
                        equal_alleles = 1
                    else:
                        equal_alleles = 0
                    ## get gene info
                    nms = [] if cv_variant["NM_ids"] == "" else cv_variant["NM_ids"].split("||")
                    nps = [] if cv_variant["NP_ids"] == "" else cv_variant["NP_ids"].split("||")
                    effects = (
                        [] if cv_variant["Effects"] == "" else cv_variant["Effects"].split("||")
                    )
                    # start from transcript ids
                    if len(nms) > 0:
                        for idx, nm in enumerate(nms):
                            np = '.' 
                            eff = annotator.GetCode("Effect",'.')
                            aamatch = annotator.GetCode('AA_MatchType',".")
                            if idx < len(nps):
                                np = nps[idx]
                                aamatch = GetAminoAcidOverlap(variant, np, nm)
                                aamatch = annotator.GetCode("AA_MatchType", aamatch)
                                log.debug(f"Variant ID : {variant['id']} : cv_id {cv_variant['VariantID']} ; np: {np} match {aamatch} from nms")
                            if idx < len(effects):
                                eff = effects[idx]
                                # remove trailing 'variant' in effect
                                eff = re.sub(r"[\s_]variant", "", eff)
                                log.debug(f" variant {variant['id']} effect in nms: {eff}")
                                eff = annotator.GetCode("Effect", eff)
                                log.debug(f"   => coded to {eff}")
                            # write out.
                            fh.write(
                                "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
                                    variant["id"], cv_variant["VariantID"], matchtype, nm, np, eff, aamatch,equal_alleles
                                )
                            )
                    elif len(nps) > 0:
                        for idx, np in enumerate(nps):
                            nm = '.' 
                            eff = annotator.GetCode("Effect",'.')
                            aamatch = annotator.GetCode('AA_MatchType',".")
                            if idx < len(effects):
                                eff = effects[idx]
                                # remove trailing 'variant' in effect
                                eff = re.sub(r"[\s_]variant", "", eff)
                                eff = annotator.GetCode("Effect", eff)
                            aamatch = GetAminoAcidOverlap(variant, np, nm)
                            aamatch = annotator.GetCode("AA_MatchType", aamatch)
                            log.debug(f"Variant ID : {variant['id']} : match {aamatch} from nps")
                            fh.write(
                                "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
                                    variant["id"], cv_variant["VariantID"], matchtype, nm, np, eff, aamatch,equal_alleles
                                )
                            )


                    elif len(effects) > 0:
                        for idx, eff in enumerate(effects):
                            nm = '.'
                            np = '.'
                            aamatch = annotator.GetCode('AA_MatchType','.')
                            # remove trailing 'variant' in effect
                            eff = re.sub(r"[\s_]variant", "", eff)
                            eff = annotator.GetCode("Effect", eff)
                            #aamatch = annotator.GetCode("AA_MatchType", aamatch)
                            fh.write(
                                "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
                                    variant["id"], cv_variant["VariantID"], matchtype, nm, np, eff, aamatch,equal_alleles
                                )
                            )
                    else:
                        nm = np = "."
                        eff = annotator.GetCode("Effect", ".")
                        aamatch = annotator.GetCode("AA_MatchType", ".")
                        fh.write(
                            "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
                                variant["id"], cv_variant["VariantID"], matchtype, nm, np, eff, aamatch,equal_alleles
                            )
                        )
                    
                # no clinvar found : add as missing
                if not hit:
                    fh.write("{}\t{}\n".format(variant["id"], missing_variant))
                # strip some cv_variants to prevent double iterations.
                if pop_cnt > 0:
                    log.debug(f"removing {pop_cnt} clinvar entries")
                    del cv_variants[0:pop_cnt]

            ## if here, variants from previous round was not empty. next round.
            variants = annotator.dbh.GetNextBatch()

    # all done; close file
    log.info("Processing finished")
    fh.close()
    return tmp_file


def GetAminoAcidOverlap(variant, np, nm):
    # no overlap.
    if np == ".":
        return "."
    if not re.findall(r".*:(p.*)", np):
        log.warning("Unexpected protein notation in ClinVar: {} -- {} : {}".format(nm, np, variant))
        return "."
    np_full = re.last_match[0]
    # extract gene info from refgene annotations
    if nm == ".":
        log.warning("no transcript, cannot extract refgene info: {}".format(np))
        return "overlap"
    if re.findall(r"(.*)\.\d+:c\..*", nm):
        sql_pat = "{}.%".format(re.last_match[0])
    elif re.findall(r"(.*)\.\d+\(.*\):c\..*", nm):
        sql_pat = "{}.%".format(re.last_match[0])
    else:
        log.error(
            "Unexpected transcript notation in ClinVar (setting aamatch to overlap): {}".format(nm)
        )
        return "overlap"
    #sql_pat = "{}.%".format(re.last_match[0])
    rg_rows = annotator.dbh2.runQuery(
        "SELECT CPointAA FROM `Variants_x_ANNOVAR_ncbigene` WHERE vid = %s AND Transcript LIKE %s",
        params=[variant["id"], sql_pat],
    )

    # nothing in refseq => just 'overlap'
    if len(rg_rows) == 0:
        log.debug("no ncbigene results from below query:")
        log.debug(f"SELECT CPointAA FROM `Variants_x_ANNOVAR_ncbigene` WHERE vid = {variant['id']} AND Transcript LIKE {sql_pat}")
        return "overlap"
    
    caa = rg_rows[0]["CPointAA"]
    pnot_full = ""
    for i in list(range(len(caa))):
        log.debug(f"evaluate position {i} : {caa[i]}")
        # single to three letter notation if a valid codon
        try:
            codon = annotator.ConvertCodons(caa[i])
            pnot_full = f"{pnot_full}{codon}"
        # else : take original content
        except Exception as e:
            log.debug(f"could not convert : {e} ")
            pnot_full = f"{pnot_full}{caa[i]}"
        # rewrite synonymous
        if (
            re.findall(r"p.([A-Za-z\*]+)(\d+)([A-Za-z\*]+)", pnot_full)
            and re.last_match[0][0] == re.last_match[0][2]
        ):
            pnot_full = "p.{}{}=".format(re.last_match[0][0], re.last_match[0][1])
    # compare
    if np_full == pnot_full:
        log.debug(f"{np_full} matches {pnot_full}")
        return "match"
    else:
        log.debug(f"{np_full} does not match {pnot_full}")
        return "overlap"


def BuildVCF(load_file):
    # first : complete clinvar data using shelf as on-disk dict.
    sf = os.path.join(
        config["LOCATIONS"]["SCRIPTDIR"],
        "Annotations",
        annotator.method,
        "tmp_files",
        "{}.{}.shelf".format(annotator.args.table_name, time.time()),
    )
    s = shelve.open(sf)
    for suffix in ["bak", "dat", "dir"]:
        annotator.files.append(f"{sf}.{suffix}")

    with open(load_file, "r") as f:
        for line in f:
            cols = line.rstrip().split("\t")
            cv_details = annotator.dbh.runQuery(
                "SELECT Class, Disease, ClassComment FROM `ClinVar_db` WHERE id = %s", cols[1]
            )[0]
            # variant not seen yet.
            if cols[0] not in s:
                s[cols[0]] = ""
            # append new entry.
            s[
                cols[0]
            ] = "{}cvid:{},MatchType:{},NM_ID:{},NP_ID:{},Effect:{},AA_MatchType:{},Class:{},ClassComment:{},Disease:{}|".format(
                s[cols[0]],
                cols[1],
                cols[2],
                cols[3],
                cols[4],
                cols[5],
                cols[6],
                cv_details["Class"],
                cv_details["ClassComment"],
                cv_details["Disease"],
            )

    # then add info to vcf file.
    with open(annotator.args.infile, "r") as f_in, open(annotator.args.outfile, "w") as f_out:
        for line in f_in:
            if line.startswith("#"):
                f_out.write(line)
                continue
            cols = line.rstrip().split("\t")
            # use vid if present
            if re.search(r"^\d+$", cols[2]):
                vid = cols[2]
            elif re.findall(r"id=(\d+)[;\t]", cols[7]):
                vid = re.last_match[0]
            # else : use generic vid
            else:
                vid = "{}:{}:{}:{}".format(cols[0], cols[1], cols[3], cols[4])
            # if clinvar info found:
            if vid in s:
                cols[7] += ";ClinVar={}".format(s[vid][:-1])
            # else : add .
            else:
                cols[7] += ";ClinVar=."

            f_out.write("{}\n".format("\t".join(cols)))
    # close shelve
    s.close()


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def ValidateTables():
    for table in ["ClinVar_db"]:
        if not annotator.dbh.tableExists(f"{annotator.args.prefix}{table}"):
            raise Exception(f"Table missing : {annotator.args.prefix}{table}")
    for table in ["Variants_x_ANNOVAR_ncbigene"]:
        if not annotator.dbh.tableExists(f"{table}"):
            raise Exception(f"Table missing : {table}")


if __name__ == "__main__":
    try:
        config, args = LoadConfig()
    except DataError as e:
        print(e)
        sys.exit(1)
    except Exception as e:
        raise (e)

    # get logger
    log = StartLogger(config, "ClinVar")

    # get the annotator object
    annotator = Annotator(config=config, method="ClinVar", annotation="ClinVar", args=args)

    # increase timeout for receiving data.
    annotator.dbh.doQuery("SET SESSION net_write_timeout=900")
    
    # set table name.
    annotator.args.table_name = "Variants_x_ClinVar_ncbigene"
    annotator.annotation = annotator.dbh.runQuery(
        "SELECT `Value` FROM `Annotation_DataValues` WHERE `Annotation` = 'ClinVar'"
    )[0]["Value"]

    ## Validate options
    try:
        # main table exists ?
        annotator.ValidateTable()
        # other tables exist ?
        ValidateTables()
        # get field/column names:
        annotator.fieldnames = annotator.dbh.getColumns(annotator.args.table_name)["names"]
        # strip aid
        d = annotator.fieldnames.pop(0)
    except (DataError, ValueError) as e:
        log.error(e)
        sys.exit(1)

    # load variants to process into tmp memory table.
    try:
        tmp_table = LoadVariants()
    except Exception as e:
        log.error("Failed to prepare variants for processing: {}".format(e))
        sys.exit(1)

    # annotate
    try:
        load_file = AnnotateVariants(tmp_table)
    except Exception as e:
        log.error("Failed to annotate ClinVar variants in {}: {}".format(tmp_table, e))
        sys.exit(1)

    # load.
    if annotator.args.infile:
        log.info("Building output file")
        try:
            BuildVCF(load_file)
        except Exception as e:
            log.error(
                "Could not build output VCF file: {} => {} : {}".format(
                    load_file, annotator.args.outfile, e
                )
            )
            sys.exit(1)
    else:
        try:
            log.info("Loading data into Database")
            annotator.LoadDataTable(load_file)
            
        except Exception as e:
            log.error(
                "Failed to load annotation data to DB : {} => {}{} : {}".format(
                    load_file, annotator.args.prefix, annotator.args.table_name, e
                )
            )
            sys.exit(1)

    # clean up.
    if not config['LOGGING']['LOG_LEVEL'] == 'DEBUG':
        log.info("Cleaning up...")
        annotator.dbh.doQuery("DROP TABLE IF EXISTS `{}`".format(tmp_table))
        annotator.cleanup()
