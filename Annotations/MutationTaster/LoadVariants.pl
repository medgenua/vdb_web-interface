#!/usr/bin/perl

## only start if the Mechanize is available.
eval
{
  my $toload = "WWW//Mechanize.pm";
  require $toload;
  $module->import();
};
unless($@)
{
	dieWithGrace("Module WWW::Mechanize is not available. MutationTaster Scores will not be fetched.");
}

# other modules
use DBI;
use Getopt::Std;
use Cwd 'abs_path';

# load credentials
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);

# disable buffer
$|++;

####################
## GENERAL HASHES ##
####################
my %chromhash ;
%chromhash = (); 
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_; 
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ "M" } = 25;
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";

###############################
## Do not run double process ##
###############################
if (-e 'Status.txt') {
	$status = 0;
	system("echo $status > Status.txt");
}
else {
	my $status = `cat Status.txt`;
	chomp($status);
	while ($status == 1) {
		## is it really running?
		my $ps = `ps auxwwwe | grep 'perl LoadVariants.pl' | grep MutationTaster | grep -i grep`;
		chomp($ps);
		if ($ps ne '') {
			## not running. update status.
			system("echo 0 > Status.txt");
		}
		else {
			## sleep.
			sleep 60;
		}
		my $status = `cat Status.txt`;
		chomp($status);
	}
}

## ok, ready to start : set status.
$status = 1;
system("echo $status > Status.txt");
###########################
# CONNECT TO THE DATABASE #
###########################
my $db = "NGS-Variants-hg19";
$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}

# opts
# n : (n)ew variants (missing mutation taster scores)
# u : (u)pdate scores older than 6 months (future work)

getopts('nu', \%opts) ;

if (!defined($opts{'n'}) && !defined($opts{'u'})) {
	$opts{'n'} = '';
}
our $anno = 'MutationTaster';
our $file = "Input_Files/$anno.list.txt";

## GET VARIANTS
our $anno = 'MutationTaster';
if (defined($opts{'n'})) {
	$query = "SELECT v.chr, v.start, v.RefAllele, v.AltAllele, v.id FROM `Variants` v LEFT JOIN `Variants_x_MutationTaster` va ON v.id = va.vid WHERE va.vid IS NULL LIMIT 15000";
}
else {
	exit;
}
## label : comes here after data from current query are stored, untill nrrows == 0
our %vids;
NEXTBATCH:
open OUT, ">$file";
print OUT "##fileformat=VCFv4.1\n";
print OUT "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t1\n";

$sth = $dbh->prepare($query);
my $nrrows = $sth->execute();
if ($nrrows < 10000) {
	$max = $nrrows;
}
else {
	$max = 10000;
}

my $rowcache;
if ($nrrows == 0) {
	print "No variants to update. Exiting\n";
	## first clean 
	close OUT;
	system("rm -f $file");
	system("rm -f Output_Files/Results.txt");
	## clear status.
	$status = 0;
	system("echo $status > Status.txt");
	exit();
}
%vids = ();
print "Fetching Variants to update from database\n";
while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
	$result->[0] = $chromhash{$result->[0]};
	my $string = "chr".$result->[0]."\t".$result->[1]."\t.\t".$result->[2]."\t".$result->[3]."\t1000\t.\tAC=1;AF=0.50;AN=2\tGT:AD:DP:GQ:PL\t0/1:50,50:100:1000:1000,0,1000\n";
	## muttast rewrites indels!
	if (length($result->[2]) > length($result->[3])) {
		# deletion.
		# position
		$result->[1] = $result->[1] + 1; #( length($result->[2]) - length($result->[3]));
		# ref : 
		$result->[2] = substr($result->[2],1); # from second pos to end.
		# alt:
		if (length($result->[3]) == 1) {
			$result->[3] = ''; 
		}
		else {
			$result->[3] = substr($result->[3],1);
		}
	}
	if (length($result->[2]) <  length($result->[3])) {	
		# pos++
		$result->[1] = $result->[1] + 1;
		# ref:
		if (length($result->[2]) == 1) {
			$result->[2] = ''; 
		}
		else {
			$result->[2] = substr($result->[2],1);
		}
		# alt: 
		$result->[3] = substr($result->[3],1);
	}	
	## store vid based on pos & alleles (chr is numeric in MTQE output. 1-25)
	$vids{$chromhash{$result->[0]}."-".$result->[1]."-".$result->[2]."-".$result->[3]}{'vid'} = $result->[4];
	$vids{$chromhash{$result->[0]}."-".$result->[1]."-".$result->[2]."-".$result->[3]}{'ok'} = 0;
	print OUT "$string";

}
$sth->finish();
close OUT;
## disconnect from db. 
$dbh->disconnect();
print "Connecting to MutationTaster QE\n";
# come here if fetching results takes abnormally long.
MTQE:
############################
## Connect To QueryEngine ##
############################
# create object
my $mech = WWW::Mechanize->new();

my $url = "http://www.mutationtaster.org/StartQueryEngine.html";

# fetch url
$mech->get( $url) 
	or dieWithGrace("Could not fetch MutationTaster QueryEngine Page",$!);

# select the correct field
$mech->form_name('form') 
	or dieWithGrace("Form 'form' not found on MutationTaster QueryEngine Page",$!);

# set the field values.
my %fields = ('name' => 'VariantDB_Updater', 
		'filename'  => $file, 
		'email' => $adminemail, 
		'no_html' => 1, 
		'homo' => undef,
		'regions_filter' => 0,
		'only_exons' => undef,
		'tgp_filter_homo' => undef,
		'tgp_count_hetero' => undef,
		'min_cov' => 1
);
# fill in form and check if fields exist.
my $form = $mech->form_name('form');
foreach (keys(%fields)) {
	$field = $_;
	$value = $fields{$field};
	if ($form->find_input($field)) {
		eval {
			$mech->field($field,$value)
		};
		if($@){
			#if($@ =~ /No such field/i){
			#	dieWithGrace("Could not set field : 'name'. Field is missing.",$@);
			#}
			#else {
				dieWithGrace("Could not set field : '$field' with '$value'",$@);
			#}
		}
	}
	else {
		dieWithGrace("Could not set field : '$field'. Field is missing.",$@);
	}
}	

# submit.
$mech->click_button('name' => 'Submit');

# base url of new page (other server!)
$base = $mech->uri();
if ($base =~ m/(http:\/\/[^\/]*\/).*/) {
	$base = $1;
}
else {
	dieWithGrace("Could not extract base url from $base");
}
# extract the refresh url from the temp page
if ($mech->content() =~ m/<meta http-equiv="refresh" content="(\d+);\sURL=([^"]*)"/) {
	$sleep = $1;
	$url = $base.$2;
	print "Sleeping $sleep seconds before going to '$url'\n";
	sleep($1);
	eval {
		$mech->get($url);
	};
	$try = 0;
	if (($@ || !$mech->success()) && $try <= 10) {
		sleep 2;
		$try++;
		eval {
			$mech->get($url);
		};
	}
	if ($@ || !$mech->success()) {
		dieWithGrace("Could not fetch '$url'",$!);
	}
}
else {
	dieWithGrace("Could not extract meta-refresh url from submitted form",$mech->content());
}

## wait for the results page to finish. This can take a very long time !
my $waitidx = 1;
while ($mech->content() =~ m/<meta http-equiv="refresh" content="(\d+);\sURL=([^"]*)"/) {
	$url = $2;
	$sleep = 60; # in url = 3, but I don't want to overload the server.
	$base = $mech->uri();
	if ($base =~ m/(http:\/\/[^\/]*\/).*/) {
		$base = $1;
	}
	else {
		dieWithGrace("Could not extract base url from $base");
	}
	$url = $base.$url;
	print "$waitidx/ : Sleeping $sleep seconds before going to '$url'\n";
	$waitidx++;
	if ($waitidx > 20) {
		## waited 20 minutes => not normal, so retry.
		goto MTQE;
	}
	sleep($sleep);
	eval {
		$mech->get($url);
	};
	
	$try = 0;
	if (($@ || !$mech->success() )&& $try <= 10) {
		sleep 2;
		$try++;
		eval {
			$mech->get($url);
		};
	}
	if ($@ || !$mech->success()) {
		dieWithGrace("Could not fetch '$url'",$!);
	}

}

## click export as tsv link.
$mech->form_name("main");
$form = $mech->form_name("main");
$i = 1;
#while ($form->find_input('output',$i)) {
#	if ($mech->value('output',$i) ne 'export as TSV') {
#		print "Field: output nr $i : value=".$mech->value('output',$i)."\n";
#		$i++;
#		next;
#	}
#	else {
#		last;
#	}
#}

# check if the correct button was found.
#if (!$form->find_input('output',$i)) {
#	dieWithGrace("Could not fetch TSV-output due to missing 'output' button.",$!);
#}

# set the tuples field to '' to fetch all variants at once.
$field = "tuples";
$value = '';
if ($form->find_input($field)) {
	eval {
		$mech->field($field,$value)
	};
	if($@){
		#if($@ =~ /No such field/i){
		#	dieWithGrace("Could not set field : 'name'. Field is missing.",$@);
		#}
		#else {
			dieWithGrace("Could not set field : '$field' with '$value'",$@);
		#}
	}
}
else {
	dieWithGrace("Could not set field : '$field'. Field is missing.",$@);
}
	
# submit (might take a few seconds for complete answer).
$mech->click_button('value' => 'export as TSV');

# check for the link.
if (!$mech->find_link( text => 'retrieve TSV file')) {
	dieWithGrace("Could not download results. Link is missing.",$@);
}
## download!
$base = $mech->uri();
if ($base =~ m/(http:\/\/[^\/]*\/).*/) {
	$base = $1;
}
else {
	dieWithGrace("Could not extract base url from $base");
}

my $link = $mech->find_link( text => 'retrieve TSV file');
my $url = $base.$link->url();
print "Downloading results from '$url'\n";
system("wget '$url' -O 'Output_Files/Results.txt.zip'");
system("cd Output_Files && unzip Results.txt.zip && mv export.tsv Results.txt");

###########
## STORE ##
###########
my $db = "NGS-Variants-hg19";
$connectionInfocnv="dbi:mysql:$db:$dbhost"; 
$dbh = DBI->connect($connectionInfocnv,$dbuser,$dbpass) or die('Could not connect to database');
$dbh->{mysql_auto_reconnect} = 1;
## retry on failed connection (server overload?)
$i = 0;
while ($i < 10 && ! defined($dbh)) {
	sleep 7;
	$i++;
	print "Connection to $host failed, retry nr $i/10\n";
	$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
}
#open infile
open IN, 'Output_Files/Results.txt';
my $head = <IN>;
## prepare query
$sth = $dbh->prepare("INSERT DELAYED INTO `Variants_x_MutationTaster` (vid, score, Effect) VALUES (?,?,?)");
%short = ('disease_causing' => 'D', 'disease_causing_automatic' => 'A', 'polymorphism' => 'N', 'polymorphism_automatic' => 'P');
my %stored;
while (<IN>) {
	chomp($_);
	if ($_ eq '') {
		next;
	}
	elsif ($_ =~ m/MutationTaster predictions/) {
		next;
	}
	my @p = split(/\t/,$_);
	my $vid = $vids{$p[0]."-".$p[1]."-".$p[9]."-".$p[10]}{'vid'};
	if ($vid != '') {
		#print "   Found vid = $vid for : ".$p[0]."-".$p[1]."-".$p[9]."-".$p[10]."\n";
		if ($p[3] =~ m/polymorphism/i) {
			# invert score (similar to annovar).
			$p[5] = 1 - $p[5] ;
		}
		if ($p[3] =~ m/n\/a/i) {
			$p[3] = '.';
			$p[5] = '-1';
		}
		else {
			$p[3] = $short{$p[3]};
		}
		if (!defined($stored{"$vid-".$p[5]."-".$p[3]})) {
			$sth->execute($vid,$p[5],$p[3]);
			$stored{"$vid-".$p[5]."-".$p[3]} = 1;
			$vids{$p[0]."-".$p[1]."-".$p[9]."-".$p[10]}{'ok'} = 1;
		}
		
	}
	else {
		#print "ERROR: no vid for : ".$p[0]."-".$p[1]."-".$p[9]."-".$p[10]."\n";
		#print " $_\n";
	}
}
## scan for failed variants and store as such.( not mapped to transcripts, give no result back.)
foreach (keys(%vids)) {
	if ($vids{$_}{'ok'} == 0) {
		$sth->execute($vids{$_}{'vid'},'-1','.');
	}
}
$sth->finish();
## process next batch. 
goto NEXTBATCH;
#exit;

##########
## SUBS ##
##########
sub dieWithGrace {
	my ($message,$error) = @_; 
	
	open OUT, ">dwg.mail.txt";
	print OUT "To: $admin\n";
	print OUT "subject: Mutation Taster Batch Query died\n";
	print OUT "from: $admin\n\n";
	print OUT "Issue:\n";
	print OUT " $message\n";
	print OUT " \n\n $error\n";
	print OUT "\n";
	close OUT;
	## send mail, 
	#system("sendmail -t < 'dwg.mail.txt'");
	print  "####################\n";
	print  "# CRITICAL PROBLEM #\n";
	print  "####################\n";
	print  "\n";
	print  "MTQE died\n";
	print  "Message was:\n";
	print  "$message\n\n";
	print  "$error\n\n";
	print  "\tMail sent to notify admin of crash.\n";
	print  "Monitor will exit now\n";
	exit;
}

