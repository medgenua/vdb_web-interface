<?php
// connect to the database
include('../.LoadCredentials.php');
$db = 'NGS-Variants' . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$userid = $_SESSION['userID'];
require("../inc_memcache.inc");
// define types
$types = array();
$data = array();
$legends = array();
// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");
// get information for plotting.
// known
$rows = runSlicedQuery("SELECT vid AS VariantID FROM `Variants_x_ANNOVAR_snp137` WHERE rsID != -1 AND Clinical = 0 AND vid IN (?)", "Variants_x_ANNOVAR_snp137", $vids);
$data[0] = count($rows);
$legends[0] = 'Known Variants';
// novel
$rows = runSlicedQuery("SELECT vid AS VariantID FROM `Variants_x_ANNOVAR_snp137` WHERE rsID = -1 AND vid IN (?)", "Variants_x_ANNOVAR_snp137", $vids);
$data[1] = count($rows);
$legends[1] = 'Novel Variants';
// clinical
$data[2] = 0; //$row['nr'];
$rows = runSlicedQuery("SELECT vid AS VariantID FROM `Variants_x_ANNOVAR_snp137` WHERE Clinical = 1 AND vid IN (?)", "Variants_x_ANNOVAR_snp137", $vids);
$data[2] = count($rows);
$legends[2] = 'Clinically Associated';


$title = 'Known vs Novel Variants (dbSNP137)';

# the ratio: 
$ratio = number_format(($data[0] + $data[2]) / $data[1], 3);

$json = '{"cols":[{"id":"VarType","label":"Variant Type","type":"string"},{"id":"nrVars","label":"Nr.Variants","type":"number"}],"rows":[';
if ($data[0] + $data[1] + $data[2] == 0) {
    $json .= ']';
} else {
    $json .= '{"c":[{"v":"Known"},{"v":' . $data[0] . '}]},';
    $json .= '{"c":[{"v":"Novel"},{"v":' . $data[1] . '}]},';
    $json .= '{"c":[{"v":"Clinical"},{"v":' . $data[2] . '}]}';
    $json .= ']';
}
$json .= '}';
$json = '{"title":"Known vs Novel Variants (dbSNP137): ' . $ratio . '","data":' . $json . '}';
echo $json;
exit;
