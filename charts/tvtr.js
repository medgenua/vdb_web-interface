function Loadtvtr(random) {
      
        $.ajax({
		url:'charts/tvtr.php',
		dataType:"json",
		async:true,
		success:function(result) {
			var jsonData = result;
			var title = jsonData.title;
			var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
			var options = {
      			    title: title,
			    is3D: true,
	 		    height: 325,
	  		    legend: {'position':'bottom'},
        		};
			var chart = new google.visualization.PieChart(document.getElementById('Chart_tvtr'));
        		chart.draw(data, options);
		}
	});

	/*var jsonData = JSON.parse($.ajax({
		url:'charts/tvtr.php',
		dataType:"json",
		async:false 
	}).responseText);
	//var Data = jsonData.data;
	//var jD = JSON.parse(jsonData);
	var title = jsonData.title;
	var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
	
        var options = {
          title: title,
	  is3D: true,
	  height: 325,
	  legend: {'position':'bottom'},
        };

        var chart = new google.visualization.PieChart(document.getElementById('Chart_tvtr'));
        chart.draw(data, options);
*/

}
