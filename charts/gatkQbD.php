<?php // content="text/plain; charset=utf-8"

// connect to the database
include('../.LoadCredentials.php');
$db = 'NGS-Variants' . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$userid = $_SESSION['userID'];
require("../inc_memcache.inc");
// define types
$types = array();
$data = array();
$legends = array();
// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");
$sids = file_get_contents("/tmp/VariantDB.ChartData.$userid.sids");
$bins = array();
$xlab = array();
// get information for plotting.
$rows = runSlicedQuery("SELECT vid AS VariantID,(AltDepth/(RefDepth+AltDepth)) as AllRat, QualityByDepth, (RefDepth+AltDepth) As TotalDepth FROM `Variants_x_Samples` WHERE sid in ($sids) AND vid IN (?)", "Variants_x_Samples", $vids);

$all = count($rows);
$json = '{"cols":[{"id":"AllellicRatioBin","label":"Allelic Ratio","type":"number"},{"id":"AllellicRatioCount","label":"Count","type":"number"},{"id":"Cummulative","label":"Cummulative Count","type":"number"}],"rows":[';
$cumm = 0;


foreach ($rows as $k => $row) {

    $rounded = number_format(round($row['QualityByDepth'], 0), 0);

    if (array_key_exists($rounded, $bins)) {
        $bins["$rounded"]++;
    } else {
        $bins["$rounded"] = 1;
    }
}
$keys = array_keys($bins);
$max = max($keys);
$data = array();
$cdata = array();
$idx = 0;
$labs = array();
for ($i = 0; $i <= $max; $i++) {
    if (array_key_exists($i, $bins)) {
        $cumm += $bins[$i] / $all;
        $json .= '{"c":[{"v":' . $i . '},{"v":' . $bins[$i] . '},{"v":' . $cumm . '}]},';
    } else {
        $json .= '{"c":[{"v":' . $i . '},{"v":0},{"v":' . $cumm . '}]},';
    }
    $labs[] = $i;
    $idx++;
}
$title = 'Quality By Depth Distribution';
$json = substr($json, 0, -1) . ']}';
$json = '{"title":"' . $title . '","data":' . $json . '}';
echo $json;
exit;
