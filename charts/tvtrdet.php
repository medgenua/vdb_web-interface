<?php
// include the graphing class 

// connect to the database
include('../.LoadCredentials.php');
$db = 'NGS-Variants' . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$userid = $_SESSION['userID'];
require("../inc_memcache.inc");
// define types
$types = array();
$data = array();
$legends = array();
$types["ins"] = 0;
$legends[0] = 'Ins.';
$types["del"] = 1;
$legends[1] = 'Del.';
$types["sub"] = 2;
$legends[2] = 'Sub.';
$types["A/C"] = 3;
$legends[3] = "A/C";
$types["C/A"] = 4;
$legends[4] = "C/A";
$types["G/T"] = 5;
$legends[5] = "G/T";
$types["T/G"] = 6;
$legends[6] = "T/G";
$types["A/T"] = 7;
$legends[7] = "A/T";
$types["T/A"] = 8;
$legends[8] = "T/A";
$types["G/C"] = 9;
$legends[9] = "G/C";
$types["C/G"] = 10;
$legends[10] = "C/G";
$types["A/G"] = 11;
$legends[11] = "A/G";
$types["G/A"] = 12;
$legends[12] = "G/A";
$types["C/T"] = 13;
$legends[13] = "C/T";
$types["T/C"] = 14;
$legends[14] = "T/C";
for ($i = 0; $i <= 14; $i++) {
    $data[$i] = 0;
}
// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");

// get information for plotting.
$rows = runSlicedQuery("SELECT id AS VariantID, RefAllele, AltAllele FROM `Variants` WHERE id IN (?)", "Variants", $vids);

$title = 'Allelic Mutation Counts';
$json = '{"cols":[{"id":"VarType","label":"Variant Type","type":"string"},{"id":"nrVars","label":"Nr.Variants","type":"number"}],"rows":[';
if (count($rows) == 0) {
    $json .= ']';
} else {
    $all = count($rows);
    $snp = 0;
    foreach ($rows as $k => $row) {
        if (isset($types[$row['RefAllele'] . "/" . $row['AltAllele']])) {
            $data[$types[$row['RefAllele'] . "/" . $row['AltAllele']]]++;
            $snp++;
        } else {
            if (strlen($row['RefAllele']) > strlen($row['AltAllele'])) {
                $data[$types['del']]++;
            } elseif (strlen($row['RefAllele']) < strlen($row['AltAllele'])) {
                $data[$types['ins']]++;
            } else {
                $data[$types['sub']]++;
            }
        }
    }
    foreach ($legends as $idx => $type) {
        $json .= '{"c":[{"v":"' . $type . '"},{"v":' . $data[$idx] . '}]},';
    }
    $json = substr($json, 0, -1) . ']';
}
$json .= '}';
$json = '{"title":"Detailed Overview of Variant Types By Allele Change","data":' . $json . '}';
echo $json;
exit;
