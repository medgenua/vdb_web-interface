function Loadrgeffectdet(random) {
         $.ajax({
		url:'charts/rgeffectdet.php',
		dataType:"json",
		async:true,
		success:function(result) {
			var jsonData = result;
			var title = jsonData.title;
			var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
			var options = {
      			    title: title,
	 		    height: 325,
			    width:1005,
	  		    legend: {'position':'right'},
			    isStacked: true,
        		};
			var chart = new google.visualization.ColumnChart(document.getElementById('Chart_rgeffectdet'));
        		chart.draw(data, options);
		}
	});
 /*
	var jsonData = JSON.parse($.ajax({
		url:'charts/rgeffectdet.php',
		dataType:"json",
		async: false
	}).responseText);
	var title = jsonData.title;
	var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
	
        var options = {
          title: title,
	  height: 325,
	  width: 1005,
	  legend: {'position':'right'},
	  isStacked: true,
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('Chart_rgeffectdet'));
        chart.draw(data, options);
*/

}
