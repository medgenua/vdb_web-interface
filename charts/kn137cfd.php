<?php // content="text/plain; charset=utf-8"

// connect to the database
include('../.LoadCredentials.php');
require('../inc_memcache.inc');
$db = 'NGS-Variants' . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$userid = $_SESSION['userID'];

// define types
$types = array();
$data = array();
$legends = array();
// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");

// the bins : 1000
$bins = array();
$xlab = array();
for ($i = 0.000; $i <= 0.500; $i += 0.001) {
    $v = number_format($i, 3);
    $bins["$v"] = 0;
}
// get information for plotting.
$rows = runSlicedQuery("SELECT vid AS VariantID, MAF FROM `Variants_x_ANNOVAR_snp137` WHERE vid IN (?)", "Variants_x_ANNOVAR_snp137", $vids);
$all = count($rows);
foreach ($rows as $k => $row) {
    if ($row['MAF'] > 0.5) {
        $row['MAF'] = 1 - $row['MAF'];
    }
    if ($row['MAF'] < 0) {
        $bins["0.000"]++;
    } else {
        $rounded = number_format(round($row['MAF'], 3), 3);
        $bins["$rounded"] += 1;
    }
}
$json = '{"cols":[{"id":"AllellicRatioBin","label":"Allelic Ratio","type":"number"},{"id":"AllellicRatioCount","label":"Count","type":"number"},{"id":"Cummulative","label":"Cummulative Count","type":"number"}],"rows":[';
$cumm = 0;

foreach ($bins as $bin => $count) {
    $cumm += $count / $all;
    $json .= '{"c":[{"v":' . $bin . '},{"v":' . $count . '},{"v":' . $cumm . '}]},';
}
$title = 'MAF Cumulative Frequency (dbSNP137)';

$json = substr($json, 0, -1) . ']}';
$json = '{"title":"' . $title . '","data":' . $json . '}';
echo $json;
exit;
