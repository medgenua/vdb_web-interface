<?php
// connect to the database
include('../.LoadCredentials.php');
require('../inc_memcache.inc');
$db = 'NGS-Variants' . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$userid = $_SESSION['userID'];

// define types
$types = array();
$data = array();
$legends = array();
// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");

// get information for plotting.
// known
$rows = runSlicedQuery("SELECT vid AS VariantID FROM `Variants_x_ANNOVAR_esp6500si_all` WHERE vid IN (?) AND AlleleFreq != -1", "Variants_x_ANNOVAR_esp6500si_all", $vids);
$data[0] = count($rows);
$legends[0] = 'Known Variants';
// novel
$rows = runSlicedQuery("SELECT vid AS VariantID FROM `Variants_x_ANNOVAR_esp6500si_all` WHERE vid IN (?) AND AlleleFreq = -1", "Variants_x_ANNOVAR_esp6500si_all", $vids);
$data[1] = count($rows);
$legends[1] = 'Novel Variants';


$title = 'Known vs Novel Variants (ESP_6500si_all)';
// set labels if items are found.
$all = array_sum($data);
for ($i = 0; $i <= 1; $i++) {

    $labels[$i] = "%.1f%%";
}

# the ratio: 
$ratio = number_format($data[0] / $data[1], 3);

$json = '{"cols":[{"id":"VarType","label":"Variant Type","type":"string"},{"id":"nrVars","label":"Nr.Variants","type":"number"}],"rows":[';
if ($data[0] + $data[1] == 0) {
    $json .= ']';
} else {
    $json .= '{"c":[{"v":"Known"},{"v":' . $data[0] . '}]},';
    $json .= '{"c":[{"v":"Novel"},{"v":' . $data[1] . '}]}';
    $json .= ']';
}
$json .= '}';
$json = '{"title":"Known vs Novel Variants (esp6500_all): ' . $ratio . '","data":' . $json . '}';
echo $json;
