<?php // content="text/plain; charset=utf-8"

// connect to the database
include('../.LoadCredentials.php');
require("../includes/inc_query_functions.inc");

$db = 'NGS-Variants' . $_SESSION['dbname'];

$userid = $_SESSION['userID'];

require("../inc_memcache.inc");
// define types
$types = array();
$data = array();
$legends = array();
// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");
$sids = file_get_contents("/tmp/VariantDB.ChartData.$userid.sids");
// the bins : 1000
$bins = array();
$xlab = array();
for ($i = 0.00; $i <= 1.00; $i += 0.01) {
    $v = number_format($i, 2);
    $bins["$v"] = 0;
    $xlab[] = $v;
}
$bins['1.00'] = 0;
// get information for plotting.
$rows = runSlicedQuery("SELECT vid AS VariantID,(AltDepth/(RefDepth+AltDepth)) as AllRat, QualityByDepth, (RefDepth+AltDepth) As TotalDepth FROM `Variants_x_Samples` WHERE sid in ($sids) AND vid IN (?)", "Variants_x_Samples", $vids);


foreach ($rows as $k => $row) {
    //if (!isset($va[$row['vid']])) continue;
    $rounded = number_format(round($row['AllRat'], 2), 2);
    $bins["$rounded"]++;;
}
$data = array();
$cdata = array();
$idx = 0;
$labs = array();
$json = '{"cols":[{"id":"AllellicRatioBin","label":"Allelic Ratio","type":"number"},{"id":"AllellicRatioCount","label":"Count","type":"number"},{"id":"Cummulative","label":"Cummulative Count","type":"number"}],"rows":[';
$cumm = 0;
foreach ($bins as $bin => $count) {
    $cumm += $count / $all;
    $json .= '{"c":[{"v":' . $bin . '},{"v":' . $count . '},{"v":' . $cumm . '}]},';
    //$labs[] = $bin;
    //$data[] = $count;
    //if ($idx == 0) {
    //	$cdata[$idx] = $count;
    //}
    //else {
    //	$cdata[$idx] = $cdata[$idx - 1] + $count;
    //}
    //$idx++;
}
$json = substr($json, 0, -1) . ']}';
$title = 'Allelic Ratio Distribution';
$json = '{"title":"' . $title . '","data":' . $json . '}';
echo $json;
exit;
