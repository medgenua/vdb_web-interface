function Loadrgeffect(random) {
     $.ajax({
		url:'charts/rgeffect.php',
		dataType:"json",
		async:true,
		success:function(result) {
			var jsonData = result;
			var title = jsonData.title;
			var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
			var options = {
      			    title: title,
			    is3D: true,
	 		    height: 325,
			    width: 665,
	  		    legend: {'position':'right'},
        		};
			var chart = new google.visualization.PieChart(document.getElementById('Chart_rgeffect'));
        		chart.draw(data, options);
		}
	});
 /*
	var jsonData = JSON.parse($.ajax({
		url:'charts/rgeffect.php',
		dataType:"json",
		async: false
	}).responseText);
	
	var title = jsonData.title;
	var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
	
        var options = {
          title: title,
	  is3D: true,
	  height: 325,
 	  width: 665,
	  legend: {'position':'right'},
        };

        var chart = new google.visualization.PieChart(document.getElementById('Chart_rgeffect'));
        chart.draw(data, options);
*/
}
