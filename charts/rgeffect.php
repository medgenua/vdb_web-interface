<?php

// connect to the database
include('../.LoadCredentials.php');
$db = 'NGS-Variants' . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");

$userid = $_SESSION['userID'];
require("../inc_memcache.inc");
// define types
$types = array();
$data = array();
$legends = array();
//Transversion == 1

// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");
$nrvids = count(explode(",", $vids));
// get information for plotting.
$rows = runSlicedQuery("SELECT vid as VariantID, VariantType FROM `Variants_x_ANNOVAR_refgene` WHERE vid IN (?) AND VariantType != ''", "Variants_x_ANNOVAR_refgene", $vids);

// get types for the legend.
$types = runQuery("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_ANNOVAR_refgene_VariantType'", "Value_Codes");

$vttypes = array();

foreach ($types as $k => $row) {
    if ($row['Item_Value'] == '') {
        $row['Item_Value'] = 'Intronic/Intergenic';
    }
    $vttypes[$row['id']] = $row['Item_Value'];
    $data[$row['id']] = 0;
}
$title = 'RefSeq Effects Overview';
$json = '{"cols":[{"id":"VarType","label":"Variant Type","type":"string"},{"id":"nrVars","label":"Nr.Variants","type":"number"}],"rows":[';

if (count($rows) == 0) {
    $json .= ']';
} else {
    foreach ($rows as $k => $row) {
        $data[$row['VariantType']]++;
        #$data[] = $row['nr'];
        #$legends[] = $vttypes[$row['VariantType']] ;
    }
    foreach ($data as $k => $v) {
        $json .= '{"c":[{"v":"' . $vttypes[$k] . '"},{"v":' . $v . '}]},';
    }
    $json = substr($json, 0, -1) . ']';
}

$json .= '}';
$json = '{"title":"' . $title . '","data":' . $json . '}';
echo $json;
exit;
