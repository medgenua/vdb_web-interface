function Loadtvtrdet(random) {
  $.ajax({
		url:'charts/tvtrdet.php',
		dataType:"json",
		async:true,
		success:function(result) {
			var jsonData = result;
			var title = jsonData.title;
			var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
			var options = {
      			    title: title,
			    is3D: true,
	 		    height: 325,
			    width:1005,
	  		    legend: {'position':'none'},
        		};
			var chart = new google.visualization.ColumnChart(document.getElementById('Chart_tvtrdet'));
        		chart.draw(data, options);
		}
	});
    /*
	var jsonData = JSON.parse($.ajax({
		url:'charts/tvtrdet.php',
		dataType:"json",
		async: false
	}).responseText);
	var title = jsonData.title;
	var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
	
        var options = {
          title: title,
	  height: 325,
	  width: 1005,
	  legend: {'position':'none'},
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('Chart_tvtrdet'));
        chart.draw(data, options);
*/

}
