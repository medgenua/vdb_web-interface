function Loadknesp6500allcfd(random) {
         $.ajax({
		url:'charts/knesp6500allcfd.php',
		dataType:"json",
		async:true,
		success:function(result) {
			var jsonData = result;
			var title = jsonData.title;
			var data = new google.visualization.DataTable(JSON.stringify(jsonData.data));
			var options = {
      			    title: title,
		            htitle: 'Minor Allele Fraction',
			    seriesType:'bars',
			    series:  {1: {type:'line',targetAxisIndex:1}}, 
			    vAxes:[ {0:{title:'Number of Variants'}},{1:{title:'Cumulative Fraction'}}],
	 		    height: 325,
			    width:1005,
	  		    legend: {'position':'bottom'},
        		};
			var chart = new google.visualization.ComboChart(document.getElementById('Chart_knesp6500allcfd'));
        		chart.draw(data, options);
		}
	});
}
