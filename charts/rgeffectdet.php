<?php
// connect to the database
include('../.LoadCredentials.php');
$db = 'NGS-Variants' . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
$userid = $_SESSION['userID'];
require("../inc_memcache.inc");
// define types
$types = array();
$data = array();
$legends = array();
// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");
$va = array_flip(explode(",", $vids));
$nrvar = count($va);
// get different varianttypes.
// get types for the legend.
$types = runQuery("SELECT id, Item_Value FROM `Value_Codes` WHERE Table_x_Column = 'Variants_x_ANNOVAR_refgene_VariantType'", "Value_Codes");
$vttypes = array();
$json = '{"cols":[{"id":"VarType","label":"Variant Type","type":"string"},';
foreach ($types as $k => $row) {
    if ($row['Item_Value'] == '') {
        $row['Item_Value'] = 'Intronic/Intergenic';
    }
    $vttypes[$row['id']] = $row['Item_Value'];
    $json .= '{"id":"type' . $row['id'] . '","label":"' . $row['Item_Value'] . '","type":"number"},';
}
$json = substr($json, 0, -1) . '],"rows":[';


// get information for plotting.
for ($i = 1; $i <= 25; $i++) {

    if ($i == 23) {
        $chr = 'chrX';
    } elseif ($i == 24) {
        $chr = 'chrY';
    } elseif ($i == 25) {
        $chr = 'chrM';
    } else {
        $chr = "chr$i";
    }
    $data[$i] = array();
    foreach ($vttypes as $k => $v) {
        $data[$i][$k] = 0;
    }
    $rows = runSlicedQuery("SELECT vr.vid AS VariantID, vr.VariantType FROM `Variants_x_ANNOVAR_refgene` vr JOIN `Variants` v ON v.id = vr.vid WHERE v.chr = '$i' AND vr.vid IN (?)", "Variants_x_ANNOVAR_refgene:Variants", $vids);

    foreach ($rows as $k => $row) {
        // initialise the varianttypes.
        //if (!in_array($row['VariantType'],$data[$i])) {
        //	$data[$i][$row['VariantType']] = 0;
        //}
        $data[$i][$row['VariantType']]++;

        //if (!isset($sums[$row['VariantType']])) {
        //	$sums[$row['VariantType']] = 0;
        //	$ydata[$row['VariantType']] = array();
        //	for ($j = 1; $j <= 25; $j++) {
        //		//$data[$i][$row['type']] = 0;
        //		$ydata[$row['VariantType']][($j-1)] = 0;
        //	}
        //}
        //$data[$i][$row['VariantType']] = $row['nr'];
        //$sums[$row['VariantType']]++; // = $row['nr'];
        //$ydata[$row['VariantType']][($i-1)]++; //= $row['nr'];
        //echo "chr$i : ". $row['VariantType'] . " => " . $row['nr'] . "<br/>";
        //$legends[] = $row['VariantType'];
    }
    $json .= '{"c":[{"v":"' . $chr . '"}';
    foreach ($vttypes as $k => $v) {
        $json .= ',{"v":' . $data[$i][$k] . '}';
    }
    $json .= ']},';
}
$json = substr($json, 0, -1) . ']}';
$json = '{"title":"Detailed Overview of Variant Types By Chromosome and Effect on RefGene Transcripts","data":' . $json . '}';
echo $json;
exit;
