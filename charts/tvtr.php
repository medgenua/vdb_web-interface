<?php
// new code: return json formatted data for google charts api.
// connect to the database
include('../.LoadCredentials.php');
require('../inc_memcache.inc');
$db = 'NGS-Variants' . $_SESSION['dbname'];
require("../includes/inc_query_functions.inc");
$userid = $_SESSION['userID'];
// define types
$types = array();
$data = array();
$legends = array();
$data[0] = 0;
$legends[0] = 'InDels';
//Transversion == 1
$data[1] = 0;
$legends[1] = 'Transversion';
$types["A/C"] = 1;
$types["C/A"] = 1;
$types["G/T"] = 1;
$types["T/G"] = 1;
$types["A/T"] = 1;
$types["T/A"] = 1;
$types["G/C"] = 1;
$types["C/G"] = 1;
// transition == 2;
$data[2] = 0;
$legends[2] = 'Transition';
$types["A/G"] = 2;
$types["G/A"] = 2;
$types["C/T"] = 2;
$types["T/C"] = 2;

// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");

// get information for plotting.
$rows = runSlicedQuery("SELECT id AS VariantID, RefAllele, AltAllele FROM `Variants` WHERE id IN (?)", "Variants", $vids);

$title = 'Transition/Transversion Ratio';
$json = '{"cols":[{"id":"VarType","label":"Variant Type","type":"string"},{"id":"nrVars","label":"Nr.Variants","type":"number"}],"rows":[';
if (count($rows) == 0) {
    $json .= ']';
    //$data = array();
    //$legends = array('No Data Found');
} else {
    foreach ($rows as $k => $row) {
        if (isset($types[$row['RefAllele'] . "/" . $row['AltAllele']])) {
            $data[$types[$row['RefAllele'] . "/" . $row['AltAllele']]]++;
        } else {
            $data[0]++;
        }
    }
    $json .= '{"c":[{"v":"Indel"},{"v":' . $data[0] . '}]},';
    $json .= '{"c":[{"v":"Tv"},{"v":' . $data[1] . '}]},';
    $json .= '{"c":[{"v":"Tr"},{"v":' . $data[2] . '}]}';
    $json .= ']';
}
$json .= '}';
# the ratio: 
$ratio = number_format($data[2] / $data[1], 3);


// final json string
$json = '{"title":"Transition/Transversion Ratio: ' . $ratio . '","data":' . $json . '}';
echo $json;
exit;






/*
// include the graphing class 
require_once ('jpgraph/src/jpgraph.php');
require_once ('jpgraph/src/jpgraph_pie.php');
require_once ('jpgraph/src/jpgraph_pie3d.php');

// connect to the database
include('../.LoadCredentials.php');
require('../inc_memcache.inc');
$db = 'NGS-Variants'.$_SESSION['dbname'];

$userid = $_SESSION['userID'];

// define types
$types = array();
$data = array();
$legends = array();
$data[0] = 0;
$legends[0] = 'InDels';
//Transversion == 1
$data[1] = 0;
$legends[1] = 'Transversion';
$types["A/C"] = 1;
$types["C/A"] = 1;
$types["G/T"] = 1;
$types["T/G"] = 1;
$types["A/T"] = 1;
$types["T/A"] = 1;
$types["G/C"] = 1;
$types["C/G"] = 1;
// transition == 2;
$data[2] = 0;
$legends[2] = 'Transition';
$types["A/G"] = 2;
$types["G/A"] = 2;
$types["C/T"] = 2;
$types["T/C"] = 2;

// get data
$vids = file_get_contents("/tmp/VariantDB.ChartData.$userid.vids");

// get information for plotting.
$rows = runSlicedQuery("SELECT id AS VariantID, RefAllele, AltAllele FROM `Variants` WHERE id IN (?)","Variants",$vids);
if (count($rows) > 0 && !is_array($rows[0])) $rows = OneToMulti($rows);

$title = 'Transition/Transversion Ratio';
if (count($rows) == 0) {
	$data = array(1);
	$legends = array('No Data Found');
}
else {
	foreach($rows as $k => $row){
		if (isset($types[$row['RefAllele']."/".$row['AltAllele']])) {
			$data[$types[$row['RefAllele']."/".$row['AltAllele']]]++;
		}
		else {
			$data[0]++;
		}
	}
}
// set labels if items are found.
$all = $data[0] + $data[1] + $data[2];
$snp = $data[1] + $data[2];
if ($snp > 0) {
	$labels[0] = '';
	for ($i = 0; $i <= 2; $i++) {

		
		if ($data[$i]/$all >= 0.03) {
			$labels[$i] = "%.1f%%";
		}
		else {
			$labels[$i] = '';
		}
	}
}
# the ratio: 
$ratio = number_format($data[2] / $data[1],3);


// Create the Pie Graph. 
$graph = new PieGraph(325,325);
$theme_class= new UniversalTheme;
$graph->SetTheme($theme_class);
//$graph->img->SetAntiAliasing();

// Set A title for the plot
$graph->title->Set("$title: $ratio");
$all = number_format($all,0,'',',');
$snp =  number_format($snp,0,'',',');
$graph->subtitle->Set("Total : $all Variants, $snp SNVs");
// Create
$p1 = new PiePlot3D($data);
$p1->SetSize(0.38);
$p1->SetLegends($legends);
$graph->legend->SetPos(0.5,0.97,'center','bottom');
$graph->legend->SetColumns(2);
$graph->legend->SetShadow('gray@0.4',5);
$p1->SetCenter(0.5,0.42);

// labels ?
$p1->SetLabels($labels);
$p1->SetLabelPos(1);
// add to graph field
$graph->Add($p1);
if (function_exists('imageantialias')) {
    $graph->SetAntiAliasing();
}
// draw the plot
$graph->Stroke();

*/
