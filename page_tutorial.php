<?php

$topic = $_GET['topic'];
$topicmap = array('main' =>'','upload' => 'newdata');
if (array_key_exists($topic,$topicmap)) {
	$topic = $topicmap[$topic];
}
$include = "includes/inc_docs_$topic.inc";

if ($topic == '') {
	// list topics. 
	$dir = "includes/";
	$topics = array();
	$It =  opendir("$dir");
	if ($It) {
		while ($Filename = readdir($It)) {
 			if ($Filename == '.' || $Filename == '..' )
  				continue;
 			//$LastModified = filemtime($dir . $Filename);
			if (substr($Filename,0,9) == "inc_docs_") {
	 			$topics[] = substr($Filename,9,-4);
			}
		}
	}
	sort($topics);
	if (count($topics) == 0) {
		echo "<div class=section><h3>No Documentation topics found</h3>";
		echo "<p>If you are looking for specific topics, please contact the VariantDB administrator.</p>";
		echo "</div>";
		exit();
	}
	//////////////////////////////
	// DEFINE SOME PRETTY NAMES //
	//////////////////////////////
	$names = array();
	$names['intro'] = 'Getting Started';
	$names['newdata'] = 'Loading New Samples into VariantDB';
	$names['igv'] = 'Using IGV with VariantDB';
	$names['anno'] = 'Annotations available in VariantDB';
	$names['filters'] = 'Filters available in VariantDB';
	$names['variants'] = 'Variant Filtering Introduction';
	$names['install'] = 'Local Version : Complete Installation';
	$names['virtualboxVM'] = 'Local Version : Plug-n-Play Virtual Machine';
	$names['strategies'] = 'Example Filtering Strategies';
	$names['api'] = 'Automating VariantDB through API access';
	$names['api_examples'] = 'Example API-scripts';
	$names['api_import'] = 'Import sample through API';

	echo "<div class=section><h3>Available Documentation topics</h3>";
	echo "<p><ol>";
	$done = array();
	$order = array('intro','newdata','igv','anno','filters','variants','strategies','api','api_examples','api_import','virtualboxVM','install');
	foreach($order as $key => $topic) {
		if (array_key_exists($topic,$names)) {
			$name = $names[$topic];
		}
		else {
			$name = $topic;
		}
		echo "<li><a href='index.php?page=tutorial&topic=".$topic."'>$name</a></li>";
		$done[$topic] = 1;
	}
	for ($i = 0; $i < count($topics) ; $i++) {	
		if (array_key_exists($topics[$i],$done)) {
			continue;
		}
		//else {
		//	//echo $topics[$i]. " is not done yet.<br/>";
		//}
		if (array_key_exists($topics[$i],$names)) {
			$name = $names[$topics[$i]];
		}
		else {
			$name = $topics[$i];
		}
		echo "<li><a href='index.php?page=tutorial&topic=".$topics[$i]."'>$name</a></li>";
		
	}
	echo "</ol></p></div>";
	exit();
}
if (file_exists($include)) {
	include($include);
}
else {
	echo "<div class=section><h3>Documentation not found</h3><p>The requested documentation ($include) was not found. Please contact the system admin to request more help.</p></div>";
}

