<?php
if (!isset($loggedin) || $loggedin != 1) {
	include('page_login.php');
	exit;
}

else {
	$type = $_GET['type'];
	if ($type == 'create') {
		include('includes/inc_groups_create.inc');
	}
	elseif ($type == 'join') {
		include('includes/inc_groups_join.inc');
	}
	elseif ($type == 'mine') {
		include('includes/inc_groups_mine.inc');
	}
	elseif ($type == 'manage') {
		include('includes/inc_groups_manage.inc');
	}
	else {
		echo "work in progress...<br>";
	}

}
?>
