<?php
// check login
if (!isset($loggedin) || $loggedin != 1) {
    include('page_login.php');
    exit;
}
// processing type specified?
$processingtype = '';
if (isset($_GET['pt'])) {
    $processingtype = $_GET['pt'];
}
// sample specified? 
$sid = '';
if (isset($_GET['sid'])) {
    $sid = $_GET['sid'];
}

// needed array in permission settings
$yesno = array("1" => 'Yes', '0' => 'No');

if (isset($_POST['BatchMove'])) {
    // get samples to move.
    $SampleToMove = $_POST['batchMove'];
    $instring = '';
    foreach ($SampleToMove as $key => $value) {
        $instring .= "$value,";
    }
    if ($instring != '') {
        $instring = substr($instring, 0, -1);
    } else {
        echo "PROBLEM : No samples were selected. Please go back and try again.<br/>";
        exit();
    }

    // target pid
    $targetpid = $_POST['BatchToPid'];
    $NewName = '';
    if ($targetpid == 'new') {
        $NewName = $_POST['NewProjectName'];
        if ($NewName == '') {
            echo "PROBLEM : The name for the new project was empty. Please go back and try again.<br/>";
            exit();
        }
        $NewNamedb = addslashes($NewName);
        $targetpid = InsertQuery("INSERT INTO `Projects` (Name, userID) VALUES ('$NewNamedb','$userid')", "Projects:Variants_x_Projects_Summary:Summary");
        // set permissions for current user
        insertQuery("INSERT INTO `Projects_x_Users` (pid, uid, editvariant, editclinic, editsample) VALUES ('$targetpid','$userid','1','1','1')", "Projects_x_Users");
        // set update status for user (needed? this is not new data, just relocation...).
        doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$userid'", "Users");
    } else {
        $row = array_shift(...[runQuery("SELECT Name FROM `Projects` WHERE id = '$targetpid'", "Projects")]);
        if (count($row) == 0) {
            echo "ERROR: invalid project selected.<br/>";
            exit;
        }
        $NewName = $row['Name'];
    }
    // get originating projects
    $rows = runQuery("SELECT DISTINCT(pid) AS pid FROM `Projects_x_Samples` WHERE sid IN ($instring)", "Projects_x_Samples");
    $fromstring = '';
    $fromarray = array();
    foreach ($rows as $k => $row) {
        $fromstring .= $row['pid'] . ',';
        $fromarray[$row['pid']] = 1;
    }
    $fromstring = substr($fromstring, 0, -1);
    // update permissions for other user => present options if needed.
    $gconflicts = '';
    // list groups with old permission and option to change it
    $groups = runQuery("SELECT gid, p.Name AS pname FROM `Projects_x_Usergroups` pu JOIN `Projects` p ON p.id = pu.pid WHERE pid In ($fromstring) OR pid = $targetpid", "Projects_x_Usergroups:Projects");
    if (count($groups) > 0) {
        $gconflicts .= "<tr><th colspan=4 $firstcell class=top>User Groups</th></tr>";
        $gconflicts .= "<tr><th class=top>Group Name</th><th class=top>Access Type</th><th class=top>Source Project (name)</th><th class=top>Target Project</th></tr>";
        foreach ($groups as $k => $gr) {
            $groupid = $gr['gid'];
            $pname = $gr['pname'];
            //## get group permissions
            $gpr = runQuery("SELECT editclinic, editvariant, editsample, name FROM `Usergroups` WHERE id = '$groupid'", "Usergroups")[0];
            $editcnv = $gpr['editvariant'];
            $editclinic = $gpr['editclinic'];
            $editsample = $gpr['editsample'];
            $gname = $gpr['name'];
            if ($editcnv == 1 && $editclinic == 1) {
                $perm = 'Full';
            } elseif ($editcnv == 1) {
                $perm = 'CNV';
            } elseif ($editclinic == 1) {
                $perm = 'Clinic';
            } else {
                $perm = 'Read-only';
            }
            if ($editsample == 1) {
                $perm .= " / Project Control";
            }
            $check = runQuery("SELECT gid FROM `Projects_x_Usergroups` WHERE pid IN ($fromstring) AND gid = '$groupid'", "Projects_x_Usergroups");
            if (count($check) > 0) {
                $oldacc = 1;
            } else {
                $oldacc = 0;
            }
            $check = runQuery("SELECT gid FROM `Projects_x_Usergroups`WHERE pid = '$targetpid' AND gid = '$groupid'", "Projects_x_Usergroups");
            if (count($check) > 0) {
                $newacc = 1;
            } else {
                $newacc = 0;
            }
            $gconflicts .= "<tr><th class=left NOWRAP>$gname</th><td>$perm</td><td>" . $yesno[$oldacc] . " ($pname)</td><td><span id=add$groupid>" . $yesno[$newacc] . "</span></td></tr>";
        }
    }
    // seperate users
    $users = runQuery("SELECT u.FirstName, u.LastName, pu.uid, pu.editvariant, pu.editclinic, pu.editsample, pu.pid FROM `Projects_x_Users` pu JOIN `Users` u ON pu.uid = u.id WHERE pid IN ($fromstring) OR pid = '$targetpid'", "Projects_x_Users:Users");
    $uconflicts = '';
    if (count($users) > 0) {
        $doneusers = array();
        $uconflicts .= "<tr><th colspan=4 class=top>Seperate Users</th></tr>";
        $uconflicts .= "<tr><th class=top>User Name</th><th class=top>Source Project Permission</th><th class=top>Target Project Permission</th></tr>";
        foreach ($users as $k => $ur) {
            $thisuserid = $ur['uid'];
            if ($doneusers[$thisuserid] == 1) {
                // user done before
                continue;
            }
            $doneusers[$thisuserid] = 1;
            $thisusername = $ur['FirstName'] . " " . $ur['LastName'];
            // get group permissions
            $editcnv = $ur['editvariant'];
            $editclinic = $ur['editclinic'];
            $editsample = $ur['editsample'];
            $project = $ur['pid'];
            if ($editcnv == 1 && $editclinic == 1) {
                $perm = 'Full';
            } elseif ($editcnv == 1) {
                $perm = 'CNV';
            } elseif ($editclinic == 1) {
                $perm = 'Clinic';
            } else {
                $perm = 'Read-only';
            }
            if ($editsample == 1) {
                $perm .= " / Project Control";
            }
            // check other project permission for current user.
            // current pid is source project . check the new permission.
            if (array_key_exists($project, $fromarray)) {
                $oldperm = $perm;
                $cr = array_shift(...[runQuery("SELECT editvariant, editclinic, editsample FROM `Projects_x_Users` WHERE pid = '$targetpid' AND uid = $thisuserid", "Projects_x_Users")]);
                if (is_array($cr) && count($cr) > 0) {
                    $editcnv = $cr['editvariant'];
                    $editclinic = $cr['editclinic'];
                    $editsample = $cr['editsample'];
                    if ($editcnv == 1 && $editclinic == 1) {
                        $perm = 'Full';
                    } elseif ($editcnv == 1) {
                        $perm = 'CNV';
                    } elseif ($editclinic == 1) {
                        $perm = 'Clinic';
                    } else {
                        $perm = 'Read-only';
                    }
                    if ($editsample == 1) {
                        $perm .= " / Project Control";
                    }
                    $newperm = $perm;
                } else {
                    $newperm = 'none';
                }
            }
            // current pid is the target project, check source projects.
            else {
                $newperm = $perm;
                $cr = array_shift(...[runQuery("SELECT editvariant, editclinic, editsample FROM `Projects_x_Users` WHERE pid IN ($fromstring) AND uid = $thisuserid", "Projects_x_Users")]);
                if (is_array($cr) && count($cr) > 0) {
                    $editcnv = $cr['editvariant'];
                    $editclinic = $cr['editclinic'];
                    $editsample = $cr['editsample'];
                    if ($editcnv == 1 && $editclinic == 1) {
                        $perm = 'Full';
                    } elseif ($editcnv == 1) {
                        $perm = 'CNV';
                    } elseif ($editclinic == 1) {
                        $perm = 'Clinic';
                    } else {
                        $perm = 'Read-only';
                    }
                    if ($editsample == 1) {
                        $perm .= " / Project Control";
                    }
                    $oldperm = $perm;
                } else {
                    $oldperm = 'none';
                }
            }
            // print user
            $uconflicts .= "<tr><th class=left NOWRAP>$thisusername</th><td >$oldperm</td><td >$newperm </td></tr>";
        }
    }
    echo "<div class=section>";
    // are there conflicts? => print title
    if ($uconflicts != '' || $gconflicts != '') {
        echo "<h3>Possible Permission Conflicts</h3>";
        echo "<p>An overview of usergroup and user permissions with regard to the affected projects is given below. Click on the 'Change Permissions' button below to manage permissions for the new project. The old projects are now deleted, and permissions are not automatically transferred !</p>";
        echo "<p><form action='index.php?page=share_projects' method=POST><input type=hidden name=pid value='$targetpid'><input type='submit' name='ManagePermissions' value='Change Permissions'></form></p>";
    }
    // group conflicts ?
    if ($gconflicts != '') {
        echo "<p><table cellspacing=0 class=w75>";
        echo $gconflicts;
        echo "</table></p>";
    }
    if ($uconflicts != '') {
        echo "<p style='margin-top:2em'><table cellspacing=0 class=w75>";
        echo $uconflicts;
        echo "</table></p>";
    }

    // NOW MOVE THE SAMPLES
    doQuery("UPDATE `Projects_x_Samples` SET pid = '$targetpid' WHERE sid IN ($instring)", "Projects_x_Samples:Variants_x_Projects_Summary:Variants_x_Users_Summary:Summary");

    // Remove empty projects ; reset summaryStatus for non-empty. 
    foreach ($fromarray as $pid => $dummy) {
        $rows = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$pid'", "Projects_x_Samples");
        if (count($rows) == 0) {
            // remove project
            doQuery("DELETE FROM `Projects` WHERE id = '$pid'", "Projects");
            // remove from permissiosn
            doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$pid'", "Projects_x_Users");
            doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$pid'", "Projects_x_Usergroups");
        } else {
            doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id = '$pid'", "Projects");
        }
    }
    // set targetpid SummaryStatus.
    doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id = '$targetpid'", "Projects"); // summary caches cleared above
    // set summarystatus for affected users.
    foreach ($users as $k => $ur) {
        $thisuserid = $ur['uid'];
        doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$thisuserid'", "Users"); // summary caches cleared above.
    }

    // present user with button to go back to 'recent'
    echo "<h3>Sample Migration Done</h3>";
    echo "<p>Samples were moved successfully to '$NewName'</p>";
    echo "<p><form action='index.php?page=samples&pt=$processingtype&sid=$sid' method=POST><input type=submit name='dummy' value='Back To Samples'></form></p>";
    exit();
}

///////////////////////////////
// UPDATE SAMPLES SEPERATELY //
///////////////////////////////
// this is deprecated ? buttons are not shown anymore.
if (isset($_POST['UpdateDetails'])) {
    // get the samples
    $rows = runQuery("SELECT s.id AS sid, ps.pid,s.gender,s.Name  FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects` p JOIN `Projects_x_Users` pu ON s.id = ps.sid AND p.id = ps.pid AND pu.pid = p.id WHERE pu.uid = $userid AND pu.editsample = 1  ORDER BY s.id DESC", "Samples:Projects_x_Samples:Projects:Projects_x_Users");
    // posted values
    $pgenders = $_POST['gender'];
    $pnames = $_POST['sname'];
    $ppids = $_POST['pid'];
    foreach ($rows as $k => $row) {
        $sid = $row['sid'];
        $oldpid = $row['pid'];
        $newgender = $pgenders[$sid];
        $newname = addslashes($pnames[$sid]);
        $newproject = $ppids[$sid];
        $oldname = $row['Name'];
        $oldgender = $row['gender'];
        $oldic = $row['IsControl'];
        $oldia = $row['affected'];
        // not in the displayed page: 
        if (!array_key_exists($sid, $_POST['sname'])) {
            continue;
        }
        // isControl?
        if (in_array($sid, $_POST['IsControl'])) {
            $ic = 1;
        } else {
            $ic = 0;
        }
        $update = 0;
        // update if changed
        if ($oldgender != $newgender || $oldname != $newname || $oldic != $ic) {
            $update = 1;
            // update sample
            doQuery("UPDATE `Samples` SET gender = '$newgender', Name = '$newname', IsControl = '$ic' WHERE id = '$sid'", "Samples");
        }
        // update project if changed
        if ($newproject != $oldpid) {
            $update = 1;
            doQuery("UPDATE `Projects_x_Samples` SET pid = '$newproject' WHERE sid = '$sid'", "Projects_x_Samples:Variants_x_Projects_Summary:Summary");
            // delete project if empty
            $rows = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$oldpid'", "Projects_x_Samples");
            if (count($rows) == 0) {
                doQuery("DELETE FROM `Projects` WHERE id = '$oldpid'", "Projects");
                doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$oldpid'", "Projects_x_Users");
                doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$oldpid'", "Projects_x_Usergroups");
            }
        }
        // update summaryStatus for affected projects.
        if ($update == 1) {
            doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id IN ('$newproject','$oldpid')", "Projects:Variants_x_Projects_Summary:Summary");
            // update summaryStatus for users with access to affected projects.
            $urows = runQuery("SELECT `uid` FROM `Projects_x_Users` WHERE `pid` IN ('$newproject','$oldpid')", "Projects_x_Users");
            foreach ($urows as $thisuid) {
                doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$thisuid'", "Users:Variants_x_Users_Summary:");
            }
        }
    }
}


////////////////////////////////////////
// END OF PROCESSING SUBMITTED VALUES //
////////////////////////////////////////


// set processing type
echo "<div class=section>";
echo "<h3>Select Processing Type</h3>";
echo "<p style='margin-left:1em';><select id='processingtype' onChange='ToggleForm()'>";
if ($processingtype == 'batch') {
    echo "<option value='batch' selected>Process multiple samples in batch</option>";
    echo "<option value='single'>Process single samples in more detail</option>";
} elseif ($processingtype == 'single') {
    echo "<option value='batch'>Process multiple samples in batch</option>";
    echo "<option value='single' selected>Process single samples in more detail</option>";
} else {
    echo "<option value='batch' >Process multiple samples in batch</option>";
    echo "<option value='single'>Process single samples in more detail</option>";
}


echo "</select></p>";
echo "</div>";

/////////////////////////////
// SINGLE SAMPLE EDIT FORM //
/////////////////////////////
$display = '';
if ($processingtype != 'single') {
    $display = "style='display:none';";
}
echo "<div class=section id='SingleForm' $display>";
echo "<!-- Left Panel: Select Sample options -->";
echo "<div class='toleft w75' >";
echo "<h3>Select a Sample</h3>";
echo "<table id=sampletable cellspacing=0 style='width:95%'>";

// select a sample
$output = "<thead id='SourceSample'>\n";
$output .= "  <tr>\n";
$output .= "    <td colspan=5 style='padding-left:0em;text-align:left;background:#cecece;font-style:italic'>Manage This Sample</td>\n";
$output .= "  </tr>\n";
$output .= "  <tr>\n";
// no sample specified in url
if ($sid == '') {
    $output .= "    <td colspan=1><select id='selecttype' onChange=\"SetSampleSelect('$userid')\" name='selecttype'><option value='Type' selected>Type the sample name</option><option value='List' >Select From List</option></select></td>";
    $output .= "    <td colspan=4 id=sampleselect><input type=hidden name=sampleid id=sampleid value=''><input type=text style='width:98%;' value='' name=samplesource id=searchfield></td>";
    $output .= "  </tr>\n";
    $output .= "</thead>";
    echo $output;
    $output = '';
} else {
    $output .= "    <td colspan=1><select id='selecttype' onChange=\"SetSampleSelect('$userid')\" name='selecttype'><option value='Type' >Type the sample name</option><option value='List'  selected>Select From List</option></select></td>";
    $output .= "    <td colspan=4 id=sampleselect>";
    // project of selected sample ? 
    $sample_pid = array_shift(...[runQuery("SELECT pid FROM `Projects_x_Samples` WHERE sid = '$sid'", "Projects_x_Samples")])['pid'];
    // available projects:
    $projects = runQuery("SELECT p.Name, p.id FROM `Projects` p JOIN `Projects_x_Users` pu ON pu.pid = p.id WHERE pu.uid = '$userid' ORDER BY p.Name", 'Projects_x_Users:Projects');
    $output .= "Projects : <select name='projectid' id='projectid' style='width:92%' onChange='UpdateProjectSamples($userid)'>";
    foreach ($projects as $project) {
        $selected = ($project['id'] == $sample_pid) ? 'SELECTED' : '';
        $output .= "<option value='" . $project['id'] . "' $selected>" . $project['Name'] . "</option>";
    }
    $output .= "</select><br/>";

    $rows = runQuery("SELECT s.id, s.Name FROM `Samples` s JOIN  `Projects_x_Samples` ps ON ps.sid = s.id WHERE ps.pid = '$sample_pid' ORDER BY  s.Name ASC", "Samples:Projects_x_Samples");
    if (count($rows) == 0) {
        $output .= "No samples available";
    } else {
        $output .= "Samples: <select name=sampleid id=sampleid style='width:92%'><option value='' disabled='disabled'></option>";
        foreach ($rows as $key => $row) {
            if ($row['id'] == $sid) {
                $output .= "<option value='" . $row['id'] . "' selected>" . $row['Name'] . "</option>";
            } else {
                $output .= "<option value='" . $row['id'] . "' >" . $row['Name'] . "</option>";
            }
        }
        $output .= "</select>";
    }
    $output .= "  </tr>\n";
    $output .= "</thead>";
    echo $output;
    $output = '';
}
?>
</table>
<p><input type=submit value='Submit' onClick="LoadDetails()"></p>
<!-- DETAILS WILL COME HERE -->
<div id='SampleDetails'>
</div>

</div>
<div style="clear:both;"></div>

<!-- end of section -->
</div>


<?php
/////////////////////
// BATCH EDIT FORM //
////////////////////
$display = '';
if ($processingtype == 'single') {
    $display = "style='display:none;'";
}
echo "<div class='section' id='BatchForm' $display>";
// Get all projects you have 'editsample' access to .
$rows = runQuery("SELECT p.id, p.Name FROM `Projects` p JOIN `Projects_x_Users` pu ON p.id = pu.pid WHERE pu.uid = '$userid' AND pu.editsample = 1", "Projects:Projects_x_Users");
$projects = array();
$rprojects = array();
foreach ($rows as $k => $row) {
    $projects[$row['Name']] = $row['id'];
    $rprojects[$row['id']] = $row['Name'];
}
ksort($projects);
// prepare output.
if (isset($_GET['sp'])) {
    $sp = $_GET['sp'];
    $offset = ($_GET['sp'] - 1) * 100;
} else {
    $offset = 0;
    $sp = 1;
}
$from_to = "Sample " . (($sp - 1) * 100 + 1) . " to " . (($sp) * 100);
echo "<h3>Batch Sample Editing : $from_to</h3>";

// count.
$nr_samples = runQuery("SELECT COUNT(1) AS cnt FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON pu.pid = ps.pid WHERE pu.uid = '$userid' ", "Samples:Projects_x_Samples:Projects:Projects_x_Users")[0]['cnt'];
$nr_pages = floor(($nr_samples - 1) / 100) + 1;


// get the samples you're allowed to edit.

$rows = runQuery("SELECT s.id AS sid, s.Name AS sname, s.gender, s.IsControl, p.id AS pid, pu.editsample FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects` p JOIN `Projects_x_Users` pu ON s.id = ps.sid AND p.id = ps.pid AND pu.pid = p.id WHERE pu.uid = $userid  ORDER BY p.Name ASC, s.Name ASC LIMIT $offset,100", "Samples:Projects_x_Samples:Projects:Projects_x_Users");
// no samples. short text and exit.
if (count($rows) == 0) {
    echo "<p>You have no access to samples.</p>";
    echo "<script type='text/javascript' src='javascripts/page.samples.js?$tip'></script>";
    exit();
}

// page selection
echo "<p class=indent><span class=emph>Select page:</span> <select onChange='SetSampleSubPage()' id='sp' name='sp'>";
foreach (range(1, $nr_pages) as $pagenr) {
    if ($pagenr == $sp) {
        $selected = "SELECTED";
    } else {
        $selected = "";
    }
    $msg = "Sample " . (($pagenr - 1) * 100 + 1) . "-" . (($pagenr) * 100);
    echo "<option value='$pagenr' $selected>$msg</option>";
}
echo "</select> (Unsaved changes are discarded!)";
echo "</p>";

// create form and print intro
echo "<p class=indent><span class=emph>Move/Delete Samples:</span> Select the checkboxes on the left. Options to delete/move will appear below the table.</p>";
echo "<p class=indent><span class=emph>Update Details:</span> Edit names, genders and control settings, submit by 'Update Samples' below the table. All changes made to all samples are stored. </p>";


echo "<p><form id='sampleform' name='sampleform' action='' method='GET'>";
echo "<table cellspacing=0>";
$disabled = '';
if (array_key_exists('Archived', $_SESSION) && $_SESSION['Archived'] == 1) {
    $disabled = 'disabled';
}
echo "<thead><tr><th class=top style='padding-left:0.5em;width:2.5em'><input type=checkbox id=BatchBox onChange='BatchSet();' $disabled></th><th class=top>Sample Name</th><th class=top>Gender</th><th class=top>Project</th><th class=top style='padding-left:0.5em;'>Is Control Sample</th><th class=top style='padding-left:0.5em;'>Family</th><th class=top>&nbsp</th></tr></thead>";
echo "<tbody>";

// get the samples you're allowed to edit.
foreach ($rows as $k => $row) {
    $edit = $row['editsample'];
    $disabled = '';
    if ($edit == 0) {
        $disabled = 'disabled';
    }
    $out = "<tr class=vrow><td><input type=checkbox name='batchMove[]' value='" . $row['sid'] . "' onChange='toggleDisplay();' $disabled></td>";
    $out .= "<td><input type=text name='sname[" . $row['sid'] . "]' value='" . stripslashes($row['sname']) . "' size=60 $disabled></td>";
    switch ($row['gender']) {
        case "Male":
            $out .= "<td><select name='gender[" . $row['sid'] . "]' $disabled><option value='Undef'>Unknown</option><option value='Male' SELECTED>Male</option><option value='Female' >Female</option></select></td>";
            break;
        case "Female":
            $out .= "<td><select name='gender[" . $row['sid'] . "]' $disabled><option value='Undef'>Unknown</option><option value='Male'>Male</option><option value='Female' SELECTED>Female</option></select></td>";
            break;
        default:
            $out .= "<td><select name='gender[" . $row['sid'] . "]' $disabled><option value='Undef' SELECTED>Unknown</option><option value='Male'>Male</option><option value='Female' >Female</option></select></td>";
            break;
    }
    if (isset($rprojects[$row['pid']])) {
        $out .= "<td><input type=hidden name='pid[" . $row['sid'] . "]' value='" . $row['pid'] . "' />" . $rprojects[$row['pid']] . "</td>";
    } else {
        $out .= "<td><input type=hidden name='pid[" . $row['sid'] . "]' value='" . $row['pid'] . "' />-read-only-</td>";
    }
    // is control sample ? 
    if ($row['IsControl'] == 1) {
        $out .= "<td align=center><input type=checkbox name='IsControl[]' value='" . $row['sid'] . "' checked $disabled></td>";
    } else {
        $out .= "<td align=center><input type=checkbox name='IsControl[]' value='" . $row['sid'] . "' $disabled></td>";
    }

    // Get family info (Replicate, children, siblings)
    $subrows = runQuery("SELECT sid2, Relation FROM `Samples_x_Samples` WHERE sid1 = " . $row['sid'], "Samples_x_Samples");
    $family = array();
    foreach ($subrows as $k => $subrow) {
        $family[$subrow['Relation']][$subrow['sid2']] = 1;
    }
    // Get family info (parents)
    $subrows = runQuery("SELECT sid1 FROM `Samples_x_Samples` WHERE sid2 = " . $row['sid'] . " AND Relation = 2", "Samples_x_Samples");

    foreach ($subrows as $k => $subrow) {
        $family[4][$subrow['sid1']] = 1;
    }
    // create family string
    $famcode = array(1 => 'Replicates', 2 => 'Children', 3 => 'Siblings', 4 => 'Parents');
    $famstring = '-';
    for ($i = 1; $i <= 4; $i++) {
        if (array_key_exists($i, $family) && count($family[$i]) > 0) {
            $famstring .= count($family[$i]) . " " . $famcode[$i] . ", ";
        }
    }
    if ($famstring != '-') {
        $famstring = substr($famstring, 1, -2);
    }
    $out .= "<td>$famstring </td>";
    $out .= "<td><a href='index.php?page=samples&pt=single&sid=" . $row['sid'] . "' style='font-style:italic'>To Details</a></td>";
    $out .= "</tr>";
    echo $out;
}
echo "</table></p>";

// spans for action buttons
if (!array_key_exists('Archived', $_SESSION) || $_SESSION['Archived'] != 1) {
    echo "<span id='BatchMove' style='display:none;margin-top:1em;margin-bottom:1em;'>";
    echo "<input type=button name='DeleteSelected' value='Delete Selected Samples' onClick='DeleteSamples($userid)'> Or move selected to this project: ";
    echo "<select name='BatchToPid' id='MoveToPidSelect' onChange='CheckNew()'>";
    echo "<optgroup label='Existing Projects'>";
    foreach ($projects as $pname => $pid) {
        echo "<option value='$pid' >$pname</option>";
    }
    echo "</optgroup><optgroup label='New Project'>";
    echo " <option value='new'>Create New Project</option>";
    echo "</optgroup></select>";
    echo "<input type=text size=40 name='NewProjectName' id='NewProjectName' style='display:none;'>";
    echo "<input type=button name=BatchMove value='Move Samples' onClick='MoveSamples($userid)'></span>";
    //echo "<p id='UpdateButton'><input type=submit name='UpdateDetails' value='Update Samples'>
    echo "</p>";
}
echo "</form>";
echo "</div>";


?>
</div>