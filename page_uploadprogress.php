<?php

$wd = $_GET['wd'];
echo "<div class=section><h3>Data Import Progress</h3>";
echo "<p>You can monitor the import progress here. This page updates automatically every 30 seconds. Once the import is completed, you will recieve an email. Hence, you can safely close this page.</p>";

echo "<p><span class=emph>Runtime output:<br/>";
echo "<pre class=indent>";
if (file_exists("$wd/Process.output.txt")) {
	readfile("$wd/Process.output.txt");
}
else {
	print "Initializing...";
}
echo "</pre></p>";
echo "</div>";
// status : refresh page if status file is still present (wd gets deleted) AND if status file == 0.
if (file_exists("$wd/status")) {
	$stat = file_get_contents("$wd/status");
	if (substr($stat,0,1) == "0") {
		echo "<meta http-equiv='refresh' content='30;URL=index.php?page=uploadprogress&wd=$wd'>";
		echo "<div class=section><p>Import still running. Page will auto-refresh in 30 seconds.</p></div>";
			
	}
}
else {
	echo "<meta http-equiv='refresh' content='30;URL=index.php?page=uploadprogress&wd=$wd'>";
	echo "<div class=section><p>Import still running. Page will auto-refresh in 30 seconds.</p></div>";
}
