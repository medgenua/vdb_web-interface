<?php

if (!isset($loggedin) || $loggedin != 1) {
    include('page_login.php');
    exit;
}
// needed array in permission settings
$yesno = array("1" => 'Yes', '0' => 'No');


############################
## process posted values. ##
############################
if (isset($_POST['BatchMove'])) {
    // get samples to move.
    $SampleToMove = $_POST['batchMove'];
    $instring = '';
    foreach ($SampleToMove as $key => $value) {
        $instring .= "$value,";
    }
    if ($instring != '') {
        $instring = substr($instring, 0, -1);
    } else {
        echo "PROBLEM : No samples were selected. Please go back and try again.<br/>";
        exit();
    }
    // target pid
    $targetpid = $_POST['BatchToPid'];
    if ($targetpid == 'new') {
        $NewName = $_POST['NewProjectName'];
        if ($NewName == '') {
            echo "PROBLEM : The name for the new project was empty. Please go back and try again.<br/>";
            exit();
        }
        $NewNamedb = addslashes($NewName);
        $targetpid = insertQuery("INSERT INTO `Projects` (Name, userID) VALUES ('$NewNamedb','$userid')", "Projects:Variants_x_Projects_Summary:Summary");
        // set permissions for current user
        doQuery("INSERT INTO `Projects_x_Users` (pid, uid, editvariant, editclinic, editsample) VALUES ('$targetpid','$userid','1','1','1')", "Projects_x_Users");
    } else {
        doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE `id` = '$pid'", "Projects:Variants_x_Projects_Summary:Summary");
    }

    // update SummaryStatus for current user. (is this needed? no new/changed sample_data. Yes, just make sure.)
    doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE id = '$userid'", "Users");
    // get originating projects
    $rows = runQuery("SELECT pid FROM `Projects_x_Samples` WHERE sid IN ($instring)", "Projects_x_Samples");
    $fromstring = '';
    $fromarray = array();
    foreach ($rows as $k => $row) {
        if (!array_key_exists($row['pid'], $fromarray)) {
            $fromstring .= $row['pid'] . ',';
            $fromarray[$row['pid']] = 1;
        }
    }
    $fromstring = substr($fromstring, 0, -1);

    // update permissions for other user => present options if needed.
    echo "<div class=section>";
    echo "<h3>Possible Permission Conflicts</h3>";
    echo "<p>An overview of usergroup and user permissions with regard to the affected projects is given below. Click on the 'Change Permissions' button below to manage permissions for the new project. The old projects are now deleted, and permissions are not automatically transferred !</p>";
    echo "<p><form action='index.php?page=share_projects.php' method=POST><input type=hidden name=pid value='$targetpid'><input type='submit' name='ManagePermissions' value='Change Permissions'></form></p>";

    echo "<p><table cellspacing=0 class=w75>";
    // list groups with old permission and option to change it
    $groups = runQuery("SELECT gid, p.Name AS pname FROM `Projects_x_Usergroups` pu JOIN `Projects` p ON p.id = pu.pid WHERE pid In ($fromstring) OR pid = $targetpid", "Projects_x_Usergroups:Projects");
    if (count($groups) > 0) {
        echo "<tr><th colspan=4 $firstcell class=top>User Groups</th></tr>";
        echo "<tr><th class=top>Group Name</th><th class=top>Access Type</th><th class=top>Source Project (name)</th><th class=top>Target Project</th></tr>";
        foreach ($groups as $k => $gr) {
            $groupid = $gr['gid'];
            $pname = $gr['pname'];
            // get group permissions
            $gpr = runQuery("SELECT editclinic, editvariant, editsample, name FROM `Usergroups` WHERE id = '$groupid'", "Usergroups")[0];
            $editcnv = $gpr['editvariant'];
            $editclinic = $gpr['editclinic'];
            $editsample = $gpr['editsample'];
            $gname = $gpr['name'];
            if ($editcnv == 1 && $editclinic == 1) {
                $perm = 'Full';
            } elseif ($editcnv == 1) {
                $perm = 'CNV';
            } elseif ($editclinic == 1) {
                $perm = 'Clinic';
            } else {
                $perm = 'Read-only';
            }
            if ($editsample == 1) {
                $perm .= " / Project Control";
            }
            $check = runQuery("SELECT gid FROM `Projects_x_Usergroups` WHERE pid IN ($fromstring) AND gid = '$groupid'", "Projects_x_Usergroups");

            if (count($check) > 0) {
                $oldacc = 1;
            } else {
                $oldacc = 0;
            }
            $check = runQuery("SELECT gid FROM `Projects_x_Usergroups`WHERE pid = '$targetpid' AND gid = '$groupid'", "Projects_x_Usergroups");

            if (count($check) > 0) {
                $newacc = 1;
            } else {
                $newacc = 0;
            }
            echo "<tr><th class=left NOWRAP>$gname</th><td>$perm</td><td>" . $yesno[$oldacc] . " ($pname)</td><td><span id=add$groupid>" . $yesno[$newacc] . "</span></td></tr>";
        }
    }
    // seperate users
    echo "</table></p>";
    echo "<p style='margin-top:2em'><table cellspacing=0 class=w75>";
    $users = runQuery("SELECT u.FirstName, u.LastName, pu.uid, pu.editvariant, pu.editclinic, pu.editsample, pu.pid FROM `Projects_x_Users` pu JOIN `Users` u ON pu.uid = u.id WHERE pid IN ($fromstring) OR pid = $targetpid", "Projects_x_Users:Users");
    if (count($users) > 0) {
        $doneusers = array();
        echo "<tr><th colspan=4 class=top>Seperate Users</th></tr>";
        echo "<tr><th class=top>User Name</th><th class=top>Source Project Permission</th><th class=top>Target Project Permission</th></tr>";
        foreach ($users as $k => $ur) {
            $thisuserid = $ur['uid'];
            if ($doneusers[$thisuserid] == 1) {
                # user done before
                continue;
            }
            // trigger summary for all users with access to either source or target
            doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE `id` = '$thisuserid'", "Users:Variants_x_Users_Summary:Summary");
            // update summarystatus.
            //doQuery("UPDATE `Users` SET `SummaryStatus`= 0 WHERE `id` = '$thisuserid'","Users:Variants_x_Users_Summary:Summary");
            $doneusers[$thisuserid] = 1;
            $thisusername = $ur['FirstName'] . " " . $ur['LastName'];
            ## get group permissions
            $editcnv = $ur['editvariant'];
            $editclinic = $ur['editclinic'];
            $editsample = $ur['editsample'];
            $project = $ur['pid'];
            if ($editcnv == 1 && $editclinic == 1) {
                $perm = 'Full';
            } elseif ($editcnv == 1) {
                $perm = 'CNV';
            } elseif ($editclinic == 1) {
                $perm = 'Clinic';
            } else {
                $perm = 'Read-only';
            }
            if ($editsample == 1) {
                $perm .= " / Project Control";
            }
            ## check other project permission for current user.
            if (array_key_exists($project, $fromarray)) {
                $oldperm = $perm;
                $cr = array_shift(...[runQuery("SELECT editvariant, editclinic, editsample FROM `Projects_x_Users` WHERE pid = $targetpid AND uid = $thisuserid", "Projects_x_Users")]);
                if (is_array($cr) && count($cr) > 0) {
                    $editcnv = $cr['editvariant'];
                    $editclinic = $cr['editclinic'];
                    $editsample = $cr['editsample'];
                    if ($editcnv == 1 && $editclinic == 1) {
                        $perm = 'Full';
                    } elseif ($editcnv == 1) {
                        $perm = 'Variant';
                    } elseif ($editclinic == 1) {
                        $perm = 'Clinic';
                    } else {
                        $perm = 'Read-only';
                    }
                    if ($editsample == 1) {
                        $perm .= " / Project Control";
                    }
                    $newperm = $perm;
                } else {
                    $newperm = 'none';
                }
            } else {
                $newperm = $perm;
                $cr = array_shift(...[runQuery("SELECT editvariant, editclinic, editsample FROM `Projects_x_Users` WHERE pid IN ($fromstring) AND uid = $thisuserid", "Projects_x_Users")]);
                if (is_array($cr) && count($cr) > 0) {
                    $editcnv = $cr['editvariant'];
                    $editclinic = $cr['editclinic'];
                    $editsample = $cr['editsample'];
                    if ($editcnv == 1 && $editclinic == 1) {
                        $perm = 'Full';
                    } elseif ($editcnv == 1) {
                        $perm = 'Variant';
                    } elseif ($editclinic == 1) {
                        $perm = 'Clinic';
                    } else {
                        $perm = 'Read-only';
                    }
                    if ($editsample == 1) {
                        $perm .= " / Project Control";
                    }
                    $oldperm = $perm;
                } else {
                    $oldperm = 'none';
                }
            }
            // print user
            echo "<tr><th class=left NOWRAP>$thisusername</th><td >$oldperm</td><td >$newperm </td></tr>";
        }
    }
    echo "</table></p>";

    // NOW MOVE THE SAMPLES
    doQuery("UPDATE `Projects_x_Samples` SET pid = '$targetpid' WHERE sid IN ($instring)", "Projects_x_Samples:Variants_x_Projects_Summary:Summary");
    // Remove empty projects. 
    foreach ($fromarray as $pid => $dummy) {
        $c = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$pid'", "Projects_x_Samples");
        if (count($c) == 0) {
            // remove project
            doQuery("DELETE FROM `Projects` WHERE id = '$pid'", "Projects");
            // remove from permissions
            doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$pid'", "Projects_x_Users");
            doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$pid'", "Projects_x_Usergroups");
            //clearMemcache("Projects:Projects_x_Users:Projects_x_Usergroups");
        } else {
            doQuery("UPDATE `Projects` SET `SummaryStatus` = 0 WHERE id = '$pid'", "Projects"); // summary tables were cleared above.
            // users with access were updated above (in user loop).
        }
    }

    // present user with button to go back to 'recent'
    echo "<h3>Sample Migration Done</h3>";
    echo "<p>Samples were moved successfully to '$NewName'</p>";
    echo "<p><form action='index.php?page=recent' method=POST><input type=submit name='dummy' value='Back To Recent Samples'></form></p>";
    exit();
}

///////////////////////////////
// UPDATE SAMPLES SEPERATELY //
///////////////////////////////
if (isset($_POST['UpdateDetails'])) {
    // get the recent samples
    $rows = runQuery("SELECT s.id AS sid, s.Name AS sname, s.gender, p.id AS pid FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects` p JOIN `Projects_x_Users` pu ON s.id = ps.sid AND p.id = ps.pid AND pu.pid = p.id WHERE pu.uid = $userid AND pu.editsample = 1 AND s.filed = 0 ORDER BY s.id DESC", "Samples:Projects_x_Samples:Projects:Projects_x_Users");
    foreach ($rows as $k => $row) {
        $sid = $row['sid'];
        $oldpid = $row['pid'];
        $newgender = $_POST["gender$sid"];
        $newname = addslashes($_POST["sname$sid"]);
        $newproject = $_POST["pid$sid"];
        $oldname = $row['sname'];
        $oldgender = $row['gender'];
        $update = 0;
        // update sample if changed.
        if ($newgender != $oldgender || $newname != $oldname) {
            $update = 1;
            doQuery("UPDATE `Samples` SET gender = '$newgender', Name = '$newname' WHERE id = '$sid'", "Samples:Variants_x_Users_Summary:Variants_x_Projects_Summary:Summary");
        }
        // update project if changed
        if ($newproject != $oldpid) {
            $update = 1;
            doQuery("UPDATE `Projects_x_Samples` SET pid = '$newproject' WHERE sid = '$sid'", "Projects_x_Samples:Variants_x_Projects_Summary:Summary");
            // delete project if empty
            $c = runQuery("SELECT sid FROM `Projects_x_Samples` WHERE pid = '$oldpid'", "Projects_x_Samples");
            if (count($c) == 0) {
                doQuery("DELETE FROM `Projects` WHERE id = '$oldpid'", "Projects");
                doQuery("DELETE FROM `Projects_x_Users` WHERE pid = '$oldpid'", "Projects_x_Users");
                doQuery("DELETE FROM `Projects_x_Usergroups` WHERE pid = '$oldpid'", "Projects_x_Usergroups");
            }
            // force summary rebuild on both projects.
            //doQuery("UPDATE `Projects` SET `SummaryStatus`= 0 WHERE id IN ('$oldpid','$newproject')", "Projects"); // summary table caches cleared above.
        }
        if ($update == 1) {
            // either moved or genders changed : schedule update. 
            doQuery("UPDATE Projects SET SummaryStatus = 0 WHERE id IN ('$oldpid','$newproject')", "Projects");
            $urows = runQuery("SELECT `uid` FROM `Projects_x_Users` WHERE `pid` in ('$newproject','$oldpid')", "Projects_x_Users");
            foreach ($urows as $urow) {
                $thisuid = $urow['uid'];
                // this updates the user-summary tables. Aggressive approach, not sure anything was changed.
                doQuery("UPDATE `Users` SET `SummaryStatus` = 0 WHERE `id` = '$thisuid'", "Users");
            }
        }
    }
}

////////////////////
// FINISH SAMPLES //
////////////////////
if (isset($_POST['FinishSamples'])) {
    $Finish = $_POST['batchMove'];
    $sids = implode("','", $Finish);
    foreach ($Finish as $key => $sid) {
        doQuery("UPDATE `Samples` SET filed = 1 WHERE id IN ('$sids')", "Samples"); // no impact on summary tables
    }
    //clearMemcache("Samples");

}

////////////////////////////
// PRINT FORM FOR EDITING //
////////////////////////////
## Get all projects you have 'editsample' access to .
$rows = runQuery("SELECT p.id, p.Name FROM `Projects` p JOIN `Projects_x_Users` pu ON p.id = pu.pid WHERE pu.uid = '$userid' AND pu.editsample = 1", "Projects:Projects_x_Users");
$projects = array();
foreach ($rows as $k => $row) {
    $projects[$row['Name']] = $row['id'];
}
if (count($projects)) {
    ksort($projects);
}

// prepare output.

if (isset($_GET['p'])) {
    $p = $_GET['p'];
} else {
    $p = 1;
}

$offset = ($p - 1) * 100;


$from_to = "Sample " . (($p - 1) * 100 + 1) . " to " . (($p) * 100);
## prepare output table
echo "<div class=section><h3>Recent samples: $from_to</h3>";

// count.
$nrS = array_shift(...[runQuery("SELECT COUNT(s.id) as NrSamples FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON s.id = ps.sid AND pu.pid = ps.pid WHERE pu.uid = $userid AND pu.editsample = 1 AND s.filed = 0", "Samples:Projects_x_Samples:Projects:Projects_x_Users")]);
$nrS = $nrS['NrSamples'];


## get the non-filed samples you're allowed to edit.
$rows = runQuery("SELECT s.id AS sid, s.Name AS sname, s.gender, p.id AS pid FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects` p JOIN `Projects_x_Users` pu ON s.id = ps.sid AND p.id = ps.pid AND pu.pid = p.id WHERE pu.uid = $userid AND pu.editsample = 1 AND s.filed = 0 ORDER BY s.id DESC LIMIT $offset, 100", "Samples:Projects_x_Samples:Projects:Projects_x_Users");
// no samples. short text and exit.
if (count($rows) == 0) {
    echo "<p>You have no recent samples. You can manage your samples from the 'Manage Samples' page.</p>";
    exit();
}

echo "<p>Select Page: <select id='setPage' onChange='SetPage()'>";
$i = 1;
while ($i * 100 < $nrS) {
    if ($i == $p) {
        $selected = 'SELECTED';
    } else {
        $selected = '';
    }
    $from = ($i - 1) * 100 + 1;
    $to = $from + 99;
    echo "<option value='$i' $selected>Samples $from to $to</option>";
    $i += 1;
}
echo "</select> <span class=italic>(Unsaved changes are discarded!)</p>";


// create form and print intro
echo "<p>The following samples were recently added to the database, and need some additional information. Multiple samples can be selected and moved or finished at once, but in that case, any changes made to individual entries will be ignored. By 'Update Samples', all changes made to all samples are stored. </p>";
echo "<p>Once you hit 'Sample Finished' for one or more samples, these will be removed from this list of 'Recent' samples, and can be accessed on the 'Manage Samples' page from further annotation.</p>";
echo "<p><form name='sampleform' action='index.php?page=recent' method='POST'><table cellspacing=0>";
echo "<thead><tr><th class=top style='padding-left:0.5em;width:2.5em'><input type=checkbox id=BatchBox onChange='BatchSet();' ></th><th class=top>Sample Name</th><th class=top>Gender</th><th class=top>Project</th><th class=top>Family</th><th class=top>Actions</th></tr></thead>";
echo "<tbody>";

## get the non-filed samples you're allowed to edit.
foreach ($rows as $k => $row) {
    $out = "<tr class=vrow><td><input type=checkbox name='batchMove[]' value='" . $row['sid'] . "' onChange='toggleDisplay();'></td>";
    $out .= "<td><input type=text name='sname" . $row['sid'] . "' value='" . stripslashes($row['sname']) . "' size=60></td>";
    switch ($row['gender']) {
        case "Male":
            $out .= "<td><select name='gender" . $row['sid'] . "' ><option value='Undef'>Unknown</option><option value='Male' SELECTED>Male</option><option value='Female' >Female</option></select></td>";
            break;
        case "Female":
            $out .= "<td><select name='gender" . $row['sid'] . "' ><option value='Undef'>Unknown</option><option value='Male'>Male</option><option value='Female' SELECTED>Female</option></select></td>";
            break;
        default:
            $out .= "<td><select name='gender" . $row['sid'] . "' ><option value='Undef' SELECTED>Unknown</option><option value='Male'>Male</option><option value='Female' >Female</option></select></td>";
            break;
    }
    $out .= "<td><select name='pid" . $row['sid'] . "'>";
    foreach ($projects as $pname => $pid) {
        $selected = '';
        if ($pid == $row['pid']) {
            $selected = 'SELECTED';
        }
        $out .= "<option value='$pid' $selected>$pname</option>";
    }
    $out .= "</select></td>";
    ## Get family info (Replicate, children, siblings)
    $subrows =  runQuery("SELECT sid2, Relation FROM `Samples_x_Samples` WHERE sid1 = " . $row['sid'], "Samples_x_Samples");
    $family = array();
    foreach ($subrows as $k => $subrow) {
        $family[$subrow['Relation']][$subrow['sid2']] = 1;
    }
    # Get family info (parents)
    $subrows = runQuery("SELECT sid1 FROM `Samples_x_Samples` WHERE sid2 = " . $row['sid'] . " AND Relation = 2", "Samples_x_Samples");
    foreach ($subrows as $k => $subrow) {
        $family[4][$subrow['sid1']] = 1;
    }
    ## create family string
    $famcode = array(1 => 'Replicates', 2 => 'Children', 3 => 'Siblings', 4 => 'Parents');
    $famstring = '';
    for ($i = 1; $i <= 4; $i++) {
        if (array_key_exists($i, $family) && count($family[$i]) > 0) {
            $famstring .= count($family[$i]) . " " . $famcode[$i] . ", ";
        }
    }
    if ($famstring != '') {
        $famstring = substr($famstring, 0, -2);
    } else {
        $famstring = 'None Defined';
    }
    $out .= "<td>$famstring</td>";
    $out .= "<td><a href='index.php?page=samples&pt=single&sid=" . $row['sid'] . "' style='font-style:italic'>Edit</a> | <a href='index.php?page=variants&s=" . $row['sid'] . "' style='font-style:italic'>Filter</a></td>";
    $out .= "</tr>";
    echo $out;
}
echo "</table></p>";

// spans for action buttons
echo "<span id='BatchMove' style='display:none;margin-top:1em;margin-bottom:1em;'>";
echo "<input type=submit name='FinishSamples' value='Finish Selected Samples'> &nbsp; OR &nbsp; ";
echo "Move Selected to this project: ";
echo "<select name='BatchToPid' id='MoveToPidSelect' onChange='CheckNew()'>";
echo " <option value='new'>Create New Project</option>";
foreach ($projects as $pname => $pid) {
    echo "<option value='$pid' $selected>$pname</option>";
}
echo "</select>";
echo "<input type=text size=40 name='NewProjectName' id='NewProjectName' >";
echo "<input type=submit name=BatchMove value='Move Samples' ></span>";
echo "<p id='UpdateButton'><input type=submit name='UpdateDetails' value='Update Samples'></p>";

echo "</form>";
echo "</div>";

?>

<!-- javascript -->
<!--<script type='text/javascript' src='javascripts/page.recent.js'></script> -->