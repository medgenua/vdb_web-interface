<?php
$lastname = "";
$firstname = "";
$email = "";
$affiliation = "";
$mailing = "0";
$check = "";
$password = "";
$confirm = "";
if (isset($_POST['register'])) {
    $lastname = $_POST['lastname'];
    $firstname = $_POST['firstname'];
    $email = $_POST['email'];
    $affiliation = $_POST['affiliation'];
    if ($affiliation == 'other') {
        $affiliation = $_POST['NewAffiliation'];
        $newaffi = 1;
    } else {
        $newaffi = 0;
    }
    $mailing = $_POST['mailing'];
    #$text = $_POST['text'];
    $check = $_POST['check'];
    #$requsername = $_POST['requsername'];
    $password = $_POST['password'];
    $confirm = $_POST['confirm'];

    if ($lastname == "Last Name" || $lastname == "" || $firstname == "First Name" || $firstname == "") {
        echo "<div class=sectie><h3>No name specified</h3><p>Please enter your name.</p></div>";
    } elseif (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email) || $email == "") {
        echo "<div class=sectie><h3>Invalid email address</h3><p>Please correct your emailadres to a valid one.</p></div>";
    } elseif ($affiliation == "Affiliation" || $affiliation == "") {
        echo "<div class=sectie><h3>No affiliation specified</h3><p>Please enter the university or company you work for.</p></div>";
    } elseif ($password == "") {
        echo "<div class=sectie><h3>No password specified</h3><p>Please choose your password.</p></div>";
    } elseif ($password != $confirm) {
        echo "<div class=sectie><h3>Passwords don't match</h3><p>Please enter your password again and make sure the two fields match.</p></div>";
    } elseif ($check != "G" && $check != "g") {
        echo "<div class=sectie><h3>Wrong answer</h3><p>Answer the question please. This is a simple anti-spam check. If you don't know the answer, it's \"G\". :-) </p></div>";
    } elseif (isset($_POST['sendnews']) && $sendnews != "") {
        ## this is a hidden field, so most likely spam. send message of succes, but do nothing
        echo "Registration Complete !\n";
    } else {
        $result = runQuery("SELECT id FROM `Users` WHERE email = '" . addslashes($email) . "'", "Users");
        if (count($result) > 0) {
            echo "<div class=sectie><h3>Username exists</h3><p>'$email' is already registered. Did you forget your password? Please request a new password here: <a href='index.php?page=send_resetemail'>Request password reset</a></p></div>";
        } else {
            $lastname = addslashes($lastname);
            $firstname = addslashes($firstname);
            $affiliation = addslashes($affiliation);
            $email = addslashes($email);
            //$requsername = addslashes($requsername);
            if ($newaffi == 1) {
                $affid = insertQuery("INSERT INTO `Affiliation` (name) VALUES('$affiliation')", "Affiliation");
                $affiliation = $affid;
            }
            $uid = insertQuery("INSERT INTO `Users` (LastName, FirstName, Affiliation, email, password, level, mailinglist) VALUES ('$lastname', '$firstname', '$affiliation', '$email', MD5('$password'), '1','$mailing')", "Users");

            $subject = "VariantDB : Account Request";
            //compose message, etc
            $message = "Message sent from https://$domain\r";
            $message .= "Sent by: $adminemail\r";
            $message .= "Subject: $subject\r";
            $message .= "Registered Email: $email\r";
            $message .= "Your email address was successfully registered on the VariantDB website.\r\r";
            $headers = "From: $adminemail\r\n";
            $headers .= "To: $adminemail, $email\r\n";

            /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */

            if (mail($email, "$subject", $message, $headers)) {

                echo "<h4>Your email address was successfully registered on the VariantDB website.</h4>";
                //echo "<p>The email has also been sent your own specified address.</p>";
                echo "<p><a href=\"index.php?page=main\">Back to Homepage</a></p>";
            } else {
                echo "<h4>Can't send email </h4>";
            }
        }
    }
} else {
?>

    <div class=section>
        <h3>Register Your Email Address</h3>
        <p>Fill in the form below to register on the website (all fields are mandatory). Activation of the account is automatic and instant.</p>


        <?php


        if ($lastname == "")
            $lastname = "Last Name";
        if ($firstname == "")
            $firstname = "First Name";
        if ($email == "")
            $email = "Email";
        ?>

        <form action="index.php?page=request_user" method="POST">
            <table cellspacing=0>
                <tr>
                    <th class=left>Last Name:</th>
                    <td><input type="text" name="lastname" value="<?php echo $lastname ?>" size="40" maxlength="40" onfocus="if (this.value == 'Last Name') {this.value = '';}" /></td>
                </tr>
                <tr>
                    <th class=left>First Name:</th>
                    <td><input type="text" name="firstname" value="<?php echo $firstname ?>" size="40" maxlength="40" onfocus="if (this.value == 'First Name') {this.value = '';}" /></td>
                </tr>
                <tr>
                    <th class=left>Your email:</th>
                    <td><input type="text" name="email" value="<?php echo $email ?>" size="40" maxlength="50" onfocus="if (this.value == 'Email') {this.value = '';}" /></td>
                </tr>
                <tr>
                    <th class=left> Affiliation:</th>
                    <td><select name=affiliation>
                            <?php
                            if ($affiliation == 'other' || $affiliation == '') {
                                echo "<option value='other'>Other, Specify below</option>\n";
                            }
                            $rows = runQuery("SELECT id, name FROM `Affiliation` ORDER BY name", "Affiliation");
                            foreach ($rows as $k => $row) {
                                $affid = $row['id'];
                                $affname = $row['name'];
                                if ($affid == $affiliation) {
                                    echo "<option value='$affid' SELECTED>$affname</option>\n";
                                } else {
                                    echo "<option value='$affid'>$affname</option>\n";
                                }
                            }
                            echo "</select>\n";
                            if ($affiliation == '' || $affilation == 'other') {
                                $affiliation = 'New Affiliation';
                            }
                            ?>
                </tr>
                <tr>
                    <th class=left>&nbsp;</th>
                    <td> OR <input type="text" name="NewAffiliation" value="<?php echo $affiliation ?>" size="37" maxlength="255" onfocus="if (this.value == 'New Affiliation') {this.value = '';}" /></td>
                </tr>
                <tr>
                    <th class=left>Recieve E-Mails</th>
                    <td><select id='mailing' name='mailing'>
                            <?php
                            if ($mailing == 0) {
                                echo "<option value='0' SELECTED>No emails</option><option value='1'>Critical (e.g. bugs affecting #results)</option><option value='2'>Critical &amp; New Features (max 1/month)</option>";
                            } elseif ($mailing == 1) {
                                echo "<option value='0' >No</option><option value='1' SELECTED>Critical (e.g. bugs affecting #results)</option><option value='2'>Critical &amp; New Features (max 1/month)</option>";
                            } elseif ($mailing == 2) {
                                echo "<option value='0' >No</option><option value='1' >Critical (e.g. bugs affecting #results)</option><option value='2' SELECTED>Critical &amp; New Features (max 1/month)</option>";
                            }
                            ?>
                        </select></td>
                </tr>

                <tr>
                    <th class=left>Choose a Password:</th>
                    <td><input type="password" name="password" size="40" maxlength="20" /></td>
                </tr>
                <tr>
                    <th class=left>Confirm Password:</th>
                    <td><input type="password" name="confirm" size="40" maxlength="20" /></td>
                </tr>
                <tr>
                    <th class=left>DNA consists of A, C, T and ... ?</th>
                    <td><input type="text" name="check" value="You should know this" size="40" onfocus="if (this.value == 'You should know this') {this.value = '';}" /></td>
                </tr>
                <tr>
                    <td class='sendnews'>Provide an email where we will send the monthly news</td>
                    <td class='sendnews'><input type=text name=sendnews size=25></td>
                </tr>
                <tr>
                    <td><input type="submit" name=register value=" Send " class=button></td>
                </tr>
            </table>
    </div>
<?php
}
?>