<?php


// STORE POSTED VARS IF ANY
if (isset($_POST['register'])) {
    $lastname = $_POST['lastname'];
    $firstname = $_POST['firstname'];
    $email = $_POST['email'];
    $affiliation = $_POST['affiliation'];
    if ($affiliation == 'other') {
        $affiliation = $_POST['NewAffiliation'];
        $newaffi = 1;
    } else {
        $newaffi = 0;
    }
    $requsername = $_POST['requsername'];
    $password = $_POST['password'];
    $confirm = $_POST['confirm'];
} else {
    $lastname = '';
    $firstname = '';
    $email = '';
    $affiliation = '';
    $requsername = '';
    $password = '';
    $confirm = '';
}
// FIRST IF SUBMITTED, CHECK AND CREATE USER
if ($_POST['register']) {
    if ($lastname == "" || $firstname == "") {
        echo "<div class=section><h3>No name specified</h3><p>Please enter your name.</p></div>";
    } elseif (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email) || $email == "") {
        echo "<div class=sectie><h3>Invalid email address</h3><p>Please correct your emailadres to a valid one.</p></div>";
    } elseif ($affiliation == "Affiliation" || $affiliation == "") {
        echo "<div class=sectie><h3>No affiliation specified</h3><p>Please enter the university or company you work for.</p></div>";
    }
    #elseif ($requsername == "Requested User Name" || $requsername == "") 
    #{
    #	  echo "<div class=sectie><h3>No username specified</h3><p>Please choose a username.</p></div>";
    #}
    elseif ($password == "") {
        echo "<div class=sectie><h3>No password specified</h3><p>Please choose your password.</p></div>";
    } elseif ($password != $confirm) {
        echo "<div class=sectie><h3>Passwords don't match</h3><p>Please enter your password again and make sure the two fields match.</p></div>";
    } else {
        $row = array_shift(...[runQuery("SELECT name, StringName, Description FROM `NGS-Variants-Admin`.`CurrentBuild` LIMIT 1")]);
        $_SESSION['dbname'] = $row['name'];
        $_SESSION['dbstring'] = $row['StringName'];
        $_SESSION['dbdescription'] = stripslashes($row['Description']);

        $result = runQuery("SELECT id FROM `Users` WHERE email = '" . addslashes($email) . "'");
        if (!empty($result)) {
            echo "<div class=sectie><h3>Email Address Already Registered</h3><p>The user $email already exists. Please choose a different email address.</p></div>";
        } else {
            $lastname = addslashes($lastname);
            $firstname = addslashes($firstname);
            $affiliation = addslashes($affiliation);
            $email = addslashes($email);
            #$requsername = addslashes($requsername);
            $text = addslashes($text);
            if ($newaffi == 1) {
                $affid = insertQuery("INSERT INTO `Affiliation` (name) VALUES('$affiliation')");
                $affiliation = $affid;
            }
            $query = "INSERT INTO `Users` (LastName, FirstName, Affiliation, email, password, level) VALUES ('$lastname', '$firstname', '$affiliation', '$email', MD5('$password'), '3')";
            $uid = insertQuery($query);

            $subject = "VariantDB: Admin Account Created";
            //compose message, etc
            $message = "Message sent from https://$domain\r";
            $message .= "Sent by: $email\r";
            $message .= "Subject: $subject\r";
            $message .= "Requested login: $email\r";
            $message .= "\r\rThe VariantDB administrator account ($email) has been created. Use this user for maintenance tasks. All Admin-related email will be sent to this address. \r\r";
            $headers = "From: $adminemail\r\n";
            $headers .= "To: $email\r\n";

            /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */

            if (mail($email, "$subject", $message, $headers)) {
                /// user is created
                // set up email 
                #system("find -type f | xargs sed -i s/'ADMIN@EMAIL.com'/'$email'/g;'");
                echo "<div class=section>";
                echo "<h3>VariantDB Final Configuration</h3>";
                echo "<p></p><p><div class=strong>1: setting up administrative account</div></p>";
                echo "<p> &nbsp; &nbsp; => Succeeded.</p>";
                // update website status, all email & passwords are correct. 
                echo "<div class=strong>2: Activating CNV-WebStore</div>";
                doQuery("UPDATE `NGS-Variants-Admin`.`SiteStatus` SET status = 'Operative' WHERE 1");
                clearMemcache("SiteStatus");
                echo "<p> &nbsp; &nbsp; => Done.</p>";
                // this should not be, but if needed, updates the system-admin email address 
                #if ($adminemail == 'ADMIN@EMAIL.COM') {
                #	echo "<p><div class=nadruk>4: Setting up hostname</div></p>";
                #	ob_flush();
                #	flush();
                #	// replace email in credentials file
                #	system("sed -i s/'ADMIN@EMAIL.COM'/'$email'/g /opt/ServerMaintenance/.credentials");
                #}
                ob_flush();
                flush();
                echo "<p>You will now be redirected to the main page.</p>";
                echo "<meta http-equiv='refresh' content='5;URL=index.php'>\n";
                exit;
            } else {
                echo "<h4>Can't send email </h4>";
            }
        }
    }
}
// if not: print form
if ($lastname == "")
    $lastname = "VariantDB";
if ($firstname == "")
    $firstname = "Administrator";
if ($email == "")
    $email = "Email";

echo "<div class=section>";
echo "<h3>VariantDB Final Configuration</h3>";
echo "<p><form action= 'index.php' method=POST></p>";
echo "<div class=strong> 1: Setting up administrative account</div>";
echo "<p> Provide a username and password for the VariantDB adminstrative user.  It is recommended to create a user for this that will not be used for daily usage. The associated email address will be used to send username request, and update information (if wanted). This Email is NOT related to the email address that will recieve database backup notifications.</p>";
?>

<table cellspacing=0>
    <tr>
        <th class=left>Last Name:</td>
        <td><input type="text" name="lastname" value="<?php echo $lastname ?>" size="40" maxlength="40" onfocus="if (this.value == 'VariantDB') {this.value = '';}" /></td>
    </tr>
    <tr>
        <th class=left>First Name:</td>
        <td><input type="text" name="firstname" value="<?php echo $firstname ?>" size="40" maxlength="40" onfocus="if (this.value == 'Administrator') {this.value = '';}" /></td>
    </tr>
    <tr>
        <th>Administrator email:</td>
        <td><input type="text" name="email" value="<?php echo $email ?>" size="40" maxlength="50" onfocus="if (this.value == 'Email') {this.value = '';}" /></td>
    </tr>
    <tr>
        <th> Affiliation:</td>
        <td><select name=affiliation>
                <?php
                if ($affiliation == 'other' || $affiliation == '') {
                    echo "<option value='other'>Other, Specify below</option>\n";
                }
                $rows = runQuery("SELECT id, name FROM `Affiliation` ORDER BY name", "Affiliation");
                foreach ($rows as $k => $row) {
                    $affid = $row['id'];
                    $affname = $row['name'];
                    if ($affid == $affiliation) {
                        echo "<option value='$affid' SELECTED>$affname</option>\n";
                    } else {
                        echo "<option value='$affid'>$affname</option>\n";
                    }
                }
                echo "</select>\n";
                if ($affiliation == '' || $affilation == 'other') {
                    $affiliation = 'New Affiliation';
                }
                ?>
    </tr>
    <tr>
        <th>&nbsp;</td>
        <td> OR <input type="text" name="NewAffiliation" value="<?php echo $affiliation ?>" size="37" maxlength="255" onfocus="if (this.value == 'New Affiliation') {this.value = '';}" /></td>
    </tr>
    <tr>
        <th>Choose a Password:</td>
        <td><input type="password" name="password" size="40" maxlength="20" /></td>
    </tr>
    <tr>
        <th>Confirm Password:</td>
        <td><input type="password" name="confirm" size="40" maxlength="20" /></td>
    </tr>
    <tr>
        <td class=clear><input type="submit" name=register value=" Send " class=button></td>
    </tr>
</table>
</form>
</div>