<?php

class Database
{

private $host;
private $user;
private $pass;

// construct
function __construct( $host,$user,$pass ) {
	$this->host = $host;
	$this->user = $user;
	$this->pass = $pass;
	// connect
	$dsn = "mysql:host=$host;dbname=NGS-Variants-Admin;charset=utf8";
	$options = [
	  PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
	  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
	  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
	];
	try {
	  	$pdo = new PDO($dsn, $user, $pass, $options);
	} catch (Exception $e) {
  		error_log($e->getMessage());
		return FALSE;
	}
	$query = $pdo->query("SELECT name, StringName, Description FROM `CurrentBuild` LIMIT 1");
	$row = $query->fetch();
	$dbname = "NGS-Variants".$row['name'];
	// switch database
	try {
		$pdo->exec("USE `$dbname`");
	} catch (Exception $e) {
		error_log($e->getMessage());
		return FALSE;
	}

	$this->handler = $pdo;
	$this->dbname = $dbname;

}

// validate placeholders vs query in #
private function CheckPlaceholders ($query,$placeholders = array(),$status = FALSE) {
	// validated before (in nested routines)
	if ($status) {
		return array("STATUS"=>TRUE);
	}
	// number of question marks?
	$nr = substr_count($query,'?');
	// array length should be zero if $nr == 0
	if ($nr == 0 && count($placeholders) > 0) {
		return array("ERROR"=>"Placeholder values provided without available slots","STATUS"=>FALSE);
	}
	// single, non-array provided ?
	if (!is_array($placeholders)) {
		$placeholders = array($placeholders);
	}
	// if nr > 0 and placeholders is empty : forgotten?
	if ($nr > 0 && count($placeholders) == 0) {
		return array("ERROR"=>"Query contains placeholder slots, but none were provided","STATUS"=>FALSE);
	}
	// first entry of placeholders is type string. (not for pdo
	//$placeholders[0] = strtolower($placeholders[0]);
	//if (!preg_match("/^[idbs]+$/",$placeholders[0])) {
	//	return array("ERROR"=>"First placeholder item must be a string of types of the actual values (i,d,s,b)","STATUS"=>FALSE);
	//}
	//if (strlen($placeholders[0]) !== $nr) {
	//	return array("ERROR"=>"Incorrect number of placeholder types provided: ".strlen($placeholders[0])," where $nr were needed","STATUS"=>FALSE);
	//}
	// not the correct number of values provided?
	if (count($placeholders) !== $nr ) {
		return array("ERROR"=>"Incorrect number of placeholder values provided: ".count($placeholders)," where $nr were needed","STATUS"=>FALSE);
	}
	return array("STATUS"=>TRUE);

}

// Get a single value (one column, from one row)
public function GetValue ($query,$placeholders = array()) {
	$placeholders = (is_array($placeholders) ? $placeholders : array($placeholders));
	// Get a row.
	$row = $this->GetRow($query,$placeholders);
	// failure.
	if (!$row) {
		return FALSE;
	}
	// success.
	$value = reset($row);
	return $value;

}
public function GetRow($query,$placeholders = array(),$status=FALSE) {
	$placeholders = (is_array($placeholders) ? $placeholders : array($placeholders));
	// check query vs placeholders
	$result = $this->CheckPlaceholders($query,$placeholders);
	if (!$result['STATUS']) {
		error_log("MSQL ERROR : ".$result['ERROR']);
		error_log(json_encode(debug_backtrace()));
		// return.
		return FALSE; //array("STATUS"=>FALSE);
	}
	// prepare:
	try {
		$sth = $this->handler->prepare($query);
	} catch (Exception $e) {
  		error_log("MSQL ERROR : failed to prepare statement");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	// execute
	try {
		$sth->execute($placeholders) ;

	} catch (Exception $e) {
		error_log("MSQL ERROR : failed to execute prepared query");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	// no data ?
	if ($sth->rowCount() == 0) {
		return array();
	}
	// fetch.
	try {
		$row = $sth->fetch();
	} catch (Exception $e) {
		error_log("MSQL ERROR : failed to fetch row");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	return $row;
}

public function GetRows($query,$placeholders = array(),$status=FALSE) {
	$placeholders = (is_array($placeholders) ? $placeholders : array($placeholders));
	// check query vs placeholders
	$result = $this->CheckPlaceholders($query,$placeholders);
	if (!$result['STATUS']) {
		error_log("MSQL ERROR : ".$result['ERROR']);
		error_log(json_encode(debug_backtrace()));
		// return.
		return FALSE; //array("STATUS"=>FALSE);
	}
	// prepare:
	try {
		$sth = $this->handler->prepare($query);
	} catch (Exception $e) {
  		error_log("MSQL ERROR : failed to prepare statement");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	// execute
	try {
		$sth->execute($placeholders) ;

	} catch (Exception $e) {
		error_log("MSQL ERROR : failed to execute prepared query");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	// no data ?
	if ($sth->rowCount() == 0) {
		return array();
	}
	// fetch.
	try {
		$rows = $sth->fetchAll();
	} catch (Exception $e) {
		error_log("MSQL ERROR : failed to fetch row");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	return $rows;
}

// INSERT data : return insertid
public function InsertRow($query,$placeholders) {
	$placeholders = (is_array($placeholders) ? $placeholders : array($placeholders));
	// check query vs placeholders
	$result = $this->CheckPlaceholders($query,$placeholders);
	if (!$result['STATUS']) {
		error_log("MSQL ERROR : ".$result['ERROR']);
		error_log(json_encode(debug_backtrace()));
		// return.
		return FALSE; //array("STATUS"=>FALSE);
	}
	// prepare:
	try {
		$sth = $this->handler->prepare($query);
	} catch (Exception $e) {
  		error_log("MSQL ERROR : failed to prepare statement");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	// execute
	try {
		$sth->execute($placeholders) ;

	} catch (Exception $e) {
		error_log("MSQL ERROR : failed to execute prepared query");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	// get ID
	$id = $this->handler->lastInsertId();
	return($id);
}


// execute without returning anything
public function Execute($query,$placeholders=array()) {
	$placeholders = (is_array($placeholders) ? $placeholders : array($placeholders));
	// check query vs placeholders
	$result = $this->CheckPlaceholders($query,$placeholders);
	if (!$result['STATUS']) {
		error_log("MSQL ERROR : ".$result['ERROR']);
		error_log(json_encode(debug_backtrace()));
		// return.
		return FALSE; //array("STATUS"=>FALSE);
	}
	// prepare:
	try {
		$sth = $this->handler->prepare($query);
	} catch (Exception $e) {
  		error_log("MSQL ERROR : failed to prepare statement");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	// execute
	try {
		$sth->execute($placeholders) ;

	} catch (Exception $e) {
		error_log("MSQL ERROR : failed to execute prepared query");
		error_log($e->getMessage());
		error_log(json_encode(debug_backtrace()));
		return FALSE;
	}
	return TRUE;
}

// end of class
}
