<!-- First unset all cookies -->

<script type='text/javascript'>
    function deleteCookies() {
        var allcookies = document.cookie.split(";");

        for (var i = 0; i < allcookies.length; i++) {
            var cookie = allcookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
    deleteCookies();
</script>

<?php
// then delete all cookies from the DB.
doQuery("DELETE FROM `Cookies` WHERE ckey LIKE '$uid" . "_%'", 'Cookies');
// then destroy the session
$_SESSION = array();
session_destroy();
?>

<!-- Then redirect to the variants page. -->
<meta http-equiv='refresh' content='0;URL=index.php?page=variants'>