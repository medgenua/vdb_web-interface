var xmlhttp;
var response;
var spanid;
var content;
function showinfo(gid,uid)
{
	spanid = 'group_'+gid;
	response = '';
	content = document.getElementById(spanid).innerHTML;
	// show
	if (content == '') {

		var url="ajax_queries/GetGroupInfo.php";
		var pars = { 	q:gid,
				u:uid,
				rv:Math.random()
		};
		$('#'+spanid).load(url,pars);
	}
	else {
		//document.getElementById(spanid).innerHTML = '';
		$('#'+spanid).html('');
		
	}
}


function ToggleAll(institute) {
	// check if checked or unchecked.
	var check_status = document.getElementById('AddAll_'+institute).checked
	// get checkboxes in the category.
	var parent = document.getElementById('AddFrom_'+institute)
	var checkboxes = parent.getElementsByTagName('input')
	for (var i = 0; i< checkboxes.length ; i++) {
		checkboxes[i].checked = check_status
	}
}


function LoadGroupInfo(userid) {
	var selected_group = $('#JoinSelect').val();
	var url='ajax_queries/GetGroupInfo.php';
	var pars = {	u:userid,
			q:selected_group,
			a:0
	}
	$('#GroupDetails').load(url,pars);
}
