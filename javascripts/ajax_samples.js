// general counters
var listed = 0;

function GetXmlHttpObject() {
	if (window.XMLHttpRequest) {
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  return new XMLHttpRequest();
	}
	if (window.ActiveXObject) {
  		// code for IE6, IE5
  		return new ActiveXObject("Microsoft.XMLHTTP");
  	}
	return null;
}

// function to toggle sample selection
function SetSampleSelect(userid) {
	var type = document.getElementById("selecttype").options[document.getElementById("selecttype").selectedIndex].value;
	if (type == 'List') {
		
		// get options from ajax 
		xmlhttp=GetXmlHttpObject();
		if (xmlhttp==null) {
			alert ("Browser does not support HTTP Request");
			return;
		}
		// settings 
		var url="ajax_queries/GetSampleSelect.php";
		url=url+"?uid="+userid;
		url=url+"&rv="+Math.random();
		xmlhttp.onreadystatechange=function() {
		   if (xmlhttp.readyState==4) {
			 document.getElementById('sampleselect').innerHTML = xmlhttp.responseText;	
		   }
		};
		xmlhttp.open("GET",url,true);
		xmlhttp.send(null);
	
	}
	else {
		// provide the text field for autosuggest
		document.getElementById('sampleselect').innerHTML = "<input type=hidden name=sampleid id=sampleid value=''><input type=text style='width:98%;' value='' name=samplesource id=searchfield>";
		// reload the autosuggest.
		var as_json = new bsn.AutoSuggest('searchfield', options);	
	}
}


/////////////////////////////////////////////////////////////////////////////////
// function to provide the sample details for the currently selected sample ID //
/////////////////////////////////////////////////////////////////////////////////
var RelationCounter = 0;
function LoadDetails() {
	// RESET THE RELATION COUNTER 
	RelationCounter = 0;	
	// get selected option
	var sid = document.getElementById('sampleid').value;
	if (sid == "") {
		alert("You need to set a sample first!");
		return;
	}
	document.getElementById('SampleDetails').innerHTML = "<div class='w100' style='text-align:center'><img src='Images/layout/ajax-loader.gif' ><br/>Loading Results...</div>";

	// create ajax request
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}
	// url
	var url="ajax_queries/SampleDetails.php";
	url=url+"?sid="+sid;
	url=url+"&rv="+Math.random();
	xmlhttp.onreadystatechange=function() {
	   if (xmlhttp.readyState==4) {
		// get relevant row 
		//var row = document.getElementById("FilterStep"+index);
		// settings Arguments come in column 3 
		//row.cells[2].innerHTML = xmlhttp.responseText;	
		document.getElementById('SampleDetails').innerHTML = xmlhttp.responseText;
	   }

	};
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);

}

// ADD A RELATION
function AddRelation(sid, relationID) {
	if (sid == "") {
		alert("You need to set a sample first!");
		return;
	}
	if (relationID == "") {
		alert("No RelationID found. Please report this.");
		return;
	}
	RelationCounter = RelationCounter + 1;
	// create ajax request
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}
	// url
	var url="ajax_queries/AddRelationRow.php";
	url=url+"?sid="+sid;
	url=url+"&RelCount="+RelationCounter;
	url=url+"&rv="+Math.random();
	// add row
	var tbody = document.getElementById('Relation'+relationID);
	var row = tbody.insertRow(-1);
	row.id = "RelationRow"+RelationCounter;
	// get number of cells (from 1th row = row with general sample details)
	var cells = document.getElementById("DetailsTable").rows[1].cells.length;
	// add cells to the row.
	var i;
	for (i = 0; i < (cells -1); i++) {
		var newCell = row.insertCell(i);
	}
	// first cell : trashbin
	row.cells[0].innerHTML = "<span style='float:right' onClick=\"RemoveRelationSelector('"+RelationCounter+"')\"><img src='Images/layout/minus-icon.png' style='height:1em;'><input type=hidden id='RelationType"+RelationCounter+"' value='"+relationID+"'></span>";
	// second cell: loader  
	row.cells[1].innerHTML = '<img src="Images/layout/loader-bar.gif" />';
	row.cells[1].colSpan = (cells - 1);
	
	// insert content when ready
	xmlhttp.onreadystatechange=function() {
	   if (xmlhttp.readyState==4) {
		// second cell: select list from ajax 
		row.cells[1].innerHTML = xmlhttp.responseText;
	   }

	};
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);

}

function RemoveRelationSelector(relcounter) {
	// this function remove a selection box from the form, but does NOT delete relations in the database!!		
	var row = document.getElementById("RelationRow"+relcounter);
	row.parentNode.removeChild(row);
}

function SaveSampleDetails(sid)  {
	if (sid == "") {
		alert("You need to set a sample first!");
		return;
	}
	// compose the url...
	//var url="ajax_queries/RunFilterQuery.php";
	var url="ajax_queries/SetRelations.php";
	///////////////////////
	// set the variables //
	///////////////////////
	// sid
	param="sid="+sid;
	// gender
	param = param + "&gender="+document.getElementById('gender'+sid).value;
	// project
	param = param + "&pid="+ document.getElementById('pid'+sid).value;
	// sample name
	param = param + "&sname="+ encodeURIComponent(document.getElementById('sname'+sid).value);
	// then all newly added relations => # = RelationCounter.
	var idx;
	var posted = 0;	
	for (idx = 1; idx <= RelationCounter; idx++) {
		// process if present (may be removed again by user
		//if (document.getElementById('RelationType'+RelationCounter) != null) {
		if (document.getElementById("RelationSelect"+idx).selectedIndex != null) {
			posted++;
			var RelationSid = document.getElementById('RelationSelect'+idx).options[document.getElementById("RelationSelect"+idx).selectedIndex].value;
			var RelationType = document.getElementById('RelationType'+RelationCounter).value;
			param = param + '&Relation'+posted+'=SET-'+RelationType+'-'+sid+'-'+RelationSid; 
		}
	}
	// then the existing relations
	var selectElements = document.getElementsByTagName('select');
	for(idx=0; idx < selectElements.length; idx++){
		var myID = selectElements[idx].id;
		if (myID.match(/DEL-/) ) {
			if (selectElements[idx].value == 1) {
				posted++;
				// add to params to delete.
				param = param + '&Relation'+posted+'='+myID;
			}
		}
   	}
	// add number of posted to param
	param = param + "&posted="+posted;
	// create the ajax call
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}
	// onchangestate function
	xmlhttp.onreadystatechange=function() {
	   if (xmlhttp.readyState==4) {
	   	// need to reload the sample details here.
		LoadDetails()
	   }
	}
	// SEND REQUEST WITH POST
	xmlhttp.open("POST",url,true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(param);
}

// TOGGLE FORM DISPLAY
function ToggleForm() {
	if (document.getElementById('processingtype').value == 'single') {
		document.getElementById('SingleForm').style.display = '';
		document.getElementById('BatchForm').style.display = 'none';
	}
	else {
		document.getElementById('SingleForm').style.display = 'none';
		document.getElementById('BatchForm').style.display = '';
	}
}
