// set of global variables, used for jslint debugging
/*global window*/
/*global XMLHttpRequest*/
/*global ActiveXObject*/
/*global document*/


// general counters
var listed = 0;

function GetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}

// function to toggle sample selection
function SetSampleSelect(userid) {
    var type = document.getElementById("selecttype").options[document.getElementById("selecttype").selectedIndex].value;
    if (type === 'List') {
        // settings
        var url = "ajax_queries/GetSampleSelect.php";
        var params = {
            uid: userid,
            pid: '',
            rv: Math.random()
        };
        $.get(url, params, function (data) {
            $('#sampleselect').html(data);
        });
    }
    else {
        // provide the text field for autosuggest
        $('#sampleselect').html("<input type=hidden name=sampleid id=sampleid value=''><input type=text style='width:98%;' value='' name=samplesource id=searchfield>");
        // reload the autosuggest.
        var as_json = new bsn.AutoSuggest('searchfield', options);
    }
}

function UpdateProjectSamples(userid) {
    // get selected value in dropdown
    var pid = $('#projectid').find(":selected").val();
    // settings
    var url = "ajax_queries/GetSampleSelect.php";
    var params = {
        uid: userid,
        pid: pid,
        rv: Math.random()
    };
    $.get(url, params, function (data) {
        $('#sampleselect').html(data);
    });
}

/////////////////////////////////////////////////////////////////////////////////
// function to provide the sample details for the currently selected sample ID //
/////////////////////////////////////////////////////////////////////////////////
var RelationCounter = 0;
function LoadDetails() {
    // RESET THE RELATION COUNTER
    RelationCounter = 0;
    // get selected option
    var sid = document.getElementById('sampleid').value;
    if (sid === "") {
        alert("You need to set a sample first!");
        return;
    }
    document.getElementById('SampleDetails').innerHTML = "<div class='w100' style='text-align:center'><img src='Images/layout/ajax-loader.gif' ><br/>Loading Results...</div>";

    // create ajax request
    xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // url
    var url = "ajax_queries/SampleDetails.php";
    url = url + "?sid=" + sid;
    url = url + "&rv=" + Math.random();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // get relevant row
            //var row = document.getElementById("FilterStep"+index);
            // settings Arguments come in column 3 
            //row.cells[2].innerHTML = xmlhttp.responseText;
            document.getElementById('SampleDetails').innerHTML = xmlhttp.responseText;
        }

    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

}

// ADD A RELATION
function AddRelation(sid, relationID) {
    if (sid == "") {
        alert("You need to set a sample first!");
        return;
    }
    if (relationID == "") {
        alert("No RelationID found. Please report this.");
        return;
    }
    RelationCounter = RelationCounter + 1;
    // create ajax request

    // url
    var url = "ajax_queries/AddRelationRow.php";
    var params = {
        sid: sid,
        RelCount: RelationCounter,
        type: 'projects',
        rv: Math.random()
    };
    // add row
    var tbody = document.getElementById('Relation' + relationID);
    var row = tbody.insertRow(-1);
    row.id = "RelationRow" + RelationCounter;
    // get number of cells (from 1th row = row with general sample details)
    var cells = document.getElementById("DetailsTable").rows[1].cells.length;
    // add cells to the row.
    var i;
    for (i = 0; i < (cells - 1); i += 1) {
        var newCell = row.insertCell(i);
    }
    // first cell : trashbin
    row.cells[0].innerHTML = "<span style='float:right' onClick=\"RemoveRelationSelector('" + RelationCounter + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'><input type=hidden id='RelationType" + RelationCounter + "' value='" + relationID + "'></span>";
    // second cell: loader 
    row.cells[1].innerHTML = '<img src="Images/layout/loader-bar.gif" />';
    row.cells[1].colSpan = (cells - 1);

    $.get(url, params, function (data) {
        row.cells[1].innerHTML = data;
    });
}

// load samples from a project to add to relations.
function LoadRelationSamples(relcount, sid) {
    // selected project:
    var sp = $('#RelationSelect' + relcount + '_pid').find(":selected").val();
    console.log("selected: " + sp);

    // url
    var url = "ajax_queries/AddRelationRow.php";
    var params = {
        sid: sid,
        RelCount: relcount,
        type: 'samples',
        pid: sp,
        rv: Math.random()
    };
    $.get(url, params, function (data) {
        $('#ProjectSamples_' + relcount).html(data);
    });
}

function RemoveRelationSelector(relcounter) {
    // this function remove a selection box from the form, but does NOT delete relations in the database!!
    var row = document.getElementById("RelationRow" + relcounter);
    row.parentNode.removeChild(row);
}

function AddPhenotype(sid) {
    // this function provides the overlay and selection box to insert new phenotypes from HPO
    // autocomplete function for phenotypes
    //var as_json = new bsn.AutoSuggest('HPOsearchfield', hpooptions);


    // show overlay
    document.getElementById('overlay').innerHTML = '<div style="position:relative"><h3>Add a Human Phenotype Ontology entry</h3><p style="text-align:center">Start typing a phenotype into the textbox below. After entering two characters, suggestions will load. <br/>Click with the mouse on the item you need to select it, and submit your selection. If you item is not in the list with suggestions, keep typing to load more specific suggestions.<br/>Multiple synonyms are present for many terms. This means that the selected term might be replaced by its canonical name upon submission.</p><p style="text-align:center"><input type=hidden id="HPOid">Phenotype Term: <input type=text id=HPOsearchfield size=40  autocomplete="off" onkeyup="LoadTerms(\'' + sid + '\')"></p><p style="text-align:center"><input type=submit class=button value=Cancel OnClick="document.getElementById(\'overlay\').style.display=\'none\'"> &nbsp; <input type=submit id=hposubmit class=button value=Submit style="display:none"></div><div id=TermOptions></div>';
    //var as_json = new bsn.AutoSuggest('HPOsearchfield', hpooptions);
    document.getElementById('overlay').style.display = '';

}

function LoadTerms(sid) {
    // typed term
    var term = document.getElementById('HPOsearchfield').value;
    if (term.length < 2) {
        return;
    }
    // set loading
    document.getElementById('TermOptions').innerHTML = "<div class='w100' style='text-align:center'><img src='Images/layout/loading.gif' ><br/>Loading Matching HPO Terms</div>";
    // url
    var url = "ajax_queries/ManageHPO.php?action=LoadTerms&sid=" + sid;
    url = url + "&rv=" + Math.random();
    url = url + "&term=" + escape(term);
    xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // insert content when ready
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // select list from ajax
            document.getElementById('TermOptions').innerHTML = xmlhttp.responseText;

            return;
        }

    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);


}

function DelPhenotype(sid, tid) {
    // this function remove a phenotype from the database and from the form.
    var row = document.getElementById("HPO_" + tid);
    // remove from database
    var url = "ajax_queries/ManageHPO.php";
    url = url + "?action=delete";
    url = url + "&sid=" + sid;
    url = url + "&tid=" + tid;
    url = url + "&rv=" + Math.random();
    var delxmlhttp = GetXmlHttpObject();
    if (delxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    delxmlhttp.onreadystatechange = function () {
        if (delxmlhttp.readyState == 4) {
            // second cell: select list from ajax
            row.parentNode.removeChild(row);
        }

    };
    delxmlhttp.open("GET", url, true);
    delxmlhttp.send(null);
}

// ADD Phenotype to sample
function SetHPO(sid, tid) {
    // add phenotype to database
    var url = "ajax_queries/ManageHPO.php";
    url = url + "?action=add";
    url = url + "&sid=" + sid;
    url = url + "&tid=" + tid;
    url = url + "&rv=" + Math.random();
    var addxmlhttp = GetXmlHttpObject();
    if (addxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    addxmlhttp.onreadystatechange = function () {
        if (addxmlhttp.readyState == 4) {
            if (addxmlhttp.responseText == 'exists') {
                // alert user, but keep overlay open.
                alert('The selected phenotype is already associated to this sample.');
            }
            else {
                // add row to table and close overlay.
                var rowcontent = new Array();
                rowcontent = addxmlhttp.responseText.split(":#:");
                // create new row in HPO_body
                var tbody = document.getElementById('HPO');
                var row = tbody.insertRow(-1);
                row.id = "HPO_" + tid;
                // get number of cells (from 1th row = row with general sample details)
                var cells = document.getElementById("DetailsTable").rows[1].cells.length;
                //add cells to the row. // WE ASSUME THREE COLUMNS HERE !!
                var i;
                for (i = 0; i < 3; i += 1) {
                    var newCell = row.insertCell(i);
                    row.cells[i].style.verticalAlign = 'top';
                    row.cells[i].style.paddingBottom = '0.5em';
                }
                // first cell
                row.cells[0].style.paddingLeft = '1em';
                row.cells[0].innerHTML = rowcontent[0];
                // second cell
                row.cells[1].innerHTML = rowcontent[1];
                // merge third cell if more are available
                if (cells > 3) {
                    row.cells[2].colSpan = (cells - 2);
                }
                row.cells[2].innerHTML = rowcontent[2];
                document.getElementById('hpo_header').style.display = '';
                document.getElementById('nopheno').style.display = 'none';
                //row.innerHMTL = rowcontent;
                // close overlay
                document.getElementById('overlay').style.display = 'none';
            }
        }

    };
    addxmlhttp.open("GET", url, true);
    addxmlhttp.send(null);
}

function SaveSampleDetails(sid) {
    if (sid == "") {
        alert("You need to set a sample first!");
        return;
    }
    // compose the url...
    var url = "ajax_queries/SetRelations.php";
    ///////////////////////
    // set the variables //
    ///////////////////////
    // sid
    var params = {
        sid: sid,
        gender: $('#gender' + sid).val(),
        pid: $('#pid' + sid).val(),
        sname: encodeURIComponent($("#sname" + sid).val()),
        sic: $('input[name="IC' + sid + '"]:checked').val(),
        sia: $('input[name="IA' + sid + '"]:checked').val()

    }

    // then all newly added relations => # = RelationCounter.
    var idx;
    var posted = 0;
    for (idx = 1; idx <= RelationCounter; idx++) {
        // process if present (may be removed again by user
        if ($('#RelationSelect' + idx).length == 0) {
            console.log('idx not found' + idx)
            continue;
        }
        // skip if no sample selected
        if ($('#RelationSelect' + idx).find(':selected').length == 0) {
            console.log('idx not selected' + idx);
            continue;
        }
        // add to params    
        posted++;
        var RelationSid = $('#RelationSelect' + idx).find(':selected').val();
        var RelationType = $('#RelationType' + idx).val();
        params['Relation' + posted] = 'SET-' + RelationType + '-' + sid + '-' + RelationSid;
        //param = param + '&Relation' + posted + '=SET-' + RelationType + '-' + sid + '-' + RelationSid;

    }
    // then the existing relations
    var selectElements = document.getElementsByTagName('select');
    for (idx = 0; idx < selectElements.length; idx++) {
        var myID = selectElements[idx].id;
        if (myID.match(/DEL-/)) {
            if (selectElements[idx].value == 1) {
                posted++;
                // add to params to delete.
                params['Relation' + posted] = myID;
                //param = param + '&Relation' + posted + '=' + myID;
            }
        }
    }
    // add number of posted to param
    params['posted'] = posted;
    //param = param + "&posted=" + posted;
    // add free clinical 
    params['FreeClin'] = escape($('#FreeClinArea').val())
    //param = param + "&FreeClin=" + escape(document.getElementById('FreeClinArea').value);
    // create the ajax call
    console.log(params);
    $.post(url, params, function (data) {
        LoadDetails()
    })

}

// TOGGLE FORM DISPLAY
function ToggleForm() {
    if (document.getElementById('processingtype').value == 'single') {
        document.getElementById('SingleForm').style.display = '';
        document.getElementById('BatchForm').style.display = 'none';
    }
    else {
        document.getElementById('SingleForm').style.display = 'none';
        document.getElementById('BatchForm').style.display = '';
    }
}
// autocomplete function for samples
var options = {
    script: "ajax_queries/samplesearch.php?json=true&limit=10&",
    varname: "input",
    json: true,
    shownoresults: true,
    delay: 200,
    cache: false,
    timeout: 10000,
    minchars: 2,
    callback: function (obj) { document.getElementById('searchfield').value = obj.value; document.getElementById('sampleid').value = obj.id; }
};
var as_json = new bsn.AutoSuggest('searchfield', options);

// Load Details for sid if provided
var mysid = document.getElementById('sampleid').value;
if (mysid != '') {
    LoadDetails();
}

// DELETE SAMPLES function from page_samples.
function DeleteSamples(userid) {
    var chk_arr = document.getElementsByName("batchMove[]");
    var chklength = chk_arr.length;
    if (chklength == 0) {
        return;
    }
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p><img src='Images/layout/loader-bar.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p><input type=button value='Cancel' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";

    document.getElementById('overlay').innerHTML = newcontent;
    document.getElementById('overlay').style.display = '';

    var to_delete = '';
    for (k = 0; k < chklength; k++) {
        if (chk_arr[k].checked === true) {
            to_delete += chk_arr[k].value + ',';
        }
    }
    if (to_delete.length > 0) {
        to_delete = to_delete.substring(0, to_delete.length - 1)
        // submit to ajax.
        var xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        // onchangestate function
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                // need to reload the sample details here.
                document.getElementById('overlay').innerHTML = xmlhttp.responseText;
            }
        }
        // settings 
        var url = "ajax_queries/SampleHandler.php";
        url = url + "?uid=" + userid;
        url = url + '&a=d';
        url = url + "&s=" + to_delete;
        // SEND REQUEST WITH POST
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);


    }


}

// DELETE SAMPLES function from page_variants.
function ConfirmedDeleteSamples(userid) {
    var to_delete = document.getElementById('todelete').value;
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p><img src='Images/layout/loader-bar.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p><input type=button value='Close (this might screw up your data)' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";

    document.getElementById('overlay').innerHTML = newcontent;
    document.getElementById('overlay').style.display = '';


    // submit to ajax.
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // need to reload the sample details here.
            document.getElementById('overlay').innerHTML = xmlhttp.responseText;
        }
    }
    // settings 
    var url = "ajax_queries/SampleHandler.php";
    url = url + "?uid=" + userid;
    url = url + '&a=cd';
    url = url + "&s=" + to_delete;
    // SEND REQUEST WITH POST
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

}


// MOVE SAMPLES function from page_samples.
function MoveSamples(userid) {
    var chk_arr = document.getElementsByName("batchMove[]");
    var chklength = chk_arr.length;
    // no samples in sample-list. 
    if (chklength == 0) {
        var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>No Samples Found</span>";
        newcontent += "<p><input type=button value='Close' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";
        document.getElementById('overlay').innerHTML = newcontent;
        document.getElementById('overlay').style.display = '';
        return;
    }
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p><img src='Images/layout/loader-bar.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p><input type=button value='Cancel' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";

    document.getElementById('overlay').innerHTML = newcontent;
    document.getElementById('overlay').style.display = '';

    // get samples
    var to_move = '';
    for (k = 0; k < chklength; k++) {
        if (chk_arr[k].checked === true) {
            to_move += chk_arr[k].value + ',';
        }
    }
    // get target project.
    var targets = document.getElementById('MoveToPidSelect');
    var t_pid = targets.options[targets.selectedIndex].value;



    if (to_move.length > 0) {
        to_move = to_move.substring(0, to_move.length - 1)
        // submit to ajax.
        var xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        // onchangestate function
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                // need to reload the sample details here.
                document.getElementById('overlay').innerHTML = xmlhttp.responseText;
            }
        }
        // settings 
        var url = "ajax_queries/SampleHandler.php";
        url = url + "?uid=" + userid;
        url = url + '&a=m';
        url = url + "&s=" + to_move;
        url = url + "&p=" + t_pid;
        // if target == new, pass name along.
        if (t_pid == 'new') {
            url = url + "&tn=" + document.getElementById('NewProjectName').value;
        }
        url = url + "&rv=" + Math.random();
        // SEND REQUEST WITH POST
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);


    }
    else {
        // submit should be hidden if nothing is selected. But provide an exit option to be sure.
        var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>No Samples Selected</span>";
        newcontent += "<p><input type=button value='Close' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";
        document.getElementById('overlay').innerHTML = newcontent;
        document.getElementById('overlay').style.display = '';
        return;


    }


}


function BatchSet() {
    var BatchBoxVal = document.getElementById('BatchBox').checked;
    for (i = 0; i < document.sampleform.elements.length; i++) {
        var myName = document.sampleform.elements[i].name;
        if (myName.match(/batch/)) {
            if (BatchBoxVal === true) {
                document.sampleform.elements[i].checked = true;
            }
            else {
                document.sampleform.elements[i].checked = false;
            }
        }
    }
    // display batch move options if checkboxes are activated. 
    if (BatchBoxVal === true) {
        document.getElementById('BatchMove').style.display = '';
        document.getElementById('UpdateButton').style.display = 'none';

    }
    else {
        document.getElementById('BatchMove').style.display = 'none';
        document.getElementById('UpdateButton').style.display = '';

    }

}

function toggleDisplay() {
    // check if any checkbox is activated.
    var toggle = 0;
    for (i = 0; i < document.sampleform.elements.length; i++) {
        var myName = document.sampleform.elements[i].name;
        if (myName.match(/batch/)) {
            if (document.sampleform.elements[i].checked === true) {
                toggle = 1;
                break;
            }
        }
    }
    if (toggle == 1) {
        document.getElementById('BatchMove').style.display = '';
        document.getElementById('UpdateButton').style.display = 'none';

    }
    else {
        document.getElementById('BatchMove').style.display = 'none';
        document.getElementById('UpdateButton').style.display = '';

    }
    CheckNew();

}
function CheckNew() {
    if (document.getElementById('MoveToPidSelect').options[document.getElementById('MoveToPidSelect').selectedIndex].value === 'new') {
        document.getElementById('NewProjectName').style.display = '';
    }
    else {
        document.getElementById('NewProjectName').style.display = 'none';
    }

}


// switch sample page
function SetSampleSubPage() {
    // get selected value in dropdown.
    var sp = $('#sp').find(":selected").val();
    window.location = 'index.php?page=samples&sp=' + sp;

}
