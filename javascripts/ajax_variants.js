// ALL VARIABLES THAT NEED TO BE GLOBAL.
var xmlhttpRQ = null;
var AbortLoad = 0;
var interval_id = null;
var nr_loaded_filters = 0;
var nr_to_load_filters = 0;
var loading_intervalId = null;
var Loading_Rules = {};


///////////////////////
// SET VISITED LINKS //
///////////////////////
function setVisited(linkid) {
    document.getElementById(linkid).style.color = 'blue';
}
/////////////////
// TOGGLE TABS //
/////////////////
var CurrSelected = 'filtertab';
//var cuid = null;
function ToggleTab(toshow) {
    // tab changed 
    if (CurrSelected !== toshow.toLowerCase()) {
        // showing export.
        if (toshow.toLowerCase() == "exporttab") {
            // clear the export results.
            document.getElementById('QueryResults').innerHTML = '<h3>Query Results</h3><p style="padding-left:1em"><input type=button onClick="RunQuery(\'all\');" value="Export Results"></p>';
        }
        // showing charts.
        else if (toshow.toLowerCase() == "charttab") {
            // clear the chart area.
            document.getElementById('QueryResults').innerHTML = '<h3>QC-Plots for Filter Results</h3><p>Specified filters are applied before generating plots.</p><p style="padding-left:1em"><input type=button onClick="GetCharts(\'all\');" value="Generate Charts"></p>';
        }
        // showing annotation/filtering settings
        else if (CurrSelected === "exporttab" || CurrSelected === "charttab" || CurrSelected === "savedtab") {
            // clear the export results.
            if (cuid == null) {
                var url = "ajax_queries/Cookies.php";
                var param = "a=u";
                $.post(url, { a: 'u' }, function (data) {
                    cuid = data;
                });
            }
            document.getElementById('QueryResults').innerHTML = '<h3>Query Results</h3><p style="padding-left:1em"><input type=checkbox id="widetable" name="widetable"> Use wide output table format, putting all annotations in a single line per variant</p><p style="padding-left:1em"><input type=button onClick="RunQuery(\'0\');" value="Run Query"> &nbsp; &nbsp <input type=button onClick="SaveResults(\'' + cuid + '\');" value="Save Results"></p>';

        }
    }
    var childs = document.getElementById('FilterTabs').childNodes;
    for (var i = 0, len = childs.length; i < len; i++) {
        currChild = childs[i];
        if (currChild.nodeType == 1) {
            if (currChild.id.toLowerCase() == toshow.toLowerCase()) {
                currChild.style.display = '';
                document.getElementById(toshow + 'List').className = 'selected';
                CurrSelected = toshow.toLowerCase();
            }
            else {
                currChild.style.display = 'none';
                document.getElementById(currChild.id + 'List').className = 'unselected';
            }
        }
    }
}
// general counters
var listed = 0;
//////////////////////
// MAIN AJAX OBJECT //
//////////////////////
function GetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}

////////////////////////////////////////////////
// function to toggle sample/region selection //
////////////////////////////////////////////////
function SetSampleSelect(userid) {
    var type = document.getElementById("selecttype").options[document.getElementById("selecttype").selectedIndex].value;
    // set cookie
    setCookie("SST", type, 1);
    if (type == 'List') {
        // set loader
        $('#sampleselect').html('<img scr="Images/layout/loader-bar.gif">');
        // settings 
        var url = "ajax_queries/GetSampleSelect.php";
        params = {
            uid: userid,
            setlink: 1,
            pid: '',
            rv: Math.random()
        }
        $.get(url, params, function (data) {
            $('#sampleselect').html(data);
        });

    }
    else if (type == 'Type') {
        // provide the text field for autosuggest
        document.getElementById('sampleselect').innerHTML = "<input type=hidden name=sampleid id=sampleid value=''><input type=text style='width:98%;' value='' name=samplesource id=searchfield>";
        // reload the autosuggest.
        var as_json = new bsn.AutoSuggest('searchfield', options);
    }
    else if (type == 'Region') {
        // provide the text field for gene / region submission
        setCookie('SID', 'region', 1);
        document.getElementById('sampleselect').innerHTML = "<input type=hidden name=sampleid id=sampleid value='region'><input type=text style='width:98%;' value='' title='Provide a gene symbol (GAPDH) or region (chr11:1-500)' name=regionquery id=searchfield onChange='SetRegionCookie()'>";
    }
    else if (type == 'Project') {
        // provide the selection list for projects.
        setCookie('SID', 'project', 1);
        // set loader
        document.getElementById('sampleselect').innerHTML = '<img scr="Images/layout/loader-bar.gif">';
        // get options from ajax 
        var xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        // settings 
        var url = "ajax_queries/GetProjectSelect.php";
        url = url + "?uid=" + userid;
        url = url + "&rv=" + Math.random();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                document.getElementById('sampleselect').innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
    }


}

function UpdateProjectSamples(userid) {
    // get selected value in dropdown 
    var pid = $('#projectid').find(":selected").val();
    // settings 
    var url = "ajax_queries/GetSampleSelect.php";
    var params = {
        uid: userid,
        pid: pid,
        setlink: 1,
        rv: Math.random()
    }
    $.get(url, params, function (data) {
        $('#sampleselect').html(data);
    });
}


/////////////////////////////////
// CHECK AND PROVIDE IGV LINKS //
/////////////////////////////////
// This function also sends a query to populate memcache for the sample + loads the summary log.
function CheckDataFiles(userid, sid) {
    setCookie('SID', sid, 1);
    if (getCookie('SST') == 'Type') {
        setCookie('SN', document.getElementById('searchfield').value, 1);
    }
    else if (getCookie('SST') == 'List') {
        setCookie('SN', document.getElementById('sampleid').options[document.getElementById('sampleid').selectedIndex].text, 1);
    }
    else {
        setCookie('SN', document.getElementById('searchfield').value, 1);
    }
    // send query to populate memcache
    //ar mcurl = "cgi-bin/LoadData.cgi";
    //var param = "t=s";
    //param += "&s=" + sid;
    //var xmlhttp = GetXmlHttpObject();
    //if (xmlhttp == null) {
    //    alert("Browser does not support HTTP Request");
    //    return;
    //}
    //xmlhttp.onreadystatechange = function () {
    //    if (xmlhttp.readyState == 4) {
    //        return;
    //    }
    //};
    //xmlhttp.open("POST", mcurl, true);
    //xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //xmlhttp.send(param);
    // load logs
    var xmlhttpLOG = GetXmlHttpObject();
    if (xmlhttpLOG == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var lurl = "ajax_queries/LoadLog.php";
    var params = {
        uid: userid,
        sid: sid,
        type: 'summary',
        rv: Math.random()
    }

    lurl = lurl + "?uid=" + userid;
    lurl = lurl + "&sid=" + sid;
    lurl = lurl + "&type=summary";
    lurl = lurl + "&rv=" + Math.random();
    document.getElementById('LogTab').innerHTML = "<div class='w100' style='text-align:center'><img src='Images/layout/ajax-loader.gif' style='margin-top:3em'><br/>Loading Summary Log...</div>";
    xmlhttpLOG.onreadystatechange = function () {
        if (xmlhttpLOG.readyState == 4) {
            document.getElementById('LogTab').innerHTML = xmlhttpLOG.responseText;
        }
    };
    xmlhttpLOG.open("GET", lurl, true);
    xmlhttpLOG.send(null);

    // load stored result sets
    var xmlhttpSR = GetXmlHttpObject();
    if (xmlhttpSR == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var rurl = "ajax_queries/LoadStoredResults.php";
    rurl = rurl + "?uid=" + userid;
    rurl = rurl + "&sid=" + sid;
    rurl = rurl + "&rv=" + Math.random();
    document.getElementById('SavedTab').innerHTML = "<div class='w100' style='text-align:center'><img src='Images/layout/ajax-loader.gif' style='margin-top:3em'><br/>Loading List of Saved Results...</div>";
    xmlhttpSR.onreadystatechange = function () {
        if (xmlhttpSR.readyState == 4) {
            document.getElementById('SavedTab').innerHTML = xmlhttpSR.responseText;
        }
    };
    xmlhttpSR.open("GET", rurl, true);
    xmlhttpSR.send(null);


    // check datafiles and provide links. 
    var xmlhttpCDF = GetXmlHttpObject();
    if (xmlhttpCDF == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "ajax_queries/CheckDataFiles.php";
    url = url + "?uid=" + userid;
    url = url + "&sid=" + sid;
    url = url + "&type=check";
    //url=url+"&rv="+Math.random();
    xmlhttpCDF.onreadystatechange = function () {
        if (xmlhttpCDF.readyState == 4) {
            document.getElementById('LoadDataLink').innerHTML = xmlhttpCDF.responseText;
            // if CRAM, check cookies && show overlay.
            checkCRAM(xmlhttpCDF.responseText, userid);
        }
    };
    xmlhttpCDF.open("GET", url, true);
    xmlhttpCDF.send(null);

}
// check if the IGV link is CRAM, then show notification overlay if needed.
function checkCRAM(link, userid) {
    if (link.indexOf('.cram') >= 0) {
        var cram = getCookie('cram');
        if (cram !== "1") {
            var newcontent = "<div class='section' style='font-size:1.2em;border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;margin-top:5%;padding:1em;color:#fff;text-align:left'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em;text-decoration:underline'>IMPORTANT UPDATE : </span>";
            newcontent += '<p style="text-align:left;">VariantDB converted your BAM files to the new, more efficient CRAM format. Apart from a 75% reduction in storage size, this holds some important implications:</p>';
            newcontent += '<p><ol>';
            newcontent += '<li> You need the lastest snapshot build of IGV to load these files. Download it <a style="color:#ececec" href=\'http://software.broadinstitute.org/software/igv/download_snapshot\' target=\'_blank\'> here </a></li>';
            newcontent += '<li> We recommend to download and install the full reference genome locally, this improves performance. See <a style="color:#ececec" href=\'index.php?page=tutorial&topic=igv\'> here</a>, under point 2.</li>';
            newcontent += '<li> CRAM uses lossy compression. We converted your BAM files using the following settings:<ol>';
            newcontent += '  <li>Read names were discarded, but read pair info is maintained within chromosomes</li>';
            newcontent += '  <li>INDEL-base quality scores (BD:BI) from GATK BQSR are discarded</li>';
            newcontent += '  <li>Pre-BQSR base qualities (OQ) are discarded</li>';
            newcontent += '  <li>All other tags are retained</li>';
            newcontent += '  <li>Base qualities are binned following the 8-bin system of Illumina, except mismatches and positions with less than 10X coverage. These are kept at full resolution.</li>';
            newcontent += '</ol></li>'
            newcontent += '<li> We do not recommend to use these CRAM files for anything but visualisation</li></ol>';
            newcontent += '</p>';
            newcontent += '<p style=\'text-align:left\'>Remember my choice. Leave unselected to be reminded periodically. <input type=checkbox name=remember id=remember value=1 /></p>';
            newcontent += '<p><input type=submit value="I Understand" onClick="CRAMagree(\'' + userid + '\')"> &nbsp; <input type=submit value="Close" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
            newcontent += '</div>';
            document.getElementById('overlay').innerHTML = newcontent;
            document.getElementById('overlay').style.display = '';


        }
    }
}

// This function adds available custom annotations to the list.
function LoadCustomAnnotations(type, userid) {

    if (type == 'sid_list') {
        id = document.getElementById('sampleid').options[document.getElementById('sampleid').selectedIndex].value;
        type = 's';
    }
    else if (type == 'sid_type') {
        id = document.getElementById('searchfield').value;
        type = 's';
    }
    else if (type == 'pid') {
        id = document.getElementById('searchfield').options[document.getElementById('searchfield').selectedIndex].value;
        type = 'p';
    }
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings 
    var url = "ajax_queries/LoadCustomAnnotations.php";
    url = url + "?uid=" + userid;
    url = url + "&id=" + id;
    url = url + "&type=" + type;
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            var dom = document.getElementById('AnnotateByCustom_VCF_Fields');
            var newcontent = "<tr ><th colspan=4 class=top style='padding-left:0em;text-align:left;background:#cecece;font-style:italic;'>Custom_VCF_Fields Information &nbsp;</td></tr>";
            newcontent += xmlhttp.responseText
            dom.innerHTML = newcontent
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
    return;
}

function GetDataFiles(userid, sid) {
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings 
    var url = "ajax_queries/CheckDataFiles.php";
    url = url + "?uid=" + userid;
    url = url + "&sid=" + sid;
    url = url + "&type=download";
    //url=url+"&rv="+Math.random();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            //alert(xmlhttpGDF.responseText);
            document.getElementById('IGVlink').innerHTML = '<iframe src="' + xmlhttp.responseText + '"></iframe>';
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

}
// manage favourites (filters/annotations)
function ManageFavorites(item, id, toggle, callback) {
    // send jquery
    $.get('ajax_queries/ManageFavorites.php', { item: item, id: id, toggle: toggle }, callback)

}
////////////////////////////////////////////////
// add a category to the relevant table block //
////////////////////////////////////////////////
function AddCategory(category, negate, param, argum, values, ssqv, callback, parentnode, dyn_opts) {
    // manual change, clear 'loaded filters'
    SetLoaded();
    // get provided values
    var negate = (typeof negate === "undefined") ? "" : negate;
    var param = (typeof param === "undefined") ? "" : param;
    var argum = (typeof argum === "undefined") ? "" : argum;
    var values = (typeof values === "undefined") ? "" : values;
    var ssqv = (typeof ssqv === "undefined") ? "" : ssqv;
    var parentnode = (typeof parentnode === "undefined") ? "" : parentnode;
    FiltersToCookies(listed, parentnode);
    // $listed is now the previously set row.
    // if complete, send to load the data
    if (typeof document.getElementById('sampleid') !== 'undefined' && document.getElementById('sampleid').value != 'project' && document.getElementById('sampleid').value != 'region' && document.getElementById('FilterStep' + listed) != null) {
        var params = "t=f";
        // category
        if (document.getElementById("FilterCategory" + listed) != null) {
            params = params + "&c=" + encodeURIComponent(document.getElementById("FilterCategory" + listed).value);
            // if Dynamic_Filters, add extra fields.
            if (document.getElementById("FilterCategory" + listed).value === 'Dynamic_Filters' && document.getElementById('nr_dyn_op' + listed)) {
                // number of options
                for (j = 1; j <= document.getElementById('nr_dyn_op' + listed).value; j++) {
                    var on = document.getElementById('dyn_op' + listed + '_' + j + '_name').value
                    var ov = '0';
                    // checkbox
                    if (document.getElementById("dyn_op" + listed + '_' + j + "_value").type === 'checkbox') {
                        if (document.getElementById("dyn_op" + listed + '_' + j + "_value").checked == true) {
                            ov = '1';
                        }
                    }
                    // textfield
                    else if (document.getElementById("dyn_op" + listed + '_' + j + "_value").type === 'text') {
                        ov = document.getElementById("dyn_op" + listed + '_' + j + "_value").value;
                    }
                    // multiple select box
                    else if (document.getElementById("dyn_op" + listed + '_' + j + "_value").type === 'select-multiple') {
                        ov = ''; //document.getElementById("dyn_op"+idx+'_'+j+"_value").value;
                        for (var k = 0; k < document.getElementById("dyn_op" + listed + '_' + j + "_value").length; k++) {
                            if (document.getElementById("dyn_op" + listed + '_' + j + "_value").options[k].selected) {
                                ov += document.getElementById("dyn_op" + listed + '_' + j + "_value").options[k].value + '@@@';
                            }
                        }
                    }
                    params = params + "&do" + listed + "[" + on + ']=' + ov;
                }
            }


        }
        // Parameter
        if (document.getElementById("Selection" + listed) != null) {
            params = params + "&p=" + encodeURIComponent(document.getElementById("Selection" + listed).options[document.getElementById("Selection" + listed).selectedIndex].value);
        }
        // Arguments => concat options
        if (document.getElementById("Argument" + listed) != null) {
            params = params + "&a=";
            if (document.getElementById("Argument" + listed).options && document.getElementById("Argument" + listed).options.length > 0) {
                var i;
                for (i = 0; i < document.getElementById("Argument" + listed).options.length; i++) {
                    // concatenate with multiple @@@.
                    if (document.getElementById("Argument" + listed).options[i].selected) {
                        params = params + encodeURIComponent(document.getElementById("Argument" + listed).options[i].value + "@@@");
                    }
                }
            }
            else if (document.getElementById("Argument" + listed).value) {
                params = params + encodeURIComponent(document.getElementById("Argument" + listed).value);
            }
        }
        // Value
        if (document.getElementById("Value" + listed) != null) {
            params = params + "&v=" + encodeURIComponent(document.getElementById("Value" + listed).value);
        }
        // subselect
        if (document.getElementById("ssqv" + listed) != null) {
            params = params + "&ssqv=";
            if (document.getElementById("ssqv" + listed).options && document.getElementById("ssqv" + listed).options.length > 0) {
                var i;
                var ssqv = '';
                for (i = 0; i < document.getElementById("ssqv" + listed).options.length; i++) {
                    // concatenate with multiple @@@.
                    if (document.getElementById("ssqv" + listed).options[i].selected) {
                        ssqv = ssqv + document.getElementById("ssqv" + listed).options[i].value + ",";
                    }
                }
                if (ssqv != '') {
                    ssqv = ssqv.substr(0, (ssqv.length - 1));
                }
                params = params + encodeURIComponent(ssqv);
            }
            else if (document.getElementById("ssqv" + listed).value) {
                params = params + encodeURIComponent(document.getElementById("ssqv" + listed).value);
            }
        }


    }
    // now proceed to add the extra row.
    listed = listed + 1;
    var index = listed;
    Loading_Rules[index] = 0;
    // process dynamic_options passed from ApplyFilter.
    if (typeof dyn_opts !== "undefined") {
        var opts = {};
        for (var d in dyn_opts) {
            opts["dyn" + listed + "_" + d] = dyn_opts[d];
        }
        setDbCookies(opts, 1, true);
    }
    // clear previous cookies on this index.
    var vars = {};
    vars["N" + index] = 1;
    vars["C" + index] = 1;
    vars["P" + index] = 1;
    vars["A" + index] = 1;
    vars["V" + index] = 1;
    vars["s" + index] = 1;
    deleteDbCookies(vars);
    /*	
        deleteDbCookie("N"+index);
        deleteDbCookie("C"+index);
        deleteDbCookie("P"+index);
        deleteDbCookie("A"+index);
        deleteDbCookie("V"+index);
        deleteDbCookie("s"+index);
    */
    // get set sampleid
    var sid = document.getElementById("sampleid").value;
    var region = 'false';
    var project = 'false';
    if (sid == "") {
        alert("You need to set a sample first!");
        return;
    }
    else if (sid == "region") {
        // selected a region. get it!
        var region = document.getElementById("searchfield").value;
        //alert("You selected a region as query, this is under development");
    }
    else if (sid == "project") {
        var project = document.getElementById("searchfield").value;
    }

    // create ajax request	
    //var xmlhttpAC=GetXmlHttpObject();
    //if (xmlhttpAC==null) {
    //	alert ("Browser does not support HTTP Request");
    //	return;
    //}
    // the url
    var url = "ajax_queries/FilterBuilder.php";
    /*
        url=url+"?get=sets";
        url=url+"&cat="+category;
        url=url+"&param="+param;
        url=url+'&index='+index;
        url=url+"&sid="+sid;
        url=url+"&project="+project;
        url=url+"&region="+region;
        url=url+"&rv="+Math.random();
    */
    var params = {
        get: 'sets',
        cat: category,
        param: param,
        index: index,
        sid: sid,
        project: project,
        region: region,
        rv: Math.random()
    };

    // add row to relevant tbody
    var tbody = document.getElementById("FilterBy" + category);
    var row = tbody.insertRow(-1);
    row.id = "FilterStep" + index;
    // get number of cells (from 0th row = header row)
    var cells = document.getElementById("querytable").rows[0].cells.length;
    // add cells to the row.
    var i;
    for (i = 0; i < cells; i++) {
        var newCell = row.insertCell(i);
    }
    //row.cells[0].innerHTML = '<img scr="Images/layout/loader-bar.gif">';
    //xmlhttpAC.open("GET",url,false);
    //xmlhttpAC.send(null);

    // negate option
    if (category === "Dynamic_Filters") {
        row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;margin-bottom:-0.2em'></span><input type=hidden id='FilterCategory" + index + "' value='" + category + "'><select id='negate" + index + "' style='display:none'><option value='' SELECTED>Match</option><option value='NOT'>Not Match</option></select>Match";
    }
    else if (negate === "") {
        row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'></span><input type=hidden id='FilterCategory" + index + "' value='" + category + "'><select id='negate" + index + "'><option value=''>Match</option><option value='NOT'>Not Match</option></select>";
    }
    else {
        row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'></span><input type=hidden id='FilterCategory" + index + "' value='" + category + "'><select id='negate" + index + "'><option value=''>Match</option><option value='NOT' selected>Not Match</option></select>";
    }
    row.cells[1].innerHTML = '<img scr="Images/layout/loader-bar.gif">';

    // settings selection comes in column 2
    $.get(url, params, function (data) {
        row.cells[1].innerHTML = data + '<span id=ss' + index + '></span>';
        if (callback) {
            // run callback function if defined. This is getArguments function or none (pass getValues as its callback).
            callback(category, index, argum, param, values, ssqv, getValues, parentnode);
        }
        else {
            Loading_Rules[index] = 1;
        }

    });

}

///////////////////////////////////////////////////////////////////////////////////
// function to provide the 'Setting' options for the currently selected category //
///////////////////////////////////////////////////////////////////////////////////
function getArguments(category, index, argum, sel, values, ssqv, callback, parentnode) {

    // get set sampleid
    var sid = document.getElementById("sampleid").value;
    if (sid == "") {
        alert("You need to set a sample first!");
        return;
    }
    // manual change, clear 'loaded filters'
    SetLoaded();
    // other values can be set if loading from saved values.
    argum = (typeof argum === "undefined") ? "" : argum;
    values = (typeof values === "undefined") ? "" : values;
    ssqv = (typeof ssqv === "undefined") ? "" : ssqv;
    parentnode = (typeof parentnode === "undefined") ? "" : parentnode;

    // update cookie.
    FiltersToCookies(listed, parentnode);
    // get selected option
    var selection = (typeof sel === "undefined") ? document.getElementById("Selection" + index).options[document.getElementById("Selection" + index).selectedIndex].value : sel;

    // set/create notes field.
    if ($("#note_" + index).length == 0) {
        // novel : create.
        $("#Selection" + index).after("<span id='note_" + index + "' style='color:red'></span>");
    }
    $.get('ajax_queries/CheckFilterDeprecated.php?c=' + category + '&s=' + selection, function (data) {
        $("#note_" + index).html(data.content);
        $("#note_" + index).prop('title', data.title);
    }, 'json');



    // create ajax request
    //var xmlhttpGA=GetXmlHttpObject();
    //if (xmlhttpGA==null) {
    //	alert ("Browser does not support HTTP Request");
    //	return;
    //}
    var row = document.getElementById("FilterStep" + index);
    row.cells[2].innerHTML = '<img scr="Images/layout/loader-bar.gif">';
    // check subselect && append to row.cells[1] if not empty
    var url = "ajax_queries/FilterBuilder.php";
    var params = {
        get: 'subselect',
        cat: category,
        index: index,
        param: selection,
        project: 'false',
        region: 'false',
        args: argum,
        ssqv: ssqv,
        sid: sid,
        rv: Math.random()
    };
    /*
    url=url+"?get=subselect";
    url=url+"&cat="+category;
    url=url+"&index="+index;
    url=url+"&param="+selection;
    url=url+"&project="+'false';
    url=url+"&region="+'false';
    url=url+"&args="+argum;
    url=url+"&ssqv="+ssqv;
    url=url+"&sid="+sid;
    url=url+"&rv="+Math.random();
    */

    //xmlhttpGA.open("GET",url,false);
    //xmlhttpGA.send(null);
    $.get(url, params, function (data) {
        document.getElementById('ss' + index).innerHTML = data;
    });
    //document.getElementById('ss'+index).innerHTML = xmlhttpGA.responseText;	

    // url for the arguments
    url = "ajax_queries/FilterBuilder.php";
    params = {
        get: 'arguments',
        cat: category,
        index: index,
        param: selection,
        args: argum,
        values: values,
        sid: sid,
        rv: Math.random()
    };
    /*
        url=url+"?get=arguments";
        url=url+"&cat="+category;
        url=url+"&index="+index;
        url=url+"&param="+selection;
        url=url+"&args="+argum;
        url=url+"&values="+values;
        url=url+"&sid="+sid;
        url=url+"&rv="+Math.random();
    */
    //var row = document.getElementById("FilterStep"+index);
    row.cells[2].innerHTML = '<img scr="Images/layout/loader-bar.gif">';
    $.get(url, params, function (data) {
        row.cells[2].innerHTML = data;
        if (data == '--') {
            FiltersToCookies(listed);
            Loading_Rules[index] = 1;
            return;
        }

        else if (argum != '') {
            // select the selected items if any
            var argumentlist = argum.split('@@@');
            var arglist = document.getElementById('Argument' + index);
            var nrargs = arglist.length;
            for (i = 0; i < nrargs; i++) {
                for (j = 0; j < argumentlist.length; j++) {
                    if (arglist[i].value == argumentlist[j]) {
                        arglist[i].selected = true;
                        //break;
                    }
                }
            }

        }

        if (callback) {
            // run callback if defined. This is getValues or none.
            callback(category, selection, index, values);
        }
        else {
            Loading_Rules[index] = 1;
        }
    });
    //xmlhttpGA.open("GET",url,false);
    //xmlhttpGA.send(null);
    //row.cells[2].innerHTML = xmlhttpGA.responseText;
    /*
        if (xmlhttpGA.responseText == '--') {
            FiltersToCookies(listed);
            return;
        }
        else if (argum != '') {
            // select the selected items if any
            var argumentlist = argum.split('@@@');
            var arglist = document.getElementById('Argument'+index);
            var nrargs = arglist.length;
            for (i = 0; i <nrargs ; i++) {
                for (j = 0; j<argumentlist.length;j++) {
                    if (arglist[i].value == argumentlist[j]) {
                        arglist[i].selected = true;
                        //break;
                    }
                }
            }
                    	
        }
        if (callback) {
            // run callback if defined. This is getValues or none.
            callback(category,selection,index,values);
        }
    */
}
///////////////////////////////////////////////////////////////////////////////////
// function to provide the 'values' options for the currently selected parameter //
///////////////////////////////////////////////////////////////////////////////////
function getValues(category, parameter, index, values) {
    // set parameter to cookie
    //FiltersToCookies(listed);
    if (document.getElementById('Value' + index) !== null) {
        FiltersToCookies(listed);
        Loading_Rules[index] = 1;
        return;
    }
    // manual change, clear 'loaded filters'
    SetLoaded();
    // values passed on from ApplyFilter
    values = (typeof values === "undefined") ? "" : values;
    // selection : passed on as 'sel' from ApplyFilter, or fetch from document dom
    var selection = (typeof parameter === "undefined") ? document.getElementById("Argument" + index).options[document.getElementById("Argument" + index).selectedIndex].value : parameter;
    // get set sampleid
    var sid = document.getElementById("sampleid").value;
    if (sid == "") {
        alert("You need to set a sample first!");
        return;
    }

    var url = "ajax_queries/FilterBuilder.php";
    var params = {
        get: 'values',
        cat: category,
        index: index,
        param: parameter,
        item: selection,
        values: values,
        sid: sid,
        rv: Math.random()
    };

    // get relevant row 
    var row = document.getElementById("FilterStep" + index);
    row.cells[3].innerHTML = '<img scr="Images/layout/loader-bar.gif">';
    $.get(url, params, function (data) {
        row.cells[3].innerHTML = '';
        if (data !== "") {
            if (row.cells[2].innerHTML != data) {
                row.cells[3].innerHTML = data;
            }
        }
        FiltersToCookies(listed);
        Loading_Rules[index] = 1;

    });
    /*
        xmlhttpGV.open("GET",url,false);
        xmlhttpGV.send(null);
        if (xmlhttpGV.resonseText !== "") {
            // new 2014-04-06: check if not already added to row.cells[2] from cookie based filters
            if (row.cells[2].innerHTML != xmlhttpGV.responseText){
                //alert(row.cells[2].innerHTML+"\n\n"+xmlhttpGV.responseText);
                row.cells[3].innerHTML = xmlhttpGV.responseText; //newcontent;
            }
            else {
                row.cells[3].innerHTML = '';
            }
        } else {
            row.cells[3].innerHTML = '';
        }
        FiltersToCookies(listed);
    */
}

// STORE Annotations to Cookies
function AnnotationsToCookies() {
    var annos = '';
    var i;
    for (var i = 0; i < document.annotationform.annotations.length; i++) {
        if (document.annotationform.annotations[i].checked) {
            var value = document.annotationform.annotations[i].value.replace(/;/g, '||');
            // seperate items by double hash
            annos += "##" + value;
        }
    }
    setDbCookie('SA', annos.substring(2), 1, true);
}
// LOAD Annotations from Cookies
function CookiesToAnnotations() {
    var url = "ajax_queries/Cookies.php";
    var pars = {
        a: 'g',
        uid: cuid,
        name: 'SA'
    }
    $.get(url, pars, function (annostring) {

        //var annostring = getDbCookie('SA');
        // split string to individual items
        var annos = annostring.split("##");
        var i;
        // replace the || to ; 
        for (i = 0; i < annos.length; i++) {
            annos[i] = annos[i].replace(/\|\|/g, ";");
        }
        for (i = 0; i < document.annotationform.annotations.length; i++) {
            if (annos.contains(document.annotationform.annotations[i].value)) {
                document.annotationform.annotations[i].checked = true;
            }
        }
    });
}


// STORE FILTERS TO COOKIES
function FiltersToCookies(listed, parentnode) {
    // collect all cookies to store to DB.
    var vars = {};

    setCookie('listed', listed, 1);
    parentnode = (typeof parentnode === "undefined") ? "j1_1" : parentnode;
    var ref = $("#filter_tree").jstree(true)
    for (var i = 1; i <= listed; i++) {
        var treeid = i
        var treename = ''
        if (document.getElementById("FilterCategory" + i) != null) {
            if (getCookie('skipCookieSave') !== '1') {
                //setDbCookie('C'+i,document.getElementById("FilterCategory"+i).value,1,true)
                vars['C' + i] = document.getElementById("FilterCategory" + i).value;
            }
            treename += ' ' + document.getElementById("FilterCategory" + i).value
            // dynamic filters have extra info.
            if (document.getElementById("FilterCategory" + i).value === 'Dynamic_Filters' && document.getElementById("nr_dyn_op" + i)) {
                // nr of options
                var nrop = document.getElementById("nr_dyn_op" + i).value
                if (getCookie('skipCookieSave') !== '1') {
                    //setDbCookie('dyn'+i+'_nrop',nrop);
                    vars['dyn' + i + '_nrop'] = nrop;
                    vars['nr_do_' + i] = nrop;
                }
                for (j = 1; j <= nrop; j++) {
                    var on = document.getElementById("dyn_op" + i + '_' + j + "_name").value;
                    var ov = '0';
                    // checkbox
                    if (document.getElementById("dyn_op" + i + '_' + j + "_value").type === 'checkbox') {
                        if (document.getElementById("dyn_op" + i + '_' + j + "_value").checked == true) {
                            ov = '1';
                        }
                    }
                    // textfield
                    else if (document.getElementById("dyn_op" + i + '_' + j + "_value").type === 'text') {
                        ov = document.getElementById("dyn_op" + i + '_' + j + "_value").value;
                    }
                    // multiple select box
                    else if (document.getElementById("dyn_op" + i + '_' + j + "_value").type === 'select-multiple') {
                        ov = ''; //document.getElementById("dyn_op"+idx+'_'+j+"_value").value;
                        for (var k = 0; k < document.getElementById("dyn_op" + i + '_' + j + "_value").options.length; k++) {
                            if (document.getElementById("dyn_op" + i + '_' + j + "_value").options[k].selected) {
                                ov += document.getElementById("dyn_op" + i + '_' + j + "_value").options[k].value + '__';
                            }
                        }
                    }
                    if (getCookie('skipCookieSave') !== '1') {
                        //setDbCookie('dyn'+i+'_'+on,ov);
                        vars['dyn' + i + '_' + on] = ov;
                        vars['do_' + i + '_' + j + '_name'] = on;
                        vars['do_' + i + '_' + j + '_value'] = ov;
                    }

                }
            }

        }
        if (document.getElementById("negate" + i) != null) {
            if (getCookie('skipCookieSave') !== '1') {
                //setDbCookie('N'+i,document.getElementById("negate"+i).options[document.getElementById("negate"+i).selectedIndex].value,1,true)
                vars['N' + i] = document.getElementById("negate" + i).options[document.getElementById("negate" + i).selectedIndex].value;
            }
            treename += ": " + document.getElementById("negate" + i).options[document.getElementById("negate" + i).selectedIndex].text + " ";
        }

        if (document.getElementById("Selection" + i) != null) {
            if (getCookie('skipCookieSave') !== '1') {
                //setDbCookie('P'+i,document.getElementById("Selection"+i).options[document.getElementById("Selection"+i).selectedIndex].value,1,true)
                vars['P' + i] = document.getElementById("Selection" + i).options[document.getElementById("Selection" + i).selectedIndex].value;
            }
            treename += document.getElementById("Selection" + i).options[document.getElementById("Selection" + i).selectedIndex].text;
        }
        if (document.getElementById("Argument" + i) != null) {
            var cargs = '';
            var targs = '';
            if (document.getElementById("Argument" + i).options && document.getElementById("Argument" + i).options.length > 0) {
                var j;
                var nri = 0;
                for (j = 0; j < document.getElementById("Argument" + i).options.length; j++) {
                    // concatenate with multiple @@@.
                    if (document.getElementById("Argument" + i).options[j].selected) {
                        nri++;
                        cargs += document.getElementById("Argument" + i).options[j].value + "@@@";
                        if (nri <= 3) {
                            targs += document.getElementById("Argument" + i).options[j].text + ', ';
                        }
                    }
                }
                if (nri > 3) {
                    targs += "and others";
                }
                else {
                    targs = targs.substring(0, targs.length - 2);
                }
                var re = /@@@$/;
                cargs = cargs.replace(re, "");

            }
            else if (document.getElementById("Argument" + i).value) {
                cargs += document.getElementById("Argument" + i).value;
                targs = document.getElementById("Argument" + i).value;
            }
            treename += ': ' + targs;
            if (getCookie('skipCookieSave') !== '1') {
                // setDbCookie('A'+i,cargs,1,true);
                vars['A' + i] = cargs;
            }
        }
        if (document.getElementById("Value" + i) != null) {
            if (getCookie('skipCookieSave') !== '1') {
                // setDbCookie('V'+i,document.getElementById("Value"+i).value,1,true);
                vars['V' + i] = document.getElementById("Value" + i).value;
            }
            if (document.getElementById("Value" + i).options) {
                treename += ': ' + document.getElementById("Value" + i).options[document.getElementById("Value" + i).selectedIndex].text;
            }
            else {
                treename += ': ' + document.getElementById("Value" + i).value;
            }


        }
        if (document.getElementById("ssqv" + i) != null) {
            if (document.getElementById("ssqv" + i).options && document.getElementById("ssqv" + i).options.length > 0) {
                var j;
                var ssqv = '';
                for (j = 0; j < document.getElementById("ssqv" + i).options.length; j++) {
                    // concatenate with multiple @@@.
                    if (document.getElementById("ssqv" + i).options[j].selected) {
                        ssqv += document.getElementById("ssqv" + i).options[j].value + ",";
                    }
                }
                if (ssqv != '') {
                    ssqv = ssqv.substr(0, (ssqv.length - 1));
                }
                if (getCookie('skipCookieSave') !== '1') {
                    // setDbCookie('s'+i,ssqv,1,true); 
                    vars['s' + i] = ssqv;
                }
            }
            else if (document.getElementById("ssqv" + i).value) {
                if (getCookie('skipCookieSave') !== '1') {
                    // setDbCookie('s'+i,document.getElementById('ssqv'+i).value,1,true);
                    vars['s' + i] = document.getElementById('ssqv' + i).value;
                }
            }
        }
        // add to filter_tree
        if (treename != '' && getCookie('skipCookieSave') != 1) {
            treename = treename.replace(/_/g, ' ')
            treename = treename.replace(/;/g, '/')
            // update name if node exists
            if (ref.get_node('rule_' + i)) {
                ref.set_text('rule_' + i, treename);
            }
            // create if not existing.
            else {
                // new rules don't have parent node. set to root.
                if (parentnode == '') {
                    parentnode = 'j1_1';
                }
                var ruleChild = ref.create_node(parentnode, { 'id': 'rule_' + i, 'text': treename, "type": "default" }, 'last');
                ref.open_node(ref.get_parent(ruleChild))
            }
            if (treename.match(/Not Match/)) {
                ref.set_icon('rule_' + i, 'Images/layout/filterNM.png');
            }
            else {
                ref.set_icon('rule_' + i, 'Images/layout/filter.png');
            }
        }
    }
    // update the tree/logic cookies.
    if (getCookie('skipCookieSave') !== '1') {
        // store the cookies in DB.
        var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"));
        var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
        vars['FS'] = treestate;
        vars['FCs'] = treecontents;
        setDbCookies(vars, 1, true);


    }
}

//////////////////////////
// Remove a filter step //
//////////////////////////
function RemoveFilter(index) {
    SetLoaded();
    var row = document.getElementById("FilterStep" + index);
    if (typeof (row) != 'undefined' && row != null) {
        row.parentNode.removeChild(row);
        var vars = {};
        vars['N' + index] = 1;
        vars['C' + index] = 1;
        vars['P' + index] = 1;
        vars['A' + index] = 1;
        vars['s' + index] = 1;
        vars['V' + index] = 1;
        deleteDbCookies(vars);

        var ref = $("#filter_tree").jstree(true)
        //$("#filter_tree").jstree('remove','rule_'+index)
        ref.delete_node('rule_' + index)
        var jstree_content = JSON.stringify($("#filter_tree").jstree("get_json"))
        //setCookie('FilterTree',jstree_content,1)
        //setCookie('TreeSettings',$("#filter_tree").jstree("get_settings"),1)

    }

}

function RemoveNode(node_id, nested) {
    nested = typeof nested !== 'undefined' ? nested : 0;
    var node_id = (typeof node_id === "undefined") ? "" : node_id;
    var ref = $('#filter_tree').jstree(true)
    if (node_id != '') {
        ref.deselect_all();
        ref.select_node(node_id);
    }
    var sel = ref.get_selected();
    if (!sel.length) {
        document.getElementById('LogicComments').innerHTML = 'Select a node prior to deleting';
        return;
    }
    sel = sel[0];
    // protect the root node.
    if (sel === 'j1_1') {
        document.getElementById('LogicComments').innerHTML = '&nbsp;';
        document.getElementById('LogicComments').innerHTML = 'Root node cannot be deleted';
        return;
    }
    if (nested == 0) {
        setCookie('skipCookieSave', '1', 1);
    }
    // delete a rule by Calling the RemoveFilter function.	
    if (ref.get_node(sel).type.substring(0, 5) != 'logic') {
        // rule, extract index.
        var index = sel.substring(5)
        RemoveFilter(index)
    }
    // delete logic nodes  
    else {
        var children = ref.get_children_dom(sel)
        if (children.length == 0) {
            ref.delete_node(sel)
        } else {
            for (var i = 0; i < children.length; i++) {
                RemoveNode(children[i].id, 1);
            }
            ref.delete_node(sel);
        }
    }
    if (nested == 0) {
        //FiltersToCookies(listed);
        deleteCookie('skipCookieSave');
        FiltersToCookies(listed);
    }

}
function RecurseTree(node) {
    var ref = $('#filter_tree').jstree(true)
    var ntype = ref.get_node(node).type
    var children = ref.get_children_dom(node)
    if (children.length == 0) {
        return;
    }
    for (idx = 0; idx < children.length; idx++) {
        var child = children[idx].id;
        RecurseTree(child)
    }
}



function GetQuerySettings(uid) {
    // get set sampleid
    var sid = document.getElementById("sampleid").value;
    var region = 'false';
    var project = 'false';
    if (sid == "") {
        alert("You need to set a sample first!");
        return;
    }
    else if (sid == "region") {
        // selected a region. get it!
        region = document.getElementById("searchfield").value;
        if (region == "") {
            alert("Invalid region provided!")
            return;

        }
        //alert("You selected a region as query, this is under development");
    }
    else if (sid == "project") {
        project = document.getElementById("searchfield").value;
    }
    // kill file & active build
    var killfile = "/tmp/vdb.kill." + Math.random() + ".txt";
    var build = document.getElementById('ActiveBuild').innerHTML;
    // the base parameters:
    var params = {
        'build': build,
        'sid': sid,
        'kill': killfile,
        'session_path': session_save_path,
        'uid': uid,
        'region': region,
        'project': project,
        'listed': listed
    };
    // get checkbox value for wide output format.
    //var wt = '';
    if (document.getElementById('widetable') && document.getElementById('widetable').checked) {
        //wt = '&widetable=1';
        params['widetable'] = 1;
        //wt = '&widetable=1';
    }
    // FILTERS 
    var idx;
    for (idx = 1; idx <= listed; idx++) {
        // skip removed items
        if (document.getElementById("FilterStep" + idx) === null) {
            continue;
        }
        // category
        if (document.getElementById("FilterCategory" + idx) != null) {
            params["category" + idx] = document.getElementById("FilterCategory" + idx).value;
            // if Dynamic_Filters, add extra fields.
            if (document.getElementById("FilterCategory" + idx).value === 'Dynamic_Filters' && document.getElementById('nr_dyn_op' + idx)) {
                // number of options
                //params['nr_dyn_op' + idx] = document.getElementById('nr_dyn_op' + idx).value;
                params['nr_do_' + idx] = document.getElementById('nr_dyn_op' + idx).value;
                for (j = 1; j <= document.getElementById('nr_dyn_op' + idx).value; j++) {
                    var on = document.getElementById('dyn_op' + idx + '_' + j + '_name').value
                    var ov = '0';
                    // checkbox
                    if (document.getElementById("dyn_op" + idx + '_' + j + "_value").type === 'checkbox') {
                        if (document.getElementById("dyn_op" + idx + '_' + j + "_value").checked == true) {
                            ov = '1';
                        }
                    }
                    // textfield
                    else if (document.getElementById("dyn_op" + idx + '_' + j + "_value").type === 'text') {
                        ov = document.getElementById("dyn_op" + idx + '_' + j + "_value").value;
                    }
                    // (multi)-select
                    else if (document.getElementById("dyn_op" + idx + '_' + j + "_value").type === 'select-multiple') {
                        ov = '';
                        for (var i = 0; i < document.getElementById("dyn_op" + idx + '_' + j + "_value").length; i++) {
                            if (document.getElementById("dyn_op" + idx + '_' + j + "_value").options[i].selected) {
                                // separated by double underscore __ 
                                ov += document.getElementById("dyn_op" + idx + '_' + j + "_value").options[i].value + '__';
                            }
                        }
                    }
                    params["do" + idx + "_" + j + '_name'] = on;
                    params["do" + idx + "_" + j + '_value'] = ov;

                }
            }

        }
        // negate
        if (document.getElementById("negate" + idx) != null) {
            params["negate" + idx] = document.getElementById("negate" + idx).options[document.getElementById("negate" + idx).selectedIndex].value
        }
        // Parameter
        if (document.getElementById("Selection" + idx) != null) {
            params["param" + idx] = document.getElementById("Selection" + idx).options[document.getElementById("Selection" + idx).selectedIndex].value;
        }
        // Arguments => concat options
        if (document.getElementById("Argument" + idx) != null) {

            if (document.getElementById("Argument" + idx).options && document.getElementById("Argument" + idx).options.length > 0) {
                var i;
                params["argument" + idx] = '';
                for (i = 0; i < document.getElementById("Argument" + idx).options.length; i++) {
                    // concatenate with multiple @@@.
                    if (document.getElementById("Argument" + idx).options[i].selected) {
                        //param = param + encodeURIComponent(document.getElementById("Argument" + idx).options[i].value + "@@@");
                        params["argument" + idx] += document.getElementById("Argument" + idx).options[i].value + "__";
                    }
                }
            }
            else if (document.getElementById("Argument" + idx).value) {
                params["argument" + idx] = document.getElementById("Argument" + idx).value;
            }
        }
        // Value
        if (document.getElementById("Value" + idx) != null) {
            params["value" + idx] = document.getElementById("Value" + idx).value;
        }
        // subselect
        if (document.getElementById("ssqv" + idx) != null) {
            if (document.getElementById("ssqv" + idx).options && document.getElementById("ssqv" + idx).options.length > 0) {
                var i;
                var ssqv = '';
                for (i = 0; i < document.getElementById("ssqv" + idx).options.length; i++) {
                    if (document.getElementById("ssqv" + idx).options[i].selected) {
                        ssqv = ssqv + document.getElementById("ssqv" + idx).options[i].value + ",";
                    }
                }
                if (ssqv != '') {
                    ssqv = ssqv.substr(0, (ssqv.length - 1));
                }
                params["ssqv" + idx] = ssqv;
            }
            else if (document.getElementById("ssqv" + idx).value) {
                params["ssqv" + idx] = document.getElementById("ssqv" + idx).value;
            }
        }
    }
    // ANNOTATIONS
    var nranno = 0;
    for (var i = 0; i < document.annotationform.annotations.length; i++) {
        if (document.annotationform.annotations[i].checked) {
            nranno++;
            //param = param + "&anno" + nranno + "=" + document.annotationform.annotations[i].value;
            params["anno" + nranno] = document.annotationform.annotations[i].value;
        }
    }
    //param = param + "&nranno=" + nranno;
    params["nranno"] = nranno;
    // the query structure from jstree.
    var ref = $("#filter_tree").jstree(true)
    var js = ref.get_json('#', { 'no_state': true, 'flat': false, 'no_data': true });
    logic = JSON.stringify(ref.get_json('#', { 'no_state': true, 'flat': false, 'no_data': true }))
    //param = param + "&logic=" + logic
    params['logic'] = logic;

    // return
    return params;
}


//////////////////////////////
// COMPOSE THE ACTUAL QUERY //
//////////////////////////////
function RunQuery(slicestart, offset = 0) {
    FiltersToCookies(listed);
    // get settings for the filters to run.
    var params = GetQuerySettings(cuid);
    if (params === undefined) {
        // invalid entries found. 
        return;
    }
    // url
    var url = "cgi-bin/Query_Wrapper.py";
    // slice start == ALL for export queries : add other arguments.
    var slicestart = (typeof slicestart === "undefined") ? "" : slicestart;
    params["slicestart"] = slicestart;
    if ($('#vf_ackn').prop('checked')) {
        // console.log("vf_ackn is checked. add to params");
        params["vf_ackn"] = 1;
    }
    if (slicestart == 'all') {
        //param = param + "&format=" + document.getElementById('seperator').options[document.getElementById('seperator').selectedIndex].value;
        params["format"] = document.getElementById('seperator').options[document.getElementById('seperator').selectedIndex].value;
        //param = param + "&quote=" + document.getElementById('quote').options[document.getElementById('quote').selectedIndex].value;
        params['quote'] = document.getElementById('quote').options[document.getElementById('quote').selectedIndex].value;
        //param = param + "&outstyle=" + document.getElementById('returnAs').options[document.getElementById('returnAs').selectedIndex].value;
        params['outstyle'] = document.getElementById('returnAs').options[document.getElementById('returnAs').selectedIndex].value;
        //param = param + "&collapse=" + document.getElementById('collapse').options[document.getElementById('collapse').selectedIndex].value;
        params['collapse'] = document.getElementById('collapse').options[document.getElementById('collapse').selectedIndex].value;
    }

    // set the results to loading.
    $("#QueryResults").html("<div class='w100'><h3>Query is running </h3><p><input type=button class=button onClick=\"KillQuery('" + params['kill'] + "')\" value='Stop Query'></p></div>")
    // create the ajax call into global var.
    $.post(url, params, function (data) {
        var qid = data.queryid;
        interval_id = setInterval(function () { Check_Query_Status(cuid, qid, interval_id, params['wt'], slicestart, params['kill'], offset) }, 1000);

    }, "json")
        .fail(function (data) {
            $("#QueryResults").html("<div class='w100'><h3>Query Failed</h3><p>Could not launch query, please report. Returned error was : </p><p>" + JSON.stringify(data).replaceAll('\\n', '<br/>') + "</p></div>");

        });

}
////////////////////////////
// MONITOR QUERY PROGRESS //
////////////////////////////
function Check_Query_Status(uid, qid, interval_id, wt, slicestart, killfile, offset = 0) {
    // url
    var url = "ajax_queries/Check_Query_Status.php";
    // parameters
    var pars = { uid: uid, qid: qid, rv: Math.random() }
    // send request.
    $.get(url, pars, function (data) {
        // process result.
        switch (data.status.toLowerCase()) {
            case 'finished':
                clearInterval(interval_id);
                Load_Query_Results(uid, qid, wt, slicestart);
                $(window).scrollTop(offset);
                break;
            case 'error':
                clearInterval(interval_id);
                Load_Query_Error(uid, qid);
                break;
            default:
                Load_Query_Log(uid, qid, killfile);
                break
        }
    }, 'json')

}
function Load_Query_Log(uid, qid, killfile) {
    var url = "ajax_queries/Load_Query_Log.php";
    var pars = { uid: uid, qid: qid, kf: killfile, rv: Math.random() }
    $.get(url, pars, function (data) {
        $('#QueryResults').html(data);
    })

}

function Load_Query_Error(uid, qid) {

    var url = "ajax_queries/Load_Query_Error.php";
    pars = { uid: uid, qid: qid, rv: Math.random() }
    $.get(url, pars, function (data) {
        $("#QueryResults").html(data)
    })

}

/////////////////////////////////////
// LOAD QUERY RESULTS INTO BROWSER //
/////////////////////////////////////
function Load_Query_Results(uid, qid, wt, slicestart) {
    var url = "ajax_queries/Load_Query_Results.php";
    pars = { uid: uid, qid: qid, rv: Math.random(), slicestart: slicestart }
    if (slicestart == 'all') {
        pars['format'] = $('#returnAs').val()
    }
    $.get(url, pars, function (data) {
        $("#QueryResults").html(data);
        // wide table?
        if (typeof wt !== 'undefined' && wt != '') {
            myHeader = $('#fixed_header');
            myHeader.data('position', myHeader.position());
            // some reformatting.
            $(function () {
                var $tableBodyCell = $('#WideVariantTable tbody tr:first td');
                var $headerCell = $('#WideVariantTable thead tr th');
                $tableBodyCell.each(function (index) {
                    $(this).width($headerCell.eq(index).width());
                });

            });
        }
        // results were saved 
        if (slicestart.substr(0, 4) == 'save') {
            var sid = $("#sampleid").val();

            var rurl = "ajax_queries/LoadStoredResults.php";
            var rpars = { uid: uid, sid: sid, rv: Math.random() }
            $("#SavedTab").html("<div class='w100' style='text-align:center'><img src='Images/layout/ajax-loader.gif' style='margin-top:3em'><br/>Loading List of Saved Results...</div>")
            $.get(rurl, rpars, function (rdata) {
                $("#SavedTab").html(rdata)
            })

        }
        // get the random value for charting
        if (document.getElementById('randID')) {
            // add the charts to create.
            var inpfields = document.getElementsByName('chart[]');
            var nr_inpfields = inpfields.length;
            var chartlist = new Array();
            for (var i = 0; i < nr_inpfields; i++) {
                if (inpfields[i].type == 'checkbox' && inpfields[i].checked == true) {
                    chartlist[chartlist.length] = inpfields[i].value;
                }
            }
            var rand = $("#randID").val();
            for (var i = 0; i < chartlist.length; i++) {
                if (typeof (window["Load" + chartlist[i]]) == "function") {
                    window["Load" + chartlist[i]](rand);
                } else {
                    console.log(chartlist[i] + " is not a function");
                }
            }
        }
    })

}

/////////////////////
// Generate Charts //
/////////////////////
function GetCharts(slicestart, uid) {
    // get set sampleid
    var sid = document.getElementById("sampleid").value;
    var region = 'false';
    if (sid == "") {
        alert("You need to set a sample first!");
        return;
    }
    else if (sid == "region") {
        // selected a region. get it!
        var region = document.getElementById("searchfield").value;
        //alert("You selected a region as query, this is under development");
    }
    var slicestart = (typeof slicestart === "undefined") ? "" : slicestart;
    var killfile = "/tmp/vdb.kill." + Math.random() + ".txt";
    // compose the url...
    //var url="cgi-bin/RunFilterQuery.cgi";
    var url = "cgi-bin/Query_Wrapper.cgi";
    // set the variables
    param = "sid=" + sid + "&kill=" + killfile + "&uid=" + cuid + "&session_path=" + session_save_path;
    param = param + "&chart=1";
    param = param + "&region=" + region;
    param = param + "&listed=" + listed;
    var idx;
    for (idx = 1; idx <= listed; idx++) {
        // skip removed items
        if (document.getElementById("FilterStep" + idx) === null) {
            continue;
        }
        // category
        if (document.getElementById("FilterCategory" + idx) != null) {
            param = param + "&category" + idx + "=" + encodeURIComponent(document.getElementById("FilterCategory" + idx).value);
        }
        // negate
        if (document.getElementById("negate" + idx) != null) {
            param = param + "&negate" + idx + "=" + encodeURIComponent(document.getElementById("negate" + idx).options[document.getElementById("negate" + idx).selectedIndex].value);
        }
        // Parameter
        if (document.getElementById("Selection" + idx) != null) {
            param = param + "&param" + idx + "=" + encodeURIComponent(document.getElementById("Selection" + idx).options[document.getElementById("Selection" + idx).selectedIndex].value);
        }
        // Arguments => concat options
        if (document.getElementById("Argument" + idx) != null) {
            param = param + "&argument" + idx + "=";
            if (document.getElementById("Argument" + idx).options && document.getElementById("Argument" + idx).options.length > 0) {
                var i;
                for (i = 0; i < document.getElementById("Argument" + idx).options.length; i++) {
                    // concatenate with multiple @@@.
                    if (document.getElementById("Argument" + idx).options[i].selected) {
                        param = param + encodeURIComponent(document.getElementById("Argument" + idx).options[i].value + "@@@");
                    }
                }
            }
            else if (document.getElementById("Argument" + idx).value) {
                param = param + encodeURIComponent(document.getElementById("Argument" + idx).value);
            }
        }
        // Value
        if (document.getElementById("Value" + idx) != null) {
            param = param + "&value" + idx + "=" + encodeURIComponent(document.getElementById("Value" + idx).value);
        }
    }
    // get the selected annotations
    var nranno = 0;
    for (var i = 0; i < document.annotationform.annotations.length; i++) {
        if (document.annotationform.annotations[i].checked) {
            nranno++;
            param = param + "&anno" + nranno + "=" + document.annotationform.annotations[i].value;
        }
    }
    param = param + "&nranno=" + nranno + "&session_path=" + session_save_path;;
    // set the slice start
    param = param + "&slicestart=" + slicestart;
    // add the charts to create.
    var inpfields = document.getElementsByName('chart[]');
    var nr_inpfields = inpfields.length;
    var charts = '';
    for (var i = 0; i < nr_inpfields; i++) {
        if (inpfields[i].type == 'checkbox' && inpfields[i].checked == true) {
            charts += inpfields[i].value + ',';
        }
    }
    param = param + "&charts=" + charts;
    // the query structure from jstree.
    var ref = $("#filter_tree").jstree(true)
    logic = JSON.stringify(ref.get_json('#', { 'no_state': true, 'flat': false, 'no_data': true }))
    param = param + "&logic=" + logic
    // create the ajax call
    xmlhttpRQ = GetXmlHttpObject();
    if (xmlhttpRQ == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttpRQ.onreadystatechange = function () {
        if (xmlhttpRQ.readyState == 4) {
            var qid = xmlhttpRQ.responseText;
            interval_id = setInterval(function () { Check_Query_Status(cuid, qid, interval_id, 0, slicestart, killfile) }, 1000);
            document.getElementById('QueryResults').innerHTML = "<div class='w100'><h3>Query is running </h3><p><input type=button class=button onClick=\"KillQuery('" + killfile + "')\" value='Stop Query'></p></div>";

        }
    }
    // SEND REQUEST WITH POST
    xmlhttpRQ.open("POST", url, true);
    xmlhttpRQ.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttpRQ.send(param);
}


///////////////////////////
// SAVE A SET OF FILTERS //
///////////////////////////
function SaveFilter(uid) {
    var Filtertable = document.getElementById('querytable');
    var param = 'StoreWhat=Filter&uid=' + uid;
    var nrr = listed;
    //for (var i = 0, nrr; row = Filtertable.rows[i]; i++) {
    for (var i = 0; i <= nrr; i++) {
        // skip removed items
        if (document.getElementById("FilterStep" + i) === null) {
            continue;
        }
        // category
        if (document.getElementById("FilterCategory" + i) != null) {
            param = param + "&category" + i + "=" + encodeURIComponent(document.getElementById("FilterCategory" + i).value);
            // dynamic filter?
            if (document.getElementById("FilterCategory" + i).value === 'Dynamic_Filters' && document.getElementById('nr_dyn_op' + i)) {
                // number of options
                param = param + '&nr_do_' + i + '=' + document.getElementById('nr_dyn_op' + i).value;
                for (j = 1; j <= document.getElementById('nr_dyn_op' + i).value; j++) {
                    var on = document.getElementById('dyn_op' + i + '_' + j + '_name').value;
                    var ov = '0';
                    // checkbox
                    if (document.getElementById("dyn_op" + i + '_' + j + "_value").type === 'checkbox') {
                        if (document.getElementById("dyn_op" + i + '_' + j + "_value").checked == true) {
                            ov = '1';
                        }
                    }
                    // textfield
                    else if (document.getElementById("dyn_op" + i + '_' + j + "_value").type === 'text') {
                        ov = document.getElementById("dyn_op" + i + '_' + j + "_value").value;
                    }
                    // multiple select box
                    else if (document.getElementById("dyn_op" + i + '_' + j + "_value").type === 'select-multiple') {
                        ov = ''; //document.getElementById("dyn_op"+idx+'_'+j+"_value").value;
                        for (var k = 0; k < document.getElementById("dyn_op" + i + '_' + j + "_value").length; k++) {
                            if (document.getElementById("dyn_op" + i + '_' + j + "_value").options[k].selected) {
                                ov += document.getElementById("dyn_op" + i + '_' + j + "_value").options[k].value + '__';
                            }
                        }
                    }
                    param = param + '&do' + i + '_' + j + '_name=' + on;
                    param = param + "&do" + i + "_" + j + '_value=' + ov;
                }
            }
        }
        // negate
        if (document.getElementById("negate" + i) != null) {
            param = param + "&negate" + i + "=" + encodeURIComponent(document.getElementById("negate" + i).options[document.getElementById("negate" + i).selectedIndex].value);
        }
        // Parameter
        if (document.getElementById("Selection" + i) != null) {
            param = param + "&param" + i + "=" + encodeURIComponent(document.getElementById("Selection" + i).options[document.getElementById("Selection" + i).selectedIndex].value);
        }
        // Arguments => concat options
        if (document.getElementById("Argument" + i) != null) {
            param = param + "&argument" + i + "=";
            if (document.getElementById("Argument" + i).options && document.getElementById("Argument" + i).options.length > 0) {
                var j;
                for (j = 0; j < document.getElementById("Argument" + i).options.length; j++) {
                    // concatenate with two underscores: __.
                    if (document.getElementById("Argument" + i).options[j].selected) {
                        param = param + encodeURIComponent(document.getElementById("Argument" + i).options[j].value + "__");
                    }
                }
            }
            else if (document.getElementById("Argument" + i).value) {
                param = param + encodeURIComponent(document.getElementById("Argument" + i).value);
            }
        }
        // Value
        if (document.getElementById("Value" + i) != null) {
            param = param + "&value" + i + "=" + encodeURIComponent(document.getElementById("Value" + i).value);
        }
        // subselect
        if (document.getElementById("ssqv" + i) != null) {
            param = param + "&ssqv" + i + "=";
            if (document.getElementById("ssqv" + i).options && document.getElementById("ssqv" + i).options.length > 0) {
                var j;
                for (j = 0; j < document.getElementById("ssqv" + i).options.length; j++) {
                    // concatenate with two underscores: __.
                    if (document.getElementById("ssqv" + i).options[j].selected) {
                        param = param + encodeURIComponent(document.getElementById("ssqv" + i).options[j].value + "__");
                    }
                }
            }
            else if (document.getElementById("ssqv" + i).value) {
                param = param + encodeURIComponent(document.getElementById("ssqv" + i).value);
            }
        }

    }
    // final update of cookie to reflect all changes
    var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"));
    var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
    setDbCookie('FCs', treecontents, 1, true)
    setDbCookie('FS', treestate, 1, true)
    // get content back from cookie (redundant)
    //var jstree_content = getCookie('FilterContents');
    param = param + "&FilterLogic=" + encodeURIComponent(treecontents);
    // compose overlay content
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Save Filter Settings</span>";
    newcontent += '<p class=white>Provide a name to save the settings under:<br/>';
    newcontent += '<input  type=hidden name="params" id=SaveParams value="' + param + '"><input type=text id=SaveAs name=SaveAs size=30 style="margin-left:1em;">';
    newcontent += '</p>';
    newcontent += '<p class=white>Additional Comments:<br/>';
    newcontent += '<input type=text id=SaveComments name=SaveComments size=30 style="margin-left:1em;">';
    newcontent += '</p>';
    newcontent += '<p><input type=submit value="Save Settings" onClick="StoreFilter(\'' + uid + '\',getFilters)"> &nbsp; <input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
    newcontent += '</div>';
    document.getElementById('overlay').innerHTML = newcontent;
    document.getElementById('overlay').style.display = '';

}

///////////////////////////////
// GET LIST OF SAVED FILTERS //
///////////////////////////////
function getFilters(uid) {
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // url
    var url = "ajax_queries/getFilters.php";
    url = url + "?uid=" + uid;
    url = url + "&rv=" + Math.random();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            document.getElementById('StoredFilters').innerHTML = xmlhttp.responseText;
        }

    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

}
///////////////////////////////////
// GET LIST OF SAVED Annotations //
///////////////////////////////////
function getAnnotations(uid) {
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // url
    var url = "ajax_queries/getAnnotations.php";
    url = url + "?uid=" + uid;
    url = url + "&rv=" + Math.random();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            document.getElementById('StoredAnnotations').innerHTML = xmlhttp.responseText;
        }

    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
}

function SetOverlay(display, message) {
    if (typeof message !== 'undefined') {
        document.getElementById('overlay').innerHTML = message;
    }
    document.getElementById('overlay').style.display = display;
}
////////////////////////////////////////////////////
// LOAD SELECTED FILTER SETTINGS AND ADD TO TABLE //
////////////////////////////////////////////////////
function WrapApplyFilter() {
    AbortLoad = 0;
    Loading_Rules = {};
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Applying Filter Settings</span><p>Just a second...</p>";
    //newcontent += "<p><input type=submit value='Cancel' onClick='AbortLoad=1' /></p>";
    newcontent += "<p><input type=submit value='Cancel' onClick='Abort_Load(1)' /></p></div>";
    SetOverlay('', newcontent);
    //setTimeout(function() {
    ApplyFilterNew();
    //	SetOverlay('none');
    //},0);
}

function ApplyFilterNew() {
    // get set sampleid
    var sid = document.getElementById("sampleid").value;
    if (sid == "") {
        SetOverlay('none');
        alert("You need to set a sample first!");
        WaitForApplying(' ', '');
        return;
    }

    var fids = '';
    if (document.Storedfilters.filters.length) {
        nrb = document.Storedfilters.filters.length;
        for (var i = 0; i < nrb; i++) {
            if (document.Storedfilters.filters[i].checked) {
                var fid = document.Storedfilters.filters[i].value;
                fids = fids + fid + ",";
            }
        }
    }
    else if (document.Storedfilters.filters.checked) {
        fids = document.Storedfilters.filters.value + ","
    }
    // no filters selected
    if (fids == '') {
        SetOverlay('none');
        WaitForApplying(' ', '');
        return;
    }
    fids = fids.slice(0, -1);
    // set loaded filters string.
    var content = GetLoaded();
    // skip storing cookies while processing
    setCookie('skipCookieSave', '1', 1);

    // initialise tree
    var ref = $('#filter_tree').jstree(true)
    sel = 'j1_1'
    var children = ref.get_children_dom(sel)
    // move all root elements to new child, if root is not logic_AND.
    if (children.length > 0) {
        // add the child node.
        if (ref.get_node(sel).type == 'logic_AND') {
            // add new data to same root.
        } else {
            var oldtype = ref.get_node(sel).type;
            var oldtext = ref.get_node(sel).text;
            var newchild = ref.create_node(sel, { "type": oldtype });
            ref.open_node(ref.get_parent(newchild))
            if (newchild) {
                ref.set_text(newchild, oldtext);
            }
            // set root to AND
            ref.set_type(sel, 'logic_AND');
            ref.set_text(sel, "AND (keep if complying with all rules)");
        }
        // move all original children to new child.
        for (idx = 0; idx < children.length; idx++) {
            var child = children[idx].id;
            ref.move_node(child, newchild, 'last')
        }
    }

    var nrb = 0;
    var Loaded_Filters = '';
    nr_loaded_filters = 0;
    nr_to_load_filters = 0;
    // get all needed data.
    var url = "ajax_queries/GetFullFilters.php";
    $.get(url, { fids: fids, uid: cuid, listed: listed, sid: sid }, function (data) {
        // add all rules. 
        $.each(data.rules, function (index, rule) {
            var c = rule.c;
            var v = rule.v;
            var tbody = document.getElementById("FilterBy" + c);
            var row = tbody.insertRow(-1);
            row.id = "FilterStep" + index;
            // get number of cells (from 0th row = header row)
            var cells = document.getElementById("querytable").rows[0].cells.length;
            // add cells to the row.
            var i;
            for (i = 0; i < cells; i++) {
                var newCell = row.insertCell(i);
            }
            // negate cell
            if (c === 'Dynamic_Filters') {
                row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;margin-bottom:-0.2em'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "' style='display:none'><option value='' SELECTED>Match</option><option value='NOT'>Not Match</option></select>Match";
                row.cells[1].innerHTML = v[1];
                row.cells[2].innerHTML = v[2];
                row.cells[3].innerHTML = v[3];
            }
            else {
                if (v[0] === "") {
                    row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "'><option value=''>Match</option><option value='NOT'>Not Match</option></select>";
                }
                else {
                    row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "'><option value=''>Match</option><option value='NOT' selected>Not Match</option></select>";
                }
                // level 1
                row.cells[1].innerHTML = v[1].result + '<span id=ss' + index + '></span>';
                // level 2
                row.cells[2].innerHTML = v[2].result;
                // level 3
                if (v[2].result !== v[3].result) {
                    row.cells[3].innerHTML = v[3].result;
                }
            }

        });
        // add trees per loaded filter.
        var fid_list = fids.split(",");
        var i;
        for (i = 0; i < fid_list.length; i++) {
            var l_tree = JSON.parse(data.jstrees[fid_list[i]])[0];
            var roottype = l_tree.type
            var newbranch = AddChild(roottype, 'j1_1');
            var RuleIDs = ParseLogicNew(l_tree, newbranch);
            Loaded_Filters = Loaded_Filters + ' : ' + document.getElementById('f_name_' + fid_list[i]).innerHTML;

        }
        // expand the tree.
        ref.open_all();
        // set listed
        listed = data.listed;
        // update cookies.
        deleteCookie('skipCookieSave');

        if (typeof content !== null && content == '') {
            content = "Loaded Filters " + Loaded_Filters;
        }
        else {
            content = content + Loaded_Filters;
        }
        document.getElementById('Loaded_Filter').innerHTML = content;

        //deleteCookie('skipCookieSave');
        FiltersToCookies(listed);
        SetOverlay('none');
        //WaitForApplying(content,Loaded_Filters);
        //return;

    }, "json");


}
function GetLoaded() {
    var content = document.getElementById('Loaded_Filter').innerHTML;
    return (content);
}
function SetLoaded(content) {
    content = (typeof content !== 'undefined') ? content : '';
    document.getElementById('Loaded_Filter').innerHTML = content;
    return;
}

function ApplyFilter() {

    // get set sampleid
    var sid = document.getElementById("sampleid").value;
    if (sid == "") {
        SetOverlay('none');
        alert("You need to set a sample first!");
        WaitForApplying(' ', '');
        return;
    }
    // set loaded filters string.
    var content = GetLoaded();

    // skip storing cookies while processing
    setCookie('skipCookieSave', '1', 1);

    // get xmlhttp object
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp === null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var initiated = 0;
    var nrb = 0;
    var Loaded_Filters = '';
    nr_loaded_filters = 0;
    nr_to_load_filters = 0;
    if (document.Storedfilters.filters.length) {
        nrb = document.Storedfilters.filters.length;
        for (var i = 0; i < nrb; i++) {

            if (document.Storedfilters.filters[i].checked) {
                // initiate if needed by adding a filter level.
                if (initiated == 0) {
                    var ref = $('#filter_tree').jstree(true)
                    sel = 'j1_1'
                    var children = ref.get_children_dom(sel)
                    // move all root elements to new child, if root is logic_OR.
                    if (children.length > 0) {
                        // add the child node.
                        if (ref.get_node(sel).type == 'logic_AND') {
                            //var newchild = ref.create_node(sel, {"type":"logic_AND"});
                            //ref.open_node(ref.get_parent(newchild))
                            //if(newchild) {
                            //	ref.set_text(newchild,'AND');
                            //}
                        } else {
                            var oldtype = ref.get_node(sel).type;
                            var oldtext = ref.get_node(sel).text;
                            var newchild = ref.create_node(sel, { "type": oldtype });
                            ref.open_node(ref.get_parent(newchild))
                            if (newchild) {
                                ref.set_text(newchild, oldtext);
                            }
                            // set root to AND
                            ref.set_type(sel, 'logic_AND');
                            ref.set_text(sel, "AND (keep if complying with all rules)");
                        }
                        // move all original children to new child.
                        for (idx = 0; idx < children.length; idx++) {
                            var child = children[idx].id;
                            ref.move_node(child, newchild, 'last')
                        }
                        initiated = 1
                    }

                }
                var fid = document.Storedfilters.filters[i].value;
                Loaded_Filters = Loaded_Filters + ' : ' + document.getElementById('f_name_' + fid).innerHTML;
                var url = "ajax_queries/GetFilterStringTree.php";
                //url += '?fid='+fid;
                nr_to_load_filters = nr_to_load_filters + 1;
                $.get(url, { fid: fid }, function (data) {
                    // load.
                    processLoadedFilter(data.rules, data.filtertree);
                    // mark finished.
                }, "json");

            }

        }

    }
    else if (document.Storedfilters.filters.checked) {
        var ref = $('#filter_tree').jstree(true)
        sel = 'j1_1'
        var children = ref.get_children_dom(sel)
        if (children.length > 0) {
            // add the child node.
            if (ref.get_node(sel).type === 'logic_AND') {
                //var newchild = ref.create_node(sel, {"type":"logic_AND"});
                //ref.open_node(ref.get_parent(newchild))
                //if(newchild) {
                //	ref.set_text(newchild,'AND');
                //}
            } else {
                var oldtype = ref.get_node(sel).type;
                var oldtext = ref.get_node(sel).text;
                var newchild = ref.create_node(sel, { "type": oldtype });
                ref.open_node(ref.get_parent(newchild))
                if (newchild) {
                    ref.set_text(newchild, oldtext);
                }
                ref.set_type(sel, 'logic_AND');
                ref.set_text(sel, "AND (keep if complying with all rules)");

            }
            // move all original children to new child.
            for (idx = 0; idx < children.length; idx++) {
                var child = children[idx].id;
                ref.move_node(child, newchild, 'last')
            }
        }

        fid = document.Storedfilters.filters.value
        Loaded_Filters = Loaded_Filters + ' : ' + document.getElementById('f_name_' + fid).innerHTML;
        // get the logic

        var url = "ajax_queries/GetFilterStringTree.php";
        //url += '?fid='+fid;
        nr_to_load_filters = nr_to_load_filters + 1;
        $.get(url, { fid: fid }, function (data) {
            processLoadedFilter(data.rules, data.filtertree);

        }, "json");
    }
    WaitForApplying(content, Loaded_Filters);
}
function WaitForApplying(content, Loaded_Filters) {
    // wait for loading to finish.
    loading_intervalId = setInterval(function () {
        if (AbortLoad == 1) {
            deleteCookie('skipCookieSave');
            FiltersToCookies(listed);
            SetOverlay('none');
            AbortLoad = 0;
            clearInterval(loading_intervalId);
            loading_intervalId = null;
            return;
        }
        // first check : started loading all filters.
        if (nr_loaded_filters == nr_to_load_filters) {
            // second check : all rules are done.
            if (sum(Loading_Rules) == Object.keys(Loading_Rules).length) {
                clearInterval(loading_intervalId);
                loading_intervalId = null;
                // update the cookies!
                deleteCookie('skipCookieSave');
                FiltersToCookies(listed);

                if (typeof content !== null && content == '') {
                    content = "Loaded Filters" + Loaded_Filters;
                }
                else {
                    content = content + Loaded_Filters;
                }
                SetLoaded(content);
                SetOverlay('none');
            }
        }
        else {
            console.log('waiting for filters');
        }
    }, 250);



}
// sum values of a dictionary
function sum(obj) {
    var sum = 0;
    for (var el in obj) {
        if (obj.hasOwnProperty(el)) {
            sum += parseFloat(obj[el]);
        }
    }
    return sum;
}

function AddChild(childtype, parentnode) {
    var ref = $('#filter_tree').jstree(true)
    if (childtype === 'logic_AND') {
        var newchild = ref.create_node(parentnode, { "type": "logic_AND" });
        ref.open_node(ref.get_parent(newchild))
        if (newchild) {
            ref.set_text(newchild, 'AND (keep if complying with all rules)');
        }
    }
    else if (childtype === 'logic_OR') {
        var newchild = ref.create_node(parentnode, { "type": "logic_OR" });
        ref.open_node(ref.get_parent(newchild))
        if (newchild) {
            ref.set_text(newchild, 'OR (keep if complying with at least one rule)');
        }
    }
    else if (childtype === 'logic_NAND') {
        var newchild = ref.create_node(parentnode, { "type": "logic_NAND" });
        ref.open_node(ref.get_parent(newchild))
        if (newchild) {
            ref.set_text(newchild, 'NAND (discard if complying with all rules)');
        }
    }
    else if (childtype === 'logic_NOR') {
        var newchild = ref.create_node(parentnode, { "type": "logic_NOR" });
        ref.open_node(ref.get_parent(newchild))
        if (newchild) {
            ref.set_text(newchild, 'NOR (discard if complying with at least one rule)');
        }
    }

    // return the ID
    return (newchild);
}

function ParseLogic(tree, parentnode) {
    var children = tree.children
    var result = {}
    for (var idx = 0; idx < children.length; idx++) {
        var childtype = children[idx].type
        // skip the rules themselves, only add logic tree
        if (childtype.substring(0, 5) == 'logic') {
            // add the node.
            var newchild = AddChild(childtype, parentnode)
            var oldchild = children[idx].id
            result[oldchild] = newchild;
            // set type.
            // recurse if children.
            if (children[idx].children.length > 0) {
                subresult = ParseLogic(children[idx], newchild)
                for (var key in subresult) {
                    result[key] = subresult[key]
                }
            }

        }
        else {
            // for rules : get the new parent node.
            var oldid = children[idx].id
            result[oldid] = parentnode
        }
    }
    return (result)
}
function ParseLogicNew(tree, parentnode) {
    var ref = $('#filter_tree').jstree(true)
    var children = tree.children
    var result = {}
    for (var idx = 0; idx < children.length; idx++) {
        var childtype = children[idx].type
        // skip the rules themselves, only add logic tree
        if (childtype.substring(0, 5) == 'logic') {
            // add the node.
            var newchild = AddChild(childtype, parentnode)
            var oldchild = children[idx].id
            result[oldchild] = newchild;
            // set type.
            // recurse if children.
            if (children[idx].children.length > 0) {
                subresult = ParseLogicNew(children[idx], newchild)
                for (var key in subresult) {
                    result[key] = subresult[key]
                }
            }

        }
        else {
            // for rules : get the new parent node.
            var oldid = children[idx].id
            result[oldid] = parentnode
            var ruleChild = ref.create_node(parentnode, { 'id': oldid, 'text': children[idx].text, "type": "default" }, 'last');
        }
    }

    return (result)
}
function processLoadedFilterNew(filterstring, treestring) {
    var ref = $('#filter_tree').jstree(true)
    var RuleIDs = {}
    if (treestring != '') {
        var loadedtree = JSON.parse(treestring);
        loadedtree = loadedtree[0]
        var roottype = loadedtree['type']
        var newbranch = AddChild(roottype, 'j1_1');
        // add logic subrules
        RuleIDs = ParseLogicNew(loadedtree, newbranch);
    }
    return;
    //return;
    // process the rules
    var fields = filterstring.split("@@@");
    var opts = [];
    for (var j = 0; j < fields.length; j++) {
        opts[fields[j].split('|||')[0]] = fields[j].split('|||')[1];
    }
    var pattern = /category(\d+)/i;
    for (var i = 0; i < fields.length; i++) {
        if (pattern.test(fields[i])) {

            var subfields = fields[i].split('|||');
            var idx = pattern.exec(subfields[0])[1];
            var category = subfields[1];
            // get substr length to match later on (e.g negate14) 
            /*
            var toneg = 6 + idx.length;
            var topar = 5 + idx.length;
            var toarg = 8 + idx.length;
            var toval = 5 + idx.length;
            var tossqv = 4 + idx.length;
            */
            var negate = (typeof opts['negate' + idx] == 'undefined') ? '' : opts['negate' + idx];
            var param = (typeof opts['param' + idx] == 'undefined') ? '' : opts['param' + idx];
            var argum = (typeof opts['argument' + idx] == 'undefined') ? '' : opts['argument' + idx];
            var values = (typeof opts['value' + idx] == 'undefined') ? '' : opts['value' + idx];
            var ssqv = (typeof opts['ssqv' + idx] == 'undefined') ? '' : opts['ssqv' + idx];
            /*
            for (var j = 0; j < fields.length; j++) {
                if (fields[j].substring(0,toneg) === 'negate'+idx) {
                    negate = fields[j].split('|||')[1];
                }
                else if (fields[j].substr(0,topar) === 'param'+idx) {
                    param = fields[j].split('|||')[1];
                }
                else if (fields[j].substr(0,toarg) === 'argument'+idx) {
                    argum = fields[j].split('|||')[1];
                    var re = /__$/;
                    argum = argum.replace(re,"");
                }
                else if (fields[j].substr(0,toval) === 'value'+idx) {
                    values = fields[j].split('|||')[1];
                }
                else if (fields[j].substr(0,tossqv) === 'ssqv'+idx) {
                    ssqv = fields[j].split('|||')[1];
                }
            }
            */
            // dynamic options? => set DB cookies. They are fetched by the FilterBuilder to set options.
            dyn_opts = {}; // set from AddCategory to prevent 'listed' racing
            if (category === 'Dynamic_Filters') {
                // nr_of_dyn_opts
                var nr = opts['nr_do_' + idx];
                if (!nr) {
                    alert('Filter ' + idx + ' (section ' + category + ') was incomplete and not loaded');
                    continue;
                }
                dyn_opts['nrop'] = nr.toString();

                //setDbCookie('dyn'+(listed+1)+'_nrop',nr.toString(),1,true);
                // the options 
                for (var j = 1; j <= nr; j++) {
                    var o_name = opts['do' + idx + '_' + j + '_name'];
                    var o_value = opts['do' + idx + '_' + j + '_value'];
                    //setDbCookie('dyn'+(listed+1)+'_'+o_name,o_value.toString(),1,true);
                    dyn_opts[o_name] = o_value.toString();
                }


            }
            // get the node to add the filter to (add to j1_1 if missing)
            var nodeID = 'j1_1';
            if ('rule_' + idx in RuleIDs) {
                nodeID = RuleIDs['rule_' + idx];
            }
            var ret = AddCategory(category, negate, param, argum, values, ssqv, getArguments, nodeID, dyn_opts);



        }
    }
    nr_loaded_filters = nr_loaded_filters + 1;
}

function processLoadedFilter(filterstring, treestring) {
    var ref = $('#filter_tree').jstree(true)
    var RuleIDs = {}
    if (treestring != '') {

        var loadedtree = JSON.parse(treestring);
        loadedtree = loadedtree[0]
        var roottype = loadedtree['type']
        var newbranch = AddChild(roottype, 'j1_1');
        // add logic subrules
        RuleIDs = ParseLogic(loadedtree, newbranch);
    }
    //return;
    // process the rules
    var fields = filterstring.split("@@@");
    var opts = [];
    for (var j = 0; j < fields.length; j++) {
        opts[fields[j].split('|||')[0]] = fields[j].split('|||')[1];
    }
    var pattern = /category(\d+)/i;
    for (var i = 0; i < fields.length; i++) {
        if (pattern.test(fields[i])) {

            var subfields = fields[i].split('|||');
            var idx = pattern.exec(subfields[0])[1];
            var category = subfields[1];
            // get substr length to match later on (e.g negate14) 
            /*
            var toneg = 6 + idx.length;
            var topar = 5 + idx.length;
            var toarg = 8 + idx.length;
            var toval = 5 + idx.length;
            var tossqv = 4 + idx.length;
            */
            var negate = (typeof opts['negate' + idx] == 'undefined') ? '' : opts['negate' + idx];
            var param = (typeof opts['param' + idx] == 'undefined') ? '' : opts['param' + idx];
            var argum = (typeof opts['argument' + idx] == 'undefined') ? '' : opts['argument' + idx];
            var values = (typeof opts['value' + idx] == 'undefined') ? '' : opts['value' + idx];
            var ssqv = (typeof opts['ssqv' + idx] == 'undefined') ? '' : opts['ssqv' + idx];
            /*
            for (var j = 0; j < fields.length; j++) {
                if (fields[j].substring(0,toneg) === 'negate'+idx) {
                    negate = fields[j].split('|||')[1];
                }
                else if (fields[j].substr(0,topar) === 'param'+idx) {
                    param = fields[j].split('|||')[1];
                }
                else if (fields[j].substr(0,toarg) === 'argument'+idx) {
                    argum = fields[j].split('|||')[1];
                    var re = /__$/;
                    argum = argum.replace(re,"");
                }
                else if (fields[j].substr(0,toval) === 'value'+idx) {
                    values = fields[j].split('|||')[1];
                }
                else if (fields[j].substr(0,tossqv) === 'ssqv'+idx) {
                    ssqv = fields[j].split('|||')[1];
                }
            }
            */
            // dynamic options? => set DB cookies. They are fetched by the FilterBuilder to set options.
            dyn_opts = {}; // set from AddCategory to prevent 'listed' racing
            if (category === 'Dynamic_Filters') {
                // nr_of_dyn_opts
                var nr = opts['nr_do_' + idx];
                if (!nr) {
                    alert('Filter ' + idx + ' (section ' + category + ') was incomplete and not loaded');
                    continue;
                }
                dyn_opts['nrop'] = nr.toString();

                //setDbCookie('dyn'+(listed+1)+'_nrop',nr.toString(),1,true);
                // the options 
                for (var j = 1; j <= nr; j++) {
                    var o_name = opts['do' + idx + '_' + j + '_name'];
                    var o_value = opts['do' + idx + '_' + j + '_value'];
                    //setDbCookie('dyn'+(listed+1)+'_'+o_name,o_value.toString(),1,true);
                    dyn_opts[o_name] = o_value.toString();
                }


            }
            // get the node to add the filter to (add to j1_1 if missing)
            var nodeID = 'j1_1';
            if ('rule_' + idx in RuleIDs) {
                nodeID = RuleIDs['rule_' + idx];
            }
            var ret = AddCategory(category, negate, param, argum, values, ssqv, getArguments, nodeID, dyn_opts);



        }
    }
    nr_loaded_filters = nr_loaded_filters + 1;
}

////////////////////////////////////////////////////
// LOAD SELECTED ANNOTATIONS AND ADD TO TABLE //
////////////////////////////////////////////////////
function ApplyAnnotations() {

    // get xmlhttp object
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }

    var nrb = 0;
    if (document.StoredAnnotations.annotations.length) {
        nrb = document.StoredAnnotations.annotations.length;
        for (var i = 0; i < nrb; i++) {
            if (document.StoredAnnotations.annotations[i].checked) {
                var aid = document.StoredAnnotations.annotations[i].value;
                var url = "ajax_queries/GetAnnotationString.php";
                url += '?aid=' + aid;
                xmlhttp.open("GET", url, false);
                xmlhttp.send(null);
                processLoadedAnnotation(xmlhttp.responseText);
            }

        }

    }
    else if (document.StoredAnnotations.annotations.checked) {
        aid = document.StoredAnnotations.annotations.value;
        var url = "ajax_queries/GetAnnotationString.php";
        url += '?aid=' + aid;
        xmlhttp.open("GET", url, false);
        xmlhttp.send(null);
        processLoadedAnnotation(xmlhttp.responseText);

    }
    // AND ADD TO COOKIES
    AnnotationsToCookies();
}


// extend array prototype...
Array.prototype.contains = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}

function processLoadedAnnotation(annotationstring) {
    // split on @@@ = different fields
    var fields = annotationstring.split("@@@");
    var nrchecks = document.annotationform.annotations.length;
    for (var idx = 0; idx < nrchecks; idx++) {
        if (fields.contains(document.annotationform.annotations[idx].value)) {
            document.annotationform.annotations[idx].checked = true;
        }
    }

}

// load cookies (filters & annotations)
function LoadVariantsPage() {
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Restoring previous settings</span><p><img src='Images/layout/loader-bar.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p><input type=submit value='Cancel' onClick='Abort_Load(1)' /></p></div>";
    SetOverlay('', newcontent);
}
function Abort_Load(value) {
    AbortLoad = value;
}

// store cram agreement
function CRAMagree(uid) {
    var remember = document.getElementById("remember").checked;
    // regular cookie.
    setCookie('cram', 1, 7)
    // remember choice in DB
    if (remember) {
        var xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        // settings 
        var url = "ajax_queries/SaveCRAMselection.php";
        url = url + "?uid=" + uid;
        url = url + "&rv=" + Math.random();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                return;
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
    }
    document.getElementById('overlay').style.display = 'none';

}


// Cookies To Filters
function CookiesToFilters(uid, type) {
    error.log("cookies to filters")
    //alert(document.cookie)
    var sid = getCookie('SID');
    if (sid == "") {
        WaitForReload(0, '');
        return;
    }
    // 1. Set Sample or region
    if (getCookie("SST") == "Region") {
        setCookie('SID', 'region', 1);
        var sel = document.getElementById('selecttype');
        var opts = sel.options;
        for (var opt, j = 0; opt = opts[j]; j++) {
            if (opt.value == 'Region') {
                sel.selectedIndex = j;
                break;
            }
        }
        document.getElementById('sampleselect').innerHTML = "<input type=hidden name=sampleid id=sampleid value='region'><input type=text style='width:98%;' value='" + getCookie('RV') + "' title='Provide a gene symbol (GAPDH) or region (chr11:1-500)' name=regionquery onChange='SetRegionCookie()' id=searchfield>";
    }
    else if (getCookie("SST") == "Project") {
        setCookie('SID', 'project', 1);
        var sel = document.getElementById('selecttype');
        var opts = sel.options;
        for (var opt, j = 0; opt = opts[j]; j++) {
            if (opt.value == 'Project') {
                sel.selectedIndex = j;
                break;
            }
        }
        // set loader
        document.getElementById('sampleselect').innerHTML = '<img scr="Images/layout/loader-bar.gif">';
        // get options from ajax 
        var xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        // settings 
        var url = "ajax_queries/GetProjectSelect.php";
        url = url + "?uid=" + uid;
        url = url + "&pid=" + getCookie('PV');
        url = url + "&rv=" + Math.random();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                document.getElementById('sampleselect').innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
    }
    else {
        // set sample. On fresh load, always the "type sample" input 
        setCookie('SST', 'Type', 1);
        document.getElementById('sampleid').value = sid;
        document.getElementById('searchfield').value = getCookie('SN');
        // load the data
        CheckDataFiles(uid, sid);
    }
    // 2. Load Filters.
    var todo = parseInt(getCookie('listed'), 10)
    if (todo == '') {
        todo = 0
    }
    //setCookie('skipCookieSave','1',1)
    // get all cookies.
    var params = { a: 'bg', 'FCs': 1 };
    for (x = 1; x <= todo; x++) {
        params['C' + x] = 1;
        params['N' + x] = 1;
        params['C' + x] = 1;
        params['A' + x] = 1;
        params['P' + x] = 1;
        params['V' + x] = 1;
        params['s' + x] = 1;
    }
    var url = "ajax_queries/Cookies.php";
    // force synchro call to get cuid
    if (cuid == null) {
        getDbCookie('C0');
    }
    params['uid'] = cuid;
    Loading_Rules = {};
    $.post(url, params, function (cookies) {
        for (x = 1; x <= todo; x++) {
            if (cookies.hasOwnProperty('C' + x)) {
                Loading_Rules[x] = 0;
                var negate = cookies['N' + x];
                var category = cookies['C' + x];
                var argument = cookies['A' + x];
                var parameter = cookies['P' + x];
                var value = cookies['V' + x];
                var ssqv = cookies['s' + x];
                var ret = AddCategory(category, negate, parameter, argument, value, ssqv, getArguments)
            }
            else {
                listed = listed + 1;
                Loading_Rules[x] = 1;
                continue;
            }

        }
        // lauch interval waiting trigger.
        WaitForReload(todo, type);
    }, 'json');
}

function WaitForReload(todo, type) {
    // wait for finish.
    loading_intervalId = setInterval(function () {
        if (AbortLoad == 1) {
            deleteCookie('skipCookieSave');
            FiltersToCookies(listed);
            SetOverlay('none');
            AbortLoad = 0;
            clearInterval(loading_intervalId);
            loading_intervalId = null;
            return;
        }
        //  check : all rules are done.
        if (sum(Loading_Rules) == todo) {
            switchType(type, 'j1_1');
            clearInterval(loading_intervalId);
            loading_intervalId = null;
            // update the cookies!
            deleteCookie('skipCookieSave');
            FiltersToCookies(listed);
            // check data.	
            var data_link = document.getElementById('LoadDataLink').innerHTML;
            if (data_link.indexOf('.cram') < 0) {
                SetOverlay('none');
            } else {
                var cram = getCookie('cram');
                if (cram == "1") {
                    SetOverlay('none');
                } else {
                    checkCRAM(data_link, uid)
                }
            }
            SetOverlay('none');
        }

    }, 250);
    // one shot cookie update at the end.
    //deleteCookie('skipCookieSave');
    //var loadedtree = JSON.parse(treestring);
    //FiltersToCookies(todo);
    // update root node type
}

function SetRegionCookie() {
    var d = new Date();
    d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = "RV=" + document.getElementById('searchfield').value + "; " + expires;
}
function SetProjectCookie() {
    var d = new Date();
    d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = "PV=" + document.getElementById('searchfield').value + "; " + expires;
}

function setDbCookie(cname, cvalue, exdays, type) {
    type = typeof type !== 'undefined' ? type : false;
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var chash = cvalue.hashCode();
    // store true value to DB	
    // get uid if not defined
    if (cuid == null) {
        var url = "ajax_queries/Cookies.php";
        var param = "a=u";
        xmlhttp.open("POST", url, false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(param);
        cuid = xmlhttp.responseText;
    }
    var url = "ajax_queries/Cookies.php";
    var param = "a=s&uid=" + cuid + "&name=" + cname + "&value=" + cvalue;
    xmlhttp.open("POST", url, type);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(param);

}

function setDbCookies(vars, exdays, type) {
    // async by default.
    type = typeof type !== 'undefined' ? type : true;
    // get expiration date
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();

    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    //var chash = cvalue.hashCode();
    // store true value to DB	
    // get uid if not defined
    if (cuid == null) {
        var url = "ajax_queries/Cookies.php";
        var param = "a=u";
        $.post(url, param, function (data, status) {
            vars['a'] = 'bs';
            cuid = data;
            vars['uid'] = cuid;
            // post all cookies
            $.post(url, vars);
        });
    }
    else {
        // immediately post all cookies
        var url = "ajax_queries/Cookies.php";
        vars['a'] = 'bs';
        vars['uid'] = cuid;
        $.post(url, vars);
    }

    //	xmlhttp.open("POST",url,false);
    //	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //	xmlhttp.send(param);
    //	cuid = xmlhttp.responseText;
    //}
    //var url="ajax_queries/Cookies.php";
    //var param = "a=s&uid="+cuid+"&name="+cname+"&value="+cvalue;
    //xmlhttp.open("POST",url,type);
    //xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //xmlhttp.send(param);

}

function deleteDbCookie(cname) {
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // get uid if not defined
    if (cuid == null) {
        var url = "ajax_queries/Cookies.php";
        var param = "a=u&uid=&name=";
        xmlhttp.open("POST", url, false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(param);
        cuid = xmlhttp.responseText;
    }
    //document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    var url = "ajax_queries/Cookies.php";
    var param = "a=d&uid=" + cuid + "&name=" + cname;
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(param);
}
function deleteDbCookies(vars) {
    // get uid if not defined
    if (cuid == null) {
        var url = "ajax_queries/Cookies.php";
        var param = "a=u";
        $.post(url, param, function (data, status) {
            vars['a'] = 'bd';
            cuid = data;
            vars['uid'] = cuid;
            // post all cookies
            $.post(url, vars);
        });
    }
    else {
        // immediately post all cookies
        var url = "ajax_queries/Cookies.php";
        vars['a'] = 'bd';
        vars['uid'] = cuid;
        $.post(url, vars);
    }
}

function deleteCookie(cname) {
    document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
/*
// moved to Global.js
function getCookie(cname)
{
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) 
  {
    var c = ca[i].trim();
    if (c.indexOf(name)===0)
    {
    // empty cookie (invalid or empty value) => return 
    if (c.substring(name.length,c.length) === "")
    { 
        return ""
    }
    return(c.substring(name.length,c.length));
    }
  }
  return "";
}
*/
function getDbCookie(cname) {
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    if (cuid == null) {
        var url = "ajax_queries/Cookies.php";
        var param = "a=u";
        xmlhttp.open("POST", url, false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(param);
        cuid = xmlhttp.responseText;
    }
    var url = "ajax_queries/Cookies.php";
    var param = "a=g&uid=" + cuid + "&name=" + cname;
    xmlhttp.open("POST", url, false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(param);
    return (xmlhttp.responseText);

}

function KillQuery(killfile) {
    var url = "ajax_queries/KillQuery.php";
    // set the variables
    param = "kf=" + killfile;
    // create the ajax call
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // show empty export tab results
    if (CurrSelected === "exporttab") {
        document.getElementById('QueryResults').innerHTML = '<h3>Export Results</h3><p>Previous query was cancelled.</p><p style="padding-left:1em"><input type=button onClick="RunQuery(\'all\');" value="Export Results"></p>';
    }
    // show empty charting tab results
    else if (CurrSelected === "charttab") {
        document.getElementById('QueryResults').innerHTML = '<h3>QC-Plots for Filter Results</h3><p>Previous query was cancelled.</p><p>Specified filters are applied before generating plots.</p><p style="padding-left:1em"><input type=button onClick="GetCharts(\'all\');" value="Generate Charts"></p>';
        // show empty filter tab results
    } else {//|| CurrSelected === "charttab"  || CurrSelected === "savedtab") {
        document.getElementById('QueryResults').innerHTML = '<h3>Query Results</h3><p>Previous query was cancelled.</p><p style="padding-left:1em"><input type=checkbox id="widetable" name="widetable"> Use wide output table format, putting all annotations in a single line per variant</p><p style="padding-left:1em"><input type=button onClick="RunQuery(\'0\');" value="Run Query"></p>';
    }
    // kill ajax call
    if (xmlhttpRQ !== null) {
        xmlhttpRQ.abort();
    }
    // stop the query monitor 
    if (interval_id !== null) {
        clearInterval(interval_id);
    }
    /*			
    xmlhttp.onreadystatechange=function() {
       if (xmlhttp.readyState==4) {
        //document.getElementById('overlay').style.display='none';
        // switches to filtertab.
        //ToggleTab('filtertab');
        return;
       }
    }
    */
    // SEND REQUEST WITH POST
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(param);
}

String.prototype.hashCode = function () {
    var hash = 0, i, chr, len;
    if (this.length == 0) return hash;
    for (i = 0, len = this.length; i < len; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

// set the jquery call to process cookies when the document is ready.
$(document).ready(function () {
    setCookie('skipCookieSave', '1', 1)
    LoadFilteringTree();
    // call the function on initial loading of the script.
    if (typeof cuid === undefined || cuid === '' || cuid === null) {
        getDbCookie('C0');
    }
    uid = cuid;
    // set sid/region.
    var sid = getCookie('SID');
    if (getCookie("SST") == "Region") {
        setCookie('SID', 'region', 1);
        var sel = document.getElementById('selecttype');
        var opts = sel.options;
        for (var opt, j = 0; opt = opts[j]; j++) {
            if (opt.value == 'Region') {
                sel.selectedIndex = j;
                break;
            }
        }
        document.getElementById('sampleselect').innerHTML = "<input type=hidden name=sampleid id=sampleid value='region'><input type=text style='width:98%;' value='" + getCookie('RV') + "' title='Provide a gene symbol (GAPDH) or region (chr11:1-500)' name=regionquery onChange='SetRegionCookie()' id=searchfield>";
    }
    else if (getCookie("SST") == "Project") {
        setCookie('SID', 'project', 1);
        var sel = document.getElementById('selecttype');
        var opts = sel.options;
        for (var opt, j = 0; opt = opts[j]; j++) {
            if (opt.value == 'Project') {
                sel.selectedIndex = j;
                break;
            }
        }
        // set loader
        document.getElementById('sampleselect').innerHTML = '<img scr="Images/layout/loader-bar.gif">';
        // get options from ajax 
        var xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        // settings 
        var url = "ajax_queries/GetProjectSelect.php";
        url = url + "?uid=" + uid;
        url = url + "&pid=" + getCookie('PV');
        url = url + "&rv=" + Math.random();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                document.getElementById('sampleselect').innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
    }
    else {
        // set sample. On fresh load, always the "type sample" input 
        setCookie('SST', 'Type', 1);
        document.getElementById('sampleid').value = sid;
        document.getElementById('searchfield').value = getCookie('SN');
        // load the data
        CheckDataFiles(uid, sid);
    }

    var url = "ajax_queries/GetFullFilters.php";

    var pars = {
        fc: '1',
        uid: cuid,
        sid: sid,
        //region:region,
        //project:project
    }
    // load empty tree.
    //LoadFilteringTree();
    // get contents.
    $.get(url, pars, function (data) {
        // nothing loaded.
        if (data.listed == 0 || typeof data.jstree == "undefined" || data.jstree == '') {
            deleteCookie('skipCookieSave');
            FiltersToCookies(listed);
            SetOverlay('none');
            return;
        }

        var ref = $('#filter_tree').jstree(true)
        sel = 'j1_1';
        // add all rules. 
        $.each(data.rules, function (index, rule) {
            var c = rule.c;
            var v = rule.v;
            var tbody = document.getElementById("FilterBy" + c);
            var row = tbody.insertRow(-1);
            row.id = "FilterStep" + index;
            // get number of cells (from 0th row = header row)
            var cells = document.getElementById("querytable").rows[0].cells.length;
            // add cells to the row.
            var i;
            for (i = 0; i < cells; i++) {
                var newCell = row.insertCell(i);
            }
            // negate cell
            if (c === 'Dynamic_Filters') {
                row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;margin-bottom:-0.2em'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "' style='display:none'><option value='' SELECTED>Match</option><option value='NOT'>Not Match</option></select>Match";
                row.cells[1].innerHTML = v[1];
                row.cells[2].innerHTML = v[2];
                row.cells[3].innerHTML = v[3];
            }
            else {
                if (v[0] === "") {
                    row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "'><option value=''>Match</option><option value='NOT'>Not Match</option></select>";
                }
                else {
                    row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "'><option value=''>Match</option><option value='NOT' selected>Not Match</option></select>";
                }
                // level 1
                row.cells[1].innerHTML = v[1].result + '<span id=ss' + index + '></span>';
                // level 2
                row.cells[2].innerHTML = v[2].result;
                // level 3
                if (v[2].result !== v[3].result) {
                    row.cells[3].innerHTML = v[3].result;
                }
            }

        });

        // add trees per loaded filter.
        var l_tree = JSON.parse(data.jstree)[0];
        //LoadFilteringTreeWithContents(data.jstree);
        var roottype = l_tree['type']
        var rootid = 'j1_1';
        // add to the root 'and' as child if not 'and'.
        if (roottype != 'logic_AND') {
            rootid = AddChild(roottype, 'j1_1');
        }
        var RuleIDs = ParseLogicNew(l_tree, rootid);
        // set listed
        listed = data.listed;
        // load annotations.
        CookiesToAnnotations();
        // update cookies.
        ref.open_all();
        deleteCookie('skipCookieSave');
        FiltersToCookies(listed);

        SetOverlay('none');

    }, "json");
});

