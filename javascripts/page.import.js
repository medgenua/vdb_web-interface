function toggleSelect() {
    if (document.getElementById('direct_upload').style.display == 'none') {
        document.getElementById('direct_upload').style.display = '';
    }
    else {
        document.getElementById('direct_upload').style.display = 'none';
    }
    if (document.getElementById('ftp_server').style.display == 'none') {
        document.getElementById('ftp_server').style.display = '';
    }
    else {
        document.getElementById('ftp_server').style.display = 'none';
    }

}

function ValidateUpload() {
    // file has correct extension?
    var filename = document.getElementById('upload_file').value.split(/(\\|\/)/g).pop();
    var ext = filename.substr(filename.length - 11)
    if (ext !== '.tar.gz.gpg') {
        document.getElementById('upload_file_comment').innerHTML = 'Invalid file format selected. Only \'.tar.gz.gpg\' allowed.';
        document.getElementById('SubmitUPLOAD').disabled = true;
        return;
    }
    else {
        document.getElementById('upload_file_comment').innerHTML = '';
    }
    // passphrase has length of 11
    if (document.getElementById('upload_pass').value.length < 12) {
        document.getElementById('SubmitUPLOAD').disabled = true;
        return;
    }
    // here : all valid, enable submit
    document.getElementById('SubmitUPLOAD').disabled = false;

}
