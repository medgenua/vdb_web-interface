function GetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}



// DELETE SAMPLES function from page_variants.
function DeleteProject(userid, pid) {
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p><img src='Images/layout/loader-bar.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p><input type=button value='Cancel' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";

    document.getElementById('overlay').innerHTML = newcontent;
    document.getElementById('overlay').style.display = '';

    // submit to ajax.
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // need to reload the sample details here.
            document.getElementById('overlay').innerHTML = xmlhttp.responseText;
        }
    }
    // settings 
    var url = "ajax_queries/ProjectHandler.php";
    url = url + "?uid=" + userid;
    url = url + '&a=d';
    url = url + "&p=" + pid;
    url = url + "&rv=" + Math.random();
    // SEND REQUEST WITH POST
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

}
// DELETE SAMPLES function from page_variants.
function ExportProject(userid, pid) {
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><h3 style='float:left;'>Exporting Project </h3><p style='float:left;'>The project is being exported. Exported information includes:</p><p style='text-align:left;'><ul style='text-align:left;'>";
    newcontent += "<li>Sample info: Name, Gender, parental information.</li>";
    newcontent += "<li>Variants: Only variants classified as (likely) pathogenic, with limited annotations</li>";
    newcontent += "<li>Variant annotations include : location, depth, inheritance.</li>";
    newcontent += "<li>Gene annotations includes : Gene, transcripts, exon-nr, cPoint, pPoint</li>";
    newcontent += "<li>If available, OMIM phenotype summary.</li>";
    newcontent += "</ul><br/></p>";
    newcontent += "<p ><img src='Images/layout/loader-bar.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p ><input type=button value='Cancel' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";

    document.getElementById('overlay').innerHTML = newcontent;
    document.getElementById('overlay').style.display = '';

    // submit to ajax.
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // need to reload the sample details here.
            document.getElementById('overlay').innerHTML = xmlhttp.responseText;
        }
    }
    // settings 
    var url = "ajax_queries/ProjectHandler.php";
    url = url + "?uid=" + userid;
    url = url + '&a=e';
    url = url + "&p=" + pid;
    url = url + "&rv=" + Math.random();
    // SEND REQUEST WITH POST
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

}
// DELETE SAMPLES function from page_variants.
function ExportCSV(userid, pid) {
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><h3 style='float:left;'>Exporting Project summary as CSV</h3><p style='float:left;'>The project is being exported. Exported information includes:</p><p style='text-align:left;'><ul style='text-align:left;'>";
    newcontent += "<li>Sample info: Name, Gender</li>";
    newcontent += "<li>Variants:  originally imported from VCF : GT, DP, PL, etc</li>";
    newcontent += "<li>Log: All log entries related to actions on variants of the exported samples.</li>";
    newcontent += "</ul><br/><span style='float:left;' >What is not included:</span><br/><ul style='text-align:left;'>";
    newcontent += "<li>Saved Results</li>";
    newcontent += "<li>Datafiles (vcf/bam)</li>";
    newcontent += "<li>Annotations added by VariantDB</li>";
    newcontent += "</ul></p>";
    newcontent += "<p style='float:left;'>Results:</p>";
    newcontent += "<p ><textarea id='outputbox' style='width:80%' rows=30>Just a second...</textarea></p>";
    newcontent += "<p ><img id='loaderbar' src='Images/layout/loader-bar.gif' />&nbsp;<br/><input type=button value='Cancel' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";
    document.getElementById('overlay').innerHTML = newcontent;
    document.getElementById('overlay').style.display = '';
    // submit to ajax.
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            document.getElementById('loaderbar').style.display = 'none';
            document.getElementById('outputbox').innerHTML = xmlhttp.responseText;

        }
    }
    // settings 
    var url = "ajax_queries/ProjectHandler.php";
    url = url + "?uid=" + userid;
    url = url + '&a=c';
    url = url + "&p=" + pid;
    url = url + "&rv=" + Math.random();
    // SEND REQUEST WITH POST
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

}

// DELETE SAMPLES function from page_variants.
function ConfirmedDeleteProject(userid, pid) {
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p><img src='Images/layout/loader-bar.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p><input type=button value='Close (this might screw up your data)' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";

    document.getElementById('overlay').innerHTML = newcontent;
    document.getElementById('overlay').style.display = '';


    // submit to ajax.
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // need to reload the sample details here.
            document.getElementById('overlay').innerHTML = xmlhttp.responseText;
        }
    }
    // settings 
    var url = "ajax_queries/ProjectHandler.php";
    url = url + "?uid=" + userid;
    url = url + '&a=cd';
    url = url + "&p=" + pid;
    // SEND REQUEST WITH POST
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);


}


// TOGGLE INCUSION IN SUMMARY STATS
function ToggleProjectFrequencies(pid) {
    $.get("ajax_queries/ToggleProjectSummaries.php", { pid: pid }, function (data) {
        if (data.status == 'Error') {
            alert("Failed to load Results:\n" + data.msg);
            document.getElementById('overlay').style.display = 'none';
            return;
        }
        // refresh page.
        location.reload();
    }, 'json');

}
