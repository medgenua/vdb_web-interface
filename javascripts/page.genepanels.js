// general counters
var listed = 0;

function GetXmlHttpObject() {
	if (window.XMLHttpRequest) {
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  return new XMLHttpRequest();
	}
	if (window.ActiveXObject) {
  		// code for IE6, IE5
  		return new ActiveXObject("Microsoft.XMLHTTP");
  	}
	return null;
}

function DeletePanelGene(gpid,slice,idx) {
	// this function deletes a gene from a gene panel.
	// 1. ajax call to dete from db
	var symbol = document.getElementById("orig["+slice+"]["+idx+"]").value;
	document.getElementById("trash["+slice+"]["+idx+"]").innerHTML="<img src='Images/layout/ajax-loader.gif' width='15px'>";
	var url = "ajax_queries/DeletePanelGene.php?gpid="+gpid;
	url = url + "&symbol="+symbol;
	url = url + "&rv="+Math.random();
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}
	// insert content when ready
	xmlhttp.onreadystatechange=function() {
	   if (xmlhttp.readyState==4) {
		if (xmlhttp.responseText == 'denied') {
			alert("Error, This action was denied\n"+xmlhttp.responseText);
			return;
		}
		// no return value. But wait to complete before removing the row
		// 2. delete row from table
		var row = document.getElementById("row["+slice+"]["+idx+"]");
		row.parentNode.removeChild(row);

		return;
	   }

	};
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);

}




