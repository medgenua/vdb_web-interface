// preload data using cgi script (filters & annotation) (if memcache is active)

function preload(uid) {
    var xmlhttp = plGetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // the cgi preloading.
    var url = "cgi-bin/preload.cgi";
    var param = "uid=" + uid;
    // onchangestate function
    xmlhttp.onreadystatechange = function () {
        // don't need actions.
        if (xmlhttp.readyState == 4) {
            // no action required.
            //alert(xmlhttp.responseText);	
        }
    }
    // SEND REQUEST WITH POST
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(param);
    return null;
}


//////////////////////
// MAIN AJAX OBJECT //
//////////////////////
function plGetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}

