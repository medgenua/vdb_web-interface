// main ajax functionality is loaded from ajax_variants because it is needed earlier on.

// autocomplete for search
var options = {
    script: "ajax_queries/samplesearch.php?json=true&limit=10&",
    varname: "input",
    json: true,
    shownoresults: true,
    delay: 200,
    cache: false,
    timeout: 10000,
    minchars: 2,
    callback: function (obj) {
        document.getElementById('searchfield').value = obj.value;
        document.getElementById('sampleid').value = obj.id;

        CheckDataFiles(cuid, obj.id);
    }
};
var as_json = new bsn.AutoSuggest('searchfield', options);


// the default "loading" overlay.
function SetLoading(message) {
    var content = '<p style="text-align:center"><img src="Images/layout/loader-bar.gif" /></p>';
    if (typeof message !== 'undefined') {
        content += "<p style='text-align:center'>" + message + "</p>";
    }
    // closing button.
    content += "<p style='text-align:center'><input type=submit value=Close onClick=\"$('#overlay').hide()\" /></p>";
    $('#overlay').html(content);
    $('#overlay').show();

}
// checkall by annotation name
function CheckAll(name) {
    // check if checked or unchecked.
    var check_status = document.getElementById('CheckAll_' + name).checked
    // get checkboxes in the category.
    var parent = document.getElementById('AnnotateBy' + name)
    var checkboxes = parent.getElementsByTagName('input')
    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = check_status
    }
}


// validate a result set.
function ValidateSavedResults(uid, setid, sid) {
    SetLoading('Fetching altered entries.');
    // compose overlay content
    var newcontent = "<div class='section' style='height:90vh;border:0.2em dashed #aaa;margin-left:3vh;margin-right:3vh;padding:1em;color:#fff;text-align:left;overflow:auto'><span style='text-align:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Validate Result set</span>";
    newcontent += '<input type=hidden name="sid" id="sid" value="' + sid + '"><input  type=hidden name="setid" id=setid value="' + setid + '"><input type=hidden name="uid" id="uid" value="' + uid + '">';
    // get differences.
    var valxmlhttp = GetXmlHttpObject();
    if (valxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings 
    var url = "ajax_queries/CompareStoredResults.php";
    var param = "uid=" + uid;
    param = param + "&setid=" + setid;
    param = param + "&sid=" + sid;
    param = param + "&rv=" + Math.random();
    valxmlhttp.onreadystatechange = function () {
        if (valxmlhttp.readyState == 4) {
            var response = valxmlhttp.responseText;
            if (response !== '') {
                newcontent += '<p class=white style="text-align:left">The following changes were made to the saved results, and will be pushed to the database:</p>';
                newcontent += response;
                newcontent += "</p>";
            } else {
                newcontent += '<span id="all_diffs" style="display:none"></span>';
            }
            newcontent += '<p class=white style="text-align:left;">Add Comments:<br/>';
            newcontent += '<input type=text id=SaveComments name=SaveComments size=30 style="text-align:left;margin-left:1em;">';
            newcontent += '</p>';
            newcontent += '<p style="text-align:left;"><input type=submit value="Save Selection" onClick="ValidateResultSet(\'' + uid + '\')"> &nbsp; <input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
            newcontent += '</div>';
            document.getElementById('overlay').innerHTML = newcontent;

        }
    };
    valxmlhttp.open("POST", url, true);
    valxmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    valxmlhttp.send(param);

}
function DeleteStoredResults(uid, setid, sid) {
    // compose overlay content
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Delete Result set</span>";

    newcontent += '<input type=hidden name="sid" id="sid" value="' + sid + '"><input  type=hidden name="setid" id=setid value="' + setid + '"><input type=hidden name="uid" id="uid" value="' + uid + '">';
    newcontent += '<p class=white>Please confirm deletion:<br/>';
    newcontent += '<p><input type=submit value="Proceed" onClick="DeleteResultSet(\'' + uid + '\')"> &nbsp; <input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
    newcontent += '</div>';
    $('#overlay').html(newcontent); //document.getElementById('overlay').innerHTML = newcontent;
    $('#overlay').show(); //document.getElementById('overlay').style.display = '';	

}
function DeleteValidatedResults(uid, setid, sid) {
    // compose overlay content
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='color:red;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Deleting Validated Result set</span>";

    newcontent += '<input type=hidden name="sid" id="sid" value="' + sid + '"><input  type=hidden name="setid" id=setid value="' + setid + '"><input type=hidden name="uid" id="uid" value="' + uid + '">';
    newcontent += '<p class=white >Provide a reason:<br/>';
    newcontent += '<input type=text id=DelComments name=DelComments size=30 style="text-align:left;margin-left:1em;"> <br/><span id="reason_error" style="color:red;display:none">ERROR: Reason is mandatory</span>';
    newcontent += '</p>';
    newcontent += '<p class=white>Action:<br:>';
    newcontent += '<input type="radio" name="action" value="u" checked />Undo Validation <input type="radio" name="action" value="d" />Delete</p>';
    newcontent += '<p><input type=submit value="Proceed" onClick="DeleteValidatedResultSet(\'' + uid + '\')"> &nbsp; <input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
    newcontent += '</div>';
    $('#overlay').html(newcontent); //document.getElementById('overlay').innerHTML = newcontent;
    $('#overlay').show(); //document.getElementById('overlay').style.display = '';	
}

function ValidateResultSet(uid) {
    var sid = document.getElementById('sid').value;
    var setid = document.getElementById('setid').value;
    var comments = document.getElementById('SaveComments').value;
    var all_diffs = document.getElementById('all_diffs').innerHTML
    // show loading notifier
    //document.getElementById('overlay').innerHTML = '<p style="text-align:center"><img src="Images/layout/loader-bar.gif" /><br/>Validating...</p>';
    SetLoading('Processing validation request...');
    var valxmlhttp = GetXmlHttpObject();
    if (valxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings 
    var url = "ajax_queries/ValidateResultSet.php";
    var param = "uid=" + uid;
    param = param + "&action=validate";
    param = param + "&setid=" + setid;
    param = param + "&sid=" + sid;
    param = param + "&comments=" + encodeURIComponent(comments);
    param = param + "&allchanges=" + encodeURIComponent(all_diffs);
    param = param + "&rv=" + Math.random();
    valxmlhttp.onreadystatechange = function () {
        if (valxmlhttp.readyState == 4) {
            var response = valxmlhttp.responseText;
            if (response == 'OK') {
                // load the log file"
                var logurl = "ajax_queries/LoadLog.php?type=summary&sid=" + sid;
                logurl = logurl + "&rv=" + Math.random();
                var xmlhttpLOG = GetXmlHttpObject();
                if (xmlhttpLOG == null) {
                    alert("Browser does not support HTTP Request");
                    return;
                }
                // onchangestate function
                xmlhttpLOG.onreadystatechange = function () {
                    if (xmlhttpLOG.readyState == 4) {
                        document.getElementById('LogTab').innerHTML = xmlhttpLOG.responseText;
                    }
                }
                // SEND REQUEST WITH POST
                xmlhttpLOG.open("GET", logurl, true);
                xmlhttpLOG.send(null);
                // load the saved results file"
                surl = "ajax_queries/LoadStoredResults.php?sid=" + sid;
                surl = surl + "&rv=" + Math.random();
                var xmlhttpS = GetXmlHttpObject();
                if (xmlhttpS == null) {
                    alert("Browser does not support HTTP Request");
                    return;
                }
                // onchangestate function
                xmlhttpS.onreadystatechange = function () {
                    if (xmlhttpS.readyState == 4) {
                        document.getElementById('SavedTab').innerHTML = xmlhttpS.responseText;
                    }
                }
                // SEND REQUEST WITH POST
                xmlhttpS.open("GET", surl, true);
                xmlhttpS.send(null);

                $('#overlay').hide();//document.getElementById('overlay').style.display='none';	
            }
            else {
                $('#overlay').html('<h3>ERROR</h3><p>An error occured during submission of the form. See details below:<br/>' + response + '</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
            }
        }
    };
    valxmlhttp.open("POST", url, true);
    valxmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    valxmlhttp.send(param);



}
function DeleteValidatedResultSet(uid) {
    var sid = document.getElementById('sid').value;
    var setid = document.getElementById('setid').value;
    var action = $("input[name='action']:checked").val();
    // show loading notifier
    var comments = $('#DelComments').val(); //document.getElementById('DelComments').value;
    if (comments.length == 0) {
        $("#reason_error").show();
        return;
    }
    else {
        $("#reason_error").hide();
        //$("#overlay").html('<p style="text-align:center"><img src="Images/layout/loader-bar.gif" /><br/>Processing...</p>');
        SetLoading('Processing...');
    }
    // settings 
    var url = "ajax_queries/DeleteValidatedResultSet.php";
    var param = {
        uid: uid,
        sid: sid,
        setid: setid,
        comments: comments,
        a: action,
    };
    $.post(url, param, function (data) {
        if (data == 'OK') {
            // log
            var logurl = "ajax_queries/LoadLog.php";//?type=summary&sid="+sid;
            //logurl = logurl+"&rv="+Math.random();
            var param = { type: 'summary', sid: sid, rv: Math.random() };
            $("#LogTab").load(logurl + "?" + $.param(param));
            // saved results
            var surl = "ajax_queries/LoadStoredResults.php";
            param = { sid: sid, rv: Math.random() };
            $("#SavedTab").load(surl + "?" + $.param(param), function () {
                $("#overlay").hide();
            });
        }
        else {
            $("#overlay").html('<h3>ERROR</h3><p>An error occured during submission of the form. See details below:<br/>' + data + '</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>')
        }

    });
}
function ShowDeletedSets() {
    $("#ShowDelSets").hide();
    $("#DeletedResults").show();
    return;
}
function UndeleteSavedResults(uid, setid, sid) {
    var url = "ajax_queries/UndeleteResultSet.php";
    var param = {
        uid: uid,
        setid: setid,
        sid: sid
    };
    $.post(url, param, function (data) {
        if (data == 'OK') {
            // log
            var logurl = "ajax_queries/LoadLog.php";//?type=summary&sid="+sid;
            //logurl = logurl+"&rv="+Math.random();
            var param = { type: 'summary', sid: sid, rv: Math.random() };
            $("#LogTab").load(logurl + "?" + $.param(param));
            // saved results
            var surl = "ajax_queries/LoadStoredResults.php";
            param = { sid: sid, rv: Math.random() };
            $("#SavedTab").load(surl + "?" + $.param(param), function () {
                $("#overlay").hide();
            });
        }
        else {
            $("#overlay").html('<h3>ERROR</h3><p>An error occured. See details below:<br/>' + data + '</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>')
        }


    });


}
function DeleteResultSet(uid) {
    var sid = document.getElementById('sid').value;
    var setid = document.getElementById('setid').value;
    // show loading notifier
    SetLoading('Deleting...');
    var valxmlhttp = GetXmlHttpObject();
    if (valxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings 
    var url = "ajax_queries/ValidateResultSet.php";
    var param = "uid=" + uid;
    param = param + "&action=delete";
    param = param + "&setid=" + setid;
    param = param + "&sid=" + sid;
    param = param + "&rv=" + Math.random();
    valxmlhttp.onreadystatechange = function () {
        if (valxmlhttp.readyState == 4) {
            var response = valxmlhttp.responseText;
            if (response == 'OK') {
                // load the log file"
                var logurl = "ajax_queries/LoadLog.php?type=summary&sid=" + sid;
                logurl = logurl + "&rv=" + Math.random();
                var xmlhttpLOG = GetXmlHttpObject();
                if (xmlhttpLOG == null) {
                    alert("Browser does not support HTTP Request");
                    return;
                }
                // onchangestate function
                xmlhttpLOG.onreadystatechange = function () {
                    if (xmlhttpLOG.readyState == 4) {
                        document.getElementById('LogTab').innerHTML = xmlhttpLOG.responseText;
                    }
                }
                // SEND REQUEST WITH POST
                xmlhttpLOG.open("GET", logurl, true);
                xmlhttpLOG.send(null);
                // load the saved results file"
                surl = "ajax_queries/LoadStoredResults.php?sid=" + sid;
                surl = surl + "&rv=" + Math.random();
                var xmlhttpS = GetXmlHttpObject();
                if (xmlhttpS == null) {
                    alert("Browser does not support HTTP Request");
                    return;
                }
                // onchangestate function
                xmlhttpS.onreadystatechange = function () {
                    if (xmlhttpS.readyState == 4) {
                        document.getElementById('SavedTab').innerHTML = xmlhttpS.responseText;
                    }
                }
                // SEND REQUEST WITH POST
                xmlhttpS.open("GET", surl, true);
                xmlhttpS.send(null);

                $('#overlay').hide();
            }
            else {
                $('#overlay').html('<h3>ERROR</h3><p>An error occured during submission of the form. See details below:<br/>' + response + '</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
            }
        }
    };
    valxmlhttp.open("POST", url, true);
    valxmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    valxmlhttp.send(param);



}

// function to set validity and significance details in overlay.
function setValidity(vid, sid, uid) {
    // show loading notifier
    SetLoading('Loading...');
    valxmlhttp = GetXmlHttpObject();
    if (valxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings 
    var url = "ajax_queries/SetValidity.php";
    var param = "uid=" + uid;
    param = param + "&action=getform";
    param = param + "&vid=" + vid;
    param = param + "&sid=" + sid;
    param = param + "&rv=" + Math.random();
    valxmlhttp.onreadystatechange = function () {
        if (valxmlhttp.readyState == 4) {
            document.getElementById('overlay').innerHTML = valxmlhttp.responseText;
        }
    };
    valxmlhttp.open("POST", url, true);
    valxmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    valxmlhttp.send(param);

}
// function to set validity and significance details in overlay, for a saved page.
function setStoredValidity(vid, sid, uid, setid, page) {
    // show overlay
    SetLoading('Loading...');
    valxmlhttp = GetXmlHttpObject();
    if (valxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings are now directly from saved value.
    var current_values = document.getElementById('sv_' + vid).innerHTML;
    // settings 
    var url = "ajax_queries/SetStoredValidity.php";
    var param = "uid=" + uid;
    param = param + "&action=getform";
    param = param + "&vid=" + vid;
    param = param + "&sid=" + sid;
    param = param + "&setid=" + setid;
    param = param + "&page=" + page;
    param = param + "&cv=" + current_values
    param = param + "&rv=" + Math.random();
    valxmlhttp.onreadystatechange = function () {
        if (valxmlhttp.readyState == 4) {
            document.getElementById('overlay').innerHTML = valxmlhttp.responseText;
        }
    };
    valxmlhttp.open("POST", url, true);
    valxmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    valxmlhttp.send(param);

}

// function to add a variant to a classifier list;
function AddToClassifier(vid, sid, uid, saved = 0) {
    // show overlay
    SetLoading('Loading...');
    // get contents
    var url = "ajax_queries/AddToClassifier.php";
    var param = {
        action: 'getform',
        //uid : uid,
        vid: vid,
        sid: sid,
        saved: saved,
        lcb: 1 // load default checkboxes from user defaults
    };
    $.post(url, param, function (data) {
        // show form in overlay.
        $('#overlay').html(data);
    });


}

function ApproveClassifierVariant(action, vid, cid, uid, sid, saved = 0) {
    //SetLoading('Loading...');
    // get contents
    var url = "ajax_queries/AddToClassifier.php";
    var cbTable = $('#cb_vc_' + cid + '_' + vid).html();
    var param = {
        action: 'approve',
        //uid : uid,
        vid: vid,
        cid: cid,
        sid: sid,
        saved: saved,
        type: action,
        cb: cbTable,
    };
    $.post(url, param, function (data) {
        // show form in overlay.
        if (data == 'ok') {
            AddToClassifier(vid, sid, uid, saved)
        }
        else {
            $('#overlay').html(data);
        }
    });

}

// function to add a variant to a classifier list;
function AddSavedToClassifier(vid, sid, uid) {
    // show overlay
    SetLoading('Loading...');
    // load checkboxes in ajax if not on saved page.
    var load_cb = 1;
    if ($('#cb_table_' + vid).length > 0) {
        load_cb = 0;
    }
    // get contents
    var url = "ajax_queries/AddToClassifier.php";
    var param = {
        action: 'getform',
        //uid : uid,
        vid: vid,
        sid: sid,
        saved: 1,
        lcb: load_cb
    };
    $.post(url, param, function (data) {
        // show form in overlay.
        $('#overlay').html(data);
        // add the checkboxes
        if (load_cb == 0) {
            table = $('#cb_table_' + vid).clone().prop('id', 'cb_a2c_' + vid).prop('class', 'cb_a2c');// .appendTo('#cbContainer');
            cbs = table.find("input[type='checkbox']")
            // reset id to prevent clashes. reset disabled (because of saved page)
            for (i = 0; i < cbs.length; i++) {
                // reset checkbox id
                table.find("input[id='" + cbs[i].id + "']").prop('id', 'a2c_' + cbs[i].id);  // cannot use #id because of potential spaces
                // reset label for
                table.find("input[id='" + cbs[i].id + "']").next().attr({ 'for': cbs[i].id });
                // reset disabled
                table.find("input[id='" + cbs[i].id + "']").removeAttr("disabled");
                // formalize checked
                if (table.find("input[id='" + cbs[i].id + "']").is(':checked')) {
                    table.find("input[id='" + cbs[i].id + "']").attr('checked', 'checked');
                }
            }
            table.appendTo('#cbContainer');
            $('#cbContainer').show();
        }

    });


}

// function to set validity and significance details in overlay.
function AddVariantToClassifier(reload) {
    /// form variables
    var vid = $('#vid').val();
    var uid = $('#uid').val();
    var sid = $('#sid').val();
    var cid = $('select#add_to option:checked').val();
    var gt = $('select#GenoType option:checked').val();
    var cbtable = $('#cb_a2c_' + vid).html();
    //var sid = document.getElementById('sid').value;
    var vclass = $('select#VariantClass option:checked').val();
    var vinhm = $('select#VariantInhMode option:checked').val();

    var comments = document.getElementById('comments').value;
    var need_validation = (Number(document.getElementById('need_validation').checked));
    // show loading notifier
    SetLoading('Loading...');

    // settings 
    var url = "ajax_queries/AddToClassifier.php";
    var param = {
        uid: uid,
        action: 'Add',
        vid: vid,
        sid: sid,
        cid: cid,
        gt: gt,
        vclass: vclass,
        vinhm: vinhm,
        comments: comments,
        nv: need_validation,
        cb: cbtable
    }
    $.post(url, param, function (data) {
        if (data == 'OK') {
            if (need_validation == 1) {
                $('#overlay').html('<h3>Variant Added to classifier</h3><p>The people responsible for validation will be notified upon their next login.</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
            }
            else {
                $('#overlay').html('<h3>Variant Added to classifier</h3><p>Validation requirement was removed: variant is added and active.</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
            }
        }
        else {
            $('#overlay').html('<h3>ERROR</h3><p>An error occured during submission of the form. See details below:<br/>' + data + '</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
        }
    });
}
// function to set validity and significance details in overlay.
function AddSavedVariantToClassifier(reload) {
    /// form variables
    var vid = $('#vid').val();
    var uid = $('#uid').val();
    var sid = $('#sid').val();
    var cid = $('select#add_to option:checked').val();
    var gt = $('select#GenoType option:checked').val();
    var cbtable = $('#cb_a2c_' + vid).html();
    // saved data details
    var setid = $('#setid').val();
    var page = $('#pagenr').val();
    var vclass = $('select#VariantClass option:checked').val();
    var vinhm = $('select#VariantInhMode option:checked').val();

    var comments = document.getElementById('comments').value;
    var need_validation = (Number(document.getElementById('need_validation').checked));
    // show loading notifier
    SetLoading('Loading...');
    // settings
    var url = "ajax_queries/AddToClassifier.php";
    var param = {
        uid: uid,
        action: 'Add',
        vid: vid,
        sid: sid,
        cid: cid,
        gt: gt,
        vclass: vclass,
        vinhm: vinhm,
        comments: comments,
        nv: need_validation,
        cb: cbtable
    }
    $.post(url, param, function (data) {
        if (data == 'OK') {
            if (need_validation == 1) {
                $('#overlay').html('<h3>Variant Added to classifier</h3><p>The people responsible for validation will be notified upon their next login.</p><p><input type=submit value=Close class=button onClick="ShowStoredResults(\'' + uid + '\',\'' + sid + '\',\'' + setid + '\',\'' + page + '\',\'' + $(window).scrollTop() + '\');document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
            }
            else {
                $('#overlay').html('<h3>Variant Added to classifier</h3><p>Validation requirement was removed: variant is added and active.</p><p><input type=submit value=Close class=button onClick="ShowStoredResults(\'' + uid + '\',\'' + sid + '\',\'' + setid + '\',\'' + page + '\',\'' + $(window).scrollTop() + '\');document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
            }
        }
        else {
            $('#overlay').html('<h3>ERROR</h3><p>An error occured during submission of the form. See details below:<br/>' + data + '</p><p><input type=submit value=Close class=button onClick="ShowStoredResults(\'' + uid + '\',\'' + sid + '\',\'' + setid + '\',\'' + page + '\',\'' + $(window).scrollTop() + '\');document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
        }
    });
}

// toggle visibility and value of need_validation on AddToClassifier
function CheckSkipValidation(uid) {
    // selected classifier
    var cid = $('select#add_to option:checked').val();
    var url = "ajax_queries/AddToClassifier.php";
    var sid = $('#sid').val();
    var param = {
        uid: uid,
        action: 'CheckSkipVal',
        cid: cid,
        sid: sid,
    };
    $.post(url, param, function (data) {
        // allowed to skip.
        if (data == '1') {
            // show the box; do not change it.
            $('#SkipValRow').show();
        }
        else {
            // hide box; set to 'needed'
            $('#SkipValRow').hide();
            $('#need_validation')[0].checked = true;
        }
    });
}


// function to set validity and significance details in overlay.
function UpdateValidity(reload) {
    var offset = $(window).scrollTop();
    /// form variables
    var vid = document.getElementById('vid').value;
    var uid = document.getElementById('uid').value;
    var sid = document.getElementById('sid').value;
    var vclass = document.getElementById('VariantClass').options[document.getElementById('VariantClass').selectedIndex].value;
    var vinhm = document.getElementById('VariantInhMode').options[document.getElementById('VariantInhMode').selectedIndex].value;

    var vinh = document.getElementById('VariantInh').options[document.getElementById('VariantInh').selectedIndex].value;
    var vval = document.getElementById('VariantValidation').options[document.getElementById('VariantValidation').selectedIndex].value;
    var vvaldet = document.getElementById('Validationdetails').value;
    // show loading notifier
    SetLoading('Loading...');

    uvalxmlhttp = GetXmlHttpObject();
    if (uvalxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings 
    var url = "ajax_queries/SetValidity.php";
    var param = "uid=" + uid;
    param = param + "&action=Update";
    param = param + "&vid=" + vid;
    param = param + "&sid=" + sid;
    param = param + "&vclass=" + vclass;
    param = param + "&vinhm=" + vinhm;
    param = param + "&vinh=" + vinh;
    param = param + "&vval=" + vval;
    param = param + "&vvaldet=" + vvaldet;
    param = param + "&rv=" + Math.random();
    uvalxmlhttp.onreadystatechange = function () {
        if (uvalxmlhttp.readyState == 4) {
            var response = uvalxmlhttp.responseText;
            if (response == 'OK') {
                // load the log file"
                var logurl = "ajax_queries/LoadLog.php?type=summary&sid=" + sid;
                logurl = logurl + "&rv=" + Math.random();
                var xmlhttpLOG = GetXmlHttpObject();
                if (xmlhttpLOG == null) {
                    alert("Browser does not support HTTP Request");
                    return;
                }
                // onchangestate function
                xmlhttpLOG.onreadystatechange = function () {
                    if (xmlhttpLOG.readyState == 4) {
                        document.getElementById('LogTab').innerHTML = xmlhttpLOG.responseText;
                    }
                }
                // SEND REQUEST WITH POST
                xmlhttpLOG.open("GET", logurl, true);
                xmlhttpLOG.send(null);

                if (reload == 1) {
                    RunQuery('0', offset);
                }
                $('#overlay').hide();
            }
            else {
                $('#overlay').html('<h3>ERROR</h3><p>An error occured during submission of the form. See details below:<br/>' + response + '</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
            }
        }
    };
    uvalxmlhttp.open("POST", url, true);
    uvalxmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    uvalxmlhttp.send(param);

}
// function to set validity and significance details in overlay.
function UpdateStoredValidity() {
    var offset = $(window).scrollTop();
    /// form variables
    var vid = document.getElementById('vid').value;
    var uid = document.getElementById('uid').value;
    var sid = document.getElementById('sid').value;
    var setid = document.getElementById('setid').value;
    var page = document.getElementById('page').value;
    var current_values = document.getElementById('sv_' + vid).innerHTML;
    var vclass = document.getElementById('VariantClass').options[document.getElementById('VariantClass').selectedIndex].value;
    var vinhm = document.getElementById('VariantInhMode').options[document.getElementById('VariantInhMode').selectedIndex].value;

    var vinh = document.getElementById('VariantInh').options[document.getElementById('VariantInh').selectedIndex].value;
    var vval = document.getElementById('VariantValidation').options[document.getElementById('VariantValidation').selectedIndex].value;
    var vvaldet = document.getElementById('Validationdetails').value;
    // show loading notifier
    SetLoading('Loading...');

    uvalxmlhttp = GetXmlHttpObject();
    if (uvalxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // settings 
    var url = "ajax_queries/SetStoredValidity.php";
    var param = "uid=" + uid;
    param = param + "&action=Update";
    param = param + "&vid=" + vid;
    param = param + "&sid=" + sid;
    param = param + "&vclass=" + vclass;
    param = param + "&vinhm=" + vinhm;
    param = param + "&vinh=" + vinh;
    param = param + "&vval=" + vval;
    param = param + "&vvaldet=" + vvaldet;
    param = param + "&setid=" + setid;
    param = param + "&page=" + page;
    param = param + "&cv=" + current_values;
    param = param + "&rv=" + Math.random();
    uvalxmlhttp.onreadystatechange = function () {
        if (uvalxmlhttp.readyState == 4) {
            var response = uvalxmlhttp.responseText;
            if (response == 'OK') {
                // load the log file"
                var logurl = "ajax_queries/LoadLog.php?type=summary&sid=" + sid;
                logurl = logurl + "&rv=" + Math.random();
                var xmlhttpLOG = GetXmlHttpObject();
                if (xmlhttpLOG == null) {
                    alert("Browser does not support HTTP Request");
                    return;
                }
                // onchangestate function
                xmlhttpLOG.onreadystatechange = function () {
                    if (xmlhttpLOG.readyState == 4) {
                        document.getElementById('LogTab').innerHTML = xmlhttpLOG.responseText;
                    }
                }
                // SEND REQUEST WITH POST
                xmlhttpLOG.open("GET", logurl, true);
                xmlhttpLOG.send(null);

                //reload 
                ShowStoredResults(uid, sid, setid, page, offset);
                // set scrollTop 
                $('#overlay').hide();
            }
            else {
                $('#overlay').html('<h3>ERROR</h3><p>An error occured during submission of the form. See details below:<br/>' + response + '</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>');
            }
        }
    };
    uvalxmlhttp.open("POST", url, true);
    uvalxmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    uvalxmlhttp.send(param);

}

function ShowStoredResults(uid, sid, setid, page, offset = 0) {
    // if page not specified, check the selected value.
    if (typeof page === 'undefined') {
        var list = document.getElementById("stored_page_select");
        page = list.options[list.selectedIndex].value;
    }
    // put loading icon in target div
    document.getElementById('QueryResults').innerHTML = "<div class='w100' style='text-align:center'><img src='Images/layout/ajax-loader.gif' ><br/>Loading Results...<br/></div>";

    // settings 
    var url = "ajax_queries/LoadStoredpage.php";
    var param = {
        uid: uid,
        sid: sid,
        setid: setid,
        page_idx: page,
    };
    $.post(url, param, function (data) {
        document.getElementById('QueryResults').innerHTML = data;
        $(window).scrollTop(offset);
    });
}
// launch rerun of stored result set.
function ReRunStoredResult(uid, sid, setid) {
    // construct overlay
    SetLoading('ReApplying saved settings...');
    ToggleTab('FilterTab');
    // load settings 
    var url = "ajax_queries/ReRunStoredResults.php";
    $.get(url, { uid: uid, setid: setid, get: 'rules', strip: 1 }, function (data) {
        if (data.status == 'Error') {
            alert("Failed to load Results:\n" + data.msg);
            document.getElementById('overlay').style.display = 'none';
            return;
        }
        // skip storing cookies while processing
        setCookie('skipCookieSave', '1', 1);
        // clear existing filters.
        ClearFiltersAndAnnotations();
        // add rules.
        $.each(data.rules, function (index, rule) {
            var c = rule.c;
            var v = rule.v;
            var tbody = document.getElementById("FilterBy" + c);
            var row = tbody.insertRow(-1);
            row.id = "FilterStep" + index;
            // get number of cells (from 0th row = header row)
            var cells = document.getElementById("querytable").rows[0].cells.length;
            // add cells to the row.
            var i;
            for (i = 0; i < cells; i++) {
                var newCell = row.insertCell(i);
            }
            // negate cell
            if (c === 'Dynamic_Filters') {
                row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;margin-bottom:-0.2em'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "' style='display:none'><option value='' SELECTED>Match</option><option value='NOT'>Not Match</option></select>Match";
                row.cells[1].innerHTML = v[1];
                row.cells[2].innerHTML = v[2];
                row.cells[3].innerHTML = v[3];
            }
            else {
                if (v[0] === "") {
                    row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "'><option value=''>Match</option><option value='NOT'>Not Match</option></select>";
                }
                else {
                    row.cells[0].innerHTML = "<span style='margin-right:1em;' onClick=\"RemoveFilter('" + index + "')\"><img src='Images/layout/minus-icon.png' style='height:1em;'></span><input type=hidden id='FilterCategory" + index + "' value='" + c + "'><select id='negate" + index + "'><option value=''>Match</option><option value='NOT' selected>Not Match</option></select>";
                }
                // level 1
                row.cells[1].innerHTML = v[1].result; // + '<span id=ss' + index + '></span>';
                // level 2
                row.cells[2].innerHTML = v[2].result;
                // level 3
                if (v[2].result !== v[3].result) {
                    row.cells[3].innerHTML = v[3].result;
                }
            }

        });
        // build logic tree
        var ref = $('#filter_tree').jstree(true)
        sel = 'j1_1';
        try {
            var tree = JSON.parse(data.jstree)[0];
        } catch (error) {
            alert("Failed to reload filter. Please inform admin : \n " + error);
            throw error;
        }
        var roottype = tree.type
        var newbranch = AddChild(roottype, 'j1_1');
        var RuleIDs = ParseLogicNew(tree, newbranch);
        ref.open_all();
        // set nr of rules.
        listed = data.listed;
        // set annotations
        processLoadedAnnotation(data.Anno);
        // update cookies.
        deleteCookie('skipCookieSave');
        // clear 'loaded filters' 
        document.getElementById('Loaded_Filter').innerHTML = '';
        FiltersToCookies(listed);
        SetOverlay('none');
        RunQuery('0');


    }, "json");

}
// reload the filter tree of a saved_rerun query.
function LoadStoredResultsTree(uid, sid, setid, rules) {
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    url = "ajax_queries/LoadStoredResultsDetails.php";
    url = url + "?uid=" + uid;
    url = url + "&setid=" + setid;
    url = url + "&get=tree";
    url = url + "&rv=" + Math.random();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // logic tree is passed back.
            var tree = xmlhttp.responseText;
            // now next function.
            setTimeout(LoadStoredResultsAnno, 0, uid, sid, setid, rules, tree);
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
}

function LoadStoredResultsAnno(uid, sid, setid, rules, tree) {
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    url = "ajax_queries/LoadStoredResultsDetails.php";
    url = url + "?uid=" + uid;
    url = url + "&setid=" + setid;
    url = url + "&get=anno";
    url = url + "&rv=" + Math.random();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            // annotations are passed back.
            var annotationstring = xmlhttp.responseText;
            //var annotations = annotationstring.split("@@@");

            // now next functions.
            setTimeout(ClearFiltersAndAnnotations, 0);
            setTimeout(processLoadedFilter, 0, rules, tree);
            setTimeout(processLoadedAnnotation, 0, annotationstring);

            document.getElementById('overlay').style.display = 'none';
            setTimeout(RunQuery, 0, '0');

        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
}

function ClearFiltersAndAnnotations() {
    // filters
    for (var j = 0; j <= listed; j++) {
        if (document.getElementById("FilterStep" + j)) {
            RemoveFilter(j);
        }
    }
    // annotations.
    for (var i = 0; i < document.annotationform.annotations.length; i++) {
        document.annotationform.annotations[i].checked = false;
    }
    // logic tree.
    var ref = $('#filter_tree').jstree(true)
    sel = 'j1_1'
    var children = ref.get_children_dom(sel)
    if (children.length > 0) {
        // add the child node.
        if (ref.get_node(sel).type !== 'logic_AND') {
            // set root to AND
            ref.set_type(sel, 'logic_AND');
            ref.set_text(sel, "AND (keep if complying with all rules)");
        }
        // move all original children to new child.
        for (idx = 0; idx < children.length; idx++) {
            var child = children[idx].id;
            RemoveNode(child)
        }

    }
}
function ClearFilters() {
    // filters
    for (var j = 0; j <= listed; j++) {
        if (document.getElementById("FilterStep" + j)) {
            RemoveFilter(j);
        }
    }
    // logic tree.
    var ref = $('#filter_tree').jstree(true)
    sel = 'j1_1'
    var children = ref.get_children_dom(sel)
    if (children.length > 0) {
        // add the child node.
        if (ref.get_node(sel).type !== 'logic_AND') {
            // set root to AND
            ref.set_type(sel, 'logic_AND');
            ref.set_text(sel, "AND (keep if complying with all rules)");
        }
        // move all original children to new child.
        for (idx = 0; idx < children.length; idx++) {
            var child = children[idx].id;
            RemoveNode(child)
        }

    }
}


function LoadFilteringTree() {
    // jsTREE functionality to create filtering logic
    $(function () {
        //  create an instance when the DOM is ready
        $('#filter_tree').jstree({
            "core": {
                "check_callback": function (operation, node, node_parent, node_position, more) {
                    // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
                    // in case of 'rename_node' node_position is filled with the new node name
                    if (operation === "move_node") {
                        if (node_parent.type.substring(0, 5) === "logic") {  // allow dropping inside nodes of type 'logic'
                            return true;
                        }
                        else if (node.type.substring(0, 5) === "logic" && node_parent.type === '#') {
                            return true;
                        }
                    }
                    return true;  //allow all other operations
                },
                "multiple": false,
                //"data": [contents]
            },
            "dnd": {
                "check_while_dragging": true
            },
            "types": {
                "#": {
                    "valid_children": ["logic_AND", "logic_OR", "logic_NOR", "logic_NAND"],
                    "max_children": 1,
                },
                "default": {
                    "valid_children": [],
                    "icon": "Images/layout/filter.png",
                },
                "logic_AND": {
                    "icon": "Images/layout/AND.png",
                    "valid_children": ["logic_AND", "logic_OR", "default", "logic_NOR", "logic_NAND"],
                },
                "logic_OR": {
                    "icon": "Images/layout/OR.png",
                    "valid_children": ["logic_AND", "logic_OR", "default", "logic_NOR", "logic_NAND"],
                },
                "logic_NAND": {
                    "icon": "Images/layout/NAND.png",
                    "valid_children": ["logic_AND", "logic_OR", "default", "logic_NOR", "logic_NAND"],
                },
                "logic_NOR": {
                    "icon": "Images/layout/NOR.png",
                    "valid_children": ["logic_AND", "logic_OR", "default", "logic_NOR", "logic_NAND"],
                }
            },
            "plugins": ["dnd", "types", "state"],
        });

        $.jstree.defaults.dnd.copy = 'false';
        //  bind to events triggered on the tree
        $('#filter_tree').on("changed.jstree", function (e, data) {
            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"))
            if (getCookie('skipCookieSave') != 1) {
                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }

        });
        $('#filter_tree').on("create_node.jstree", function (e, data) {
            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));

            if (getCookie('skipCookieSave') != 1) {

                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }
        });
        $('#filter_tree').on("rename_node.jstree", function (e, data) {
            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));

            if (getCookie('skipCookieSave') != 1) {
                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }
        });
        $('#filter_tree').on("delete_node.jstree", function (e, data) {
            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
            ;
            if (getCookie('skipCookieSave') != 1) {

                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }

        });

        $('#filter_tree').bind('move_node.jstree', function (e, data) {
            $('#filter_tree').jstree('open_node', data.parent)

            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
            if (getCookie('skipCookieSave') != 1) {
                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }

        });

    });
}

function LoadFilteringTreeWithContents(contents) {
    // jsTREE functionality to create filtering logic
    $(function () {
        //  create an instance when the DOM is ready
        $('#filter_tree').jstree({
            "core": {
                "check_callback": function (operation, node, node_parent, node_position, more) {
                    // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
                    // in case of 'rename_node' node_position is filled with the new node name
                    if (operation === "move_node") {
                        if (node_parent.type.substring(0, 5) === "logic") {  // allow dropping inside nodes of type 'logic'
                            return true;
                        }
                        else if (node.type.substring(0, 5) === "logic" && node_parent.type === '#') {
                            return true;
                        }
                    }
                    return true;  //allow all other operations
                },
                "multiple": false,
                "data": JSON.parse(contents)[0]
                //"data":contents
            },
            "dnd": {
                "check_while_dragging": true
            },
            "types": {
                "#": {
                    "valid_children": ["logic_AND", "logic_OR", "logic_NOR", "logic_NAND"],
                    "max_children": 1,
                },
                "default": {
                    "valid_children": [],
                    "icon": "Images/layout/filter.png",
                },
                "logic_AND": {
                    "icon": "Images/layout/AND.png",
                    "valid_children": ["logic_AND", "logic_OR", "default", "logic_NOR", "logic_NAND"],
                },
                "logic_OR": {
                    "icon": "Images/layout/OR.png",
                    "valid_children": ["logic_AND", "logic_OR", "default", "logic_NOR", "logic_NAND"],
                },
                "logic_NAND": {
                    "icon": "Images/layout/NAND.png",
                    "valid_children": ["logic_AND", "logic_OR", "default", "logic_NOR", "logic_NAND"],
                },
                "logic_NOR": {
                    "icon": "Images/layout/NOR.png",
                    "valid_children": ["logic_AND", "logic_OR", "default", "logic_NOR", "logic_NAND"],
                }
            },
            "plugins": ["dnd", "types", "state"],
            //"cookies" : { prefix : "filter_tree", opts: {path : '/' }}
        });
        $.jstree.defaults.dnd.copy = 'false';
        //  bind to events triggered on the tree
        $('#filter_tree').on("changed.jstree", function (e, data) {
            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
            if (getCookie('skipCookieSave') !== '1') {
                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }
        });
        $('#filter_tree').on("create_node.jstree", function (e, data) {
            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
            if (getCookie('skipCookieSave') !== '1') {
                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }
        });
        $('#filter_tree').on("rename_node.jstree", function (e, data) {
            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
            if (getCookie('skipCookieSave') !== '1') {
                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }
        });
        $('#filter_tree').on("delete_node.jstree", function (e, data) {
            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
            if (getCookie('skipCookieSave') !== '1') {
                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }
        });

        $('#filter_tree').bind('move_node.jstree', function (e, data) {
            $('#filter_tree').jstree('open_node', data.parent)

            var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
            var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
            if (getCookie('skipCookieSave') !== '1') {
                setDbCookie('FS', treestate, 1, true)
                setDbCookie('FCs', treecontents, 1, true)
            }
        });
    });
}




function switchType(type, sel) {
    var ref = $('#filter_tree').jstree(true);
    if (typeof sel === "undefined") {
        sel = ref.get_selected();
        if (!sel.length) {
            return false;
        }
        else {
            sel = sel[0];
        }
    }
    // type: 
    //var type = ref.get_type(sel)
    if (type === 'OR') {
        ref.set_type(sel, 'logic_OR');
        ref.set_text(sel, "OR (keep if complying with at least one rule)");
    }
    else if (type === 'AND') {
        ref.set_type(sel, 'logic_AND');
        ref.set_text(sel, "AND (keep if complying with all rules)");
    }
    else if (type === 'NOR') {
        ref.set_type(sel, 'logic_NOR');
        ref.set_text(sel, "NOR (discard if complying with at least one rule)");
    }
    else if (type === 'NAND') {
        ref.set_type(sel, 'logic_NAND');
        ref.set_text(sel, "NAND (discard if complying with all rules)");
    }

    var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
    var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
    setDbCookie('FS', treestate, 1, true)
    setDbCookie('FCs', treecontents, 1, true)

}

function AddRule(type) {
    var ref = $('#filter_tree').jstree(true), sel = ref.get_selected();
    if (!sel.length) {
        // select root
        //sel = ref.get_node('#')
        sel = 'j1_1';
    }
    else {
        sel = sel[0];
    }
    if (ref.get_node(sel).type.substring(0, 5) != 'logic') {
        sel = 'j1_1';
    }
    if (type === 'AND') {
        sel = ref.create_node(sel, { "type": "logic_AND" });
        ref.open_node(ref.get_parent(sel))
        if (sel) {
            ref.set_text(sel, 'AND (keep if complying with all rules)');
        }
    }
    else if (type === 'OR') {
        sel = ref.create_node(sel, { "type": "logic_OR" });
        ref.open_node(ref.get_parent(sel))

        if (sel) {
            ref.set_text(sel, 'OR (keep if complying with at least one rule)');
        }
    }
    else if (type === 'NAND') {
        sel = ref.create_node(sel, { "type": "logic_NAND" });
        ref.open_node(ref.get_parent(sel))

        if (sel) {
            ref.set_text(sel, 'NAND (discard if complying with all rules)');
        }
    }
    else if (type === 'NOR') {
        sel = ref.create_node(sel, { "type": "logic_NOR" });
        ref.open_node(ref.get_parent(sel))

        if (sel) {
            ref.set_text(sel, 'NOR (discard if complying with at least one rule)');
        }
    }

    var treecontents = JSON.stringify($('#filter_tree').jstree("get_json"))
    var treestate = JSON.stringify($('#filter_tree').jstree("get_state"));
    setDbCookie('FS', treestate, 1, true)
    setDbCookie('FCs', treecontents, 1, true)

};

// the fixed header for wide_table output.
var myHeader;
function getScroll() {
    var b = document.body;
    var e = document.documentElement;
    return {
        left: parseFloat(window.pageXOffset || b.scrollLeft || e.scrollLeft),
        top: parseFloat(window.pageYOffset || b.scrollTop || e.scrollTop)
    };
}

////////////////////////////////////////
// STORE A SET OF FILTERS TO DATABASE //
////////////////////////////////////////
function SaveAnnotations(uid) {
    var nrchecks = document.annotationform.annotations.length;
    var param = 'StoreWhat=Annotation&uid=' + uid;
    var content = '';
    for (var idx = 0; idx < nrchecks; idx++) {
        if (document.annotationform.annotations[idx].checked) {
            content += document.annotationform.annotations[idx].value + '@@@';
        }
    }
    if (content == '') {
        alert('No annotations were selected.');
        return (1);
    }
    param = param + "&selected=" + encodeURIComponent(content.substr(0, (content.length - 3)));
    // compose overlay content
    var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Save Annotation Selection</span>";
    newcontent += '<p class=white>Provide a name to save the selection under:<br/>';
    newcontent += '<input  type=hidden name="params" id=SaveParams value="' + param + '"><input type=text id=SaveAs name=SaveAs size=30 style="margin-left:1em;">';
    newcontent += '</p>';
    newcontent += '<p class=white>Additional Comments:<br/>';
    newcontent += '<input type=text id=SaveComments name=SaveComments size=30 style="margin-left:1em;">';
    newcontent += '</p>';
    newcontent += '<p><input type=submit value="Save Selection" onClick="StoreAnnotations(\'' + uid + '\',getAnnotations)"> &nbsp; <input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
    newcontent += '</div>';
    $('#overlay').html(newcontent);
    $('#overlay').show();

}
function StoreAnnotations(uid, callback) {
    var url = "ajax_queries/StoreSettings.php";
    // set the variables
    param = document.getElementById('SaveParams').value;
    param += "&SaveAs=" + document.getElementById('SaveAs').value;
    param += "&SaveComments=" + document.getElementById('SaveComments').value;
    // create the ajax call
    xmlhttpSA = GetXmlHttpObject();
    if (xmlhttpSA == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttpSA.onreadystatechange = function () {
        if (xmlhttpSA.readyState == 4) {
            callback(uid);
            document.getElementById('overlay').style.display = 'none';
        }
    }
    // SEND REQUEST WITH POST
    xmlhttpSA.open("POST", url, true);
    xmlhttpSA.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttpSA.send(param);
}


////////////////////////////////////////
// STORE A SET OF FILTERS TO DATABASE //
////////////////////////////////////////
function StoreFilter(uid, callback) {
    var url = "ajax_queries/StoreSettings.php";
    // set the variables
    param = document.getElementById('SaveParams').value;
    param += "&SaveAs=" + document.getElementById('SaveAs').value;
    param += "&SaveComments=" + document.getElementById('SaveComments').value;
    //param += "&FilterLogic="+document.getElementById('FilterLogic').value;
    // create the ajax call
    xmlhttpSF = GetXmlHttpObject();
    if (xmlhttpSF == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttpSF.onreadystatechange = function () {
        if (xmlhttpSF.readyState == 4) {
            callback(uid);
            document.getElementById('overlay').style.display = 'none';
        }
    }
    // SEND REQUEST WITH POST
    xmlhttpSF.open("POST", url, true);
    xmlhttpSF.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttpSF.send(param);


}
////////////////////////////////////////
// STORE A SET OF RESULTS TO DATABASE //
////////////////////////////////////////
/////////////////////////
// SAVE FILTER RESULTS //
/////////////////////////
function SaveResults(uid) {
    // prepare
    FiltersToCookies(listed);
    // check access to the sample & get overlay to select checkboxes.
    var params = GetQuerySettings(uid);
    // COMPOSE OVERLAY to set name & checkboxes + check for SummaryStatus.
    $.post("ajax_queries/GetCheckBoxes.php", params, function (data) {
        if (data.status == 'error') {
            var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>ERROR !</span>";
            newcontent += '<p class="white strong">Could not save results. More information:<p><p class=white>';
            newcontent += data.error;
            newcontent += '</p><p><input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></div>';
        }
        else {
            var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Save Filter Results</span>";
            newcontent += '<p class="white strong">Provide a name to save the results under:<p><p class=white>';
            newcontent += '<input  type=hidden name="params" id=SaveParams value="">';
            newcontent += '<input type=text id=SaveAs name=SaveAs size=30 style="margin-left:1em;">';
            newcontent += '</p>';
            newcontent += '<p class="white strong">Additional Comments:</p><p class=white>';
            newcontent += '<input type=text id=SaveComments name=SaveComments size=30 style="margin-left:1em;">';
            newcontent += '</p>';
            newcontent += '<p class="white strong">Add checkboxes to results:</p><p class=white>' + data.checkboxes + '</p>';
            // warning about frequency
            if (data.frequencies != 'up-to-date') {
                disabled = 'disabled';
                newcontent += '<p class="white strong">Variant Frequency data is not up-to-date. This might affect results. <input type=checkbox id="vf_ackn" onchange=\'document.getElementById("SubmitStoreResults").disabled = !this.checked;\'> Acknowledge</p>';
            } else {
                disabled = '';
                newcontent += "<input type=hidden id=vf_ok value=1>";
            }
            newcontent += '<p><input type=submit value="Save Results" id=SubmitStoreResults ' + disabled + ' onClick="StoreResults(\'' + uid + '\',RunQuery)"> &nbsp; <input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
            newcontent += '</div>';

        }
        $('#overlay').html(newcontent);
        // set parameters using .val() to protect quotes
        // show
        $('#overlay').show();
    }, 'json');

}


function StoreResults(uid, callback) {
    var url = "ajax_queries/StoreResults.php";
    var params = GetQuerySettings(uid);
    // set the variables
    params["SaveAs"] = document.getElementById('SaveAs').value;
    params["SaveComments"] = document.getElementById('SaveComments').value;
    // selected checkboxes
    var checkboxes = $("#overlay input[name='cb']:checkbox:checked").map(function () {
        return $(this).val();
    }).get().join(',');
    params["cb"] = checkboxes;
    // acknowledged outdated frequencies.
    if (!$('#vf_ok').length > 0) {
        console.log("vf_ok is not present. check for acknowledgement")
        // submission is only possible when checked, but this is a double check.
        if (!$('#vf_ackn').prop('checked')) {
            alert("You must acknowledge out-dated frequency data to continue.")
            return;
        }
        // add to params.
        params["vf_ackn"] = 1;
    }
    // call to create the saved result & get its id. 
    $.post(url, params, function (set_id) {
        callback('save|' + set_id);
        $("#overlay").hide();
    }, "text")
        .fail(function (error) {
            $("#QueryResults").html("<div class='w100'><h3>Query Failed</h3><p>Could not launch query, please report. Returned error was : </p><p>" + error + "</p></div>");
            $("#overlay").hide();
        });

}

/////////////////////////////////////////////////////////////
// DELETE SETTINGS FROM DATABASE (annotations/filters/...) //
/////////////////////////////////////////////////////////////
function DeleteSetting(what, id, uid, callback) {
    if (confirm("Please Confirm Action!") == true) {
        var url = "ajax_queries/DeleteSettings.php";
        // set the variables
        url += "?DeleteWhat=" + what;
        url += "&id=" + id;
        url += "&uid=" + uid;
        url = url + "&rv=" + Math.random();
        // create the ajax call
        xmlhttpDS = GetXmlHttpObject();
        if (xmlhttpDS == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        // onchangestate function
        xmlhttpDS.onreadystatechange = function () {
            if (xmlhttpDS.readyState == 4) {
                if (xmlhttpDS.responseText == 1) {
                    callback(uid);
                }
                else {
                    alert(xmlhttpDS.responseText);
                }
            }
        }
        // SEND REQUEST WITH POST
        xmlhttpDS.open("GET", url, true);
        xmlhttpDS.send(null);
    }
}
////////////////////////////////////////////////////
// SHARE saved settings (annotations/filters/...) //
////////////////////////////////////////////////////
function ShareSetting(what, id, uid) {
    var url = "ajax_queries/ShareSettings.php?";
    var pars = {
        ShareWhat: what,
        id: id,
        uid: uid,
        rv: Math.random()
    };
    document.getElementById('overlay').style.display = '';
    $('#overlay').load(url + $.param(pars));

}
function UnShareSettings(what, id, uid, shareuid, type) {
    var url = "ajax_queries/UnShareSettings.php?";
    // set the variables
    var pars = {
        UnShareWhat: what,
        SharedWith: shareuid,
        id: id,
        uid: uid,
        type: type,
        rv: Math.random()
    }
    $.get(url + $.param(pars), function (data) {
        ShareSetting(what, id, uid);
    });
}


function ShareWithUser(what, id, uid, shareuid) {
    var url = "ajax_queries/ShareSettings.php";
    // set the variables
    url += "?ShareWhat=" + what;
    url += "&ShareWith=" + shareuid;
    url += "&id=" + id;
    url += "&uid=" + uid;
    url = url + "&rv=" + Math.random();
    // create the ajax call
    var xmlhttpSW = GetXmlHttpObject();
    if (xmlhttpSW == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttpSW.onreadystatechange = function () {
        if (xmlhttpSW.readyState == 4) {
            if (xmlhttpSW.responseText == 1) {
                //callback(uid);
                ShareSetting(what, id, uid);
            }
            else {
                alert("ERROR, Please REPORT\n" + xmlhttpSW.responseText);
            }
        }
    }
    // SEND REQUEST WITH POST
    xmlhttpSW.open("GET", url, true);
    xmlhttpSW.send(null);



}
function ShareWithGroup(what, id, uid, sharegid) {
    var url = "ajax_queries/ShareSettings.php?";
    // set the variables
    var pars = {
        ShareWhat: what,
        ShareWith: 'group',
        uid: uid,
        gid: sharegid,
        id: id,
        rv: Math.random()
    }
    $.get(url + $.param(pars), function (data) {
        ShareSetting(what, id, uid);
    });
}

function SamplesWithVariant(vid, uid, sid) {
    // loading ...
    SetLoading('Loading...');
    // fetch.
    var url = "ajax_queries/SamplesWithVariant.php";
    var pars = {
        uid: uid,
        vid: vid,
        sid: sid
    };
    $.post(url, pars, function (data) {
        $('#overlay').html(data);
    });
}

function ToggleSamplesWithVariant(gid, vid = Null, uid = Null) {
    // Detail Sections
    // hide others.
    var list = ['HomRef', 'Het', 'HomAlt'];
    for (var i = 0; i < list.length; i++) {
        if (list[i] != gid) {
            $("#tbody_" + list[i]).hide()
            $("#head_" + list[i]).removeClass('headsel');
        }
    }
    // show a details table
    if (list.includes(gid)) {
        // hide summary.
        $("#head_summary").removeClass('headsel');
        $("#summary_table").hide();
        // show correct tbody.
        $("#tbody_" + gid).show()
        $("#details_table").show()
        $("#head_" + gid).addClass('headsel');
        // if not loaded yet, load now
        if ($("#loaded_" + gid).val() == '0') {
            var pars = {
                uid: uid,
                vid: vid,
                altcount: gid
            };
            var url = "ajax_queries/SamplesWithVariantByGenotype.php";
            $.post(url, pars, function (data) {
                $('#tbody_' + gid).html(data);
                $("#loaded_" + gid).val('1')
            });
        }
    }
    // else : show summary.
    else if (gid == 'Summary') {
        // set selected head.
        $("#head_summary").addClass('headsel');
        // hide details
        $("#details_table").hide();
        // show summary
        $("#summary_table").show();
    }
}
// dynamically assign listener to checkboxes in saved results.
$(document).on('click', '.NoteBox', function (event) {
    var cb_id = event.target.id;
    var checked = event.target.checked ? 1 : 0

    // work locally if running from the AddToClassifier overlay.
    if (cb_id.substr(0, 3) == 'a2c' || cb_id.substr(0, 2) == 'ic') {
        if (checked) {
            $("input[id='" + cb_id + "']").attr('checked', 'checked')
        }
        else {
            $("input[id='" + cb_id + "']").removeAttr('checked')
        }

        return;
    }
    // format : "(a2c_)variantID_setID_ceckboxListID_value"
    var url = "ajax_queries/ToggleStoredCheckbox.php";
    // set the variables
    var pars = {
        id: cb_id,
        v: checked,
        //uid:uid,
        p: $("#pagenr").val()
    }
    $.post(url, pars, function (data) {
        // error handling? 
        if (data != 'OK') {
            $("#overlay").html('<h3>ERROR</h3><p>An error occured during processing your request. See details below:<br/>"' + data + '"</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>')
            $("#overlay").show();
        }

    });
});

function UpdateSummary(type, uid) {
    if (type == 'User') {
        $.get('ajax_queries/UpdateSummary.php', { type: 'u', uid: uid }, function (data) {
            if (data.status == 'Error') {
                alert("Failed to queue summarization:\n" + data.msg);
                return;
            }
            // clear note.
            $('#updateUserSummary').hide();
        }, 'json');

        return;
    }
    if (type == 'Project') {
        $.get('ajax_queries/UpdateSummary.php', { type: 'p', uid: uid }, function (data) {
            if (data.status == 'Error') {
                alert("Failed to queue summarization:\n" + data.msg);
                return;
            }
            // clear note.
            $('#updateProjectSummary').hide();
        }, 'json');
        return;
    }
    // do nothing if no/wrong type.
    return;
}

function LoadQueryLog(filepath) {
    // set notifier
    $("#QueryLog").html('<img class=indent src="Images/layout/loader-bar.gif" />');
    $.get("ajax_queries/getFileContent.php", { filename: filepath }, function (data) {
        $("#QueryLog").html("<pre style='margin-left:15px;padding-left:5px;border-left:solid 1px #000'>" + data.replace(/(warning|error)/gi, '<span class="red">$1</span>') + "</pre>");
    });
}

function LoadStoredQueryLog(uid, setid) {
    // set notifier
    // put loading icon in target div
    $('#QueryResults').html("<div class='w100' style='text-align:center'><img src='Images/layout/ajax-loader.gif' ><br/>Loading Results...<br/></div>");
    // settings 
    var url = "ajax_queries/LoadStoredQueryLog.php";
    var param = {
        uid: uid,
        setid: setid,
    };
    $.get(url, param, function (data) {
        // highlighting routine 
        $("#QueryResults").html(data.replace(/(warning|error)/gi, '<span class="red">$1</span>'));
        //$('#QueryResults').html(data);
    });

}

function ShowResults(uid, qid) {
    // get selected value
    var page = $('#setPage').val()
    var url = "ajax_queries/Load_Query_Results.php"
    var param = {
        uid: uid,
        qid: qid,
        slicestart: page
    }
    $.get(url, param, function (data) {
        $('#QueryResults').html(data);
    })
}
