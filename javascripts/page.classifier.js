function AddUsers(userid) {
	var target = document.getElementById('ownerrow');
	var AddUserID = document.getElementById('AddUserID').value;
	if (AddUserID === '') {
		document.getElementById('comment_field').innerHTML="Select a user by typing the name in the search box.";
		//alert('Select a user by typing the name in the search box.');
		return;
	}
	// listed users. => don't add twice.
	var users = document.getElementsByName("add_users[]");
	var ulen = users.length;
	for(k=0;k<ulen;k++) {
		if (users[k].value === AddUserID) {
			// new user already listed.
			document.getElementById('comment_field').innerHTML="User is already listed.";
			
			return;
		}
	}
	var AddUserName = document.getElementById('searchfield').value;
	var row = document.createElement('tr');
	target.parentNode.insertBefore(row, target);
	var c_user = row.insertCell(0);
	c_user.innerHTML="<input type=hidden name='add_users[]' value='"+AddUserID+"'/>"+AddUserName;
	var c_add = row.insertCell(1);
	c_add.innerHTML="<input type='checkbox' name='can_add[]' value='"+AddUserID+"'/>";
	var c_val = row.insertCell(2);
	c_val.innerHTML="<input type='checkbox' name='can_validate[]' value='"+AddUserID+"'/>";
	var c_del = row.insertCell(3);
	c_del.innerHTML="<input type='checkbox' name='can_remove[]' value='"+AddUserID+"'/>";
	var c_use = row.insertCell(4);
	c_use.innerHTML="<input type='checkbox' name='can_use[]' value='"+AddUserID+"' checked/>";
	var c_sha = row.insertCell(5);
	c_sha.innerHTML="<input type='checkbox' name='can_share[]' value='"+AddUserID+"' />";
	document.getElementById('comment_field').innerHTML=AddUserName+" added. Set permissions below and submit changes.";
	
	
}
function AddGroup(userid) {
	var target = document.getElementById('ownerrowGroup');
	// selected group.
	var AddGroupID = $('#AddGroupSelect').val();
	if (AddGroupID === '' || AddGroupID === null) {
		document.getElementById('group_comment_field').innerHTML="Select a group.";
		//alert('Select a user by typing the name in the search box.');
		return;
	}
	// listed groups. => don't add twice.
	var groups = document.getElementsByName("add_groups[]");
	var glen = groups.length;
	for(k=0;k<glen;k++) {
		if (groups[k].value === AddGroupID) {
			// new group already listed.
			document.getElementById('group_comment_field').innerHTML="Group is already listed.";
			return;
		}
	}
	var AddGroupName = $('#AddGroupSelect :selected').text();
	var row = document.createElement('tr');
	target.parentNode.insertBefore(row, target);
	var c_user = row.insertCell(0);
	c_user.innerHTML="<input type=hidden name='add_groups[]' value='"+AddGroupID+"'/>"+AddGroupName;
	var c_add = row.insertCell(1);
	c_add.innerHTML="<input type='checkbox' name='g_can_add[]' value='"+AddGroupID+"'/>";
	var c_val = row.insertCell(2);
	c_val.innerHTML="<input type='checkbox' name='g_can_validate[]' value='"+AddGroupID+"'/>";
	var c_del = row.insertCell(3);
	c_del.innerHTML="<input type='checkbox' name='g_can_remove[]' value='"+AddGroupID+"'/>";
	var c_use = row.insertCell(4);
	c_use.innerHTML="<input type='checkbox' name='g_can_use[]' value='"+AddGroupID+"' checked/>";
	var c_sha = row.insertCell(5);
	c_sha.innerHTML="<input type='checkbox' name='g_can_share[]' value='"+AddGroupID+"' />";
	document.getElementById('group_comment_field').innerHTML=AddGroupName+" added. Set permissions below and submit changes to finalize.";
	
	
}


// autocomplete for search
var options = {
	script:"ajax_queries/usersearch.php?json=true&limit=10&",
	varname:"input",
	json:true,
	shownoresults:true,
	delay:200,
	cache:false,
	timeout:10000,
	minchars:2,
	callback: function (obj) { 
		document.getElementById('searchfield').value = obj.value;
		document.getElementById('AddUserID').value = obj.id;
		//CheckDataFiles('<?php echo $userid;?>',obj.id); 
	}
};

var as_json = new bsn.AutoSuggest('searchfield', options);

