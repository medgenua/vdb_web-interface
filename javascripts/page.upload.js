//////////////////////
// MAIN AJAX OBJECT //
//////////////////////
function GetXmlHttpObject() {
	if (window.XMLHttpRequest) {
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  return new XMLHttpRequest();
	}
	if (window.ActiveXObject) {
  		// code for IE6, IE5
  		return new ActiveXObject("Microsoft.XMLHTTP");
  	}
	return null;
}


function AddSampleField(uid) {
	// get counter
	nrs = document.getElementById('nrsample').value
	nrs++;
	// build ajax request
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null) {
		alert ("Browser does not support HTTP Request");
		return;
	}
	var url="ajax_queries/GetFTPfiles.php";
	url += "?uid="+uid;
	url += "&nrs="+nrs;
	// get ajax response
	var target = document.getElementById('sampleselects');
	var newsample = document.createElement('p');
	newsample.innerHTML = '<img src="Images/layout/loader-bar.gif"/>';
	target.appendChild(newsample);
	xmlhttp.onreadystatechange=function() {
	   if (xmlhttp.readyState==4) {
		// append response to the "sampleselects div"
		//var target = document.getElementById('sampleselects');
		//var newsample = document.createElement('p');
		newsample.innerHTML = xmlhttp.responseText;
		//target.appendChild(newsample); 
	   }
	};
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	
	// set counter
	 document.getElementById('nrsample').value = nrs;


}
// on load => add first field
var element =  document.getElementById('uid');
if (typeof(element) != 'undefined' && element != null)
{
	var uid = document.getElementById('uid').value;
	AddSampleField(uid);
}
