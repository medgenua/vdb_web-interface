function DeleteSetTranscript(setid, nmid) {
    // update the comment.
    var id_string = '#row\\[' + nmid.replace('.', '\\.') + '\\]'
    // delete from DB.
    pars = { set: setid, nm: nmid, action: 'd' };
    $.post("ajax_queries/ManageMane.php", pars, function (data) {
        if (data == 'OK') {
            // remove from table
            $(id_string).remove();
        }
        else {
            alert("Failed to remove transcript. Error:\n" + data);
        }

    });
}

function UpdateSetComment(setid, nmid) {
    // update the comment.
    nm_esc = nmid.replace('.', '\\.')
    console.log(nm_esc)
    comment = $("#comment\\[" + nm_esc + "\\]").val();
    console.log(comment);
    pars = { set: setid, nm: nmid, comment: comment, action: 'c' }
    $.post("ajax_queries/ManageMane.php", pars, function (data) {
        if (data != 'OK') {
            alert("Failed to update comment in DB. Error:\n" + data);
        }

    })
}

function UpdateSetType(setid, nmid) {
    // update the type to selected value.
    var id_string = '#type\\[' + nmid.replace('.', '\\.') + '\\]'
    value = $(id_string).find(":selected").val();;
    pars = { set: setid, nm: nmid, type: value, action: 't' }
    $.post("ajax_queries/ManageMane.php", pars, function (data) {
        if (data != 'OK') {
            alert("Failed to update comment in DB. Error:\n" + data);
        }

    })
}