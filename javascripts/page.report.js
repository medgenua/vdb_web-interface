//////////////////////
// MAIN AJAX OBJECT //
//////////////////////

// global variable: the statuspoller ID.
var timerID = -1;

function LoadReportDetails() {
    var selected = $('#report').find(":selected").val();
    var url = "ajax_queries/GetReportDetails.php"
    var params = { 'r': selected, 'rv': Math.random() };
    $.get(url, params, function (data) {
        $('#ReportDetails').html(data);
    });
}

function GenerateReport(uid) {
    // first call, fetch from page
    var sel = document.getElementById('samples');
    // jquery returns an array in case of multiselect.
    sids = $("#samples").val();
    // prepare output.
    if (sids.length == 0) {
        $('#report_links').html('ERROR: Select at least one sample.');
        return;
    }
    $('#report_links').html('<h3>Download Links</h3><div id=loading>... Loading : Results will appear below for download ... </div> <div><p><ol id="links"></ol></p></div>');

    // report id
    var rid = $('#report').find(":selected").val();
    if (rid === '') {
        $('#report_links').html('ERROR: Select a report template.');
        return;
    }

    // first kill the old query.
    var oldtimerID = $('#timerID').html();
    if (oldtimerID !== '') {
        var r = confirm("WARNING: There are still reports under generation. \n\nClick 'OK' to continue and discard these reports. \nClick 'Cancel' to wait for them");
        if (r == true) {
            clearInterval(oldtimerID)
        } else {
            return;
        }
    }
    // new procedure: submit all, let wrapper decide to use HPC or not based on settings.
    sidstr = sids.join(",");


    var url = "cgi-bin/Report_Wrapper.py"
    var params = { 'rid': rid, 'sids': sidstr, 'uid': uid };
    $.get(url, params, function (data) {
        //console.log(data);
        //if (!IsJsonString(data)) {
        //    $('#links').html('Reports could not be generated: <pre>' + data + "</pre>");
        //    return;
        //}
        var obj = data;
        //var obj = JSON.parse(data);
        if ('ERROR' in obj) {
            $('#links').html('Reports could not be generated: <pre>' + data + "</pre>");
            return;
        }
        var qids = obj.qids;
        // start the status poller.
        timerID = setInterval(StatusPoller, 10000, qids, uid);
        $('#timerID').html(timerID);
    }, 'json');


}
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function StatusPoller(qids, uid) {

    var url = "ajax_queries/LoadReportLinks.php";
    var params = { 'uid': uid, 'qids': qids, 'rv': Math.random() };
    $.get(url, params, function (data) {
        //if (!IsJsonString(data)) {
        //    $("#links").html('Reports could not be generated: <pre>' + data + "</pre>");
        //    return;
        //}
        //var obj = JSON.parse(data);
        var obj = data;
        if ('ERROR' in obj) {
            $("#links").html('Reports could not be generated: <pre>' + data + "</pre>");
            return;
        }
        if (obj.status == 'Finished') {
            //console.log('Clearing Interval '+timerID);
            clearInterval(timerID);
            $('#loading').hide();
            $('#timerID').html('');
        }
        $("#links").html(obj.links);
        return;
    }, 'json')

}

function AddCBitem() {
    var nr = document.getElementById('nrFields').value;
    nr = parseInt(nr) + 1;
    document.getElementById('nrFields').value = nr;
    var cblist = document.getElementById('cblist');
    var entry = document.createElement('li');
    entry.id = 'item' + nr;
    var input = document.createElement("input");
    input.type = "text";
    input.size = 50;
    input.name = "cbitems[" + nr + "]";
    entry.appendChild(input);
    cblist.appendChild(entry);

    var minus = document.createElement('img');
    minus.src = 'Images/layout/minus-icon.png';
    minus.style.height = '0.9em';
    minus.style.marginLeft = '1em';
    minus.onclick = function () { RemoveCBitem(nr) };
    minus.title = 'Remove this item';
    document.getElementById('item' + nr).appendChild(minus);

}

function RemoveCBitem(nr) {
    // get item.
    console.log(nr)
    var item = document.getElementById('item' + nr);
    item.parentNode.removeChild(item);
}

function AddUsers(userid) {
    var target = document.getElementById('ownerrow');
    var AddUserID = document.getElementById('AddUserID').value;
    if (AddUserID === '') {
        document.getElementById('comment_field').innerHTML = "Select a user by typing the name in the search box.";
        //alert('Select a user by typing the name in the search box.');
        return;
    }
    // listed users. => don't add twice.
    var users = document.getElementsByName("add_users[]");
    var ulen = users.length;
    for (k = 0; k < ulen; k++) {
        if (users[k].value === AddUserID) {
            // new user already listed.
            document.getElementById('comment_field').innerHTML = "User is already listed.";

            return;
        }
    }
    var AddUserName = document.getElementById('searchfield').value;
    var row = document.createElement('tr');
    target.parentNode.insertBefore(row, target);
    var c_user = row.insertCell(0);
    c_user.innerHTML = "<input type=hidden name='add_users[]' value='" + AddUserID + "'/>" + AddUserName;
    var c_add = row.insertCell(1);
    c_add.innerHTML = "<input type='checkbox' name='can_use[]' value='" + AddUserID + "' checked />";
    var c_val = row.insertCell(2);
    c_val.innerHTML = "<input type='checkbox' name='can_edit[]' value='" + AddUserID + "'/>";
    var c_del = row.insertCell(3);
    c_del.innerHTML = "<input type='checkbox' name='can_share[]' value='" + AddUserID + "'/>";
    document.getElementById('comment_field').innerHTML = AddUserName + " added. Set permissions below and submit changes.";


}
function AddGroup(userid) {
    var target = document.getElementById('ownerrowGroup');
    // selected group.
    var AddGroupID = $('#AddGroupSelect').val();
    if (AddGroupID === '' || AddGroupID === null) {
        document.getElementById('group_comment_field').innerHTML = "Select a group.";
        //alert('Select a user by typing the name in the search box.');
        return;
    }
    // listed groups. => don't add twice.
    var groups = document.getElementsByName("add_groups[]");
    var glen = groups.length;
    for (k = 0; k < glen; k++) {
        if (groups[k].value === AddGroupID) {
            // new group already listed.
            document.getElementById('group_comment_field').innerHTML = "Group is already listed.";
            return;
        }
    }
    var AddGroupName = $('#AddGroupSelect :selected').text();
    var row = document.createElement('tr');
    target.parentNode.insertBefore(row, target);
    var c_user = row.insertCell(0);
    c_user.innerHTML = "<input type=hidden name='add_groups[]' value='" + AddGroupID + "'/>" + AddGroupName;
    var c_add = row.insertCell(1);
    c_add.innerHTML = "<input type='checkbox' name='g_can_use[]' value='" + AddGroupID + "'/>";
    var c_val = row.insertCell(2);
    c_val.innerHTML = "<input type='checkbox' name='g_can_edit[]' value='" + AddGroupID + "'/>";
    var c_del = row.insertCell(3);
    c_del.innerHTML = "<input type='checkbox' name='g_can_share[]' value='" + AddGroupID + "'/>";
    document.getElementById('group_comment_field').innerHTML = AddGroupName + " added. Set permissions below and submit changes to finalize.";


}



// autocomplete for search
var options = {
    script: "ajax_queries/usersearch.php?json=true&limit=10&",
    varname: "input",
    json: true,
    shownoresults: true,
    delay: 200,
    cache: false,
    timeout: 10000,
    minchars: 2,
    callback: function (obj) {
        document.getElementById('searchfield').value = obj.value;
        document.getElementById('AddUserID').value = obj.id;
        //CheckDataFiles('<?php echo $userid;?>',obj.id); 
    }
};

var as_json = new bsn.AutoSuggest('searchfield', options);

