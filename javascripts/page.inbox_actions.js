function GetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}


function expandVar(vid, uid, type) {

    if (uid > 0) {
        // ajax url.
        var url = "ajax_queries/LoadVariantAnnotations.php";
        params = {
            uid: uid,
            vid: vid
        };
        $.get(url, params, function (data) {
            $('#anno_' + vid).css('text-align', 'left');
            $('#anno_' + vid).css('border', '1px solid #aeaeae');
            $('#varpos_' + vid).addClass('red');
            $('#anno_' + vid).html(data);
            //}
        });

    }
    //document.getElementById('details_'+vid).style.display='';
    $('#details_' + vid).show();
    if (type == 'n') {
        document.getElementById('general_' + vid).onclick = function () { collapseVar(vid, type); };
    } else if (type == 's') {
        document.getElementById('var_' + vid).onclick = function () { collapseVar(vid, type); };
    }
    return;
}

function collapseVar(vid, type) {
    $('#details_' + vid).hide();
    $('#varpos_' + vid).removeClass('red');

    if (type == 'n') {
        document.getElementById('general_' + vid).onclick = function () { expandVar(vid, 0, type); };
    } else if (type == 's') {
        document.getElementById('var_' + vid).onclick = function () { expandVar(vid, 0, type); };
    }
    return;
}

function ApproveClassifier(vid, uid, sid) {
    // get details.
    var cid = document.getElementById('classifier_id').value;
    var comments = document.getElementById('comments_' + vid).value;
    var actions = document.getElementsByName('action_' + vid);
    var action = 0;
    for (var i = 0; i < actions.length; i++) {
        if (actions[i].checked) {
            action = actions[i].value;
            break;
        }
    }

    // get contents
    var url = "ajax_queries/AddToClassifier.php";
    var cbTable = $('#cb_vc_' + cid + '_' + vid).html();
    var param = {
        action: 'approve',
        //uid : uid,
        vid: vid,
        cid: cid,
        sid: sid,
        type: action,
        cb: cbTable,
    };
    $.post(url, param, function (data) {
        // show form in overlay.
        if (data == 'ok') {
            // finalise : replace contents. make unhidable, replace pointer to default.
            var row = document.getElementById('details_' + vid);
            row.cells[0].innerHTML = '<span class=italic>done</span>';
            row.cells[1].innerHTML = ' &nbsp; ';
            document.getElementById('anno_' + vid).style.border = '';
            document.getElementById('general_' + vid).onclick = '';
            document.getElementById('general_' + vid).onmouseover = document.getElementById('general_' + vid).style.cursor = '';

        }
        else {
            var row = document.getElementById('details_' + vid);
            row.cells[1].innerHTML = ' Saving results failed.  Please report to the system administrator:<br/> ';
            row.cells[1].innerHTML += '<pre>' + data + '</pre>';
        }
    });

}

// dynamically assign listener to checkboxes in saved results.
$(document).on('click', '.NoteBox', function (event) {
    var cb_id = event.target.id;
    var checked = event.target.checked ? 1 : 0

    // work locally if running from the AddToClassifier overlay.
    if (cb_id.substr(0, 3) == 'a2c' || cb_id.substr(0, 2) == 'ic') {
        if (checked) {
            $("input[id='" + cb_id + "']").attr('checked', 'checked')
        }
        else {
            $("input[id='" + cb_id + "']").removeAttr('checked')
        }

        return;
    }
    // format : "(a2c_)variantID_setID_ceckboxListID_value"
    var url = "ajax_queries/ToggleStoredCheckbox.php";
    // set the variables
    var pars = {
        id: cb_id,
        v: checked,
        //uid:uid,
        p: $("#pagenr").val()
    }
    $.post(url, pars, function (data) {
        // error handling? 
        if (data != 'OK') {
            $("#overlay").html('<h3>ERROR</h3><p>An error occured during processing your request. See details below:<br/>"' + data + '"</p><p><input type=submit value=Close class=button onClick="document.getElementById(\'overlay\').style.display=\'none\'"/></p>')
            $("#overlay").show();
        }

    });
});

function ResetValidation(cid, vid, uid) {
    // get details.
    // update DB.
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // ajax url.
    var url = "ajax_queries/ValidateVariant.php?";
    url += "uid=" + uid;
    url += "&vid=" + vid;
    url += "&cid=" + cid;
    url += "&action=0";
    //url += "&comments="+comments;
    url += "&rv=" + Math.random();
    // onchangestate function
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.responseText == 'OK') {
                // finalise : replace contents. make unhidable, replace pointer to default.
                if (confirm("OK: Reload the page to refresh pending variants (top panel).\nCancel: Hide reset variant, don't reload the page.") == true) {
                    location.reload();
                } else {
                    document.getElementById('general_' + vid).style.display = 'none';
                    document.getElementById('details_' + vid).style.display = 'none';
                }
            }
            else {
                var row = document.getElementById('details_' + vid);
                row.cells[1].innerHTML = ' Resetting variant status failed.  Please report to the system administrator:<br/> ';
                row.cells[1].innerHTML += '<pre>' + xmlhttp.responseText + '</pre>';
            }
        }
    }

    var row = document.getElementById('details_' + vid);
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);


}
function DeleteVariant(cid, vid, uid, variant) {
    // get details.
    // update DB.
    var xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    // ajax url.
    var url = "ajax_queries/ValidateVariant.php?";
    url += "uid=" + uid;
    url += "&vid=" + vid;
    url += "&cid=" + cid;
    url += "&action=delete";
    //url += "&comments="+comments;
    url += "&rv=" + Math.random();
    // onchangestate function
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.responseText == 'OK') {
                document.getElementById('general_' + vid).style.display = 'none';
                document.getElementById('details_' + vid).style.display = 'none';
            }
            else {
                document.getElementById('details_' + vid).style.display = '';
                var row = document.getElementById('details_' + vid);
                row.cells[1].style.border = '1px solid red';
                row.cells[1].innerHTML = ' Resetting variant status failed.  Please report to the system administrator:<br/> ';
                row.cells[1].innerHTML += '<pre>' + xmlhttp.responseText + '</pre>';
            }
        }
    }
    if (confirm("Please confirm deletion of this variant:\n" + variant) == true) {
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);

    } else {
        return;
    }

}


// switch sample page
function LoadVariantPage() {
    // get selected value in dropdown.
    var sp = $('#sp').find(":selected").val();
    var url = window.location.href;
    if (/sp=\d+/.test(url)) {
        console.log("url contains sp: " + url);
        url = url.replace(/sp=\d+/, 'sp=' + sp);
    }
    else {
        console.log("url did not have sp: " + url)
        url = url + "&sp=" + sp;
    }

    window.location = url;

}