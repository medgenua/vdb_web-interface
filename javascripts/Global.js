var sbxmlhttp;

// General error handling:
// see : https://cypressnorth.com/web-programming-and-development/global-ajax-error-handling-with-jquery/
// jquery errors.
$(function () {
    //setup ajax error handling
    $.ajaxSetup({
        error: function (x, status, error) {
            params = {
                source: "jquery",
                status: status,
                code: x['status'],
                msg: error,
                url: this.url
            }
            // it's minimal, but at least the URL and CODE get mailed.
            $.post("ajax_queries/jquery.ErrorHandler.php", params, function (data) {
                //console.log("Error Logged from " + this.url);
            });

        }
    });
});

// plain javascript errors.
window.onerror = function errorHandler(msg, url, line, column, errorObj) {
    params = {
        source: "javascript",
        status: "error",
        code: '',
        msg: url + ' [line ' + line + '] : \n' + msg
    }
    // not supported in all browsers.
    if (errorObj !== undefined) {
        params['msg'] += "\n\nStack Trace:\n" + errorObj.stack
    }
    $.post("ajax_queries/jquery.ErrorHandler.php", params, function (data) {
        console.log("Error Logged : " + msg);
    });
    // Just let default handler run.
    return false;
}



function setBuild(gb) {
    sbresponse = '';
    sbxmlhttp = BuildGetXmlHttpObject();
    if (sbxmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "ajax_queries/setBuild.php";
    url = url + "?gb=" + gb;
    url = url + "&rv=" + Math.random();
    sbxmlhttp.onreadystatechange = BuildChanged;
    sbxmlhttp.open("GET", url, true);
    sbxmlhttp.send(null);
    return;
}


function BuildChanged() {
    if (sbxmlhttp.readyState == 4) {

        // reload page
        //window.location.href=window.location.href;
        //window.opener.location.reload();
        window.location.reload();

    }
}

function BuildGetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}

// GET THE SITE REVISION and/or NOTIFY OF AVAILABLE UPDATES
function CheckRevision(task, target_el) {
    // get current revision
    if (task == 'tip') {
        if (getCookie('revision') == '') {
            var url = "ajax_queries/GetRevision.php";
            url = url + "?rv=" + Math.random();
            $("#" + target_el).load(url, function () {
                setCookie('revision', $('#' + target_el).text(), 1);
            });
        } else {
            $("#" + target_el).text(getCookie('revision'));
        }
    }

}

function Show_CC_Details() {
    document.getElementById("see_cc_det").style.display = 'none';
    document.getElementById("hide_cc_det").style.display = '';
    var x = document.getElementsByClassName("cc_banner")[0];
    x.appendChild(document.getElementById("cc_details"));
    document.getElementById("cc_details").style.display = '';

}
function Hide_CC_Details() {
    document.getElementById("see_cc_det").style.display = '';
    document.getElementById("hide_cc_det").style.display = 'none';
    document.getElementById("cc_details").style.display = 'none';
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) === 0) {
            // empty cookie (invalid or empty value) => return 
            if (c.substring(name.length, c.length) === "") {
                return ""
            }
            return (c.substring(name.length, c.length));
        }
    }
    return "";
}


function CheckServices() {
    var url = "ajax_queries/ServiceCheck.php";
    $.get(url);
}
