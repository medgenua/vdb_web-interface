function BatchSet() {
    var BatchBoxVal = document.getElementById('BatchBox').checked;
    for (i = 0; i < document.sampleform.elements.length; i++) {
        var myName = document.sampleform.elements[i].name;
        if (myName.match(/batch/)) {
            if (BatchBoxVal === true) {
                document.sampleform.elements[i].checked = true;
            }
            else {
                document.sampleform.elements[i].checked = false;
            }
        }
    }
    // display batch move options if checkboxes are activated. 
    if (BatchBoxVal === true) {
        document.getElementById('BatchMove').style.display = '';
        document.getElementById('UpdateButton').style.display = 'none';

    }
    else {
        document.getElementById('BatchMove').style.display = 'none';
        document.getElementById('UpdateButton').style.display = '';

    }

}

function SetPage() {
    page = $("#setPage").val();
    window.location.replace("index.php?page=recent&p=" + page);
}
function toggleDisplay() {
    // check if any checkbox is activated.
    var toggle = 0;
    for (i = 0; i < document.sampleform.elements.length; i++) {
        var myName = document.sampleform.elements[i].name;
        if (myName.match(/batch/)) {
            if (document.sampleform.elements[i].checked === true) {
                toggle = 1;
                break;
            }
        }
    }
    if (toggle == 1) {
        document.getElementById('BatchMove').style.display = '';
        document.getElementById('UpdateButton').style.display = 'none';

    }
    else {
        document.getElementById('BatchMove').style.display = 'none';
        document.getElementById('UpdateButton').style.display = '';

    }
    CheckNew();

}
function CheckNew() {
    if (document.getElementById('MoveToPidSelect').options[document.getElementById('MoveToPidSelect').selectedIndex].value === 'new') {
        document.getElementById('NewProjectName').style.display = '';
    }
    else {
        document.getElementById('NewProjectName').style.display = 'none';
    }

}


