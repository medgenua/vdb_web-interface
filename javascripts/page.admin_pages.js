function reloadspan(spanname) {
	var filename = "Files/"+spanname+".out";
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="LiftOver/getLiftOutput.php";
	url=url+"?filename="+filename;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			document.getElementById(spanname).innerHTML = xmlhttp.responseText;		
			document.getElementById(spanname).scrollTop = document.getElementById(spanname).scrollHeight;
		}
	}
	xmlhttp.send(null);
}

function reloadupdatespan(rand) {
	var spanname = 'db'+rand;
	var filename = "/tmp/"+rand+".hg.out";
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="ajax_queries/getFileContent.php";  // borrow this from the LiftOver scripts
	url=url+"?filename="+filename;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			document.getElementById(spanname).innerHTML = xmlhttp.responseText;		
			document.getElementById(spanname).scrollTop = document.getElementById(spanname).scrollHeight;
		}
	}
	xmlhttp.send(null);
}

function checkstatus(items) {
	var intInterval = 0;
	var filename = "/status/LiftOver.status";
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="LiftOver/getLiftOutput.php";
	url=url+"?filename="+filename;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			var statusvalue = xmlhttp.responseText;
			if (statusvalue == 0) {
				for (item=1;item<=items;item=item+1) {
					var interval = eval("interval"+item);
					clearInterval(interval); 		
				}
				document.getElementById('continuebutton').style.display = '';
				alert('done');
		
			}
		}
	}
	xmlhttp.send(null);
}
function checkdownloadstatus(items) {
	var intInterval = 0;
	var filename = "Files/UpdateDB.status";
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="LiftOver/getLiftOutput.php";
	url=url+"?filename="+filename;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			var statusvalue = xmlhttp.responseText;
			if (statusvalue == 0) {
				for (item=1;item<=items;item=item+1) {
					var interval = eval("interval"+item);
					clearInterval(interval); 		
				}
				document.getElementById('continuebutton').style.display = '';
				alert('done');
		
			}
		}
	}
	xmlhttp.send(null);
}

function checkupdatestatus(items,rand) {
	var intInterval = 0;
	var filename = "/tmp/"+rand+".db.status";
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="ajax_queries/getFileContent.php";
	url=url+"?filename="+filename;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			var statusvalue = xmlhttp.responseText;
			if (statusvalue == 0) {
				for (item=1;item<=items;item=item+1) {
					var interval = eval("interval"+item);
					clearInterval(interval); 		
				}
				document.getElementById('toupdates').style.display = '';
				// send ajax query to reenable site.
				var xmlhttpb = false;
				if (window.XMLHttpRequest)
  				{
  					// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp = new XMLHttpRequest();
				}
				if (window.ActiveXObject)
  				{
  					// code for IE6, IE5
  					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  				}
				if (xmlhttp==null)
 				{
 			 		alert ("Browser does not support HTTP Request");
  					return;
				}
				var url="ajax_queries/SetSiteStatus.php";
				url=url+"?stat=Operative";
				url=url+"&rv="+Math.random();
				xmlhttp.open("GET",url,true);
				xmlhttp.send(null);
				alert('done');
		
			}
		}
	}
	xmlhttp.send(null);
}

function NewLicense(level) {
	var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Define new license</span>";
	if (level < 3) {
		newcontent += '<p class=white> Access Denied. Only admin users can add license definitions.</p>';
		newcontent += '<p class=white> <input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
		newcontent += '</div>';
		document.getElementById('overlay').innerHTML = newcontent;
		document.getElementById('overlay').style.display = '';

		return;
	}
	newcontent += "<p class=white style='text-align:left;'>License Name: <input type=text size=40 id=lname> : use this name in the VariantDB config files.</p>";
	newcontent += "<p class=white style='text-align:left;'>Package URL : <input type=text size=40 id=url> : homepage with additional information on package</p>";
	newcontent += "<p class=white style='text-align:left;'>Description :<br/> <textarea style='margin-left:7em;' id='ldesc' cols=40 rows=10></textarea></p>";
	newcontent += '<p class=white style="text-align:left;"><input type=submit class=button id="Create" value="Create" /> <input type=submit value="Cancel" onClick="document.getElementById(\'overlay\').style.display=\'none\'"></p>';
	document.getElementById('overlay').innerHTML = newcontent;
	document.getElementById('overlay').style.display = '';

}


function check(setthis) {
	var node_list = document.getElementsByTagName('input');
	for (var i = 0; i < node_list.length; i++) {
    		var node = node_list[i];
		if (node.getAttribute('value') == setthis ) {
			// do something here with a <input type="text" .../>
		        // we alert its value here
			node.checked = true;
	       	}
	} 
}
	
