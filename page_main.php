<div class=section>
    <h3>VariantDB Server Outline</h3>
    <p>The following options are available throught the menu above:</p>
    <ol>
        <li>New Data:<ul>
                <li>Import variants files from the VariantDB FTP server (if enabled)</li>
                <li>Overview of your data on the ftp server</li>
            </ul>
        </li>
        <li>Variant Filter: <ul>
                <li>Filter Variants from a certain sample using a variety of criteria. </li>
                <li>Exported results to CSV or produce summary graphs</li>
            </ul>
        </li>
        <li>Manage Samples: <ul>
                <li>Specify sample info such as clinical info, gender, family etc</li>
                <li>Move samples around to create relevant groups or projects</li>
            </ul>
        </li>
        <li>Manage Projects: <ul>
                <li>Set Permissions on selected groups of samples.</li>
                <li>Permissions can be set for indivual users or user groups (to be implemented).</li>
            </ul>
        </li>
        <li>Recent Samples: <ul>
                <li>An overview of the last samples you added to the database, still needing initial annotations.</li>
            </ul>
        </li>
        <li>Settings:<ul>
                <li>Overview of the system vesion history</li>
                <li>Manage Gene Panels</li>
            </ul>
        </li>
        <li>Documentation:<ul>
                <li>System documentation.</li>
            </ul>
        </li>
    </ol>
    </p>
</div>
<div class=section>
    <h3>Documentation</h3>
    <p>Use the following links to read quickstart instructions:
    <ul>
        <li><a href='index.php?page=tutorial&amp;topic=intro'>Getting Started:</a> Very short instructions to register, log in and start browsing data</li>
        <li><a href='index.php?page=tutorial&amp;topic=newdata'>Loading Data:</a> How to get data into VariantDB, either using FTP or the galaxy tool</li>
        <li><a href='index.php?page=tutorial&amp;topic=anno'>Available Annotations:</a> Descriptions of all annotations available in VariantDB</li>
        <li><a href='index.php?page=tutorial&amp;topic=filters'>Available Filters:</a> Overview and explaination of the available filtering schemes</li>
    </ul>
    </p>
</div>


<div class=section>
    <h3>IGV Support <span style='color:red'>UPDATED</span></h3>
    <p>IGV visualisation is integrated in this website using direct links to load and navigate the variants and reads in IGV. Where possible, VariantDB converts BAM to CRAM. This requires the latest BETA version of IGV. See <a href='index.php?page=tutorial&topic=igv'>documentation</a> for full guidelines: </p>
    <p>Quick Download links:
    <ol>
        <li>Reference genome: <a href='http://143.169.238.8/variantdb_files/Genomes/variantdb.genome.zip'>http://143.169.238.8/variantdb_files/Genomes/variantdb.genome.zip</a></li>
        <li>IGV-Snapshot: <a href='https://software.broadinstitute.org/software/igv/download_snapshot' target='_blank'>https://software.broadinstitute.org/software/igv/download_snapshot</a></li>
    </ol>
    </p>
    <p>&nbsp;</p>
    <p><a href='index.php?page=tutorial&topic=igv'>DOCUMENTATION</a> : Here are the full guidelines on how to install and use IGV with our database.</p>
</div>



<div class=section>
    <h3>Install VariantDB Locally</h3>
    <p>VariantDB can be installed locally in two ways:
    <ol>
        <li>Use our openbox virtual machine : <a href='index.php?page=tutorial&topic=virtualboxVM'>Instructions</a></li>
        <li>Manually install on your own hardware: <a href='index.php?page=tutorial&topic=install'>Instructions</a></li>
</div>