<?php
abstract class API
{
    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';
    /**
     * Property: endpoint
     * The Model requested in the URI. eg: /files
     */
    protected $endpoint = '';
    /**
     * Property: verb
     * An optional additional descriptor about the endpoint, used for things that can
     * not be handled by the basic methods. eg: /files/process
     */
    protected $verb = '';
    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    protected $args = array();
    /**
     * Property: file
     * Stores the input of the PUT request
     */
    protected $file = Null;

    /**
     * User details
     */
    public $uid = "";
    protected $UserLevel = '';
    protected $email = '';

    /*
     * config data
     */
    protected $config = array();

    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */

    public function __construct($request)
    {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
        $this->args = explode('/', rtrim($request, '/'));
        $this->endpoint = array_shift($this->args);
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }

        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        switch ($this->method) {
            case 'DELETE':
            case 'POST':
                $this->request = $this->_cleanInputs($_POST);
                break;
            case 'GET':
                $this->request = $this->_cleanInputs($_GET);
                break;
            case 'PUT':
                $this->request = $this->_cleanInputs($_GET);
                $this->file = file_get_contents("php://input");
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }

        // Read credentials
        global $config;
        $config = $this->_ReadCredentials();
        $this->path = $_SERVER['HTTP_HOST'] . "/" . $config['WEBPATH'] . "/api/";

        // initialize logger
        require('../includes/inc_logging.inc');
        // initialize the DB.
        $mysqli = $this->_ConnectToDb($config['DBUSER'], $config['DBPASS'], $config['DBHOST'], $this->config['DBSUFFIX']);
    }


    public function processAPI()
    {
        if ((int)method_exists($this, $this->endpoint) > 0) {
            return $this->_response($this->{$this->endpoint}($this->args));
        }
        return $this->_response("No Endpoint: $this->endpoint", 404);
    }

    private function _response($data, $status = 200)
    {
        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
        return json_encode($data);
    }

    private function _cleanInputs($data)
    {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }

    private function _requestStatus($code)
    {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code]) ? $status[$code] : $status[500];
    }
    private function _ReadCredentials()
    {
        $file = realpath(".credentials");
        $lines = file($file) or die("Could not read $file"); //return(0);
        $config = array();
        foreach ($lines as $line_num => $line) {
            # Comment?
            if (!preg_match("/#.*/", $line)) {
                # Contains non-whitespace?
                if (preg_match("/\S/", $line) && preg_match("/=/", $line)) {
                    list($key, $value) = explode("=", trim($line), 2);
                    $config[$key] = $value;
                }
            }
        }
        // set a few general config items for usage elseswhere.
        $this->config['WEBPATH'] = $config['WEBPATH'];
        $this->config['SITEDIR'] = $config['SITEDIR'];
        $this->config['IGVdata'] = $config['IGVdata'];
        $this->config['SCRIPTUSER'] = $config['SCRIPTUSER'];
        $this->config['SCRIPTPASS'] = $config['SCRIPTPASS'];
        $this->config['USEHPC'] = $config['USEHPC'];
        $this->config['SCRIPTDIR'] = $config['SCRIPTDIR'];
        $this->config['DATADIR'] = $config['DATADIR'];
        $this->config['FTP_DATA'] = $config['FTP_DATA'];
        $this->config['DOMAIN'] = $config['DOMAIN'];
        if (array_key_exists('DBSUFFIX', $config)) {
            $this->config['DBSUFFIX'] = "_" . $config['DBSUFFIX'];
        } else {
            $this->config['DBSUFFIX'] = "";
        }


        return $config;
    }

    public function getUserLevel($apikey)
    {
        $row = $this->_runQuery("SELECT id, LastName, FirstName, email, level, apiExpires, apiExpirationDate FROM `Users` WHERE apiKey = '$apikey'", False);
        if (empty($row)) {
            return 0;
        } else {
            return $row['level'];
        }
    }
    public function verifyKey($apikey)
    {
        // validate key : key esists && Age < 1 week if set to expire.
        $row = $this->_runQuery("SELECT id, LastName, FirstName, email, level, apiExpires, apiExpirationDate FROM `Users` WHERE apiKey = '$apikey'", False);
        if (empty($row)) {
            return array('status' => FALSE, 'reason' => 'Invalid API Key');
        }
        if ($row['apiExpires'] == 1 && strtotime($row['apiExpirationDate']) < time()) {
            return array('status' => FALSE, 'reason' => "API Key expired on " . $row['apiExpirationDate']);
        }
        // validate key : user is active
        if ($row['level'] < 1) {
            return array('status' => FALSE, 'reason' => "API-key belongs to disabled user: " . $row['level']);
        }
        $this->uid = $row['id'];
        $this->email = $row['email'];
        $this->UserLevel = $row['level'];
        $this->apiKey = $apikey;
        if ($row['level'] == 3 && array_key_exists("RunAs", $this->request)) {
            if (!is_numeric($this->request['RunAs'])) {
                return array("ERROR" => "RunAs takes a numeric userID as  value");
            }
            $urow = $this->_runQuery("SELECT id FROM `Users` WHERE id = '" . $this->request['RunAs'] . "'", False);
            if (empty($urow)) {
                return array("ERROR" => "Unknown UserID provided for 'RunAs'");
            }
            $this->uid = $this->request['RunAs'];
        }
        return array('status' => TRUE); //, 'uid' => $row['id'],'LastName' => $row['LastName'],'FirstName' => $row['FirstName'],'UserLevel' => $row['level'],'Email' => $row['email']);

    }

    public function CheckPlatformStatus()
    {
        //$this->ConnectToDB() or die("Could not connect to database");
        ## check platform status.
        $query = "SELECT status FROM `NGS-Variants-Admin`.`SiteStatus`";
        $row = $this->_runQuery($query, False);
        return ($row);
    }

    ///////////////////////
    // DATABASE ROUTINES //
    ///////////////////////
    private function _ConnectToDB($dbuser, $dbpass, $dbhost, $dbsuffix)
    {
        if (!isset($this->mysqli)) {
            // get build.
            $mysqli = new mysqli($dbhost, $dbuser, $dbpass, "NGS-Variants-Admin$dbsuffix");
            if ($mysqli->connect_errno) {
                echo json_encode("Failed to connect to MySQL: " . $mysqli->connect_error);

                error_log("Failed to connect to MySQL: " . $mysqli->connect_error);
                exit();
            }
            $result = $mysqli->query("SELECT name, StringName, Description FROM `CurrentBuild` LIMIT 1");
            $row = $result->fetch_assoc();
            $dbname = $row['name'];
            $result->free();
            $selectdb = $mysqli->select_db("NGS-Variants$dbname$dbsuffix");
            if ($selectdb) {
                $this->mysqli = $mysqli;
            } else {
                echo json_encode("Failed to connect to Database NGS-Variants$dbname$dbsuffix");
                error_log("Failed to connect to MySQL: " . $mysqli->connect_error);
                exit;
            }
        }
        return $mysqli;
    }

    // run query without feedback (create / drop /truncate)
    protected function _doQuery($query)
    {
        $sth = $this->mysqli->query($query);
        if (!$sth) {
            trigger_error($this->mysqli->error, E_USER_ERROR);
            return FALSE;
        } else {
            return TRUE;
        }
    }

    protected function _runQuery($query, $type = 'assoc', $aoa = True)
    {
        //workaround because php does not support named args: if called as rq($query,False)
        if (!$type) {
            // set aoa to false, reset type to def
            $aoa = False;
            $type = 'assoc';
        }
        // type "assoc" return key/value arrays (associative) ; type "row" returns anonymous array (numeric keys)
        $sth = $this->mysqli->query($query);
        if (!$sth) {
            trigger_error($this->mysqli->error, E_USER_ERROR);
            return FALSE;
        }
        // one result, and not requesting an array of arrays as result
        if ($sth->num_rows == 1 && !$aoa) {
            if ($type != 'row') {
                $result = $sth->fetch_assoc();
            } else {
                $result = $sth->fetch_row();
            }
        } elseif ($sth->num_rows == 0) {
            $result = array();
        } else {
            $result = array();
            if ($type != 'row') {
                while ($row = $sth->fetch_assoc()) {
                    array_push($result, $row);
                }
            } else {
                while ($row = $sth->fetch_row()) {
                    array_push($result, $row);
                }
            }
        }
        return $result;
    }

    protected function _insertQuery($query)
    {

        // run query
        if ($this->mysqli->query($query)) {
            $id = $this->mysqli->insert_id;
        } else {
            trigger_error($this->mysqli->error, E_USER_ERROR);
            $id = -1;
        }
        return ($id);
    }
    // create AoA from array in case of a single result
    protected function _OneToMulti($a)
    {
        $result = array();
        if (empty($a)) {
            return ($a);
        }
        array_push($result, $a);
        return ($result);
    }

    // escape values for MYSQL
    protected function _SqlEscapeValues($data)
    {
        if (is_array($data)) {
            for ($i = 0; $i < count($data); $i = $i + 1) {
                $data[$i] = $this->mysqli->real_escape_string($data[$i]);
            }
        } else {
            $data = $this->mysqli->real_escape_string($data);
        }
        return ($data);
    }

    // get the latest sql error
    protected function _GetSqlError()
    {
        return ($this->mysqli->error);
    }

    /** ClearMemcache routine after changes to database. **/
    protected function _clearMemcache($table)
    {
        $t = explode(":", $table);
        foreach ($t as $key => $tname) {
            $this->_doQuery("UPDATE `NGS-Variants-Admin`.`memcache_idx` SET `Table_IDX` = `Table_IDX` + 1 WHERE `Table_Name` = '$tname'");
        }
    }
}
