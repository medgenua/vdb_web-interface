#!/usr/bin/python
# default modules
import sys
import json
import urllib
import urllib2
import getopt 
import os.path
# non-default packages (install with "pip install <package>")
import requests

## SET LOCAL INSTALLATIONS HERE:
vdb_url = "http://143.169.238.104/variantdb/api/"

def main() :
	# parse commandline
	optlist,args = getArgs(sys.argv[1:])
	# if no api key provided : exit
	try: 
		apikey = optlist['k']
	except: 
		print('No API key provided.')
		Usage()
			
	## if both -I and -a are provided : exit
	if 'a' in optlist and 'I' in optlist:
		Usage() 


	# check correctness of API.
	answer = fetch_json_page(vdb_url + 'CheckApiKey?apiKey='+apikey)
	try:
		answer == '1'
	except:
		print(answer) 
		print("Invalid API Key provided.")
		print("Log in on VariantDB and check the key under User-Settings (click on you name)")
		Usage()

	## EXPORT
	if 'a' in optlist:
		try: 
			optlist['a'].isdigit()
		except:
			print("Invalid (non-numeric) Annotation_ID provided: '"+optlist['a']+"'")
			Usage();

		try:
			answer = fetch_json_page(vdb_url+'SavedAnnotations/'+optlist['a']+'?apiKey='+apikey)
		except: 
			print("Unknown Annotation_ID provided: "+optlist['a'])
			Usage();
		
		with open(optlist['O'],'w') as outfile:	
			json.dump(answer,outfile)	
		sys.exit(0)
	## IMPORT 
	elif 'I' in optlist:
		try:
			os.path.isfile(optlist['I'])
			
		except:
			print("Provided file path does not exist")
			Usage()	

		## now pass it VariantDB api.
		answer = requests.post(vdb_url+"LoadAnnotations?apiKey="+apikey,data={'apiKey':apikey},files={'json_file': open(optlist['I'], 'r')})
		aid = json.loads(answer.text)
	 	print "Annotation Set Stored under ID: ",aid['id']

	## LIST ALL
	else:
		try:
			answer = fetch_json_page(vdb_url+'SavedAnnotations?apiKey='+apikey)
		except:
			print "Failed to retrieve list of saved annotations."
		
		for aset in answer:
			aid = aset['aid']
			aname = aset['AnnotationName']
			acomments = aset['Comments']
			aitems = aset['nrItems']
			print aname+":"
			print "=" * (len(aname)+1)
			print "  ID:",aid
			print "  nrItems:",aitems
			print "  Comments:",acomments,"\n"


def getArgs(args):  
	## arguments
	# -k : apikey (mandatory)
	# -a : annotation_id (optional)
	# -I : in_file_path to import (optional)
	# -O : out_file_path
	opts, args = getopt.getopt(args, 'k:a:O:I:h')
	optlist = dict()
	for opt, arg in opts:
		optlist[opt[1:]] = arg
	if 'a' in optlist and 'O' not in optlist:
		print "Missing argument : -O"
		Usage()
	if 'h' in optlist:
		Usage()
	return(optlist,args)

def Usage():
	# print help
	print "\n\nUsage: python Import_Export_Annotation.py -k <apikey> "
	print " Default: lists all annotations sets stored by the provided user"
	print " Optional : -a <anno_id> : print json representation of annotation set. Combine with -O."
	print " Optional : -O <file_path> : where to write the annotation set."
	print " Optional : -I <file_path>: import an annotation set from a file (exported by -a). "
	print "  \n  => -a and -I are mutually exclusive !"
	print "  => In absence of both -a and -I, a summary of all saved anntotations is listed to pick from.";
	print "\n\n"
	sys.exit(0)

def fetch_json_page(url):
    try: 
        data = urllib2.urlopen(url)
	j = json.load(data)
    except:
        print('Fetching api repsonse failed for following url:')
        print(url)
        sys.exit(2)
     
    ## return data
    return j 


if __name__ == "__main__":
	main()
