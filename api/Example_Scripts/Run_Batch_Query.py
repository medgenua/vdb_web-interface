#!/usr/bin/python
# default modules
import sys
import json
import urllib
import urllib2
import getopt 
import os.path
import time
# non-default packages (install with "pip install <package>")
import requests



## SET LOCAL INSTALLATIONS HERE:
vdb_url = "http://DevBox/variantdb/api/"

## global variables
apiKey = "" 
sid_x_sname = {}
q_x_s = {}

def main() :
	# parse commandline
	optlist,args = getArgs(sys.argv[1:])
	apiKey = optlist['k']
	print "apikey:",apiKey
	# check correctness of API.
	CheckAPIKey(vdb_url,apiKey)	

	# check if filter_id is valid.
	try: 
		optlist['f'].isdigit()
	except:
		print("Invalid (non-numeric) Filter_ID provided: '"+optlist['f']+"'")
		Usage();
	try:
		answer = fetch_json_page(vdb_url+'SavedFilters/'+optlist['f']+'?apiKey='+apiKey)
	except: 
		print("Unknown Filter_ID provided: "+optlist['f'])
		Usage();
	fid = optlist['f']	
	# check anno_id(s)
	q_anno = ''
	if 'a' in optlist:
		annos = optlist['a'].split(',')
		for aid in annos:
			# digit?
			try:
				aid.isdigit()
			except:
				print("Invalid (non-numeric) Annotation_ID provided: '"+aid+"'")
				Usage();
			# known in db?
			try:
				answer = fetch_json_page(vdb_url+'SavedAnnotations/'+aid+'?apiKey='+apiKey)
			except: 
				print("Unknown Annotation_ID provided: "+aid)
				Usage();
		q_anno = '&aid='+optlist['a']
	# get list of all samples.
	answer = fetch_json_page(vdb_url+'Samples?apiKey='+apiKey)
	db_samples = {}
	for sample in answer:
		if not sample['sample_name'] in db_samples:
			db_samples[sample['sample_name']] = {}
		db_samples[sample['sample_name']][sample['id']] = sample['project_name']	
	# sample list to work on
	q_snames = list()
	q_sids = list()
	# get provided sample names
	if os.path.isfile(optlist['I']):
		with open(optlist['I'],'r') as f:
			for line in f:
				q_snames.append(line.rstrip())
			f.close()
	else:
		q_snames.append(optlist['I'])
	# get corresponding sample_ids
	for sname in q_snames:
		if sname in db_samples:
		  if len(db_samples[sname]) == 1:
			q_sids.append(db_samples[sname].keys()[0])
			sid_x_sname[db_samples[sname].keys()[0]] = sname
		  elif len(db_samples[sname]) == 0:
			print "\n ERROR: Sample '"+sname+"' was not found in the database.\n"
			sys.exit(2)
		  else:
			print "\nSample name '"+sname+"' is not unique. Please pick the project to use the sample from:"
			idx = 0
			sids = {}
			for i in db_samples[sname]:
				idx += 1
				print " ",str(idx),": project name",db_samples[sname][i]	
				sids[idx] = i
			choice = int(raw_input("Your Choice [1]: "))
			if not (choice in sids.keys()):
     	   			print "\n ERROR: Invalid selection."
        			sys.exit(2)
			q_sids.append(sids[choice])
			sid_x_sname[sids[choice]] = sname
	## launch queries.
	q_ids = {} # query_id => status

	for sid in q_sids:
		answer = fetch_json_page(vdb_url+'SubmitQuery/Sample/'+sid+'?apiKey='+apiKey+'&fid='+fid+q_anno)
		q_ids[answer['query_key']] = 0
		q_x_s[answer['query_key']] = sid

	## wait for them to finish.
	all_done = 0
	while all_done == 0:
		all_done = 1
		for q_id in q_ids.keys():
			if q_ids[q_id] == 0:
				answer = fetch_json_page(vdb_url+'GetStatus/Query/'+q_id+'?apiKey='+apiKey)
				if answer['status'] == 'finished':
					q_ids[q_id] = 1
					# get query_results
					FetchQueryResults(q_id,apiKey)
				else:
					all_done = 0
		if all_done == 0:
			answer = fetch_json_page(vdb_url+'GetStatus/Queue?apiKey='+apiKey)
			print "Waiting for queries to finish. Items on queue:",str(answer['nr_queued'])
			time.sleep(15)		

		
def getArgs(args):  
	## arguments
	# -k : apikey (mandatory)
	# -f : filter_id 
	# -a : annotation_id (comma-seperated list, optional)
	# -O : out_file_path
	# -I : infile with sample-names. one per line, or one sample name.
	opts, args = getopt.getopt(args, 'k:f:O:I:a:h')
	optlist = dict()
	for opt, arg in opts:
		optlist[opt[1:]] = arg
	if 'k' not in optlist:
		print "ERROR: Missing argument : -k"
		Usage()
	if 'f' not in optlist:
		print "ERROR: Missing argument : -f"
		Usage()
	if 'O' not in optlist:
		print "ERROR: Missing argument : -O"
		Usage()
	if 'I' not in optlist:
		print "ERROR: Missing argument : -I"
		Usage()

	if 'h' in optlist:
		Usage()
	return(optlist,args)

def Usage():
	# print help
	print "\n\nUsage: python Run_Batch_Query.py -k <apikey> -I <sample_file> -f <filter_id> -a <anno_id1,anno_id2> -O <output_prefix>"
	print "\n Output is written to files <prefix>_<sample_name>.txt. If <prefix>"
	print "\n\n"
	sys.exit(0)

def CheckAPIKey(url,key):
	# check correctness of API.
	answer = fetch_json_page(url + 'CheckApiKey?apiKey='+key)
	try:
		answer == '1'
	except:
		print("Invalid API Key provided.")
		print("Log in on VariantDB and check the key under User-Settings (click on you name)")
		Usage()


def fetch_json_page(url):
    try: 
        data = urllib2.urlopen(url)
	j = json.load(data)
    except:
        print('Fetching api repsonse failed for following url:')
        print(url)
        sys.exit(2)
     
    ## return data
    return j 

def FetchQueryResults(q_id,apiKey):
	answer = fetch_json_page(vdb_url+'GetQueryResults/'+q_id+'?apiKey='+apiKey)
	sname = sid_x_sname[q_x_s[q_id]]
	## todo: parse json to tabular, and write to file for sample 'sname'.
	print "printed results for "+sname


if __name__ == "__main__":
	main()
