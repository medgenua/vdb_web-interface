#!/usr/bin/python
# default modules
import sys
import json
import urllib
import urllib2
import getopt 
import os.path
# non-default packages (install with "pip install <package>")
import requests

## SET LOCAL INSTALLATIONS HERE:
vdb_url = "http://DevBox/variantdb/api/"

def main() :
	# parse commandline
	optlist,args = getArgs(sys.argv[1:])
	# if no api key provided : exit
	try: 
		apikey = optlist['k']
	except: 
		print('No API key provided.')
		Usage()
			
	## if both -I and -a are provided : exit
	if 'f' in optlist and 'I' in optlist:
		Usage() 


	# check correctness of API.
	answer = fetch_json_page(vdb_url + 'CheckApiKey?apiKey='+apikey)
	try:
		answer == '1'
	except:
		print(answer) 
		print("Invalid API Key provided.")
		print("Log in on VariantDB and check the key under User-Settings (click on you name)")
		Usage()

	## EXPORT
	if 'f' in optlist:
		try: 
			optlist['f'].isdigit()
		except:
			print("Invalid (non-numeric) Filter_ID provided: '"+optlist['f']+"'")
			Usage();

		try:
			answer = fetch_json_page(vdb_url+'SavedFilters/'+optlist['f']+'?apiKey='+apikey)
		except: 
			print("Unknown Filter_ID provided: "+optlist['f'])
			Usage();
		
		with open(optlist['O'],'w') as outfile:	
			json.dump(answer,outfile)	
		sys.exit(0)
	## IMPORT 
	elif 'I' in optlist:
		try:
			os.path.isfile(optlist['I'])
			
		except:
			print("Provided file path does not exist")
			Usage()	

		## now pass it VariantDB api.
		answer = requests.post(vdb_url+"LoadFilters?apiKey="+apikey,data={'apiKey':apikey},files={'json_file': open(optlist['I'], 'r')})
		fid = json.loads(answer.text)
	 	print "Filter Stored under ID: ",fid['id']

	## LIST ALL
	else:
		try:
			answer = fetch_json_page(vdb_url+'SavedFilters?apiKey='+apikey)
		except:
			print "Failed to retrieve list of saved filters."
		
		for fset in answer:
			fid = fset['fid']
			fname = fset['FilterName']
			fcomments = fset['Comments']
			fitems = fset['nrRules']
			print fname+":"
			print "=" * (len(fname)+1)
			print "  ID:",fid
			print "  nrRules:",fitems
			print "  Comments:",fcomments,"\n"


def getArgs(args):  
	## arguments
	# -k : apikey (mandatory)
	# -f : filter_id (optional)
	# -I : in_file_path to import (optional)
	# -O : out_file_path
	opts, args = getopt.getopt(args, 'k:f:O:I:h')
	optlist = dict()
	for opt, arg in opts:
		optlist[opt[1:]] = arg
	if 'f' in optlist and 'O' not in optlist:
		print "Missing argument : -O"
		Usage()
	if 'h' in optlist:
		Usage()
	return(optlist,args)

def Usage():
	# print help
	print "\n\nUsage: python Import_Export_Filter.py -k <apikey> "
	print " Default: lists all Filtersets stored by the provided user"
	print " Optional : -f <filter_id> : get json representation of filterset. Combine with -O."
	print " Optional : -O <file_path> : where to write the filterset."
	print " Optional : -I <file_path>: import a filterset from a file (exported by -f). "
	print "  \n  => -f and -I are mutually exclusive !"
	print "\n\n"
	sys.exit(0)

def fetch_json_page(url):
    try: 
        data = urllib2.urlopen(url)
	j = json.load(data)
    except:
        print('Fetching api repsonse failed for following url:')
        print(url)
        sys.exit(2)
     
    ## return data
    return j 


if __name__ == "__main__":
	main()
