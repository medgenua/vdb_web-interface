#!/usr/bin/env perl


# time
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();

# infinite loop.
while (1) {
	## once every day.
	my @now = localtime();
	if ($mday ne $now[3]) {
		$mday = $now[3];
		# find && delete older than 1 week ,with one day margin (queueing etc).
		system('cd /VariantDB/Web-Interface/api/query_results  && find . -maxdepth 1 -type d -ctime +8 -exec rm -rf {} \;');
	}
	# run once an hour, to keep it short to midnight. 
	sleep 3600;
}
