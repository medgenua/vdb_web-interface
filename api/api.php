<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
//
// STRUCTURE OF EACH RESPONSE IS ASSOC ARRAY WITH: 
//   - IF SUCCESS: 
//      - help => some_documentation
//      - status => "ok" (lowercase)
//      - results => the actual response structure
//   - IF FAILURE:
//      - help => some_documentation
//      - status => "error" (lowercase)
//      - msg => "error message"
//
require_once 'API.class.php';
class MyAPI extends API
{
    protected $User;

    public function __construct($request)
    {
        parent::__construct($request);
    }
    /////////////////////////
    // AVAILABLE ENDPOINTS //
    /////////////////////////
    // list user details
    // list all stored filters 			ok
    // list details of a specific filter		ok
    // list all stored annotation sets		ok
    // list details of a specific annotation set	ok
    // list all samples				ok
    // list details of a specific sample		ok
    // Load Annotations using a POSTed JSON file	ok
    // Phenotypes : details by ID, search by term
    // run query, for sample/region (mandatory), using a specific filter (just one) and one or more annotation sets ok
    // download data : sample ID, vcf.gz, bam, bai, vcf.gz.tbi
    // gene panel info, make&get bedfiles		ok
    // set sample relations.

    ///////////////////////////////////
    //                               //
    // SECTION 1 :: RETRIEVING DATA  //
    //                               //
    ///////////////////////////////////

    /*********************$**/
    /** Check API Validity **/
    /************************/
    protected function CheckApiKey($args)
    {
        $doc = array("goal" => "Validate an apikey", "method" => "GET/POST", "arguments" => "None", "return" => "0/1 for invalid/valid");
        ## fact that it's here, means that API key passed.
        return array("help" => $doc, "status" => "ok", "results" => "1");
    }
    /****************************************************/
    /** GET VARIOUS STATUS-DETAILS: queries/system/... **/
    /****************************************************/
    protected function GetStatus($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Get various status reports",
            "method" => "GET",
            "sub-call" => 'status item. Values: variantdb, annotation, revision, stats, query, queue, import, summarystatus',
            "arguments" => array(
                'variantdb' => "none",
                'annotation' => array("None" => 'overall annotation status', "sample_ID" => "single sample annotation status"),
                'revision' => "none",
                'stats' => "none",
                'query' => 'query_key',
                'import' => 'import_key',
                'queue' => 'none',
                'summarystatus' => 'user_id or project_id'
            ),
            "parameters" => array("type" => "Required for 'summarystatus', values (u)ser, (p)roject"),

            "return" => array(
                "variantdb" => "Platform status : Operative, Construction, LiftOver",
                "annotation" => array(
                    'no_sample_given' => 'array(running/pending annotations (strings)',
                    'sample_given' => 'Annotation status (string)'
                ),
                "revision" => array("Site" => "git revision of UI", "Database" => "git revision of Database"),
                "stats" => array(
                    "size_database" => array("database" => "size"),
                    "size_datafiles" => "Total size of stored bam/vcf files",
                    "system_df" => "Dump of 'df -h' command on the server"
                ),
                "query" => array(
                    "status" => "queued/running/finished/error",
                    "message" => "Error message if status == error",
                    "queue_position" => "position in the queue if status == queued"
                ),
                "queue" => "number of queued items",
                "import" => array(
                    "status" => "finished/error/running",
                    "runtime_output" => "Output from import script",
                    "ProjectID" => "ID of the project where samples where imported",
                    "id_map" => "Map between sample names and sample_ids"
                ),
                'summarystatus' => array(
                    "SummaryStatus" => "0/1/2",
                    "SummaryTextStatus" => "Out-Of-Date / Running / Up-To_Date",
                )

            )
        );
        if ($this->method == 'GET') {
            // PlatForm Status.
            if (strtolower($this->verb) == 'variantdb') {
                $result = $this->_runQuery("SELECT status FROM `NGS-Variants-Admin`.`SiteStatus` WHERE 1", False);
                return array("help" => $doc, "status" => "ok", "results" => $result);
            }
            // Annotation status
            elseif (strtolower($this->verb) == 'annotation') {
                // global. (update.txt + genepanels.txt)
                if (!isset($args[0])) {
                    if ($this->config['USEHPC'] == 1) {
                        $annorunning = exec("qstat -u " . $this->config['SCRIPTUSER'] . " | grep 'Ann.' | wc -l");
                    } else {
                        $annorunning = file_get_contents("../Annotations/global.update.running.txt");
                    }
                    $annorunning = rtrim($annorunning);
                    $panelrunning = rtrim(file_get_contents("../Annotations/panel.update.running.txt"));
                    $panelpending = rtrim(file_get_contents("../Annotations/update.panel.txt"));
                    $globalpending = rtrim(file_get_contents("../Annotations/update.txt"));
                    $result = array();
                    // running?
                    if ($panelrunning == 1) {
                        $result[] = 'Running GenePanel Annotations';
                    }
                    if ($annorunning > 0) {
                        $result[] = 'Running Variant Annotations';
                    }
                    // pending?
                    if ($panelpending != "") {
                        $result[] = 'Pending GenePanel Annotations';
                    }
                    if ($globalpending != "0") {
                        $result[] = 'Pending Variant Annotations';
                    }
                    if (count($result) == 0) {
                        $result[] = 'All Annotations are up-to-date';
                    }
                    return array("help" => $doc, "status" => "ok", "results" => $result);
                }
                // specific sample.
                if (isset($args[0])) {
                    if (is_numeric($args[0])) {
                        // access?
                        $access = $this->_runQuery("SELECT ps.sid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid WHERE pu.uid = '" . $this->uid . "' AND ps.sid = '" . $args[0] . "'", False);
                        if (count($access) == 0) {
                            return array("help" => $doc, "status" => "error", "msg" => "No access to requested sample");
                        }
                        // get status from database (IsAnnotated field) => is a timestamp.
                        $row = $this->_runQuery("SELECT AnnotationStatus FROM `Samples` WHERE id = '$args[0]'", False);
                        return array("help" => $doc, "status" => "ok", "results" => $row);
                    } else {
                        return array("help" => $doc, "status" => "error", "msg" => "Sample_ID must be numeric");
                    }
                }
            }
            // System status : revision
            elseif (strtolower($this->verb) == 'revision') {
                // list current system revisions: database + web interface.
                $revision = array();
                $cmd = "(echo " . $this->config['SCRIPTPASS'] . " | sudo -u " . $this->config['SCRIPTUSER'] . " -S bash -c \"cd " . $this->config['SITEDIR'] . " && git rev-parse --short HEAD \" 2>/dev/null)";
                exec($cmd, $output_ui, $exit);
                if ($exit) {
                    return array("help" => $doc, "status" => "error", "msg" => 'Could not get UI revision : ' . implode(" ", $output_ui));
                }
                $revision['Site'] = $output_ui[0];
                $cmd = "(echo " . $this->config['SCRIPTPASS'] . " | sudo -u " . $this->config['SCRIPTUSER'] . " -S bash -c \" cd " . $this->config['SCRIPTDIR'] . "/../vdb_database_revisions && git rev-parse --short HEAD \" 2>/dev/null)";
                //trigger_error($cmd);

                exec($cmd, $output_db, $exit);
                if ($exit) {
                    return array("help" => $doc, "status" => "error", "msg" => 'Could not get DB revision : ' . implode(" ", $output_db));
                }
                $revision['Database'] = $output_db[0];
                return array("help" => $doc, "status" => "ok", "results" => $revision);
            }
            // System status : disk usage
            elseif (strtolower($this->verb) == 'stats') {
                // only for admin users. (check on level)
                if ($this->UserLevel <= 1) {
                    return array("help" => $doc, "status" => "error", "msg" => 'Access denied');
                }
                $result = array();
                // database size
                $rows = $this->_runQuery("SELECT table_schema 'Data Base Name',sum( data_length + index_length ) / 1024 / 1024 'Data Base Size in MB' FROM information_schema.TABLES GROUP BY table_schema HAVING table_schema LIKE 'NGS%'");
                $result['size_database'] = array();
                foreach ($rows as $row) {
                    $result['size_database'][] = $row;
                }
                // storage size
                exec("cd " . $this->config['DATADIR'] . " && du -hs", $output, $exit);
                $output = explode("\t", $output[0]);
                $result['size_datafiles'] = $output[0];
                // system free disk space
                $output = array();
                exec("df -h | grep -v none ", $output, $exit);
                $result['system_df'] = $output;

                return array("help" => $doc, "status" => "ok", "results" => $result);
            }
            // Query status.
            elseif (strtolower($this->verb) == 'query') {
                $result = array();
                if (!isset($args[0]) || !is_numeric($args[0])) {
                    return array("help" => $doc, "status" => "error", "msg" => "No, or invalid query-key provided ");
                }
                $qid = $args[0];
                // retrieve data from disk and put in $result.
                if (!file_exists("query_results/$qid/status")) {
                    return array("help" => $doc, "status" => "error", "msg" => "No results found for provided key. Results might be expired");
                }
                if (file_exists("query_results/$qid/results")) {
                    $fh = fopen("query_results/$qid/results", 'r');
                    $line = fgets($fh);
                    if ($line == 'Status: 500') {
                        $msg = "System error. Please report the query_id $qid for investigation.\n\n";
                        $msg .= fread($fh, filesize("query_results/$qid/results"));
                        fclose($fh);
                        return array("help" => $doc, "status" => "error", "msg" => $msg);
                    }
                }

                // check if it matches the apiKey
                if (file_exists("query_results/$qid/uid")) {
                    $query_uid = rtrim(file_get_contents("query_results/$qid/uid"));
                    $api_result = $this->_runQuery("SELECT apiKey FROM `Users` WHERE id = '" . $query_uid . "'", False);
                    $apikey = md5($api_result['apiKey']);
                } elseif (file_exists("query_results/$qid/apiKey")) {
                    $apikey = rtrim(file_get_contents("query_results/$qid/apiKey"));
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Access Denied. Provided apiKey does not match query");
                }


                if ($apikey != md5($this->request['apiKey'])) {
                    return array("help" => $doc, "status" => "error", "msg" => "Access Denied. Provided apiKey does not match query results: $apikey vs " . md5($this->request['apiKey']));
                }
                $status = strtolower(rtrim(file_get_contents("query_results/$qid/status")));
                if ($status == 'finished') {
                    $result['status'] = 'finished';
                    // results available by performing GetQueryResults/$qid .
                } elseif ($status == 'error' || $status == 'failed') {
                    // get results and return the error message
                    $result['message'] = "Execution failed. See 'stderr' for details"; //file_get_contents("query_results/$qid/results");
                    // option 1 : report query
                    $stderr = '';
                    if (file_exists("query_results/$qid/section_query_ids")) {
                        $sections = file("query_results/$qid/section_query_ids");
                        foreach ($sections as $section) {
                            list($k, $idx) = explode("=", rtrim($section));
                            // read stderr of section.
                            if (file_exists("query_results/$idx/stderr")) {
                                $stderr .= "STDERR from section " . ($idx + 1) . "\n";
                                $stderr .= "======================\n";
                                $stderr .= file_get_contents("query_results/$idx/stderr");
                                $stderr .= "\n";
                            } else {
                                trigger_error("File not found : query_results/$idx/stderr");
                            }
                        }
                    }
                    // option 2 : plain query.
                    else {
                        $stderr = file_get_contents("query_results/$qid/stderr");
                    }
                    if ($stderr != '') {
                        $result['stderr'] = $stderr;
                    }
                    $result['status'] = 'error';
                } elseif ($status == 'running') {
                    $result['status'] = 'running';
                } else {
                    // check the queue
                    $queue = file("query_results/.job_queue");
                    $pos = 0;
                    $queuepos = -1;
                    foreach ($queue as $k => $v) {
                        $pos++;
                        $v = rtrim($v);
                        if ($v == $qid) {
                            $queuepos = $pos;
                            break;
                        }
                    }
                    $result['status'] = 'queued';
                    $result['queue_position'] = $queuepos;
                }
                return array("help" => $doc, "status" => "ok", "results" => $result);
            }
            // Queue status.
            elseif (strtolower($this->verb) == 'queue') {
                $result = array();
                // retrieve data from disk and put in $result.
                if (!file_exists("query_results/.job_queue")) {
                    return array("help" => $doc, "status" => "error", "msg" => "Queue File not found. Please report");
                }
                // check the queue
                $queue = file("query_results/.job_queue");
                $result['nr_queued'] = count($queue);
                return array("help" => $doc, "status" => "ok", "results" => $result);
            }
            // import
            elseif (strtolower($this->verb) == 'import') {
                $result = array();
                if (!isset($args[0]) || !is_numeric($args[0])) {
                    return array("help" => $doc, "status" => "error", "msg" => "No, or invalid import-key provided ");
                }
                $iid = $args[0];
                if (!file_exists("import_jobs/$iid/status")) {
                    return array("help" => $doc, "status" => "error", "msg" => "Status file for job $iid not found");
                }
                $result['status'] = file_get_contents("import_jobs/$iid/status");
                if ($result['status'] == 'Finished') {
                    if (file_exists("import_jobs/$iid/Process.output.txt")) {
                        $result['runtime_output'] = file_get_contents("import_jobs/$iid/Process.output.txt");
                    } else {
                        $result['runtime_output'] = 'Runtime output not found';
                    }
                    if (file_exists("import_jobs/$iid/pid.txt")) {
                        $pid = file_get_contents("import_jobs/$iid/pid.txt");
                        $pid = trim($pid);
                        $result['ProjectID'] = $pid;
                    }
                    if (file_exists("import_jobs/$iid/id_map.txt")) {
                        $result['id_map'] = array();
                        $fh = fopen("import_jobs/$iid/id_map.txt", 'r');
                        while (($line = fgets($fh)) !== false) {
                            $line = trim($line);
                            $c = explode("=", $line);
                            $result['id_map'][$c[1]] = $c[0];
                        }
                        fclose($fh);
                    }
                } elseif ($result['status'] == 'error') {
                    if (file_exists("import_jobs/$iid/scratch/stderr")) {
                        $result['msg'] = file_get_contents("import_jobs/$iid/scratch/stderr");
                    } else {
                        $result['msg'] = 'No error msg found';
                    }
                }
                return array("help" => $doc, "status" => "ok", "results" => $result);
            }
            // SummaryStatus
            elseif ((strtolower($this->verb) == 'summarystatus')) {
                $map = array("0" => "Out-Of-Date", "1" => "Running", "2" => "Up-To-Date");
                if (!isset($args[0]) || !is_numeric($args[0])) {
                    return array("help" => $doc, "status" => "error", "msg" => 'No, or invalid item identifier provided');
                }
                $id = $args[0];
                if (!isset($this->request['type']) || !preg_match("/^[up]/", strtolower($this->request['type']))) {
                    return array("help" => $doc, "status" => "error", "msg" => 'No, or invalid Summary Type (type) provided');
                }
                $type = substr(strtolower($this->request['type']), 0, 1);
                if ($type == 'u') {
                    $r = $this->_runQuery("SELECT `SummaryStatus` FROM `Users` WHERE id = '$id'", False);
                    if (count($r) == 0) {
                        return array("help" => $doc, "status" => "error", "msg" => "Could not fetch user status");
                    }
                    return array("help" => $doc, "status" => "ok", "results" => array('SummaryStatus' => $r['SummaryStatus'], "SummaryTextStatus" => $map[$r['SummaryStatus']]));
                } elseif ($type == 'p') {
                    $r = $this->_runQuery("SELECT `SummaryStatus` FROM `Projects` WHERE id = '$id'", False);
                    if (count($r) == 0) {
                        return array("help" => $doc, "status" => "error", "msg" => "Could not fetch Project status");
                    }
                    return array("help" => $doc, "status" => "ok", "results" => array("SummaryStatus" => $r['SummaryStatus'], "SummaryTextStatus" => $map[$r['SummaryStatus']]));
                }
                // check for type is upfront.

            } else {
                return array("help" => $doc, "status" => "error", "msg" =>  "Verb '" . $this->verb . "' not supported.");
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }


    /**************************************************************************/
    /* Get user details, either by provided email, or for the current apikey. */
    /**************************************************************************/
    protected function GetUserDetails($args)
    {
        $doc = array(
            "goal" => "Request user details",
            "method" => "GET",
            "arguments" => 'email address of the user. Only allowed as admin user',
            "return" => "Array(id, LastName, FirstName, email, level, SummaryStatus)",
        );
        if ($this->method == 'GET') {
            // email specified. only allowed for admin users.
            if (isset($this->verb) && $this->verb != '') {
                if (filter_var($this->verb, FILTER_VALIDATE_EMAIL)) {
                    if ($this->UserLevel < 3) {
                        return array("help" => $doc, "status" => "error", "msg" => "Insufficient UserLevel for this action");
                    }
                    $row = $this->_runQuery("SELECT id, LastName, FirstName,email,level,SummaryStatus FROM `Users` WHERE email = '" . $this->_SqlEscapeValues($this->verb) . "'", False);
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Invalid argument provided. Must be email address.");
                }
            } elseif (isset($args[0])) {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid argument provided. Must be email address.");
            }
            // details of current api key
            elseif (isset($this->apiKey) && $this->apiKey != '') {
                $row = $this->_runQuery("SELECT id, LastName, FirstName,email,level,SummaryStatus FROM `Users` WHERE apiKey = '" . $this->_SqlEscapeValues($this->apiKey) . "'", False);
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "No ApiKey provided");
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
        return array("help" => $doc, "status" => "ok", "results" => $row);
    }
    /***********************************************/
    /** LIST ALL STORED FILTERS // FILTER DETAILS **/
    /***********************************************/
    protected function SavedFilters($args)
    {
        // documentation.
        $doc = array(
            "goal" => "List all saved filtersettings, or details of one specified filter (by id)",
            "method" => "GET",
            "arguments" => 'none or numeric filter-id',
            "return" => array(
                "List_ALL" => "Array(Array(fid, FilterName, Comments, NrRules)",
                "List_ONE" => "Array(FilterName, Created, FilterTree, Comments, FilterSettings)"
            ),
        );
        if ($this->method == 'GET') {
            // details of specific filter.
            if (isset($args[0]) && is_numeric($args[0])) {
                $args[0] = $this->_SqlEscapeValues($args[0]);
                // trigger_error("SELECT FilterName, Created, FilterTree, Comments, FilterSettings FROM `Users_x_FilterSettings` WHERE uid = " . $this->uid . " AND fid = '" . $args[0] . "'");
                $rows = $this->_runQuery("SELECT FilterName, Created, FilterTree, Comments, FilterSettings FROM `Users_x_FilterSettings` WHERE uid = " . $this->uid . " AND fid = '" . $args[0] . "'", False);
                if (empty($rows)) {
                    $rows = $this->_runQuery("SELECT f.FilterName, f.Created, f.FilterTree, f.Comments, f.FilterSettings,f.uid AS Owner FROM `Users_x_FilterSettings` f JOIN `Users_x_Shared_Filters` s ON s.fid = f.fid WHERE s.uid = " . $this->uid . " AND f.fid = '" . $args[0] . "'", False);
                }
                if (empty($rows)) {
                    return array("help" => $doc, "status" => "error", "msg" => "No access to specified filter");
                }
                $translate = array(
                    'Effect_On_Transcript|RefSeq_GeneLocation' => 'Variants_x_ANNOVAR_ncbigene_GeneLocation',
                    'Effect_On_Transcript|RefSeq_VariantType' => 'Variants_x_ANNOVAR_ncbigene_VariantType',
                    'Effect_On_Transcript|RefGene_GeneLocation' => 'Variants_x_ANNOVAR_refgene_GeneLocation',
                    'Effect_On_Transcript|RefGene_VariantType' => 'Variants_x_ANNOVAR_refgene_VariantType',
                    'Effect_On_Transcript|UCSC_GeneLocation' => 'Variants_x_ANNOVAR_knowngene_GeneLocation',
                    'Effect_On_Transcript|UCSC_VariantType' => 'Variants_x_ANNOVAR_knowngene_VariantType',
                    'Effect_On_Transcript|Ensembl_GeneLocation' => 'Variants_x_ANNOVAR_ensgene_GeneLocation',
                    'Effect_On_Transcript|Ensembl_VariantType' => 'Variants_x_ANNOVAR_ensgene_VariantType',
                    'Oncology_Specific|COSMIC_v70_Occurence_in_tissue' => 'Variants_x_ANNOVAR_cosmic70_tissue',
                    'ClinVar_SNPs|Classification' => 'Variants_x_ClinVar_ncbigene_Class',
                    'ClinVar_SNPs|Variant_Effect' => 'Variants_x_ClinVar_ncbigene_Effect',
                    'ClinVar_SNPs|Match_Type' => 'Variants_x_ClinVar_ncbigene_MatchType',
                    'snpEff|Effect' => 'Variants_x_snpEff_Effect',
                    'snpEff|Effect_Impact' => 'Variants_x_snpEff_EffectImpact',
                    'snpEff|Functional_Class' => 'Variants_x_snpEff_FunctionalClass',
                    'snpEff|Gene_Coding' => 'Variants_x_snpEff_GeneCoding',
                    'snpEff|Transcript_BioType' => 'Variants_x_snpEff_TranscriptBiotype'
                );
                $result = $rows;
                $settings_array = array();
                foreach (explode('@@@', $result['FilterSettings']) as $kv) {
                    list($k, $v) = explode('|||', $kv);
                    $settings_array[$k] = $v;
                }
                foreach ($settings_array as $k => $v) {
                    if (substr($k, 0, 5) != 'param') {
                        continue;
                    }
                    $idx = substr($k, 5);
                    $cat = "category$idx";
                    $vsql = $this->_SqlEscapeValues($v);
                    // if custom annation field: translate here.
                    if ($settings_array[$cat] == 'Custom_VCF_Fields') {
                        // get the field name.

                        $r = $this->_runQuery("SELECT `field_name`,`value_type` FROM `Custom_Annotations` WHERE aid = '$vsql'", False);
                        $settings_array[$k] = $r['field_name'] . '___' . $r['value_type'];
                        // list items are translated.
                        if ($r['value_type'] == 'list') {
                            $args = explode("__", $settings_array["argument$idx"]);
                            $decode_args = "";
                            foreach ($args as $i => $a) {
                                if ($a == '') {
                                    continue;
                                }
                                $a = $this->_SqlEscapeValues($a);
                                $r = $this->_runQuery("SELECT `field_value` FROM `Custom_Annotations_x_Value_Codes` WHERE aid = '$vsql' AND `field_code` = '$a'", False);
                                $decode_args .= $r['field_value'] . '__';
                            }
                            $settings_array["argument$idx"] = $decode_args;
                        }
                    }
                    // if value in translate => get the corresponding decoded stuff from DB.
                    elseif (array_key_exists($settings_array[$cat] . '|' . $v, $translate)) {
                        // get values.
                        $args = explode("__", $settings_array["argument$idx"]);
                        $decode_args = "";
                        foreach ($args as $i => $a) {
                            if ($a == '') {
                                continue;
                            }
                            $a = $this->_SqlEscapeValues($a);
                            $r = $this->_runQuery("SELECT Item_Value FROM `Value_Codes` WHERE id = '$a' AND Table_x_Column = '" . $translate[$settings_array[$cat] . '|' . $v] . "'", False);
                            if (!$r) {
                                trigger_error("WARNING : $a could not be decoded for " . $settings_array[$cat] . '|' . "$v", E_USER_ERROR);
                            }
                            $decode_args .= $r['Item_Value'] . '__';
                        }
                        $settings_array["argument$idx"] = $decode_args;
                    }
                }
                // rebuild settings.
                $decode_settings = '';
                foreach ($settings_array as $key => $value) {
                    $decode_settings .= $key . '|||' . $value . '@@@';
                }
                $result['FilterSettings'] = substr($decode_settings, 0, -3);
            } elseif (isset($this->verb) && $this->verb != '') {
                return array("help" => $doc, "status" => "error", "msg" => "Expected a numeric argument");
            }
            // list of all filters
            else {
                $rows = $this->_runQuery("SELECT fid, FilterName, Comments, FilterSettings FROM `Users_x_FilterSettings` WHERE uid = " . $this->uid);
                $result = array();
                foreach ($rows as $row) {
                    foreach ($row as $key => $value) {
                        if ($key == 'FilterSettings') {
                            $p = explode("@@@", $value);
                            $nrR = 0;
                            foreach ($p as $k => $v) {
                                if (substr($v, 0, 3) == 'cat') {
                                    $nrR++;
                                }
                            }
                            $row['nrRules'] = $nrR;
                            unset($row['FilterSettings']);
                        }
                    }
                    $result[] = $row;
                }
            }
            //$result['help'] = $doc;
            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }
    // internal function. filter details without decoding
    protected function SavedFiltersEncoded($args)
    {
        // documentation.
        $doc = array(
            "goal" => "List details of one specified filter (by id), without decoding numeric values",
            "method" => "GET",
            "arguments" => 'numeric filter-id',
            "return" => "Array(FilterName, Created, FilterTree, Comments, FilterSettings)"

        );
        if ($this->method == 'GET') {
            // details of specific filter.
            if (isset($args[0]) && is_numeric($args[0])) {
                $args[0] = $this->_SqlEscapeValues($args[0]);
                $rows = $this->_runQuery("SELECT FilterName, Created, FilterTree, Comments, FilterSettings FROM `Users_x_FilterSettings` WHERE uid = " . $this->uid . " AND fid = '" . $args[0] . "'", False);
                if (empty($rows)) {
                    $rows = $this->_runQuery("SELECT f.FilterName, f.Created, f.FilterTree, f.Comments, f.FilterSettings,f.uid AS Owner FROM `Users_x_FilterSettings` f JOIN `Users_x_Shared_Filters` s ON s.fid = f.fid WHERE s.uid = " . $this->uid . " AND f.fid = '" . $args[0] . "'", False);
                }
                if (empty($rows)) {
                    return array("help" => $doc, "status" => "error", "msg" => "No access to specified filter");
                }
                // permissions ok.
                $result = $rows;

                $settings_array = array();
                foreach (explode('@@@', $result['FilterSettings']) as $kv) {
                    if (strpos($kv, '|||') !== false) {
                        list($k, $v) = explode('|||', $kv);
                        $settings_array[$k] = $v;
                    } else {
                        $settings_array[$kv] = NULL;
                    }
                }
                foreach ($settings_array as $k => $v) {
                    if (substr($k, 0, 5) != 'param') {
                        continue;
                    }
                    $idx = substr($k, 5);
                    $cat = "category$idx";
                }
                // rebuild settings.
                $decode_settings = '';
                foreach ($settings_array as $key => $value) {
                    $decode_settings .= $key . '|||' . $value . '@@@';
                }
                $result['FilterSettings'] = substr($decode_settings, 0, -3);
                return array("help" => $doc, "status" => "ok", "results" => $result);
            }
            // list of all filters
            else {
                return  array("help" => $doc, "status" => "error", "msg" => "no fid specified");
            }
        } else {
            return  array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }



    /****************************************************/
    /** LIST ALL STORED ANNOTATION SETS // SET DETAILS **/
    /****************************************************/
    protected function SavedAnnotations($args)
    {

        // documentation.
        $doc = array(
            "goal" => "List all saved annotation-lists, or details of one specified annotation list (by id)",
            "method" => "GET",
            "arguments" => 'none or numeric annotation-list-id',
            "return" => array(
                "List_ALL" => "Array(Array(aid, AnnotationName, Comments, AnnotationSettings)",
                "List_ONE" => "Array(AnnotationName, Created, AnnotationSettings, Comments)"
            ),
        );
        if ($this->method == 'GET') {
            // details of specific annotation set.
            if (isset($args[0]) && is_numeric($args[0])) {
                // permissions should be checked before using this function.
                $rows = $this->_runQuery("SELECT AnnotationName, Created, AnnotationSettings, Comments FROM `Users_x_Annotations` WHERE uid = " . $this->uid . " AND aid = '" . $args[0] . "'", False);
                if (empty($rows)) {
                    $rows = $this->_runQuery("SELECT a.AnnotationName, a.Created, a.AnnotationSettings, a.Comments, ua.uid AS Owner FROM `Users_x_Annotations` a JOIN `Users_x_Shared_Annotations` ua ON ua.aid = a.aid WHERE ua.uid = " . $this->uid . " AND a.aid = '" . $args[0] . "'", False);
                }
                if (empty($rows)) {
                    return array("help" => $doc, "status" => "error", "msg" => "No access to specified annotations");
                }

                // decode the value_codes
                // go over annoations settings to correctly handle custom vcf fields.
                $result = $rows;
                $annos = explode("@@@", $result['AnnotationSettings']);
                foreach ($annos as $i => $kv) {
                    list($k, $v) = explode(";", $kv);
                    if ($k == 'Custom_VCF_Fields') {
                        $r = $this->_runQuery("SELECT `field_name`,`value_type` FROM `Custom_Annotations` WHERE aid = '$v' LIMIT 1", False);

                        $annos[$i] = $k . ';' . $r['field_name'] . '___' . $r['value_type'];
                    }
                }
                $result['AnnotationSettings'] = implode("@@@", $annos);
            }
            // invalid (non-numeric)
            elseif (isset($this->verb) && $this->verb != '') {
                return array("help" => $doc, "status" => "error", "msg" => "Expected a numeric argument");
            }
            // list of all annotation sets (private)
            else {
                $rows = $this->_runQuery("SELECT aid, AnnotationName, Comments, AnnotationSettings FROM `Users_x_Annotations` WHERE uid = " . $this->uid);
                $result = array();
                foreach ($rows as $row) {
                    foreach ($row as $key => $value) {
                        if ($key == 'AnnotationSettings') {
                            $p = explode("@@@", $value);
                            $row['nrItems'] = count($p);
                            unset($row['AnnotationSettings']);
                        }
                    }
                    $result[] = $row;
                }
            }
            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }


    /****************************************/
    /** LIST ALL SAMPLES // SAMPLE DETAILS **/
    /****************************************/
    protected function Samples($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Retrieve sample details for all or one sample",
            "method" => "GET",
            "arguments" => 'None, or a numeric sample_id',
            "return" => array(
                "ONE_SAMPLE" => "ASSOC_Array(sample_id, sample_name, gender, affected, IsControl, clinical_freetext, marked_as_finished, control_sample, project_id, annotation_status, project_name, project_created, clinical_hpo, pedigree, nr_variants, Saved_Results => Array()) ",
                "ALL_SAMPLES" => "Array(Array(id, sample_name, gender, project_name, project_id, affected, IsControl)"
            )
        );
        if ($this->method == 'GET') {
            /////////////////////////////////
            // details of specific sample. //
            /////////////////////////////////
            if (isset($args[0]) && is_numeric($args[0])) {
                // general sample details.
                $result = $this->_runQuery("SELECT s.id AS sample_id, s.Name AS sample_name, s.gender AS gender, s.affected,s.IsControl,s.clinical AS clinical_freetext,s.filed AS marked_as_finished, s.IsControl AS control_sample, ps.pid AS project_id, s.AnnotationStatus AS annotation_status FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON s.id = ps.sid AND ps.pid = pu.pid WHERE pu.uid = " . $this->uid . " AND s.id = '" . $args[0] . "'", False);
                if (empty($result)) {
                    return array("help" => $doc, "status" => "error", "msg" => "Sample not found, or no access to this sample");
                }
                $result['clinical_freetext'] = utf8_encode($result['clinical_freetext']);
                // project details.
                $row = $this->_runQuery("SELECT Name AS project_name, created AS project_created FROM `Projects` WHERE id = " . $result['project_id'], False);
                foreach ($row as $k => $v) {
                    $result[$k] = $v;
                }
                $row = $this->_runQuery("SELECT pu.editvariant AS Edit_Variants, pu.editclinic AS Edit_Clinic, pu.editsample AS Manage_Sample FROM `Projects_x_Users` pu WHERE uid = " . $this->uid . " AND pid = " . $result['project_id'], False);
                $result['Access_Rights'] = $row;
                // phenotype info.
                $result['clinical_hpo'] = array();
                $crows = $this->_runQuery("SELECT ht.Name as hpo_term, ht.Definition AS hpo_definition, ht.Comment AS hpo_comment, ht.xref AS hpo_xref FROM `Samples_x_HPO_Terms` sh JOIN `HPO_Terms` ht ON  ht.id = sh.tid WHERE sh.sid = " . $result['sample_id']);
                foreach ($crows as $row) {
                    $result['clinical_hpo'][] = $row;
                }
                // Family relations.
                $result['pedigree'] = $this->_GetFamily($result['sample_id']);
                // Nr.Variants
                $row = $this->_runQuery("SELECT COUNT(vid) AS NrVids FROM `Variants_x_Samples` WHERE sid = " . $result['sample_id'], False);
                $result['nr_variants'] = $row['NrVids'];
                // saved results.
                $srows = $this->_runQuery("SELECT ss.set_id, ss.set_name, ss.set_comments, ss.set_vids, ss.date, ss.Validated, ss.Validation_Comments, ss.validation_uid, ss.delete_uid, ss.deleted, ss.delete_comments, u.LastName, u.FirstName FROM `Samples_x_Saved_Results` ss JOIN `Users` u ON u.id = ss.uid WHERE ss.sid = '" . $args[0] . "' ORDER BY ss.set_id");
                $result['Saved_Results'] = array();
                foreach ($srows as $row) {
                    $set_id = $row['set_id'];
                    #unset( $row['set_id'] );
                    $nr_v = count(explode(",", $row['set_vids']));
                    unset($row['set_vids']);
                    $row['validated_by'] = '';
                    if ($row['Validated'] != 0) {
                        $sr = $this->_runQuery("SELECT FirstName, LastName FROM `Users` WHERE id = '" . $row['validation_uid'] . "'", False);
                        $row['validated_by'] = $sr['LastName'] . " " . substr($sr['FirstName'], 0, 1) . ".";
                    }
                    unset($row['validation_uid']);
                    $row['deleted_by'] = '';
                    if ($row['deleted'] != 0) {
                        $sr = $this->_runQuery("SELECT FirstName, LastName FROM `Users` WHERE id = '" . $row['delete_uid'] . "'", False);
                        $row['deleted_by'] = $sr['LastName'] . " " . substr($sr['FirstName'], 0, 1) . ".";
                    }
                    unset($row['delete_uid']);
                    $result['Saved_Results'][] = $row;
                }
            } elseif (isset($this->verb) && $this->verb != '') {
                return array("help" => $doc, "status" => "error", "msg" => "Expected numeric argument");
            }
            ///////////////////////////////////////////////
            // list of all samples, projects and gender. //
            ///////////////////////////////////////////////
            else {
                $result = $this->_runQuery("SELECT s.id, s.Name AS sample_name, s.gender, p.Name as project_name, p.id AS project_id, s.affected,s.IsControl FROM `Samples` s JOIN `Projects` p JOIN `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON s.id = ps.sid AND p.id = ps.pid AND p.id = pu.pid WHERE pu.uid = " . $this->uid . " ORDER BY s.id");
            }
            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }

    /***********************/
    /** LIST ALL PROJECTS */
    /**********************/
    protected function Projects($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Retrieve details for all or one project",
            "method" => "GET",
            "arguments" => 'None, or a numeric sample_id',
            "return" => array(
                "ONE_PROJECT" => "ASSOC_Array(project_name, project_created, SummaryStatus,access_rights, nr_samples) ",
                "ALL_PROJECTS" => "Array(Array(project_id, project_name, project_created, SummaryStatus,access_rights, nr_samples)"
            )
        );
        if ($this->method == 'GET') {
            /////////////////////////////////
            // details of specific project.//
            /////////////////////////////////
            if (isset($args[0]) && is_numeric($args[0])) {
                // project details.
                $result = $this->_runQuery("SELECT p.Name AS project_name, p.created AS project_created,p.SummaryStatus FROM `Projects` p JOIN `Projects_x_Users` pu ON p.id = pu.pid WHERE pu.uid = " . $this->uid . " AND p.id = " . $args[0], False);
                // no access.
                if (!count($result)) {
                    return array("help" => $doc, "status" => "error", "msg" => "No access to requested project");
                }
                // permissions
                $row = $this->_runQuery("SELECT pu.editvariant AS Edit_Variants, pu.editclinic AS Edit_Clinic, pu.editsample AS Manage_Sample FROM `Projects_x_Users` pu WHERE uid = " . $this->uid . " AND pid = " . $args[0], False);
                $result['Access_Rights'] = $row;
                // nr_samples
                $row = $this->_runQuery("SELECT COUNT(sid) AS nr_samples FROM `Projects_x_Samples` WHERE pid = " . $args[0], False);
                $result['nr_samples'] = $row['nr_samples'];
                // sample details.
                $rows = $this->_runQuery("SELECT s.id, s.Name AS sample_name, s.gender, s.affected,s.IsControl FROM `Samples` s JOIN `Projects_x_Samples` ps ON s.id = ps.sid WHERE ps.pid = " . $args[0]);
                $result['samples'] = $rows;
            }
            // invalid argument
            elseif (isset($this->verb) && $this->verb != '') {
                return array("help" => $doc, "status" => "error", "msg" => "Expected a numeric argument");
            }
            ///////////////////////////
            // list of all projects  //
            ///////////////////////////
            else {
                $result = $this->_runQuery("SELECT p.id AS project_id, p.Name AS project_name, p.created AS project_created,p.SummaryStatus FROM `Projects` p JOIN `Projects_x_Users` pu ON p.id = pu.pid WHERE pu.uid = " . $this->uid);
                for ($idx = 0; $idx < count($result); $idx++) {
                    // permissions
                    $row = $this->_runQuery("SELECT pu.editvariant AS Edit_Variants, pu.editclinic AS Edit_Clinic, pu.editsample AS Manage_Sample FROM `Projects_x_Users` pu WHERE uid = " . $this->uid . " AND pid = " . $result[$idx]['project_id'], False);
                    $result[$idx]['Access_Rights'] = $row;
                    // nr_samples
                    $row = $this->_runQuery("SELECT COUNT(sid) AS nr_samples FROM `Projects_x_Samples` WHERE pid = " . $result[$idx]['project_id'], False);
                    $result[$idx]['nr_samples'] = $row['nr_samples'];
                }
            }
            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }

    /********************/
    /** GET REPORT ID  **/
    /********************/
    protected function GetReportID($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Get ID for report-definition(s)",
            "method" => "GET",
            "arguments" => 'Report definition name (string), usergroup name (string) or none',
            "return" => array(
                "ONE_REPORT_DEFINITION" => "ASSOC_Array(rid)",
                "USERGROUP_REPORT_DEFINITIONS" => "Array(rid => ASSOC_Array(rid,ReportName,Description,Public,gid,UsergroupName))",
                "ALL_REPORT_DEFINITIONS" => "Array(rid => ASSOC_Array(rid,Name,Description,Public,Sections => array(rsid,Name,Description,FilterSet,Annotations)))"
            )
        );
        if ($this->method != 'GET') {
            return array("help" => $doc, "status" => "error", "msg" => "Only GET requests supported");
        }

        // get uid
        $uid = $this->uid;

        if (isset($this->request['report'])) {
            // get rid
            $rows = $this->_runQuery("SELECT rid FROM `Report_Definitions` WHERE Name = '" . $this->_SqlEscapeValues($this->request['report']) . "'");

            if (count($rows) == 0) {
                return array("help" => $doc, "status" => "error", "msg" => "Non-existing reportname given: " . $this->request['report']);
            } elseif (count($rows) > 1 && isset($rows[0]) && is_array($rows[0])) {
                return array("help" => $doc, "status" => "error", "msg" => "Multiple reports with name " . $this->request['report']);
            }
            $rid = $rows[0]['rid'];

            // check permission
            $rows = $this->_runQuery("SELECT rid FROM `Report_Definitions` WHERE rid = '" . $rid . "' AND (Owner = '" . $uid . "' OR Public = 1)", False);
            if (count($rows) == 0) {
                $rows = $this->_runQuery("SELECT rid FROM `Users_x_Report_Definitions` WHERE rid = '" . $rid . "' AND uid = '" . $uid . "'");
                if (count($rows) == 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "No permission for report " . $this->request['report']);
                }
            }
            return array("help" => $doc, "status" => "ok", "results" => array('rid' => $rid));
        } elseif (isset($this->request['usergroup'])) {
            $usergroup = $this->_SqlEscapeValues($this->request['usergroup']);
            $query = "SELECT rd.rid, rd.Name AS ReportName, rd.Description, rd.Public, ugrd.gid, ug.name AS UsergroupName FROM `Report_Definitions` rd
            JOIN `Usergroups_x_Report_Definitions` ugrd ON rd.rid = ugrd.rid
            JOIN `Usergroups` ug ON ug.id = ugrd.gid
            WHERE rd.Owner = '$uid' AND ug.name = '$usergroup'";
            $rows = $this->_runQuery($query);
            $results = array();
            foreach ($rows as $row) {
                $results[$row['rid']] = $row;
            }
            return array("help" => $doc, "status" => "ok", "results" => $results);

        } else {
            // get all report definitions that are available to this user

            $def_statement = "SELECT rid,Name,Description,Public,Sections FROM `Report_Definitions` WHERE Owner = '" . $uid . "'";

            $rows = $this->_runQuery("SELECT rid FROM `Users_x_Report_Definitions` WHERE uid = '" . $uid . "'");

            if (count($rows) > 0) {
                $shared_rids = array();
                foreach ($rows as $row) {
                    array_push($shared_rids, $row['rid']);
                }
                $def_statement .= " OR rid in (" . implode(",", $shared_rids) . ")";
            }

            $rows = $this->_runQuery($def_statement);
            $results = array();

            // expand sections
            foreach ($rows as $row) {
                $results[$row['rid']] = $row;

                $sections = array();
                $sections_rsid = explode(",", $row['Sections']);
                foreach ($sections_rsid as $rsid) {
                    $srow = $this->_runQuery("SELECT rsid,Name,Description,FilterSet,Annotations FROM `Report_Sections` WHERE rsid = '" . $rsid . "'", False);
                    $sections[$rsid] = $srow;
                }
                unset($rsid);
                $results[$row['rid']]['Sections'] = $sections;
            }

            return array("help" => $doc, "status" => "ok", "results" => $results);
        }
    }


    /************************/
    /* GET ID OF GENE PANEL */
    /************************/
    protected function GenePanel($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Get Lis of all gene panels the user has access to, or details of a single panel ",
            "method" => "GET",
            "arguments" => 'Gene_Panel_ID (numeric, optional)',
            "return" => array(
                "All_Panels" => "Array(array('id','Name','Description','Public','LastEdit','Owner_LastName','Owner_FirstName'",
                "One_Panel" => array(
                    'id' => 'gene_panel_id',
                    'Name' => 'gene panel name',
                    'Description' => 'gene panel description',
                    'Public' => '0/1 : public access enabled',
                    'LastEdit' => 'DateTime',
                    'Owner_LastName' => 'Name of gene-panel owner',
                    'Owner_FirstName' => 'Name of gene-panel owner',
                    'BED_present' => "array('cds' => '0/1', 'full' => '0/1')",
                    'Transcript_BED_present' => "array('cds' => '0/1', 'full' => '0/1')",
                    'genes' => "array(array('NCBI_GeneID', 'Symbol', 'Comment', 'LastEdit'))"
                )
            ),
        );
        if ($this->method == 'GET') {
            ///////////////////////////////
            // details of specific panel //
            ///////////////////////////////
            if (isset($args[0]) && is_numeric($args[0])) {
                // general details.
                $result = $this->_runQuery("SELECT gp.id , gp.Name , gp.Description, gp.Public , gp.LastEdit, u.LastName AS Owner_LastName, u.FirstName AS Owner_FirstName FROM `GenePanels` gp JOIN `GenePanels_x_Users` gpu JOIN `Users` u ON gp.id = gpu.gpid AND gp.Owner = u.id WHERE (gp.Owner = " . $this->uid . " OR gpu.uid = " . $this->uid . " OR gp.Public = 1) AND gp.id = '" . $args[0] . "' GROUP BY gp.id", False);
                if (count($result) == 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "Gene Panel not found, or no access to this gene panel");
                }
                $gpid = $result['id'];
                // bed file present?
                $result['BED_present'] = array();

                $result['BED_present']['cds']  = file_exists("../BED_Files/$gpid.1.cds.bed") ? 1 : 0;
                $result['BED_present']['full'] = file_exists("../BED_Files/$gpid.1.full.bed") ? 1 : 0;
                $result['Transcript_BED_present']['cds']  = file_exists("../BED_Files/$gpid.0.cds.bed") ? 1 : 0;
                $result['Transcript_BED_present']['full'] = file_exists("../BED_Files/$gpid.0.full.bed") ? 1 : 0;

                // panel genes
                $rows = $this->_runQuery("SELECT gid AS NCBI_GeneID, Symbol, `Comment`, LastEdit FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = " . $result['id']);
                $result['genes'] = $rows;
                return array("help" => $doc, "status" => "ok", 'results' => $result);
            } elseif (isset($this->verb) && $this->verb != '') {
                return array("help" => $doc, "status" => "error", "msg" => "Expected a numeric argument");
            }
            /////////////////////////
            // list of all panels. //
            /////////////////////////
            else {
                $rows = $this->_runQuery("SELECT DISTINCT(gp.id) , gp.Name , gp.Description, gp.Public , gp.LastEdit, u.LastName AS Owner_LastName, u.FirstName AS Owner_FirstName FROM `GenePanels` gp JOIN `GenePanels_x_Users` gpu JOIN `Users` u ON gp.id = gpu.gpid AND gp.Owner = u.id WHERE (gp.Owner = " . $this->uid . " OR gpu.uid = " . $this->uid . " OR gp.Public = 1) ");
                return array("help" => $doc, "status" => "ok", 'results' => $rows);
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }


    /*********************/
    /* CREATE A BED FILE */
    /*********************/
    protected function MakeGenePanelBED($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Make BED file for a specified Gene Panel",
            "method" => "GET",
            "arguments" => 'Gene_Panel_ID (numeric)',
            "parameters" => array(
                "summary" => "0/1 : summarize transcripts into a meta-transcript",
                'type' => 'ncbigene / refseq (default). Gene Annotations to use in BED file'

            ),
            "return" => array(
                'details' => array('id', 'LastEdit', 'Name', 'Description', 'Public', 'Owner_LastName', 'Owner_FirstName'),
                'cds' => array(
                    'path' => 'internal location',
                    'genome.build' => 'hg19/hg38',
                    'report' => 'comments during generation',
                    'nr.genes' => 'Number of genes in BED file',
                    'target.size' => 'total region size over all entries'
                ),
                'full' =>  array(
                    'path' => 'internal location',
                    'genome.build' => 'hg19/hg38',
                    'report' => 'comments during generation',
                    'nr.genes' => 'Number of genes in BED file',
                    'target.size' => 'total region size over all entries'
                )
            )
        );

        if ($this->method == 'GET') {
            // valid ID?
            if (!isset($args[0]) || !is_numeric($args[0]) || (isset($this->verb) && $this->verb != '')) {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid GenePanel_ID provided.");
            }
            $gpid = $args[0];
            // access ?
            $gp_details = $this->_runQuery("SELECT gp.id,gp.LastEdit, gp.Name, gp.Description, gp.Public, u.LastName AS Owner_LastName, u.FirstName AS Owner_FirstName FROM `GenePanels` gp JOIN `GenePanels_x_Users` gpu JOIN `Users` u ON gp.id = gpu.gpid AND gp.Owner = u.id WHERE (gp.Owner = " . $this->uid . " OR gpu.uid = " . $this->uid . " OR gp.Public = 1) AND gp.id = '" . $args[0] . "' GROUP BY gp.id", False);
            if (count($gp_details) == 0) {
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: No access to this gene panel");
            }

            // read in gene panel.
            $gp = array();
            $rows = $this->_runQuery("SELECT gid, Symbol FROM `GenePanels_x_Genes_ncbigene` WHERE gpid = '$gpid'");
            $nr_panel_genes = count($rows);
            foreach ($rows as $row) {
                // replace C(.{1,2})ORF(\d+) by C(.{1,2})orf(\d+)
                if (preg_match('/^C[0-9XYMT]{1,2}O[Rr][Ff](\d+)$/', $row['Symbol'])) {
                    $row['Symbol'] = preg_replace("/[O][Rr][Ff]/", "orf", $row['Symbol']);
                }
                $gp[$row['Symbol'] . "|" . $row['gid']] = $row['Symbol'];
            }

            // by default, the transcripts are summarized.
            if (array_key_exists("summary", $this->request)) {
                if ($this->request['summary'] === "0" || strtolower($this->request['summary']) === 'false') {
                    $summary = 0;
                } elseif ($this->request['summary'] === "1" || strtolower($this->request['summary']) === 'true') {
                    $summary = 1;
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Value of 'summary' must be 0,1,true or false");
                }
            } else {
                $summary = 1;
            }
            // type : default ncbigene/refseq. optional: refseq
            if (array_key_exists("type", $this->request)) {
                $type = $this->request['type'];
            } else {
                $type = 'refseq';
            }
            // replace by shared function with MakeGeneListBED
            $bed_result = $this->GetBED($gp, $summary, $type);
            if (array_key_exists('ERROR', $bed_result)) {
                trigger_error("ERROR generating bed file: " . $bed_result['ERROR'], E_USER_ERROR);
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: " . $bed_result['ERROR']);
            }


            // write CDS BED
            $fh = fopen("../BED_Files/$gpid.$summary.cds.bed", "w");
            if (!$fh) {
                trigger_error("ERROR : Could not open BED File for writing : ED_Files/$gpid.$summary.cds.bed", E_USER_ERROR);
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: Failed to open CDS BED file for writing");
            }
            fwrite($fh, $bed_result['cds_bed']);
            fclose($fh);

            // write FULL BED
            $fh = fopen("../BED_Files/$gpid.$summary.full.bed", "w");
            if (!$fh) {
                trigger_error("ERROR : Could not open BED File for writing: ../BED_Files/$gpid.$summary.full.bed", E_USER_ERROR);
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: Failed to open FULL BED file for writing");
            }
            fwrite($fh, $bed_result['full_bed']);
            fclose($fh);

            // write report to file.
            $fh = fopen("../BED_Files/$gpid.$summary.cds.report", 'w');
            if (!$fh) {
                trigger_error("ERROR : Could not open cds report File for writing: ../BED_Files/$gpid.$summary.cds.report", E_USER_ERROR);
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: Failed to open cds report file for writing");
            }
            fwrite($fh, $bed_result['cds_report']);
            fclose($fh);
            $fh = fopen("../BED_Files/$gpid.$summary.full.report", 'w');
            if (!$fh) {
                trigger_error("ERROR : Could not open full report File for writing: ../BED_Files/$gpid.$summary.full.report", E_USER_ERROR);
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: Failed to open full report file for writing");
            }
            fwrite($fh, $bed_result['full_report']);
            fclose($fh);
            // write a details file with panel details.
            $fh = fopen("../BED_Files/$gpid.$summary.cds.details", 'w');
            if (!$fh) {
                trigger_error("ERROR : Could not open cds details File for writing: ../BED_Files/$gpid.$summary.cds.details", E_USE_ERROR);
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: Failed to open cds details file for writing");
            }
            fwrite($fh, "last.edit=" . $gp_details['LastEdit'] . "\n");
            fwrite($fh, "genome.build=" . $bed_result['build'] . "\n");
            fwrite($fh, "type=cds\n");
            fwrite($fh, "nr.panel.genes=" . $nr_panel_genes . "\n");
            fwrite($fh, "nr.bed.genes=" . $bed_result['nr_cds_genes'] . "\n");
            fwrite($fh, "target.size=" . $bed_result['cds_size'] . "\n");
            fclose($fh);
            // write a details file with panel details.
            $fh = fopen("../BED_Files/$gpid.$summary.full.details", 'w');
            if (!$fh) {
                trigger_error("ERROR : Could not open full details File for writing: ../BED_Files/$gpid.$summary.full.details", E_USER_ERROR);
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: Failed to open full details file for writing");
            }
            fwrite($fh, "last.edit=" . $gp_details['LastEdit'] . "\n");
            fwrite($fh, "genome.build=" . $bed_result['build'] . "\n");
            fwrite($fh, "type=full\n");
            fwrite($fh, "nr.panel.genes=" . $nr_panel_genes . "\n");
            fwrite($fh, "nr.bed.genes=" . $bed_result['nr_full_genes'] . "\n");
            fwrite($fh, "target.size=" . $bed_result['full_size'] . "\n");
            fclose($fh);

            $result = array();
            // add values to output.
            $result['details'] = $gp_details;
            $result['cds']['path'] = "BED_Files/$gpid.$summary.cds.bed";
            $result['cds']['genome.build'] = $bed_result['build'];
            $result['cds']['report'] = $bed_result['cds_report'];
            $result['cds']['nr.genes'] = $bed_result['nr_cds_genes'];
            $result['cds']['target.size'] = $bed_result['cds_size'];
            $result['full']['path'] = "BED_Files/$gpid.$summary.full.bed";
            $result['full']['genome.build'] = $bed_result['build'];
            $result['full']['report'] = $bed_result['full_report'];
            $result['full']['nr.genes'] = $bed_result['nr_full_genes'];
            $result['full']['target.size'] = $bed_result['full_size'];

            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }

    /*****************************/
    /* Download A Panel BED FILE */
    /*****************************/
    protected function GetGenePanelBED($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Retrieve BED file for a specified Gene Panel",
            "method" => "GET",
            "arguments" => 'Gene_Panel_ID (numeric)',
            "parameters" => array(
                "summary" => "0/1 : summarize transcripts into a meta-transcript",
                'type' => 'cds/full : full includes UTR regions in the BED file (default:cds)',

            ),
            "return" => array(
                'summary' => '0/1 : are Transcripts summarized',
                'last.edit' => 'Date of last modification',
                'genome.build' => "Reference Version",
                'type' => "Full/CDS",
                'nr.panel.genes' => "Number of genes in the panel",
                "nr.bed.genes" => "Number of genes in the BED file",
                "target.size" => "Total size of regions in the BED file",
                "bed" => "The BED contents",
                "report" => "Details from BED file generations",
                "comment" => "(optional) Messages about default settings",
            )
        );
        if ($this->method == 'GET') {
            // valid ID?
            if (!isset($args[0]) || !is_numeric($args[0]) || (isset($this->verb) && $this->verb != '')) {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid GenePanel_ID provided.");
            }
            $gpid = $args[0];
            // access ?
            $access = $this->_runQuery("SELECT gp.id,gp.LastEdit FROM `GenePanels` gp JOIN `GenePanels_x_Users` gpu JOIN `Users` u ON gp.id = gpu.gpid AND gp.Owner = u.id WHERE (gp.Owner = " . $this->uid . " OR gpu.uid = " . $this->uid . " OR gp.Public = 1) AND gp.id = '" . $args[0] . "'", False);
            if (count($access) == 0) {
                return array("help" => $doc, "status" => "error", "msg" => "No access to this gene panel");
            }

            // output structure.
            $return = array();

            // by default, the transcripts are summarized.
            if (array_key_exists("summary", $this->request)) {
                if ($this->request['summary'] === "0" || strtolower($this->request['summary']) === 'false') {
                    $summary = 0;
                } elseif ($this->request['summary'] === "1" || strtolower($this->request['summary']) === 'true') {
                    $summary = 1;
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Value for 'summary' must be 0,1,true or false");
                }
            } else {
                $summary = 1;
            }
            $return['summary'] = $summary;

            // type
            if (!array_key_exists("type", $this->request) || (strtolower($this->request['type']) != 'cds' && strtolower($this->request['type']) != 'full')) {
                $return['comment'] = "invalid Bed type provided. Returning CDS-only bed. (valid options are 'cds' and 'full' (including UTR regions)";
                $this->request['type'] = 'cds';
            }
            if (!file_exists("../BED_Files/$gpid.$summary." . strtolower($this->request['type']) . ".bed")) {
                return array("help" => $doc, "status" => "error", "msg" => 'Bed file not found found (' . $gpid . "." . $summary . "." . strtolower($this->request['type']) . '.bed). Generate it first using the "MakeGenePanelBED" call, providing the genepanel ID ( ' . $args[0] . ' ) as argument');
            }
            $fh = fopen("../BED_Files/$gpid.$summary." . strtolower($this->request['type']) . ".details", 'r');
            while (($line = fgets($fh)) !== false) {
                $line = trim($line);
                $arr = explode("=", $line);
                $return[$arr[0]] = $arr[1];
            }
            fclose($fh);

            $return['bed'] = file_get_contents("../BED_Files/$gpid.$summary." . strtolower($this->request['type']) . ".bed");
            $return['report'] = file_get_contents("../BED_Files/$gpid.$summary." . strtolower($this->request['type']) . ".report");
            return array("help" => $doc, "status" => "ok", "results" => $return);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }
    /*************************************************************************/
    /* CREATE A BED FILE FROM LIST OF GENES, summarized over all transcripts */
    /*************************************************************************/
    protected function MakeGeneListBED($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Retrieve BED file for a list of specified genes",
            "method" => "GET",
            "arguments" => 'none',
            "parameters" => array(
                "genes" => "comma seperated list of gene symbols",
                "summary" => "0/1 : summarize transcripts into a meta-transcript",
                'type' => 'refgene/refseq : annotation source to use (default: refseq)',

            ),
            "return" => array(
                'comments' => "Comments on BED file generation",
                'genome.build' => "Used Reference Version",
                "type" => "RefGene / RefSeq : Gene Anntation release used",
                "summary" => "0/1 : Were transcripts summarized",
                'cds' => array(
                    'bed' => 'BED file contents',
                    'nr.genes' => 'Number of genes in the bed file',
                    'report' => 'comments during generation',
                    'target.size' => 'total region size over all entries'
                ),
                'full' =>  array(
                    'bed' => 'BED file contents',
                    'nr.genes' => 'Number of genes in the bed file',
                    'report' => 'comments during generation',
                    'target.size' => 'total region size over all entries'
                )
            )
        );
        if ($this->method == 'GET') {
            // output structure.
            $result = array();

            // genes
            if (!array_key_exists("genes", $this->request) || $this->request['genes'] == '') {
                return array("help" => $doc, "status" => "error", "msg" => "ERROR: No genes provided (argument 'genes' missing or empty)");
            }
            // by default, the transcripts are summarized.
            if (array_key_exists("summary", $this->request)) {
                if ($this->request['summary'] === "0" || strtolower($this->request['summary']) === 'false') {
                    $summary = 0;
                } elseif ($this->request['summary'] === "1" || strtolower($this->request['summary']) === 'true') {
                    $summary = 1;
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Value of 'summary' must be 0,1,true or false");
                }
            } else {
                $summary = 1;
            }
            $result["summary"] = $summary;
            // type : refgene/RefSeq
            if (array_key_exists("type", $this->request)) {
                $type = $this->request['type'];
            } else {
                $type = "refseq";
            }
            $result["type"] = $type;

            // read in gene list.
            $gp = array();
            $genes = explode(",", $this->request['genes']);
            foreach ($genes as $idx => $gene) {
                // replace C(.{1,2})ORF(\d+) by C(.{1,2})orf(\d+)
                if (preg_match('/^C[0-9XYMT]{1,2}O[Rr][Ff](\d+)$/', $gene)) {
                    $gene = preg_replace("/[O][Rr][Ff]/", "orf", $gene);
                }
                $gp[$gene] = $gene;
            }
            // replace by shared function with MakeGeneListBED
            $bed_result = $this->GetBED($gp, $summary, $type);
            if (array_key_exists('ERROR', $bed_result)) {
                return array("help" => $doc, "status" => "error", "msg" => $bed_result['ERROR']);
            }

            // add values to output.
            $result['comments'] = $bed_result['comments'];
            $result['genome.build'] = $bed_result['build'];
            $result['cds']['bed'] = $bed_result['cds_bed'];
            $result['cds']['report'] = $bed_result['cds_report'];
            $result['cds']['nr.genes'] = $bed_result['nr_cds_genes'];
            $result['cds']['target.size'] = $bed_result['cds_size'];
            $result['full']['bed'] = $bed_result['full_bed'];
            $result['full']['report'] = $bed_result['full_report'];
            $result['full']['nr.genes'] = $bed_result['nr_full_genes'];
            $result['full']['target.size'] = $bed_result['full_size'];

            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }


    /////////////////////////////////
    //                             //
    // SECTION 2 : MANIPULATE DATA //
    //                             //
    /////////////////////////////////


    /*******************************/
    /** LOAD new ANNOTATION SETS  **/
    /******************************/
    protected function LoadFilters($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Load a JSON dump of a saved filterset into the database",
            "method" => "POST",
            "arguments" => 'FILE(name => "json_file", contents => "dump from SavedFilters["results"])',
            "return" => "Array(fid => 'FilterID of newly created filter', 'WARNINGS' => 'info on unmatched criteria'"
        );
        if ($this->method == 'POST') {

            if ($_FILES['json_file']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['json_file']['tmp_name'])) {
                $json_file = $_FILES['json_file']['tmp_name'];
            }
            $json_string = file_get_contents($json_file);
            $json = json_decode($json_string);

            // get details to store:
            if (isset($json->FilterName)) {
                $name = $json->FilterName;
            } else {
                //trigger_error(var_dump($json, true));
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'FilterName'");
            }
            if (isset($json->Comments)) {
                $comments = $json->Comments;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'Comments'");
            }
            if (isset($json->Created)) {
                $created = $json->Created;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'Created'");
            }
            if (isset($json->FilterSettings)) {
                $settings = $json->FilterSettings;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'FilterSettings'");
            }
            if (isset($json->FilterTree)) {
                $tree = $json->FilterTree;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'FilterTree'");
            }
            $result = array();
            // code values in FilterSettings
            $translate = array(
                'Effect_On_Transcript|RefSeq_GeneLocation' => 'Variants_x_ANNOVAR_ncbigene_GeneLocation',
                'Effect_On_Transcript|RefSeq_VariantType' => 'Variants_x_ANNOVAR_ncbigene_VariantType',
                'Effect_On_Transcript|RefGene_GeneLocation' => 'Variants_x_ANNOVAR_refgene_GeneLocation',
                'Effect_On_Transcript|RefGene_VariantType' => 'Variants_x_ANNOVAR_refgene_VariantType',
                'Effect_On_Transcript|UCSC_GeneLocation' => 'Variants_x_ANNOVAR_knowngene_GeneLocation',
                'Effect_On_Transcript|UCSC_VariantType' => 'Variants_x_ANNOVAR_knowngene_VariantType',
                'Effect_On_Transcript|Ensembl_GeneLocation' => 'Variants_x_ANNOVAR_ensgene_GeneLocation',
                'Effect_On_Transcript|Ensembl_VariantType' => 'Variants_x_ANNOVAR_ensgene_VariantType',
                'Oncology_Specific|COSMIC_v70_Occurence_in_tissue' => 'Variants_x_ANNOVAR_cosmic70_tissue',
                'ClinVar_SNPs|Classification' => 'Variants_x_ClinVar_ncbigene_Class',
                'ClinVar_SNPs|Variant_Effect' => 'Variants_x_ClinVar_ncbigene_Effect',
                'ClinVar_SNPs|Match_Type' => 'Variants_x_ClinVar_ncbigene_MatchType',
                'snpEff|Effect' => 'Variants_x_snpEff_Effect',
                'snpEff|Effect_Impact' => 'Variants_x_snpEff_EffectImpact',
                'snpEff|Functional_Class' => 'Variants_x_snpEff_FunctionalClass',
                'snpEff|Gene_Coding' => 'Variants_x_snpEff_GeneCoding',
                'snpEff|Transcript_BioType' => 'Variants_x_snpEff_TranscriptBiotype'
            );
            $settings_array = array();
            foreach (explode('@@@', $settings) as $kv) {
                list($k, $v) = explode('|||', $kv);
                $settings_array[$k] = $v;
            }
            foreach ($settings_array as $k => $v) {
                if (substr($k, 0, 5) != 'param') {
                    continue;
                }
                $idx = substr($k, 5);
                $cat = "category$idx";
                // if custom annation field: translate here.
                if ($settings_array[$cat] == 'Custom_VCF_Fields') {
                    list($c_param, $c_type) = explode("___", $v);
                    // get the field id.
                    $c_param = $this->_SqlEscapeValues($c_param);
                    $c_type = $this->_SqlEscapeValues($c_type);
                    $r = $this->_runQuery("SELECT `aid` FROM `Custom_Annotations` WHERE `field_name` = '$c_param' AND `value_type` = '$c_type' LIMIT 1", False);
                    if (!empty($r)) {
                        $aid = $r['aid'];
                    } else {
                        $aid = $this->_insertQuery("INSERT INTO `Custom_Annotations` (`field_name`, `value_type`) VALUES ('$c_param','$c_type')");
                    }
                    $settings_array[$k] = $aid;
                    // get the coded arguments.
                    if ($c_type == 'list') {
                        $args = explode("__", $settings_array["argument$idx"]);
                        $encode_args = '';
                        foreach ($args as $i => $a) {
                            if ($a == '') {
                                continue;
                            }
                            $a = $this->_SqlEscapeValues($a);
                            $r = $this->_runQuery("SELECT `field_code` FROM `Custom_Annotations_x_Value_Codes` WHERE aid = '$aid' AND `field_value` = '$a' LIMIT 1", False);
                            if (!empty($r)) {
                                $encode_args .= $r['field_code'] . '__';
                            } else {
                                $aid = $this->_insertQuery("INSERT INTO `Custom_Annotations_x_Value_Codes` (aid,field_value) VALUES ('$aid','$a')");
                                $encode_args .= $aid . '__';
                            }
                        }
                        $settings_array["argument$idx"] = $encode_args;
                    }
                }
                // if value in translate => get the corresponding encoded id from DB.
                elseif (array_key_exists($settings_array[$cat] . '|' . $v, $translate) && $settings_array["argument$idx"] != '') {
                    // get values.
                    $settings_array["argument$idx"] = substr($settings_array["argument$idx"], 0, -2);
                    $args = explode("__", $settings_array["argument$idx"]);
                    $encode_args = "";
                    foreach ($args as $i => $a) {
                        //if ($a == '') {    // there is empty values. circumvented by the && statement to get here.
                        //	continue;
                        //}
                        $a = $this->_SqlEscapeValues($a);
                        $r = $this->_runQuery("SELECT id FROM `Value_Codes` WHERE Item_Value = '$a' AND Table_x_Column = '" . $translate[$settings_array[$cat] . '|' . $v] . "' LIMIT 1", False);
                        if (empty($r)) {
                            if (!isset($result['WARNINGS'])) {
                                $result['WARNINGS'] = array();
                            }
                            array_push($result['WARNINGS'], "Filter argument '$a' is not known in target database. Value was added, but no variants will match it.");
                            $code = $this->_insertQuery("INSERT INTO `Value_Codes` (Table_x_Column, Item_Value) VALUES ('" . $translate[$settings_array[$cat] . '|' . $v] . "','$a')");
                            $encode_args .= $code . "__";
                        } else {
                            $encode_args .= $r['id'] . '__';
                        }
                    }
                    $settings_array["argument$idx"] = $encode_args;
                }
            }
            // rebuild settings.
            $encode_settings = '';
            foreach ($settings_array as $key => $value) {
                $encode_settings .= $key . '|||' . $value . '@@@';
            }
            //$result['FilterSettings'] = substr($encode_settings,0,-3);
            // import
            $id = $this->_insertQuery("INSERT INTO `Users_x_FilterSettings` (uid, FilterName, FilterSettings, Comments,Created,FilterTree) VALUES (" . $this->uid . ",'" . $this->_SqlEscapeValues($name) . "','" . $this->_SqlEscapeValues($encode_settings) . "','" . $this->_SqlEscapeValues($comments) . "','" . $this->_SqlEscapeValues($created) . "','" . $this->_SqlEscapeValues($tree) . "')");
            $result['id'] = $id;
            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts POST requests");
        }
    }

    /*******************************/
    /** LOAD new ANNOTATION SETS  **/
    /******************************/
    protected function LoadAnnotations($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Load a JSON dump of a saved Annotation List into the database",
            "method" => "POST",
            "arguments" => 'FILE(name => "json_file", contents => "dump from SavedAnnotations["results"])',
            "return" => "Array(fid => 'FilterID of newly created filter')"
        );
        if ($this->method == 'POST') {

            if ($_FILES['json_file']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['json_file']['tmp_name'])) {
                $json_file = $_FILES['json_file']['tmp_name'];
            }
            $json_string = file_get_contents($json_file);
            $json = json_decode($json_string, true);
            // get details to store:
            if (isset($json->AnnotationName)) {
                $name = $json->AnnotationName;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'AnnotationName'");
            }
            if (isset($json->Comments)) {
                $comments = $json->Comments;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'Comments'");
            }
            if (isset($json->Created)) {
                $created = $json->Created;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'Created'");
            }
            if (isset($json->AnnotationSettings)) {
                $settings = $json->AnnotationSettings;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid JSON provided. Missing key: 'AnnotationSettings'");
            }
            // go over annoations settings to correctly handle custom vcf fields.
            $annos = explode("@@@", $settings);
            foreach ($annos as $i => $kv) {
                list($k, $v) = explode(";", $kv);
                if ($k == 'Custom_VCF_Fields') {
                    list($field_name, $field_type) = explode("___", $v);
                    $field_name = $this->_SqlEscapeValues($field_name);
                    $field_type = $this->_SqlEscapeValues($field_type);
                    $r = $this->_runQuery("SELECT aid FROM `Custom_Annotations` WHERE `field_name` = '$field_name' AND `value_type` = '$field_type' LIMIT 1", False);
                    if (!empty($r)) {
                        $annos[$i] = $k . ';' . $r['aid'];
                    } else {
                        $aid = $this->_insertQuery("INSERT INTO `Custom_Annotations` (`field_name`, `value_type`) VALUES ('$field_name','$field_type')");
                        $annos[$i] = $k . ';' . $aid;
                    }
                }
            }
            $settings = implode("@@@", $annos);
            // store to database.
            $id = $this->_insertQuery("INSERT INTO `Users_x_Annotations` (uid, AnnotationName, AnnotationSettings, Comments,Created) VALUES (" . $this->uid . ",'" . $this->_SqlEscapeValues($name) . "','" . $this->_SqlEscapeValues($settings) . "','" . $this->_SqlEscapeValues($comments) . "','" . $this->_SqlEscapeValues($created) . "')");
            $result = array('id' => $id);
            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts POST requests");
        }
    }

    /***********************/
    /** Create a project **/
    /**********************/
    protected function CreateProject($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Create a new project",
            "method" => "GET",
            "arguments" => 'a project name',
            "return" => array(
                "project_id" => "number",

            )
        );
        if ($this->method == 'GET') {
            $result = array();
            $pname = '';
            if ($this->verb != '') {
                $pname = $this->_SqlEscapeValues($this->verb);
            } elseif (count($args) > 0 && $args[0] != '') {
                $pname = $this->_SqlEscapeValues($args[0]);
            }
            if ($pname != '') {
                // create project, summaryStatus defaults to zero.
                $pid = $this->_insertQuery("INSERT INTO `Projects` (`Name`,`collection`,`userID`,`pipeline`) VALUES ('$pname','none','" . $this->uid . "','0')");
                if ($pid < 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "Could not create project");
                }

                if (!$this->_doQuery("INSERT INTO `Projects_x_Users` (`pid`,`uid`,`editvariant`,`editclinic`,`editsample`) VALUES ('$pid','" . $this->uid . "','1','1','1')")) {
                    return array("help" => $doc, "status" => "error", "msg" => "Could not grant permissions to new project");
                }
                // clear the user
                $r = $this->_clearSummaryStatus('u', $this->uid);
                $result['project_id'] = $pid;
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "No project name provided");
            }
            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }

    /**********************************/
    /** MOVE SAMPLE TO OTHER PROJECT **/
    /**********************************/
    protected function MoveSample($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Move sample between projects",
            "method" => "GET",
            "arguments" => 'sample ID',
            'params' =>  '{"f" : source project_id, "t" : target project id}',
            "return" => "none"


        );
        if ($this->method == 'GET') {
            if ((!isset($args[0]) || !is_numeric($args[0])) || (isset($this->verb) && $this->verb != '')) {
                return array("help" => $doc, "status" => "error", "msg" => "Numeric sample-ID expected");
            } else {
                $sid = $args[0];
                // access ?
                $rows = $this->_runQuery("SELECT s.id FROM `Samples` s JOIN `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON s.id = ps.sid AND ps.pid = pu.pid WHERE pu.uid = '" . $this->uid . "' AND s.id = '" . $sid . "' AND pu.editsample = 1", False);
                if (!$rows) {
                    return array("help" => $doc, "status" => "error", "msg" => "Sample not found, or no access to this sample");
                }

                $from = $this->request['f'];
                if (!is_numeric($from)) {
                    return array("help" => $doc, "status" => "error", "msg" => "invalid source project provided.");
                }
                $to = $this->request['t'];
                if (!is_numeric($to)) {
                    return array("help" => $doc, "status" => "error", "msg" => "invalid target project provided.");
                }
                // access ?
                $rows = $this->_runQuery("SELECT pid FROM `Projects_x_Users` WHERE uid = '" . $this->uid . "' AND editsample = 1 AND pid = '$to'", False);
                if (!$rows) {
                    return array("help" => $doc, "status" => "error", "msg" => "Insufficient permissions on target project");
                }
                // move.
                $query = $this->_doQuery("UPDATE `Projects_x_Samples` SET pid = '$to' WHERE sid = '$sid' AND pid = '$from'");

                if (!$query) {
                    return array("help" => $doc, "status" => "error", "msg" => "Unable to move sample: " . $this->_GetSqlError());
                } else {
                    $this->_clearMemcache('Projects_x_Samples');
                    // queue projects for summarazition (and users if needed)
                    $result = $this->_clearSummaryStatus('p', "$from,$to");
                    return array("help" => $doc, "status" => "ok", "results" => "Sample $sid was moved from project $from to $to");
                    //return array('result' => "ok");
                }
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }
    /***********************/
    /** SET SAMPLE DETAILS */
    /***********************/
    protected function SetSampleDetails($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Set a variety of sample meta-data",
            "method" => "GET",
            "arguments" => 'numeric sampleID',
            'params' =>  '"set" : Item to set. Values ["gender","parent","sibling", "name", "affected", "control" , "hpo"]',
            'param_allowed_values' => array(
                "gender" => array("man", "male", "female", "vrouw"),
                "parent" => "numeric_sample_id",
                "sibling" => "numeric_sample_id",
                "name" => "string",
                "affected" => array("true", "yes", 1, "false", "no", 0),
                "control" => array("true", "yes", 1, "false", "no", 0),
                "hpo" => "comma-separated list of HP:0000111 (HP: then seven digits)"
            ),
            "return" => "none"
        );
        if ($this->method == 'GET') {
            if ((!isset($args[0]) || !is_numeric($args[0])) || (isset($this->verb) && $this->verb != '')) {
                return array("help" => $doc, "status" => "error", "msg" => "This function requires a numeric sample id as main argument.");
            }
            $sid = $args[0];
            // access ?
            $row = $this->_runQuery("SELECT ps.sid, ps.pid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid WHERE pu.uid = '" . $this->uid . "' AND ps.sid = '" . $sid . "' AND pu.editsample = 1", False);
            if (!$row) {
                return array("help" => $doc, "status" => "error", "msg" => "Sample not found, or no access to this sample");
            }
            $pid = $row['pid'];
            // get action
            if (!isset($this->request['set']) || $this->request['set'] == '') {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid action provided. provide item to change as 'set=[gender,parent,sibling,name,affected,control,hpo]'");
            }
            $set = strtolower($this->request['set']);
            $value = $this->request['value'];
            // set gender
            if ($set == 'gender') {
                $g = strtolower(substr($value, 0, 1));
                $val = 'Undef';
                if ($g == 'm') {
                    $val = 'Male';
                } elseif ($g == 'v' || $g == 'f') {
                    $val = 'Female';
                } elseif ($g == 'u') {
                    $val = 'Undef';
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Invalid gender provided ($value). Accepted values are 'Male' and 'Female' and 'Undef'");
                }
                $result = $this->_doQuery("UPDATE `Samples` SET gender = '" . $val . "' WHERE id = '$sid'");
                if ($result) {
                    $this->_clearMemcache('Samples');
                    // queue project for summarazition (and users if needed)
                    $r_s = $this->_clearSummaryStatus('s', "$sid");
                    $r_p = $this->_clearSummaryStatus('p', "$pid");
                    if ($r_s['result'] == 'ok' && $r_p['result'] == 'ok') {
                        return array("help" => $doc, "status" => "ok", "results" => "Gender of sample $sid was changed to $val");
                    }
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Failed to update gender. please report: " . $this->_GetSqlError());
                }
            }
            // set parent.
            elseif ($set == 'parent') {
                // valid numeric
                if (!is_numeric($value)) {
                    return array("help" => $doc, "status" => "error", "msg" => "This function requires a numeric sample id as value.");
                }
                // different from sample
                if ($value == $sid) {
                    return array("help" => $doc, "status" => "error", "msg" => "Sample and Parent can not be the same");
                }
                // max 2 parents.
                $nr_p = $this->_runQuery("SELECT sid1 FROM `Samples_x_Samples` WHERE `sid2` = '$sid' AND `Relation` = '2' AND `sid1` != '$value'");
                if (count($nr_p) >= 2) {
                    return array("help" => $doc, "status" => "error", "msg" => "A sample can have max two parents");
                }
                // access to parental sample to edit ?
                $a_r = $this->_runQuery("SELECT ps.sid FROM `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON ps.pid = pu.pid WHERE ps.sid = '$value' AND pu.uid = '" . $this->uid . "' AND pu.editsample = 1", False);
                if (count($a_r) == 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "Insufficient access rights on parental sample");
                }
                // update
                $result = $this->_doQuery("REPLACE INTO `Samples_x_Samples` (`sid1`,`sid2`,Relation) VALUES ('$value','$sid','2')");
                if ($result) {
                    $this->_clearMemcache('Samples_x_Samples');
                    return array("help" => $doc, "status" => "ok", "results" => "Parent of $sid set to $value");
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Failed to add parent relation. please report: " . $this->_GetSqlError());
                }
            }
            // set sibling.
            elseif ($set == 'sibling') {
                if (!is_numeric($value)) {
                    return array("help" => $doc, "status" => "error", "msg" => "This function requires a numeric sample id as value.");
                }
                // different from sample
                if ($value == $sid) {
                    return array("help" => $doc, "status" => "error", "msg" => "Sample and Sibling can not be the same");
                }
                // access to sibling sample to edit ?
                $a_r = $this->_runQuery("SELECT ps.sid FROM `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON ps.pid = pu.pid WHERE ps.sid = '$value' AND pu.uid = '" . $this->uid . "' AND pu.editsample = 1", False);
                if (count($a_r) == 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "Insufficient access rights on sibling sample");
                }
                // update
                $result = $this->_doQuery("REPLACE INTO `Samples_x_Samples` (`sid1`,`sid2`,Relation) VALUES ('$value','$sid','3'),('$sid','$value','3')");
                if ($result) {
                    $this->_clearMemcache('Samples_x_Samples');
                    return array("help" => $doc, "status" => "ok", "results" => "");
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Failed to add sibling relation. please report: " . $this->_GetSqlError());
                }
            }

            // sample name
            elseif ($set == 'name') {
                $result = $this->_doQuery("UPDATE `Samples` SET Name = '" . $this->_SqlEscapeValues($value) . "' WHERE id = '$sid'");
                if ($result) {
                    $this->_clearMemcache('Samples');
                    return array("help" => $doc, "status" => "ok", "results" => "Updated name of sample '$sid' to '$value'");
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Failed to update name. please report: " . $this->_GetSqlError());
                }
            }

            // affected
            elseif ($set == 'affected') {
                if ($value === "true" || $value === "yes") {
                    $value = "1";
                } elseif ($value === "false" || $value === "no") {
                    $value = "0";
                }

                if ($value !== "0" && $value !== "1") {
                    return array("help" => $doc, "status" => "error", "msg" => "Value must be 0,1,true or false");
                }

                $result = $this->_doQuery("UPDATE `Samples` SET affected = '$value' WHERE id = '$sid'");
                if ($result) {
                    $this->_clearMemcache('Samples');
                    // queue project for summarazition (and users if needed)
                    $r_s = $this->_clearSummaryStatus('s', "$sid");
                    $r_p = $this->_clearSummaryStatus('p', "$pid");
                    if ($r_s['result'] == 'ok' && $r_p['result'] == 'ok') {
                        return array("help" => $doc, "status" => "ok", "results" => "Updated affected status of $sid to $value");
                    } else {
                        return array("help" => $doc, "status" => "error", "msg" => "Failed to clear memcache : $result");
                    }
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Failed to update affected status. please report: " . $this->_GetSqlError());
                }
            }

            // control
            elseif ($set == 'control') {
                if ($value === "true" || $value === "yes") {
                    $value = "1";
                } elseif ($value === "false" || $value === "no") {
                    $value = "0";
                }

                if ($value !== "0" && $value !== "1") {
                    return array("help" => $doc, "status" => "error", "msg" => "Value must be 0,1,true or false");
                }

                $result = $this->_doQuery("UPDATE `Samples` SET IsControl = '$value' WHERE id = '$sid'");
                if ($result) {
                    $this->_clearMemcache('Samples');
                    // queue project for summarazition (and users if needed)
                    $r_s = $this->_clearSummaryStatus('s', "$sid");
                    $r_p = $this->_clearSummaryStatus('p', "$pid");
                    if ($r_s['result'] == 'ok' && $r_p['result'] == 'ok') {
                        return array("help" => $doc, "status" => "ok", "results" => "Updated control status of $sid to $value");
                    } else {
                        return array("help" => $doc, "status" => "error", "msg" => "Failed to reset summary status: $result");
                    }
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Failed to update control status. please report: " . $this->_GetSqlError());
                }
            } elseif ($set == 'hpo') {
                if ($value == '') {
                    return array("help" => $doc, "status" => "error", "msg" => "No HPO ids provided. Should be comma separated list");
                }
                $values = explode(",", $value);
                $found = array();
                $not_found = array();
                $provided = array();
                foreach ($values as $hpo_id) {
                    // assume format HP:0000111 (HP: then seven digits)
                    // so strip front part.
                    $db_id = preg_replace("/^HP:[0]+/i", "", $hpo_id);
                    array_push($provided, $db_id);
                }
                $db_ids = "'" . implode("','", $provided) . "'";
                $rows = $this->_runQuery("SELECT id, Name, Definition FROM `HPO_Terms` WHERE id IN ($db_ids)");
                foreach ($rows as $row) {
                    // add to return array
                    $found[$row['id']] = $row;
                    // add to database
                    $sth = $this->_doQuery("REPLACE INTO `Samples_x_HPO_Terms` (sid, tid) VALUES ('$sid','" . $row['id'] . "')");
                    if (!$sth) {
                        return array("help" => $doc, "status" => "error", "msg" => "Failed to update HPO term " . $row['id'] . " for $sid. please report: " . $this->_GetSqlError());
                    }
                }
                // scan again for 'not found'
                foreach ($values as $hpo_id) {
                    // assume format HP:0000111 (HP: then seven digits)
                    // so strip front part.
                    $db_id = preg_replace("/^HP:[0]+/i", "", $hpo_id);
                    if (!array_key_exists($db_id, $found)) {
                        array_push($not_found, $hpo_id);
                    }
                }
                return array("help" => $doc, "status" => "ok", "results" => array("FOUND" => $found, "NOT_FOUND" => $not_found));
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid action provided. provide item to change as 'set=[gender,parent,sibling,name,affected,control,hpo]'");
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }

    /*********************/
    /** GENERATE REPORT **/
    /*********************/
    protected function SubmitReport($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Submit a Report generation for given sample and report ID",
            "method" => "GET",
            "arguments" => 'Numeric Sample ID',
            "parameters" => array(
                "rid" => "Numeric Report ID (Mandatory, see GetReportID)",
                "save" => "String. Optional: If provided, also store sections as online saved-results using given name",
                "comment" => "String. Optional: comment for saved results (see: save)",
                "addfiltername" => "0/1. Optional: append filters used in sections to the name of the saved results (see: save)"
            ),
            "return" => array(
                'query_key' => 'assoc_array(sid => query_key)',
                'qids' => "comma-separated list of all query_keys",
                'queue_position' => 'position(s) in query queue',
                'nr_submitted_reports' => 'amount of submitted requests, default to one in API usage'
            )

        );
        if ($this->method == 'GET') {
            // sample
            if (!isset($args[0]) || !is_numeric($args[0])) {
                return array("help" => $doc, "status" => "error", "msg" => "SubmitReport requires a numeric Sample_ID.");
            }
            $sid = $args[0];

            // access 
            $access = $this->_runQuery("SELECT pu.editsample FROM `Projects_x_Users` pu JOIN `Projects_x_Samples` ps ON ps.pid = pu.pid WHERE ps.sid = '$sid' AND pu.uid = '" . $this->uid . "'", False);

            if (count($access) == 0) {
                return array("help" => $doc, "status" => "error", "msg" => "Insufficient access rights on sample");
            }
            // report
            if (!isset($this->request['rid']) || !is_numeric($this->request['rid'])) {
                return array("help" => $doc, "status" => "error", "msg" => "SubmitReport requires a numeric report id as value for 'rid'.");
            }
            $rid = $this->request['rid'];

            $post_string = "rid=$rid&sids=$sid&uid=" . $this->uid;
            // submit using CURL.
            $url = "https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/cgi-bin/Report_Wrapper.py";

            if (array_key_exists('save', $this->request)) {
                // check extra access:
                if ($access['editsample'] == 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "Insufficient access rights to save results");
                }
                // save filters and annotations
                $name = $this->request['save'];
                if ($name == '') {
                    $name = "API.submitreport." . date('Y-m-d_H:i');
                }
                $post_string .= "&save=" . urlencode($name);
                if (array_key_exists('comment', $this->request) && $this->request['comment'] != '') {
                    $post_string .= "&comment=" . urlencode($this->request['comment']);
                }
                if (array_key_exists('addfiltername', $this->request) && $this->request['addfiltername'] != '') {
                    $post_string .= "&addfiltername=" . urlencode($this->request['addfiltername']);
                }
            }
            $result = $this->_InternalCall($url, $post_string);
            // $curl_connection = curl_init($url);
            // curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 300);
            // curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
            // $result = curl_exec($curl_connection);
            // //close the connection
            // curl_close($curl_connection);
            // // trigger_error($result);
            // $result = json_decode($result);
            // error ? 
            if (isset($result->ERROR)) {
                return array("help" => $doc, "status" => "error", "msg" => $result->ERROR);
            } else {
                return array("help" => $doc, "status" => "ok", 'results' => $result);
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }






    ///////////////////////////////////////////////
    //                                           //
    // SECTION 3 :: RUNNING QUERIES & REPORTS    //
    //                                           //
    ///////////////////////////////////////////////
    protected function GetReport($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Download Report PDF file",
            "method" => "GET",
            "arguments" => 'Numeric query ID (see SubmitReport)',

            "return" => array(
                'on_success' => 'PDF_file',
                'on_failure' => "error responses in the header"
            )

        );
        // bypass logic : if $this->GetDocs == 1 : return docs.
        if ($this->GetDocs == 1) {
            return (array('help' => $doc));
        }
        // returns pdf on success, error responses in headers otherwise.
        if (
            $this->method != 'GET' ||
            (!(isset($args[0]) && is_numeric($args[0])) &&
                !(isset($this->request['qid']) && is_numeric($this->request['qid'])))
        ) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }

        if (isset($args[0]) && is_numeric($args[0])) {
            $qid = $args[0];
        }
        if (isset($this->request['qid']) && is_numeric($this->request['qid'])) {
            $qid = $this->request['qid'];
        }

        // check if it matches the apiKey
        if (file_exists("query_results/$qid/uid")) {
            $query_uid = rtrim(file_get_contents("query_results/$qid/uid"));
            $api_result = $this->_runQuery("SELECT apiKey FROM `Users` WHERE id = '" . $query_uid . "'", False);
            $apikey = md5($api_result['apiKey']);
        } elseif (file_exists("query_results/$qid/apiKey")) {
            $apikey = rtrim(file_get_contents("query_results/$qid/apiKey"));
        } else {
            header("HTTP/1.0 403 Forbidden");
            exit;
        }

        if ($apikey != md5($this->request['apiKey'])) {
            header("HTTP/1.0 403 Access denied");
            exit;
        }

        $report_path = "query_results/$qid/Report.pdf";

        if (!file_exists($report_path)) {
            header("HTTP/1.0 404 File not found");
            exit;
        }
        // get sample name.
        $fh = fopen("query_results/$qid/form", 'r');
        $form = array();
        while (($line = fgets($fh)) !== false) {
            $line = trim($line);
            $c = explode("=", $line);
            $form[$c[0]] = $c[1];
        }
        fclose($fh);
        if (!array_key_exists('sid', $form)) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }
        $sid = $form['sid'];
        $row = $this->_runQuery("SELECT Name FROM `Samples` WHERE id = '$sid'", False);
        if (count($row) == 0) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        }
        $sname = $row['Name'];
        header('Content-Disposition: attachment; filename="' . $sname . '.pdf"');
        header("Content-Type: application/pdf");
        $file_size  = filesize($report_path);
        header("Content-Length: $file_size");
        readfile($report_path);
        exit;
    }

    /****************/
    /** Submit QUERY  **/
    /****************/
    protected function SubmitQuery($args)
    {
        $doc = array(
            "goal" => "Submit a query for sample/region/project and get results in json",
            "method" => "GET",
            "arguments" => "Query Type : string : [sample, region, project]",
            "examples" => array(
                "region" => $this->path . "SubmitQuery/region?r=ADNP&fid=5",
                "sample" => $this->path . "SubmitQuery/sample/?s=1248&fid=7&aid=1,27",
                "project" => $this->path . "SubmitQuery/project/?p=254&aid=27"
            ),
            "parameters" => array(
                "fid" => "Numeric Filter_ID (optional)",
                "aid" => "Numeric Annotations_ID (comma-separated, optional)",
                "slice_start" => "Paginate results, return 100 variants, starting at given idx. (optional)",
                "save" => "Store Results as static set using value of 'save' as name (optional, overrides slicing)",
                "comment" => "If saving results, add comment to saved set (optional)",
                "cbs" => "if saving results, add checkbox lists (by ID) for reviewing to the HTML page (optional)",
                "format" => "value: vcf => return VCF format instead of JSON. (optional, defaults to json)"
            ),
            "return" => array(
                'query_key' => 'numeric query_key',
                'hpc_job_id' => "Job ID of query on HPC (if HPC is enabled)",
                'queue_position' => 'position in query queue'
            )
        );
        $types = array('sample' => 's', 'region' => 'r', 'project' => 'p');
        $verb = strtolower($this->verb);
        if ($this->method == 'GET') {
            // sid: sample.
            $pars = array(); // for post call

            $pars['apiKey'] = $this->request['apiKey'];
            if (array_key_exists("local_call", $this->request)) {
                $pars['local_call'] = 1;
            }
            // sample query
            if ($types[$verb] == 's') {
                if (isset($args[0])) {
                    if (is_numeric($args[0])) {
                        $pars['sid'] = $args[0];
                        $pars['region'] = 'false';
                        $pars['project'] = 'false';
                    } else {
                        return array("help" => $doc, "status" => "error", "msg" => "Sample Query requires a numeric Sample_ID as value of parameter 's'");
                    }
                } elseif (!array_key_exists($types[$verb], $this->request) || !is_numeric($this->request[$types[$verb]])) {
                    return array("help" => $doc, "status" => "error", "msg" => "Sample Query requires a numeric Sample_ID as value of parameter 's'");
                } else {
                    $pars['sid'] = $this->request[$types[$verb]];
                    $pars['region'] = 'false';
                    $pars['project'] = 'false';
                }
                // access ?
                $access = $this->_runQuery("SELECT ps.sid,pu.editsample FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid WHERE pu.uid = '" . $this->uid . "' AND ps.sid = '" . $pars['sid'] . "'", False);
                if (count($access) == 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "No access to requested sample");
                }
                // saving requires editsample rights
                if (array_key_exists('save', $this->request) && $access['editsample'] == 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "Insufficient access to save results for requested sample");
                }
            }
            // region query
            elseif ($types[$verb] == 'r') {
                if (isset($args[0])) {
                    $pars['sid'] = '';
                    $pars['region'] = $args[0];
                    $pars['project'] = 'false';
                } elseif (array_key_exists($types[$verb], $this->request)) {
                    $pars['sid'] = '';
                    $pars['region'] = $this->request[$types[$verb]];
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Region Query requires parameter 'r'");
                }
            }
            // project query
            elseif ($types[$verb] == 'p') {
                if (isset($args[0])) {
                    if (is_numeric($args[0])) {
                        $pars['sid'] = '';
                        $pars['region'] = 'false';
                        $pars['project'] = $args[0];
                    } else {
                        return array("help" => $doc, "status" => "error", "msg" => "Project Query requires a numeric Project>_ID as value of parameter 'p'");
                    }
                } elseif (array_key_exists($types[$verb], $this->request)) {
                    if (is_numeric($this->request[$types[$verb]])) {
                        $pars['sid'] = '';
                        $pars['region'] = 'false';
                        $pars['project'] = $this->request[$types[$verb]];
                    } else {
                        return array("help" => $doc, "status" => "error", "msg" => "Project Query requires a numeric Project>_ID as value of parameter 'p'");
                    }
                } else {
                    return array("help" => $doc, "status" => "error", "msg" => "Project Query requires a numeric Project>_ID as value of parameter 'p'");
                }
                //access ?
                $access = $this->_runQuery("SELECT pu.pid FROM `Projects_x_Users` pu WHERE pu.uid = '" . $this->uid . "' AND pu.pid = '" . $pars['project'] . "'", False);
                if (count($access) == 0) {
                    return array("help" => $doc, "status" => "error", "msg" => "No access to requested project");
                }
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Query Type not supported. Options are 'sample', 'project' and 'region'");
            }
            // variables needed when saving
            $set_filters = '';
            $set_filtertree = '';
            $set_annotations = '';

            // filter ID
            if (array_key_exists('fid', $this->request) && $this->request['fid'] != '') {
                // check permission.
                if (!array_key_exists("local_call", $this->request)) {
                    $q1 = $this->_runQuery("SELECT fid FROM `Users_x_FilterSettings` WHERE fid = '" . $this->request['fid'] . "' AND uid = '" . $this->uid . "'", False);
                    $q2 = $this->_runQuery("SELECT fid FROM `Users_x_Shared_Filters` WHERE fid = '" . $this->request['fid'] . "' AND uid = '" . $this->uid . "'", False);
                    if (count($q1) == 0 && count($q2) == 0) {
                        return array("help" => $doc, "status" => "error", "msg" => "No access to requested filter");
                    }
                }
                // get filter
                $filter_data = $this->SavedFiltersEncoded(array($this->request['fid']));
                if ($filter_data['status'] == 'error') {
                    trigger_error("Failed to extract encoded filter settings for " . $this->request['fid'] . " : " . $filter_data['msg'], E_USER_ERROR);
                    return array("help" => $doc, "status" => "error", "msg" => "Internal error, please report: " . $filter_data['msg']);
                }
                $row = $filter_data['results'];
                $set_filters = $row['FilterSettings'];
                $set_filtertree = $row['FilterTree'];  // fully escaped.
                foreach ($row as $key => $value) {
                    // parse filter rules to post format.
                    if ($key == 'FilterSettings') {
                        $p = explode("@@@", $value);
                        $nrR = 0;
                        $do_names = array();
                        foreach ($p as $k => $v) {
                            $q = explode('|||', $v);

                            // track category numbers
                            if (substr($q[0], 0, 3) == 'cat') {
                                $idx = substr($q[0], 8);
                                if ($idx > $nrR) {
                                    $nrR = $idx;
                                }
                                // dynamic fiter options. 
                            }
                            $pars[$q[0]] = $q[1];
                        }
                        // dynamic filters ? rephrase params // obsolete code
                        // foreach ($do_names as $dkey => $do_name) {
                        //     // do11_1_name = varName
                        //     // do11_1_value = varValue
                        //     // first, extract the value
                        //     $do_value = $pars[str_replace("_name", "_value", $do_name)];
                        //     // second, extract the actual name 
                        //     $do_name_value = $pars[$do_name];
                        //     // create do11_{varName} = {varValue}
                        //     $do_key = preg_replace('/(do\d+)_.*/', '${1}' . '_' . $do_name_value, $do_name);
                        //     //$pars[$do_key] = $do_value;
                        //     // then unset the original variables. 
                        //     //unset($pars[str_replace("_name", "_value", $do_name)], $pars[$do_name]);
                        // }

                        $pars['listed'] = $nrR;
                    }
                    // place logic-tree in post array ; stripping some quotes & escapes.
                    elseif ($key == 'FilterTree') {
                        $value = str_replace("\\", "", $value);
                        $value = str_replace('"{', '{', $value);
                        $value = str_replace('}"', '}', $value);

                        $pars['logic'] = $value;
                    }
                }
            } else {
                $pars['logic'] = '';
                $pars['listed'] = 0;
            }
            // annotations
            if (array_key_exists('aid', $this->request)) {
                $anno = $this->request['aid'];
                // 1. array (specified multiple times)
                $set_annos = array();
                $idx = 0;
                // array if provided multiple times in URL, else: comma-sep list.
                if (!is_array($anno)) {
                    $anno = explode(",", $anno);
                }

                foreach ($anno as $k => $anno_id) {
                    if ($anno_id == '') {
                        continue;
                    }
                    // check permissions. skip if not allowed.
                    if (!array_key_exists("local_call", $this->request)) {
                        $q1 = $this->_runQuery("SELECT aid FROM `Users_x_Annotations` WHERE aid = '$anno_id' AND uid = '" . $this->uid . "'", False);
                        $q2 = $this->_runQuery("SELECT aid FROM `Users_x_Shared_Annotations` WHERE aid = '$anno_id' AND uid = '" . $this->uid . "'", False);
                        if (count($q1) == 0 && count($q2) == 0) {
                            continue;
                        }
                    }
                    //$aa =      $this->SavedAnnotations(array($anno_id));
                    $annotation_set = $this->SavedAnnotations(array($anno_id));
                    if ($annotation_set['status'] == 'error') {
                        trigger_error("Failed to extract annotation settings for " . $this->request['aid'] . " : " . $annotation_set['msg'], E_USER_ERROR);
                        return array("help" => $doc, "status" => "error", "msg" => "Internal error, please report: " . $annotation_set['msg']);
                    }
                    $details = $annotation_set['results'];
                    $set_annotations .= $details['AnnotationSettings'] . '@@@';
                    $details = explode("@@@", $details['AnnotationSettings']);
                    foreach ($details as $kk => $value) {
                        $idx++;
                        $pars["anno$idx"] = $value;
                    }
                }
                $pars['nranno'] = $idx;
            } else {
                $pars['nranno'] = 0;
            }
            if ($set_annotations != '') {
                $set_annotations = rtrim($set_annotations, '@@@');
            }

            // slice start set?
            if (array_key_exists("slice_start", $this->request)) {
                $pars['slicestart'] = $this->request['slice_start'];
            }
            // else add all variants.
            else {
                $pars['slicestart'] = 'all';
            }
            // saving overwrites slice
            if (array_key_exists('save', $this->request)) {
                $name = $this->_SqlEscapeValues($this->request['save']);
                // get name
                if ($name == '') {
                    $name = "Result_Set." . date("Y-m-d");
                }
                // get comments
                $comments = 'Saved by API';
                if (array_key_exists('comments', $this->request)) {
                    $comments = $this->_SqlEscapeValues($this->request['comments']);
                }
                // get checkboxes
                $chbxs = '';
                if (array_key_exists('cbs', $this->request)) {
                    $chbxs = $this->request['cbs'];
                }
                // create set.
                $sid = $pars['sid'];
                $uid = $this->uid;
                $set_filtertree = $this->_SqlEscapeValues($set_filtertree);
                $set_id = $this->_insertQuery("INSERT INTO `Samples_x_Saved_Results` (sid,uid,`set_name`,`set_comments`,`set_filters`,`set_filtertree`,`set_annotations`,`set_vids`,`cbs`) VALUES ('$sid','$uid','$name','$comments','$set_filters','$set_filtertree','$set_annotations','Queued','$chbxs')");
                $slicestart = 'save|' . $set_id;
                $pars['slicestart'] = $slicestart;
            } else {
                $pars['fmt'] = 'json';
            }
            // return VCF format?
            if (array_key_exists("format", $this->request) && strtolower($this->request['format']) == 'vcf') {
                $pars['vcf'] = 1;
            }

            // add the build
            $row = $this->_runQuery("SELECT name FROM `NGS-Variants-Admin`.`CurrentBuild` WHERE 1", False);
            $hg = substr($row['name'], 1);
            $pars['build'] = $hg;
            // parse to correct format.
            $post_items = array();
            foreach ($pars as $key => $value) {
                $post_items[] = $key . '=' . urlencode($value);
            }
            $post_string = implode('&', $post_items);
            // submit using curl functionality.
            $url = "https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/cgi-bin/API_wrapper.py";
            $json = $this->_InternalCall($url, $post_string);
            // $curl_connection = curl_init($url);
            // curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 300);
            // curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
            // //curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
            // //curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
            // curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
            // $result = curl_exec($curl_connection);
            // #print_r(curl_getinfo($curl_connection));
            // #echo curl_errno($curl_connection) . '-' . curl_error($curl_connection);
            // //close the connection
            // curl_close($curl_connection);
            //$json = json_decode($result);
            // error ? 
            //if (!$result) {
            //    return array("help" => $doc, "status" => "error", "msg" => "No response recieved from CGI: ".$result->ERROR);
            //} else
            if (isset($json->ERROR)) {
                return array("help" => $doc, "status" => "error", "msg" => $json->ERROR);
            } else {
                return array("help" => $doc, "status" => "ok", 'results' => $json);
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }


    /************************/
    /** GET QUERY  RESULTS **/
    /************************/

    protected function GetQueryResults($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Get Results of SubmitQuery, by query_key",
            "method" => "GET",
            "arguments" => 'numeric query_key',
            "return" => array(
                "if_not_finished" => "error",
                "if_finished_and_call_to_save" => "Array(html content of saved pages)",
                "if_finished_and_json_call" => "Json structure",
            )
        );
        if ($this->method == 'GET') {
            if (!isset($args[0]) || !is_numeric($args[0])) {
                return array("help" => $doc, "status" => "error", "msg" => "Invalid or no Query_ID provided.");
            }
            $qid = $args[0];
            $result = array();
            // query_id is valid?
            if (!file_exists("query_results/$qid/status")) {
                return array("help" => $doc, "status" => "error", "msg" => "No results found for provided key. Results might be expired");
            }
            // check if it matches the apiKey
            $apikey = rtrim(file_get_contents("query_results/$qid/apiKey"));
            if ($apikey != md5($this->request['apiKey'])) {
                return array("help" => $doc, "status" => "error", "msg" => "Access Denied. Provided apiKey does not match query results: $apikey vs " . md5($this->request['apiKey']));
            }
            // check if finished.
            $status = rtrim(file_get_contents("query_results/$qid/status"));
            if ($status != 'finished') {
                return array("help" => $doc, "status" => "error", "msg" => "Query is not marked as finished (status = $status). Use the GetStatus/Query/<query_id> syntax to get more details on the query status.");
            }
            // finished. get results.
            // check for a 'save' call.
            if (!file_exists("query_results/$qid/form")) {
                return array("help" => $doc, "status" => "error", "msg" => "No form values found for provided key. Please report");
            }
            $fh = fopen("query_results/$qid/form", 'r');
            $set_id = 0;
            while (($line = fgets($fh)) !== false) {
                $line = trim($line);
                $c = explode("=", $line);
                if ($c[0] == 'slicestart' && substr($c[1], 0, 4) == 'save') {
                    $set_id = substr($c[1], 5);
                    break;
                }
            }
            fclose($fh);
            if ($set_id != 0) {
                $result = $this->_GetSavedResults($set_id);
            } else {
                $res = rtrim(file_get_contents("query_results/$qid/results"));
                $result = json_decode($res);
            }

            return array("help" => $doc, "status" => "ok", "results" => $result);
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }

    /////////////////////////
    // RERUN SAVED RESULTS //
    /////////////////////////

    protected function ReRunSavedResult($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Repeat the identical query behind a saved variant query set, obtain/resave the results",
            "method" => "GET",
            "arguments" => 'none',
            "parameters" => array(
                "sid" => "sample_id (numeric)",
                "setid" => "identifier of saved results (numeric)",
                "save" => "Save results under this name (optional)",
                "comments" => "comments for newly saved set (optional)"
            ),
            "return" => array(
                "query_key" => "numeric id of the query process",
                "queue_position" => "indication of position on processing queue"
            )
        );
        if ($this->method == 'GET') {
            // need a Sample ID, and a Saved Set ID
            // sid: sample.
            $pars = array(); // for post call
            $pars['apiKey'] = $this->request['apiKey'];
            $pars['region'] = 'false';
            $pars['project'] = 'false';
            // sampleid
            if (isset($args[0])) {
                if (is_numeric($args[0])) {
                    $pars['sid'] = $args[0];
                } elseif (!array_key_exists('sid', $this->request) || !is_numeric($this->request['sid'])) {
                    return array("help" => $doc, "status" => "error", "msg" => "ReRun Saved Query requires a numeric Sample_ID as 'sid'");
                } else {
                    $pars['sid'] = $this->request['sid'];
                }
            } else {
                $pars['sid'] = $this->request['sid'];
                if (!is_numeric($pars['sid'])) {
                    return array("help" => $doc, "status" => "error", "msg" => "ReRun Saved Query requires a numeric Sample_ID as 'sid'");
                }
            }
            // access ?
            $access = $this->_runQuery("SELECT ps.sid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON ps.pid = pu.pid WHERE pu.uid = '" . $this->uid . "' AND ps.sid = '" . $pars['sid'] . "' AND pu.editvariant = 1", False);
            if (count($access) == 0) {
                return array("help" => $doc, "status" => "error", "msg" => 'No edit rights on requested sample');
            }

            // get filters & annotations from set_id
            $set_filters = '';
            $set_filtertree = '';
            $set_annotations = '';
            if (array_key_exists('setid', $this->request) && is_numeric($this->request['setid'])) {
                $setid = $this->request['setid'];
                // filters.
                $row = $this->_runQuery("SELECT `set_filters`, `set_filtertree`, `set_annotations` FROM `Samples_x_Saved_Results` WHERE `set_id` = '$setid'", False);
                // filters.
                $set_filters = $row['set_filters'];
                $set_filtertree = $this->_SqlEscapeValues($row['set_filtertree']);
                $pars = $this->_ParseFilter($set_filters, $set_filtertree, $pars);
                // annotations.
                $set_annotations = $row['set_annotations'];
                $pars = $this->_ParseAnnotations($set_annotations, $pars);
                $set_annotations = rtrim($set_annotations, '@@@');
            } else {
                return array("help" => $doc, "status" => "error", "msg" =>  "ReRun Saved Query requires a numeric Saved Set ID (see sample/sid to show the list)");
            }
            // save again ?
            if (array_key_exists('save', $this->request)) {
                $name = $this->_SqlEscapeValues($this->request['save']);
                // get name
                if ($name == '') {
                    $name = "Result_Set." . date("Y-m-d");
                }
                // get comments
                $comments = 'Saved by API';
                if (array_key_exists('comments', $this->request)) {
                    $comments = $this->_SqlEscapeValues($this->request['comments']);
                }
                // create set.
                $sid = $pars['sid'];
                $uid = $this->uid;
                $set_id = $this->_insertQuery("INSERT INTO `Samples_x_Saved_Results` (sid,uid,`set_name`,`set_comments`,`set_filters`,`set_filtertree`,`set_annotations`,`set_vids`) VALUES ('$sid','$uid','$name','$comments','$set_filters','$set_filtertree','$set_annotations','Queued')");
                $slicestart = 'save|' . $set_id;
                $pars['slicestart'] = $slicestart;
            } else {
                $pars['fmt'] = 'json';
                $pars['slicestart'] = "0";
            }

            // add the build
            $row = $this->_runQuery("SELECT name FROM `NGS-Variants-Admin`.`CurrentBuild` WHERE 1", False);
            $hg = substr($row['name'], 1);
            $pars['build'] = $hg;
            // parse to correct format.
            $post_items = array();
            foreach ($pars as $key => $value) {
                if (strstr($key, "ssqv")) {
                    trigger_error("encode value: $value");
                    trigger_error("  => " . urlencode($value));
                }
                $post_items[] = $key . '=' . urlencode($value);
            }
            $post_string = implode('&', $post_items);
            // submit using curl functionality.
            $url = "https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/cgi-bin/API_wrapper.py";
            $json = $this->_InternalCall($url, $post_string);
            // $curl_connection = curl_init($url);
            // curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 300);
            // curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
            // //curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
            // //curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
            // curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
            // $result = curl_exec($curl_connection);
            // #print_r(curl_getinfo($curl_connection));
            // #echo curl_errno($curl_connection) . '-' . curl_error($curl_connection);
            // //close the connection
            // curl_close($curl_connection);
            // $json = json_decode($result);
            // // error ? 
            // if (!$result) {
            //     return array("help" => $doc, "status" => "error", "msg" => "No response recieved from CGI: $result");
            // } else
            if (isset($json->ERROR)) {
                return array("help" => $doc, "status" => "error", "msg" => $json->ERROR);
            } else {
                return array("help" => $doc, "status" => "ok", 'results' => $json);
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "only accepts GET requests");
        }
    }

    /////////////////////////////////
    //                             //
    // SECTION 4 :: IMPORTING DATA //
    //                             //
    /////////////////////////////////
    /************************/
    /** LAUNCH DATA IMPORT **/
    /************************/
    protected function ImportData($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Start import of samples, prestructured using PrepareImportData",
            "method" => "GET",
            "verb" => "working directory, provided by PrepareImportData",
            "arguments" => 'none',
            "return" => array(
                "job_key" => "numeric. import identifier to track progress (GetStatus)",
                "result" => "Started / Error.",
                "error" => "optional. message if import could not be started"
            )
        );
        if ($this->method == 'GET') {
            // Working dir provided as verb.
            $wd = $this->config['FTP_DATA'] . '/VariantDB_API/' . $this->verb;
            if (!isset($this->verb) || $this->verb == '' || !file_exists($wd)) {
                return array("help" => $doc, "status" => "error", "msg" => "Specified folder '$wd' does not exist");
            }
            // get nr of samples.
            //if (!isset($args[0]) || !is_numeric($args[0])) {
            //    return array("help" => $doc, "status" => "error", "msg" => "Number of samples not provided.");
            //}
            //$nrs = $args[0];
            // make sure annotation services are running
            $result = $this->_InternalCall("https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/ajax_queries/ServiceCheck.php");
            if (isset($result->ERROR)) {
                return array("help" => $doc, "status" => "error", "msg" => $result->ERROR);
            }
            // submit.
            $post_string = "s=API&datadir=$wd&uid=" . $this->uid;
            $url = "https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/cgi-bin/Import_Wrapper.py";
            $result = $this->_InternalCall($url, $post_string);
            // $curl_connection = curl_init($url);
            // curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 300);
            // curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
            // $result = curl_exec($curl_connection);
            // //close the connection
            // curl_close($curl_connection);
            // $result = json_decode($result);
            // error ? 
            if (isset($result->ERROR)) {
                return array("help" => $doc, "status" => "error", "msg" => $result->ERROR);
            } else {
                return array("help" => $doc, "status" => "ok", 'results' => $result);
            }
            //exec("cd " . $this->config['SCRIPTDIR'] . "/cgi-bin ; perl galaxy_communication.cgi 'API' '$wd' '$nrs'", $output, $exit);
            //exec("cd " . $this->config['SCRIPTDIR'] . "/cgi-bin ; python ");
            //return array("help" => $doc, "status" => "ok", "results" => json_decode($output[0]));
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }
    /********************************/
    /* BUILD DATADIR FOR IMPORT JOB */
    /********************************/
    protected function PrepareImportData($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Structure external data into an internal import-ready format working directory (note: numbering starts at 1)",
            "method" => "GET/POST",
            "arguments" => 'none',
            "parameters" => array(
                'g\d' => 'gender for sample \d (digit)',
                'n\d' => 'Name for sample \d (digit)',
                'v\d' => 'VCF path for sample \d (digit), accessible from VDB server or https/ftp URL',
                'f\d' => 'Format of VCF \d (digit) : Options are UG (unified genotyper), HC (haplotypecaller)',
                'b\d' => 'BAM path for sample \d (digit), accessible from VDB server or https/ftp URL',
                's\d' => 'Store datafiles for sample \d in internal storage (0/1), defaults to 0',
                'cf_name\i' => 'custom fields to parse from VCF',
                'cf_type\i' => 'type of values in field \i (integer) : list, decimal, varchar',
                'p\s' => "Project name"
            ),

            "return" => array(
                "path" => "path of the generated working directory. Pass to 'ImportData'",
                "status" => "ok"

            ),
        );
        if ($this->method == 'GET' || $this->method == 'POST') {
            // construct datadir path.
            $uid = $this->uid;
            $time = time();
            if (!file_exists($this->config['FTP_DATA'] . "/VariantDB_API/")) {
                system("echo " . $this->config['SCRIPTPASS'] . " | sudo -u " . $this->config['SCRIPTUSER'] . " -S bash -c \"mkdir '" . $this->config['FTP_DATA'] . "/VariantDB_API/' && chmod 777 '" . $this->config['FTP_DATA'] . "/VariantDB_API/'\"");
            }
            $path = $this->config['FTP_DATA'] . "/VariantDB_API/$uid" . "_$time";
            while (is_dir($path)) {
                trigger_error("Directory exists : $path");
                sleep(1);
                $time = time();
                $path = $this->config['FTP_DATA'] . "/VariantDB_API/$uid" . "_$time";
            }
            // construct strings.
            $i = 1;
            $no_access = $names = $formats = $genders = $vcfs = $bams = '';
            $store = array();

            while (array_key_exists("n$i", $this->request)) {
                if (!array_key_exists("v$i", $this->request)) {
                    return array("help" => $doc, "status" => "error", "msg" => 'No VCF path provided for sample ' . $i);
                } else {
                    $vcf = $this->request["v$i"];
                    // access ?
                    if (!is_readable($vcf) && !preg_match('/^(http|ftp)/', $vcf) && $vcf != '') {
                        $no_access .= "$vcf;";
                    } else {
                        // add to valid items.
                        $vcfs .= "vcf$i==" . $this->request["v$i"] . "\n";
                    }
                }
                $names .= "name$i==" . $this->request["n$i"] . "\n";
                if (array_key_exists("g$i", $this->request)) {
                    $genders .= "gender$i==" . $this->request["g$i"] . "\n";
                } else {
                    $genders .= "gender$i==undef\n";
                }
                if (array_key_exists("f$i", $this->request)) {
                    $formats .= "format$i==" . $this->request["f$i"] . "\n";
                } else {
                    $formats .= "format$i==UG\n";
                }
                if (array_key_exists("b$i", $this->request)) {
                    $bam = $this->request["b$i"];
                    if (!is_readable($bam) && !preg_match('/^(http|ftp)/', $bam) && $bam != '') {
                        $no_access .= "$bam;";
                    } else {
                        $bams .= "bam$i==" . $this->request["b$i"] . "\n";
                    }
                } else {
                    $bams .= "bam$i==\n";
                }
                if (array_key_exists("s$i", $this->request)) {
                    $store[$i] = $this->request["s$i"];
                } else {
                    $store[$i] = 0;
                }

                $i++;
            }
            if ($names == '') {
                return array("help" => $doc, "status" => "error", "msg" => 'No information provided about samples.');
            }
            if ($no_access != '') {
                return array("help" => $doc, "status" => "error", "msg" => "The following files could not be accessed from VariantDB: " . substr($no_access, 0, -1) . "\n");
            }

            // ok. create datafiles.
            mkdir("$path");
            if (array_key_exists("p", $this->request)) {
                $fh = fopen("$path/project.name.txt", "w");
                fwrite($fh, $this->request['p']);
                fclose($fh);
            }
            $fh = fopen("$path/names.txt", "w");
            fwrite($fh, $names);
            fclose($fh);
            $fh = fopen("$path/genders.txt", "w");
            fwrite($fh, $genders);
            fclose($fh);
            $fh = fopen("$path/formats.txt", "w");
            fwrite($fh, $formats);
            fclose($fh);
            $fh = fopen("$path/uid.txt", "w");
            fwrite($fh, $this->uid);
            fclose($fh);
            $fh = fopen("$path/vcfs.txt", "w");
            fwrite($fh, $vcfs);
            fclose($fh);
            $fh = fopen("$path/bams.txt", "w");
            fwrite($fh, $bams);
            fclose($fh);
            // store file triggers.
            foreach ($store as $index => $value) {
                $fh = fopen("$path/store.$index.txt", "w");
                fwrite($fh, $value);
                fclose($fh);
            }

            // any custom fields to store?
            $fidx = 1;
            while (array_key_exists("cf_name$fidx", $this->request)) {
                $fh = fopen("$path/cf.txt", "a");
                fwrite($fh, "field_$fidx=" . $this->request["cf_name$fidx"] . "\n");
                if (array_key_exists("cf_type$fidx", $this->request)) {
                    fwrite($fh, "type_$fidx=" . $this->request["cf_type$fidx"] . "\n");
                } else {
                    fwrite($fh, "type_$fidx=varchar\n");
                }
                fclose($fh);
                $fidx++;
            }
            // are we importing in lockdown-mode && user == admin?
            if (array_key_exists("l", $this->request) && $this->request['l'] == 1 && $this->UserLevel == 3) {
                $fh = fopen("$path/lockdbs.txt", "w");
                fwrite($fh, 1);
                fclose($fh);
            }
            // return working path.
            return array("help" => $doc, "status" => "ok", 'results' => array("status" => "ok", "path" => "$uid" . "_$time"));
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET/POST requests");
        }
    }
    //////////////////////////
    // FORCE ANNOTATION NOW //
    //////////////////////////
    // normally it starts once / hour
    protected function ForceAnnotationLaunch($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Force Annotation jobs to start in next iteration. This is only allowed for admin users",
            "method" => "GET",
            "arguments" => 'none',
            "parameters" => 'none',
            "return" => array(
                "status" => "ok"
            ),
        );
        if ($this->method == 'GET') {
            // insufficient rights
            if ($this->UserLevel < 3) {
                return array("help" => $doc, "status" => "error", "msg" => "Insufficient Access");
            }
            // touch the trigger.
            $fh = fopen($this->config['SCRIPTDIR'] . "/Annotations/update.force.txt", "w");
            fwrite($fh, 1);
            fclose($fh);
            return array("help" => $doc, "status" => "ok", 'results' => array("status" => "ok"));
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }


    ////////////////////////////////////
    // SECTION 5 :: PRIVATE FUNCTIONS //
    ////////////////////////////////////

    // these do not follow the doc/status/msg|results format

    // call some variantdb endpoint.
    private function _InternalCall($url, $post_string = NULL, $json_decode = true)
    {
        // initialize   
        $curl_connection = curl_init($url);
        curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_connection, CURLOPT_FAILONERROR, true);
        if (!is_null($post_string)) {
            curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
        }
        $result = curl_exec($curl_connection);
        if ($result === false) {
            trigger_error("Call to url failed: '$url' : " . curl_errno($curl_connection) . " : " . curl_error($curl_connection), E_USER_ERROR);
            $result = array();
            $result['ERROR'] = "Call to url failed: '$url' : error code was " . curl_errno($curl_connection);
            // make it compatible with other results
            $result = json_encode($result);
            if ($json_decode) {
                $result = json_decode($result);
            }
            return $result;
        }

        //close the connection
        curl_close($curl_connection);
        if ($json_decode) {
            $result = json_decode($result);
        }
        return $result;
    }

    // Recursive function to build pedigree (internal use only)
    private function _GetFamily($sid, $return = array())
    {

        if (!isset($return[$sid])) {
            $return[$sid] = array();
            $return[$sid]['Replicates'] = array();
            $return[$sid]['Siblings'] = array();
            $return[$sid]['Parents'] = array();
            $return[$sid]['Children'] = array();
        }

        //1. sample replicates
        $rows = $this->_runQuery("SELECT s2.Name AS sn2, s2.id AS sid2,s2.gender AS gender2 FROM `Samples` s2 JOIN `Samples_x_Samples` ss ON s2.id = ss.sid2 WHERE ss.sid1 = $sid AND ss.Relation = 1");
        foreach ($rows as $row) {
            if (!isset($return[$sid]['Replicates'][$row['sid2']])) {
                $return[$sid]['Replicates'][$row['sid2']] = $row['sn2'];
                $return = $this->_GetFamily($row['sid2'], $return);
            }
        }
        //2. siblings
        $rows = $this->_runQuery("SELECT s2.Name AS sn2, s2.id AS sid2, s2.gender AS gender2 FROM `Samples` s2 JOIN `Samples_x_Samples` ss ON s2.id = ss.sid2 WHERE ss.sid1 = $sid AND ss.Relation = 3");
        foreach ($rows as $row) {
            if (!isset($return[$sid]['Siblings'][$row['sid2']])) {
                $return[$sid]['Siblings'][$row['sid2']]['sample_name'] = $row['sn2'];
                $return[$sid]['Siblings'][$row['sid2']]['sample_gender'] = $row['gender2'];
                $return = $this->_GetFamily($row['sid2'], $return);
            }
        }
        //3. parents
        $rows = $this->_runQuery("SELECT s2.Name AS sn2, s2.id AS sid2,s2.gender AS gender2 FROM `Samples` s2 JOIN `Samples_x_Samples` ss ON s2.id = ss.sid1 WHERE ss.sid2 = $sid AND ss.Relation = 2");
        foreach ($rows as $row) {
            if (!isset($return[$sid]['Parents'][$row['sid2']])) {
                $return[$sid]['Parents'][$row['sid2']]['sample_name'] = $row['sn2'];
                $return[$sid]['Parents'][$row['sid2']]['sample_gender'] = $row['gender2'];
                $return = $this->_GetFamily($row['sid2'], $return);
            }
        }
        //4. children
        $rows = $this->_runQuery("SELECT s2.Name AS sn2, s2.id AS sid2,s2.gender AS gender2 FROM `Samples` s2 JOIN `Samples_x_Samples` ss ON s2.id = ss.sid2 WHERE ss.sid1 = $sid AND ss.Relation = 2");
        foreach ($rows as $row) {
            if (!isset($return[$sid]['Children'][$row['sid2']])) {
                $return[$sid]['Children'][$row['sid2']]['sample_name'] = $row['sn2'];
                $return[$sid]['Children'][$row['sid2']]['sample_gender'] = $row['gender2'];
                $return = $this->_GetFamily($row['sid2'], $return);
            }
        }
        if (!isset($return[$sid])) {
            $return[$sid] = array();
            $return[$sid]['Replicates'] = array();
            $return[$sid]['Siblings'] = array();
            $return[$sid]['Parents'] = array();
            $return[$sid]['Children'] = array();
        }
        return ($return);
    }
    // CHECK organism for a geneID on NCBI
    private function _IsHuman($geneID)
    {
        sleep(2);
        $url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gene&id=$geneID&retmode=json";
        $info = $this->_GetJSON($url);
        if (property_exists($info, 'ERROR')) {
            trigger_error("Failed to get result from '$url' : " . $info['ERROR'], E_USER_ERROR);
            return 0;
        } else {
            $org = $info->{'result'}->{$geneID}->{'organism'}->{'taxid'};
            if ($org == 9606) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    /*****************************/
    /* GENERAL BED FILE CREATION */
    /*****************************/
    private function GetBED($gp, $summary = 1, $type = 'refseq')
    {
        $return = array('comments' => '');
        // get genome build.
        $row = $this->_runQuery("SELECT name FROM `NGS-Variants-Admin`.`CurrentBuild` WHERE 1", False);
        $hg = substr($row['name'], 1);
        $return['build'] = $hg;
        // annovar data available?  (default to ncbi/refseq gene data)
        if (strtolower($type) == 'refgene') {
            $gene_file = "refGene";
            $link_file = "refLink";
        } else {
            $gene_file = "ncbiGene";
            $link_file = "refLink_ncbigene";
        }
        if (!file_exists("../Annotations/ANNOVAR/humandb/$hg" . "_" . $link_file . ".txt")) {
            $return['ERROR'] = "Missing annotation file: ANNOVAR/humandb/$hg" . "_" . $link_file . ".txt";
            trigger_error("Missing annotation file: ANNOVAR/humandb/$hg" . "_" . $link_file . ".txt", E_USER_ERROR);
            return ($return);
        }
        if (!file_exists("../Annotations/ANNOVAR/humandb/$hg" . "_" . $gene_file . ".txt")) {
            $return['ERROR'] = "Missing annotation file: ANNOVAR/humandb/$hg" . "_" . $gene_file . ".txt";
            trigger_error("Missing annotation file: ANNOVAR/humandb/$hg" . "_" . $gene_file . ".txt", E_USER_ERROR);
            return ($return);
        }
        // read in refLink file
        $NMs = array();
        $fh = fopen("../Annotations/ANNOVAR/humandb/$hg" . "_" . $link_file . ".txt", 'r');
        $counter = 0;
        $IsHumanResults = array();
        while (($line = fgets($fh)) !== false) {
            $line = trim($line);
            $c = explode("\t", $line);
            // gene without geneID.
            if (count($c) < 7 || $c[6] == '') {
                continue;
            }
            // work without vesion ids: 
            if (preg_match("/(.*)\.\d+/", $c[2], $matches)) {
                $c[2] = $matches[1];
            }
            // from genepanel : "symbol|id"
            if (array_key_exists($c[0] . "|" . $c[6], $gp)) {
                $NMs[$c[2]]['symbol'] = $c[0] . "|" . $c[6];
            }
            // from plain gene list : "symbol"
            elseif (array_key_exists($c[0], $gp)) {
                // human?
                if (!array_key_exists($c[6], $IsHumanResults)) {
                    $IsHumanResults[$c[6]] = $this->_IsHuman($c[6]);
                }
                if ($IsHumanResults[$c[6]]) {
                    $return['comments'] .= $c[0] . " found as GeneID: " . $c[6] . "; ";
                    $NMs[$c[2]]['symbol'] = $c[0] . "|" . $c[6];
                    // replace in list.
                    $gp[$c[0] . "|" . $c[6]] = $c[0];
                    unset($gp[$c[0]]);
                }
            }
        }
        fclose($fh);
        // summarized data
        $cds_report = "";
        $full_report = "";
        $non_coding = array();
        $comments = array();
        $cds = array();
        $full = array();
        $genes_x_chr = array();
        $symbol_strands = array();

        // non-summarized data...
        $cds_out = '';
        $cds_size = array();
        $full_out = '';
        $full_size = array();
        $cds_nc_out = array();
        $cds_nc_size = array();
        $coding = array();
        // read in refGene file.
        $fh = fopen("../Annotations/ANNOVAR/humandb/$hg" . "_" . $gene_file . ".txt", 'r');
        while (($line = fgets($fh)) !== false) {
            $line = trim($line);
            $c = explode("\t", $line);
            // skip non-canonical chromosomes.
            if (preg_match("/_/", $c[2])) {
                continue;
            }
            $full_NM = $c[1];
            // reflink based on non-versioned NM
            if (preg_match("/(.*)\.\d+/", $c[1], $matches)) {
                $c[1] = $matches[1];
            }

            // only process transcripts related to gene panel.
            if (!array_key_exists($c[1], $NMs)) {
                continue;
            }
            $chr = $c[2];
            // store strand of the transcript.
            $strand = $c[3];
            $NMs[$c[1]]['strand'] = $strand;
            // transcript level data.
            $nm = $full_NM; //$c[1];
            // exons
            $estarts = explode(",", $c[9]);
            $eends = explode(",", $c[10]);
            // get symbol
            $symbol = $gp[$NMs[$c[1]]['symbol']];
            $symbol_strands[$symbol] = $strand;
            /////////////////
            // PREPARE     //
            ////////////////
            // first transcript of gene.
            // if non-coding: include whole gene, if NO coding transcripts found.
            $nc = '';
            # if non coding : CDS start and end are set to gene end.
            if ($c[6] == $c[5] && $c[7] == $c[5]) {
                $cds_start = $c[4];
                $cds_stop = $c[5];
                //$non_coding[$c[1]] = array();
                $nc = '.nc';
                if (!array_key_exists($symbol, $cds_nc_out)) {
                    $cds_nc_out[$symbol] = '';
                    $cds_nc_size[$symbol] = array();
                }
                $cds_nc_size[$symbol][$nm] = 0;
            } else {
                $cds_start = $c[6];
                $cds_stop = $c[7];
                $coding[$symbol] = 1;
                //$cds[$symbol] = 1; // a counter for report of unique entries in CDS bed.
            }
            // special care of PAR region: genes are present twice!
            if (array_key_exists($symbol . $nc, $genes_x_chr) && $genes_x_chr[$symbol . $nc] == 'chrX' && $c[2] == 'chrY') {
                # gene seen before on X, now on Y : skip Y
                continue;
            }
            if (array_key_exists($symbol . $nc, $genes_x_chr) && $genes_x_chr[$symbol . $nc] == 'chrY' && $c[2] == 'chrX') {
                # gene seen before on Y, now on X : clear entries on Y
                unset($genes_x_chr[$symbol . $nc]);
                unset($cds[$symbol . $nc]);
                unset($full[$symbol]);
            }
            //////////////////////////////
            // PROCESS FULL TRANSCRIPTS //
            //////////////////////////////
            // first transcript of gene.
            if (!array_key_exists($symbol, $full)) {
                $full[$symbol] = array();
            }
            if ($strand == '+') {
                $eidx = 0;
            } else {
                $eidx = count($estarts); // is one too high
            }

            $genes_x_chr[$c[1]] = $c[2];
            for ($i = 0; $i < (count($estarts) - 1); $i++) {
                if ($summary == 0) {
                    if ($strand == '+') {
                        $eidx++;
                    } else {
                        $eidx--;
                    }
                    $full_out .= $chr . "\t" . $estarts[$i] . "\t" . $eends[$i] . "\t$symbol($nm)|Exon_" . ($eidx) . "\t0\t$strand\n";
                    if (!array_key_exists($symbol, $full_size)) {
                        $full_size[$symbol] = array($nm => 0);
                    } elseif (!array_key_exists($nm, $full_size[$symbol])) {
                        $full_size[$symbol][$nm] = 0;
                    }
                    $full_size[$symbol][$nm] += $eends[$i] - $estarts[$i];
                    $full[$symbol] = 1;
                } else {

                    // combine in stored data.
                    $overlap = 0;
                    for ($j = 0; $j < count($full[$symbol]); $j++) {
                        //identical || within
                        if ($full[$symbol][$j]['b'] <= $estarts[$i] && $full[$symbol][$j]['e'] >= $eends[$i]) {
                            $overlap = 1;
                            break;
                        }
                        // 5' overlap => extend upstream
                        if ($full[$symbol][$j]['b'] >= $estarts[$i] && $full[$symbol][$j]['b'] <= $eends[$i] && $full[$symbol][$j]['e'] >= $eends[$i]) {
                            $full[$symbol][$j]['b'] = $estarts[$i];
                            $overlap = 1;
                            break;
                        }
                        // 3' overlap => extend downstream
                        if ($full[$symbol][$j]['b'] <= $estarts[$i] && $full[$symbol][$j]['e'] >= $estarts[$i] && $full[$symbol][$j]['e'] <= $eends[$i]) {
                            $full[$symbol][$j]['e'] = $eends[$i];
                            $overlap = 1;
                            break;
                        }
                        // 2 end overlap
                        if ($full[$symbol][$j]['b'] > $estarts[$i] && $full[$symbol][$j]['e'] < $eends[$i]) {
                            $full[$symbol][$j]['e'] = $eends[$i];
                            $full[$symbol][$j]['b'] = $estarts[$i];
                            $overlap = 1;
                            break;
                        }
                    }
                    if ($overlap == 0) {
                        $full[$symbol][] = array('b' => $estarts[$i], 'e' => $eends[$i]);
                    }
                }
            }
            /////////
            // CDS //
            /////////
            if (!array_key_exists($symbol . $nc, $cds)) {
                $cds[$symbol . $nc] = array();
            }

            $genes_x_chr[$symbol . $nc] = $c[2];
            //counting direction.
            if ($strand == '+') {
                $eidx = 0;
            } else {
                $eidx = count($estarts); // is one too high
            }

            for ($i = 0; $i < (count($estarts) - 1); $i++) {
                // upstream
                if ($eends[$i] < $cds_start) {
                    if ($strand == '+') {
                        $eidx++;
                    } else {
                        $eidx--;
                    }
                    continue;
                }
                // downstream
                if ($estarts[$i] > $cds_stop) {
                    break;
                }
                // overlap at 5'
                if ($estarts[$i] < $cds_start && $eends[$i] >= $cds_start) {
                    $estarts[$i] = $cds_start;
                }
                // overlap at 3'.
                if ($estarts[$i] <= $cds_stop && $eends[$i] > $cds_stop) {
                    $eends[$i] = $cds_stop;
                }
                // plain add.
                if ($summary == 0) {
                    if ($strand == '+') {
                        $eidx++;
                    } else {
                        $eidx--;
                    }
                    if ($nc == '') {
                        $cds_out .= $chr . "\t" . $estarts[$i] . "\t" . $eends[$i] . "\t$symbol$nc($nm)|Exon_" . ($eidx) . "\t0\t$strand\n";
                        if (!array_key_exists($symbol, $cds_size)) {
                            $cds_size[$symbol] = array($nm => 0);
                        } elseif (!array_key_exists($nm, $cds_size[$symbol])) {
                            $cds_size[$symbol][$nm] = 0;
                        }
                        $cds_size[$symbol][$nm] += $eends[$i] - $estarts[$i];
                    } else {
                        $cds_nc_out[$symbol] .= $chr . "\t" . $estarts[$i] . "\t" . $eends[$i] . "\t$symbol$nc($nm)|Exon_" . ($eidx) . "\t0\t$strand\n";
                        $cds_nc_size[$symbol][$nm] += $eends[$i] - $estarts[$i];
                    }
                }
                // combine transcripts.
                else {
                    $overlap = 0;
                    // go over previously stored exons to find overlap.
                    for ($j = 0; $j < count($cds[$symbol . $nc]); $j++) {
                        //identical || within
                        if ($cds[$symbol . $nc][$j]['b'] <= $estarts[$i] && $cds[$symbol . $nc][$j]['e'] >= $eends[$i]) {
                            $overlap = 1;
                            break;
                        }
                        // 5' overlap => extend upstream
                        if ($cds[$symbol . $nc][$j]['b'] >= $estarts[$i] && $cds[$symbol . $nc][$j]['b'] <= $eends[$i] && $cds[$symbol . $nc][$j]['e'] >= $eends[$i]) {
                            $cds[$symbol . $nc][$j]['b'] = $estarts[$i];
                            $overlap = 1;
                            break;
                        }
                        // 3' overlap => extend downstream
                        if ($cds[$symbol . $nc][$j]['b'] <= $estarts[$i] && $cds[$symbol . $nc][$j]['e'] >= $estarts[$i] && $cds[$symbol . $nc][$j]['e'] <= $eends[$i]) {
                            $cds[$symbol . $nc][$j]['e'] = $eends[$i];
                            $overlap = 1;
                            break;
                        }
                        // 2 end overlap
                        if ($cds[$symbol . $nc][$j]['b'] > $estarts[$i] && $cds[$symbol . $nc][$j]['e'] < $eends[$i]) {
                            $cds[$symbol . $nc][$j]['e'] = $eends[$i];
                            $cds[$symbol . $nc][$j]['b'] = $estarts[$i];
                            $overlap = 1;
                            break;
                        }
                    }
                    if ($overlap == 0) {
                        $cds[$symbol . $nc][] = array('b' => $estarts[$i], 'e' => $eends[$i]);
                    }
                }
            }
        }
        fclose($fh);
        // custom sorting function
        function sortByBegin($a, $b)
        {
            return $a['b'] - $b['b'];
        }
        // add to return array: info CDS
        //$return['cds'] = $cds;
        $return['cds_bed'] = '';
        $nr_cds_genes = 0; //count($cds);
        $return['cds_size'] = 0;
        $return['nr_cds_genes'] = 0;

        if ($summary != 0) {
            $return['nr_cds_genes'] = 0;
            // merge overlapping exons for CDS.
            foreach ($cds as $symbol => $regions) {
                $overlap = 1;
                while ($overlap == 1) {
                    $overlap = 0;
                    $last_start = $last_stop = -1;
                    $my_cds = $regions;
                    usort($my_cds, 'sortByBegin');
                    $new_cds = array();
                    $nr_new = -1;
                    for ($j = 0; $j < count($my_cds); $j++) {
                        // region is complete 3' to previous one
                        if ($my_cds[$j]['b']  > $last_stop) {
                            $nr_new++;
                            $new_cds[] = $my_cds[$j];
                            $last_start = $my_cds[$j]['b'];
                            $last_stop = $my_cds[$j]['e'];
                            continue;
                        }
                        // region extends 3'
                        if ($my_cds[$j]['b'] <= $last_stop && $my_cds[$j]['e'] > $last_stop) {
                            $new_cds[$nr_new]['e'] = $my_cds[$j]['e'];
                            $last_stop = $my_cds[$j]['e'];
                            $overlap = 1;
                            continue;
                        }
                        // 5' extension is not possible (sorted data)
                        // fully inside => doesn't need to change.

                    }
                    $regions = $new_cds;
                }
                $cds[$symbol] = $regions;
            }
        }
        // construct BED.
        if ($summary == 0) {
            // any non-coding genes?
            foreach ($cds_nc_out as $symbol => $content) {
                if (!array_key_exists($symbol, $coding)) {
                    // purely non-coding gene. add to output.
                    $cds_out .= $content;
                    foreach ($cds_nc_size[$symbol] as $nm => $size) {
                        $cds_size[$symbol][$nm] = $size;
                    }
                    $non_coding[$symbol] = 1;
                    // add to cds counter
                    $cds[$symbol] = 1;
                }
            }
            $return['cds_bed'] = $cds_out;
            foreach ($cds_size as $symbol => $nms) {
                $return['cds_size'] += array_sum($nms);
            }
            $return['nr_cds_genes'] = count($cds_size);
        } else {
            foreach ($cds as $symbol => $regions) {
                // if nc variant of coding gene, skip.
                $strand = "+";
                if (substr($symbol, -3) == '.nc') {
                    if (array_key_exists(substr($symbol, 0, -3), $cds)) {
                        continue;
                    } else {
                        $strand = $symbol_strands[substr($symbol, 0, -3)];
                        $non_coding[substr($symbol, 0, -3)] = 1;
                    }
                } else {
                    $strand = $symbol_strands[$symbol];
                }
                $return['nr_cds_genes']++;
                for ($j = 0; $j < count($regions); $j++) {
                    $return['cds_bed'] .= $genes_x_chr[$symbol] . "\t" . $regions[$j]['b'] . "\t" . $regions[$j]['e'] . "\t$symbol|Region_" . ($j + 1) . "\t0\t$strand\n";
                    $return['cds_size'] += ($regions[$j]['e'] - $regions[$j]['b'] + 1);
                }
            }
        }


        // add to return array: info FULL.
        $return['full_bed'] = '';

        $return['full_size'] = 0;
        if ($summary == 0) {
            $return['full_bed'] = $full_out;
            foreach ($full_size as $symbol => $nms) {
                $return['full_size'] += array_sum($nms);
            }
            $return['nr_full_genes'] = count($full_size);
        } else {
            $return['nr_full_genes'] = count($full);
            // merge overlapping exons for FULL.
            foreach ($full as $symbol => $regions) {
                $overlap = 1;
                while ($overlap == 1) {
                    $overlap = 0;
                    $last_start = $last_stop = -1;
                    $my_full = $regions;
                    usort($my_full, 'sortByBegin');
                    $new_full = array();
                    $nr_new = -1;
                    for ($j = 0; $j < count($my_full); $j++) {
                        // region is complete 3' to previous one
                        if ($my_full[$j]['b']  > $last_stop) {
                            $nr_new++;
                            $new_full[] = $my_full[$j];
                            $last_start = $my_full[$j]['b'];
                            $last_stop = $my_full[$j]['e'];
                            continue;
                        }
                        // region extends 3'
                        if ($my_full[$j]['b'] <= $last_stop && $my_full[$j]['e'] > $last_stop) {
                            $new_full[$nr_new]['e'] = $my_full[$j]['e'];
                            $last_stop = $my_full[$j]['e'];
                            $overlap = 1;
                            continue;
                        }
                        // 5' extension is not possible (sorted data)
                        // fully inside => doesn't need to change.

                    }
                    $regions = $new_full;
                }
            }

            // build bed
            foreach ($full as $symbol => $regions) {
                $strand = $symbol_strands[$symbol];
                for ($j = 0; $j < count($regions); $j++) {
                    $return['full_bed'] .= $genes_x_chr[$symbol] . "\t" . $regions[$j]['b'] . "\t" . $regions[$j]['e'] . "\t$symbol|Region_" . ($j + 1) . "\t0\t$strand\n";
                    $return['full_size'] += ($regions[$j]['e'] - $regions[$j]['b'] + 1);
                }
            }
        }
        // missing genes ?
        $return['missing'] = '';
        $return['nr_missing'] = 0;
        $return['cds_report'] = '';
        $return['full_report'] = '';
        $nr_nc = 0;
        foreach ($gp as $symbol) {
            if (!array_key_exists($symbol, $full)) {
                $return['missing'] .= "$symbol, ";
                $return['nr_missing']++;
            }
        }
        if ($return['missing'] != '') {
            $return['missing'] = substr($return['missing'], 0, -2);
            $return['cds_report'] = $return['full_report'] = $return['nr_missing'] . " symbols were not recognized: " . $return['missing'] . "\n";
        }
        if (count($non_coding) > 0) {
            $return['cds_report'] .= count($non_coding) . " genes were non-coding, but added in full: " . implode(", ", array_keys($non_coding)) . "\n";
        }
        if ($gene_file == 'refGene') {
            $return['cds_report'] .= "BED file is based on RefGene data (recent RefSeq transcripts BLAT'ed to genome to get positions)\n";
            $return['full_report'] .= "BED file is based on RefGene data (recent RefSeq transcripts BLAT'ed to genome to get positions)\n";
        } else {
            $return['cds_report'] .= "BED file is based on RefSeq data (static RefSeq positions provided by NCBI)\n";
            $return['full_report'] .= "BED file is based on RefSeq data (static RefSeq positions provided by NCBI)\n";
        }
        // return.
        return ($return);
    }


    /*************************************/
    /** RETRIEVE SAVED RESULT AND PAGES **/
    /*************************************/
    private function _GetSavedResults($set_id)
    {
        // permission should be checked upstream.
        $result = array();
        $result['set_id'] = $set_id;
        // general info
        $row = $this->_runQuery("SELECT ss.sid, u.LastName, u.FirstName, ss.uid, ss.set_name, ss.set_comments, ss.set_vids, ss.date, ss.Validated, ss.Validation_Comments, ss.validation_uid, ss.deleted, ss.delete_comments, ss.delete_uid FROM `Samples_x_Saved_Results` ss JOIN `Users` u ON ss.uid = u.id WHERE ss.set_id = '$set_id'", False);

        $result['sample_id'] = $row['sid'];
        $result['Created_By'] = $row['LastName'] . " " . substr($row['FirstName'], 0, 1) . ".";
        $result['Created_By_uid'] = $row['uid'];
        $result['Set_Name'] = $row['set_name'];
        $result['Set_Comments'] = $row['set_comments'];
        $result['Nr_Variants'] = count(explode(",", $row['set_vids']));
        $result['Date'] = $row['date'];
        // validated ?
        $result['Validated'] = $row['Validated'];
        $result['Validation_Comments'] = $row['Validation_Comments'];
        if ($row['Validated'] == 1) {
            $sr = $this->_runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '" . $row['validation_uid'] . "'", False);
            $row['Validated_By'] = $sr['LastName'] . " " . substr($sr['FirstName'], 0, 1);
            $row['Validated_By_uid'] = $row['validation_uid'];
        }
        // deleted ?
        $result['Deleted'] = $row['deleted'];
        $result['Deletion_Comments'] = $row['delete_comments'];
        if ($row['deleted'] == 1) {
            $sr = $this->_runQuery("SELECT LastName, FirstName FROM `Users` WHERE id = '" . $row['delete_uid'] . "'", False);
            $row['Deleted_By'] = $sr['LastName'] . " " . substr($sr['FirstName'], 0, 1);
            $row['Deleted_By_uid'] = $row['delete_uid'];
        }
        // pages.
        $rows = $this->_runQuery("SELECT page, contents FROM `Samples_x_Saved_Results_Pages` WHERE set_id = '$set_id' ORDER BY page");
        $result['pages'] = array();
        foreach ($rows as $row) {
            $result['pages'][$row['page']] = $row['contents'];
        }
        return ($result);
    }

    /////////////////////////////////////////////////
    // reformat filters from DB to submission form. //
    /////////////////////////////////////////////////
    private function _ParseFilter($settings, $tree, $pars)
    {
        $p = explode("@@@", $settings);
        $nrR = 0;
        $do_names = array();
        foreach ($p as $k => $v) {
            $q = explode('|||', $v);
            //if (substr($q[0], 0, 4) == 'argu') {
            // argument values are differently concatenated in DB and runtime.
            //$q[1] = str_replace("__", "@@@", $q[1]);
            //} else
            if (substr($q[0], 0, 3) == 'cat') {
                $idx = substr($q[0], 8);
                if ($idx > $nrR) {
                    $nrR = $idx;
                }
            } elseif (substr($q[0], 0, 2) == 'do' && substr($q[0], -4) == 'name') {
                array_push($do_names, $q[0]);
            }
            //if (strstr($q[1], "+")) {
            //    trigger_error("update : $q[1]");
            //    $q[1] = str_replace("+", "&#43;", $q[1]);
            //    trigger_error("result: $q[1]");
            //}

            $pars[$q[0]] = $q[1];
        }

        $pars['listed'] = $nrR;

        $tree = str_replace("\\", "", $tree);
        $tree = str_replace('"{', '{', $tree);
        $tree = str_replace('}"', '}', $tree);
        $pars['logic'] = $tree;
        return $pars;
    }
    private function _ParseAnnotations($anno, $pars)
    {
        $idx = 0;
        $details = explode("@@@", $anno);
        foreach ($details as $kk => $value) {
            $idx++;
            $pars["anno$idx"] = $value;
        }
        $pars['nranno'] = $idx;
        return $pars;
    }
    /////////////////
    // json loader //
    /////////////////
    private function _GetJSON($url)
    {
        if ($json_str = file_get_contents($url)) {
            if ($json = json_decode($json_str)) {
                return ($json);
            } else {
                trigger_error("FAiled to decode : $json_str", E_USER_ERROR);
                return (array('ERROR' => 'failed to decode'));
            }
        } else {
            trigger_error("Failed to fetch url: $url", E_USER_ERROR);
            return (array('ERROR' => 'failed to fetch url'));
        }
    }
    /////////////////////////////
    // SummaryStatus Resetting //
    /////////////////////////////
    private function _clearSummaryStatus($type, $id)
    {
        if (!preg_match("/^[\d,]+$/", $id)) {
            return (array('ERROR' => 'Invalid ID provided, should be csv list of numeric values: ' . $id));
        }
        // permissions should be checked upstream.
        if ($type == 's') {
            // samples => projects => users
            $results = $this->_doQuery("UPDATE `Projects` SET SummaryStatus = 0 WHERE id IN (SELECT pid FROM `Projects_x_Samples` WHERE sid IN ($id))");
            if (!$results) {
                trigger_error("ERROR in query : UPDATE `Projects` SET SummaryStatus = 0 WHERE id IN (SELECT pid FROM `Projects_x_Samples` WHERE sid IN ($id))", E_USER_ERROR);
                return (array('ERROR' => "Could not Reset project summarystatus for samples $id"));
            }
            $results = $this->_doQuery("UPDATE `Users` SET SummaryStatus = 0 WHERE id IN (SELECT pu.uid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON pu.pid = ps.pid WHERE ps.sid IN ($id))");
            if (!$results) {
                trigger_error("ERROR in query : UPDATE `Users` SET SummaryStatus = 0 WHERE id IN (SELECT pu.uid FROM `Projects_x_Samples` ps JOIN `Projects_x_Users` pu ON pu.pid = ps.pid WHERE ps.sid IN ($id))", E_USER_ERROR);
                return (array('ERROR' => "Could not Reset user summarystatus for samples $id"));
            }
        } elseif ($type == 'p') {
            //projects => users
            $results = $this->_doQuery("UPDATE `Projects` SET SummaryStatus = 0 WHERE id IN ($id)");
            if (!$results) {
                trigger_error("ERROR in query : UPDATE `Projects` SET SummaryStatus = 0 WHERE id IN ($id)", E_USER_ERROR);
                return (array('ERROR' => "Could not Reset project summarystatus for projects $id"));
            }
            $results = $this->_doQuery("UPDATE `Users` SET SummaryStatus = 0 WHERE id IN (SELECT pu.uid FROM `Projects_x_Users` pu WHERE pu.pid IN ($id))");
            if (!$results) {
                trigger_error("ERROR in query : UPDATE `Users` SET SummaryStatus = 0 WHERE id IN (SELECT pu.uid FROM `Projects_x_Users` pu WHERE pu.pid IN ($id))", E_USER_ERROR);
                return (array('ERROR' => "Could not Reset user summarystatus for projects $id"));
            }
        } elseif ($type == 'u') {
            // users only.
            $results = $this->_doQuery("UPDATE `Users` SET SummaryStatus = 0 WHERE id IN ($id)");
            if (!$results) {
                trigger_error("ERROR in query : UPDATE `Users` SET SummaryStatus = 0 WHERE id IN ($id) ", E_USER_ERROR);
                return (array('ERROR' => "Could not Reset user summarystatus for users $id"));
            }
        } else {
            return (array('ERROR' => 'Unknown Summary type'));
        }
        return array('result' => 'ok');
    }

    // Function to test errorhandling. It contains a bad function call
    protected function _TestErrorHandling()
    {
        $doc = array(
            "goal" => "Validate Error Handling by triggering errors",
            "method" => "GET",
            "arguments" => 'String (Manual/Syntax) : type of error to simulate',
            "return" => "none",
        );
        if ($this->method == 'GET') {
            if (strtolower($this->verb) == 'manual') {
                // type one: manual trigger.
                trigger_error("This error originated from the API/TestErrorHandling routine", E_USER_ERROR);
            } elseif (strtolower($this->verb) == 'syntax') {
                // type two : bad function call:
                $myStr = str_replace('hello', 'world'); // missing input string
            } else {
                return array("help" => $doc, "status" => "error", "msg" => "Requires Manual/Syntax as argument");
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
        return array("help" => $doc, "status" => "ok", "results" => "");
    }
    // a method to show all available endpoints.
    public function ListMethods()
    {
        $return = array();
        foreach (get_class_methods($this) as $method) {
            // skip internal _<method>s
            if (substr($method, 0, 1) == '_') {
                continue;
            }
            $foo = new ReflectionMethod($this, $method);
            // only report "protected" methods:
            //    - public : from constructor
            //    - private : internal use only.
            if (strstr('protected', implode(Reflection::getModifierNames($foo->getModifiers())))) {
                $this->uid = -1;
                $this->apiKey = 'none';
                $this->GetDocs = 1;
                $resp = $this->$method('');
                if (is_array($resp) && array_key_exists('help', $resp)) {
                    $return[$method] = $resp['help'];
                } else {
                    $return[$method] = 'no documentation.';
                }
            }
        }
        return ($return);
    }
}

/////////////////////
// PROCESS REQUEST //
/////////////////////
// GET posted request.
if (isset($_GET['request'])) {
    $request = $_GET['request'];
} elseif (isset($_POST['request'])) {
    $request = $_POST['request'];
} else {
    // result : list available routines.
    $api = new MyAPI('listMethods');
    $result = array("status" => "error", "msg" => "no action provided. See help for available actions", 'help' => array('Methods' => $api->ListMethods(), 'Required parameter' => 'apiKey'));
    echo json_encode($result);

    exit;
}

////////////////////////
// INITIALIZE THE API //
////////////////////////
$myAPI = new MyAPI($request);
// Check Status
// is platform online
$status = $myAPI->CheckPlatformStatus();
if (array_key_exists('ERROR', $status)) {
    echo json_encode($status);
    exit;
}
//if ($status['status'] != 'Operative') {
//    echo json_encode("VariantDB is down, please try again later : " . $status['status']);
//    //throw new Exception('VariantDB is down, please try again later');
//    exit;
//}

// check API key.
// verify if apikey exists.
if (!array_key_exists('apiKey', $myAPI->request)) {
    // only allowed if from localhost
    $whitelist = array('127.0.0.1', '::1', $_SERVER['SERVER_ADDR']);
    if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
        echo json_encode("No API Key provided");
        trigger_error("No api key provided. Exit", E_USER_NOTICE);
        exit;
    } elseif (array_key_exists("uid", $myAPI->request)) {
        // called internally. Add the userid as apikey, if defined.
        $myAPI->request['apiKey'] = $myAPI->request['uid'];
        $myAPI->uid = $myAPI->request['uid'];
        $myAPI->request['local_call'] = 1;
    } else {
        echo json_encode("No uid provided for internal api call.");
        trigger_error("No uid provided for internal api call.", E_USER_ERROR);
        exit;
    }
} else {
    $verify = $myAPI->verifyKey($myAPI->request['apiKey']);
    if ($verify['status'] == FALSE) {
        // simulatie the checkApiKey call
        $doc = array("goal" => "General API goal is to automate VariantDB actions", "method" => "GET/POST", "arguments" => "required : apiKey", "return" => "depends on endpoint");
        echo json_encode(array("status" => "error", "msg" => "Invalid/no apiKey provided", 'help' => $doc));
        exit;
    }
}

if ($status['status'] != 'Operative') {
    // get access level
    $user_level = $myAPI->getUserLevel($myAPI->request['apiKey']);
    if ($user_level < 3) {
        echo json_encode("VariantDB is down, please try again later : " . $status['status']);
        //throw new Exception('VariantDB is down, please try again later');
        exit;
    }
}
// PROCESS THE PROVIDED REQUEST
$var = $myAPI->processAPI();
echo $var;
