<?php
if (count($_POST) > 0) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $affiliation = $_POST['affiliation'];
    $text = $_POST['text'];
    $subject = $_POST['subject'];
    $check = $_POST['check'];
} else {
    $name = $email = $affiliation = $text = $subject = $check = '';
}
if (isset($_POST['sendmail'])) {
    if (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email) || $email == "") {
        echo "<div class=section><h3>Invalid email address</h3><p>Please correct your emailadres to a valid one.</p></div>";
    } elseif ($affiliation == "affiliation" || $affiliation == "") {
        echo "<div class=section><h3>No affiliation specified</h3><p>Please enter the university or company you work for.</p></div>";
    } elseif ($name == "name" || $name == "") {
        echo "<div class=section><h3>No name specified</h3><p>Please enter your name.</p></div>";
    } elseif ($text == "message" || $text == "") {
        echo "<div class=section><h3>No message specified</h3><p>Please type a message, since that's what this form is meant for.</p></div>";
    } elseif ($subject == "subject" || $subject == "") {
        echo "<div class=section><h3>No subject specified</h3><p>Please enter a subject for your message</p></div>";
    } elseif ($check != "G" && $check != "g") {
        echo "<div class=section><h3>Wrong answer</h3><p>Answer the question please. This is a simple anti-spam check. If you don't know the answer, it's \"G\". :-) </p></div>";
    } else {
        //compose message, etc
        $message = "Message sent from https://$domain\r";
        $message .= "Sent by: $email\r";
        $message .= "Subject: $subject\r";
        $message .= "Message: \r\r";
        $message .= "$text\r";
        $headers = "From: $email\r\n";
        $headers .= "To: $adminemail\r\n";
        /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
        if (mail($email, "$subject", $message, $headers)) {
            echo "<div class=section>";
            echo "<h3>Thank you for contacting us.</h3>";
            echo "<p>The email has also been sent your own specified address.</p>";
            echo "<p><a href=\"index.php?page=main\">Back to Homepage</a></p>";
        } else {
            echo "<h4>Can't send email </h4>";
        }
    }
} else {
?>
    <div class="section">
        <h3>Contact us </h3>
        <h4>Send us an email</h4>
        <p>If you have questions, remarks about the site, please use this contact form to get in touch.</p>
    </div>
<?php
}

if ($name == "")
    $name = "name";
if ($email == "")
    $email = "email";
if ($subject == "")
    $subject = "subject";
if ($affiliation == "")
    $affiliation = "affiliation";
if ($text == "")
    $text = "message";
?>

<div class="section">
    <form action="index.php?page=contact" method="POST">
        <table cellspacing=0>
            <tr>
                <th>Your name:</td>
                <td><input type="text" name="name" value="<?php echo $name ?>" size="40" maxlength="40" onfocus="if (this.value == 'name') {this.value = '';}" /></td>
            </tr>
            <tr>
                <th>Your email:</td>
                <td><input type="text" name="email" value="<?php echo $email ?>" size="40" maxlength="40" onfocus="if (this.value == 'email') {this.value = '';}" /></td>
            </tr>
            <tr r>
                <th>Your Affiliation:</td>
                <td><input type="text" name="affiliation" value="<?php echo $affiliation ?>" size="40" maxlength="40" onfocus="if (this.value == 'affiliation') {this.value = '';}" /></td>
            <tr>
                <th>Subject:</td>
                <td><input type="text" name="subject" value="<?php echo $subject ?>" size="40" maxlength="40" onfocus="if (this.value == 'subject') {this.value = '';}" /></td>
            </tr>
            <tr>
                <th>DNA consists of A, C, T and ... ?</td>
                <td><input type="text" name="check" value="You should know this" size="40" onfocus="if (this.value == 'You should know this') {this.value = '';}" /></td>
            </tr>
            <tr>
                <td colspan=2 class=clear><textarea name="text" cols="100" rows="20" onfocus="if (this.value == 'message') {this.value = '';}"><?php echo $text ?></textarea></td>
            </tr>
            <tr>
                <td><input type="submit" name=sendmail value=" Send " class=button></td>
            </tr>
        </table>

</div>